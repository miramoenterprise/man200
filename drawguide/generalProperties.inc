@------------------------------------------------------------
@--- inc.genprops
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Generic drawing properties"
	longTitle="Generic drawing properties"
	dest="<elementName>.properties.generic_drawing_properties"
	/>
@------------------------------------------------------------

@- <@include> ${MD_INLINE}/inc.penprops

@------------------------------------------------------------
@- <@include> ${CTEXT}/pstyle.inc
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fill" long="fillNum" value="0 - 7, 15" >
Fill style.

<propertyDefault>15</propertyDefault>
</property>

@------------------------------------------------------------
<property short="a" long="rotate" value="number" >
Angle of rotation.

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="color" long="color" value="name" >
Color name.

<propertyDefault>Black</propertyDefault>
</property>

@------------------------------------------------------------
<property short="o" long="overprint" value="Y | N" >
Overprint.

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property short="tint" long="tint" value="percent" >
Tint per cent.

<propertyDefault>100</propertyDefault>
</property>

@------------------------------------------------------------
<property short="sep" long="separation" value="int" >
Separation number.

<propertyDefault>1</propertyDefault>
</property>

@------------------------------------------------------------
<@include> ${CTEXT}/runaroundTextGap.inc
@------------------------------------------------------------

<!--
<@suspend>

@------------------------------------------------------------
<property short="RA" long="runAround" value="N | C | B" >

Text runaround. N = no runaround,
C = runaround contour, B = runaround
bounding box.

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property short="RAgap" long="runAroundGap" value="dim" >

Runaround gap. Effective only if the <on>RA</on> option is
set to <ov>C</ov> or <ov>B</ov>.

<propertyDefault>6pt</propertyDefault>
</property>

<@resume>
-->


@------------------------------------------------------------
