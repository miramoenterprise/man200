@--------------------------------------------------------------------------------
@- drawguide.inc
@- include files for drawguide guide
@--------------------------------------------------------------------------------
<@include>	${refguide}/cover/cover.inc
<@include>	${refguide}/toc/toc
@- <chapterStart
@- 	dest="inlineMarkupElements"
@- 	>mmDraw markup elements
@- </chapterStart>
<#def> inINLINE         1
<#def> elementType      inline
<@include>	${drawguide}/introduction
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${drawguide}/aline
<@include>	${drawguide}/arc
<@include>	${drawguide}/circle
<@include>	${drawguide}/ellipse
<@include>	${drawguide}/plineto
<@include>	${drawguide}/point
<@include>	${drawguide}/polygon
<@include>	${drawguide}/polyline		@-- **** Put back
<@include>	${drawguide}/rectangle
<@include>	${drawguide}/textline
<@include>	${drawguide}/arrowdef
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

