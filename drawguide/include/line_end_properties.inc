@------------------------------------------------------------
@--- inc.line_end_props
@------------------------------------------------------------


@-===========================================================
<propertyGroup
	title="Line termination properties"
	name="Line termination properties"
	dest="<#elementName>.optgroup.line_termination_properties"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="lineCap"
	value="round | butt | square"
	>
<pdp>
Line termination style.
</pdp>

<propertyDefault>Round</propertyDefault>
</property>


@------------------------------------------------------------
<property
	name="arrowHead"
	value="none | head | tail | both"
	>

<pdp>
Arrowhead placement.
</pdp>

<propertyDefault>None</propertyDefault>
</property>

@-===========================================================
<propertyGroup
	title="Arrowhead options"
	dest="<#elementName>.optgroup.arrow_options"
	>See
Note <xparanumonly id="Polyline.note.arrowhead_parameters" /> on
page <xnpage id="Polyline.note.arrowhead_parameters" />.

</propertyGroup>

@------------------------------------------------------------
<property
	name="arrowType"
	value="filled | stick | hollow"
	>
<pdp>
Arrow type.
</pdp>

<propertyDefault>Filled</propertyDefault>
</property>


@------------------------------------------------------------
<property
	name="arrowLength"
	value="dim"
	>

<pdp>
Length of arrow head.
</pdp>

@- <propertyDefault>30</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="arrowTipAngle"
	value="number"
	>
<pdp>
Angle of arrow tip.
</pdp>

<propertyDefault>30</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="arrowBaseAngle"
	value="number"
	>

<pdp>
Angle of arrow base.
</pdp>

<propertyDefault>90</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="arrowHeadScale"
	value="N | Y"
	>

<pdp>
Enable arrow head scaling.
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
@- <property short="ScaleFactor" long="arrowScaleFactor"
@- 	value="dim" >
@- 
@- Scale factor for arrow head.
@- 
@- <propertyDefault>4 pt</propertyDefault>
@- </property>

@------------------------------------------------------------
