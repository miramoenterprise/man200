@---------------------------------------------------
@-- aline.inc
@---------------------------------------------------
<@xmacro> cArc {
	<#def> osq		{&#x2018;}
	<#def> csq		{&#x2019;}
	<#def> osqb		{��}
	<#def> csqb		{��}
	<#def> ca_Radius	5mm
	<#def> ca_L		04mm
	<#def> ca_T		14mm
	<#def> ca_pw		0.5pt
	<#def> ca_StartAngle	0
	<#def> ca_arcAngle	45
	@xgetvals(ca_)
	<#def> ca_L		@eval(<#ca_L> - <#ca_Radius>)
	<#def> ca_T		@eval(<#ca_T> - <#ca_Radius>)
	<#def> ca_W		@eval(<#ca_Radius> * 2)
	<#def> ca_H		<#ca_W>
	@- <@write> error {<#ca_W> W-------------------------}
	@- <@write> error {<#ca_H> H-------------------------}
	<Arc
		L="<#ca_L>"
		T="<#ca_T>"
		W="<#ca_W>"
		H="<#ca_H>"
		StartAngle="<#ca_StartAngle>"
		arcAngle="<#ca_arcAngle>"
		Arrow="Head"
		penColor="<#ca_arcColor>"
		penWidth="<#ca_pw>"
		@- $ArrowDef[<#ca_ArrowNo>]
		ArrowType="Filled"
		arrowLength="1.4mm"
		arrowTipAngle="18"
		arrowBaseAngle="60"
		lineCap="butt"
		/>
}
<@xmacro> - LineAngle {
	<#def> la_Angle		60
	<#def> la_ArrowNo	1
	<#def> la_AFrame.opts	{}
	<#def> la_textFmt	{F_AnnoMedium}
	<#def> la_textStartAngle	{}
	<#def> la_textStartRadius	{}
	@xgetvals(la_)
	@- <AFrame W="14mm" H="14mm" P="R" A="R" fill="15" <#la_AFrame.opts> >
	@- <AFrame W="16mm" H="14mm" P="R" A="R" fill="15"
	<AFrame W="48.1234pt" H="14mm" position="runin" align="end" fill="15"
		penWidth="0.5pt"
		@- <#la_AFrame.opts>
		>
	<mmDraw>
	<ALine L="<#la_L>" T="<#la_T>"
		Angle="<#la_Angle>"
		Arrow="None"
		ArrowNo="<#la_ArrowNo>"
		lineLen="<#la_lineLen>"
		@- textAngle="<#la_Angle>"
		pw="<#la_pw>"
		penColor="<#la_lineColor>"
		/>
		<cArc Radius="<#la_Radius>"
			penWidth="<#la_apw>"
			arcAngle="<#la_Angle>"
			arcColor="<#la_arcColor>"
			L="<#la_L>"
			T="<#la_T>"
			ArrowNo="<#la_arcArrowNo>"
			/>
	@-----------------------------------------------
	<#def> la_L2		@eval(<#la_L> - 1.5)
	<#def> la_T2		@eval(<#la_T> + 1.5)
	@-----------------------------------------------
	@--- Vertical axis
	<ALine T="<#la_T2>" L="<#la_L>"
		Arrow="None"
		penColor="Blue"
		penWidth="0.5pt"
		@- penStyle="3pt 0.5pt 3pt 3pt 3pt 0.5pt"
		lineLen="11mm"
		Angle="0"
		textAlign="C"
		Ky="100"
		textPos="T"
		textAngle="90"
		p="5pt"
		><#osq>L<#csq></ALine>
	@--- Horizontal axis
	<ALine L="<#la_L2>" T="<#la_T>"
		Arrow="None"
		penColor="Blue"
		penWidth="0.5pt"
		lineLen="11mm"
		@- textAlign="C"
		textPos="T"
		Angle="90"
		p="5pt"
		><#osq>T<#csq></ALine>
}{
	@if(@len(<#la_textStartAngle>) AND @len(<#la_textStartRadius>)) {
		<#def> la_textX	@eval(<#la_textStartRadius> * @sin(<#la_textStartAngle>))
		<#def> la_textY	@eval(<#la_textStartRadius> * @cos(<#la_textStartAngle>))
		<#def> la_textX	@eval(<#la_L> + <#la_textX>)
		<#def> la_textY	@eval(<#la_T> - <#la_textY>)
		<TextLine L="<#la_textX>" T="<#la_textY>" A="L"
			fmt="<#la_textFmt>">$LineAngle</TextLine>
		}
	</mmDraw>
	</AFrame>
}
@---------------------------------------------------
