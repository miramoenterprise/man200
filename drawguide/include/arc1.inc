@------------------------------------------------------------
@-- arc1.inc
@-
@-- Included in <Arc> element
@------------------------------------------------------------
<@macro> arc1 {
@------------------------------------------------------------
<#def> osqb             {&#x2018;}
<#def> csqb             {&#x2019;}
@------------------------------------------------------------
<#def> arc.L            06mm
<#def> arc.L            <#arc.L>
<#def> arc.T            06mm
<#def> arc.W            30mm
<#def> arc.H            20mm
<#def> arc.startAngle   52
<#def> arc.arcAngle     92
@------------------------------------------------------------
}
@- <|arc1>


<#def> arc1.inc {
<#def> arc.X		@eval(<#arc.L> + <#arc.W> / 2)
<#def> arc.Y		@eval(<#arc.T> + <#arc.H> / 2)
@------------------------------------------------------------
@--- Distance to bottom dimension line, `W'.
@------------------------------------------------------------
<#def> arc.BDLoffset	02mm
<#def> arc.BDLT		@eval(<#arc.T> + <#arc.H> + <#arc.BDLoffset>)
@------------------------------------------------------------
@--- Distance to right dimension line, `H'.
@------------------------------------------------------------
<#def> arc.RDLoffset	02.5mm
<#def> arc.RDLT		@eval(<#arc.L> + <#arc.W> + <#arc.RDLoffset>)
@------------------------------------------------------------
@------------------------------------------------------------
@--- Filled ellipse
@------------------------------------------------------------
@- <Ellipse L="<#arc.L>" T="<#arc.T>" W="<#arc.W>" H="<#arc.H>"
<Ellipse left="<#arc.L>" top="<#arc.T>" width="<#arc.W>" height="<#arc.H>"
	@- pw="0.2pt"
	@- pen="15"
	@- color="Yellow"
	@- fill="3"
	penOpacity="0"
	penWidth="0"
	fillColor="Yellow"
	fillTint="30"
	fillOpacity="100"
	/>

@------------------------------------------------------------
@--- Rectangular bounding box
@------------------------------------------------------------
<Rectangle L="<#arc.L>" T="<#arc.T>" W="<#arc.W>" H="<#arc.H>"
	@- color="Black"
	@- pw="0.3pt"
	@- pstyle="3pt 1pt"
	penColor="Black"
	penWidth="0.3pt"
	penStyle="3pt 1pt"
	/>
@------------------------------------------------------------
@--- Dashed ellipse
@------------------------------------------------------------
@- <Ellipse L="<#arc.L>" T="<#arc.T>" W="<#arc.W>" H="<#arc.H>"
<Ellipse L="<#arc.L>" T="<#arc.T>" W="<#arc.W>" H="<#arc.H>"
	penColor="Black"
	penWidth="0.2pt"
	penStyle="3pt 1pt"
	@- penStyle="dashed"
	/>

@------------------------------------------------------------
@--- Top dimension line, `T'.
@------------------------------------------------------------
<ALine L="12mm" T="0mm" Angle="180" 
	lineLen="<#arc.T>"
	textLen="2.2mm"
	arrowHead="both"
	textAngle="90"
	arrowNumber="2"
	penColor="Blue"
	penWidth="0.3pt"
	Ky="20"
	fmt="F_AnnoSmall"
	@- >��T��</ALine>
	><sq>T</sq></ALine>

@------------------------------------------------------------
@--- Left dimension line, `L'.
@------------------------------------------------------------
<ALine L="00mm" T="12mm" Angle="90" 
	lineLen="<#arc.L>"
	textLen="2.5mm"
	arrowHead="both"
	penColor="Blue"
	penWidth="0.3pt"
	textAngle="90"
	arrowNumber="2"
	fmt="F_AnnoSmall"
	@- >��L��</ALine>
	><sq>L</sq></ALine>

@------------------------------------------------------------
@--- Right dimension line, `H'.
@------------------------------------------------------------
<ALine L="<#arc.RDLT>" T="<#arc.T>" Angle="180" 
	lineLen="<#arc.H>"
	textLen="3mm"
	textAngle="90"
	arrowHead="both"
	@- penColor="Green"
	penColor="Blue"
	penWidth="0.3pt"
	lineCap="butt"
	arrowNumber="2"
	fmt="F_AnnoSmall"
	><sq>H</sq></ALine>

@------------------------------------------------------------
@--- Bottom dimension line, `W'.
@------------------------------------------------------------
<ALine L="<#arc.L>" T="<#arc.BDLT>" Angle="90" 
	lineLength="<#arc.W>"
	textLength="3mm"
	arrowHead="both"
	penColor="Blue"
	arrowNumber="2"
	penWidth="0.3pt"
	fmt="F_AnnoSmall"
	@- >��W��</ALine>
	><sq>W</sq></ALine>

@------------------------------------------------------------
@--- Axes
@------------------------------------------------------------
<#def> arc.Wx	@eval(<#arc.W> / 2 + 3)
<#def> arc.Hx	@eval(<#arc.H> / 2 + 3)
<#def> arc.X1	@eval(<#arc.X> - 2)
<#def> arc.Y1	@eval(<#arc.Y> + 2)
@------------------------------------------------------------
<#def> arc.X2	@eval((<#arc.W> / 2) * @sin(<#arc.startAngle>))
<#def> arc.Y2	@eval((<#arc.W> / 2) * @cos(<#arc.startAngle>))
<#def> arc.Y2	@eval(<#arc.Y2> * <#arc.H> / <#arc.W>) 
<#def> arc.T2a	@eval(<#arc.T> + <#arc.H> / 2 - <#arc.Y2>) 
<#def> arc.a1	@atan(<#arc.X2> / <#arc.Y2> ) 
<#def> arc.a1	@round(<#arc.a1>, 3) 
<#def> arc.L1	@sqrt(<#arc.Y2> * <#arc.Y2> + <#arc.X2> * <#arc.X2>) 
<#def> arc.L1	@eval(<#arc.L1> + 1 )
@- <@write> error {----------------- <#arc.a1>  <#arc.L1>}
@------------------------------------------------------------
<#def> arc.zz	@eval(<#arc.startAngle> + <#arc.arcAngle>)
<#def> arc.X3	@eval((<#arc.W> / 2) * @sin(<#arc.startAngle> + <#arc.arcAngle>))
<#def> arc.Y3	@eval((<#arc.W> / 2) * @cos(<#arc.startAngle> + <#arc.arcAngle>))
<#def> arc.X3	@eval((<#arc.W> / 2) * @sin(<#arc.zz>))
<#def> arc.Y3	@eval((<#arc.W> / 2) * @cos(<#arc.zz>))
@- <@write> error {----------------- <#arc.startAngle>  <#arc.arcAngle>}
<#def> arc.Y3	@eval(<#arc.Y3> * <#arc.H> / <#arc.W> )
<#def> arc.T3a	@eval(<#arc.T> + <#arc.H> / 2 - <#arc.Y3>) 
<#def> arc.a2	@atan(<#arc.X3> / <#arc.Y3> ) 
<#def> arc.a2	@round(<#arc.a2>, 3) 
<#def> arc.a2	@eval(<#arc.a2> + 180)
<#def> arc.L2	@sqrt(<#arc.Y3> * <#arc.Y3> + <#arc.X3> * <#arc.X3>) 
<#def> arc.L2	@eval(<#arc.L2> + 1 )
@- <@write> error {----------------- <#arc.a2>  <#arc.L2>}
@------------------------------------------------------------
<#def> arc.Ha	@eval(<#arc.T3a> - <#arc.T2a>) 
<#def> arc.Wa	@eval(<#arc.W> / 2) 
@------------------------------------------------------------

@--- Axes
<ALine L="<#arc.X>" T="<#arc.Y>" Angle="0" 
	lineLength="<#arc.Hx>"
	pw="0.2pt"
	/>
<ALine L="<#arc.X>" T="<#arc.Y>" Angle="180" 
	lineLen="4mm"
	pstyle="1.5pt 1.5pt"
	pw="0.2pt"
	/>
<ALine L="<#arc.X1>" T="<#arc.Y>" Angle="90" 
	lineLength="<#arc.Wx>"
	pw="0.2pt"
	/>
@--- T2a
<ALine L="<#arc.X>" T="<#arc.T2a>" Angle="90" 
	lineLength="<#arc.Wa>"
	pstyle="0.8pt 0.8pt"
	penColor="Red"
	pw="0.2pt"
	/>
@--- T3a
<ALine L="<#arc.X>" T="<#arc.T3a>" Angle="90" 
	lineLength="<#arc.Wa>"
	pstyle="1pt 1pt"
	penColor="Red"
	pw="0.2pt"
	/>
<ALine L="<#arc.X>" T="<#arc.T3a>" Angle="0" 
	lineLength="2mm"
	pstyle="1pt 1pt"
	penColor="Red"
	pw="0.2pt"
	/>
<ALine L="24mm" T="0mm" Angle="180" 
	lineLength="<#arc.T2a>"
	penColor="Red"
	arrowHead="both"
	@- ArrowNo="2"
	textAngle="90"
	textAlign="L"
	pw="0.3pt"
	Kx="40"
	Ky="-60"
	fmt="F_AnnoSmall"
	p="5pt"
	>Ta</ALine>
<ALine L="0" T="08.5mm" Angle="90" 
	lineLen="<#arc.X>"
	penColor="Red"
	Arrow="Both"
	ArrowNo="2"
	textAngle="90"
	pw="0.3pt"
	Kx="-65"
	Ky="-40"
	fmt="F_AnnoSmall"
	p="5pt"
	>La</ALine>

<ALine L="18mm" T="<#arc.T2a>" Angle="180" 
	lineLen="<#arc.Ha>"
	penColor="Red"
	Arrow="Both"
	ArrowNo="2"
	textAngle="90"
	textAlign="L"
	pw="0.3pt"
	Kx="-150"
	@- Ky="-3"
	fmt="F_AnnoSmall"
	p="5pt"
	>Ha</ALine>

<ALine L="<#arc.X>" T="23.0mm" Angle="90" 
	lineLen="<#arc.Wa>"
	penColor="Red"
	Arrow="Both"
	textAlign="R"
	ArrowNo="2"
	textAngle="90"
	pw="0.3pt"
	Kx="-150"
	Ky="-40"
	fmt="F_AnnoSmall"
	p="5pt"
	>Wa</ALine>


@------------------------------------------------------------
@--- First anno arc line
@------------------------------------------------------------
<#def> arc.factor	0.3
<#def> xL	@eval(<#arc.L> + (<#arc.W> - (<#arc.W> * <#arc.factor>)) / 2)
<#def> xT	@eval(<#arc.T> + (<#arc.H> - (<#arc.H> * <#arc.factor>)) / 2)
<#def> xW	@eval(<#arc.W> * <#arc.factor>)
<#def> xH	@eval(<#arc.H> * <#arc.factor>)
@- bug
<Arc    left="<#xL>"
	top="<#xT>"
	width="<#xW>"
	height="<#xH>"
	penColor="Blue"
	penWidth="0.300173pt"
	startAngle="0"
Arrow="Head"
	@- ArrowNo="2"
	arcAngle="<#arc.startAngle>"
	 />
<ALine L="<#arc.X>" T="<#arc.Y>" Angle="<#arc.a1>" 
	lineLen="<#arc.L1>"
	pw="0.2pt"
	penColor="Red"
	pstyle="3pt 1pt"
	/>
<TextLine
	L="21.5mm"
	T="12mm"
	fmt="F_AnnoSmall"
	textColor="Blue"
	p="4.5pt"
	@- >��startAngle��</TextLine>
	><#osqb>startAngle<#csqb></TextLine>
@------------------------------------------------------------
@--- Second anno arc line
@------------------------------------------------------------
<#def> arc.factor	0.35
<#def> xL	@eval(<#arc.L> + (<#arc.W> - (<#arc.W> * <#arc.factor>)) / 2)
<#def> xT	@eval(<#arc.T> + (<#arc.H> - (<#arc.H> * <#arc.factor>)) / 2)
<#def> xW	@eval(<#arc.W> * <#arc.factor>)
<#def> xH	@eval(<#arc.H> * <#arc.factor>)
@- bug
<Arc    L="<#xL>"
	T="<#xT>"
	W="<#xW>"
	H="<#xH>"
	@- penColor="Blue"
	penWidth="0.3pt"
	startAngle="<#arc.startAngle>"
@-	startAngle="<#arc.a1>"
	Arrow="Head"
	@- ArrowNo="2"
	penColor="Red"
	arcAngle="<#arc.arcAngle>"
	 />
<ALine L="<#arc.X>" T="<#arc.Y>" Angle="<#arc.a2>" 
	lineLen="<#arc.L2>"
	penColor="Red"
	pstyle="3pt 1pt"
	pw="0.3pt"
	/>
<TextLine
	L="26.5mm"
	T="18mm"
	fmt="F_AnnoSmall"
	p="4.5pt"
	textColor="Red"
	@- >��arcAngle��</TextLine>
	><sq>arcAngle</sq></TextLine>

@------------------------------------------------------------
@- Actual arc code here: ....
@------------------------------------------------------------
}
@------------------------------------------------------------
@- <#arc1.inc>
@------------------------------------------------------------
