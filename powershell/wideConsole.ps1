#--------------------------------------------------------------------------------
#- wideConsole.ps1
#--------------------------------------------------------------------------------

# $host.ui.RawUI.ForegroundColor = "black"
# $host.ui.RawUI.BackgroundColor = "gray"

#--------------------------------------------------------------------------------
#- Get current console size
#--------------------------------------------------------------------------------
function getCurrentConsoleSize {
	$buffer			= $host.ui.RawUI.BufferSize
	$OrigBufferWidth	= $buffer.width
	$OrigBufferHeight	= $buffer.height

	$windowSize		= $host.ui.RawUI.Get_WindowSize()
	$windowSize		= $host.ui.RawUI.Get_WindowSize()
	$OrigWindowWidth	= $windowSize.width
	$OrigWindowHeight	= $windowSize.height
}

function revert {
	$a		= (Get-Host).UI.RawUI
	$b		= $a.WindowSize
	$b.Width	= $OrigWindowWidth
	$b.Height	= $OrigWindowHeight
	$a.WindowSize	= $b

}

# Write-host $OrigWindowWidth
# Write-host $OrigWindowHeight
# Write-host $OrigBufferWidth
# Write-host $OrigBufferHeight

#--------------------------------------------------------------------------------
#- Set wide console size
#--------------------------------------------------------------------------------
function setWideConsoleSize {
	# $bs			= $host.ui.RawUI.BufferSize
	# $bs.Width		= 230
	# $bs.Height		= 50
	# $host.UI.RawUI.Set_BufferSize($bs)
	# $xxx			= (Get-Host).UI.RawUI
	# $yyy			= $xxx.BufferSize
	# $yyy.Width		= 240
	# $yyy.Height		= 70
	# $xxx.BufferSize		= $yyy

$pshost = get-host
$pswindow = $pshost.ui.rawui

$newsize = $pswindow.buffersize
$newsize.height = 3000
$newsize.width = 250
$pswindow.buffersize = $newsize


	$ws			= $host.ui.RawUI.WindowSize
	$ws.Width		= 220
	$ws.Height		= 50
	$host.ui.RawUI.Set_WindowSize($ws)
}
