#--------------------------------------------------------------------------------
#- run.ps1
#--------------------------------------------------------------------------------
#
# Create: mmSpecialCharacters.xml
#
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#- ${modifiedDate} used in macros.mmp
#--------------------------------------------------------------------------------
$env:modifiedDate	= Get-Date -format "ddd d MMM yyyy (HH.mm.ss)"
# write-host "$env:modifiedDate"
#--------------------------------------------------------------------------------
$env:commentsFile	= "xxz43"
$env:tmpFile		= "xxz44"
$env:outputFile		= "specialCharacters.tmp"	#-- Used in macros.mmp
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
mmpp comments.mm | out-file -enc oem $env:commentsFile
mmpp control.mmp | out-file -enc oem $env:tmpFile
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
mmpp -Mfile macros.mmp $env:tmpFile > $null
#--------------------------------------------------------------------------------
# C:\u\miramo\xdev\tarma\config

#--------------------------------------------------------------------------------
rm "$env:commentsFile"
rm "$env:tmpFile"
#--------------------------------------------------------------------------------

exit
mmpp -Mfile macros.mmp mmSpecialCharacters.xml > $null
mmpp control.mmp > $null


$env:qrcTmp     = "$env:pdhelp\xxTmp.qrc"

function exitMessage {
        # write-host                    "OUTPUT FILE: $env:outputFile"
        xcheck val --well-formed "$env:outputFile"
        $xmlStatus              = $LastExitCode
        # write-host            "OUTPUT STATUS: $xmlStatus"
        # See:
        # http://techibee.com/powershell/what-is-lastexitcode-and-in-powershell/1847
        if ($?) {
                # Copy files
                #----------------------------------------------------------------
                #- Copy files:
                #       mmPageDesignerHelp_xx.xml"      --> mmComposer schema folder
                #       pdhelp.qrc                      --> mmDesignerApp folder
                #----------------------------------------------------------------
                if ($pdhTest -eq 0 -and $xmlStatus -eq 0) {
                        #- Everything worked, no '-t' option & the XML file is valid
                        cp "$env:outputFile"            "$env:mmcSchemaFile"
                        Write-Host "pdhrun: Success. File copied to:  $env:mmcSchemaFile"
                        }
                else {
                        #- Just testing w/ '-t', or XML file invalid
                        if ($xmlStatus = 1) {
                                Write-Host "pdhrun: INVALID XML!!! ($env:mmcSchemaFile)"
                                }
                        Write-Host "pdhrun: -t[est]  File NOT copied to:  $env:mmcSchemaFile"
                        }
                }
        else {
                write-host      "pdhrun: FAILED !!! File not copied"
                }
}

#--------------------------------------------------------------------------------
#- Now modify and copy pdhelp.qrc file
#--------------------------------------------------------------------------------
$env:qrcTmp     = "$env:pdhelp\xxTmp.qrc"
gc "$env:tmpQRCfile" | sort | get-unique | out-file -enc oem "$env:qrcTmp"
write-host "pdhrun: Copying $env:qrcTmp To: $env:tmpQRCfile"
cp "$env:qrcTmp" "$env:tmpQRCfile"
rm "$env:qrcTmp"


