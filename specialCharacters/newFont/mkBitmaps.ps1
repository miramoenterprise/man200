#--------------------------------------------------------------------------------
#- mkBitmaps.ps1
#--------------------------------------------------------------------------------
param(
	[string]$uCharNum,	# -uCharNum	F299		(Required, text)
	[string]$charColor,	# -charcolor	'#22F322'	(Required, text)
	# ---------------------------------------------------------------------
	# Residue after -param/value and -switch params extracted
	# (usually last argument, e.g. input file name):
	# [parameter(Mandatory=$true, Position=0,
	[parameter(Position=0,
		ValueFromRemainingArguments=$true)][array]$lastArgs,
	# ---------------------------------------------------------------------
	[switch] $dummy
	)
$pointSize	= "48"
$pointSize	= "72"
$pointSize	= "96"
$pointSize	= "250"
$pointSize	= "24"
$pointSize	= "120"
$uChar = [regex]::Unescape("\u$uCharNum")	#--- Convert text to UTF-8
convert -strip -background none -font mmts5.ttf -trim -fill "$charColor"  -pointsize $pointSize label:"$uChar" -depth 8 "bitmaps\ux$ucharNum.png"
write-host	"$uCharNum	$charColor"
#--------------------------------------------------------------------------------
