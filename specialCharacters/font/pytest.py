

import sys, struct, StringIO, argparse
from png import PNG

# python27 emoji_builder.py -V -p 120 mmts4.ttf output\mmts4.ttf bitmaps/ux

print "This is the name of the script: ", sys.argv[0]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)

options = []

option_map = {
	"-V": "verbose",
	"-O": "keep_outlines",
	"-U": "uncompressed",
	"-C": "keep_chunks",
}
print "The arguments are: " , str(sys.argv)

for key, value in option_map.items ():
	if key in sys.argv:
		options.append (value)
		sys.argv.remove (key)

print "The arguments are: " , str(sys.argv)

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--ppem", type=int, help="pixels per em")
parser.add_argument("sourceFont")
parser.add_argument("outFont")
#- parser.add_argument("prefixes")
parser.add_argument('prefixList', nargs=argparse.REMAINDER)
args = parser.parse_args()

print "PPEM is: " , args.ppem
print "outFont: " , args.outFont
print "prefixList: " , args.prefixList


# if len (argv) < 4:
#	print >>sys.stderr, """




