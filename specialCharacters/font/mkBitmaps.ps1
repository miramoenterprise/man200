#--------------------------------------------------------------------------------
#- mkBitmaps.ps1
#--------------------------------------------------------------------------------
param(
	[string]$uCharNum,	# -uCharNum 0299	(Required)
	# ---------------------------------------------------------------------
	# Residue after -param/value and -switch params extracted
	# (usually last argument, e.g. input file name):
	# [parameter(Mandatory=$true, Position=0,
	[parameter(Position=0,
		ValueFromRemainingArguments=$true)][array]$lastArgs,
	# ---------------------------------------------------------------------
	[switch] $dummy
	)
write-host	"$uCharNum"
#--------------------------------------------------------------------------------
