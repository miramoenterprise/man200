
A. Objective
============

Create a script to convert a sub-set of the characters/glyphs in a 'regular' .TTF font
into an OpenType CBLC/CBDT chromatic, bitmap font.

Each glyph in the output CBLC/CBDT font should have similar metrics (wrt to positioning,
Advance Width and LSB) to the glyph in the source 'regular' .ttf font.

Each PNG file included in the CBLC/CBT font will have a bitmap representation of the
corresponding source character in a single, uniform, user-specifiable color:
apart from the glyph itself, the PNG will be transparent.

It is assumed that only one strike, Format 17: small metrics, is needed. This is all that's
in 'NotoColorEmoji.ttf', which works fine in the needed context and is far more complicated.


B. Intended use
===============

The script will used to process a single 'regular' .ttf file, comprising up to one
hundred glyphs. It will be used on rare occaions, e.g. when the shapes of some glyphs
need to be modified, or an additional glyph is needed. It is not intended to distribute
the script publicly. The resulting CBLC/CBDT font will be embedded as a (hidden) resource
for use in a GUI application.

BTW Datazone does not aspire to any exclusive ownership or IP rights relating to the script.


C. Workflow
===========

On the rare occasions when the script needs to be run, the workflow is intended as follows:

	1. Modify existing glyph shapes or add new glyphs in the 'regular' .ttf font,
	   using a simple low cost font program, e.g. FontCreator or Type 3.2.

	2. Run script to create updated CBLC/CBDT font containing selected characters from
	   the source font.

The script does not need to be polished as for general use.



D. Script language
==================

The controlling script will ideally be either Powershell or Python. Must run on Windows 7
and above.


E. Script constants
===================

Ideally the script will have the following parameters embedded within it.

E1. The name of the .ttf source file, e.g. $sourcFile="zzz.ttf".
    "zzz.ttf" is a file in the current working folder. There's no need
    for sophisticated path handling.

E2. A two-column table of Unicode addresses and the color specification for
    the character. E.gj

	00A9	#992241
	2003	#000088
	1F443	#000088

    The first column is the Unicode character. The second column is the color applicable
    to that character.

    This table can be in any easily editable format. It could be, e.g.

	def A[1] "00A9,		#992241"
	def A[2] "IF443,	#000088"

    or any other easy to edit and maintain format in the script language.
 
    This table may be within the top-level script itself, or read from an external file,
    whichever is easiest.

    It is the responsibility of the user to ensure that the Unicode character
    addresses specified in this table are actually in the font specified by $sourceFile"


E3. Some other values

    To specify values to be used for commands used in the script. E.g. 'pointsize' for
    use in the convert command. (See below)


F. Proposed script operation
============================

Two steps:

	B1. Create the required .PNG bitmaps for each character. At first glance
	   ImageMagick can do a good job here. See below.

	B2. Compile the .PNGs into a CBLC/CBDT font. At first glance 'emoji_builder.py',
	    included in the FontTools python package, can do this, but maybe needing some
	    small modifications. See below. Alternatively it can be done by creating a
	    .ttc file (this alternative can hopefully be avoided, as probably expensive
	    in time).


G. Proposed script Step [1]
===========================

    This uses the Imagemagick 'convert' command within the script.

    The following is in Powershell syntax.

        $color		= "#880000"
        $char		= [regex]::Unescape("\u2003")
        $file		= "uni2003"
        $pngDPI		= "96"
    
        For EACH char in table 'E2' REPEAT {
            convert -background none -font mmts4.ttf -trim -fill "#880000" \
		      -pointsize $pngDPI label:"$char" -depth 8 "$file.png"
	    }

     I have a very limited test of the above. The ouput bitmaps contain faithful
     replicas of the .ttf source glyphs. However some more parameters may be needed


H. Proposed script Step [2]
===========================

     Run emoji_builder.py to create the CBLC/CBDT font.

     I have no idea if this will actually work. I have not tried it (don't know
     Python & am lazy). But it looks like it should work, subject to some small
     modifications that maybe Roel could contemplate. See Notes below.



Random Notes
============

     The following notes are a random mish-mash of things of things that may
     or may not be relevant to the process. They are intended mainly as reminders
     to the author.

	Reading: https://www.microsoft.com/typography/otspec/cbdt.htm

	It says 'byte alignment is sufficient'. Does the .png produced by
	the above Imagemagick convert command have 'byte alignment'?

	Should the -depth value be 32 instead of 8? (OTOH, '8' seems to produce
	a nice .png, with a single flat color and transparency, as needed.)

	Some characters in the source, regular .ttf have Advance Width 0, i.e. negative
	values for LSB and RSB.  emoji_builder.py does not seem to allow for x_bearing < 0

	In other words, AFAICS, emoji_builder.py does not take account of the 'lsb'
	values in the <hmtx> table, only the 'width' values. Might be a small change
	to fix that? Understandably perhaps emoji_builder.py assumes people want emojis
	in a monospaced font. This may explain why the emoji_builder.py help message
	says:

		"All images for the same strike should have the same size
		for best results."

	AFAIK there's no real basis for that in the CBLC/CBDT spec.

	Some Emoji fonts are implemented as monospaced fonts. E.g. in the case of
	NotoColorEmoji.ttf all the characters have identical CBDT metrics.

         	<height value="128"/>
         	<width value="136"/>
         	<BearingX value="0"/>
         	<BearingY value="101"/>
         	<Advance value="136"/>
	

        According to: https://www.microsoft.com/typography/otspec/cbdt.htm

		"Images for each individual glyph are stored as straight PNG data.
		Only the following chunks are allowed in such PNG data:
		IHDR, PLTE, tRNS, sRGB, IDAT, and IEND.
		If other chunks are present, the behavior is undefined.
		The image data shall be in the sRGB colorspace, regardless of color
		information that may be present in other chunks in the PNG data.
		The individual images must have the same size as expected by the table
		in the bitmap metrics."

	This may be a problem, if the Imagemagick 'convert' (above) produces PNGs
	that include dis-allowed chunks.

	The subject font contains several glyphs duplicated at different code points.
	Ideally these duplicates would be referenced via the GSUB table or somesuch
	(I'm guessing). But ther's no need to bother with this refinement.


If the above proposed is infeasible for any reason, we are left having to make a script
that creates a .ttx xml input file. This seems a lot of work, and hopefully can be
avoided.

Rick
