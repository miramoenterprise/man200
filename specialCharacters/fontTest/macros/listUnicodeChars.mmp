@------------------------------------------------------------
@- mmp.listUnicodeChars
@- 
@- called by runTTX.sh
@- 
@------------------------------------------------------------
<#def> SYMBOL 	{${SYMBOL}}
@- <@write> error {SYMBOL: <#SYMBOL>}
@------------------------------------------------------------
<@include> ${CONTROL}/mmp.uniIndex
<@include> ${CONTROL}/mmp.fontListColumnWidths
@------------------------------------------------------------
<#def> nl			{
}
<#def> selectedFormat		{}
<#def> selectedPlatform		{}
<#def> selectedEncoding		{}
<#def> gotCmap		0
<#def> cm.format	4
<#def> cm.format	4

<#def> cm.plat		0
<#def> cm.plat		3
				@--- 0 = Unicode, 1 = Mac, 3 = Win
<#def> cm.enc		3
<#def> cm.enc		1
				@--- version of Unicode, if pID = 0
				@--- Macintosh encoding, if pID = 1
				@--- Windows encoding, if pID = 3
@------------------------------------------------------------
@- The file 'mkUnicodeCharArray.mmp'defines two arrays:
@-
@-      uniHexChar[n]
@-      uniCharText[n]
@-
@------------------------------------------------------------
<@include> ${MBIN}/macros/mkUnicodeCharArray.mmp	@--- Unicode char names
@------------------------------------------------------------
<@include> ${MBIN}/macros/uni_heads.mmp		@--- Gets Unicode char blocks
@------------------------------------------------------------
<@include> ${CHARMACS}/mmp.getFontMetaData	@--- Gets the font metadata
@------------------------------------------------------------
<@macro>  storeCharData	{
		<#def> itemCount[<#mapTblCount>]	{<#mapTblCount>}
		<#def> charhex[<#mapTblCount>]		{<#code>}
		<#def> psName[<#mapTblCount>]		{<#name>}
}
@------------------------------------------------------------
<#def> processedCmap1		0
<#def> processedCmap2		0
<#def> processedCmap3		0
<#def> processedCmap4		0
<#def> mapTblCount		0
@------------------------------------------------------------
<@xmacro> pdfMkDestName {
	<#def> pdfMkDestName.name	{}
	@xgetvals(pdfMkDestName.)
	@if(@len(<#pdfMkDestName.name>)) {<AFrame W=0mm H=0.4mm P=I fill=15 >
		<TextFrameDef L=-1mm T=-1mm W=44mm H=24mm p=Y fill=15 >
		[ /Dest /<#pdfMkDestName.name> /DEST FmPD
		</TextFrameDef>
		@print(</AFrame>, n)}
	@else	{
		<@write> error {Missing value for PDF Named Destination}
		}
}
@------------------------------------------------------------
<@xmacro> fontNameInIndex {
	<#def> fontIndexName	${FONT_INDEX_NAME}
	<#def> fnii.Range	Start
	@xgetvals(fnii.)
	@print({<IX fmt=Index ix-page=<#fnii.Range>})
		ix-show=N ><#fontIndexName></IX><IX fmt=Index
		ix-page=<#fnii.Range> ix-show=N >font<IXsub/><#fontIndexName></IX>
}
@------------------------------------------------------------
<@macro>  charDestinations	{
	<#def> charDests	{${CHAR_DESTINATIONS}}
	<#def> includeDests	1
	<#def> barPos		@match({<#charDests>}, {\|})
        @if(@match({<#charDests>}, {None})) {
		<#def> includeDests	0
		}
	@else	{
		@--- There are one or more char destinations
		<#def> numCharDests		@eval(@matchcount({<#charDests>}, {\|}) +1)
		@--- Get position of first bar
		@for(cdest = 1 to <#numCharDests>) {
			<#def> barPos		@match({<#charDests>}, {\|})
			@if($$cdest = <#numCharDests>)  {
				<#def> charDest		{<#charDests>}
				}
			@else	{
				<#def> charDest		@substr({<#charDests>}, 0, <#barPos>)
				}
			<#def> charDests		@substr({<#charDests>}, @eval(<#barPos>  +1))
			<#def> charDest			@sub({<#charDest>}, {\|}, {})
			<#def> decimalDest		@hex2dec(<#charDest>)
			<#def> includeDest[<#decimalDest>]		1
			}
		}
}
<|charDestinations>
@------------------------------------------------------------
<@macro>  charRanges	{
	<#def> ranges		{${CHAR_RANGE}}
	<#def> barPos		@match({<#ranges>}, {\|})
	<#def> allChars		0
        @if(@match({<#ranges>}, {All})) {
		<#def> allChars		1
		}
	@else	{
		<#def> numRanges	@eval(@matchcount({<#ranges>}, {\|}) +1)
		@--- Get position of first bar
		@for(r = 1 to <#numRanges>) {
			<#def> barPos		@match({<#ranges>}, {\|})
			@if($$r = <#numRanges>)  {
				<#def> range	{<#ranges>}
				}
			@else	{
				<#def> range	@substr({<#ranges>}, 0, <#barPos>)
				}
			<#def> ranges		@substr({<#ranges>}, @eval(<#barPos>  +1))
			<#def> range		@sub({<#range>}, {\|}, {})
			<#def> range[$$r, 1]	@sub({<#range>}, {-.*$}, {})
			<#def> range[$$r, 2]	@sub({<#range>}, {^.*-}, {})
			<#def> range[$$r, 1]	@hex2dec($range[$$r, 1])
			<#def> range[$$r, 2]	@hex2dec($range[$$r, 2])
			}
		@for(r = 1 to <#numRanges>) {
			@for(show = $range[$$r, 1] to $range[$$r, 2]) {
			<#def> showChar[$$show]		Y
				}
			}
		}
}
@------------------------------------------------------------
<|charRanges>
@------------------------------------------------------------
<@macro>  adjustedHex	{
	<#def> charHEX		{$1}
	<#def> charHEX		@sub(<#charHEX>, {0x}, {})
	@- <#def> charHEX		@sub(<#code>, {0x}, {})
	@if(<#charHEX> = 0 ) {
		<#def> charHEX		1
		}
	@if(@len(<#charHEX>) = 1 ) {
		<#def> charHEX		{000<#charHEX>}
		}
	@if(@len(<#charHEX>) = 2 ) {
		<#def> charHEX		{00<#charHEX>}
		}
	@if(@len(<#charHEX>) = 3 ) {
		<#def> charHEX		{0<#charHEX>}
		}
	@if(@len(<#charHEX>) > 4 ) {
		<@write> error {Error: Unicode value > FFFF }
		}
	@- <#def> charHEX		{toupper(<#charHEX>)}
	<#def> charHEXu		@toupper(<#charHEX>)
}
@------------------------------------------------------------
<@include> ${CHARMACS}/mmp.map	@--- Gets the font metadata
@------------------------------------------------------------
@- <@include> ${CHARDIRS}/${FONT_DIR}/${TTX_FILE_NAME}.ttx
<@include> ${FONT_DIR}/${TTX_FILE_NAME}.ttx
<#def> fontdir	${FONT_DIR}
@------------------------------------------------------------
<#def> prev.uniheading          {}
<#def> prev.uniheading.1        {}
<#def> prev.uniheading.2        {}
@------------------------------------------------------------
<|uni_headings_array> 32|@hex2dec($charhex[<#mapTblCount>])
@------------------------------------------------------------
<@xmacro> writeCmap {
	@if(<#gotCmap> = 1 ) {
		<#def> hexChars		{}
		<#def> charDestPrefix	${FONT_FILE_NAME}
		<#def> charDestPrefix	@gsub(<#charDestPrefix>, {\.}, {_})
		<#def> charDestPrefix	@gsub(<#charDestPrefix>, {\-}, {_})
		<#ifndef> charCount	0
		<#def> fn		{	${FONT_TBL_TITLE}}
		@for(i = 1 to <#mapTblCount>) {
			<#def> charhex		$charhex[$$i]
			<#def> charDecimal	@hex2dec(<#charhex>)
			@--------------------------------------------
			@if(<#allChars> OR @len($showChar[<#charDecimal>]) > 0 ) {
				<#def> itemCount	$itemCount[$$i]
				<#def> psName		$psName[$$i]
				@--------------------------------------------
				<|adjustedHex> <#charhex>
				@if($$i = 1) {
					<#def> hexChars		{<#charHEXu><#fn>}
					}
				@else {
					<#def> hexChars		{<#hexChars><#nl><#charHEXu><#fn>}
					}
				<#def> uniDecimal	@hex2dec(<#charHEX>)
				<#def> utf8hex		@ucs2_to_utf8(<#charHEX>)
				<#def> utf8bin		@hex2bin(<#utf8hex>)
				<#def> uniName		{$uniCharText[<#uniDecimal>] }
				@--------------------------------------------
				@- Do character block text
				@--------------------------------------------
				<#ifndef> unihead.first	1
				@if(<#SYMBOL> = 0) {
				<#def> unihead.1	@sub({$UniHeadArray[<#uniDecimal>]}, {\|.*}, {})
				<#def> unihead.2	@sub({$UniHeadArray[<#uniDecimal>]}, {^.*\|}, {})
					}
				@else	{
					@-- Don't include Unicode headings if Symbol encoding
					<#def> unihead.1	{abc}
					<#def> unihead.2	{abc}
					<#def> unihead.1	{&nbsp;}
					<#def> unihead.2	{&nbsp;}
					}
				@if({<#unihead.1>} <> {<#prev.uniheading.1>}) {
		 			<#def> prev.uniheading.1        {<#unihead.1>}
					@if(<#unihead.first>) {
						<P fmt=P_fontTableTitle
						sa=6pt sb=-2pt l=-2pt p=2pt P=P
							fcolor="invisibleText"
							>
						<MasterPageRule evenPages="fontChars_Left"
							oddPages="fontChars_Right"
							/>${FONT_TBL_TITLE}<pdfMkDestName
							name="table_<#charDestPrefix>_start"
							/><MkDest fmt=B
							id="table_<#charDestPrefix>_start" />
						<#def> unihead.first	0
						}
					@else  {
						</Tbl>
						<P fmt=P_Tbl sa=12pt
							sb=-2pt l=-2pt
							fcolor="invisibleText"
							p=2pt>&nbsp;
						@- <P fmt=P_Tbl sa=12pt
						@- 	sb=-2pt l=-2pt
						@- 	fcolor="Red"
						@- 	p=12pt>&nbsp;AAAA<pdfMkDestName
						@- 	name="table_<#charDestPrefix>_end"
						@- 	/><MkDest fmt=B
						@- 	id="table_<#charDestPrefix>_end" />
						}
					<|writeBlockHeader>
				@if(<#SYMBOL> = 0) {
					<TblTitle fmt=P_fontList_Head1
						Tgap=2pt ><#unihead.1> <TblCont/><IX
						fmt=unilist ix-show=N ><#unihead.1></IX>
					}
					}
				@if({<#unihead.2>} <> {<#prev.uniheading.2>} AND <#SYMBOL> = 0 ) {
					<#def> prev.uniheading.2	{<#unihead.2>}
					@if(@len(<#unihead.2>)) {
						@--- <@write> error	{	<#unihead.2>}
						<Row wp=N wn=Y >
						<Cell cs=R fmt=P_fontList_Head2 ctm=3.5pt ><#unihead.2></Cell>
						}
					}
				<#def> unihead.first	0
				@--------------------------------------------
				@if(@len(<#uniName>) =  1 OR <#SYMBOL> = 1 ) {
					<#def> uniName		{&nbsp;    ($psName[$$i])}
					}
				<#def> Font_family	@gsub(<#Font_family>, { }, {})
				<#def> Font_family	@gsub(<#Font_family>, {-}, {})
				@--------------------------------------------
				@--- Output character data
				@--------------------------------------------
				<Row>
				@--- Unicode hex char reference
				@print({<Cell fmt="P_charNum" cbm="0.5pt"
					><#charHEXu><uniIndex uniHex="<#charHEXu>" />}, n)
					@if(<#includeDests>) {
						@if(@len($includeDest[<#charDecimal>]) > 0) {<pdfMkDestName
							name="<#charDestPrefix>_<#charHEXu>"
							/><MkDest fmt=B id="<#charDestPrefix>_<#charHEXu>" />
							}
						}
					@print({</Cell>})
				@--------------------------------------------
				<Cell fmt=P_Glyph ff=${OUTPUT_FONT}
					cbm="0.5pt"
					ctm="${GLYPH_TOPMARGIN}"
					p="${GLYPH_SIZE}"
					><#utf8bin></Cell>
				@--------------------------------------------
				<Cell fmt="P_UniCharName"
					cbm="0.5pt"
					Ca=T
					p=5.5pt A="L" li="04mm">@rtrim(<#uniName>)</Cell>
				@--------------------------------------------
				<#def> charCount	@eval(<#charCount> + 1)
				}
			}
		<@write> -n ${FONT_FILE_BASE}.chars	{<#hexChars>}
	}
}{
	<#def> showCharCount	${SHOW_CHAR_COUNT}
	@if(<#showCharCount>) {
		<Row h=1mm >
		<Row>
			<Cell fmt="P_UniCharName" cs=R >
			@print({Total characters in list: <#charCount>})
		}
	</Tbl>
	<VarDef varName="${numCharsVar}" ><#charCount></VarDef>
	<P fmt=P_Tbl sa=-8pt sb=-2pt
		l=-2pt p=4pt P=A fcolor=invisibleText>
	<MasterPageRule evenPages="Left"
		oddPages="Right" /><fontNameInIndex Range="End" /><pdfMkDestName
			name="table_<#charDestPrefix>_end"
			/><MkDest fmt=B
			id="table_<#charDestPrefix>_end" />&nbsp;
}
@------------------------------------------------------------
<@macro> writeBlockHeader {
	@if(<#gotCmap> = 1 ) {
		<Tbl fmt="T_CJKchars" ctm=1.4pt cbm=1.4pt sb=-6pt clm=0 crm=0 P=A  sa=-2pt >
		<TblColWidth W=$FList_col[01]mm />
		<TblColWidth W=$FList_col[02]mm />
		<TblColWidth W=$FList_col[03]mm />
	}
}
@------------------------------------------------------------
<@macro> writePageHeaderDELETE {
	@if(<#gotCmap> = 1 ) {
		<P fmt="P_Body" >
		<Tbl fmt="T_CJKchars" ctm=1.4pt cbm=1.4pt clm=0 crm=0 P=A >
		<TblColWidth W=$FList_col[01]mm />
		<TblColWidth W=$FList_col[02]mm />
		<TblColWidth W=$FList_col[03]mm />
	}
}
@------------------------------------------------------------
<@xmacro> writeMetaData {
	<#def> wmd.text		{Metadata description}
	<#def> wmd.data		{Metadata}
	@xgetvals(wmd.)
	<Row>
	<#def> wmd.data		@trim(<#wmd.data>)
	@ifnot(@len(X<#wmd.data>) > 1) {
		<#def> wmd.data		{<Font fa=Italic>Not included</Font>}
		}
	<Cell fw=Regular p=8 fi=0><#wmd.text>:</Cell>
	<Cell fw=Light p=8 fi=0 li=4mm ><#wmd.data></Cell>
}
@------------------------------------------------------------
<@macro> fontMetaData {
	<P fmt="P_Body" ><fontNameInIndex Range="Start" />
	<Tbl fmt="T_CJKchars" ctm=1.4pt cbm=1.4pt sb=-6pt clm=0 crm=0 P=A  sa=-2pt >
	<TblColWidth W=34mm />
	<TblColWidth W=86mm />
	@----------------------------------------------------
	<writeMetaData text="Font name" data="<Font fw=Bold><#Font_name>" />
	@----------------------------------------------------
	<writeMetaData text="Creation date" data="<#creation_date>" />
	@----------------------------------------------------
	<writeMetaData text="Modification date" data="<#modification_date>" />
	@----------------------------------------------------
	<writeMetaData text="Font type" data="<#fontDataType>" />
	@----------------------------------------------------
	<writeMetaData text="Total glyphs" data="<#glyphOrderCount>" />
	@----------------------------------------------------
	<writeMetaData text="Unicode characters" data="<#mapTblCount>" />
	@----------------------------------------------------
	<writeMetaData text="Copyright" data="<#Copyright>" />
	@----------------------------------------------------
	@- @if(<#fontdir> = {AdobeSongStd-Light}) {
	@if(@match({<#fontdir>}, {AdobeSongStd-Light})) {
		<writeMetaData text="Font family" data="<Font
			ff=AdobeSongStd fv=L fw=Regular><#Font_family></Font>" />
		}
	@else	{
		<writeMetaData text="Font family" data="<#Font_family>" />
		}
	@----------------------------------------------------
	<writeMetaData text="Font style" data="<#Font_style>" />
	@----------------------------------------------------
	<writeMetaData text="Unique ID" data="<#Font_UID>" />
	@----------------------------------------------------
	<writeMetaData text="Font version" data="<#Font_version>" />
	@----------------------------------------------------
	<writeMetaData text="PSname" data="<#PS_Name>" />
	@----------------------------------------------------
	<writeMetaData text="Trademark" data="<#Trademark>" />
	@----------------------------------------------------
	<writeMetaData text="Foundry" data="<#Font_foundry>" />
	@----------------------------------------------------
	<writeMetaData text="Designer(s)" data="<#Font_designer>" />
	@----------------------------------------------------
	@- <writeMetaData text="Notes" data="<#Font_notes>" />
	@----------------------------------------------------
	<writeMetaData text="Family name" data="<#Menu_family_name>" />
	@----------------------------------------------------
	<writeMetaData text="Menu name" data="<#Menu_style_name>" />
	@----------------------------------------------------
	<writeMetaData text="CID font name" data="<#CID_font_name>" />
	@----------------------------------------------------
	<Row wn=Y h="1.5mm" br=None >
	<Row wn=Y><Cell cs=R fw=Bold >Output CMAP table:
	<Row wn=Y>
	<Cell fw=Light >Seq. Num: <#cmapNum>
	<Cell><P fmt=P_CJKchars_RowNo fw=Light
		p=8pt ><TabStops><TabStop pos=30mm
		/><TabStop pos=66mm /></TabStops>
		@print({cmap_format_<#selectedFormat>}, n)
		@print({	Platform: <#selectedPlatform> ($platform[<#selectedPlatform>])},n)
		@print({	Chars: $cmapCharCount[<#cmapNum>]<br/>})
		@print({Encoding: <#selectedEncoding>}, n)
		@print({ ($encoding[<#selectedPlatform>, <#selectedEncoding>])})
	</P></Cell>
	<Row h=2.5mm>
	@----------------------------------------------------
	@if(<#cmapNumCount> > 1) {
		<Row wn=Y h=1.5mm br=None>
		<Row wn=Y><Cell cs=R fw=Bold >Additional CMAP tables (@eval(<#cmapNumCount> -1)):
		@for(i = 1 to <#cmapNumCount>) {
			@if($$i <>  <#cmapNum>) {
				<Row wn=Y>
				<Cell Ca=T fw=Light >Seq. Num: $$i
				<Cell><P fmt=P_CJKchars_RowNo fw=Light
					p=8pt ><TabStops><TabStop pos=30mm
					/><TabStop pos=66mm /></TabStops>
				@print({$cmapFormat[$$i]}, n)
				@print({	Platform: $platformID[$$i] ($platform[$platformID[$$i]])}, n)
				@if($cmapFormat[$$i] = 2 AND 
					$cmapCharCount[$$i] = 0 ) {
					@print({	(no count)<br/>})
					}
				@else	{
					@print({	Chars: $cmapCharCount[$$i]<br/>})
					}
		@print({Encoding: $platEncID[$$i]}, n)
		@print({ ($encoding[$platformID[$$i], $platEncID[$$i]])})
				</P></Cell>
				}
			}
		}
	<#def> incDesc		${INCLUDE_DESCRIPTION}
	@if(<#incDesc> = 1 AND @len(<#Font_notes>) > 10 ) {
		<Row h="1.5mm" >
		<Row>
		<Cell cs=R >
		<P fmt="P_fontDescription"
			fw=Regular
			fa=Regular >This font contains the following description:</P>
		<P fmt="P_fontDescription" >
		@print({"<#Font_notes>"})
		@print({</P>})
		}
	</Tbl>
}
@------------------------------------------------------------
<@macro> fontNameDataDELETE {
	<#def> fontIndexName	${FONT_INDEX_NAME}
	<P><IX fmt=Index
		ix-show=N ><#fontIndexName></IX><IX fmt=Index
		ix-show=N >font<IXsub/><#fontIndexName></IX>
	<Tbl fmt="T_metaData" wo=50 ctm=3pt cbm=1pt clm=2pt crm=2pt>
	<TblColWidth W=34mm />
	<TblColWidth W=130mm />
	@----------------------------------------------------
	<Row wn=Y h=1mm br=Thin>
	@----------------------------------------------------
	<Row>
		<Cell>Copyright<Cell><#Copyright>
	@----------------------------------------------------
	<Row>
		<Cell>Font family<Cell><#Font_family>
	@----------------------------------------------------
	<Row>
		<Cell>Font style<Cell><#Font_style>
	@----------------------------------------------------
	<Row>
		<Cell>Unique ID<Cell><#Font_UID>
	@----------------------------------------------------
	<Row>
		<Cell>Font name<Cell><#Font_name>
	@----------------------------------------------------
	<Row>
		<Cell>Font version<Cell><#Font_version>
	@----------------------------------------------------
	<Row>
		<Cell>PSname<Cell><#PS_Name>
	@----------------------------------------------------
	<Row>
	@----------------------------------------------------
		<Cell>Trademark<Cell><#Trademark>
	@----------------------------------------------------
	<Row>
		<Cell>Foundry<Cell><#Font_foundry>
	@----------------------------------------------------
	<Row>
		<Cell>Designer<Cell><#Font_designer>
	@----------------------------------------------------
	<Row>
		<Cell>Family name<Cell><#Menu_family_name>
	@----------------------------------------------------
	<Row>
		<Cell>Menu name<Cell><#Menu_style_name>
	@----------------------------------------------------
	<Row>
		<Cell>CID font name<Cell><#CID_font_name>
	@----------------------------------------------------
	</Tbl>
}
@------------------------------------------------------------
<|fontMetaData>
@------------------------------------------------------------
<writeCmap/>
@------------------------------------------------------------
