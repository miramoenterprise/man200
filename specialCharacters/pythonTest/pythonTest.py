#--------------------------------------------------------------------------------
import fontTools.ttLib
import os
import subprocess
import sys

#--------------------------------------------------------------------------------
#- Hard-wired stuff. (Should be arguments. For another day, maybe.)
#--------------------------------------------------------------------------------
textSize	= "96"
fontName	= "mmts4.ttf"
bitmapFolder	= "bitmaps"		#- Subfolder of cwd, for output PNGs
charColor1	= "#0000dd"		#- Color for most chars
charColor2	= "#dd0000"		#- Color for exception chars
charColor	= charColor1



hmtx = {
 'uni2003': (1000, 48),
 'uni2002': (500, 48),
 'uni2001': (1000, 48)
}


cmap = { 0: '.null',
 8194: 'uni2002',
 8195: 'uni2003',
 8193: 'uni2001'
}

font		= fontTools.ttLib.TTFont(fontName)
hmtx		= font['hmtx'].metrics

#--------------------------------------------------------------------------------
# hmtx = font['hmtx']
# print(hmtx)
# sys.exit()
#--------------------------------------------------------------------------------
cmap = font['cmap']
for table in cmap.tables:
    if table.format in [4]:
        if table.platformID in [3]:
            if table.platEncID in [1]:
                winUnicodeCmapDict = table.cmap
#--------------------------------------------------------------------------------

# for key, val in cmap.items():
for key, val in winUnicodeCmapDict.items():
	if val in hmtx:
		uniDec = key
		uniHex = '{:04X}'.format(uniDec)
		uniDecString = str(uniDec)
		glyphName = val
		mtxVals = hmtx[val]
		aw = mtxVals[0]
		lsb = mtxVals[1]
		#print("{}	{}	{}	{}".format(uniDec, glyphName, aw, lsb))
		print(
			"uniDec=\"{}\"	uniHex=\"{}\"	glyphName=\"{}\"  	aw=\"{}\"  	lsb=\"{}\""\
			.format(uniDec, uniHex, glyphName, aw, lsb)
			)

sys.exit()

# For each key in c, use its data value to look up a corresponding
# key in h, extract the data value from h and append it as additional
# data back in c.
# 
# Use the value 


#--------------------------------------------------------------------------------
#- Get Windows Unicode cmap, i.e.
#	format		= 4
#	platformID	= 3
#	platEncID	= 1
#--------------------------------------------------------------------------------
cmap = font['cmap']
for table in cmap.tables:
    if table.format in [4]:
        if table.platformID in [3]:
            if table.platEncID in [1]:
                winUnicodeCmapDict = table.cmap
#--------------------------------------------------------------------------------

# for char in winUnicodeCmapDict:
#     print (char, winUnicodeCmapDict[char])

#--------------------------------------------------------------------------------
from html.parser import HTMLParser
h = HTMLParser()


for char in winUnicodeCmapDict:
	uniDec = char
	# print (uniDec)

for char in winUnicodeCmapDict:
	uniHex = '{:04X}'.format(char)
	if uniHex in exceptionChars:
		charColor	= charColor2
		# print("----------------------------------")
	#------------------------------------------------------------------------
	uniHexNCER = "&#x" + uniHex + ";"
	uniUTF8 = h.unescape(uniHexNCER)
	#------------------------------------------------------------------------
	outfile = bitmapFolder + "/" + "ux" + uniHex + ".png"
	print(outfile)
	subprocess.call(
		'convert.exe -strip -background none -font %s -trim \
		-fill %s -pointsize %s label:"%s" -depth 8 %s' \
		%(fontName, charColor, textSize, uniUTF8, outfile), \
		shell=True
		)	#-- Works
	#------------------------------------------------------------------------

#--------------------------------------------------------------------------------
# END
#--------------------------------------------------------------------------------
