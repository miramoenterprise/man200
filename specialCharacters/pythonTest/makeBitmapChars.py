#--------------------------------------------------------------------------------
# -*- coding: utf-8 -*-
#
# Dunno if the above makes any difference to anything--it cropped up on
# stackoverflow somewhere ...
#
#--------------------------------------------------------------------------------
#-  makeBitmapChars.py
#--------------------------------------------------------------------------------
# Convert every character listed in .ttf font's Windows Unicode <cmap> table
# to PNG format suitable for use in CBDT/CBLC fonts.
# 
# The ouput PNG files have the following naming convention:
#
#	'uxHHHH.png'
#
#	where HHHH is the unicode address in hexadecimal.
#
# This script works. There's nothing else going for it. It has a lot of crud, 
# stuff left over from the poke and play development.
# 
# The bulk of the work is done by:
#
#	- FontTools
#		Version: 3.17.1.dev0
#		https://pypi.python.org/pypi/FontTools
#
#	- ImageMagick 'convert' command
#		ImageMagick Version: 6.8.6-8 2013-08-04 Q16
#		(http://www.imagemagick.org)
#
#--------------------------------------------------------------------------------
#
# Background
# ----------
#
# A CBDT/CBLC font is needed to display GUI special text symbols, e.g. typographic
# spaces, hard return, LRO, etc. The CBDT/CBLC font format is supported in Qt on
# a cross-platform basis, independent of OS resources. CPAL needs OS resources.
# The CBDT/CBLC font format is not the coolest, but it is fine for the simple
# glyph shapes required for special text symbols that are only displayed in the
# GUI and never printed.
# 
# CBDT	= Color Bitmap Data Table
#		https://www.microsoft.com/typography/otspec/cbdt.htm
# CBLC	= Color Bitmap Location Table 
#		https://www.microsoft.com/typography/otspec/cblc.htm
# 
# Metrics info is shown here:
# EBLC	= Embedded Bitmap Location Table
#		https://www.microsoft.com/typography/otspec/eblc.htm
# 
# Background discussions here:
# https://github.com/fontforge/fontforge/issues/2044
# https://github.com/fontforge/fontforge/issues/677
# 
# 
# Next Steps
# ----------
#
# Need to incorporate the logic and function of 'emoji_builder.py'
# in the following script, while fixing its limitations ...
#
# This will probably need dictionary joins, htmx, cmap & glyf perhaps,
# and other esoterics ...
#
#--------------------------------------------------------------------------------
# This script was tested with Python 3.6 on Windows Server 2008 R2
#--------------------------------------------------------------------------------
import fontTools.ttLib
import os
import subprocess
import json

#--------------------------------------------------------------------------------
#- Hard-wired stuff. (Should be arguments. For another day, maybe.)
#--------------------------------------------------------------------------------
textSize	= "96"
fontName	= "mmts4.ttf"
bitmapFolder	= "bitmaps"		#- Subfolder of cwd, for output PNGs
charColor1	= "#0000dd"		#- Color for most chars
charColor2	= "#dd0000"		#- Color for exception chars
charColor	= charColor1

#--------------------------------------------------------------------------------
#- Skip the chars listed below.
#- (e.g. the char is non-printing and/or has no glyph outline)
#--------------------------------------------------------------------------------
skipList	= ('--------------',
			'001D',
		   '--------------')

#--------------------------------------------------------------------------------
#- List of chars for 'charColor2'
# 
#- Currently random. Later the list will be updated so that control characters
#- have a different color to most other (typographic) special characters.
#--------------------------------------------------------------------------------
exceptionChars	= ('--------------',
			'2003',
			'2004',
		   '--------------')
#--------------------------------------------------------------------------------
font 		= fontTools.ttLib.TTFont(fontName)
hmtx 		= font['hmtx'].metrics
#--------------------------------------------------------------------------------
#- The above returns a dict w/ following structure
#  It's not needed in this script. It's only here so I can find it when it *is*
#  needed for making the font.
#--------------------------------------------------------------------------------
# {
# 	'.notdef': (605, 47),
# 	'.null': (0, 0),
# 	'nonmarkingreturn': (293, 0),
# 	'space': (278, 81),
# 	'uni00A0': (278, 25),
# 	...
# }
#--------------------------------------------------------------------------------
# print (hmtx)
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#- Get Windows Unicode cmap, i.e.
#	format		= 4
#	platformID	= 3
#	platEncID	= 1
#--------------------------------------------------------------------------------
cmap = font['cmap']
for table in cmap.tables:
	if table.format in [4]:
		if table.platformID in [3]:
			if table.platEncID in [1]:
				winUnicodeCmapDict = table.cmap
#--------------------------------------------------------------------------------

# for char in winUnicodeCmapDict:
#     print (char, winUnicodeCmapDict[char])

#--------------------------------------------------------------------------------
from html.parser import HTMLParser
h = HTMLParser()
#--------------------------------------------------------------------------------


for char in winUnicodeCmapDict:
	uniDec = char
	# print (uniDec)

for char in winUnicodeCmapDict:
	uniHex = '{:04X}'.format(char)
	if uniHex in skipList:
		print("------> Skipping char: {}	<-----".format(uniHex))
	else:
		if uniHex in exceptionChars:
			charColor	= charColor2
			# print("----------------------------------")
		#----------------------------------------------------------------
		#- The following is crazy, but it's the only way I can find that
		#- seems to guarantee output in binary UTF-8, required for the
		#- 'convert' label parameter below.
		#- 
		#- (NCER is 'numeric character entity referece'.)
		#----------------------------------------------------------------
		uniHexNCER = "&#x" + uniHex + ";"
		uniUTF8 = h.unescape(uniHexNCER)
		#----------------------------------------------------------------
		outfile = bitmapFolder + "/" + "ux" + uniHex + ".png"
		print(outfile)
		subprocess.call(
			'convert.exe -strip -background none -font %s -trim \
			-fill %s -pointsize %s label:"%s" -depth 8 %s' \
			%(fontName, charColor, textSize, uniUTF8, outfile), \
			shell=True
			)	#-- Works
		#----------------------------------------------------------------

#--------------------------------------------------------------------------------
# END
#--------------------------------------------------------------------------------
