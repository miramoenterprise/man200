<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:output method="xml" indent="yes" />
<xsl:template match="/samples">
<viewArea  width="600" height="400" >
  <chartArea>
    <text x="300" y="380" >X Axis Label</text>
    <text x="20" y="180" rotate="-90" >Y Axis Label</text>
    <lineChart x="70" y="20" width="500" height="300" >
      <xAxis leftValue="0" rightValue="100">
        <grid stepValue="10" length="300" y="-20" numberFormat=".0"></grid>
      </xAxis>
      <yAxis topValue="100" bottomValue="0">
        <grid stepValue="10" length="500" x="-20" y="-5" numberFormat="0"></grid>
      </yAxis>
      <dataSet>
	<xsl:apply-templates select="sample" />
      </dataSet>
     </lineChart>
  </chartArea>
</viewArea>
</xsl:template>
<xsl:template match="sample">
  <point xValue="{x}" yValue="{y}" />
</xsl:template>
</xsl:stylesheet>