<?xml version="1.0" encoding="UTF-8"?>
<!--
  Mapping XSLT Stylesheet to map UN data to CharML.
  
  @LastModified 2008-11-08

    (c) 2007-2008 Toru Saito 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" indent="yes" />
<xsl:template match="/ROOT/data">
<viewArea  width="600" height="430" >
  <text x="300" y="30" class="title">Consumer Price Index</text>
  <chartArea y="30">
    <text x="300" y="380" class="legend" >Year</text>
    <text x="20" y="180" rotate="-90" class="legend">Consumer prices index, 2000 = 100 (IMF)</text>
    <lineChart x="70" y="20" width="500" height="300" class="chart">
      <xAxis leftValue="0" rightValue="70">
        <grid stepValue="10" length="300" y="-20" numberOffset="1940" numberFormat="0" size="2"></grid>
      </xAxis>
      <yAxis topValue="140" bottomValue="0">
        <grid stepValue="20" length="500" x="-20" y="-5" numberOffset="0" numberFormat="0"></grid>
      </yAxis>
      <dataSet id="Japan" class="unChart">
		<xsl:apply-templates select="record[field = 'Japan']" >
			<xsl:sort select="field[@name = 'Year']" data-type="number"/>
		</xsl:apply-templates>
      </dataSet>
	  <dataSet id="UnitedStates" class="unChart">
		<xsl:apply-templates select="record[field = 'United States']" >
		  <xsl:sort select="field[@name = 'Year']" data-type="number"/>
		</xsl:apply-templates>
      </dataSet>
	      <dataSet id="Australia" class="unChart">
	<xsl:apply-templates select="record[field = 'Australia']" >
	  <xsl:sort select="field[@name = 'Year']" data-type="number"/>
	</xsl:apply-templates>
      </dataSet>
	      <dataSet id="France" class="unChart">
	<xsl:apply-templates select="record[field = 'France']" >
	  <xsl:sort select="field[@name = 'Year']" data-type="number"/>
	</xsl:apply-templates>
      </dataSet>
	      <dataSet id="Canada" class="unChart">
	<xsl:apply-templates select="record[field = 'Canada']" >
	  <xsl:sort select="field[@name = 'Year']" data-type="number"/>
	</xsl:apply-templates>
      </dataSet>
	      <dataSet id="India" class="unChart">
	<xsl:apply-templates select="record[field = 'India']" >
	  <xsl:sort select="field[@name = 'Year']" data-type="number"/>
	</xsl:apply-templates>
      </dataSet>
     </lineChart>
  </chartArea>
</viewArea>
</xsl:template>
<xsl:template match="record">
<point>
  <xsl:attribute name="xValue">
    <xsl:apply-templates select="field[@name = 'Year']"  mode="year"/> 
  </xsl:attribute>
  <xsl:attribute name="yValue">
    <xsl:apply-templates select="field[@name = 'Value']"  mode="value"/>
  </xsl:attribute>  
  </point>
</xsl:template>
<xsl:template match="field" mode="year">
<xsl:value-of select="number(.) - 1940" />
</xsl:template>
<xsl:template match="field" mode="value">
<xsl:value-of select="." />
</xsl:template>
<xsl:template match="footnote"/>
</xsl:stylesheet>
