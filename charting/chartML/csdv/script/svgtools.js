/**
  svgtools.js
  Creates a svg document from a xml file using two XSLT files.
  @NOTE this class uses prototype.js library

    (c) 2007-2008 Toru Saito 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  @created 2007-10-16
*/
/* ts namespace */
if(typeof(ts)=='undefined'){
  ts = new Object();
}

/**
  SvgGenerator class 
*/
// Constructor
ts.SvgGenerator = function ( strSrcXml, strChartXslt, handler ){
  this.xsltProcessor = new XSLTProcessor();

  this.handler = handler;
  this.objSrcXml = null;
  this.objChartXslt = null;
  this.objSvgXslt = null;
  this.objChartXml = null;
  this.objSvg = null;
  
  this.loadXml(strSrcXml, 'objSrcXml');
  this.loadXml(strChartXslt, 'objChartXslt');
  this.loadXml('xsl/c2s.xsl', 'objSvgXslt');
}
// Methods
ts.SvgGenerator.prototype = {
  setSrcXml: function(strUrl){
    this.loadXml(strUrl, 'objSrcXml');
  },
  setChartXslt: function(strUrl){
	this.loadXml(strUrl, 'objChartXslt');
  },
  loadXml: function( strUrl, strObj){
    if( strUrl == null) return;
    var myself = this;
    myself[strObj] = null; 
    new Ajax.Request( strUrl,
      { method: 'get',
        onSuccess: function(transport){
          myself[strObj] = transport.responseXML;
          myself.doTransform();
        },
        onFailure: function(){ alert('Error while getting XML file fom ' +   strObj);
        }
      }
    );
  },
  doTransform: function(){
    if (this.objSrcXml == null || this.objChartXslt == null || this.objSvgXslt == null) return;
    
    if (window.ActiveXObject){
      alert("Sorry, the application currently doesn't support Internet Explorer. Please use FireFox..");
    } else {
      // 1st Transformation  XML -> ChartXML
      var processor1 = new XSLTProcessor();
      processor1.importStylesheet(this.objChartXslt);
      this.objChartXml = processor1.transformToDocument(this.objSrcXml);
      var temp = new XMLSerializer().serializeToString(this.objChartXml);
      //      alert(temp);
      
      // 2nd Transformation ChartXMl -> SVG
      var processor = new XSLTProcessor();
      processor.importStylesheet(this.objSvgXslt);
      //      this.objSvg = processor.transformToFragment(this.objSrcXml, document);
      this.objSvg = processor.transformToFragment(this.objChartXml, document);
      
      //      alert( new XMLSerializer().serializeToString(this.objSvg));
      //      var xmlDoc =( new XMLSerializer().serializeToString(this.objResult));


      if( typeof(this.handler)=='function'){
          this.handler(this.objSvg);
      } else{        
        var element = $(this.handler);
        while (element.firstChild) {
          element.removeChild(element.firstChild);
        }
        element.appendChild(this.objSvg);
      }
    }
  }
}