/**
  resizable.js

  Provide an event handeling function for resizing.

  @NOTE this class uses prototype.js library

    (c) 2007-2008 Toru Saito 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// ts namespace
if(typeof(ts)=='undefined') {
  ts = new Object(); 
}
ts.Resizable = Class.create();
ts.Resizable.prototype = {
  initialize: function(name, handler){
    this.handler = handler;
    this.isDragging = false;

    // resizable element
    this.name = name; 
    this.elm = $(name);

    // Create a wrapper around the resizable element
    this.wrapper = $(document.createElement("div"));
    this.wrapper.setStyle({
      position: 'relative',
      width: this.elm.style.width,
      height: this.elm.style.height,
      border: '2px solid #999'
    });

    this.elm = this.elm.replace(this.wrapper);
    this.wrapper.appendChild(this.elm);

    // Create a handler at the lower left corner
    this.handle = document.createElement("div");
    $(this.handle).setStyle({
      background: 'url(script/handle.gif)',
      position: 'absolute',
      right: 0,
      bottom: 0,
      width: '16px',
      height: '16px',
      zIndex: 99999
    }); 
    this.wrapper.appendChild(this.handle);
    Event.observe(this.handle, 'mousedown', this.dragStart.bindAsEventListener(this));
    Event.observe(this.handle, 'mouseover', this.changeCursor.bindAsEventListener(this));
    Event.observe(this.handle, 'mouseout', this.restoreCursor.bindAsEventListener(this));
  },
  dragStart: function(){
    window.status = "dragging....";
    this.isDragging = true;
    document.body.style.cursor = 'SE-resize';

    this.left = this.wrapper.offsetLeft;
    this.top = this.wrapper.offsetTop;

    this.wrapper.setStyle({
      opacity: 0.3,
      backgroundColor: '#999',
    });


    // Unregister Dragging handler
    // When dragging go outside of window (web browser) scope, thoese event handlers 
    // won't be unregistered. So, they need to be unregistered here, just in case.
    Event.stopObserving(window, 'mouseup', this.dragEndHandler);
    Event.stopObserving(window, 'mousemove', this.draggingHandler);

    // Need to cache hadelers so that later unregistered
    this.dragEndHandler = this.dragEnd.bindAsEventListener(this);
    this.draggingHandler = this.dragging.bindAsEventListener(this);

    // Register Event Handler for draggin
    Event.observe(window, 'mouseup', this.dragEndHandler);
    Event.observe(window, 'mousemove', this.draggingHandler);
  },
  dragging: function(e){
    this.wrapper.setStyle( {
	width:  (Event.pointerX(e) - this.left)+'px' ,
	height: (Event.pointerY(e) - this.top)+'px',
    });
  },
  dragEnd: function(e){
    window.status = "dragged";
    this.isDragging = false;
    var w = Event.pointerX(e)-this.left;
    var h = Event.pointerY(e)-this.top
    document.body.style.cursor = 'default';
 

    this.wrapper.setStyle({
      backgroundColor: 'transparent',
      opacity: 1
    });

    this.elm.setStyle({
      width: w + 'px',
      height: h + 'px'
    }); 

    // Unregister Dragging handler
    Event.stopObserving(window, 'mouseup', this.dragEndHandler);
    Event.stopObserving(window, 'mousemove', this.draggingHandler);

    if(this.handler) {
      this.handler(w, h);
    }
  },
  changeCursor: function(e){
      this.handle.style.cursor = 'SE-resize';
  },
  restoreCursor: function(e){
    if(! this.isDragging ){
      this.handle.style.cursor = 'default';
    }
  }
}
