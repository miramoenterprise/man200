<?xml version="1.0"?>
<!--
  Transfer from SChartXML to SVG

  @LastModified 2008-10-10

    (c) 2007-2008 Toru Saito 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:svg="http://www.w3.org/2000/svg" 
		xmlns:xlink="http://www.w3.org/1999/xlink">
<xsl:output method="xml" indent="yes"/>
<!-- 
==============================
    Root 
==============================
-->
<!-- Template for Root -->
<xsl:template match="chartML">
  <xsl:processing-instruction name="xml-stylesheet">href="css/chartml.css" type="text/css"</xsl:processing-instruction>
  <xsl:apply-templates select="viewArea" />
</xsl:template>

<!-- 
==============================
    View Area 
==============================
-->
<xsl:template match="viewArea">
  <xsl:variable name="x">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@x"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@y"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="width">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@width"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="height">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@height"/></xsl:call-template>
  </xsl:variable>

  <svg:svg width="{@width}" height="{@height}" id="SvgChart" viewBox="{$x} {$y} {@width} {@height}">
    <!-- Default Style -->
    <svg:style>
      text{
        font-size: 12px;
        font-family: Arial, Geneva;
        text-anchor: middle;
        fill: #000;
      }
      rect{
        fill: #fff;
        fill-opacity: 0;
      }
      line{
        stroke: black;
        stroke-width: 1;
        stroke-opacity: 1;
      }
      circle{
        stroke: black;
        fill: black;
      }
    </svg:style>
    <!-- Background -->
    <xsl:element name="svg:rect">
    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:attribute name="x"><xsl:value-of select="$x"/></xsl:attribute>
    <xsl:attribute name="y"><xsl:value-of select="$y"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />
    </xsl:element>

    <!-- Content -->
    <xsl:element name="svg:g">
      <xsl:apply-templates select="@class|@id" />
      <!-- Elements -->
      <xsl:apply-templates select="text"/>
      <xsl:apply-templates select="image" />

      <xsl:apply-templates select="chartArea" >
	<xsl:with-param name="pWidth" select="$width"/>
	<xsl:with-param name="pHeight" select="$height"/>
      </xsl:apply-templates>
    </xsl:element>
  </svg:svg>
</xsl:template>
<!-- 
==============================
    Chart Area
==============================
-->
<xsl:template match="chartArea">
  <xsl:param name="pWidth"/>
  <xsl:param name="pHeight"/>

  <!-- Set default values -->
  <xsl:variable name="x">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@x"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@y"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="width">
    <xsl:call-template name="defaultValue">
      <xsl:with-param name="value" select="@width"/>
      <xsl:with-param name="default" select="$pWidth"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="height">
    <xsl:call-template name="defaultValue">
      <xsl:with-param name="value" select="@height"/>
      <xsl:with-param name="default" select="$pHeight"/>
    </xsl:call-template>
  </xsl:variable>

  <!-- Background -->
  <xsl:element name="svg:rect">
    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:attribute name="x"><xsl:value-of select="$x"/></xsl:attribute>
    <xsl:attribute name="y"><xsl:value-of select="$y"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />
  </xsl:element>

  <!-- Content -->
  <xsl:element name="svg:g">
    <xsl:attribute name="transform">
      translate(<xsl:value-of select="$x"/>,<xsl:value-of select="$y"/>)
    </xsl:attribute>
    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />

    <xsl:apply-templates select="text" />
    <xsl:apply-templates select="image" />

    <!-- Elements -->
    <xsl:apply-templates select="lineChart|scatterChart|columnChart|barChart">
        <xsl:with-param name="pWidth"><xsl:value-of select="$width"/></xsl:with-param>
        <xsl:with-param name="pHeight"><xsl:value-of select="$height"/></xsl:with-param>
    </xsl:apply-templates>
 </xsl:element>
</xsl:template>
<!-- 
==============================
    Plot Area - Line Chart
==============================
-->
<xsl:template match="lineChart|scatterChart|columnChart|barChart">
  <xsl:param name="pWidth"/>
  <xsl:param name="pHeight"/>

  <!-- Set default values -->
  <xsl:variable name="x">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@x"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@y"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="width">
    <xsl:call-template name="defaultValue">
      <xsl:with-param name="value" select="@width"/>
      <xsl:with-param name="default" select="$pWidth"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="height">
    <xsl:call-template name="defaultValue">
      <xsl:with-param name="value" select="@height"/>
      <xsl:with-param name="default" select="$pHeight"/>
    </xsl:call-template>
  </xsl:variable>
  
  <xsl:variable name="xMinValue" select="xAxis/@leftValue" />
  <xsl:variable name="xMaxValue" select="xAxis/@rightValue" />
  <xsl:variable name="yMinValue" select="yAxis/@bottomValue" />
  <xsl:variable name="yMaxValue" select="yAxis/@topValue" />
  <xsl:variable name="xScale" select="$width div ($xMaxValue - $xMinValue)" />
  <xsl:variable name="yScale" select="$height div ($yMaxValue - $yMinValue)" />

<!--
  @Debug: width = <xsl:value-of select="$width"/>
  @Debug: xMaxValue = <xsl:value-of select="$xMaxValue"/>
  @Debug: xMinValue = <xsl:value-of select="$xMinValue"/>
  @Debug: xScale = <xsl:value-of select="$xScale" />
-->

  <!-- Background -->
  <xsl:element name="svg:rect">
    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:attribute name="x"><xsl:value-of select="$x"/></xsl:attribute>
    <xsl:attribute name="y"><xsl:value-of select="$y"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />
  </xsl:element>
  
  <!-- Main Content -->
  <!-- Axes -->
  <xsl:element name="svg:g">
    <xsl:attribute name="transform">
      translate(<xsl:value-of select="$x - ($xMinValue * $xScale)"/>,
      <xsl:value-of select="$height + $y + ($yMinValue * $yScale)"/>)
      scale(1,-1)    
    </xsl:attribute>
    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />

    <xsl:apply-templates select="xAxis">
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />
    </xsl:apply-templates>
    <xsl:apply-templates select="yAxis">
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />
    </xsl:apply-templates>  
  </xsl:element>

  <!-- Draw Data -->
  <!-- Create a clipping area -->
  <xsl:variable name="plotID" select="generate-id()" />

  <xsl:element name="svg:clipPath">
    <xsl:attribute name="id"><xsl:value-of select="$plotID"/></xsl:attribute>
    <xsl:element name="svg:rect">
      <xsl:attribute name="x"><xsl:value-of select="$xMinValue * $xScale"/></xsl:attribute>
      <xsl:attribute name="y"><xsl:value-of select="$yMinValue * $yScale" /></xsl:attribute>
      <xsl:attribute name="width"><xsl:value-of select="$width" /></xsl:attribute>
      <xsl:attribute name="height"><xsl:value-of select="$height" /></xsl:attribute>
    </xsl:element>
  </xsl:element>
  <xsl:element name="svg:g">
    <xsl:attribute name="transform">
      translate(<xsl:value-of select="$x - ($xMinValue * $xScale)"/>,
      <xsl:value-of select="$height + $y + ($yMinValue * $yScale)"/>)
      scale(1,-1)    
    </xsl:attribute>
    <xsl:attribute name="clip-path">url(#<xsl:value-of select="$plotID"/>)</xsl:attribute>

    <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
    <xsl:apply-templates select="@class|@id" />
  
    <!-- Data Set -->
    <xsl:apply-templates select="dataSet">
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />
    </xsl:apply-templates>
  </xsl:element>

  
</xsl:template>
<!-- 
==============================
    Plot Area > X Axis
==============================
-->
<xsl:template match="xAxis">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />  
 
    <xsl:element name="svg:g">
    <xsl:apply-templates select="@class|@id" />

    <!-- Draw the main axis line -->
    <xsl:call-template name="drawLine" >
      <xsl:with-param name="x1" select="@leftValue * $xScale"/>
      <xsl:with-param name="x2" select="@rightValue * $xScale"/>
      <xsl:with-param name="y1" select="0" />
      <xsl:with-param name="y2" select="0" />      
    </xsl:call-template>

    <!-- Draw the grid line(s) --> 
    <xsl:apply-templates select="grid">
      <xsl:with-param name="gridType" select="'x'" />
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />

      <!--
       There is a chart whose value decrease as the axis goes right.
	   The following takes care of the situation. 
      -->
      <xsl:with-param name="min" select="@leftValue * (@leftValue &lt; @rightValue) + @rightValue * (@leftValue >= @rightValue)" />
      <xsl:with-param name="max" select="@rightValue * (@leftValue &lt; @rightValue) + @leftValue * (@leftValue >= @rightValue)" />
    </xsl:apply-templates> 
  </xsl:element>
</xsl:template>
<!-- 
==============================
    Plot Area > Y Axis
==============================
-->
<xsl:template match="yAxis">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />  
  
    <xsl:element name="svg:g">
    <xsl:apply-templates select="@class|@id" />

    <!-- Draw the main axis line -->
    <xsl:call-template name="drawLine" >
      <xsl:with-param name="x1" select="0"/>
      <xsl:with-param name="x2" select="0"/>
      <xsl:with-param name="y1" select="@bottomValue * $yScale" />
      <xsl:with-param name="y2" select="@topValue * $yScale" />      
    </xsl:call-template>


    <!-- Draw the grid line(s) --> 
    <xsl:apply-templates select="grid">
      <xsl:with-param name="gridType" select="'y'" />
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />

      <!--
       There is a chart whose value decrease as the axis goes right.
	   The following takes care of the situation. 
      -->
      <xsl:with-param name="min" select="@bottomValue * (@bottomValue &lt; @topValue) + @topValue * (@bottomValue >= @topValue)" />
      <xsl:with-param name="max" select="@topValue * (@bottomValue &lt; @topValue) + @bottomValue * (@bottomValue >= @topValue)" />
    </xsl:apply-templates> 

  </xsl:element>
</xsl:template>
<!-- 
==============================
    Plot Area > Axis > Grid (Starting Point)
==============================
-->
<xsl:template match="grid">
  <xsl:param name="gridType" />
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <xsl:param name="min" />
  <xsl:param name="max" />

  <!-- set default value -->
  <xsl:variable name="x">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@x"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@y"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="numberOffset">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@numberOffset"/>
      </xsl:call-template>
  </xsl:variable>
  
  <xsl:comment>gridType = <xsl:value-of select="$gridType"/></xsl:comment>

  <xsl:element name="svg:g">
    <xsl:apply-templates select="@class|@id" />

    <xsl:call-template name="drawGrid">
      <xsl:with-param name="gridType" select="$gridType" />
      <xsl:with-param name="x" select="$x" />
      <xsl:with-param name="y" select="$y" />
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale"/>
      <xsl:with-param name="min" select="$min"/>
      <xsl:with-param name="max" select="$max"/>
      <xsl:with-param name="stepValue" select="@stepValue"/>
      <xsl:with-param name="length" select="@length" />
      <xsl:with-param name="numberFormat" select="@numberFormat" />
      <xsl:with-param name="numberOffset" select="$numberOffset" />
      <xsl:with-param name="current" select="$min"/>

    </xsl:call-template>
  </xsl:element>

</xsl:template>
<!-- 
==============================
    Plot Area > Axis > Grid (Recursive Item)
==============================
-->
<xsl:template name="drawGrid">
  <xsl:param name="gridType" />
  <xsl:param name="x" />
  <xsl:param name="y" />
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <xsl:param name="min" />
  <xsl:param name="max" />
  <xsl:param name="stepValue" />
  <xsl:param name="length" />
  <xsl:param name="numberFormat" />
  <xsl:param name="numberOffset" />
  <xsl:param name="current" />

  <xsl:comment>gridType = <xsl:value-of select="$gridType"/></xsl:comment>
  <xsl:if test="$current &lt;= $max">
    <!--
         Draw Tick Line
    -->
    <xsl:choose>
      <xsl:when test="($gridType = 'x')">
	<xsl:call-template name="drawLine">
	  <xsl:with-param name="x1" select="$current * $xScale" />
	  <xsl:with-param name="y1" select="$length" />
	  <xsl:with-param name="x2" select="$current * $xScale" />
	  <xsl:with-param name="y2" select="0" />
	</xsl:call-template>

	<!--  Draw Label for X Axis -->
	<xsl:if test="$numberFormat">
	  <xsl:variable name="xValue" select="$current * $xScale + $x"/>
	  <svg:text x="{$xValue}"
		    y="{$y}"
		    transform="translate({$xValue},{$y}) scale(1,-1) translate({-$xValue},{-$y})" >
	    <xsl:value-of select="format-number($current + $numberOffset, $numberFormat)" />
	  </svg:text>
	</xsl:if>

      </xsl:when>
      <xsl:when test="$gridType = 'y'">
	<xsl:call-template name="drawLine">
	  <xsl:with-param name="y1" select="$current * $yScale" />
	  <xsl:with-param name="x1" select="$length" />
	  <xsl:with-param name="y2" select="$current * $yScale" />
	  <xsl:with-param name="x2" select="0" />
	</xsl:call-template>

	<!--  Draw Label for Y Axis -->
	<xsl:if test="$numberFormat">
	  <xsl:variable name="yValue" select="$current * $yScale + $y"/>
	  <svg:text x="{$x}"
		    y="{$yValue}"
		    transform="translate({$x},{$yValue}) scale(1,-1) translate({-$x},{-$yValue})" >
	    <xsl:value-of select="format-number($current + $numberOffset, $numberFormat)" />
	  </svg:text>
	</xsl:if>

      </xsl:when>
    </xsl:choose>      
    <!-- 
	 Recursively call the drawGrid template
    -->
    <xsl:call-template name="drawGrid">
      <xsl:with-param name="gridType" select="$gridType" />
      <xsl:with-param name="x" select="$x" />
      <xsl:with-param name="y" select="$y" />
      <xsl:with-param name="mix" select="$min"/>
      <xsl:with-param name="max" select="$max"/>
      <xsl:with-param name="stepValue" select="$stepValue"/>
      <xsl:with-param name="xScale" select="$xScale" />
      <xsl:with-param name="yScale" select="$yScale" />
      <xsl:with-param name="length" select="$length" />
      <xsl:with-param name="numberFormat" select="$numberFormat" />
      <xsl:with-param name="numberOffset" select="$numberOffset" />
      <xsl:with-param name="current" select="$current + $stepValue"/>
    </xsl:call-template>
  </xsl:if> 
</xsl:template>
<!-- 
==============================
    Plot Area > Data Set
==============================
-->
<xsl:template match="dataSet">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />

  <!-- Get the chart type -->
  <xsl:variable name="chartType" select="name(parent::*)" />

  <!-- Set default values -->
  <xsl:variable name="x">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@x"/>
      </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0">
	<xsl:with-param name="value" select="@y"/>
      </xsl:call-template>
  </xsl:variable>

  <xsl:element name="svg:g">
    <xsl:attribute name="transform">
      translate(<xsl:value-of select="$x"/>,<xsl:value-of select="$y"/>)
    </xsl:attribute>

    <xsl:apply-templates select="@class|@id"/>
    <xsl:choose>
      <xsl:when test="$chartType = 'lineChart'">
	<xsl:apply-templates select="point" mode="lineChart">
	  <xsl:with-param name="xScale" select="$xScale" />
	  <xsl:with-param name="yScale" select="$yScale" />
	</xsl:apply-templates>
      </xsl:when>
      <xsl:when test="$chartType = 'scatterChart'">
	<xsl:apply-templates select="point" mode="scatterChart">
	  <xsl:with-param name="xScale" select="$xScale" />
	  <xsl:with-param name="yScale" select="$yScale" />
	</xsl:apply-templates>
      </xsl:when>
      <xsl:when test="$chartType = 'columnChart'">
	<xsl:apply-templates select="point" mode="columnChart">
	  <xsl:with-param name="xScale" select="$xScale" />
	  <xsl:with-param name="yScale" select="$yScale" />
	</xsl:apply-templates>
      </xsl:when>
      <xsl:when test="$chartType = 'barChart'">
	<xsl:apply-templates select="point" mode="barChart">
	  <xsl:with-param name="xScale" select="$xScale" />
	  <xsl:with-param name="yScale" select="$yScale" />
	</xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:element>
</xsl:template>
<!-- 
     Line Chart 
-->
<!--  First point in the chart. -->
<xsl:template match="point[1]" mode="lineChart" >
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <xsl:if test="@size">
    <svg:circle cx="{@xValue * $xScale}" cy="{@yValue * $yScale}" r="{@size}" />
  </xsl:if>
</xsl:template>
<!-- All the points except the first in the chart. -->
<xsl:template match="point" mode="lineChart">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />

<!--
     @Debug(point): xScale = <xsl:value-of select="$xScale"/>
     @Debug(point): preceding-sibling::point[1]/@xValue = <xsl:value-of select="preceding-sibling::point[1]/@xValue"/>
-->

  <!-- 
       Draw a line 
  -->
  <xsl:call-template name="drawLine" >
    <xsl:with-param name="x1" select="preceding-sibling::point[1]/@xValue * $xScale" />
    <xsl:with-param name="y1" select="preceding-sibling::point[1]/@yValue * $yScale" />
    <xsl:with-param name="x2" select="@xValue * $xScale"/>
    <xsl:with-param name="y2" select="@yValue * $yScale"/>
    <xsl:with-param name="class" select="@class|@id" />
  </xsl:call-template>
  <!-- Draw a point -->
  <xsl:if test="@size">
    <svg:circle cx="{@xValue * $xScale}" cy="{@yValue * $yScale}" r="{@size}" />
  </xsl:if>
</xsl:template>
<!-- 
     Scatter Chart 
-->
<!-- All the points in the chart. -->
<xsl:template match="point" mode="scatterChart">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <!-- Draw a point -->
  <xsl:if test="@size">
    <svg:circle cx="{@xValue * $xScale}" cy="{@yValue * $yScale}" r="{@size}" />
  </xsl:if>
</xsl:template>
<!--
    Column Chart
-->
<xsl:template match="point" mode="columnChart">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <!-- Draw a point -->
  <xsl:if test="@size">
    <xsl:choose>
      <xsl:when test="@yValue > 0">
	<svg:rect x="{@xValue * $xScale}" y="0" width="{@size}" height="{@yValue * $yScale}" />
      </xsl:when>
      <xsl:otherwise>
	<svg:rect x="{@xValue * $xScale}" y="{@yValue * $yScale}" width="{@size}" height="{@yValue * $yScale * (-1)}" />
      </xsl:otherwise>
    </xsl:choose>
</xsl:if>
</xsl:template>
<!--
    Bar Chart
-->
<xsl:template match="point" mode="barChart">
  <xsl:param name="xScale" />
  <xsl:param name="yScale" />
  <!-- Draw a point -->
  <xsl:if test="@size">
    <xsl:choose>
      <xsl:when test="@xValue > 0">
	<svg:rect x="0" y="{@yValue * $yScale}" width="{@xValue * $xScale}" height="{@size}" />
      </xsl:when>
      <xsl:otherwise>
	<svg:rect x="{@xValue * $xScale}" y="{@yValue * $yScale}" width="{@xValue * $xScale * (-1)}" height="{@size}" />
     </xsl:otherwise>
    </xsl:choose>
</xsl:if>
</xsl:template>

<!-- 
==============================
    Text
==============================
-->
<xsl:template match="text">
  <xsl:variable name="x">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@x"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@y"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="rotate">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@rotate"/></xsl:call-template>
  </xsl:variable>

  <xsl:element name="svg:text">
    <xsl:attribute name="x"><xsl:value-of select="$x"/></xsl:attribute>
    <xsl:attribute name="y"><xsl:value-of select="$y"/></xsl:attribute>
    <xsl:attribute name="transform">rotate(<xsl:value-of select="$rotate"/>,
                                         <xsl:value-of select="$x" />,
                                         <xsl:value-of select="$y" />)
    </xsl:attribute>

    <xsl:apply-templates select="@class|@id" />

    <xsl:value-of select="." />
  </xsl:element>
</xsl:template>
<!-- 
==============================
    Image
==============================
-->
<xsl:template match="image">
  <xsl:variable name="x">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@x"/></xsl:call-template>
  </xsl:variable>
  <xsl:variable name="y">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@y"/></xsl:call-template>
  </xsl:variable>

  <xsl:element name="svg:image">
    <xsl:attribute name="x"><xsl:value-of select="$x"/></xsl:attribute>
    <xsl:attribute name="y"><xsl:value-of select="$y"/></xsl:attribute>
    <xsl:attribute name="width"><xsl:value-of select="@width" /></xsl:attribute>
    <xsl:attribute name="height"><xsl:value-of select="@height" /></xsl:attribute>
    <xsl:attribute name="xlink:href"><xsl:value-of select="@src"/></xsl:attribute>

    <xsl:apply-templates select="@class|@id" />

    <xsl:value-of select="." />
  </xsl:element>
</xsl:template>


<!-- Utility Function -->
<!-- 
  Named Template: drawLine
    @param: x1
    @param: x2
    @param: y1
    @param: y2
    @param: class 
-->
<xsl:template name="drawLine" >
  <xsl:param name="x1" />
  <xsl:param name="x2" />
  <xsl:param name="y1" />
  <xsl:param name="y2" />
  <xsl:param name="class" />

  <xsl:element name="svg:line">
    <xsl:attribute name="x1"><xsl:value-of select="$x1"/></xsl:attribute>
    <xsl:attribute name="x2"><xsl:value-of select="$x2"/></xsl:attribute>
    <xsl:attribute name="y1"><xsl:value-of select="$y1"/></xsl:attribute>
    <xsl:attribute name="y2"><xsl:value-of select="$y2"/></xsl:attribute>
    <xsl:if test="string-length($class) > 0">
      <xsl:attribute name="class"><xsl:value-of select="$class" /></xsl:attribute>
    </xsl:if>
  </xsl:element>
</xsl:template>


<!-- Generic Templates -->
<!-- 
    Just copy class 
-->
<xsl:template match="@class|@width|@height|@id" >
  <xsl:copy />
</xsl:template>


<!--
  Set the default value to 0 if the value is NULL.
-->
<xsl:template name="default0">
  <xsl:param name="value"/>
  <xsl:choose>
    <xsl:when test="$value">
      <xsl:value-of select="$value"/>
    </xsl:when>
    <xsl:otherwise>0</xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template name="defaultValue">
  <xsl:param name="value"/>
  <xsl:param name="default"/>
  <xsl:choose>
    <xsl:when test="$value">
      <xsl:value-of select="$value"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$default"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!--
    Rotate element around (x,y).
-->
<xsl:template match="@rotate" mode="text">
  <xsl:variable name="rotate">
      <xsl:call-template name="default0"><xsl:with-param name="value" select="@rotate"/></xsl:call-template>
  </xsl:variable>

  <xsl:attribute name="transform">rotate(<xsl:value-of select="."/>,
                                         <xsl:value-of select="../@x" />,
                                         <xsl:value-of select="../@y" />)
  </xsl:attribute>
</xsl:template>
</xsl:stylesheet>