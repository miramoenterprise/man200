@-----------------------------------------------------------
<!--  File = positioning.inc (Start) -->
@-----------------------------------------------------------
@- positioning.inc
@- include files for autonumbering appendix
@-----------------------------------------------------------
<chapterStart
	dest="objectVerticalPositioning"
	type="mmcTest"
	>Object vertical positioning
</chapterStart>
@-----------------------------------------------------------
<#def> thisDir	{${mman}/mmcTests/objectVerticalPositioning}
@-----------------------------------------------------------
<@include>	<#thisDir>/include/verticalOffsets.mmp
@-----------------------------------------------------------
<@include>	<#thisDir>/introduction
<@include>	<#thisDir>/aframe-inparagraph
<@include>	<#thisDir>/atextframe-inparagraph
<@include>	<#thisDir>/mathml-inparagraph
<@include>	<#thisDir>/table-inparagraph

<@include>	<#thisDir>/aframe-toplevel
<@include>	<#thisDir>/atextframe-toplevel
<@include>	<#thisDir>/mathml-toplevel
<@include>	<#thisDir>/table-toplevel
<@skipStart>
<@skipEnd>

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

@-----------------------------------------------------------
<!--  File = autonumbering.inc (End) -->
@-----------------------------------------------------------
