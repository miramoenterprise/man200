@--------------------------------------------------------------------------------
@- mmserver.inc
@- include files for mmServer, miramoRP and mmVisor 
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<#def> inMFD		1
@--------------------------------------------------------------------------------
<@include>	${mmServerGuide}/cover.inc
@--------------------------------------------------------------------------------
<chapterStart
	dest="mmsg.introduction"
	>Introduction
</chapterStart>
<@include>	${mmServerGuide}/introduction
<chapterEnd/>
@--------------------------------------------------------------------------------
<chapterStart
	dest="mmsg.mmServer"
	>mmServer Guide
</chapterStart>
<@include>	${mmServerGuide}/mmserver
@--------------------------------------------------------------------------------
<@skipStart>
<chapterStart
	dest="mmsg.miramoRP"
	>miramoRP Guide
</chapterStart>
@--------------------------------------------------------------------------------
<@include>	${mmServerGuide}/miramoRP
<chapterEnd/>
@--------------------------------------------------------------------------------
<chapterStart
	dest="mmsg.mmVisor"
	>mmVisor Guide
</chapterStart>
@--------------------------------------------------------------------------------
<@include>	${mmServerGuide}/mmVisor
@--------------------------------------------------------------------------------
<@skipEnd>
<#def> inMFD		0
<chapterEnd/>
@--------------------------------------------------------------------------------
