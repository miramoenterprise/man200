@------------------------------------------------------------
@- running
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> - code {}{
	@print({<Font fontFamily="Courier New" >$code</Font>}, n)
}
<@xmacro> - ccode {}{
	@print({<Font fontFamily="Cambridge" fontWeight="Regular" >$ccode</Font>}, n)
}


@------------------------------------------------------------
<h1>Introduction</h1>
@------------------------------------------------------------


@------------------------------------------------------------
<bp>
An illustration of a simple mmServer run command is shown in Example <xparanumonly
	id="ex.simpleCommandLine1" />.
</bp>

<exampleBlock
	>
<exampleCaption
	dest="ex.simpleCommandLine1"
	>Simple Miramo run command (1)
</exampleCaption>
miramo -Opdf <xfI>outputfile</xfI>.pdf <xfI>miramoInputfile</xfI>.mmxml
</exampleBlock>

<bp>
The above command processes an mmComposer input
file, <fI>miramoInputfile</fI>, and produces the output
file, <fI>outputfile</fI>.pdf. Input and output files may be referenced
either relative to the current working directory or via
full path names.
</bp>

<bp>
Specifying input and output files is the minimum requirement for running
Miramo jobs. The <fI>outputfile</fI> name may be specified by
the <#xMiramoXML> root element <pn>Opdf</pn> property (see page <xnpage
	id="MiramoXML.property.Opdf" />). In this case the shortest possible
command line is as illustrated in Example <xparanumonly
	id="ex.simpleCommandLine2" />.
</bp>

<exampleBlock
	>
<exampleCaption
	dest="ex.simpleCommandLine2"
	>Simple Miramo run command (2)
</exampleCaption>
miramo <xfI>miramoInputfile</xfI>.mmxml
</exampleBlock>

<bp>
Output files specified by the <#xMiramoXML> <pn>Opdf</pn> property, like
all <#xMiramoXML> properties, take precedence over output files specified
on the command line.
</bp>

<bp>
Input files must be located in directories that are readable
by the <programName>mmServer</programName> RunAs user account.
The <programName>mmServer</programName> RunAs user account
is created during the installation of Miramo.
Output files must be located in directories that are readable
and writeable by the <programName>mmServer</programName> RunAs user account.
</bp>

<bp>
The .mmxml and .pdf file extensions shown in Examples <xparanumonly
	id="ex.simpleCommandLine1" /> and <xparanumonly
	id="ex.simpleCommandLine2" /> are optional.
</bp>

<bp>
The <programName>mmaccess</programName> command line utility provides
an easy way to grant the appropriate permissions for
the <programName>mmServer</programName> RunAs user account. The command:
</bp>

<exampleBlock
	>
mmaccess -help
</exampleBlock>
<bp>
displays a list of the <programName>mmaccess</programName> options.
</bp>

<bp>
Job processing may also be initiated via .NET, C++ and Java APIs.
The .NET, C++ and Java APIs are documented in <sq>docs\api</sq> sub-directories
in the Miramo installation directory.
</bp>


@------------------------------------------------------------
<h1>Command line options</h1>

<bp>
Table <xparanumonly id="tbl.command_line_options" /> lists
the Miramo command line options. Several command line options
may be invoked directly within the <#xMiramoXML> element (see
page <xnpage
	id="MiramoXML.element.start" />).
</bp>


@------------------------------------------------------------
<simpleTable
        leftIndent="10mm"
        widths="40|R"
        tblOpts='fillColor="Black" fillTint="3%" '
	rowBreakColumn="2"
        >
@------------------------------------------------------------
<title
        dest="tbl.command_line_options"
        >List of command line options</title>
<R type="header" bottomRule="Thin" topRule="none" >
<head>Option name and value</head>
<head>Description</head>
</R>
@------------------------------------------------------------

@------------------------------------------------------------
<subHead text="Input options" />
@------------------------------------------------------------

<@skipStart>
@------------------------------------------------------------
<cmdOption name="-composer" value="mmc" >
<fI>Required</fI>.
Every job must be run using the <qm>-composer mmc</qm> command
line option.
</cmdOption>
<@skipEnd>

@------------------------------------------------------------
@- <cmdOption dest="N" name="<fI>&lt;inputfile&gt;</fI>" value="<fI>filename</fI>" >
@- <cmdOption dest="&lt;<fI>inputfile</fI>&gt;" name="inputfile" value="<fI>filename</fI>" >
<cmdOption dest="inputfile" name="&lt;<fI>inputfile</fI>&gt;" value="" >
<fI>Required</fI>.
The <fI>&lt;inputfile&gt;</fI> must be the last parameter on the
command line.


<rp>
<fI>filename</fI> is name of a file in Miramo XML format.
</rp>

</cmdOption>

@------------------------------------------------------------
<cmdOption name="-mfd" value="<fI>filename</fI>" >
<fI>filename</fI> is name of a format definitions file saved
from MiramoDesigner in standard .mfd format or in .mfdp packaged
format.

<rp>
If <fI>filename</fI> has a .mfdp extension, the template
file is treated as a packaged template file.
</rp>

<rp>
Packaged template files are created using the MiramoDesigner Dxport
as .mfdp option. A .mfdp file is a single
zip file containing the MiramoDesigner template along with all its
externally-referenced images for direct processing by mmComposer.
</rp>

<rp>
If <fI>filename</fI> starts with http// or https:// then the template file
is retrieved from the URL. The .mfdp URL resource is not retrieved in the
case it has been previously downloaded and cached locally, and
the date of the URL resource is not later that the locally cached
version of the .mfdp template.
This behavior may be changed using the <pn>-updateUrlTemplateCache</pn> command-line
option.
</rp>

</cmdOption>

@------------------------------------------------------------
<subHead text="PDF output options" > 
 All the following options may be overridden by setting the
corresponding <#xMiramoXML> properties. See pages <xnpage
	id="MiramoXML.element.start"><xphyphen/><xnpage
	id="MiramoXML.element.end">.
</subHead>
@------------------------------------------------------------

@------------------------------------------------------------
<cmdOption name="-Opdf" value="<fI>filename</fI>" >
The output PDF file name.
<rp>
Every job must be run either by specifying the <qm>-Opdf <fI>filename</fI></qm> command
line option or by using the <#xMiramoXML> <pn>Opdf</pn> property (see page <xnpage
	id="MiramoXML.property.Opdf" />).
</rp>
</cmdOption>

@------------------------------------------------------------
<cmdOption
	name="-showProperties"
	value="N | Y | <fI>key</fI>"
	>
<rp>
Show input format properties in annotations
in output PDF. For use in application development and
debugging. <pv>N</pv> (default) = show no format properties.
 <pv>Y</pv> = show properties for all formats.
</rp>

<rp>
<fI>key</fI> may be any combination of the following characters: <ccode>FIPX</ccode>.
</rp>

@------------------------------------------------------------
@- Special for <propertyValueKeyList> in 'running' table
@------------------------------------------------------------
</P>
</Cell>
</Row>
@------------------------------------------------------------
<propertyValueKeyList
        @- startIndent="8mm"
        startIndent="4mm"
        keyWidth="18mm"
        keyHeading="Key value"
        descriptionHeading="Description"
        inRunning="Y"
        >

<vk key="F" >
Show <#xFont> properties.
</vk>

<vk key="I" cambridge="Y" >
Show <#xImage> properties.
</vk>

<vk key="P" >
Show <#xP> properties.
</vk>

<vk key="X" >
Show cross-reference properties.
</vk>
</propertyValueKeyList>
@------------------------------------------------------------
@- Special for <propertyValueKeyList> in 'running' table
@------------------------------------------------------------
<Row height="0.1mm" >
<Cell>
<P>
@------------------------------------------------------------

<rp>
For example, setting <pn>-showProperties</pn> to <ccode>PI</ccode> shows
the properties for paragraphs and images only.
Setting <pn>-showProperties</pn> to <ccode>FIPX</ccode> is
equivalent to <ccode>Y</ccode>.
</rp>


</cmdOption>

@------------------------------------------------------------
<cmdOption name="-jobOptions" value="N | Y" >

<rp>
Automatically downsample and compress raster images.
Effective only if the <#xDocDef> <pn>jobOptionsDef</pn> property
is not set to <pv>none</pv> (see page <xnpage
        id="DocDef.property.jobOptionsDef" />) <fI>and</fI> 
the <#xMiramoXML> <pn>jobOptions</pn> property is not
set to <pv>N</pv> (see page <xnpage
        id="MiramoXML.property.jobOptions" />).
</rp>

<rp>
The image compression and downsampling parameters
are specified by the <#xJobOptionsDef> element (see pages <xnpage
        id="JobOptionsDef.element.start" /><xphyphen/><xnpage
        id="JobOptionsDef.element.end" />) referenced
by the <#xDocDef> <pn>jobOptionsDef</pn> property (page <xnpage
        id="DocDef.property.jobOptionsDef" />).
</rp>


</cmdOption>

@------------------------------------------------------------
<cmdOption name="-PDFbookmarks" value="all | none | <fI>int</fI>" >
Show PDF bookmarks, or PDF bookmark levels when
output is displayed in a PDF viewer.
<fI>n</fI> indicates the number of PDF bookmark levels to display.

<rp>
A value of <pv>0</pv> specifies
that only top-level bookmarks are displayed on first opening in
a PDF file viewer. 
A value of <pv>1</pv> specifies 
that top-level bookmarks and the next level are displayed on first opening in
a PDF file viewer, and so on. 
</rp>
@- </rp>

</cmdOption>

@------------------------------------------------------------
<cmdOption name="-pdfType" value="untagged | UA" >
Set to <pv>UA</pv> for tagged, <fI>Universally Accessible</fI>, PDF/UA output.
<rp>
Default is <pv>untagged</pv>.
</rp>
<rp>
The PDF/UA format is defined by reference to <URL
	url="https://www.iso.org/standard/51502.html"
>ISO 32000-1:2008 <fI>Document management - Portable
document format - Part 1: PDF 1.7</fI></URL> in <URL
	url="https://www.iso.org/standard/64599.html"
>ISO 14289-1:2014 <fI>Document management applications - Electronic
document file format enhancement for accessibility - Part 1: Use of
ISO 32000-1 (PDF/UA-1)</fI></URL>.
See also: <URL
	url="https://en.wikipedia.org/wiki/PDF/UA"
>PDF/UA Wikipedia article</URL>.
</rp>
<rp>
The document processing time and output size will be significantly
increased when <pn>-pdfType</pn> is set to <pv>UA</pv>.
</rp>
</cmdOption>

<@skipStart>
https://en.wikipedia.org/wiki/PDF/UA

Duff Johnson
www.pdfa.org/wp-content/until2016_uploads/2012/07/PDFTechConf_PDFUA_Intro_forPDF.pdf

https://www.iso.org/standard/64599.html
ISO 14289-1:2014
Document management applications � Electronic document file format enhancement for accessibility � Part 1: Use of ISO 32000-1 (PDF/UA-1)

https://www.iso.org/standard/51502.html
ISO 32000-1:2008
Document management � Portable document format � Part 1: PDF 1.7
<@skipEnd>

@------------------------------------------------------------
<cmdOption name="-cropMarks" value="N | Y" >
Print cropmarks.
The media box must be larger than the page size.
</cmdOption>

@------------------------------------------------------------
<cmdOption
	name="-mediaSize"
	value="a3 | a4 | a5 | b5 | tabloid | legal | letter | <fI>n</fI>dim<fI>n</fI>dim"
	>
Media size dimensions. Either a keyword value, or a <fI>n</fI>dim<fI>n</fI>dim value.
If a <fI>n</fI>dim<fI>n</fI>dim value,
the first <fI>n</fI> value is the media width,
the second <fI>n</fI> value is the media height, for example:
"10cm12in".

<cp>
The <on>-mediaSize</on> option sets the size of the PDF media box.
</cp>

<cp>
By default the size of the PDF media, bleed, trim and crop boxes
are the same as the page size specified by the <#xDocDef> or <#xPageDef> elements
(see pages <xnpage
	id="DocDef.element.start" /><xphyphen/><xnpage
	id="DocDef.element.end" /> and <xnpage
	id="PageDef.element.start" /><xphyphen/><xnpage
	id="PageDef.element.end" />).
</cp>

</cmdOption>


@------------------------------------------------------------
<subHead text="Font directories" ></subHead>
@------------------------------------------------------------

@------------------------------------------------------------
<cmdOption name="-fontPath" value="<fI>path</fI>" >
List of font directories containing the fonts
needed for the current job. The path list must begin
with the special keyword <pv>default</pv>.

<rp>
On Linux and MacOS the font path directories are separated
by <sq>:</sq> (colon) characters.
</rp>

<rp>
On Windows the font path directories are separated
by <sq>;</sq> (semicolon) characters.
</rp>

<rp>
See <xsection
	psize="7.2pt"
	id="running.fontPath.head" /> on page <xnpage
	id="running.fontPath.head" />.
</rp>

</cmdOption>

@------------------------------------------------------------
<subHead text="URL and caching options" >
@- <br/>
See <nxsection
	psize="7.3pt"
	id="chapter.urlCaching.start" /> on page <xnpage
	id="chapter.urlCaching.start" /> for detailed information
about URL caching.
</subHead>
@------------------------------------------------------------

@------------------------------------------------------------
<cmdOption name="-updateUrlImageCache" value="N | Y" >

When <pn>-updateUrlImageCache</pn> is set to <ov>Y</ov> then in the case
each <#xImage> with a <pn>file</pn> value referencing an
image as a URL (starts with http// or https://)
if there is an already-cached version of the image, the date of the URL is checked to
see whether it has been modified since the date of the cached version.
If the URL has been modified more recently, it is re-downloaded and
replaces the version in the cache.

<@skipStart>
<rp>
See <sq>How URL caching works</sq> on page <xnpage
        id="chapter.cachingAppendix.start" />.
</rp>
<@skipEnd>

<rp>
Default: <pv>N</pv>
</rp>

</cmdOption>

@------------------------------------------------------------
<cmdOption name="-updateUrlTemplateCache" value="N | Y" >

When <pn>-updateUrlTemplateCache</pn> is set to <ov>Y</ov> then in the case
a MiramoDesigner template is referenced as a URL (starts with http// or https://)
if there is an already-cached version of the template, the date of
the URL is checked to see whether it has been modified since
the date of the cached version.
If the URL has been modified more recently, it is re-downloaded and
replaces the version in the cache.

<@skipStart>
<rp>
See <nxsection
	psize="7.3pt"
        id="chapter.urlCaching.start" /> on page <xnpage
        id="chapter.urlCaching.start" />.
</rp>
<@skipEnd>

<rp>
Default: <pv>N</pv>
</rp>

</cmdOption>

@------------------------------------------------------------
<cmdOption name="-imageCacheDir" value="<fI>dirName</fI>" >
Name of the image cache directory.

<cp>
By default cached images are located in the
<Font lineBreak="N" >.miramoCache/images</Font> directory in the user's
home directory.
</cp>

<cp>
The default location of the cache directory maybe overridden
by setting the <pn>mmImageCacheDir</pn> environment variable.
See page <xnpage
	id="envar.mmImageCacheDir" />.
</cp>

<cp>
The <pn>-imageCacheDir</pn> option has the second highest precedence,
after the <#xMiramoXML> <pn>imageCacheDir</pn> property (see page <xnpage
	id="MiramoXML.property.imageCacheDir" />).
</cp>

</cmdOption>

@------------------------------------------------------------
<cmdOption name="-templateCacheDir" value="<fI>dirName</fI>" >
Name of the template cache directory.

<cp>
By default cached templates are located in the
.miramoCache/templates directory in the user's
home directory.
</cp>

<cp>
The default location of the cache directory maybe overridden
by setting the <pn>mmTemplateCacheDir</pn> environment variable.
</cp>

<cp>
The <pn>-templateCacheDir</pn> option has the second highest precedence,
after the <#xMiramoXML> <pn>templateCacheDir</pn> property.
See page <xnpage
	id="envar.mmTemplateCacheDir" />.
</cp>

</cmdOption>

<@skipStart>
@------------------------------------------------------------
<cmdOption name="-templateCacheDir" value="<fI>dirName</fI>" >
Name of the template cache directory.

<rp>
By default cached templates are located in the
.miramoCache/templates directory in the user's
home directory.
</rp>

<rp>
The default location of the cache directory maybe overridden
by setting the <pn>mmTemplateCacheDir</pn> environment variable.
</rp>

<rp>
The <pn>-templateCacheDir</pn> option has the second highest precedence,
after the <#xMiramoXML> <pn>templateCacheDir</pn> property.
</rp>

</cmdOption>
<@skipEnd>



@------------------------------------------------------------
<subHead text="Memory usage" ></subHead>
@------------------------------------------------------------

@------------------------------------------------------------
<cmdOption name="-maxMemory" value="<fI>size</fI>" >

<fI>size</fI> specifies the allowable maximum host system RAM usage in
MB (megabytes) or GB (gigabytes).

<rp>
Number values must be followed by either MB or GB (without an intervening space).
</rp>

<rp>
The given <fI>size</fI> value is rounded down to the nearest
integer number of kilobytes. For example a <fI>size</fI> value
of 0.1GB or 100MB is converted to 104&thinsp;857 kilobytes.
</rp>


<rp>
The default value for <fI>size</fI> is as specified by
the <pn>mmMaxMemory</pn> environment variable. If the <pn>mmMaxMemory</pn> environment
variable is not set, then the default
is 70% of the host system RAM, to the next lower kilobyte (1024 octet).
</rp>

<rp>
The maximum value of <fI>size</fI> is 90% of the host system RAM.
If <fI>size</fI> is set to
a value greater than 90% of the host system RAM,
the <fI>size</fI> value is automatically
reduced to 90% of the host system RAM.
</rp>

</cmdOption>

@------------------------------------------------------------
</simpleTable>
@------------------------------------------------------------

@------------------------------------------------------------
<h1>Environment variables</h1>

<bp>
@- Table <xparanumonly id="tbl.command_line_options" /> lists
Table <xparanumonly id="tbl.runningEnvironmentVariables" /> lists
the Miramo environment variables.
</bp>


<simpleTable
        leftIndent="10mm"
        widths="40|R"
        tblOpts='fillColor="Black" fillTint="3%" '
	rowBreakColumn="2"
        >
@------------------------------------------------------------
<title
        dest="tbl.runningEnvironmentVariables"
        >List of environment variables</title>
<R type="header" bottomRule="Thin" topRule="none" >
<head>Variable name</head>
<head>Description</head>
</R>
@------------------------------------------------------------


@------------------------------------------------------------
<R>
<cell><fB>mmFontPath</fB><MkDest id="envar.mmFontPath" /></cell>
<cell>
List of ':' or ';' separated directory locations for fonts.
The path list must begin
with the special keyword <pv>default</pv>.

<cp>
See <xsection
	psize="7.2pt"
	id="running.fontPath.head" /> on page <xnpage
	id="running.fontPath.head" />.
</cp>

</cell>
</R>


@------------------------------------------------------------
<R>
<cell><fB>mmImageCacheDir</fB><MkDest id="envar.mmImageCacheDir" />
<docNote p="7.5pt" clm="3.5pt" crm="2pt" border="N" svgDef="grayGradient" >
Check setting to null, and to non-existing directory.

<P/>
<P textColor="Amber" >
For currently applicable rates, see page <xnpage
	id="Image.element.start" />.
</P>
<@skipStart>
<P/>
<P textColor="Olive" >
For more information, check this Wikipedia article: <URL
url="https://en.wikipedia.org/wiki/Wright_brothers" >Wright Brothers</URL>.</P>
<P/>
<P textColor="Olive" >
Special note: Fly NYC &#x2192; LAX &#x2192; NYC four times and you can get a good meal on expenses.

</P>
<@skipEnd>
</docNote>
</cell>
<cell>
Directory location for storing downloaded images
referenced by URLs.

<cp>
The directory referenced by the <fB>mmImageCacheDir</fB> environment
variable must be readable and writable by the Miramo user.
</cp>

<cp>
Default:<br/>
<code>$HOME/.miramoCache/images</code>
</cp>
</cell>
</R>

@------------------------------------------------------------
<R>
<cell><fB>mmTemplateCacheDir</fB><MkDest id="envar.mmTemplateCacheDir" /></cell>
<cell>
Directory location for storing downloaded .mfdp templates
referenced by URLs.

<cp>
The directory referenced by the <fB>mmTemplateCacheDir</fB> environment
variable must be readable and writable by the Miramo user.
</cp>

<cp>
Default:<br/>
<code>$HOME/.miramoCache/templates</code>
</cp>
</cell>
</R>

@------------------------------------------------------------
<R>
<cell><fB>mmMaxMemory</fB><MkDest id="envar.maxMemory" /></cell>
<cell>
Maximum memory for job.

<cp>
See the <pn>-maxMemory</pn> command-line option in Table <xparanumonly
        @- id="tbl.mmc2.command_line_options" /> on page <xnpage
        id="tbl.command_line_options" /> on page <xnpage
        id="commandline.option.-maxMemory" />.
</cp>
</cell>
</R>

@------------------------------------------------------------
</simpleTable>
@------------------------------------------------------------

@------------------------------------------------------------
@- <@include> ${refguide}/running/newapi
@------------------------------------------------------------


@-===========================================================
<h1>Accessing fonts<MkDest id="running.fontPath.head" /></h1>
@- <h1 dest="running.fontPath.head" >Accessing fonts</h1>

<bp>
The location of fonts for use by Miramo may be
specified <fI>via</fI> the <pn>mmFontPath</pn> environment
variable or the <pn>-fontPath</pn> command-line option.
See pages <xnpage
	id="envar.mmFontPath" /> and <xnpage
	id="commandline.option.-fontPath" />.
</bp>

<bp>
On all platforms, if the <pn>-fontPath</pn> command-line option is used
<fI>only</fI> fonts at the top level in the list of directories
are accessible by Miramo.
The value of the <pn>mmFontPath</pn> environment variable is ignored
when the <pn>-fontPath</pn> command-line option is used.
</bp>

<bp>
Any number of font directories may be specified by the <pn>mmFontPath</pn> environment
variable or the <pn>-fontPath</pn> command-line option.
</bp>

@------------------------------------------------------------
<h2>Accessing fonts on Windows</h2>

<bp>
<docNote p="7.5pt" clm="3.5pt" crm="2pt" border="N" fillTint="0%" svgDef="orangeGradient" >
Check setting to null, and to non-existing directory.

<P/>
<P textColor="Amber" >
For currently applicable rates, see page <xnpage
	id="DocDef.element.start" />.
</P>

<P/>
<P textColor="Olive" >
For more information, check this Wikipedia article: <URL
url="https://en.wikipedia.org/wiki/Wright_brothers" >Wright Brothers</URL>.</P>
<P/>
<P textColor="Olive" >
Special note: Fly NYC &#x2192; LAX &#x2192; NYC four times and you can get a good meal on expenses.

</P>
</docNote>
On Windows if the <pn>mmFontPath</pn> environment variable is
not set <fI>and</fI> or is set to null and the <pn>-fontPath</pn> command-line option is not
used, then by default Miramo accesses fonts in
the <Code>%systemroot%\Fonts</Code> directory (usually <Code>C:\Windows\Fonts</Code>).
</bp>

<bp>
If the <pn>mmFontPath</pn> environment variable is
set to one or more directories, then Miramo accesses only the fonts
located at the top level within the specified list of directories.
</bp>

<bp>
For example, if there's a directory called <Code>D:\project22\fonts</Code>,
setting the <pn>mmFontPath</pn> as follows:
</bp>

<exampleBlock
	tabs="5|40"
	>
mmFontPath="default;D:\project22\fonts"
</exampleBlock>

<bp>
means that only font files in that directory are available for Miramo
jobs.
</bp>

<bp>
To access fonts in the <Code>D:\project22\fonts</Code> directory <fI>and</fI> in
the <Code>%systemroot%\Fonts</Code> directory then the <pn>mmFontPath</pn> environment
variable should be defined as follows:
</bp>

<exampleBlock
	tabs="5|40"
	>
	@- mmFontPath="%systemroot%\Fonts;D:\project22\fonts"
	mmFontPath="default;D:\project22\fonts"
</exampleBlock>

<bp>
If the <pn>-fontPath</pn> command-line option is used the specified font
search path is exactly as specified. The current value of
the <pn>mmFontPath</pn> environment variable, if any, is immaterial.
</bp>


@------------------------------------------------------------
<h2>Accessing fonts on MacOS</h2>

<bp>
<docNote>
Maybe delete
</docNote>
On MacOS if the <pn>mmFontPath</pn> environment variable is
not set or is set to null <fI>and</fI> the <pn>-fontPath</pn> command-line
option is not used, then by default Miramo accesses fonts in the following three
directories:
</bp>
<exampleBlock
	tabs="5|40"
	>
/System/Library/Fonts
/Library/Fonts
~/Library/Fonts		(<xfI> Sub-directory in user's home directory</xfI>)
</exampleBlock>

@- Default fonts w/ Catalina:
@- https://support.apple.com/en-us/HT210192



@------------------------------------------------------------
<h2>Accessing fonts on Linux</h2>

<bp>
On Linux the <pn>mmFontPath</pn> environment variable must always be
set explicitly.
</bp>

<bp>
This can be done for all users during installation, or by modifying
the Miramo execution wrapper. There are no assumed, built-in default
font locations on Linux.
</bp>

<bp>
A default font path for all users may be specified by editing 
the <sq>runComposerPDF.sh</sq> file in the Miramo installation
directory to add the following after the first line.
</bp>

<exampleBlock
	tabs="5|40"
	>
@- mmFontPath="<xfI>fontDir1</xfI>:<xfI>fontDir2</xfI>:<xfI>fontDir3</xfI>:$mmFontPath"
mmFontPath="default:<xfI>fontDir1</xfI>:<xfI>fontDir2</xfI>:<xfI>fontDir3</xfI>:$mmFontPath"
export mmFontPath
</exampleBlock>

<@skipStart>
<@skipEnd>


@- <P fontFamily="Arial700" fontWeight="Regular" >Arial700</P>


@------------------------------------------------------------
<#def> elementStart.firstExample	1
@------------------------------------------------------------

@- <@include> ${mfd}/paradef5.inc
@------------------------------------------------------------
