#--------------------------------------------------------------------------------
#- runx.ps1
#--------------------------------------------------------------------------------
#
#  e.g. runx inline
#  e.g. runx drawguide
#
#--------------------------------------------------------------------------------
$env:releaseVersion	= "1.4"
$env:releaseVersion	= "2.0"
$env:releaseVersion	= "1.5"
$env:releaseVersion	= "v 2.0 [1st DRAFT DRAFT] "
$env:releaseVersion	= "Version 2.0 [DRAFT 1]"
$env:releaseVersion	= "v 2.0"
$env:releaseVersion	= "v 2.4"
$env:releaseVersion	= "v 3.0 [DRAFT]"
$env:releaseVersion	= "v 3.0"
#--------------------------------------------------------------------------------
# $env:mmFontPath		= "C:\Windows\fonts;C:\ap\miramo\fonts"
# $env:mmFontPath		= "default;C:\ap\miramo\fonts"
# $env:mmFontPath		= "default;C:\ap\fonts\MiramoFonts"
# $env:mmFontPath		= "default;"
# $env:mmFontPath		= "default;C:\ap\fonts\MiramoFonts"
$env:mmFontPath		= "$env:mman\fonts"
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#- jobOptions values:
#--------------------------------------------------------------------------------
$env:JOmaximumDPI		= "400"
$env:JOdownsampleDPI		= "200"
$env:JOdownsampleFilter		= "lanzcos"
$env:JOcompression		= "jpeg"	#- jpeg | zip
$env:JOquality			= "30%"	#- %
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
$env:mmCommand			= "miramox -composr mmc"
$env:mmCommand			= "C:/ap/miramo\MiramoPDF-3.0.0\miramoRP"
$env:mmCommand			= "miramo"
$env:port			= "22088"
$env:host			= "localhost"
$env:user			= "admin"
$env:password			= "admin"
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# man100  180 % miramo -composer sf -v
# MiramoCore:
#   Version    : develop/1.7.0p031
#   Build date : Monday, June 29, 2020 - 11:49:32
#   Build ID   : 511bc4aadfa701caa6a23b865229c24168f643fa
# 
# MiramoRender:
#   Version    : MPDF-50-listlabel-image-ck/1.7.0p005
#   Build date : Saturday, June 27, 2020 - 10:45:14
#   Build ID   : f4db544fd352c107557902d92a271f37565d  Build ID
# 
#--------------------------------------------------------------------------------
$env:composer		= "mmc"
$env:composerTag	= "sf"		#- Used for '-composer tagname' command
					#- line option. Percolates down to <@system>
					#- sub-processes, via runJob.mmp
					#- Can be "mmc" or "sf"
$env:mmVersion	= "3.5"
#--------------------------------------------------------------------------------
function skip {
switch -casesensitive ( $env:composerTag ) {
	'sf'	{
		$mmVersion	= miramo -composer sf -v 2>&1 | foreach-object {$_.tostring()} | out-string
		$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
		# delete below
		#- $mmVersion	= miramo -composer mmc -v 2>&1 | foreach-object {$_.tostring()} | out-string
		#- $mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
		#- $mmVersion	= $mmVersion | select-string -pattern "iramo"
		}
	'mmc'	{
		$mmVersion	= miramo -composer mmc -v 2>&1 | foreach-object {$_.tostring()} | out-string
		$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
		$mmVersion	= $mmVersion | select-string -pattern "iramo"
		}
			default {
				write-host 	"not found"
				}
	}
}
#--------------------------------------------------------------------------------
# $mmVersion		= miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
# $mmVersion		= $mmVersion | foreach-object {$_.tostring()} | out-string
# $mmVersion		= $mmVersion | select-string -pattern "iramo"
write-host		"$env:mmVersion"
# exit
$mmVersion		= "$env:mmVersion"
# Copy $args array to $runxArgs
$runxArgs		= $args
#--------------------------------------------------------------------------------
# write-host		"$env:mman"
$env:documentInitiator 		= "runx"
$env:installGuide 		= "$env:mman\installGuide"
$env:documentName 		= "mmDrawGuide.pdf"
$env:documentName		= "mmComposerReferenceGuide.pdf"
$env:documentFooterTitle	= "mmComposer Reference Guide ($env:releaseVersion)"
$env:includeFootnoteDef		= "1"
$env:setLandscape		= "0"
$env:QA				= "N"
#--------------------------------------------------------------------------------
$env:mkfmcRelnotes	= "N"
$env:mkfmcAddendum	= "N"
$env:mkinstallGuide	= "N"
$env:mkmmc2		= "N"
$env:sfg		= "N"	# Show grid on main text flow (only if ${QA} set
$sp			= "N"	# showProperties value
$sb			= "N"	# showBorders value
$env:jo			= "N"	# jobOptions value
$mmp			= 0	# If -mmp parameter used, only process using mmpp
#--------------------------------------------------------------------------------
$outFolder		= "$env:output"
$startFolder		= pwd
$oldv			= 0	# If 1, run old version
cd $outFolder
#--------------------------------------------------------------------------------
$startTime		= $(get-date)
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
$nowMonthNameTime	= $(Get-Date).ToString('dd MMM yyyy HH:mm:ss')
write-host 		"Start time: $nowTime"
#--------------------------------------------------------------------------------
$OrigOutputEncoding = [Console]::OutputEncoding
$runxHelp      = @"
runx [options] doctype
e.g. 
	runx inline | drawguide | mantests | refguide | lists | forms |
		language | color | annots | annotsdef
		thai 
"@
$fmc	= 0	#--- Use mmComposer
function getOpts	{
	#------------------------------------------------------------------------
	#- Store & strip '-options'.
	#------------------------------------------------------------------------
	Param([int]$optsCount)

	For ($i=0; $i -lt $optsCount ; $i++)  {
		$optionCount += 1
		switch -casesensitive ($runxArgs[$i]) {
			'-f'		{$fmc	= 1}	#--- Use fmComposer
			'-fm'		{$fmc	= 1}	#--- Use fmComposer
			'-fmc'		{$fmc	= 1}	#--- Use fmComposer
			'-mmc'		{$mmc	= 1}	#--- Use mmComposer (default)
			'-relaxed'	{$relaxed	= 1}	#--- Relaxed mode
			'-sb'		{	#- Show borders mode
						$sb		= $runxArgs[$i + 1]
						$i		= $i + 1
						$optionCount	+= 1
						}
			'-sp'		{	#- Show properties mode
						$sp		= $runxArgs[$i + 1]
						$i		= $i + 1
						$optionCount	+= 1
						}
			'-oldv'		{	#- Use old mmServer (Windows only)
						$oldv	= 1
						}
			'-l'		{$env:setLandscape	= "1"}	#--- 
			'-sfg'		{$env:sfg		= "Y"}	#--- 
			'-mmp'		{$mmp	= 1}	#--- 
			'-jo'		{	#- jobOptions, val = N | Y
						$env:jo		= $runxArgs[$i + 1]
						$i		= $i + 1
						$optionCount	+= 1
						}
			default {
				# Not an option
				$optionCount -= 1
				}
		}
	}
}
#--------------------------------------------------------------------------------
if ($args.count -gt 0) {
	if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
		write-host $runxHelp
		exit
		}
	else    {
		$lastArgument	= $args[$args.count - 1]
		}
	}
else    {
	# Just 'runx' typed, with no arguments
	write-host "runx: argument required"
	write-host "See: runx -h[elp] "
	exit
	}
#--------------------------------------------------------------------------------
#- Process arguments
#--------------------------------------------------------------------------------
if ($args.count -gt 1) {
	. getOpts $($args.count - 1)
}
$argumentCount	= $args.count - $optionCount	# Arguments excluding options
if ($argumentCount -gt 1) {
	write-host	"runx: too many arguments"
	write-host	"See: runx -h[elp] "
}
$docType		= $lastArgument
$env:docName		= $docType
#--------------------------------------------------------------------------------
write-host "spVal $sp joVal $env:jo $lastArgument "
# exit
#--------------------------------------------------------------------------------
	write-host	"runx: $docType"
#--------------------------------------------------------------------------------
switch -casesensitive ($docType) {
	'inline'	{$inFile	= "$env:inline\inline.inc"			}
	'annotsdef'	{$inFile	= "$env:annotsdef\annotsdef.inc"			}
	'annots'	{$inFile	= "$env:annotsdef\annotsdef.inc"			}
	'ishort'	{$inFile	= "$env:inline\ishort.inc"			}
	'fdefs'		{$inFile	= "$env:mfd\mfd.inc"				}
	'mfd'		{$inFile	= "$env:mfd\mfd.inc"				}
	'miramorp'	{$inFile	= "$env:mmServer\miramoRPguide\miramorp.inc"	
				$env:documentName = "miramoRemoteProcessing.pdf"
				}
	# 'mmsg'		{$inFile	= "$env:mmServer\mmServerGuide\mmserver.inc"	
	'mmsg'		{$inFile	= "$env:mmServerGuide\mmServerGuide.inc"	
				$env:documentName = "mmServerGuide.pdf"
				}
	'mmserver'	{$inFile	= "$env:mmServer\mmServerGuide\mmserver.inc"	
				$env:documentName = "mmServerGuide.pdf"
				}
	'mshort'	{$inFile	= "$env:mfd\mshort.inc"				}
	'mfds'		{$inFile	= "$env:mfd\mfd.inc"				}
	'drawguide'	{$inFile	= "$env:drawguide\drawguide.inc"		
				$env:documentName = "mmDrawGuide.pdf"
				}
	'xelements'	{$inFile	= "$env:xelements\xelements.inc"		}
	'refguide'	{$inFile	= "$env:refguide\refguide.inc"			}
	'forms'		{$inFile	= "$env:formsguide\formsguide.inc"	
				$env:documentName		= "mmFormsGuide.pdf"
				$env:documentFooterTitle	= "mmComposer Forms Guide ($env:releaseVersion)"
				}
	'formsguide'	{$inFile	= "$env:formsguide\formsguide.inc"			}
	'rshort'	{$inFile	= "$env:refguide\rshort.inc"			}
	'qa'		{$inFile	= "$env:mman\qa\qa.inc"			
			$env:QA		= "Y"
											}
	'short'		{$inFile	= "$env:refguide\short.inc"			}
	'long1'		{$inFile	= "$env:refguide\long1.inc"			}
	'long2'		{$inFile	= "$env:refguide\long2.inc"			}
	'long3'		{$inFile	= "$env:refguide\long3.inc"			}
	'long4'		{$inFile	= "$env:refguide\long4.inc"			}
	'long5'		{$inFile	= "$env:refguide\long5.inc"			}
	'long6'		{$inFile	= "$env:refguide\long6.inc"			}
	'color'		{$inFile	= "$env:appendices\color\color.inc"		}
	'caps'		{$inFile	= "$env:appendices\capitalization\capitalization.inc" }
	'language'	{$inFile	= "$env:refguide\language\language.inc"		}
	'running'	{$inFile	= "$env:refguide\running\running.inc"		}
	'types'		{$inFile	= "$env:refguide\propertyTypes\propertyTypes.inc"	}
	'ix'		{$inFile	= "$env:refguide\inline\ix.inc"			}
	'textframedef'	{$inFile	= "$env:refguide\mfd\textframedef.inc"		}
	'aframe'	{$inFile	= "$env:refguide\inline\aframe.inc"		}
	'paradef'	{$inFile	= "$env:refguide\mfd\paradef.inc"		}
	'thai'		{$inFile	= "$env:refguide\language\thai\thai.inc"	}
	'fonts'		{$inFile	= "$env:appendices\fonts\fonts.inc"		}
	'appendices'	{$inFile	= "$env:appendices\appendices.inc"		}
	'ashort'	{$inFile	= "$env:appendices\ashort.inc"			}
	'autonumbering'	{$inFile	= "$env:appendices\autonumbering\autonumbering.inc" }
	'entities'	{$inFile	= "$env:appendices\characterEntities\characterEntities.inc"		}
	'lastPage'	{$inFile	= "$env:mman\tests\lastPage\lastPageTestInput.inc"
			$env:documentFooterTitle = "&#x24;mman/tests/lastPage (runx -l lastPage)&emsp;&emsp;&emsp;"
			$env:documentFooterTitle = "$env:mmVersion &emsp; $env:documentFooterTitle"
			$env:documentFooterTitle = "$nowMonthNameTime &emsp; $env:documentFooterTitle"
						}
	'textlanguage'	{$inFile	= "$env:appendices\textlanguage\textlanguage.inc" }
	'opos'		{
$env:documentFooterTitle	= "<Font capitalization='smallCaps' >Rules and Tests</Font> (<fB>MiramoPDF v $env:releaseVersion</fB> &emdash; $startTime)"
$inFile	= "$env:mman/mmcTests/objectVerticalPositioning\positioning.inc"
}
	'lists'		{$inFile	= "$env:refguide\lists\lists.inc"		}
	'mantests'	{$inFile	= "$env:mantests\tests.inc" 
			 $outFolder	= "$env:mantests" 				}
	'svgtest1'	{$inFile	= "$env:svgtests\svg1" 
			 $outFolder	= "$env:svgtests\svg1" 				}
	#------------------------------------------------------------------------
	'fmcrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}
	'fmrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}


	'installguide'	{$inFile	= "$env:mman\installGuide\installguide.inc" 
			 $env:mkinstallGuide	= "Y" 					}
	'installGuide'	{$inFile	= "$env:mman\installGuide\installguide.inc" 
			 $env:mkinstallGuide	= "Y" 					}


	'fmcaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmcAddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'mmc2'		{$inFile		= "$env:mmc2\mmc2.inc" 
			 $env:mkmmc2		= "Y" 					}
	#------------------------------------------------------------------------
	default		{
			write-host	"Unknown document type: $docType Try runx -h"
			exit
			}
	}
#--------------------------------------------------------------------------------
function runmmc {
	#------------------------------------------------------------------------
	#- http://stackoverflow.com/questions/2902874/input-encoding-accepting-utf-8
	#------------------------------------------------------------------------
	if($fmc -eq 1) {
		#--- Use fmComposer
		$ctag			= "fm"		#- 'ctag' is for composer (mm or fm)
		$env:composer		= "$ctag"
		$env:composer		= "fmc"
		$tmpFile		= "$env:output\$docType"+".$ctag" + ".mmxml"
		$outputFile		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$oMIF			= "$outFolder\$docType"+".$ctag" + ".mif"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$env:composerText	= "fmComposer"
		mmpp -Mfile $env:control\control.mmp $inFile | out-file -enc utf8 $tmpFile
		"</MiramoXML>" | out-file -append -enc utf8 $tmpFile
		miramo -composer $ctag -PDFbookmarks 0 -sendenv Y -Omif $oMIF -Opdf $outputFile $tmpFile 2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) { $_.Exception.Message } else { $_ } }		
		}
	else	{
pwd
		#--- Use mmComposer
		$ctag			= "mm"		#- 'ctag' is for composer (mm or fm)
		$tmpFile		= "$env:output\$docType"+".$ctag" + ".mmxml"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$outputFile		= "$outFolder\$docType"+ ".pdf"
		$outputFile2		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$outputFile3		= "$outFolder\mmComposerReferenceGuide" + ".pdf"
		$env:composerText	= "$ctag" + "Composer"
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
		#----------------------------------------------------------------
		mmpp -Mfile $env:control\control.mmp $inFile | out-file  $tmpFile -enc utf8
		"</MiramoXML>" | out-file -append -enc "utf8" $tmpFile
		if($mmp -eq 1) {
			#----------------------------------------------------------------
			[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
			#----------------------------------------------------------------
			write-host	"mmpp only: no PDF output produced !!! "
			$elapsedTime	= $(get-date) - $startTime
			$totalTime	= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
			write-host	"TIME:		$totalTime"
			cd $startFolder
			exit
			}
		$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		write-host 		"Starting mmComposer [$env:composerTag]: $nowTime"
		write-host 		"-jobOptions $env:jo"
		if($oldv -eq 1) {
			write-host 	"Using OLD Miramo"
			# miramo -composer "$env:composerTag" -relaxed `
			miramo -composer "$env:composerTag" -fontPath "$env:mman\fonts" -relaxed `
				-jobOptions "$env:jo" `
				-sendEnv Y -Opdf $outputFile $tmpFile `
				2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) `
					{ $_.Exception.Message.trim("`n") } else { $_ } } 
			}
		else {
			# "$env:MM_HOME\MiramoPDF-3.0.0"
			write-host 	"Using NEW Miramo"
			#--------------------------------------------------------
			if($sb -eq 'Y') { 
				write-host 	"Using: addMfdBorders.mmp $sb "
				# mmpp -Mfile $env:manbin\addMfdBorders.mmp $tmpFile | `
				# 	out-file -enc oem $env:output\zxy | select -skip 5
				mmpp -Mfile $env:manbin\addMfdBorders.mmp $tmpFile | select -skip 9 | `
					out-file -enc oem $env:output\zxy 
				# Next two lines not working
				# $sbFile		= get-content $env:output\zxy | out-string
				# $sbFile		= [IO.File]::ReadAllText($($env:output\zxy))
				# $aaa		= $sbFile.replace("*MiramoXML", "")
				# $aaa		= $sbFile -replace '^[^<]+:', ''
				write-host 	"cp $env:output\zxy $tmpFile"
				# $aaa | Out-File -FilePath "$env:output\aaa"
				# $sbFile | Out-File -FilePath "$env:output\sbFile"
				cp $env:output\zxy $tmpFile
				# cp $sbFile $tmpFile
				}
			#--------------------------------------------------------
				# miramoRP `
				# $env:MM_HOME\MiramoPDF-3.0.0\miramoRP `
				#
				# & "C:/ap/miramo\MiramoPDF-3.0.0\miramoRP" `
				#
				#	-showProperties "$sp" -bwc 0 -keep -PDFbookmarks 0 
				write-host "Using this: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "
				# & "$env:mmCommand" `
				#
				#	-user "$env:user" `
				#	-port "$env:port" `
				#	-host "$env:host" `
				#	-password "$env:password" `
				#
				miramo `
					-relaxed `
					-showProperties "$sp" -keep -PDFbookmarks 0 `
					-fontPath "$env:mman\fonts" `
					-hh "N" `
					-jobOptions "$env:jo" `
					-sendEnv Y `
					-Opdf $outputFile $tmpFile `
					2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) `
						{ $_.Exception.Message.trim("`n") } else { $_ } } 
				}
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
		#----------------------------------------------------------------
		}
	}
#--------------------------------------------------------------------------------
$xxyz	= "$mmVersion"
#- write-host	"$xxyz			$docType"
#- write-host	"$mmVersion	$docType"
#- write-host	"$mmVersion"
$env:startTime	= Get-Date -format "yyyy-MMM-dd HH:mm"
# $env:docDate	= Get-Date -format "yy-MM-dd"
$env:docDate	= Get-Date -format "ddMMyy"
$env:docDate2	= Get-Date -UFormat "%a %b %d, %Y %R "
$env:thisYear	= Get-Date -format "yyyy"
write-host	"$env:thisYear"
#--------------------------------------------------------------------------------
$env:pid                = $pid


#--------------------------------------------------------------------------------
mkFormatdefs		# In 'manbin'
#--------------------------------------------------------------------------------
. runmmc

write-host	"$startFolder"
copy-item  "$tmpFile"		-destination	"$env:output\last.mmxml"
#--------------------------------------------------------------------------------
$elapsedTime	= $(get-date) - $startTime
$totalTime	= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
#--------------------------------------------------------------------------------
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
write-host 		"End time: $nowTime"
#--------------------------------------------------------------------------------
if ($lastexitcode -eq 0) {
	write-host	"SUCCESS"
	write-host	"TIME:		$totalTime"
	write-host	"Created: $outputFile"
	$fopFile = "$tmpFile"+".fo" 
	copy-item  "$outputFile"	-destination	"$env:output\last.pdf"
	if ($doctype -eq "refguide" ) {
		copy-item  "$outputFile"	-destination	"$env:output\mmComposerReferenceGuide.pdf"
		}
	# copy-item  "$outputFile"	-destination	"$env:output\$env:documentName"
	# C:\u\miramo\man...ide.mm.mmxml.fo:String) [Copy-Item], ItemNotFoundException
	# refguide.mm.mmxml.mmtmp.p2
	# copy-item  "$fopFile"		-destination	"$env:output\last.fo"
	$openOutput	= Read-Host "Open output file?
	[p	= Open PDF output file]
	[fop	= Open FOP output/input file]
	[i	= Open mmComposer input file]
	[anything else = quit]
 	p | fop | i ?"
	switch ($openOutput) {
			"p"		{start-process acrobat -Argumentlist "/n $outputFile"}
			# "p"		{start-process acrobat -Argumentlist "view=Fit $outputFile"}
			"fop"		{$fopFile = "$tmpFile"+".fo" ; vi "$fopFile" }
			"i"		{vi "$tmpFile" }
			default		{ cd $startFolder ; exit }
		}
} else {
	write-host	"FAILED !!!"
	$openInput	= Read-Host "Open input file?
	[i	= Open mmComposer input file]
	[anything else = quit]
 	i ?"
	switch ($openInput) {
			"i"		{vi "$tmpFile" }
			# default		{ exit }
			default		{ cd $startFolder ; exit }
		}
}
#--------------------------------------------------------------------------------
cd $startFolder
#--------------------------------------------------------------------------------
