#----------------------------------------------------------
#- mangrep
#----------------------------------------------------------
#-
# We can have mangrep [options] 'pattern'
#
# OR
#
# mangrep [options] 'pattern' 'foldernames'
#
# The last, or second last, argument has to be 'pattern'.
# How to tell which one it is, last or second last?
#
# A. We can strip out 'options' and check how many arguments
#    remain.
#
#    This means that search 'pattern' cannot ever correspond
#    with any of the options, '-i', '-v', etc, UNLESS there
#    is just one argument, or '-i', '-v', etc, is the last
#    argument.
#
#    So whatever else we exclude the last argument from being
#    a possible 'option'.
#
#    If there is one non-option argument left after this
#    exclusion, then ...
#
#    We'll grab the last argument and decide later if
#    it's folders or pattern
#
#
#
$mangrepHelp	= @"
mangrep: search folders for a pattern

	mangrep [options] pattern [ foldername(s) ]

	If <foldernames> not included, search all files in current
	folder and all subfolders.

	To search in named folders only, include a comma separated
	list of folders. E.g.

	mangrep happy folder1,folder2,folder3  [no spaces]

	Options:
	-C	Count of lines matching pattern [Not implemented]

	-i	Ignore case for match pattern

	-l	Print only names of files containing pattern
		[Not implemented]

	-v	Print all lines except those that contain
		pattern

	-c1, -c2, -c3, -c3, -c4, -c5, -c10
		Print context lines before and after matched
		line(s)

"@
#----------------------------------------------------------
# Defaults
#----------------------------------------------------------
$countLines	= 0
$context	= 0
$caseSensitive	= $true
$fileNamesOnly	= 0
$notMatch	= $false
$optionCount	= 0
#----------------------------------------------------------
function getOpts	{
     Param([int]$optsCount)

	For ($i=0; $i -lt $optsCount ; $i++)  {
		$optionCount += 1
		switch -casesensitive ($mangrepArgs[$i]) {

			'-C'		{$countLines	= 1}
			'-i'		{$caseSensitive	= $false}
			'-l'		{$fileNamesOnly	= 1}
			'-v'		{$notMatch	= $true}
			'-c'		{$context	= 1}
			'-c0'		{$context	= 0}
			'-c1'		{$context	= 1}
			'-c2'		{$context	= 2}
			'-c3'		{$context	= 3}
			'-c4'		{$context	= 4}
			'-c5'		{$context	= 5}
			'-c10'		{$context	= 10}
			default	{
				# Not an option
				$optionCount -= 1
				}
		}
	}
}
#----------------------------------------------------------
# Copy $args array to $grepArgs
$mangrepArgs	= $args
#----------------------------------------------------------
if ($args.count -gt 0) {
	if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
		write-host $mangrepHelp
		exit
		}
	else	{
		$lastArgument	= $args[$args.count - 1]
		}
	}
else	{
	# Just 'mangrep' typed, with no arguments
	write-host "mangrep: argument required"
	write-host "See: mangrep -h[elp] "
	exit
	}
#----------------------------------------------------------
#- Process arguments
#----------------------------------------------------------
if ($args.count -gt 1) {
	. getOpts $($args.count - 1)
}
#----------------------------------------------------------
$argumentCount	= $args.count - $optionCount	# Arguments excluding options
#----------------------------------------------------------
if ($argumentCount -gt 2) {
	write-host "mangrep: too many arguments"
	write-host "See: mangrep -h[elp] "
}
if ($argumentCount -eq 1) {
		$pattern	= $lastArgument
		$filename	= "."
}
if ($argumentCount -eq 2) {
		$filename	= $lastArgument
		$pattern	= $args[$args.count - 2]
}
#----------------------------------------------------------
$exclude =
		'.vimbackup',
		'output',
		'ymzp97x3nt'
# $LocalAdmins | ? { $exclude -notcontains $_ }
# | select-string -Pattern 'Administrator|daemon' -NotMatch |
# $file.DirectoryName
#----------------------------------------------------------
$excludeFolders		=  @(
				'^\.vimbackup\\',
				'^languages\\',
				'^output\\',
				'^zx9my3v7'
				)
#----------------------------------------------------------
	Get-ChildItem -exclude *.vimbackup* -Recurse $($filename) | Select-String `
			-Context $context `
			-CaseSensitive:$caseSensitive `
			-NotMatch:$notMatch `
			-pattern $pattern | out-string -stream |  `
			select-string `
			-pattern $excludeFolders -NotMatch

exit
#----------------------------------------------------------
write-host 	"Last argument:" $lastArgument
write-host	"Options Count:" $optionCount
write-host	"Argument Count:" $argumentCount
#----------------------------------------------------------
