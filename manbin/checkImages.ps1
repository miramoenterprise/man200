#--------------------------------------------------------------------------------
#- checkImages.ps1
#--------------------------------------------------------------------------------

$env:downsampleImages	= "N"
$env:quality		= "40"	#- Jpeg quality
$env:quality		= "90"	#- Jpeg quality
$env:downsamplePercent	= "40"	#- Percent of original size
$env:downsamplePercent	= "20"	#- Percent of original size
$outputPDFfile		= "$env:output\zz.pdf"

$startTime		= $(get-date)
#--------------------------------------------------------------------------------
miramo -composer sf -Mfile $env:control\newImage.mmp `
			-Opdf "$outputPDFfile" `
			-fontPath "C:\ap\miramo\fonts" `
			-sendEnv Y `
			$env:output\last.mmxml
#--------------------------------------------------------------------------------
$runTime		= $(get-date) - $startTime
$runTime		= $runTime.TotalSeconds
$runTime		= [math]::Round($runTime,1)
#--------------------------------------------------------------------------------
$size1			= (Get-Item $outputPDFfile).length/1000
qpdf --object-streams=generate $outputPDFfile $env:output/qpdfout.pdf
$size2			= (Get-Item $env:output/qpdfout.pdf).length/1000

#--------------------------------------------------------------------------------
$a = sizeOfImages	"$outputPDFfile"
$q = sizeOfImages	"$env:output/qpdfout.pdf"
#--------------------------------------------------------------------------------
write-host		"--------------------------------------------------------"
write-host		"inputFile name:		= $env:output\last.mmxml"
write-host		"downsampleImages:		= $env:downsampleImages (DS percent = $env:downsamplePercent) "
write-host		"output file:			= $outputPDFfile"
write-host		"Total time:			$runTime seconds"
write-host		"Size:				$size1 ko	($outputPDFfile)"
write-host		"Images size:			$a ko	($outputPDFfile)"
write-host		"Size (after Qpdf):		$size2 ko	($env:output/qpdfout.pdf)"
write-host		"Images size (after Qpdf):	$q ko	($env:output/qpdfout.pdf)"
write-host		"--------------------------------------------------------"
#--------------------------------------------------------------------------------
#
