$mmArgs	= @()		# Define $mmArgs as (empty) array
$pdfName = ""
# -- look for Image.element.start etc 
# -- Check params
if ($args.count -lt 1 ) {
	write-host "runtest: filename required"
	exit
}
$inputName = $args[$args.count - 1]
If ( [System.IO.Path]::GetExtension($inputName).length -eq 0 ) {
	$inputName += ".xml"
}
If ( ! (Test-Path $inputName ) ) {
	write-host "miramo: $inputName does not exist"
	exit
}
# -- build mmArgs
For ($i=0; $i -lt $args.count - 1 ; $i++)  {
	switch -casesensitive ($args[$i]) {
		'-Opdf' { $pdfName = $args[$i+1] }
	}
	$mmArgs += $args[$i]
}

$mmArgs += "-fontPath"
$mmArgs += "\\fox\u\miramo\man\man200\fonts"

if ($pdfName.length -eq 0 ) {
	$pdfName = "./windows/"
	$pdfName += [System.IO.Path]::GetFileNameWithoutExtension($inputName)
	$pdfName += ".pdf"
	$mmArgs += "-Opdf"
	$mmArgs += $pdfName
	}
write-host "miramo $mmArgs -keep $inputName"
$env:images="\\fox\u\miramo\man\images"
$env:inline="\\fox\u\miramo\man\man200\refguide\inline"
$env:mfd="\\fox\u\miramo\man\man200\refguide\mfd"
$env:appendices="\\fox\u\miramo\man\man200\refguide\appendices"
miramo $mmArgs -keep $inputName
if ($LastExitCode -eq 0 ) {
ar $pdfName
}