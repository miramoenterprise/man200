@--------------------------------------------------------------------------------
@- addMfdBorders.mmp
@--------------------------------------------------------------------------------
<@skipStart>
MiramoDesigner: Print all master pages to PDF
Foreground:
fillColor="mtfFill" fillTint="50%" fillOpacity="80%"
penWidth="1.5pt" penColor="mtfPen" penOpacity="100"
penTint="100%"
penStyle="dotted"

Background:
fillColor="Black" fillTint="5%" fillOpacity="80%"
penWidth="1.5pt" penColor="Blue" penOpacity="100"
penTint="100%"
penStyle="dotted"

Scenario

Whenever MiramoXML has a TextFrame TextFrameDef within a PageDef:

	Outpt a border and fill


<@skipEnd>
@--------------------------------------------------------------------------------
<#def> showTFborders	Y
@--------------------------------------------------------------------------------
<@xmacro> foregroundTextFrame {
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
	<#def> fgFillColor	{<#fillColor>}
	<#def> fgFillRed	{<#fillRed>}
	<#def> fgFillGreen	{<#fillGreen>}
	<#def> fgFillBlue	{<#fillBlue>}
	<#def> fgFillTint	{<#fillTint>}
	<#def> fgFillOpacity	{<#fillOpacity>}
	<#def> fgPenStyle	{<#penStyle>}
	<#def> fgPenWidth	{<#penWidth>}
	<#def> fgPenColor	{<#penColor>}
	<#def> fgPenRed		{<#penRed>}
	<#def> fgPenGreen	{<#penGreen>}
	<#def> fgPenBlue	{<#penBlue>}
	<#def> fgPenOpacity	{<#penOpacity>}
}{}
@--------------------------------------------------------------------------------
<@xmacro> backgroundTextFrame {
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
	<#def> bgFillColor	{<#fillColor>}
	<#def> bgFillRed	{<#fillRed>}
	<#def> bgFillGreen	{<#fillGreen>}
	<#def> bgFillBlue	{<#fillBlue>}
	<#def> bgFillTint	{<#fillTint>}
	<#def> bgFillOpacity	{<#fillOpacity>}
	<#def> bgPenStyle	{<#penStyle>}
	<#def> bgPenWidth	{<#penWidth>}
	<#def> bgPenColor	{<#penColor>}
	<#def> bgPenRed		{<#penRed>}
	<#def> bgPenGreen	{<#penGreen>}
	<#def> bgPenBlue	{<#penBlue>}
	<#def> bgPenOpacity	{<#penOpacity>}
}{}
@--------------------------------------------------------------------------------
<@xmacro> - mmShowBorders {
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
}{$mmShowBorders}
@--------------------------------------------------------------------------------
<mmShowBorders>
	@- <!-- In $mmHome/config/. mmShowBorders.xml -->
	@- <!-- C:\ap\Miramo\MiramoPDF-3.0.0\config -->
	@- <!-- <installDir>/config/mmShowBorders.xml -->
	@- <!-- Default styles for -showProperties B -->
	@- <!-- -sbConfig  <filename> -->
	@- <ColorDef colorDef="~mmFgTFfill" red="95%" green="95%" blue="87%" />
	@- <ColorDef colorDef="~mmBgTFfill" red="94%" green="94%" blue="94%" />
	<foregroundTextFrame
		fillColor="mmFgTFfill"
		fillRed="95%"
		fillGreen="95%"
		fillBlue="87%"
		fillTint="100%"
		fillOpacity="80%"
		penStyle="dotted"
		penWidth="0.5pt"
		penColor="mmFgTFpen"
		penRed="50%"
		penGreen="0%"
		penBlue="0%"
		penTint="100%"
		penOpacity="80%"
		/>
	<backgroundTextFrame
		fillColor="mmBgTFfill"
		fillRed="94%"
		fillGreen="94%"
		fillBlue="94%"
		fillTint="100"
		fillOpacity="80%"
		penStyle="dotted"
		penWidth="0.5pt"
		penColor="mmBgTFpen"
		penRed="36%"
		penGreen="36%"
		penBlue="0%"
		penTint="100%"
		penOpacity="80%"
		/>
</mmShowBorders>
@--------------------------------------------------------------------------------
<@xmacro> ColorDef {
	<#ifndef> cdCount	0
	<#def> cdCount		@eval(<#cdCount> + 1)
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
	@if(<#cdCount> = 1) {
		<$0 colorDef="<#fgFillColor>"		@- Foreground fill color
			red="<#fgFillRed>" green="<#fgFillGreen>" blue="<#fgFillBlue>" />
		<$0 colorDef="<#bgFillColor>"		@- Background fill color
			red="<#bgFillRed>" green="<#bgFillGreen>" blue="<#bgFillBlue>" />
		@----------------------------------------------------------------
		<$0 colorDef="<#fgPenColor>"	@- Foreground pen color"
			red="<#fgPenRed>" green="<#fgPenGreen>" blue="<#fgPenBlue>" />
		<$0 colorDef="<#bgPenColor>"	@- Background pen color 
			red="<#bgPenRed>" green="<#bgPenGreen>" blue="<#bgPenBlue>" />
		}
	<$0 @xputvals()  />
}{}
@--------------------------------------------------------------------------------
<@xmacro> - TextFrame {
	<#ifndef> tfCount	0
	<#def> tfCount		@eval(<#tfCount> + 1)
	@------------------------------------------------------------------------
	<#def> columns		1
	<#def> columnGap	0
	<#def> L		{}
	<#def> T		{}
	<#def> W		{}
	<#def> H		{}
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
	@if(@len(<#L>)) {<#def> left	<#L>}
	@if(@len(<#T>)) {<#def> top	<#T>}
	@if(@len(<#W>)) {<#def> width	<#W>}
	@if(@len(<#H>)) {<#def> height	<#H>}
	
<@skipStart>
<@skipEnd>
}{
	@------------------------------------------------------------------------
	@- The following rectangles are for b/g text frame fill *underlays*
	@- If the text frame already has a fill color, these *underlays* will be
	@- obscured. They are drawn before the TextFrame
	@------------------------------------------------------------------------
	@if(<#columns> > 100) {
		<#def> colWidth		@eval((<#width> - (<#columnGap> * (<#columns> - 1))) / <#columns>)
		@- <@write> error {Num columns: <#columns> gap: <#columnGap>	width: <#width>	colWidth: <#colWidth>	page: <#pageDef>}
		}
	@if(<#type> = foreground) {
		@- Get the width of each column, in case it's a multi-column
		@- foreground text frame
		<#def> colWidth		@eval((<#width> - (<#columnGap> * (<#columns> - 1))) / <#columns>)
		@- <@write> error {Num columns: <#columns> gap: <#columnGap>	width: <#width>	colWidth: <#colWidth>	page: <#pageDef>}
		@for(col = 1 to <#columns>) {
			@- <#def> colLeft[$$col]	@eval(<#left> + ((<#columnGap> + <#colWidth>) * (<#columns> - 1)))
			<#def> colLeft[$$col]	@eval(<#left> + ((<#columnGap> + <#colWidth>) * ($$col - 1)))
		@----------------------------------------------------------------
		@- <!-- Drawing bg fill for foreground text flow -->
		<Rectangle
			left="$colLeft[$$col]"
			top="<#top>"
			width="<#colWidth>"
			height="<#height>"
			fillColor="<#fgFillColor>" fillTint="<#fgFillTint>" fillOpacity="<#fgFillOpacity>"
			/>
			}
		}
	@if(<#type> = background ) {
		@- <!-- Drawing bg fill for background text frame -->
		<Rectangle
			left="<#left>"
			top="<#top>"
			width="<#width>"
			height="<#height>"
			fillColor="<#bgFillColor>" fillTint="<#bgFillTint>" fillOpacity="<#bgFillOpacity>"
			/>
		}
	@------------------------------------------------------------------------
	@- Output the TextFrame
	@------------------------------------------------------------------------
<$0
	@------------------------------------------------------------------------
	@xputvals()
		>$TextFrame</$0> 
	@------------------------------------------------------------------------
	@- Output the border
	@------------------------------------------------------------------------
	@- fgPenWidth
	@- bgPenWidth
	@- How to include popups?
	@if(<#type> = foreground) {
		@----------------------------------------------------------------
		@- <!-- Drawing border for foreground text flow -->
		@for(col = 1 to <#columns>) {
<@skipStart>
	<#def> fgFillColor	{<#fillColor>}
	<#def> fgFillRed	{<#fillRed>}
	<#def> fgFillGreen	{<#fillGreen>}
	<#def> fgFillBlue	{<#fillBlue>}
	<#def> fgFillTint	{<#fillTint>}
	<#def> fgFillOpacity	{<#fillOpacity>}
	<#def> fgPenStyle	{<#penStyle>}
	<#def> fgPenWidth	{<#penWidth>}
	<#def> fgPenColor	{<#penColor>}
	<#def> fgPenRed		{<#penRed>}
	<#def> fgPenGreen	{<#penGreen>}
<@skipEnd>
		<Rectangle
			left="$colLeft[$$col]"
			top="<#top>"
			width="<#colWidth>"
			height="<#height>"
			penWidth="<#fgPenWidth>"
			penColor="<#fgPenColor>"
			penStyle="<#fgPenStyle>"
			fillColor="none"
			/>
			}
		}
	@- 	<$0 colorDef="~mmFgTFpen"  red="36%" green="36%" blue="0%" />
	@- <$0 colorDef="mmBgTFpen"  red="50%" green="0%" blue="0%" />
	@if(<#type> = background) {
		@----------------------------------------------------------------
		@- <!-- Drawing border for background text flow -->
		<Rectangle
			left="<#left>"
			top="<#top>"
			width="<#width>"
			height="<#height>"
			penWidth="<#fgPenWidth>"
			penColor="<#fgPenColor>"
			penStyle="<#fgPenStyle>"
			fillColor="none"
			/>
		}
}
@--------------------------------------------------------------------------------
<@xmacro> - PageDef {
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
}{
<$0 @xputvals() >$PageDef
	<mmDraw>
		<TextLine
			textSize="8pt"
			textColor="Black"
			left="6mm"
			top="3.5mm"
			>pageDef="<#pageDef>"</TextLine>
	</mmDraw>
</$0> 
}
@--------------------------------------------------------------------------------
