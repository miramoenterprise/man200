#-----------------------------------------------------------
#- edit
#- RD, Dec 2012
#
# edit <filename> | -help
#-----------------------------------------------------------
$editHelp	= @"
edit: edit one of a set of special note files

	edit filename | -help | -h

	e.g.
	edit manlogic
"@
#-----------------------------------------------------------
if ($args[0] -eq  "-help" -or $args[0] -eq  "-h" ) {
	write-host $editHelp
	exit
}
#-----------------------------------------------------------
function editSpecialFile {
	$specialFile = $args[0]
	$lockfile = �$specialFile.lck.$env:username�
	$lockstatus = 0
	If (Test-Path $lockfile) {
		# File is being edited by someone else
		$userName = [System.IO.Path]::GetExtension($lockfile)
		$userName = $userName.substring(1)
		write-host "File $specialFile is being edited by $userName"
		exit
		}
	$PID | Out-File $lockfile
	# Run command
	if ($env:UserName -eq "Rick" ) {
		# start-process vim $specialFile
		vim $specialFile
		Remove-Item $lockfile �Force
		}
	else	{
		start-process "$env:defaultEditor" -ArgumentList $specialFile
		Remove-Item $lockfile �Force
	}
}
#-----------------------------------------------------------
$filename	=	$args[0]
$filenames	=	"manlogic",
			"xyzx33"
if ($filenames -contains $filename) {
	#- if ($filename -eq  "psprofile" -or $filename -eq "psconfig" ) {
	if ($filename -eq  "manlogic" ) {
		editSpecialFile "$env:MANBIN\manlogic.ps1"
		}
	else {
		Write-host "edit: X no special file '"$filename"'"
		#- editSpecialFile "$env:UDATA/$filename"
		}
}
else {
	Write-host "edit: no special file '"$filename"'"
}
#-----------------------------------------------------------
