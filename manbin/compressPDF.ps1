#--------------------------------------------------------------------------------
# compressPDF.ps1
#--------------------------------------------------------------------------------
#
#--------------------------------------------------------------------------------
$pdfFile	= $args[0]
$extension	= "c"
#--------------------------------------------------------------------------------

$argCount	= $args.count

if (($args.count -eq 0) -or (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
	write-host	"Usage: compressPDF filename.pdf"
	write-host	"Creates compressed output file: filename.pdfu"
	}
else {
	write-host	"Compressing $pdfFile  TO: $pdfFile$extension"
	qpdf --object-streams=generate --optimize-images "$pdfFile" "$pdfFile$extension"
	}
#--------------------------------------------------------------------------------
