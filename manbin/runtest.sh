#!/bin/sh
#--------------------------------------------------------------------------------
#- rundoc
#--------------------------------------------------------------------------------

warn="3"
# TODO: map windows paths such as c:\u\miramo\man\man200 (as defined by $env:man in .psprofile.ps1)
# to Linux paths before processing MiramoXML file
export mmFontPath="/u/miramo/man/man200/fonts"
export man="/u/miramo/man"
export mman="${man}/man200"
export images="${man}/images"
export IMG="${man}/images"
export mfd="${mman}/refguide/mfd"
export inline="${mman}/refguide/inline"
export output="${mman}/output"
export tests="${mman}/tests"
export tests="${mman}/tests"
export appendices="${mman}/refguide/appendices"

secs_to_human() {
	echo "Total time: $(( ${1} / 3600 )) hours  $(( (${1} / 60) % 60 )) minutes  $(( ${1} % 60 )) seconds"
}

if [ $# -eq 0 ]
then
	echo "Usage: rundoc [ -w int ] filename"
	echo "<filename> is a MiramoXML file in: ${output}"
	echo "(Usually ending in: '.mm.mmxml', when produced by 'runx.ps1')"
	echo "-w int is warn level ('int' is in range 0 - 4)"
	echo ""
	exit
fi

while [ "$#" -gt 0 ]; do
	case $1 in
		-w) warn="$2"; shift 1 ;;
		# -u|--uglify) uglify=1;;
		*) infile="$1" ;;
	esac
	shift
done

f=`basename ${infile} .xml`
start=$(date +%s)
outfile="${output}/linux/$(hostname)-${f}.pdf"
#infile="${output}/${infile}"

echo $infile
echo $outfile
echo "warn = $warn"
echo "Creating file $outfile"
# time miramo -warn $warn -fontPath "/home/rick/fonts" -Opdf $outfile $infile
# time miramo -keep -warn $warn -Opdf $outfile $infile
echo miramo -keep -fontPath /u/miramo/man/man200/fonts -Opdf $outfile $infile
time miramo -keep -fontPath /u/miramo/man/man200/fonts -Opdf $outfile $infile
secs_to_human "$(($(date +%s) - ${start}))"
echo $outfile
