#--------------------------------------------------------------------------------
#- sort-xnames.ps1
#--------------------------------------------------------------------------------

$content = Get-Content("$env:mmtmp/xcnames")
$content = $content |? {$_.trim() -ne "" }
$content | sort-object -Descending | get-unique | out-file -enc oem "$env:mmtmp/xcnames.sorted"
