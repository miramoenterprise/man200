#--------------------------------------------------------------------------------
#- removeXMLdeclaration.ps1
#--------------------------------------------------------------------------------
#- One arg:		"$infile"
#- Output file:		"$infile"+"x"

$suffix		= "x"

$inFileName	= $args[0]
$outFileName	= "$inFileName"+$suffix

$regex		= ("<[?]xml[^?]+[?]>")
# replace regex with empty string and set content back on $outFileName
(get-content $inFileName) -replace "$regex","" | set-content -enc OEM $outFileName

#$file		= get-content $inFileName | Out-String
##$file		= $file.replace(, "$regex")
#$file
#$file | out-file -enc OEM $outFileName


exit
# <\?xml.*\?>
# <?xml version="1.0" encoding="ISO-8859-1"?>
# "\\<\\?xml(.+?)\\?\\>", ""
