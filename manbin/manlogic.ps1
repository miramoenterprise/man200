#--------------------------------------------------------------------------------
#- manlogic.ps1
#--------------------------------------------------------------------------------

$manLogic	=@"
-------------------------------------------------------

'element' attribute:

	element="elementName|elementName|...."

ON:	<pdp>, <exampleBlock>

The 'element' attribute specifies which elements to include the object.
E.g. A long property description paragraph, <pdp>, or an example,
<exampleBlock>, may be included in the description of the <Font> element,
but is not wanted in the <P> element. Instead the <P> element may contain
a cross-reference to the longer text in the <Font> element description.

If 'element' is not included or is set to "all" the object is included
in every element. If 'element' is set to an element name or a series of
of bar, |, separated element names, the object is included ONLY in the
listed element(s).

-------------------------------------------------------

'condition' attribute:

ON:	<property>, <pdp>, <exampleBlock>, <elementNote>
	<propertyValueKeyList>, <vk>

	condition="conditionName"

"skip"		Never include. (Useful temporary omission of object)
"fmcGuide"	Include object ONLY if the documentation is for 'fmComposer'
"mmcGuide"	Include object ONLY if the documentation is for 'mmComposer'

The logic depends on the setting of '<#guide>', which can be either
{mmComposer} or {fmComposer}. <#guide> is defined in ${control}/metaDefs.mmp


-------------------------------------------------------
This text is in the file $env:manbin/manlogic.ps1 ('edit manlogic')
-------------------------------------------------------
"@
$manLogic
