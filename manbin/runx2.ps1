#--------------------------------------------------------------------------------
#- runx.ps1
#--------------------------------------------------------------------------------
#
#  e.g. runx inline
#  e.g. runx drawguide
#
#--------------------------------------------------------------------------------
#- parameters must come first ???
#--------------------------------------------------------------------------------
param	(
	[switch] $h		= $false ,	# Help message
	[switch] $help		= $false ,	# Help message
	[string] $sp		= "N" ,		# -sp NYMVW (show properties)
	[string] $composer	= "sf" ,	# miramo -composer <val>
	[string] $bwc		= "0" ,		# Backwards compatibility flag
	[string] $sendEnv	= "Y" ,		# Send environment variables
	[switch] $l		= $false ,	# Landscape output
	[switch] $sfg		= $false ,	# Show grid on main text flow (only if ${QA} set
	[switch] $mmp		= $false ,	# If -mmp, only process using mmpp
	[switch] $relaxed	= $true ,	# Use -relaxed
	[string] $fp	= "C:\ap\miramo\fonts",	# Default font path
	[parameter(Position=0,
		ValueFromRemainingArguments=$true)][array]$lastArgs,
	[switch]$dummy
		)
#--------------------------------------------------------------------------------
$runxHelp      = @"
runx [options] doctype
	-h, -help	This message

	-sp <value>	Show properties. Values can be: Y[PFIX]|N[PFIX]
				-sp Y		Show all properties
				-sp N		Don't show properties
				-sp N[PFIX]	Show no properties *except* PFIX
				-sp Y[PFIX]	Show PFIX properties
					P = Paragraph properties
					F = Font properties
					I = Image properties

	-bwc 0|1	Backwards compatibility flag [for what???]

	-mmp   		Only process with mmpp

	-l   		Switch to landscape output

	-sfg 		Show grid on main text flow for QA docs (only if `${QA} set)
				`${svg} used in `${control}/metaData.mmp
				`${QA} used in `${control}/pageLayouts.mmp

	-relaxed 	Show grid on main text flow (only if `${QA} set
e.g. 
	runx inline | drawguide | mantests | refguide | lists |
		language | color |

"@
#--------------------------------------------------------------------------------
if($help -or $h)	{ $runxHelp ; exit }
#--------------------------------------------------------------------------------
			$env:setLandscape	= "0" 
if($l) 			{write-host	"landscape" ; $env:setLandscape	= "1" }
#--------------------------------------------------------------------------------
if($relaxed)		{$xrelaxed	= "-relaxed" }
#--------------------------------------------------------------------------------
			$env:QA = "N" ; $env:sfg = "N" 
if($sfg)		{ $env:QA = "Y" ; $env:sfg = "Y" }
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
$startFolder		= pwd
$outFolder		= "$env:output" ; cd $outFolder
#--------------------------------------------------------------------------------
$startTime		= $(get-date)
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
$nowMonthNameTime	= $(Get-Date).ToString('dd MMM yyyy HH:mm:ss')
write-host 		"Start time: $nowTime"
#--------------------------------------------------------------------------------
#- Process arguments
#--------------------------------------------------------------------------------
$docType		= $lastArgs
#--------------------------------------------------------------------------------
write-host	"runx: $docType"
#--------------------------------------------------------------------------------
switch -casesensitive ($docType) {
	'inline'	{$inFile	= "$env:inline\inline.inc"			}
	'ishort'	{$inFile	= "$env:inline\ishort.inc"			}
	'fdefs'		{$inFile	= "$env:mfd\mfd.inc"				}
	'mfd'		{$inFile	= "$env:mfd\mfd.inc"				}
	'mshort'	{$inFile	= "$env:mfd\mshort.inc"				}
	'mfds'		{$inFile	= "$env:mfd\mfd.inc"				}
	'drawguide'	{$inFile	= "$env:drawguide\drawguide.inc"		
				$env:documentName = "mmDrawGuide.pdf"
				}
	'xelements'	{$inFile	= "$env:xelements\xelements.inc"		}
	'refguide'	{$inFile	= "$env:refguide\refguide.inc"			}
	'rshort'	{$inFile	= "$env:refguide\rshort.inc"			}
	'qa'		{$inFile	= "$env:mman\qa\qa.inc"			
			$env:QA		= "Y"
											}
	'short'		{$inFile	= "$env:refguide\short.inc"			}
	'long1'		{$inFile	= "$env:refguide\long1.inc"			}
	'long2'		{$inFile	= "$env:refguide\long2.inc"			}
	'long3'		{$inFile	= "$env:refguide\long3.inc"			}
	'long4'		{$inFile	= "$env:refguide\long4.inc"			}
	'long5'		{$inFile	= "$env:refguide\long5.inc"			}
	'long6'		{$inFile	= "$env:refguide\long6.inc"			}
	'color'		{$inFile	= "$env:appendices\color\color.inc"		}
	'caps'		{$inFile	= "$env:appendices\capitalization\capitalization.inc" }
	'language'	{$inFile	= "$env:refguide\language\language.inc"		}
	'running'	{$inFile	= "$env:refguide\running\running.inc"		}
	'types'		{$inFile	= "$env:refguide\propertyTypes\propertyTypes.inc"	}
	'ix'		{$inFile	= "$env:refguide\inline\ix.inc"			}
	'textframedef'	{$inFile	= "$env:refguide\mfd\textframedef.inc"		}
	'aframe'	{$inFile	= "$env:refguide\inline\aframe.inc"		}
	'paradef'	{$inFile	= "$env:refguide\mfd\paradef.inc"		}
	'thai'		{$inFile	= "$env:refguide\language\thai\thai.inc"	}
	'fonts'		{$inFile	= "$env:appendices\fonts\fonts.inc"		}
	'appendices'	{$inFile	= "$env:appendices\appendices.inc"		}
	'autonumbering'	{$inFile	= "$env:appendices\autonumbering\autonumbering.inc" }
	'entities'	{$inFile	= "$env:appendices\characterEntities\characterEntities.inc"		}
	'lastPage'	{$inFile	= "$env:mman\tests\lastPage\lastPageTestInput.inc"
			$env:documentFooterTitle = "&#x24;mman/tests/lastPage (runx -l lastPage)&emsp;&emsp;&emsp;"
			$env:documentFooterTitle = "$env:mmVersion &emsp; $env:documentFooterTitle"
			$env:documentFooterTitle = "$nowMonthNameTime &emsp; $env:documentFooterTitle"
						}
	'textlanguage'	{$inFile	= "$env:appendices\textlanguage\textlanguage.inc" }
	'opos'		{
$env:documentFooterTitle	= "<Font capitalization='smallCaps' >Rules and Tests</Font> (<fB>MiramoPDF v $env:releaseVersion</fB> &emdash; $startTime)"
$inFile	= "$env:mman/mmcTests/objectVerticalPositioning\positioning.inc"
}
	'lists'		{$inFile	= "$env:refguide\lists\lists.inc"		}
	'mantests'	{$inFile	= "$env:mantests\tests.inc" 
			 $outFolder	= "$env:mantests" 				}
	'svgtest1'	{$inFile	= "$env:svgtests\svg1" 
			 $outFolder	= "$env:svgtests\svg1" 				}
	#------------------------------------------------------------------------
	'fmcrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}
	'fmrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}
	'fmcaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmcAddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'mmc2'		{$inFile		= "$env:mmc2\mmc2.inc" 
			 $env:mkmmc2		= "Y" 					}
	#------------------------------------------------------------------------
	default		{
			write-host	"Unknown document type: $docType Try runx -h"
			exit
			}
	}
#--------------------------------------------------------------------------------
function runmmc {
	#------------------------------------------------------------------------
	#- Run mmComposer
	#------------------------------------------------------------------------
	pwd
	$tmpFile		= "$env:output\$docType"+".$ctag" + ".mmxml"
	$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
	$outputFile		= "$outFolder\$docType"+ ".pdf"
	$outputFile2		= "$outFolder\$docType"+".$ctag" + ".pdf"
	$outputFile3		= "$outFolder\mmComposerReferenceGuide" + ".pdf"
	$env:composerText	= "$ctag" + "Composer"
	#----------------------------------------------------------------
	[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
	#----------------------------------------------------------------
	mmpp -Mfile $env:control\control.mmp $inFile | out-file  $tmpFile -enc utf8
	"</MiramoXML>" | out-file -append -enc "utf8" $tmpFile
	if($mmp -eq 1) {
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
		#----------------------------------------------------------------
		write-host	"mmpp only: no PDF output produced !!! "
		$elapsedTime	= $(get-date) - $startTime
		$totalTime	= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
		write-host	"TIME:		$totalTime"
		cd $startFolder
		exit
		}
	$nowTime	= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
	write-host 	"Starting mmComposer [$env:composerTag]: $nowTime"
	miramo -composer "$composer" -fontPath "$fp" "$xrelaxed" -showProperties "$sp" `
	-bwc "$bwc" -keep -PDFbookmarks 0 -sendEnv "$sendEnv" -Opdf $outputFile $tmpFile `
		2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) `
			{ $_.Exception.Message.trim("`n") } else { $_ } } 
	#----------------------------------------------------------------
	[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
	#----------------------------------------------------------------
}
#--------------------------------------------------------------------------------
$env:startTime	= Get-Date -format "yyyy-MMM-dd HH:mm"
# $env:docDate	= Get-Date -format "yy-MM-dd"
$env:docDate	= Get-Date -format "ddMMyy"
$env:docDate2	= Get-Date -UFormat "%a %b %d, %Y %R "
$env:thisYear	= Get-Date -format "yyyy"
write-host	"$env:thisYear"
#--------------------------------------------------------------------------------
$env:pid                = $pid

#--------------------------------------------------------------------------------
mkFormatdefs		# In 'manbin'
#--------------------------------------------------------------------------------
. runmmc

write-host	"$startFolder"
copy-item	"$tmpFile"	-destination	"$env:output\last.mmxml"
#--------------------------------------------------------------------------------
$elapsedTime	= $(get-date) - $startTime
$totalTime	= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
#--------------------------------------------------------------------------------
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
write-host 		"End time: $nowTime"
#--------------------------------------------------------------------------------
if ($lastexitcode -eq 0) {
	write-host	"SUCCESS"
	write-host	"TIME:		$totalTime"
	write-host	"Created: $outputFile"
	$fopFile = "$tmpFile"+".fo" 
	copy-item  "$outputFile"	-destination	"$env:output\last.pdf"
	if ($doctype -eq "refguide" ) {
		copy-item  "$outputFile"	-destination	"$env:output\mmComposerReferenceGuide.pdf"
		}
	# copy-item  "$outputFile"	-destination	"$env:output\$env:documentName"
	# C:\u\miramo\man...ide.mm.mmxml.fo:String) [Copy-Item], ItemNotFoundException
	# refguide.mm.mmxml.mmtmp.p2
	# copy-item  "$fopFile"		-destination	"$env:output\last.fo"
	$openOutput	= Read-Host "Open output file?
	[p	= Open PDF output file]
	[fop	= Open FOP output/input file]
	[i	= Open mmComposer input file]
	[anything else = quit]
 	p | fop | i ?"
	switch ($openOutput) {
			"p"		{start-process acrobat -Argumentlist "/n $outputFile"}
			# "p"		{start-process acrobat -Argumentlist "view=Fit $outputFile"}
			"fop"		{$fopFile = "$tmpFile"+".fo" ; vi "$fopFile" }
			"i"		{vi "$tmpFile" }
			default		{ cd $startFolder ; exit }
		}
} else {
	write-host	"FAILED !!!"
	$openInput	= Read-Host "Open input file?
	[i	= Open mmComposer input file]
	[anything else = quit]
 	i ?"
	switch ($openInput) {
			"i"		{vi "$tmpFile" }
			# default		{ exit }
			default		{ cd $startFolder ; exit }
		}
}
#--------------------------------------------------------------------------------
cd $startFolder
#--------------------------------------------------------------------------------
