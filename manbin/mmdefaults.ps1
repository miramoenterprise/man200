#--------------------------------------------------------------------------------
# mmdefaults.ps1
#--------------------------------------------------------------------------------
#
"Default formats are now in C:/u/miramo/xdev/src/mmComposer/config/defaultFormatDefinitions.in - exiting"
exit
$defaultsDir	= "$env:MM_HOME/config"
$defaultsFile	= "defaultFormatDefinitions.xml"
$copyDateTime	= Get-Date -format "dd-MM-yy"
$copyDateTime	= Get-Date -format "yyyy-MMM-dd-HH.mm"
$outfile1 	= "C:/u/miramo/xdev/src/mmComposer/config/$defaultsFile"
$outfile2	= "$env:mman/defaultformats/$defaultsFile"
$outfile2	= "$outfile2"+".$copyDateTime"
#
#
#  editSpecialFile "c:/u/miramo/xdev/src/mmComposer/schema/miramo.xml"
# editSpecialFile "c:/ap/Miramo/config/$copyFilename"
Write-Host "Copying" "$outfile1" "$defaultsDir/$defaultsFile"
cp "$outfile1" "$defaultsDir/$defaultsFile"
vi "$defaultsDir/$defaultsFile"

$copyFile	= Read-Host "Copy file?
Copy file:
$defaultsDir/$defaultsFile
to:
$outfile1
$outfile2

[y	= Yes]
[n	= No]
[anything else = quit] ? "

switch ($copyFile) {
                "y"             {
			cp "$defaultsDir/$defaultsFile" -destination "$outfile1"
			cp "$defaultsDir/$defaultsFile" -destination "$outfile2"
			# copy-item  "$env:output\mmComposerRefGuide.pdf" -destination    "$env:mman\pdfout"
				}
                default         { exit }
		}

exit
