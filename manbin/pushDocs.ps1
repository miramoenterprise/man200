#--------------------------------------------------------------------------------
# pushDocs.ps1
#--------------------------------------------------------------------------------
#
#--------------------------------------------------------------------------------
param (
	[switch]$help,
	[switch]$h,
	[switch]$a,
	[switch]$archive,
	[switch]$s,
	[switch]$shrink,
	[switch]$p,
	[switch]$push,
	[string]$optionalMFD
	)
#--------------------------------------------------------------------------------
$version	= "2.0"
$version	= "3.0"
$sourceFile	= "mmComposerReferenceGuide.pdf"	#- File in ${output}
$outDir		= "C:/u/miramo/man/miramoDocs/"
$tmpPDFfile	= "refguide.shrunk.pdf"
#--------------------------------------------------------------------------------
# E.g. -22sep2020
$dateSuffix	= get-date -format "-ddMMMyyyy"
$dateSuffix	= $dateSuffix.tolower()
$sourceBasename	= $sourceFile.Split(".")
$sourceBasename = $sourceBasename[0]

$destFile1	= "$sourceBasename"+"$version"+".pdf"
$destPath1	= "$outDir"+"$destFile1"
#--------------------------------------------------------------------------------
# $argCount	= $args.count
# write-host	"$argCount"
#--------------------------------------------------------------------------------
# if ($help -or $h -or ($args.count -eq 0)) {
if ($help -or $h) {
	write-host "
Usage: pusdocs -h[elp] | -s[hrink] | -a[rchive] | -p[ush]
	(One option must be supplied, else nothing happens)

	-h[elp]		Show this help message


	-s[hrink]	Run the following command to shrink PDF file:
				qpdf --object-streams=generate
					--optimize-images <in>.pdf <out>.pdf

			In folder: $env:output
			<in>	= $sourceFile
			<out>	= $tmpPDFfile



	-a[rchive]	Same as '-shrink' PLUS:
			  Archive file:	$tmpPDFfile
			   From folder:	$env:output

			     To folder:	$env:mman\pdfout
			   As filename
			(date stamped):	$sourceFile$dateSuffix


	-p[ush]		Same as '-archive' PLUS:

			     Copy file:	$tmpPDFfile
			   From folder:	$env:output

			     To folder:	$outDir
			   As filename:	$destFile1

			This overwrites the previous $destFile1

			Then run: syncmmdocs.cmd (in ubin folder)
			to update git distribution repository.
"
	exit
}
#--------------------------------------------------------------------------------
$iFile		= get-item $env:output/$sourceFile
$oFile		= "$env:output/$tmpPDFfile"
#--------------------------------------------------------------------------------
function shrink() {
	write-host	"Shrinking PDF file: takes a minute or two ... "
	write-host	"	-------------------------------------------------"
	write-host	"	Input file name: $iFile"
	write-host	"	Input file size:	" $iFile.Length
	write-host	"	-------------------------------------------------"
	write-host	"	qpdf: "
#	qpdf --object-streams=generate `
#		--optimize-images $iFile `
#			$oFile
	qpdf --object-streams=generate `
		--optimize-images $iFile `
			$oFile
	write-host	"	-------------------------------------------------"
	$oFile		= get-item $env:output/$tmpPDFfile
	write-host	"	Output size:		" $oFile.Length
	$reduction	= $iFile.Length - $oFile.Length
	write-host	"	Size reduction:		" $reduction
	$reduction	= 1 - $oFile.Length / $iFile.Length
	$reduction	= [math]::Round($reduction * 100, 2)
	write-host	"	Percent reduction:	" $reduction"%"
	write-host	"	-------------------------------------------------"

# 'Size'=[math]::Round($Disk.Size / 1GB,2)
}
#--------------------------------------------------------------------------------
function archive() {
	write-host "	Copying [archive]:
		$oFile
	to:
		$env:mman\pdfout\$destFile1$dateSuffix
		"
	cp "$oFile" "$env:mman\pdfout\$destFile1$dateSuffix"

}
#--------------------------------------------------------------------------------
function push() {
	$destFile	= "$outDir$destFile1"
	write-host "	Copying [push]:
		$oFile
	to:
		$outDir$destFile1
		"
	#- Check if output file writable
	Try {
		[io.file]::OpenWrite($destFile).close()
		}
	Catch {
		write-host "	-------------------------------------------------"
		write-host "	FAILED !!!!!!!!!!!!!!!!!! "
		write-host "	-------------------------------------------------"
		write-host "	Unable to write to output file: $outDir$destFile1"
		write-host "	Maybe someone is viewing it in Acrobat?"
		write-host "	-------------------------------------------------"
		exit
		}
	cp "$oFile" "$outDir$destFile1"
	#- The following is in 'ubin'
	write-host "	Running: syncmmdocs.cmd"
	syncmmdocs.cmd
}
#--------------------------------------------------------------------------------
if ($shrink -or $s) {
	shrink
}
#--------------------------------------------------------------------------------
if ($archive -or $a) {
	shrink
	archive
}
#--------------------------------------------------------------------------------
if ($push -or $p) {
	shrink
	archive
	push
}
#--------------------------------------------------------------------------------

