#--------------------------------------------------------------------------------
#- testx.ps1
#--------------------------------------------------------------------------------
#
#  e.g. testx inline
#  e.g. testx drawguide
#
#--------------------------------------------------------------------------------
# Copy $args array to $runxArgs
$runxArgs	= $args
#--------------------------------------------------------------------------------
$runxHelp      = @"
runx [options] doctype
"@
$fmc	= 0	#--- Use mmComposer
function getOpts	{
	#------------------------------------------------------------------------
	#- Store & strip '-options'.
	#------------------------------------------------------------------------
	Param([int]$optsCount)

	For ($i=0; $i -lt $optsCount ; $i++)  {
		$optionCount += 1
		switch -casesensitive ($runxArgs[$i]) {
			'-f'		{$fmc	= 1}	#--- Use fmComposer
			'-fm'		{$fmc	= 1}	#--- Use fmComposer
			'-fmc'		{$fmc	= 1}	#--- Use fmComposer
			'-mmc'		{$mmc	= 1}
			default {
				# Not an option
				$optionCount -= 1
				}
		}
	}
}
#--------------------------------------------------------------------------------
if ($args.count -gt 0) {
	if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
		write-host $runxHelp
		exit
		}
	else    {
		$lastArgument	= $args[$args.count - 1]
		}
	}
else    {
	# Just 'runx' typed, with no arguments
	write-host "runx: argument required"
	write-host "See: runx -h[elp] "
	exit
	}
#--------------------------------------------------------------------------------
#- Process arguments
#--------------------------------------------------------------------------------
if ($args.count -gt 1) {
	. getOpts $($args.count - 1)
}
$argumentCount	= $args.count - $optionCount	# Arguments excluding options
if ($argumentCount -gt 1) {
	write-host	"runx: too many arguments"
	write-host	"See: runx -h[elp] "
}
$docType		= $lastArgument
#--------------------------------------------------------------------------------
	write-host	"runx: $docType"
#--------------------------------------------------------------------------------
$outFolder              = "$env:output"		#--- Default
#--------------------------------------------------------------------------------
switch -casesensitive ($docType) {
	'inline'	{$inFile	= "$env:inline\inline.inc"			}
	'drawguide'	{$inFile	= "$env:drawguide\drawguide.inc"		}
	'svgtest1'	{
				$inFile		= "$env:svgtests\svg1" 
			 	$outFolder	= "$env:svgtests"
write-host	"----------------- outFolder: $outFolder"
				}
	default		{
			write-host	"Unknown document type: $docType"
			exit
			}
	}
#--------------------------------------------------------------------------------
function runmmc {
	if($fmc -eq 1) {
		#--- Use fmComposer
		$ctag			= "fm"		#- 'ctag' is for composer (mm or fm)
		$env:composer		= "$ctag"
		$tmpFile		= "$env:output\$docType"+".$ctag" + ".tmp"
		$outputFile		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$env:composerText	= "fmComposer"
		mmpp -Mfile $env:control\control.mmp $inFile | out-file -enc oem $tmpFile
		"</MiramoXML>" | out-file -append -enc utf8 $tmpFile
		miramo -composer $ctag -PDFbookmarks 0 -sendenv Y -Opdf $outputFile $tmpFile
		}
	else	{
		#--- Use mmComposer
write-host	"----------------- outFolder: $outFolder"
		$ctag			= "mm"		#- 'ctag' is for composer (mm or fm)
		$env:composer		= "$ctag"
		$tmpFile		= "$outFolder\$docType"+".$ctag" + ".tmp"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$outputFile		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$env:composerText	= "$ctag" + "Composer"
		mmpp -Mfile $env:control\control.mmp $inFile | out-file -enc utf8 $tmpFile
		"</MiramoXML>" | out-file -append -enc utf8 $tmpFile
		miramo -composer "$ctag" -keep -PDFbookmarks 0 -sendenv Y -warn 2 -Opdf $outputFile $tmpFile `
			2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) `
				{ $_.Exception.Message.trim("`n") } else { $_ } } 
		}
	}
#--------------------------------------------------------------------------------
$mmVersion	= miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
$mmVersion	= $mmVersion | select-string -pattern "iramo"
write-host	"$mmVersion"
$env:startTime	= Get-Date -format "yyyy-MMM-dd HH:mm"
#--------------------------------------------------------------------------------
$env:pid                = $pid


#--------------------------------------------------------------------------------
mkFormatdefs		# In 'manbin'
#--------------------------------------------------------------------------------
write-host	"Doc type is: $docType"
. runmmc

#--------------------------------------------------------------------------------
if ($lastexitcode -eq 0) {
	write-host	"SUCCESS"
	write-host	"Created: $outputFile"
	$openOutput	= Read-Host "Open output file?
	[p	= Open PDF output file]
	[fop	= Open FOP output/input file]
	[i	= Open mmComposer input file]
	[anything else = quit]
 	p | fop | i ?"
	switch ($openOutput) {
			"p"		{start-process acrobat -Argumentlist "/n $outputFile"}
			"fop"		{$fopFile = "$tmpFile"+".fo" ; vi "$fopFile" }
			"i"		{vi "$tmpFile" }
			default		{ exit }
		}
} else {
	write-host	"FAILED !!!"
	$openInput	= Read-Host "Open input file?
	[i	= Open mmComposer input file]
	[anything else = quit]
 	i ?"
	switch ($openInput) {
			"i"		{vi "$tmpFile" }
			default		{ exit }
		}
}
#--------------------------------------------------------------------------------
