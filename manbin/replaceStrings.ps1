# $lookupTable = @{
# 'something1' = 'something1aa' 
# 'something2' = 'something2bb' 
# 'something3' = 'something3cc' 
# }

$XXlookupTable = @{
	'</odefault>'		= '</propertyDefault>' 
	'<odefault'		= '<propertyDefault' 
}
# hash list not processed in order
#	'<pdp>'			= '<pp>' 
#	'</pdp>' 		= '</pp>' 
$replaceSet1 = @{
	'</xOption>'			= '</property>' 
	'<xOption'			= '<property' 
	'</xOptGroup>'			= '</propertyGroup>' 
	'<xOptGroup'			= '<propertyGroup' 
	'</xOptDefault>'		= '</propertyDefault>' 
	'<xOptDefault'			= '<propertyDefault' 
	'</odefault>'			= '</propertyDefault>' 
	'<odefault'			= '<propertyDefault' 
	'</oP>'				= '</pdp>' 
	'<oP'				= '<pdp' 
	'<xOptPara/>'			= '' 
	'</optionValueKeyList>'		= '</propertyValueKeyList>' 
	'<optionValueKeyList'		= '<propertyValueKeyList' 
	'<xvaluekeyhead'		= '<propertyValueKeyList' 
	'@-----------*$'		= '@------------------------------------------------------------' 
	'</codeStart>'			= '</elementStart>' 
	'<codeStart'			= '<elementStart' 
	'codeType='			= 'elementType=' 
	'codeName='			= 'elementName=' 
	'<@OptionsStart>'		= '<|propertiesStart>' 
	'<@CodeStart>'			= '<elementStart' 
	'CodeName1'			= 'elementName' 
	'[ 	]xml_opt_atts[ 	]'		= ' elementType="xml_opt_atts" elementName="' 
	'[ 	]xml_opt_cont[ 	]'		= ' elementType="xml_opt_cont" elementName="' 
	'[ 	]xml_no_cont[ 	]'		= ' elementType="xml_no_cont" elementName="' 
	'[ 	]xml_mand_atts[ 	]'		= ' elementType="xml_mand_atts" elementName="' 
	'[ 	]xml_no_atts[ 	]'		= ' elementType="xml_no_atts" elementName="' 
	'[ 	]xml_no_atts_or_cont[ 	]'		= ' elementType="xml_no_atts_or_cont" elementName="' 
	'XRefID='			= 'dest=' 
	'<@OptionsEnd>'			= '<|propertiesEnd>' 
	'value=`"0 - 15`"'		= 'value=`"0 - 7, 15`"' 
	'0 - 15'			= '0 - 7, 15' 
	'=F_Italic'			= '="F_Italic"' 
	'=F_Bold'			= '="F_Bold"' 
	'\|OptionsEnd'			= '|propertiesEnd' 
	'<#fI>'				= '<fI>' 
	'<#fnI>'			= '</fI>'
	'<#fB>'				= '<fB>' 
	'<#fnB>'			= '</fB>'
	'<\|NoteP>'			= '<elementNoteP>'
	'<\|Note>'			= '<elementNote>'
	'<\|npage>[ 	]'		= '<xnpage id="'
	'<\|phyphen>'			= '<xphyphen/>'
	'<\|Examples>'			= '<elementExamples>'
	'<\|CodeEnd>'			= '<|elementEnd>'
	'<@CodeEnd>'			= '<|elementEnd>'
	'<\|paranumonly>[ 	]'	= '<xparanumonly id="'
}
$replaceSet2 = @{
	'[^x][ 	]+id="[^\|]*\|'		= '$&xlqid" file="|xlqf'
	'[ 	]+elementName="[^ ]*'	= '$&"'
}
$replaceSet3 = @{
	'file="\|xlqf[^$]*$'			= '$&" />'
	'[ 	]+elementName="[^{]*{'	= '$&>'
}
$replaceSet4 = @{
	'\|xlqf'			= ''
	'\|xlqid'			= ''
	'{>'				= '>'
}
$replaceSet5 = @{
	'=[ 	]*[^\x27"][^ 	>/]*'	= 'x8x"$&yiy"'
}
$replaceSet6 = @{
	'x8x"'				= '"z'
	',yiy"'				= '",'
	'<yiy"'				= '"<'
}
$replaceSet7 = @{
	'yiy"'				= '"'
}
$replaceSet8 = @{
	'"z='				= '="'
}

$original_file		= $args[0]
$destination_file	= $args[0]
$copyFile		= "$original_file.rsb"
cp $original_file $copyFile

# Get-Content -Path $original_file | ForEach-Object { 
(Get-Content -Path $original_file ) | ForEach-Object { 
	$line = $_

	$replaceSet1.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet2.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet3.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet4.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet5.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet6.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet7.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
	$replaceSet8.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
   $line
} | Set-Content -Path $destination_file

