#--------------------------------------------------------------------------------
#- checkLog.ps1
#- 
#- model is 'mmdoc.ps1'
#--------------------------------------------------------------------------------
param(
	[switch]$f,
	[switch]$y,
	[switch]$h,
	[switch]$help,
	# Residue after -param/value and -switch params extracted
	# (usually last argument, e.g. input file name):
	# [parameter(Mandatory=$true, Position=0,
	[parameter(Position=0,
	ValueFromRemainingArguments=$true)][array]$lastArgs,
	[switch] $dummy
	)
#--------------------------------------------------------------------------------
$checkLogHelp	= @"
checkLog: summarize data in mmVisor log files

	Usage:

		checkLog [ options ] files

	Options:
		-help	Show this help message (alias is -h)
		-crlf	Process files to add CR/LF at end (so mmpp works)

	Examples:

		checkLog -f mmServer*.xml

		checkLog mmServer*.xml

"@
#--------------------------------------------------------------------------------
if($help -or $h) { $checkLogHelp ; exit }

#--------------------------------------------------------------------------------
$env:MACRODIR		= "$env:MANBIN\macros\checkLog"
$env:summaryFile	= "jobSummary.xml"

#--------------------------------------------------------------------------------
if (Test-Path $env:summaryFile ) {
	write-host	"Removing file:	$env:summaryFile"
	rm jobSummary.xml
}
#--------------------------------------------------------------------------------

$scriptName	= $MyInvocation.MyCommand.Name

#- write-host "lastArgs count:	= " $lastArgs.count
#- write-host "last argument:	= $lastArgs"

$files	= Get-ChildItem -Path $scriptPath -filter "$lastArgs"

#- For each object in $files
#- '$file' below may be '$x' or '$anything' , etc

#--------------------------------------------------------------------------------
ForEach ($file in $files) {
	if ( "$file" -match "mmServerLog.xml" )  {mv  "$file"  "mmServerLog00.xml"}
	if ( "$file" -match "mmServerLog1.xml" ) {mv  "$file"  "mmServerLog01.xml" }
	if ( "$file" -match "mmServerLog2.xml" ) {mv  "$file"  "mmServerLog02.xml" }
	if ( "$file" -match "mmServerLog3.xml" ) {mv  "$file"  "mmServerLog03.xml" }
	if ( "$file" -match "mmServerLog4.xml" ) {mv  "$file"  "mmServerLog04.xml" }
	if ( "$file" -match "mmServerLog5.xml" ) {mv  "$file"  "mmServerLog05.xml" }
	if ( "$file" -match "mmServerLog6.xml" ) {mv  "$file"  "mmServerLog06.xml" }
	if ( "$file" -match "mmServerLog7.xml" ) {mv  "$file"  "mmServerLog07.xml" }
	if ( "$file" -match "mmServerLog8.xml" ) {mv  "$file"  "mmServerLog08.xml" }
	if ( "$file" -match "mmServerLog9.xml" ) {mv  "$file"  "mmServerLog09.xml" }
}

# get-childitem -filter "*.xls" | sort LastWriteTime -Descending |select name

# exit
# $files	= Get-ChildItem -Path $scriptPath -filter "$lastArgs"
# $xfiles	= Get-ChildItem -Path $scriptPath -filter "$lastArgs" 
# $files = $xfiles | sort-object -Property name -descending
$files	= Get-ChildItem -Path $scriptPath -filter "$lastArgs" | sort-object -property name -descending
#--------------------------------------------------------------------------------
ForEach ($file in $files) {
	$env:filename	= "$file"
	#- write-host "Checking file:	$file"
	if ($crlf) {
		mmpp $env:MACRODIR/fileCheck.mmp
		try {
			#- $content = [IO.File]::ReadAllText($_.Fullname)
			$content = [IO.File]::ReadAllText($file)
				if($content -notmatch '(?<=\r\n)\z')
					{
					Add-Content -Value ([environment]::newline) -Path $file
					} 
			}
	catch {}
		}
	#------------------------------------------------------------------------
	$containsJobRecord = Select-String  -pattern "<jobRecord" -path $file
	If ($containsJobRecord -eq $null) {
		#- write-host "$file does not contain '<jobRecord' "
		$env:containsJobRecord	= "0"
		}
	Else {
		#- write-host "$file contains '<jobRecord' "
		$env:containsJobRecord	= "1"
		}
	#------------------------------------------------------------------------
	$fileSize	=  (Get-Item $file).length 
	$env:fileSize	=  $fileSize
	$env:fileSizeText	=  "{0:N0}" -f $fileSize
	#- write-host	"fileSize	$env:fileSize"
	#- write-host	"fileSize	$env:fileSizeText"
	mmpp -Mfile "$env:MACRODIR\control.mmp" $file
}

Add-Content $env:summaryFile "`n<printSummary/>"
	mmpp -Mfile "$env:MACRODIR\control.mmp" $env:summaryFile

exit
