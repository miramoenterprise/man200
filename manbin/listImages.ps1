#--------------------------------------------------------------------------------
#- listImages.ps1
#--------------------------------------------------------------------------------
# $env:MACROFILE	= "$env:MANBIN\macros\listImages\listImages.mmp"
$env:images		= "$env:manhome\images"
$MACROFILE1		= "$env:MANBIN\macros\listImages\listImagesPrefix.mmp"
$MACROFILE2		= "$env:MANBIN\macros\listImages\listImagesSuffix.mmp"
$argList		= $args

$argCount		= $args.count
$lastArg		= $argCount - 1
$fileName		= $args[$lastArg]

write-host		"$fileName"
# get-content a.txt,b.txt,c.txt | out-file output.txt
# mmpp -Mfile $MACROFILE "$fileName" | out-file /tmp/zvbgxx
# get-content $MACROFILE, "$fileName" | mmpp | out-file /tmp/zvbgxx
# get-content $MACROFILE, "$fileName" > /tmp/zvbgxx
get-content $MACROFILE1, "$fileName", $MACROFILE2 | out-file -enc oem /tmp/zvbgxx
mmpp /tmp/zvbgxx | out-file /tmp/zvbgxx2
