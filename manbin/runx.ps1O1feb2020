#--------------------------------------------------------------------------------
#- runx.ps1
#--------------------------------------------------------------------------------
#
#  e.g. runx inline
#  e.g. runx drawguide
#
#--------------------------------------------------------------------------------
$env:releaseVersion	= "1.4"
$env:releaseVersion	= "2.0"
$env:releaseVersion	= "1.5"
$env:releaseVersion	= "v 2.0 [1st DRAFT DRAFT] "
#--------------------------------------------------------------------------------
# Copy $args array to $runxArgs
$runxArgs		= $args
#--------------------------------------------------------------------------------
# write-host		"$env:mman"
$env:documentInitiator 		= "runx"
$env:documentName 		= "mmDrawGuide.pdf"
$env:documentName		= "mmComposerReferenceGuide.pdf"
$env:documentFooterTitle	= "mmComposer Reference Guide (v $env:releaseVersion)"
$env:includeFootnoteDef		= "1"
$env:setLandscape		= "0"
#--------------------------------------------------------------------------------
$env:mkfmcRelnotes	= "N"
$env:mkfmcAddendum	= "N"
$sp			= "N"	# showProperties switch
#--------------------------------------------------------------------------------
$outFolder		= "$env:output"
$startFolder		= pwd
cd $outFolder
#--------------------------------------------------------------------------------
$startTime		= $(get-date)
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
write-host 		"Start time: $nowTime"
#--------------------------------------------------------------------------------
$OrigOutputEncoding = [Console]::OutputEncoding
$runxHelp      = @"
runx [options] doctype
e.g. 
	runx inline | drawguide | mantests | refguide | lists |
		language | color |
		thai 
"@
$fmc	= 0	#--- Use mmComposer
function getOpts	{
	#------------------------------------------------------------------------
	#- Store & strip '-options'.
	#------------------------------------------------------------------------
	Param([int]$optsCount)

	For ($i=0; $i -lt $optsCount ; $i++)  {
		$optionCount += 1
		switch -casesensitive ($runxArgs[$i]) {
			'-f'		{$fmc	= 1}	#--- Use fmComposer
			'-fm'		{$fmc	= 1}	#--- Use fmComposer
			'-fmc'		{$fmc	= 1}	#--- Use fmComposer
			'-mmc'		{$mmc	= 1}	#--- Use mmComposer (default)
			'-relaxed'	{$relaxed	= 1}	#--- Relaxed mode
			'-sp'		{$sp	= "Y"}	#--- Show properties mode
			'-l'		{$env:setLandscape	= "1"}	#--- Show properties mode
			default {
				# Not an option
				$optionCount -= 1
				}
		}
	}
}
#--------------------------------------------------------------------------------
if ($args.count -gt 0) {
	if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
		write-host $runxHelp
		exit
		}
	else    {
		$lastArgument	= $args[$args.count - 1]
		}
	}
else    {
	# Just 'runx' typed, with no arguments
	write-host "runx: argument required"
	write-host "See: runx -h[elp] "
	exit
	}
#--------------------------------------------------------------------------------
#- Process arguments
#--------------------------------------------------------------------------------
if ($args.count -gt 1) {
	. getOpts $($args.count - 1)
}
$argumentCount	= $args.count - $optionCount	# Arguments excluding options
if ($argumentCount -gt 1) {
	write-host	"runx: too many arguments"
	write-host	"See: runx -h[elp] "
}
$docType		= $lastArgument
$env:docName		= $docType
#--------------------------------------------------------------------------------
	write-host	"runx: $docType"
#--------------------------------------------------------------------------------
switch -casesensitive ($docType) {
	'inline'	{$inFile	= "$env:inline\inline.inc"			}
	'ishort'	{$inFile	= "$env:inline\ishort.inc"			}
	'fdefs'		{$inFile	= "$env:mfd\mfd.inc"				}
	'mfd'		{$inFile	= "$env:mfd\mfd.inc"				}
	'mshort'	{$inFile	= "$env:mfd\mshort.inc"				}
	'mfds'		{$inFile	= "$env:mfd\mfd.inc"				}
	'drawguide'	{$inFile	= "$env:drawguide\drawguide.inc"		
				$env:documentName = "mmDrawGuide.pdf"
				}
	'xelements'	{$inFile	= "$env:xelements\xelements.inc"		}
	'refguide'	{$inFile	= "$env:refguide\refguide.inc"			}
	'short'		{$inFile	= "$env:refguide\short.inc"			}
	'long1'		{$inFile	= "$env:refguide\long1.inc"			}
	'long2'		{$inFile	= "$env:refguide\long2.inc"			}
	'long3'		{$inFile	= "$env:refguide\long3.inc"			}
	'long4'		{$inFile	= "$env:refguide\long4.inc"			}
	'long5'		{$inFile	= "$env:refguide\long5.inc"			}
	'long6'		{$inFile	= "$env:refguide\long6.inc"			}
	'color'		{$inFile	= "$env:appendices\color\color.inc"		}
	'caps'		{$inFile	= "$env:appendices\capitalization\capitalization.inc" }
	'language'	{$inFile	= "$env:refguide\language\language.inc"		}
	'running'	{$inFile	= "$env:refguide\running\running.inc"		}
	'running'	{$inFile	= "$env:refguide\running\running.inc"		}
	'ix'		{$inFile	= "$env:refguide\inline\ix.inc"			}
	'textframedef'	{$inFile	= "$env:refguide\mfd\textframedef.inc"		}
	'aframe'	{$inFile	= "$env:refguide\inline\aframe.inc"		}
	'paradef'	{$inFile	= "$env:refguide\mfd\paradef.inc"		}
	'thai'		{$inFile	= "$env:refguide\language\thai\thai.inc"	}
	'fonts'		{$inFile	= "$env:appendices\fonts\fonts.inc"		}
	'appendices'	{$inFile	= "$env:appendices\appendices.inc"		}
	'autonumbering'	{$inFile	= "$env:appendices\autonumbering\autonumbering.inc" }
	'entities'	{$inFile	= "$env:appendices\characterEntities\characterEntities.inc"		}
	'lastPage'	{$inFile	= "$env:mman\tests\lastPage\lastPageTestInput.inc"
			$env:documentFooterTitle = "&#x24;mman/tests/lastPage (runx -l lastPage)&emsp;&emsp;&emsp;"
						}
	'textlanguage'	{$inFile	= "$env:appendices\textlanguage\textlanguage.inc" }
	'opos'		{
$env:documentFooterTitle	= "<Font capitalization='smallCaps' >Rules and Tests</Font> (<fB>MiramoPDF v $env:releaseVersion</fB> &emdash; $startTime)"
$inFile	= "$env:mman/mmcTests/objectVerticalPositioning\positioning.inc"
}
	'lists'		{$inFile	= "$env:refguide\lists\lists.inc"		}
	'mantests'	{$inFile	= "$env:mantests\tests.inc" 
			 $outFolder	= "$env:mantests" 				}
	'svgtest1'	{$inFile	= "$env:svgtests\svg1" 
			 $outFolder	= "$env:svgtests\svg1" 				}
	#------------------------------------------------------------------------
	'fmcrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}
	'fmrelnotes'	{$inFile	= "$env:fmcrelnotes\fmcrelnotes.inc" 
			 $env:mkfmcRelnotes	= "Y" 					}
	'fmcaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmcAddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	'fmaddendum'	{$inFile	= "$env:fmcaddendum\fmcaddendum.inc" 
			 $env:mkfmcAddendum	= "Y" 					}
	#------------------------------------------------------------------------
	default		{
			write-host	"Unknown document type: $docType Try runx -h"
			exit
			}
	}
#--------------------------------------------------------------------------------
function runmmc {
	#------------------------------------------------------------------------
	#- http://stackoverflow.com/questions/2902874/input-encoding-accepting-utf-8
	#------------------------------------------------------------------------
	if($fmc -eq 1) {
		#--- Use fmComposer
		$ctag			= "fm"		#- 'ctag' is for composer (mm or fm)
		$env:composer		= "$ctag"
		$env:composer		= "fmc"
		$tmpFile		= "$env:output\$docType"+".$ctag" + ".mmxml"
		$outputFile		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$oMIF			= "$outFolder\$docType"+".$ctag" + ".mif"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$env:composerText	= "fmComposer"
		mmpp -Mfile $env:control\control.mmp $inFile | out-file -enc utf8 $tmpFile
		"</MiramoXML>" | out-file -append -enc utf8 $tmpFile
		miramo -composer $ctag -PDFbookmarks 0 -sendenv Y -Omif $oMIF -Opdf $outputFile $tmpFile 2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) { $_.Exception.Message } else { $_ } }		
		}
	else	{
pwd
		#--- Use mmComposer
		$ctag			= "mm"		#- 'ctag' is for composer (mm or fm)
		$env:composer		= "$ctag"
		$env:composer		= "mmc"
		$tmpFile		= "$env:output\$docType"+".$ctag" + ".mmxml"
		$foFile			= "$outFolder\$docType"+".$ctag" + ".fo"
		$outputFile		= "$outFolder\$docType"+ ".pdf"
		$outputFile2		= "$outFolder\$docType"+".$ctag" + ".pdf"
		$outputFile3		= "$outFolder\mmComposerReferenceGuide" + ".pdf"
		$env:composerText	= "$ctag" + "Composer"
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
		#----------------------------------------------------------------
		mmpp -Mfile $env:control\control.mmp $inFile | out-file  $tmpFile -enc utf8
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
		#----------------------------------------------------------------
		"</MiramoXML>" | out-file -append -enc "utf8" $tmpFile
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
write-host 		"Starting mmComposer: $nowTime"
		miramo -composer "$ctag" -showProperties "$sp" -bwc 0 -keep -PDFbookmarks 0 -sendenv Y -warn 2 -Opdf $outputFile $tmpFile `
			2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) `
				{ $_.Exception.Message.trim("`n") } else { $_ } } 
		}
	}
#--------------------------------------------------------------------------------
$mmVersion	= miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
$mmVersion	= $mmVersion | select-string -pattern "iramo"
write-host	"$mmVersion"
$env:mmVersion	= "$mmVersion"
$xxyz	= "$mmVersion"
#- write-host	"$xxyz			$docType"
#- write-host	"$mmVersion	$docType"
#- write-host	"$mmVersion"
$env:startTime	= Get-Date -format "yyyy-MMM-dd HH:mm"
# $env:docDate	= Get-Date -format "yy-MM-dd"
$env:docDate	= Get-Date -format "ddMMyy"
$env:thisYear	= Get-Date -format "yyyy"
write-host	"$env:thisYear"
#--------------------------------------------------------------------------------
$env:pid                = $pid


#--------------------------------------------------------------------------------
mkFormatdefs		# In 'manbin'
#--------------------------------------------------------------------------------
. runmmc

write-host	"$startFolder"
copy-item  "$tmpFile"		-destination	"$env:output\last.mmxml"
#--------------------------------------------------------------------------------
$elapsedTime	= $(get-date) - $startTime
$totalTime	= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
#--------------------------------------------------------------------------------
$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
write-host 		"End time: $nowTime"
#--------------------------------------------------------------------------------
if ($lastexitcode -eq 0) {
	write-host	"SUCCESS"
	write-host	"TIME:		$totalTime"
	write-host	"Created: $outputFile"
	$fopFile = "$tmpFile"+".fo" 
	copy-item  "$outputFile"	-destination	"$env:output\last.pdf"
	if ($doctype -eq "refguide" ) {
		copy-item  "$outputFile"	-destination	"$env:output\mmComposerReferenceGuide.pdf"
		}
	# copy-item  "$outputFile"	-destination	"$env:output\$env:documentName"
	# C:\u\miramo\man...ide.mm.mmxml.fo:String) [Copy-Item], ItemNotFoundException
	# refguide.mm.mmxml.mmtmp.p2
	# copy-item  "$fopFile"		-destination	"$env:output\last.fo"
	$openOutput	= Read-Host "Open output file?
	[p	= Open PDF output file]
	[fop	= Open FOP output/input file]
	[i	= Open mmComposer input file]
	[anything else = quit]
 	p | fop | i ?"
	switch ($openOutput) {
			"p"		{start-process acrobat -Argumentlist "/n $outputFile"}
			"fop"		{$fopFile = "$tmpFile"+".fo" ; vi "$fopFile" }
			"i"		{vi "$tmpFile" }
			default		{ cd $startFolder ; exit }
		}
} else {
	write-host	"FAILED !!!"
	$openInput	= Read-Host "Open input file?
	[i	= Open mmComposer input file]
	[anything else = quit]
 	i ?"
	switch ($openInput) {
			"i"		{vi "$tmpFile" }
			# default		{ exit }
			default		{ cd $startFolder ; exit }
		}
}
#--------------------------------------------------------------------------------
cd $startFolder
#--------------------------------------------------------------------------------
