#--------------------------------------------------------------------------------
# changeMarkup.ps1
#--------------------------------------------------------------------------------
#
# $lookupTable = @{
# 'something1' = 'something1aa' 
# 'something2' = 'something2bb' 
# 'something3' = 'something3cc' 
# }

# hash list not processed in order
#	'<pdp>'			= '<pp>' 
#	'</pdp>' 		= '</pp>' 
$replaceSet1 = @{
	'<P fmt='			= '<P paraDef=' 
	'<ParaDef fmt='			= '<ParaDef paraDef=' 
	'<Font fmt='			= '<Font fontDef=' 
	'<FontDef fmt='			= '<FontDef fontDef=' 
	'<ColorDef fmt='		= '<ColorDef colorDef=' 
	'<RuleDef fmt='			= '<RuleDef ruleDef=' 
	'<Tbl fmt='			= '<Tbl tblDef=' 
}

$original_file		= $args[0]
$destination_file	= $args[0]
$copyFile		= "$original_file.rsb"
cp $original_file $copyFile

(Get-Content -Path $original_file ) | ForEach-Object { 
	$line = $_

	$replaceSet1.GetEnumerator() | ForEach-Object {
		if ($line -match $_.Key) {
			$line = $line -replace $_.Key, $_.Value
			}
		}
   $line
} | Set-Content -Path $destination_file

