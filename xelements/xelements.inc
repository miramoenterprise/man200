@--------------------------------------------------------------------------------
@- xelements.inc
@- include files for drawguide guide
@--------------------------------------------------------------------------------
<MasterPageRule	allPages="Right" />
<@include>	${xelements}/cover.inc
<chapterStart
	dest="xElements"
	>X Elements
</chapterStart>
<@include>	${xelements}/introduction
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${xelements}/frameshadow
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

