@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="listLabelTests"
	>SVG images tests ($MMAN/qa/svgimages/svgimages.inc)
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@skipStart>
<@include>		${mman}/qa/svgimages/svgimages1
<@include>		${mman}/qa/svgimages/svgimages1a
<@include>		${mman}/qa/svgimages/svgimages2
<@include>		${mman}/qa/svgimages/svgimages3
<@skipEnd>
<@include>		${mman}/qa/svgimages/svgimages4
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
