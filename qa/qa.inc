@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<@skipStart>
<#def> QA				1
<chapterStart
	dest="qaRegression"
	>QA tests
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@skipEnd>
@- <@include>		${mman}/qa/verticalSpacing/verticalSpacing.inc
@- <@include>		${mman}/qa/listlabel2/listlabel.inc
@- <@include>		${mman}/qa/listlabel/listlabel.inc
@- <@include>		${mman}/qa/svgdef/svgdef.inc
@- <@include>		${mman}/qa/svgimages/svgimages.inc
<@include>		${mman}/qa/sideheads/sideheads.inc
@- <@include>		${mman}/qa/image/image.inc
@- <@skipStart>
@- <@skipEnd>
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
@- <chapterEnd/>
@-----------------------------------------------------------
