@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="listLabelTests"
	>SVG tests ($MMAN/qa/svgdef/svgdef.inc)
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@skipStart>
<@skipEnd>
@- <@include>		${mman}/qa/svgdef/svgdef1
<@include>		${mman}/qa/svgdef/svgdef2
@- <@include>		${mman}/qa/svgdef/svgdef3
@- <@include>		${mman}/qa/svgdef/svgdef4
@- <@include>		${mman}/qa/svgdef/svgdef5
@- <@include>		${mman}/qa/svgdef/svgdef6
@- <@include>		${mman}/qa/svgdef/svgdef7
@- <@include>		${mman}/qa/svgdef/svgdef8
@- <@include>		${mman}/qa/svgdef/svgdef9
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
