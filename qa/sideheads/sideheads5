@------------------------------------------------------------
@- sideheads5
@- 
@------------------------------------------------------------
<#def> fn	5
@------------------------------------------------------------
<Section	page="MP_referenceGuide" allPages="sideheadsMP_referenceGuide" />
<elementStart elementType="xml_opt_cont" elementName="sideheads<#fn>" 
	position="topOfPage"
	position="normal"
	>

</elementStart> 
@--------------------------------------------------------------------------------
<#def> shVA	tops
<#def> shSpaceAbove		{18pt}
<#def> shLeading		{6pt}
<#def> shTextSize		{12.0pt}
<#def> shLeading		{4pt}
<#def> shRoundedCorners		{Y}
<#def> shTopBottomRule		{none}
<#def> bodyTextSize		{10pt}
<#def> bodyLeading		{4pt}
@--------------------------------------------------------------------------------

<#def> sideheaddef {
<ParaDef paraDef="SideHeading<#fn>" sidehead="Y" sideheadGap="8mm" sideheadWidth="42mm" firstLineIndent="9mm" startIndent="9mm"
	fontFamily="Noto Sans" fontWeight="Bold" textSize="<#shTextSize>" hyphenate="N" ><paraFrame
		@- BUG bug with margins
		topMargin="6pt" bottomMargin="5pt" endMargin="12pt" roundedCorners="<#shRoundedCorners>"
		startRule="Thin" endRule="Thin"
		cornerRadius="4pt" topRule="<#shTopBottomRule>" startMargin="10pt" svgDef="sg2" bottomRule="<#shTopBottomRule>" />
		<ListLabel labelWidth="4mm" type="fixed" labelIndent="3mm" textColor="Maroon" seriesLabel="counter5" ><counter/>.</ListLabel>
</ParaDef>
}

<#sideheaddef>

<exampleBlock
	tabs="8|12|14"
	leftIndent="0"
	crop="Y"
	>
@--------------------------------------------------------------------------------
@- svgDef="sg5"
@--------------------------------------------------------------------------------
<!-- xSvgDef -->
<xSvgDef svgDef="sg<#fn>" width="container" height="container" >
	&lt;linearGradient id="shGrad1" gradientTransform="rotate(0)" >
		&lt;stop offset="0%" stop-color="#e7614a"/> &lt;stop offset="30%" stop-color="#ff9a83"/>
		&lt;stop offset="40%" stop-color="#e7614a"/> &lt;stop offset="99%" stop-color="#eF8a73"/>
		&lt;stop offset="99%" stop-color="#e7614a"/> &lt;/linearGradient>
	&lt;rect fill="url(#shGrad1)" width="100%" height="100%" rx="0.5mm" ry="1.0mm" />
</xSvgDef>
@--------------------------------------------------------------------------------
<!-- ParaDef -->
<#sideheaddef>
@--------------------------------------------------------------------------------
<P/>
<!-- <xfB>Sidehead paragraph (1)</xfB> -->
<P paraDef="SideHeading<#fn>" spaceAbove="<#shSpaceAbove>" leading="<#shLeading>" <xfB>textAlign="start"</xfB> <xfB>sideheadVerticalAlign="tops"</xfB>  <hc>position="topOfPage"</hc> >
A big Red Cat</P>
<!-- <xfB>Associated paragraph (1)</xfB> -->
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(1) An overview of MobileView, system operator application for StormCluster. [ <fB>sideheadVerticalAlign="tops"</fB> ]</P>
<Tbl tblDef="thinRulings" svgDef="sg<#fn>" >
<TblTitle textAlign="start" fontWeight="Bold" >Table Title: This is a table with a custom SVG gradient</TblTitle>
<TblColumn width="32mm" /><TblColumn width="32mm" /><TblColumn width="32mm" />
<Row height="8mm" ><Cell>A</Cell><Cell>B</Cell><Cell>C</Cell></Row>
<Row height="8mm" ><Cell svgDef="sg2" >A</Cell><Cell svgDef="sg2" >B</Cell><Cell>C</Cell></Row>
<Row height="8mm" svgDef="sg2" ><Cell>A</Cell><Cell>B</Cell><Cell>C</Cell></Row>
<Row height="8mm" ><Cell>A</Cell><Cell>B</Cell><Cell>C</Cell></Row>
<Row height="8mm" ><Cell>A</Cell><Cell>B</Cell><Cell>C</Cell></Row></Tbl>

<!-- <xfB>Sidehead paragraph</xfB> (2) -->
<P paraDef="SideHeading<#fn>" spaceAbove="<#shSpaceAbove>" leading="<#shLeading>" <xfB>textAlign="start"</xfB> <xfB>sideheadVerticalAlign="textTops"</xfB> >
A <Font textSize="+10pt">little</Font> White Cat</P>
<!-- <xfB>Associated paragraph (2)</xfB> -->
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(2) An overview of MobileView, system operator application for StormCluster. [ <fB>sideheadVerticalAlign="textTops"</fB> ]</P>

<!-- <xfB>Sidehead paragraph (3)</xfB> -->
<P paraDef="SideHeading<#fn>" spaceAbove="<#shSpaceAbove>" leading="<#shLeading>" <xfB>textAlign="start"</xfB> <xfB>sideheadVerticalAlign="firstBaselines"</xfB> >
White Cat with Whiskers</P>
<!-- <xfB>Associated paragraph (3)</xfB> -->
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(3) An overview of MobileView, system operator application for StormCluster. [ <fB>sideheadVerticalAlign="firstBaselines"</fB> ]</P>

<!-- <xfB>Sidehead paragraph (4)</xfB> -->
<P paraDef="SideHeading<#fn>" spaceAbove="<#shSpaceAbove>" leading="<#shLeading>" <xfB>textAlign="start"</xfB> <xfB>sideheadVerticalAlign="firstBaselines"</xfB> >
Some <Font textSize="+10pt">Cats</Font> are OK. Some other cats are definitely not OK</P>
<!-- <xfB>Associated paragraph (4)</xfB> -->
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4) An overview of <Font fontWeight="Bold" textSize="+12pt">MobileView</Font>, for StormCluster. [ <fB>sideheadVerticalAlign="firstBaselines"</fB> ]</P>
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4a) If a sidehead paragraph is immediately followed by a table, the alignment is always <fB>tops</fB>. In other
words <fB>sideheadVerticalAlign="firstBaselines"</fB> is ignored.</P>
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4a) If a sidehead paragraph is immediately followed by a table, the alignment is always</P>
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4a) If a sidehead paragraph is immediately followed by a table, the alignment is always</P>
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4a) If a sidehead paragraph is immediately followed by a table, the alignment is always</P>
<P paraDef="Body" textSize="<#bodyTextSize>" leading="<#bodyLeading>" >
(4a) If a sidehead paragraph is immediately followed by a table, the alignment is always</P>

</exampleBlock>

<#def> example_output	@gsub(<#example_output>, {&lt;}, {<})

<#example_output>




@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
