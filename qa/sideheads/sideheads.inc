@-----------------------------------------------------------
@- sideheads.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="sideHeadTests"
	>[QA]: Sidehead tests
	@- >Sidehead tests
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	page="MP_referenceGuide" allPages="sideheadsMP_referenceGuide" />
<@include>		${mman}/qa/sideheads/sideheadsVerticalSpacing
<@include>		${mman}/qa/sideheads/sideheads1
<@include>		${mman}/qa/sideheads/sideheads1b
<@include>		${mman}/qa/sideheads/sideheads1b
<@include>		${mman}/qa/sideheads/sideheads2
<@include>		${mman}/qa/sideheads/sideheads3
<@include>		${mman}/qa/sideheads/sideheads3a
<@include>		${mman}/qa/sideheads/sideheads4
<@include>		${mman}/qa/sideheads/sideheads5
<@include>		${mman}/qa/sideheads/sideheads5a
<@include>		${mman}/qa/sideheads/sideheads6
<@include>		${mman}/qa/sideheads/sideheads7
<@include>		${mman}/qa/sideheads/sideheads8
<@include>		${mman}/qa/sideheads/sideheads9
<@include>		${mman}/qa/sideheads/sideheads10
<@include>		${mman}/qa/sideheads/sideheads11
<@include>		${mman}/qa/sideheads/sideheads12
<@include>		${mman}/qa/sideheads/sideheads13
<@include>		${mman}/qa/sideheads/sideheads14
<@include>		${mman}/qa/sideheads/sideheads15
<@include>		${mman}/qa/sideheads/sideheads16
<@include>		${mman}/qa/sideheads/sideheads17
<@include>		${mman}/qa/sideheads/sideheads18
<@include>		${mman}/qa/sideheads/sideheads19
<@skipStart>
<@skipEnd>
<@skipStart>
<@skipEnd>
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
