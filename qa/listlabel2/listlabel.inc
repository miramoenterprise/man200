@-----------------------------------------------------------
@- sideheads.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="listLabelTests"
	>List label page breaking
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 		page="MP_referenceGuide" allPages="sideheadsMP_referenceGuide" />
<@include>		${mman}/qa/listlabel2/listlabelA
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
