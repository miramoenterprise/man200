@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<@skipStart>
<@skipEnd>
<chapterStart
	dest="verticalSpacingTests"
	>Vertical spacing tests
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@include>		${mman}/qa/verticalSpacing/verticalSpacing4
<@include>		${mman}/qa/verticalSpacing/verticalSpacing1
<@include>		${mman}/qa/verticalSpacing/verticalSpacing1a
<@include>		${mman}/qa/verticalSpacing/verticalSpacing1b
<@include>		${mman}/qa/verticalSpacing/verticalSpacing1c
<@include>		${mman}/qa/verticalSpacing/verticalSpacing2
<@include>		${mman}/qa/verticalSpacing/verticalSpacing3
<@include>		${mman}/qa/verticalSpacing/verticalSpacing4
<@include>		${mman}/qa/verticalSpacing/verticalSpacing5
<@include>		${mman}/qa/verticalSpacing/verticalSpacing6
<@include>		${mman}/qa/verticalSpacing/verticalSpacing6a
<@include>		${mman}/qa/verticalSpacing/verticalSpacing6b
<@include>		${mman}/qa/verticalSpacing/verticalSpacing6c
<@skipStart>
<@skipEnd>
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
