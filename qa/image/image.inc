@-----------------------------------------------------------
@- sideheads.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="Image_tests"
	>[QA]: Image tests 
	@- >Sidehead tests
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
@- <Section 	page="MP_referenceGuide" allPages="sideheadsMP_referenceGuide" />
<Section 	page="MP_referenceGuide" allPages="MP_referenceGuide" />
@- <@include>		${mman}/qa/image/image1
@- <@include>		${mman}/qa/image/image2
<@include>		${mman}/qa/image/image3
<@include>		${mman}/qa/image/image4
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
