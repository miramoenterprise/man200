@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<@skipStart>
<@skipEnd>
<chapterStart
	dest="listLabelTests"
	>List Label tests
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@skipStart>
<P paraDef="P_chapterTitle"
	spaceAbove="0pt"
        >List Label tests</P>
<@skipEnd>
@- <@include>		${mman}/qa/listlabel/listlabel0
<@include>		${mman}/qa/listlabel/listlabel0a
<@include>		${mman}/qa/listlabel/listlabel1
<@include>		${mman}/qa/listlabel/listlabel2
<@include>		${mman}/qa/listlabel/listlabel3
<@include>		${mman}/qa/listlabel/listlabel3f
<@include>		${mman}/qa/listlabel/listlabel4
<@include>		${mman}/qa/listlabel/listlabel5
<@include>		${mman}/qa/listlabel/listlabel6
<@include>		${mman}/qa/listlabel/listlabel6f
<@include>		${mman}/qa/listlabel/listlabel7
<@include>		${mman}/qa/listlabel/listlabel8
<@skipStart>
<@skipEnd>
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
