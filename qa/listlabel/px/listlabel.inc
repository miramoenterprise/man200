@-----------------------------------------------------------
@- qa.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<#def> QA				1
<chapterStart
	dest="listLabelTests"
	>List Label tests
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE				1
<#def> elementType			inline
<#def> hiddenCodeCount			0
<#def> hiddenCodeCountx			0
<#def> exampleTextOnlyCount		0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<Section 	allPages="MP_referenceGuide" />
<@skipStart>
<@include>		${mman}/qa/listlabel/listlabel1
<@include>		${mman}/qa/listlabel/listlabel2
<@include>		${mman}/qa/listlabel/listlabel3
<@include>		${mman}/qa/listlabel/listlabel4
<@include>		${mman}/qa/listlabel/listlabel1mm
<@include>		${mman}/qa/listlabel/listlabel2mm
<@include>		${mman}/qa/listlabel/listlabel3mm
<@include>		${mman}/qa/listlabel/listlabel4mm
<@include>		${mman}/qa/listlabel/listlabel5mm
<@include>		${mman}/qa/listlabel/listlabel6mm
<@skipEnd>
<@include>		${mman}/qa/listlabel/listlabel1-pt
<@include>		${mman}/qa/listlabel/listlabel2-pt
<@include>		${mman}/qa/listlabel/listlabel3-pt
<@include>		${mman}/qa/listlabel/listlabel3f-pt
<@include>		${mman}/qa/listlabel/listlabel4-pt
<@include>		${mman}/qa/listlabel/listlabel5-pt
<@include>		${mman}/qa/listlabel/listlabel6-pt
<@include>		${mman}/qa/listlabel/listlabel6f-pt
<@include>		${mman}/qa/listlabel/listlabel7-pt
<@include>		${mman}/qa/listlabel/listlabel8-pt
@-----------------------------------------------------------
<#def> inINLINE				0
<#def> QA				0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
