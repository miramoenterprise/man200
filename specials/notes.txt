Hi John,

This example contains

<Tbl>
...
..

<Row><Cell><P>Ax</P></Cell></Row>
<Row><Cell><P>Bx</P></Cell></Row>
<Row><Cell><P>Cx</P></Cell></Row>
<Row><Cell><P>Dx</P></Cell></Row>
</Tbl>

(at the very end).

I.e. the <Tbl> and <P> elements are default, unreferenced
formats.

The strange thing is that

'Ax' and 'Bx' are printed at 10pt.
'Cx' and 'Dx' are printed at 12pt.

This I guess is probably owing to our not having worked through
how to specify defaults for all cases ...

It's very low priority at this time.

It's a 'blocker' bug because from a user point of view the output
falls into the category 'unexpected and inexplicable'--unless
I'm misunderstanding--a category that ideally will have no
entries.

/Rick
