#--------------------------------------------------------------------------------
#- mmDefaultsRun.ps1
#--------------------------------------------------------------------------------
#- Create default formats for mmComposer & mmPageDesigner
#- 
#- And: create a set of selectable document page size options
#- 
#- E.g. A4, A4 landscape, USletter, and so on
#- 
#--------------------------------------------------------------------------------
$env:homeDir		= "$env:mman\defaultFormats"
$env:macroDir		= "$env:homedir\macros"
$env:macroFile		= "$env:macroDir\control.mmp"
#--------------------------------------------------------------------------------
$inputFile		= "$env:homedir\masterDefaultFormats.in"
#--------------------------------------------------------------------------------
$env:mmDefaultsPath	= "\\horse\d\\miramo\MiramoPDF\MiramoCore\designerApp"
$env:mmDefaultsFolder	= "$env:mmDefaultsPath\defaults"
$env:mmComposerTemplate	= "0"
$env:showBorders = "0"
write-host "DO NOT USE - this is no longer used to generate generic MFD templates"
exit
# CK: move to creating generic_*.mfd

$showBorders	= "0"

#--------------------------------------------------------------------------------
function runFormat {
	$size		= "$args"
	$env:paperSize	= "$size"
	if ( "$showBorders" -eq "1" ) {
		$outputFile	= "$env:homeDir\testDefaultFormats_$size.mfd"
		}
	else {
		$outputFile	= "$env:homeDir\generic_$size.mfd"
		}
	mmpp -Mfile "$env:macroFile" $inputFile | out-file  $outputFile -enc utf8
}

#--------------------------------------------------------------------------------
function makeTemplates {
		runFormat a3
		runFormat a3_landscape
		runFormat a4
		runFormat a4_landscape
		runFormat a5
		runFormat a5_landscape
		#----------------------------------------------------------------
		runFormat usletter
		runFormat usletter_landscape
		runFormat uslegal
		runFormat uslegal_landscape
		runFormat ustabloid
		runFormat ustabloid_landscape
}
#--------------------------------------------------------------------------------
$startFolder		= pwd
cd		$env:homeDir

[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
#----------------------------------------------------------------
	$showBorders	= "0"
write-host "Creating generic templates ..."
		makeTemplates
#	$showBorders	= "1"
#		makeTemplates
#----------------------------------------------------------------
write-host "Copying templates to $env:mmDefaultsFolder"
cp "$env:homeDir\generic_*.mfd" "$env:mmDefaultsFolder"
write-host "Copying templates to c:\u\miramo\xdev\src\mmDesigner\mmDesignerApp\defaults"
cp "$env:homeDir\generic_*.mfd" "c:\u\miramo\xdev\src\mmDesigner\mmDesignerApp\defaults"

#----------------------------------------------------------------
#- Create mmComposer default formats
#----------------------------------------------------------------
$env:mmComposerTemplate	= "1"
$showBorders	= "0"
#-- 		runFormat A4P
#-- cp "$env:homeDir\defaultFormats-A4P.xml" "\\horse\d\miramo\xdev\src\mmComposer\config\defaultFormatDefinitions.xml"
[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
#----------------------------------------------------------------


cd		$startFolder

exit

