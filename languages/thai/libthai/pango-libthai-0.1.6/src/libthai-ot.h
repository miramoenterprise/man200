/* Pango
 * libthai-ot.h:
 *
 * Copyright (C) 2004 Theppitak Karoonboonyanan
 * Author: Theppitak Karoonboonyanan <thep@linux.thai.net>
 */

#ifndef __LIBTHAI_OT_H__
#define __LIBTHAI_OT_H__

#include <pango/pango-ot.h>

PangoOTRuleset *
libthai_ot_get_ruleset (PangoFont *font);

void 
libthai_ot_shape (PangoFont        *font,
                  PangoGlyphString *glyphs);

#endif /* __LIBTHAI_OT_H__ */

