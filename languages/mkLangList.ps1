#--------------------------------------------------------------------------------
#- mkLangList.ps1
#--------------------------------------------------------------------------------
#-
#- 1. Create mmLanguagesList.xml file
#-
#- 2. Create Hunspell dictionaries directory for deployment
#-
#--------------------------------------------------------------------------------
$composerCoreDir	= "\\buzz\dev\MiramoPDF\MiramoCore\composerCore"
$miramoDeployDir	= "\\buzz\dev\MiramoPDF\MiramoDeploy\resources"
#--------------------------------------------------------------------------------
$startFolder		= pwd
cd			"$env:mman\languages"

#--------------------------------------------------------------------------------
#- Version for mmpp post-processing:
#- 	- Remove empty lines
#- 	- Remove XML comment lines
#--------------------------------------------------------------------------------
#	| ? { -not [String]::IsNullOrWhiteSpace($_) } |
#	https://serverfault.com/questions/868789/regex-works-everywhere-tried-regex101-and-regstorm-net-except-powershell
#--------------------------------------------------------------------------------
mmpp mmLanguages.mmp | `
	select-string -pattern '<!--' -notmatch | `
	out-file -enc oem mmLanguages.xml
#--------------------------------------------------------------------------------
write-host 'cp mmLanguages.xml "$env:appendices\textlanguage\mmLanguages.xml"'
cp mmLanguages.xml "$env:appendices\textlanguage\mmLanguages.xml"

#--------------------------------------------------------------------------------
if (Test-Path "filesystem::\\buzz\dev\MiramoPDF\MiramoCore\composerCore\XML" ) {
	write-host "Able to access BUZZ folders. Good!"
	write-host "  "
	}
else {
	write-host "Cannot access BUZZ folder:"
	write-host "	\\buzz\dev\MiramoPDF\MiramoCore\composerCore\XML"
	write-host "	Re-activate network drive share!"
	write-host "	exiting ..."
	cd $startFolder
	exit
}
#--------------------------------------------------------------------------------
write-host 'cp mmLanguages.xml \\buzz\dev\MiramoPDF\MiramoCore\composerCore\XML\mmLanguages.xml'
cp mmLanguages.xml \\buzz\dev\MiramoPDF\MiramoCore\composerCore\XML\mmLanguages.xml

# hunspellDictionaries moved down a level on transition to version 4.0
if (Test-Path "filesystem::\\buzz\dev\MiramoPDF\MiramoDeploy\resources\hunspellDictionaries" ) {
	write-host 'rm -r \\buzz\dev\MiramoPDF\MiramoDeploy\resources\hunspellDictionaries'
	rm -r \\buzz\dev\MiramoPDF\MiramoDeploy\resources\hunspellDictionaries\*

	write-host 'cp -r hyphenation\mmHunspellDicts\*   \\buzz\dev\MiramoPDF\MiramoDeploy\resources\hunspellDictionaries'
	cp -r hyphenation\mmHunspellDicts\*  \\buzz\dev\MiramoPDF\MiramoDeploy\resources\hunspellDictionaries
} elseif (Test-Path "filesystem::\\buzz\dev\MiramoPDF\MiramoDeploy\resources\MiramoCore\hunspellDictionaries" ) {
	write-host 'rm -r \\buzz\dev\MiramoPDF\MiramoDeploy\resources\MiramoCore\hunspellDictionaries'
	rm -r \\buzz\dev\MiramoPDF\MiramoDeploy\resources\MiramoCore\hunspellDictionaries\*

	write-host 'cp -r hyphenation\mmHunspellDicts\*   \\buzz\dev\MiramoPDF\MiramoDeploy\resources\MiramoCore\hunspellDictionaries'
	cp -r hyphenation\mmHunspellDicts\*  \\buzz\dev\MiramoPDF\MiramoDeploy\resources\MiramoCore\hunspellDictionaries
	
} else {
	write-host "*** CANNOT FILE HUNSPELL DICTIONARIES"
	write-host " ...exiting"
	cd $startFolder
	exit
}

#--------------------------------------------------------------------------------
cd $startFolder
#--------------------------------------------------------------------------------
#
# Check if ctrl+C was pressed and quit if so.
#    if ([console]::KeyAvailable) {
#        $key = [system.console]::readkey($true)
#        if (($key.modifiers -band [consolemodifiers]"control") -and ($key.key -eq "C")) {
#            Write-Warning "Quitting, user pressed control C..."
#            break
#        }
#
#
#   [Console]::TreatControlCAsInput = $true # at beginning of script
#
#    if ([Console]::KeyAvailable){
#
#        $readkey = [Console]::ReadKey($true)
#
#        if ($readkey.Modifiers -eq "Control" -and $readkey.Key -eq "C"){                
#            # tasks before exit here...
#            return
#        }
#
#    }
#
