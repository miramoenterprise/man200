#--------------------------------------------------------------------------------
$env:composer		= "mmc"
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
$mmVersion	= miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
$mmVersion	= $mmVersion | select-string -pattern "iramo"
$mmVersion	= $mmVersion -replace 'miramo version:'
$mmVersion	= $mmVersion.trim()
$env:mmVersion  = "$mmVersion"
$env:startTime  = Get-Date -format "yyyy-MMM-dd HH:mm"
#--------------------------------------------------------------------------------

$env:docMetaData	= "Composer = $env:composer($env:mmVersion) Test font='$env:exFontFamily1' ($env:startTime)"

$outputFile	= "textSymbols"
write-host "=================== $outputfile"
write-host "=================== $env:docMetaData"

$env:composer		= "mmc"
#--------------------------------------------------------------------------------
#- exit
mmdoc2 -mm -t xnote $outputFile
#--------------------------------------------------------------------------------
exit
if ($lastexitcode -eq 0) {
	write-host	"SUCCESS"
	write-host	"Created: $outputFile"
	copy-item	"fnote_hindi.pdf"	-destination	"$outputFile"
}
#--------------------------------------------------------------------------------

