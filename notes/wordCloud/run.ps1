
[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
#----------------------------------------------------------------
# miramo -M $env:control\control.mmp $inFile | out-file  $tmpFile -enc utf8
# mmpp wordCloud.mmp  | out-file  $tmpFile -enc utf8
# miramo -M -Opdf wc.pdf wordCloud.mmp
mmpp wordCloud.mmp  | out-file  wc.mm  -enc utf8
miramo -composer mmc -Opdf wc.pdf wc.mm

#----------------------------------------------------------------
[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii

