#--------------------------------------------------------------------------------
#- run.ps1
#--------------------------------------------------------------------------------
#

$env:imageFileName	= "test.tif"

# mmpp -Mfile miramo.in | out-file -enc utf8 miramo.xml
mmpp miramo.in | out-file -enc utf8 miramo.xml


# miramo -composer fmc -imageCache Y -PDFjobopts transparent -PDFcmyk Y -Tfile cmykaTestTpl.mif -Opdf fmc.pdf miramo.xml

miramo -composer mmc -Tfile cmykaTestTpl.mfd -Opdf mmc.pdf miramo.xml
exit

