:: --------------------------------------------------------------------------------
::  run.cmd
:: --------------------------------------------------------------------------------

set macroDIR=macros
set templateDIR=templates

set inputFile=woodProductDataInput.mm
set outputFilePDF=woodProductStats1.pdf
set outputFileFM=woodProductStats1.fm
set mmppFile=woodProductDataInput.mmxml
set templateFile=woodCategoryTpl.mif

set templateFile=%templateDIR%\%templateFile%
mmpp -Mfile %macroDIR%\control.mmp %inputFile%  > %mmppFile% 

miramo -composer fmc -Tfile "%templateFile%" -Ofm %outputFileFM% -Opdf %outputFilePDF% %mmppFile%
