@--------------------------------------------------------------------------------
@- control.mmp
@--------------------------------------------------------------------------------
<MiramoXML
	xmlns:fmc="http://www.miramo.com/fmc"
	xmlns:mmc="http://www.miramo.com/mmc"
	>
<#def> inTableBody		0	@--- Flag
@--------------------------------------------------------------------------------
@-	Wood product type
@-		Production	Consumption
@-		Imports		Cubic Metres	$$$
@-		Exports		Cubic Metres	$$$
@--------------------------------------------------------------------------------
<@include> ${macroDIR}/formatNumber.mmp
@- <@include> ${macroDIR}/tblColumnWidths.mmp
@--------------------------------------------------------------------------------
<@macro> startYear	{<#def> startYear	$1}
<@macro> End		{</Tbl></P></MiramoXML>}
@--------------------------------------------------------------------------------
<@macro> T1 {
	@------------------------------------------------------------------------
	@- Section titles & table title header rows (one for each language)
	@- We'll put the language name, and text into arrays
	@- since they are needed twice. (For section title & tbl header rows)
	@------------------------------------------------------------------------
	<#def> T1.language		$1
	<#def> T1.text			$2
	<#ifndef> T1.count		0
	@------------------------------------------------------------------------
	<#def> T1.count			@eval(<#T1.count> + 1)
	<#def> T1.language[<#T1.count>]	{<#T1.language>}
	<#def> T1.text[<#T1.count>]	{<#T1.text>}
	@------------------------------------------------------------------------
}
@--------------------------------------------------------------------------------
<@macro> processCells {
	@------------------------------------------------------------------------
	@- Utility macro. Called by: macros '<|A>' and '<|B>'
	@------------------------------------------------------------------------
	<#def> inTableBody		1	@--- Flag
	<#def> cellTextAlign		{textAlign="start"}
	<#def> cellTextAlign		{}
	@------------------------------------------------------------------------
	<#def> paraDef			$1
	@------------------------------------------------------------------------
	@for(cell = 2 to $#) {
		<#def> cellTextAlign	{A="R"}
		<#def> cellTextAlign	{}
		<#def> pC.cellValue	{$cell}
		@----------------------------------------------------------------
		@- Include 000's separator.
		@- Comment out next three macro lines if 000's separator unwanted
		@- Edit 'formatNumber.mmp' to change 000's separator
		@----------------------------------------------------------------
		@if(@isNum(<#pC.cellValue>)) {
			<|formatNumber> <#pC.cellValue>
			<#def> pC.cellValue	{<#fN.number>}
			}
		@----------------------------------------------------------------
		@if($$cell = 2) {
			<#def> cellTextAlign	{textAlign="end"}
			<#def> cellTextAlign	{A="L"}
			}
		@----------------------------------------------------------------
		@- Empty column in middle, to allow for tbl ruling break.
		@----------------------------------------------------------------
		@if($$cell = 8) {
			<Cell/>
			}
		@----------------------------------------------------------------
		<Cell><P paraDef="<#paraDef>" <#cellTextAlign> ><#pC.cellValue></P></Cell>
		}
}
@--------------------------------------------------------------------------------
<@macro> dataTypeHeaderRows {
	@------------------------------------------------------------------------
	@- Utility macro. Called by: macro '<|startTable>'
	@- After the wood category headers
	@------------------------------------------------------------------------
	<#def> dth.argCount	$#
	<#def> dth.langCount1	@eval((<#dth.argCount> - 1) / 2 + 1)
	<#def> dth.langCount2	@eval(<#dth.langCount1> + 1)
	<#def> dth.type		$1
	<#def> dth.langText1	{}
	<#def> dth.langText2	{}
	@------------------------------------------------------------------------
	@if(<#dth.type> = Main ) {
		@----------------------------------------------------------------
		@- Accummulate the parameters for <|Main> into two strings.
		@- E.g. Production|Production|Produccion|Consumption|Consommation|Consumo
		@- needs to be converted to two strings thus:
		@- 	Production Production Produccion
		@- 	Consumption Consommation Consumo
		@- (All this & the headings should really come from the data!)
		@----------------------------------------------------------------
		@for(text1 = 2 to <#dth.langCount1>) {
			<#def> dth.langText1	{<#dth.langText1> $text1 }
			}
		@for(text2 = <#dth.langCount2> to <#dth.argCount>) {
			<#def> dth.langText2	{<#dth.langText2> $text2 }
			}
		@----------------------------------------------------------------
		<#def> tblUnits1	{1000 m<Font superscript="Y">3</Font>}
		<#def> tblUnits2	{<#tblUnits1>}
		<#def> tblUnits3	{m<Font superscript="Y">3</Font>/1000<br/>capita}
		}
	@------------------------------------------------------------------------
	@if(<#dth.type> = Imports OR <#dth.type> = Exports ) {
		<#def> dth.langText1	{}
		@for(text1 = 2 to $#) {
			<#def> dth.langText1	{<#dth.langText1> $text1 }
			}
		@----------------------------------------------------------------
		<#def> dth.langText2	{<#dth.langText1>}
		<#def> tblUnits1	{1000 m<Font superscript="Y">3</Font>}
		<#def> tblUnits2	{1000<br/>$}
		<#def> tblUnits3	{unit<br/>value}
		}
	@------------------------------------------------------------------------
	@- Include data type headers: 'Production ...   Consumption'
	@------------------------------------------------------------------------
 	<Row type="header" >
		<Cell columnSpan="6" ><P paraDef="P_TblDataTypeTitle"><#dth.langText1></P></Cell>
		<Cell/>
		<Cell columnSpan="R" ><P paraDef="P_TblDataTypeTitle"><#dth.langText2></P></Cell>
	</Row>
	@------------------------------------------------------------------------
	@- Units, e.g. 1000 m3 , m3/1000 capita
	@------------------------------------------------------------------------
 	<Row type="header" >
		<Cell columnSpan="6" topRule="Very Thin" ><P paraDef="P_TblDataUnits"><#tblUnits1></P></Cell>
		<Cell/>
		<Cell columnSpan="5" topRule="Very Thin" ><P paraDef="P_TblDataUnits"><#tblUnits2></P></Cell>
		<Cell columnSpan="1" topRule="Very Thin" ><P paraDef="P_TblDataUnits" A="R" ><#tblUnits3></P></Cell>
	</Row>
	@------------------------------------------------------------------------
	@- Do year data. Same for Production/Consumption, Imports & Exports
	@------------------------------------------------------------------------
	<#def> endYear		@eval(<#startYear> + 4)
	@------------------------------------------------------------------------
 	<Row type="header" >
		<Cell topRule="Very Thin" ><P paraDef="P_TblDataYear" /></Cell>
	@for(year = <#startYear> to <#endYear>) {
		<Cell topRule="Very Thin" ><P paraDef="P_TblDataYear" >$$year</P></Cell>
		}
		<Cell/>
	@for(year = <#startYear> to <#endYear>) {
		<Cell topRule="Very Thin" ><P paraDef="P_TblDataYear" >$$year</P></Cell>
		}
		<Cell topRule="Very Thin" ><P paraDef="P_TblDataYear" ><#endYear></P></Cell>
	</Row>
	@------------------------------------------------------------------------
 	<Row type="header" height="0.4mm" >
		<Cell columnSpan="6" topRule="Very Thin" />
		<Cell/>
		<Cell columnSpan="6" topRule="Very Thin" />
	</Row>
	@------------------------------------------------------------------------
	@- Final, spacer header row.
	@------------------------------------------------------------------------
 	<Row type="header" height="0.2mm" />
	@------------------------------------------------------------------------
}
@--------------------------------------------------------------------------------
<@macro> startCategory {
	@------------------------------------------------------------------------
	@- E.g. 'Wood-based panels'
	@- Output first page of category, preceding table.
	@- Use "MP_sectionStart" master page
	@------------------------------------------------------------------------
	<P paraDef="Body" pgfPosition="R" >
		<MasterPageRule Page="MP_sectionStart" />
	</P>
	@------------------------------------------------------------------------
	@- Arrays of language names and assoc. text defined in '<|T1>' macro
	@------------------------------------------------------------------------
	@- Adobe Arabic
	@for(language = 1 to <#T1.count>) {
		<#def> sC.language	 $T1.language[$$language]
		<#def> sC.text		 $T1.text[$$language]
		<P paraDef="P_categoryTitle<#sC.language>" ><#sC.text></P>
		}
}
@--------------------------------------------------------------------------------
<@macro> startTable {
	@------------------------------------------------------------------------
	@- Called by: macros '<|Main>', '<|Imports>' and '<|Exports>'
	@------------------------------------------------------------------------
	@- <#tblType> is one of Main, Imports, Exports
	@- 'Main' is for production / consumption
	@------------------------------------------------------------------------
	<#def> tblType		$1
	@------------------------------------------------------------------------
	@if(<#inTableBody>) {
		</Tbl>
		</P>
		<#def> inTableBody	0
		}
	@------------------------------------------------------------------------
	@if($1 = Main) {
		<|startCategory>
		}
	@------------------------------------------------------------------------
	@- Start table of data (at top of page):
	@------------------------------------------------------------------------
	<P paraDef="Body" >			@--- Tbl container paragraph
	<Tbl tblDef="Main" tblPosition="P" >
	@------------------------------------------------------------------------
	@- <|tblColumnWidths> <#tblType>
	@------------------------------------------------------------------------
	@- Arrays of language names and assoc. text defined in '<|T1>' macro
	@------------------------------------------------------------------------
	@for(language = 1 to <#T1.count>) {
		<#def> sC.language	 $T1.language[$$language]
		<#def> sC.text		 $T1.text[$$language]
		<Row type="header" >
		<Cell columnSpan="R" ><P paraDef="P_mainTableTitle<#sC.language>" ><#sC.text></P></Cell>
		</Row>
		}
	@------------------------------------------------------------------------
	<|dataTypeHeaderRows> $*
	@------------------------------------------------------------------------
	<#def> T1.count		0
}
@--------------------------------------------------------------------------------
@- Three slightly different types of table (all with same column widths):
@--------------------------------------------------------------------------------
<@macro> Main {<|startTable> Main|$*}
<@macro> Exports {<|startTable> Exports|$*}
<@macro> Imports {<|startTable> Imports|$*}
@--------------------------------------------------------------------------------
<@macro> A {
	@------------------------------------------------------------------------
	@- 'A' denotes either the world or a region / continent
	@------------------------------------------------------------------------
	<#def> A.name		$1
	@if(<#A.name> = World ) {
		@----------------------------------------------------------------
		@- 'world' data
		@----------------------------------------------------------------
 		<Row fmc:fill="6" mmc:fillTint="3%" fillColor="Black" leftMargin="0.1mm" withNext="Y" >
		}
	@else {
		@----------------------------------------------------------------
		@- 'region / continent ' data
		@----------------------------------------------------------------
 		<Row height="2mm" withPrevious="Y" />	@--- Put space above new region
		@----------------------------------------------------------------
 		@- <Row fill="5" withNext="Y" >
 		<Row fmc:fill="5" mmc:fillTint="10%" fillColor="Black" leftMargin="0.1mm" withNext="Y" >
		}
	@- <|processCells> P_regionData|$*
	<|processCells> P_worldData|$*
	</Row>
}
@--------------------------------------------------------------------------------
<@macro> B {
 	<Row leftMargin="1.5mm" >
	<|processCells> B|$*
	</Row>
}
@--------------------------------------------------------------------------------
<@macro> B5 {
	@------------------------------------------------------------------------
	@- 'B5' is same as 'B', except for preceding empty spacer row.
	@------------------------------------------------------------------------
 	<Row height="2.0mm" withPrevious="Y" />
	<|B> $*
}
@--------------------------------------------------------------------------------
