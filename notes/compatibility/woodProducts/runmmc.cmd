:: --------------------------------------------------------------------------------
::  runmmc.cmd
:: --------------------------------------------------------------------------------

set macroDIR=macros
set templateDIR=templates

set inputFile=woodProductDataInput.mm
set outputFilePDF=woodProductStats1.pdf
set outputFilePDF=mmc.pdf
set outputFileFM=woodProductStats1.fm
set mmppFile=woodProductDataInput.mmxml
set templateFile=woodCategoryTpl.mfd

set templateFile=%templateDIR%\%templateFile%
mmpp -Mfile %macroDIR%\control.mmp %inputFile%  > %mmppFile% 

miramo -composer mmc -Tfile "%templateFile%" -Opdf %outputFilePDF% %mmppFile%
