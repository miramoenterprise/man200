#--------------------------------------------------------------------------------
# runPDF.ps1
#--------------------------------------------------------------------------------

$env:macroDIR 		= "C:\u\rick\tmp\woodProducts\macros"
$env:macroDIR 		= "macros"

$env:templateDIR 	= "C:\u\rick\tmp\woodProducts\templates"
$env:templateDIR 	= "templates"

$inputFile		= "input.xml"
$inputFile		= "C:\u\rick\tmp\woodProducts\woodProductDataInput.mm"
$inputFile		= "woodProductDataInput.mm"

$outputFilePDF		= "woodProductStats1.pdf"
$outputFileFM		= "woodProductStats1.fm"

$mmppFile		= "woodProductDataInput.mmxml"

$templateFileFMC 	= "woodCategoryTpl.mif"
$templateFileMMC 	= "woodCategoryTpl.mfd"
$macroFile 		= "controlMMC.mmp"
$macroFile 		= "controlBOTH.mmp"


$templateFileFMC 	= "$env:templateDIR\$templateFileFMC"
$templateFileMMC 	= "$env:templateDIR\$templateFileMMC"
$macroFile 		= "$env:macroDIR\$macroFile"

write-host "Template fileFMC:	$templateFileFMC"
write-host "Template fileMMC:	$templateFileMMC"
write-host "Input file:		$inputfile"
write-host "Macro file:		$macroFile"

#- mmpp -Mfile "$macroFile"  $inputFile | Out-File "$mmppFile" -Encoding utf8
# mmpp -Mfile "$macroFile"  $inputFile | Out-File "$mmppFile" -Encoding oem
# mmpp -Mfile "$macroFile"  $inputFile > "$mmppFile"
#--------------------------------------------------------------------------------
[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
mmpp -Mfile "$macroFile"  $inputFile | Out-File -Encoding utf8 "$mmppFile"
[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
#--------------------------------------------------------------------------------

# miramo -composer fmc -Tfile "$templateFile" -Ofm $outputFileFM -Opdf $outputFilePDF $mmppFile
#- miramo -composer fmc -Tfile "$templateFileFMC" -Ofm $outputFileFM -Opdf fmc.pdf $mmppFile

miramo -composer mmc -Tfile "$templateFileMMC" -Opdf mmc.pdf $mmppFile
