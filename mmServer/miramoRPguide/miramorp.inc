@--------------------------------------------------------------------------------
@- miramorp.inc
@- include files for miramoRP 
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<#def> inMFD		1
@--------------------------------------------------------------------------------
<@include>	${mmServer}/miramoRPguide/cover.inc
@--------------------------------------------------------------------------------
<chapterStart
	dest="miramorp.miramoRPguide"
	listLabel="N"
	>miramoRP remote processing
</chapterStart>
<@include>	${mmServer}/miramoRPguide/miramoRPguide
@--------------------------------------------------------------------------------
<#def> inMFD		0
<chapterEnd/>
@--------------------------------------------------------------------------------
