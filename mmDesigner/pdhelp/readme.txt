@--------------------------------------------------------------------------------
@- mmPageDesignerHelp.mmp
@--------------------------------------------------------------------------------
<?xml version="1.0" encoding="utf-8"?>
<@include> ${pdhelp}/mmppMacros/defs.mmp
<@include> ${pdhelp}/mmppMacros/textDefs.mmp
<@include> ${pdhelp}/mmppMacros/propertyGroupsRootElement.mmp
<@include> ${pdhelp}/mmppMacros/objectAndProperty.mmp
<@include> ${pdhelp}/mmppMacros/mmElement.mmp
<@include> ${pdhelp}/mmppMacros/mmProperty.mmp
<@include> ${pdhelp}/mmppMacros/mmDialog.mmp
<@include> ${pdhelp}/mmppMacros/mmObject.mmp
<@include> ${pdhelp}/mmppMacros/propertyGroup.mmp
<@include> ${pdhelp}/mmppMacros/includeFile.mmp
<mmPageDesignerHelp>
@- <propertyGroupsRootElement>

<!-- Defaults: 
=================================================================================
<mmPagesDesignerHelp>

<mmElement>
	<xPropertyGroup>
		<mmProperty />
	</xPropertyGroup>
</mmElement>

<mmDialog>
	<mmObject />
</mmDialog>


</mmPagesDesignerHelp>

=================================================================================
<mmElement attributes:

	id="DocDefId"		Id of parent element for property
	showInDesigner="Y"	Dunno why 'show' is here ...
	folderName="folderName"	Folder name for help file, e.g. folderName="ListLabel".
				The name of a folder in, currently one or all of:
					pdhelp\dataFiles\english\
					pdhelp\dataFiles\german\
					pdhelp\dataFiles\polish\

=================================================================================
<xPropertyGroup id="FontDefFont" name="Font" >


=================================================================================
<mmProperty attributes:

	id="idName"		Required. E.g. id="aFontColorId"

	file="filename"		File containing help text. Optional.
				E.g. file="jobName.pdh" The file is located in the folder
				specified by the <mmElement> folderName attribute

	resource="N | Y"	The help file is too big, so put it in a QT resource file.
				Resource file base name = '<elementId>.<propertyId>.pdhtml'
				

	toolTip="text"		If file attribute present and no value for 'toolTip',
				then use default tooltip: "Right-click label for Help."

	generatedOnly="N"	Properties which cannot be displayed in mmPageDesigner,
				e.g. for text kerning, this value would be "Y"  

	showInDesigner="Y"	Properties which are displayed/selectable in mmPageDesigner,
				but not used elsewhere - eg ListLabel display number.
				Also properties which are TBA eg allowLineBreaksAfter.

	pdfTooltips="Y"		Properties to display in mmComposer pdf tooltips, defaults
				to showInDesigner value 

	displayName="..."	Properties where we want to use a display name other than
				the attribute name, eg in ListLabel

	Other comments:
		inheritedFrom can probably be disregarded

=================================================================================
<mmDialog attributes:

	name="dialogName"	Name of dialog in Qt Creator.
				Required. E.g. name="ListLabelForm"

	folderName="foldername" Folder name for help file, e.g. folderName="ListLabel"
				See <mmElement> foldername above.

=================================================================================
<mmObject attributes:

	name="objectName"	Name of object in dialog in Qt Creator.

	title="text"		Help dialog title text.

	toolTip="text"		If file attribute present and no value for 'toolTip',
				then use default tooltip: "Right-click label for Help."
				Required. E.g. name="propertiesGroupBox"

	file="fileName"		File containing help text. Optional.
				E.g. file="jobName.pdh" The file is located in the folder
				specified by the <mmDialog> folderName attribute

=================================================================================
<Help attributes:

	These attributes are optionally included in the help file itself.
	Never below.

	width="Npx"		Initial help dialog display width. (Overide of default.)

	height="Npx"		Initial help dialog display height. (Overide of default.)

	file="fileName"		Qt resource filename for help. Auto-generated from
				'file' attribute value on <mmProperty> and <mmObject>.

	title="text"		Help dialog title text. From: <mmProperty> and <mmObject>.

	E.g.

	<propertyHelp
		helpHeight="A | B | Npx "
		helpWidth="Npx"
		resource="N | Y"
		title="text"
		>

=================================================================================
-->
