@--------------------------------------------------------------------------------
@- rowSplittingAll.pdh
@- 
@- Describes the properties to enable table row-splitting
@- 
@- 
@- This should be split into
@- 
@- 	splitRows
@- 		Rows containg cells which are too large to fit in a single text
@-		column or page.
@- 
@- 	splitStraddledRows
@- 		Split row straddles (vertically-merged cells) which are too
@-		too large to fit in a single page or text column.
@- 
@- 	splitMergedRows
@- 
@--------------------------------------------------------------------------------

<propertyHelp>

@--------------------------------------------------------------------------------
<sectionTitle>Summary</sectionTitle>
@--------------------------------------------------------------------------------
<bp>
Enable splitting rows within vertically-merged table cells between text columns and pages. 
See <fB>Notes</fB> below.
</bp>

<bp>
This feature is enabled/disabled by selecting/de-selecting
the <sq>Support table row splitting</sq> preference in the Edit,
Preferences dialog.
</bp>


@--------------------------------------------------------------------------------
<sectionTitle>Details</sectionTitle>
@--------------------------------------------------------------------------------
<@skipStart>
<bp>
Figure A below illustrates the parameters of a drop shadow style.
</bp>
<tbl columns="163|R" cellMargins="6pt" >
@- <r><image file="xShadowDef.all.png" W="121" label="A" bgColor="#dadada" >Drop shadow style parameters</image>
<r><image file="xShadowDef.all.png" label="A" bgColor="#dadada" />
|
@- <tbl columns="12|34|20|259"  layout="fixed" >
<tbl columns="11|30|159"  layout="fixed" >
<r></r>
@- <r>&emsp;|<fB>sC</fB>|&ensp;|Shadow color</r>
@- <r>&emsp;|<fB>sA</fB>|&ensp;|Shadow angle</r>
@- <r>&emsp;|<fB>sL</fB>|&ensp;|Shadow length</r>
<r>&emsp;|<fB>sC</fB>|Shadow color</r>
<r>&emsp;|<fB>sA</fB>|Shadow angle</r>
<r>&emsp;|<fB>sL</fB>|Shadow length</r>
</tbl>
</r>
</tbl>

<@skipEnd>
@--------------------------------------------------------------------------------

<bp>
The following properties specify the default settings for all
<en>Row</en> elements in the table.
</bp>

@-===========================================================
<tbl columns="140min|20min|R" cellMargins="6px" layout="fixed" >

@------------------------------------------------------------
@- HEADER ROW
@------------------------------------------------------------
<r><fB>Property</fB>|&#x2003;|<fB>Description</fB></r>
@------------------------------------------------------------

@------------------------------------------------------------
@- splitRows
@------------------------------------------------------------
<r></r>
<r><pn>splitRows</pn>|&#x2003;|
Allow within-row splitting between pages and columns.
</r>
@------------------------------------------------------------

@------------------------------------------------------------
@- splitStraddledRows
@------------------------------------------------------------
<r></r>
<r><pn>splitStraddledRows</pn>&#x2003;&#x2003;|&#x2003;|
Allow rows which are vertically straddled to be split between pages and columns.
No within-row splitting occurs unless the <pn>splitRow</pn> property
is set to <pv>Y</pv>.
</r>
@------------------------------------------------------------


</tbl>


@--------------------------------------------------------------------------------
<sectionTitle>Notes</sectionTitle>
@--------------------------------------------------------------------------------
<bp>
Splitting within table rows should be avoided as much as possible.
Such splitting may produce unsightly results and is sometimes caused
by badly structured information design and presentation.
</bp>

<bp>
Splitting within table rows across columns and pages may create
problems if the output needs to conform with PDF accessibility standard
PDF/UA (and consequently WCAG 2.0/2.1 and European Union
Directive 2019/882), or for those using screen readers.
</bp>

<bp>
The <pn>splitRows</pn> and the <pn>splitStraddledRows</pn> properties
may be overridden using the <en>Row</en> element <pn>splitRow</pn> and
<pn>splitStraddledRow</pn> properties. Or, in a DITA editing context,
manually specifying <pn>mmpdf:splitRow</pn> and <pn>mmpdf:splitStraddledRow</pn> values.
This approach can help minimizing splitting within table rows across
columns and pages unless absolutely necessary.
</bp>

@- Directive (EU) 2019/882 of the European Parliament and of the Council
@- of 17 April 2019 on the accessibility requirements for products and
@- services (Text with EEA relevance)


@--------------------------------------------------------------------------------
</propertyHelp>
@--------------------------------------------------------------------------------
