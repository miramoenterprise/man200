<?xml version="1.0" encoding="utf-8" ?>

<propertyHelp 
	helpWidth="430" 
	helpHeight="B" 
	>

<ref>
<en>JobOptionsDef</en> <pn>definition</pn>
</ref>

<sectionTitle>Summary</sectionTitle>


<bp>
Reduce the pixel density of applicable images with 
an effective PPI greater than the value of <pn>maximumPPI</pn>.
</bp>

<sectionTitle>Description</sectionTitle>

<bp>
Applicable image types are BMP, GIF and PNG (except for palette, PNG color
type 3) bitmap images where the rendered width <fI>and</fI> height (X and Y) PPI
is greater than the value of <pn>maximumPPI</pn>.
</bp>

<bp>
PDF, SVG and
images included with the <en>Image</en> <pn>jobOptionsDef</pn> property
set to <pv>none</pv> are not downsampled.
</bp>

<tbl columns="120|310" cellMargins="6px" layout="fixed" >

<r><fB>Property</fB>|<fB>Description</fB></r>

<r></r>
<r><pn>maximumPPI</pn>|
If an applicable image would have an effective X-ppi <fI>and</fI> Y-ppi, i.e.
as would be rendered in the PDF output, greater than the value
of <pn>maximumPPI</pn>, the image is downsampled so that
the <fI>lesser</fI> of the X-ppi or Y-ppi is reduced to the value
specified by the value of <pn>downsamplePPI</pn>.
</r>

<r></r>
<r><pn>downsamplePPI</pn>|
Downsample target value.
</r>

<r></r>
<r><pn>downsampleFilter</pn>|
Filter to use for downsampling.
</r>

<r></r>
<r><pn>compression</pn>|
If downsampling is in effect,
use <pv>jpeg</pv> for photographic images, <pv>zip</pv> for
line drawings. In the general case smaller sizes are achieved
using <pv>jpeg</pv>.
</r>

<r></r>
<r><pn>quality</pn>|
If downsampling is in effect, set JPEG quality. 
Ignored if <pn>compression</pn> is set to <pv>zip</pv>.
</r>

</tbl>

<bp>
<en>JobOptionsDef</en> settings may be applied to 
an entire document <fI>via</fI> the <en>DocDef</en> <pn>jobOptionsDef</pn>
 property or to individual images
using the corresponding <en>Image</en> element's <pn>jobOptionsDef</pn> property.
Different values may be specified in each case.
</bp>

<sectionTitle>Notes</sectionTitle>


<bp>
<fB>Note 1</fB>
In the current
context PPI is the same as and interchangeable with DPI, <sq>dots per inch</sq>.
</bp>

<bp>
<fB>Note 2 </fB>
In the general case the value of <pn>maximumPPI</pn> should be at least
1.5 times the value of <pn>downsamplePPI</pn>. Reducing image density by a
small percentage results in poor quality and performance.
</bp>

<bp spaceBelow="0pt" >
<fB>Note 3</fB>
The <pn>maximumPPI</pn> and the <pn>downsamplePPI</pn> refer to the image
pixel X-ppi and Y-ppi density when rendered within the allocated
space (width and height) in the output PDF.
For example, two images, <sq><fM>imageA.png</fM></sq> and <sq><fM>imageB.png</fM></sq>, may
have identical pixel width and height values: 400px &#xD7; 600px.
If <pn>maximumPPI</pn> is set to <pv>180</pn> and the PDF width and height
dimensions of <sq><fM>imageA.png</fM></sq> are 2in &#xD7; 3in, the
image <fI>is</fI> downsampled
since <fI>both</fI> the X-ppi and the Y-ppi are greater
than 180 (400 / 2 and 600 / 3).
</bp>

<bp spaceAbove="0pt" >
If the PDF width and height
dimensions of <sq><fM>imageB.png</fM></sq> are 3in &#xD7; 3in, the image
is <fI>not</fI> downsampled since the X-ppi is less than 188 (400 / 3).
</bp>

<bp spaceBelow="0pt" >
<fB>Note 4</fB> Intrinsic image PPI metadata is immaterial to 
the rules for downsampling.
</bp>
<bp spaceAbove="0pt" >
For example if a 600px &#xD7; 400px image has an intrinsic image PPI metadata value
of 300 and the value of <pn>maximumPPI</pn> is set to <pv>220</pv> and
the width and height of the image in the output PDF 4in &#xD7; 2in, the
image is <fI>not</fI> downsampled. This is because the actual, effective width
PPI is 600 / 4 = 150, i.e. below the value of <pn>maximumPPI</pn>.
</bp>

<bp spaceBelow="0pt" >
<fB>Note 5</fB> If the PDF document contains multiple instances
of the same image the bitmap representation of the image is
included in the PDF once only.
(An image is the same if it has the same file path and name, or the same URL.)
When the same image is included multiple times at different
sizes, the size with the lowest X-ppi and Y-ppi values is used as the
basis for deciding if downsampling should be applied.
</bp>
<bp spaceAbove="0pt" >
For example if <pn>maximumPPI</pn> is set
to <pv>220</pv> and a 600px &#xD7; 400px image is included twice
in the output PDF, first with width and
height 2in &#xD7; 1in (X-ppi = 300, Y-ppi = 400), and
second with width and height 4in &#xD7; 2in (X-ppi = 150, Y-ppi = 200) the
image is <fI>not</fI> downsampled. The is because the second image representation has
one (or both) X-ppi or Y-ppi value less than the value of <pn>maximumPPI</pn>.
</bp>

<bp>
<fB>Note 6 </fB>
Transparency components (<fM>smask</fM>s) of images are not downsampled.
</bp>

<bp/>
<bp/>




</propertyHelp>

