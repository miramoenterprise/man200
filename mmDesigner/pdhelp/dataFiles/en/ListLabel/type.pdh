@--------------------------------------------------------------------------------
@- ListLabel.type
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<propertyHelp>

@--------------------------------------------------------------------------------
<sectionTitle>Summary</sectionTitle>

<bp>
Type <pv>runin</pv> is often used for auto-numbered captions and titles.
</bp>
<bp>
Type <pv>fixed</pv> is often used for auto-numbered headings and lists.
</bp>
<bp>
        When type is set to <pv>runin</pv> insert a space or other special
        characters <fI>after</fI> the <fB>&lt;c&gt;</fB> autonumber indicator in 'Label format'.
</bp>

@--------------------------------------------------------------------------------
<sectionTitle>Runin labels</sectionTitle>

<bp>
Figure A shows an example of the output when <pv>runin</pv> is selected.
</bp>

<bp>
<image file="ListLabel.listLabelRunin.png" label="A" >type="runin"</image>
</bp>

<bp>
When <pv>runin</pv> is selected the list label runs into the first
line of paragraph text. If the list label contains an autonumber,
the width of the list label expands to accommodate the current value
(length) of the autonumber counter.
</bp>

<bp>
The list label starts at the start position of the paragraph
first line indent (<fB>pgf.fLI</fB> in Figure A).
</bp>

@--------------------------------------------------------------------------------
<sectionTitle>Fixed-width labels</sectionTitle>
<bp>
Figure B shows an example of the output when <pv>fixed</pv> (fixed-width
list label) is selected.
</bp>

<bp>
<image file="ListLabel.listLabelFixedStart.png" label="B" >type="fixed"</image>
</bp>

<bp>
When <pv>fixed</pv> is selected the width and position of the list label
are specified using the <en>ListLabel</en> <pn>width</pn> and
<pn>labelIndent</pn> properties (<fB>lW</fB> and <fB>lI</fB> in Figure B).
<pn>labelIndent</pn> is measured from the start side of the text frame.
</bp>

@--------------------------------------------------------------------------------
</propertyHelp>
@--------------------------------------------------------------------------------
