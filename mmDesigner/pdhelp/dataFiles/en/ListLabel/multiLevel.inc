@--------------------------------------------------------------------------------
@- multiLevel.inc
@- (in counter element)
@--------------------------------------------------------------------------------
<#def> iF.folderName	ListLabel
@- <#def> iF.folderName	{${pdhelp}/dataFiles/${LC}/<#iF.folderName>}

@--------------------------------------------------------------------------------
<sectionTitle>Example</sectionTitle>
@--------------------------------------------------------------------------------
<bp>
Figure A shows a typical numbering scheme that includes chapter numbers,
heading numbers and sub-heading numbers.
</bp>

<image file="ListLabel.paragraphNumbering1.png" label="A"
	folderName="<#iF.folderName>"
	>Typical numbering scheme</image>

<bp>
Three paragraph formats are used to implement the numbering scheme shown
in Figure A:
<fn>P_Chapter</fn> (<pn>type</pn>=<pv>runin</pv>),
<fn>P_Heading1</fn> (<pn>type</pn>=<pv>fixed</pv>) and
<fn>P_Heading2</fn> (<pn>type</pn>=<pv>fixed</pv>).
</bp>


<bp>
Figures B, C and D show the configuration of the <en>ListLabel</en> autonumbering for
each of the
<fn>P_Chapter</fn>,
<fn>P_Heading1</fn> and
<fn>P_Heading2</fn> paragraphs.
</bp>



@--------------------------------------------------------------------------------
<bp>
Figure B shows the configuration of the <en>ListLabel</en> autonumbering
for the <fn>P_Chapter</fn> paragraph format.
</bp>

<image file="ListLabel.autonumberP_ChapterV2.png" label="B"
	@- folderName="<#iF.folderName>"
	><fn>P_Chapter</fn> autonumber configuration</image>

<bp>
The settings shown in Figure B are explained in the following table.
</bp>

<tbl columns="36|80|R" >
<r><fB>Item</fB>|<fB>Property</fB>|<fB>Description</fB></r>
<r><fB>A</fB>|numberPrefix|
<fn>P_Chapter</fn> is at the highest level in this numbering
scheme. There is no parent.
</r>
<r><fB>B</fB>|parent|
<fn>P_Chapter</fn> is at the highest level in this numbering
scheme. There is no parent.
</r>
<r><fB>C</fB>|seriesLabel|
The seriesLabel for <fn>P_Chapter</fn> is xxxx.
Any string may be used for the value of seriesLabel, provide it is
unique to the series, e.g. xy, C, chapter.
</r>
<r><fB>D</fB>|Label format|
This is an autonumber type runin so
insert a space character after the autonumber indicator &lt;c&gt;
to ensure space between the label text and the paragraph text.
</r>
<r><fB>E</fB>|Label format|
The word <sq>Chapter</sq>, or any text, may be included as fixed text in
the list label before the
autonumber indicator &lt;c&gt;.
</r>
</tbl>

@--------------------------------------------------------------------------------
<bp>
Figure C shows the configuration of the <en>ListLabel</en> autonumbering
for the <fn>P_Heading1</fn> paragraph format.
</bp>

<image file="ListLabel.autonumberP_Heading1.png" label="C"
	folderName="<#iF.folderName>"
	><fn>P_Heading1</fn> autonumber configuration</image>

<bp>
The settings shown in Figure C are explained in the following table.
</bp>

<tbl columns="36|80|R" >
<r><fB>Item</fB>|<fB>Property</fB>|<fB>Description</fB></r>
<r><fB>A</fB>|numberPrefix|
The numberPrefix is set to a <sq>.</sq> character to create a
separator between the chapter number and the number of the
first level header.
</r>
<r><fB>B</fB>|parent|
The parent paragraph format is <fn>P_Chapter</fn>.
An occurrence of the <fn>P_Chapter</fn> paragraph format in the
input resets the <fn>P_Heading1</fn> counter to start at 1.
</r>
<r><fB>C</fB>|seriesLabel|
The seriesLabel for <fn>P_Heading1</fn> is yyyy.
Any string may be used for the value of seriesLabel, provide it is
unique to the series, e.g. xy, H1, heading1.
</r>
</tbl>


@--------------------------------------------------------------------------------
<bp>
Figure D shows the configuration of the <en>ListLabel</en> autonumbering
for the <fn>P_Heading2</fn> paragraph format.
</bp>

<image file="ListLabel.autonumberP_Heading2.png" label="D"
	folderName="<#iF.folderName>"
	><sq>P_Heading2</sq> autonumber configuration</image>

<bp>
The settings shown in Figure D are explained in the following table.
</bp>

<tbl columns="36|80|R" >
<r><fB>Item</fB>|<fB>Property</fB>|<fB>Description</fB></r>
<r><fB>A</fB>|numberPrefix|
The numberPrefix is set to a <sq>.</sq> character to create a
separator between the first level header number and the number of the
second level header.
</r>
<r><fB>B</fB>|parent|
The parent paragraph format is <fn>P_Heading1</fn>.
An occurrence of the <fn>P_Heading1</fn> paragraph format in the
input resets the <fn>P_Heading2</fn> counter to start at 1.
</r>
<r><fB>C</fB>|seriesLabel|
The seriesLabel for <fn>P_Heading2</fn> is zzzz.
Any string may be used for the value of seriesLabel, provide it is
unique to the series, e.g. xy, H1, heading1.
</r>
</tbl>


<bp>
The foregoing describes how to set up a typical three-level
numbering scheme. The foregoing procedure may be extended
to any number of levels, e.g. by defining <fn>P_Heading3</fn>,
 <fn>P_Heading4</fn> ... <fn>P_Heading<fI>n</fI></fn> paragraph
formats each having its predecessor as its <pn>parent</pn> value and each
having a unique <pn>seriesLabel</pn> value.
</bp>

<bp>
The paragraph format names <fI>per se</fI> are unimportant. All
that matters is the settings specified for the <pn>parent</pn> and
<pn>seriesLabel</pn>
</bp>
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------
