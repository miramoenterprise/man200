@--------------------------------------------------------------------------------
@- IndexDef.names
@- ??
@--------------------------------------------------------------------------------
<propertyHelp helpHeight="B" >

@--------------------------------------------------------------------------------
<sectionTitle>Summary</sectionTitle>
@--------------------------------------------------------------------------------

<bp>
Specify the <en>IX</en> element <pn>name</pn> value(s) to include in the index.
</bp>

@--------------------------------------------------------------------------------
<sectionTitle>Notes</sectionTitle>
@--------------------------------------------------------------------------------

<bp>
For example, if the input contains <en>IX</en> entries like the following:
</bp>
<code width="400" >
<codeCaption>Example A: <en>IX</en> index entries</codeCaption>
<!-- (A) Default index entry -->
<P paraDef="Body" >Giraffes are the tallest
animals<IX>giraffe</IX></P>

<!-- (B) Index entry (name="animals") -->
<P paraDef="Body" >Giraffes are the tallest
animals<IX name="animals">giraffe</IX></P>

<!-- (C) Index entry (name="birds") -->
<P paraDef="Body" >An albatross may have a wingspan
over 3 m<IX name="birds">albatross</IX></P>
</code>

<bp>
An index containing default index entries, i.e. with no <en>IX</en> element
<pn>name</pn> values or with <pn>name</pn> values set to <pv>index</pv>, is created
by specifying <pv>index</pv> as the value
for <en>IndexDef</en> element <pn>names</pn> property. In this case
the generated index will exclude index <en>IX</en> entries that
have <pn>name</pn> values <pv>animals</pv> or <pv>birds</pv>.
</bp>

<bp>
An <sq>Index of animals</sq> is created
by specifying <pv>animals</pv> as the value for <pn>names</pn>.
And an <sq>Index of birds</sq> is created
by specifying <pv>birds</pv> as the value for <pn>names</pn>.
</bp>

<bp>
An <sq>Index of animals and birds</sq> is created
by specifying both <pv>animals</pv> and <pv>birds</pv> as values for <pn>names</pn>.
</bp>

<bp>
A <sq>General index</sq> is created
by specifying <pv>index</pv>, <pv>animals</pv> and <pv>birds</pv>, and
all other <en>IX</en> <pn>name</pn> values that are included in the input.
</bp>

@--------------------------------------------------------------------------------
</propertyHelp>
@--------------------------------------------------------------------------------

<@skipStart>


@--------------------------------------------------------------------------------
<sectionTitle>Summary</sectionTitle>
@--------------------------------------------------------------------------------
<bp>

@- Nom du fichier d'entr�e pour le processus
@- fichier d'entr�e pour le processus
Name of the the input file for the job.
</bp>

<bp>
The input file must be in <en>MiramoXML</en> format.
</bp>


@--------------------------------------------------------------------------------
<sectionTitle>Example</sectionTitle>
@--------------------------------------------------------------------------------
<bp>
Example A and Example B are illustrations of input
in <en>MiramoXML</en> format.
</bp>

<code width="400" >
<MiramoXML>
<P>hello world</P>
</MiramoXML>
<codeCaption>Example A</codeCaption>
</code>

@- <bp>&#xA0; </bp>

<code width="400" >
<?xml version="1.0" encoding="utf-8"?>
<MiramoXML>
<P>hello world</P>
</MiramoXML>
<codeCaption>Example B</codeCaption>
</code>

@--------------------------------------------------------------------------------
</propertyHelp>
@--------------------------------------------------------------------------------
<@skipEnd>
