@--------------------------------------------------------------------------------
@- FontDef/textBox/textBoxRulings.pdh
@- id="aLeadingId"
@--------------------------------------------------------------------------------
<propertyHelp>

<sectionTitle>Summary</sectionTitle>

<bp>
Usage and application of
the <em>textBox</em> <pn>startRule</pn>, <pn>topRule</pn>,
 <pn>endRule</pn> and <pn>bottomRule</pn> settings.
</bp>

<sectionTitle>Details</sectionTitle>

<bp>
Figure <fB>A</fB> shows the placement of the rules. 
The <pn>startRule</pn> and <pn>endRule</pn> act as columns
resting on the <pn>bottomRule</pn> and supporting the <pn>topRule</pn> (lintel).
</bp>

<bp>
The <pn>startRule</pn> and <pn>endRule</pn> both occupy horizontal space.
The text surrounding the text in a <en>textBox</en> is
displaced laterally by the thickness of the
start and end rules (and
the width of the start and end margins).
</bp>

<bp>
The thicknesses of <pn>topRule</pn> and <pn>bottomRule</pn> (and the heights
of the <pn>topMargin</pn> and <pn>bottomMargin</pn>) have no effect on
text positioning.
</bp>


<image file="paraFrameRulings.png" label="A" bgColor="#dadada" >Horizontal
and vertical rulings</image>

@--------------------------------------------------------------------------------
</propertyHelp>
@--------------------------------------------------------------------------------
