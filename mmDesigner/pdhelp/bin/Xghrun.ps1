#--------------------------------------------------------------------------------
#- ghrun.ps1
#--------------------------------------------------------------------------------
#- Create mmDesigner help files
#- 'pdh' = MiramoDesigner help
#--------------------------------------------------------------------------------
write-host "This is ghrun.ps1. For updating 'general' help"
#
# Note: datafiles is source folder containing mmpp source text
# 1) Create schema XML files, e.g. 
#
# 	mmPageDesignerHelp_en.xml
# 	mmPageDesignerHelp_de.xml
#
#    containing short <Help> elements.
#
# 2) Create Qt <html> resource files when the Help text is too long.
#    Specified by setting resource="Y" on <mmProperty> and <mmObject> elements.
#    in file "$env:pdhelp/masterHelp.mmp". Resource files are copied
#    to:
#    		"$env:pdhelp/$env:lc/files" INCORRECT
#
# 3) All images used are stored in
#
# 		"$env:pdhelp/images/<language>"
#
#    and copied to:
#
#    		"$env:pdhelp/$env:lc/images"
#    if needed.
#
# 4) Create Qt resource file, 'pdhelp.qrc', that contains references to the
#    resource and image files to be compiled into MiramoDesigner, e.g.
#
#    <RCC>
#    	<qresource>
#    		<file>pdhelp/en/files/ListLabel.parent_en.pdhtml</file>
#    		<file>pdhelp/en/images/autonumberP_Heading2.png</file>
#
#    	</qresource>
#    </RCC>
#
# Items '1' through '4' above are done using the following .mmp files:
#
#--------------------------------------------------------------------------------

# deploySchemaFolder
# deployHelpFilesFolder

#--------------------------------------------------------------------------------
$env:productionRun	= 0
#--------------------------------------------------------------------------------
$env:helpMode		= "fileSystem"		#- New, filesystem HTML files
$env:outputType		= "qhrun"		#- Make 'genral' Help
						#- E.g. for keyboard shortcuts
$inputFile		= "$env:pdhelp\masterHelp.mmp"
$inputFile		= "$env:pdhelp\generalHelp.mmp"
$env:helpFilesFolder	= "$env:pdhelp\helpFiles"	# Local, test directory
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
$env:macroFile1		= "$env:pdhelp\mmppMacros\helpMacros.mmp"
#--------------------------------------------------------------------------------
#- Generated list of property ids. For reference only. Not used in processing:
#- generated in mmProperty.mmp and mmElement.mmp
	$env:propertiesList	= "$env:pdhelp\defaults\propertiesList"
#--------------------------------------------------------------------------------
#- E.g. $env:COMPOSERPDF_HOME set to \\BUZZ\dev\MiramoPDF
$mmcSchemaFolder	= "$env:COMPOSERPDF_HOME/MiramoCore/composerCore/XML"

#--------------------------------------------------------------------------------
# Copy $args array to $mmdHelpArgs
$mmdHelpArgs    = $args
if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
	write-host	"mmdHelp.ps1 ($env:pdhelp\bin)"
	write-host	"	Use: -p or -prod for production"
	exit
}
#--------------------------------------------------------------------------------
if (($args.count -eq 1) -and (($args[0] -eq "-prod") -or ($args[0] -eq "-p"))) {
	$env:productionRun	= 1
	}
else {
	write-host		  "mmdHelp: Test run. Use: -p or -prod for production ... "
}
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#- mmPageDesigner development source folders (remote host):
#- These are copy-to folders.
#--------------------------------------------------------------------------------
$startFolder	= pwd
cd		"$env:pdhelp"

#--------------------------------------------------------------------------------
function makeHelp {
	#------------------------------------------------------------------------
	#- Called once for each language, e.g. makeHelp en (see below)
	#------------------------------------------------------------------------
	$env:LC			= "$args"
	$language		= "$args"
	$env:filesFolder	= "$env:helpFilesFolder\$env:LC"
	$env:outputFile		= "$env:pdhelp\mmPageDesignerHelp_$env:LC.xml"
	$env:outputFile		= "$env:pdhelp\xxzz"
	write-host		  "mmdHelp: Initial files folder: $env:filesFolder"
	$env:mmcSchemaFile	= "mmPageDesignerHelp_$env:LC.xml"
	$env:mmcSchemaFile	= "$mmcSchemaFolder\$env:mmcSchemaFile"
	# resourceFolders "$language"
	#------------------------------------------------------------------------
	#- Remove files from tmp image folder: "$env:pdhelp/tmpImageFolder/$language"
	#- Then re-create: "$env:pdhelp/tmpImageFolder/$language"
	#- "$env:tmpImageFolder" is used in 'image.mmp' (for copying image files)
	#------------------------------------------------------------------------
	$env:tmpImageFolder	= "$env:pdhelp/tmpImageFolder/$language"
	Remove-Item "$env:tmpImageFolder" -Force  -Recurse -ErrorAction SilentlyContinue
	New-Item -ItemType Directory -Force -Path $env:tmpImageFolder | out-null
	#------------------------------------------------------------------------
	$env:tmpResourceFolder	= "$env:pdhelp/tmpResourceFolder/$language"
	Remove-Item "$env:tmpResourceFolder" -Force  -Recurse -ErrorAction SilentlyContinue
	New-Item -ItemType Directory -Force -Path $env:tmpResourceFolder | out-null
	#------------------------------------------------------------------------
	# Below is used in macro fName, in helpMacros.mmp
	$env:folderName		= "xxx"
	#------------------------------------------------------------------------
	write-host	"mmdHelp: Running command:- mmpp  $inputFile |
		out-file -enc utf8  xxTmpFirstPass"
	mmpp  "$inputFile" | out-file -enc utf8  "xxTmpFirstPass"
	#------------------------------------------------------------------------
	write-host	"mmdHelp: Running command:- mmpp -Mfile $env:macroFile1 xxTmpFirstPass |
		out-file -enc utf8  $env:outputFile"
	mmpp -Mfile "$env:macroFile1" "xxTmpFirstPass" | out-file -enc utf8  "$env:outputFile"
	#------------------------------------------------------------------------
	$mmppStatus		= $LastExitCode
	if ($mmppStatus -eq 1) {
		write-host	"mmdHelp: mmpp process FAILED" ;	exit
		}
	#------------------------------------------------------------------------
	#- Copy help image resources:
	#- (Currently help file resources are placed directly in the Qt 
	#- resource folder, using mmpp command in 'propertyHelp.mmp'.
	#- This should probably be changed to use the same method as for
	#- images.)
	#------------------------------------------------------------------------
	write-host	"mmdHelp: Running command:- xcheck val --well-formed $env:outputFile"
	xcheck val --well-formed "$env:outputFile" ;	$xmlStatus		= $LastExitCode
	if ($xmlStatus -eq 0) {
		write-host	"mmdHelp: XML file is VALID ($env:outputFile)"
		}
	else {
		write-host	"mmdHelp: XML file is INVALID !!!!  ($env:outputFile)"
		write-host	"terminating ..."	; exit
		}
	#------------------------------------------------------------------------
	if ($env:productionRun -eq 1 ) {
		#----------------------------------------------------------------
		write-host	"mmdHelp: -p[rod] option used"
		write-host	"mmdHelp: Copying html and image folders"
		write-host	"         From"
		write-host	"		$env:helpFilesFolder/$env:LC/html/general"
		write-host	"         To"
		write-host	"		$env:composerPDF_home/MiramoCore/designerApp/docs/help/$env:LC\html\general"
		#----------------------------------------------------------------
		# https://stackoverflow.com/questions/40744335/how-do-i-force-robocopy-to-overwrite-files
		# robocopy  $env:helpFilesFolder\$env:LC  $env:helpFilesFolder\en2  /E | Out-Null
		# $mmcSchemaFolder	= "$env:COMPOSERPDF_HOME/MiramoCore/composerCore/XML"
		# robocopy  $env:helpFilesFolder\$env:LC  $mmcSchemaFolder  /E | Out-Null
		# Copy files to 
		# E.g. to ../docs/help/en on BUZZ
		# E.g. to ../docs/help/de on BUZZ
		copy  $env:helpFilesFolder/$env:LC/html/general/*.html  `
	$env:composerPDF_home/MiramoCore/designerApp/docs/help/$env:LC\html\general 
		copy  $env:helpFilesFolder/$env:LC/html/general/images\*  `
	$env:composerPDF_home/MiramoCore/designerApp/docs/help/$env:LC\html\general\images
		}
	else {
		write-host	"mmdHelp: -p[rod] option NOT used, so no file copying"
		}
}
#--------------------------------------------------------------------------------
makeHelp en
cd	"$startFolder"
exit
