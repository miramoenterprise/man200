#--------------------------------------------------------------------------------
#- pdhrun.ps1
#--------------------------------------------------------------------------------
#- Create mmDesigner help files
#- 'pdh' = MiramoDesigner help
#--------------------------------------------------------------------------------
#
# Note: datafiles is source folder containing mmpp source text
# 1) Create schema XML files, e.g. 
#
# 	mmPageDesignerHelp_en.xml
# 	mmPageDesignerHelp_de.xml
#
#    containing short <Help> elements.
#
# 2) Create Qt <html> resource files when the Help text is too long.
#    Specified by setting resource="Y" on <mmProperty> and <mmObject> elements.
#    in file "$env:pdhelp/masterHelp.mmp". Resource files are copied
#    to:
#    		"$env:pdhelp/$env:lc/files" INCORRECT
#
# 3) All images used are stored in
#
# 		"$env:pdhelp/images/<language>"
#
#    and copied to:
#
#    		"$env:pdhelp/$env:lc/images"
#    if needed.
#
# 4) Create Qt resource file, 'pdhelp.qrc', that contains references to the
#    resource and image files to be compiled into MiramoDesigner, e.g.
#
#    <RCC>
#    	<qresource>
#    		<file>pdhelp/en/files/ListLabel.parent_en.pdhtml</file>
#    		<file>pdhelp/en/images/autonumberP_Heading2.png</file>
#
#    	</qresource>
#    </RCC>
#
# Items '1' through '4' above are done using the following .mmp files:
#
#--------------------------------------------------------------------------------
write-host "Use hrun not pdhelp!"
exit
#--------------------------------------------------------------------------------
$env:pdhTest	= 0
#--------------------------------------------------------------------------------
$env:helpMode		= "Qt"			#- Original Qt resource based
$env:helpMode		= "fileSystem"		#- New, filesystem HTML files
$env:outputType		= "qt"
$env:includeHelp	= "1"
$inputFile		= "$env:pdhelp\masterHelp.mmp"
$env:helpFilesFolder	= "$env:pdhelp\helpFiles"
$env:xyz	= "xyz"
#--------------------------------------------------------------------------------
write-host	"	Use: -t or -test for testing AAAAAAAAAA"

#--------------------------------------------------------------------------------
$env:tmpQRCfile		= "$env:pdhelp\pdhelp.qrc"	#- Temporary .qrc file
							#- Used in:
							#-  'image.mmpp'
							#-  'End.mmp' and
							#-  'propertyHelp.mmp'
#--------------------------------------------------------------------------------
$env:macroFile1		= "$env:pdhelp\mmppMacros\helpMacros.mmp"
$env:endFile		= "$env:pdhelp\mmppMacros\End.mmp"
# $env:propertiesList	= "$env:mman\mmDesigner\defaults\propertiesList"
$env:propertiesList	= "$env:pdhelp\defaults\propertiesList"
$mmcSchemaFolder	= "$env:COMPOSERPDF_HOME/MiramoCore/composerCore/XML"

#--------------------------------------------------------------------------------
# Copy $args array to $pdhrunArgs
$pdhrunArgs    = $args
if (($args.count -eq 1) -and (($args[0] -eq "-help") -or ($args[0] -eq "-h"))) {
	write-host	"pdhrun.ps1 ($env:pdhelp\bin)"
	write-host	"	Use: -t or -test for testing"
	write-host	"	Use: -xh for testing, without inline help"
	exit
}
#--------------------------------------------------------------------------------
if (($args.count -eq 1) -and (($args[0] -eq "-test") -or ($args[0] -eq "-t"))) {
	$env:pdhTest	= 1
}
#--------------------------------------------------------------------------------
if (($args.count -eq 1) -and (($args[0] -eq "-xh") -or ($args[0] -eq "-xh"))) {
	$env:pdhTest		= 1
	$env:includeHelp	= "0"
}
#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#- mmPageDesigner development source folders (remote host):
#- These are copy-to folders.
#--------------------------------------------------------------------------------
$env:pdhSrcPath		= "$env:COMPOSERPDF_HOME/MiramoCore"
$env:pdhSrcFolder	= "designerApp"
#--------------------------------------------------------------------------------
#- Derived folders and files:
#--------------------------------------------------------------------------------
$env:pdhSrcFolder	= "$env:pdhSrcPath\$env:pdhSrcFolder"
$env:pdhQRCfile		= "$env:pdhSrcFolder\pdhelp.qrc"
$env:pdhResourceFolder	= "$env:pdhSrcFolder\pdhelp"
#--------------------------------------------------------------------------------


#--------------------------------------------------------------------------------
function resourceFolders {
	#------------------------------------------------------------------------
	#- Language-dependent folders for file and image resources
	#- These are copy-to folders.
	#- E.g. zh-Hans, zh-Hant
	#- See: http://www.w3schools.com/tags/ref_language_codes.asp
	#------------------------------------------------------------------------
	$env:LC			= "$args"
	$env:pdhResourceFiles	= "$env:pdhResourceFolder\$env:LC\files"
	$env:pdhResourceImages	= "$env:pdhResourceFolder\$env:LC\images"
	#------------------------------------------------------------------------
	#- The following are used in creating pdhelp.qrc
	#------------------------------------------------------------------------
	$env:qrcImagesFolder	= "<file>pdhelp/$env:LC/images"		# (image.mmp)
	$env:qrcFilesFolder	= "<file>pdhelp/$env:LC/files"		# (propertyHelp.mmp)
	$env:mmcSchemaFile	= "mmPageDesignerHelp_$env:LC.xml"
	$env:mmcSchemaFile	= "$mmcSchemaFolder\$env:mmcSchemaFile"

	$env:outputFile	= "$env:pdhelp\mmPageDesignerHelp_$env:LC.xml"
}
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
function exitMessage {
	# write-host			"OUTPUT FILE: $env:outputFile"
	xcheck val --well-formed "$env:outputFile"
	$xmlStatus		= $LastExitCode
	# write-host		"OUTPUT STATUS: $xmlStatus"
	# See:
	# http://techibee.com/powershell/what-is-lastexitcode-and-in-powershell/1847
	if ($?) {
		# Copy files
		#----------------------------------------------------------------
		#- Copy files:
		#	mmPageDesignerHelp_xx.xml"	--> mmComposer schema folder
		#	pdhelp.qrc			--> mmDesignerApp folder
		#----------------------------------------------------------------
		if ($env:pdhTest -eq 0 -and $xmlStatus -eq 0) {
			#- Everything worked, no '-t' option & the XML file is valid
			# CK: 200b016: strip linefeed characters
			(Get-Content "$env:outputFile") -replace '&#xA;', '' | set-content "$env:mmcSchemaFile"
			# cp "$env:outputFile"		"$env:mmcSchemaFile"
			Write-Host "pdhrun: Success. File copied to:  $env:mmcSchemaFile (sans binary tabs)"
			}
		else {
			#- Just testing w/ '-t', or XML file invalid
			if ($xmlStatus = 1) {
				Write-Host "pdhrun: INVALID XML!!! ($env:mmcSchemaFile)"
				}
			Write-Host "pdhrun: -t[est]  File NOT copied to:  $env:mmcSchemaFile"
			}
		}
	else {
		write-host	"pdhrun: FAILED !!! File not copied"
		}
}
#--------------------------------------------------------------------------------
rm "$env:tmpQRCfile"

$startFolder	= pwd
cd		"$env:pdhelp"

#--------------------------------------------------------------------------------
function makeHelp {
	#------------------------------------------------------------------------
	#- Called once for each language
	#------------------------------------------------------------------------
	$language	= "$args"
	resourceFolders "$language"
	#------------------------------------------------------------------------
	#- Remove files from tmp image folder: "$env:pdhelp/tmpImageFolder/$language"
	#- Then re-create: "$env:pdhelp/tmpImageFolder/$language"
	#- "$env:tmpImageFolder" is used in 'image.mmp' (for copying image files)
	#------------------------------------------------------------------------
	$env:tmpImageFolder	= "$env:pdhelp/tmpImageFolder/$language"
	Remove-Item "$env:tmpImageFolder" -Force  -Recurse -ErrorAction SilentlyContinue
	New-Item -ItemType Directory -Force -Path $env:tmpImageFolder | out-null
	#------------------------------------------------------------------------
	$env:tmpResourceFolder	= "$env:pdhelp/tmpResourceFolder/$language"
	Remove-Item "$env:tmpResourceFolder" -Force  -Recurse -ErrorAction SilentlyContinue
	New-Item -ItemType Directory -Force -Path $env:tmpResourceFolder | out-null
	#------------------------------------------------------------------------
	# mmpp  "$inputFile" | mmpp -Mfile "$env:macroFile1" | out-file -enc utf8  "$env:outputFile"
	#------------------------------------------------------------------------
	$env:folderName		= "xxx"
	mmpp  "$inputFile" | out-file -enc utf8  "xxTmpFirstPass"
	mmpp -Mfile "$env:macroFile1" "xxTmpFirstPass" | out-file -enc utf8  "$env:outputFile"
	if ($?) {
		#----------------------------------------------------------------
		#- Copy help image resources:
		#- (Currently help file resources are placed directly in the Qt 
		#- resource folder, using mmpp command in 'propertyHelp.mmp'.
		#- This should probably be changed to use the same method as for
		#- images.)
		#----------------------------------------------------------------
		if ($env:pdhTest -eq 0) {
			rm "$env:pdhResourceImages/*"
			write-host	"pdhrun: Copying $env:tmpImageFolder/ files	To: $env:pdhResourceImages"
			copy "$env:tmpImageFolder/*"  "$env:pdhResourceImages"
			write-host	"pdhrun: Files copied to: $env:pdhResourceImages"
			}
		else {
			write-host	"pdhrun: -t[est] so $env:tmpImageFolder/ files	NOT copied"
			}
		}
	else {
		write-host	"pdhrun: FAILED !!! Files not copied"
		}
	exitMessage 
}
#--------------------------------------------------------------------------------
makeHelp en
# makeHelp de

# [System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
# [System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
#--------------------------------------------------------------------------------
#- Now modify and copy pdhelp.qrc file
#--------------------------------------------------------------------------------
$env:qrcTmp	= "$env:pdhelp\xxTmp.qrc"
gc "$env:tmpQRCfile" | sort | get-unique | out-file -enc oem "$env:qrcTmp"
write-host "pdhrun: Copying $env:qrcTmp To: $env:tmpQRCfile"
cp "$env:qrcTmp" "$env:tmpQRCfile"
rm "$env:qrcTmp"
#--------------------------------------------------------------------------------
mmpp "$env:endFile"
#--------------------------------------------------------------------------------
if ($env:pdhTest -eq 1) {
	write-host	"pdhrun: -t[est] so files not copied"
	cd	"$startFolder"
	exit
}
#--------------------------------------------------------------------------------
if ($?) {
	#------------------------------------------------------------------------
	#- Copy temporary pdhelp.qrc to real location
	#------------------------------------------------------------------------
	write-host	"pdhrun: Copying $env:tmpQRCfile  To: $env:pdhQRCfile"
	copy "$env:tmpQRCfile"  "$env:pdhQRCfile"
	}
else {
	write-host	"FAILED !!! pdhelp.qrc file not copied"
	}
#--------------------------------------------------------------------------------
cd	"$startFolder"
#--------------------------------------------------------------------------------
