@--------------------------------------------------------------------------------
@- fmcrelnotes.inc
@- include files for drawguide guide
@--------------------------------------------------------------------------------
<MasterPageRule	allPages="Right" />
<@include>	${fmcrelnotes}/cover.inc
<chapterStart
	dest="fmcRelnotes"
	>fmComposer Release Notes
</chapterStart>
<@include>	${fmcrelnotes}/introduction
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${fmcrelnotes}/frameshadow
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

