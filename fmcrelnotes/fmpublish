@------------------------------------------------------------
@-- fmpublish
@------------------------------------------------------------

<elementStart
	elementType="xml_mand_atts"
	elementName="FMpublish" >

<esp>
Use the <#xFMpublish> element to add a drop shadow to any of
the following objects:
Frame,
AFrame,
and
ATextFrame.
</esp>

<esp>
<#xFMpublish> cannot be used in a CMYK workflow.
</esp>

</elementStart>


@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Shadow properties"
	dest="<#elementName>.propertyGroup.shadow_properties"
	/>

@------------------------------------------------------------
<property
	long="shadowDistance"
	value="dim"
	>
<pdp>
Radius length.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="shadowAngle"
	value="angle"
	>
<pdp>
Shadow angle, in the range 90 - 180.
</pdp>
<pdp>
Only really works when value is 135.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="shadowBlend"
	value="normal | multiply | screen | darken | lighten"
	>
<pdp>
SVG blend mode.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="shadowColor"
	value="name"
	>
<pdp>
Shadow color.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="shadowDeviation"
	value="number"
	>
<pdp>
Standard deviation value to use for SVG feGaussianBlur stdDeviation.
Value must be less than 10. (Dunno why. Maybe something wrong the
way the SVG is written. If not fix[ed|able] in the SVG, then
mmComposer should reduce too-high values w/ warning msg.)
</pdp>

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Fill properties"
	dest="<#elementName>.propertyGroup.fill_properties"
	>
When using <#xxFrameShadow> the normal object fill properties
are in-operative. Use <#xxFrameShadow> properties instead.
</propertyGroup>

@------------------------------------------------------------
<@include>	${ctext}/fillProperties.inc

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

<exp>
</exp>

<exampleBlock
	>
@- <AFrame width="44mm" height="34mm" position="runin" align="center" >
<AFrame width="44mm" height="34mm" position="runin" align="start" 
	penWidth="0.2pt"
	>
	<xFrameShadow
		shadowDistance="3mm"
		shadowAngle="135"
		shadowBlend="screen"
		@- shadowColor="Black"
		shadowColor="rgb(#ccccd9)"
		@-shadowDeviation="9.999"
		shadowDeviation="2"
		fillColor="yellow3"
		@- fillOpacity="100"
		fillOpacity="0"
		fillTint="100"
		/>
</AFrame>
</exampleBlock>


<exp>

<exampleOutput
	width="64mm"
	@- fillOpacity="100"
	@- fillColor="rgb(#0000dd)"
	@- fillColor="Maroon"
	@- fillColor="yellow3"
	fillTint="100%"
	@- fillColor="xMaroon"
	fillColor="White"
	penWidth="0.2pt"
	shadow="N"
	@- shadowDistance="4mm"
	>
@------------------------------------------------------------
<outputCaption
	dest="fig.Tbl.simple_table1"
	>
Output from Example <xparanumonly
	id="ex.FrameShadow.shadow1" />
</outputCaption>
<P>
<#example_output>
</P>
</exampleOutput>
</exp>



@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
