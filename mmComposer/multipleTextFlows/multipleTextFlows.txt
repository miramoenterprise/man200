=============================================================================
multipleTextFlows.txt
=============================================================================


Requirements sketch for an enhancement to Apache FOP 1.1
--------------------------------------------------------

This is an outline description for a paid-for enhancement to FOP version 1.1.

The priority is 'Scenario A' below.

Note (1) It is not important that this enhancement is implemented in a way that
is consistent with the XSL:FO version 1.0 and 1.1 standards. E.g. there is
an implicit reference to the XSL:FO <flow-map> element below. It's immaterial
to this company if the following is implemented via the <flow-map> model
or not. Our only concern is to achieve the functionality described, in the
most cost-effective route. <flow-map> is probably more than we really need.


Note (2) It's possible that someone has already published code that goes some
way towards fulfilling the requirement described below (in relation to
FOP 0.95):

AAA*	Guilherme Balestieri Bedin
	http://apache-fop.1065347.n5.nabble.com/flow-map-1-1-td24195.html



Recommended steps
=================

-----------------------------------

Step 1.

Download Apache FOP version 1.1 source code from here:
http://www-eu.apache.org/dist/xmlgraphics/fop/source/


Step 2.

Compile source code downloaded in 'Step 1'


Step 3.

Run a test to produce PDF ouput from FOP 1.1. See 'TEST 1' below.

Using the command:

	fop -r oneregion.fo oneregion.pdf

'oneregion.fo' is the text shown in TEST 1 below.


Step 4.

Check whether the modifications implemented for FOP version 0.95 might be
potentially applicable for FOP version 1.1. (See 'AAA*' above.) 
[There may have been too much re-factoring between versions for this to
be useful. Hopefully not.]


Step 5.

Decide if you think you can proceed towards solving 'Scenario A' below,
and you are interested.


-----------------------------------

Step 6.

If yes is the answer to 'Step 5', meet with Datazone again at a time that suits your
best convenience. The purpose would be to discuss if we are going down the right road.


Step 7.

Implement 'Scenario A' below.


Step 8.

Run a test to produce enhanced output, i.e. multi-regions on a single
master page, as described in 'Scenario A' below.
See example 'TEST 2' below.


-----------------------------------

Step 9.

Apply enhancement from 'Step 7' to the Miramo fork of FOP 1.1. And test again.



Proposed pricing
================


Phase 1. Initial investigation     
--------
EUR  300

         This phase covers Steps 1 through 5 above.

	 PLUS some time looking at the FOP 1.1 source code, along with the
	 material in:
	 http://apache-fop.1065347.n5.nabble.com/flow-map-1-1-td24195.html

	 to check on the feasibility-for-you of proceeding to the following
	 phases.

	 At this stage, as at any stage of course, you are super welcome to
	 discuss your thoughts/opinions with us.


Phase 2. Implement enhancement in standard FOP 1.1.
--------
EUR 1600

         This phase covers Steps 6 through 8 above. And must include an early
         discussion at Datazone, e.g. one morning at your convenience, to ensure
	 that the path being followed is reasonable. And to allow any changes that
	 make sense.


Phase 3. Implement enhancement in Miramo FOP 1.1 fork.
--------
EUR  500

         This phase covers Step 9 above. (Datazone is guessing that
         it should be easy to do this.)


Note that if after Phase 1 you decide that Phases 2 and 3 are unfeasible
for you for any reason, you may decide not to proceed with those phases.
In that case you will still get paid for your work on Phase 1.


Note 2: all payments by Datazone have to be 'by the book'. This means in the
worst case payments will have to be net after deducting 'emergency' tax.
This means Datazone pays the tax to Revenue, and some time after that you
can reclaim it back from the Revenue. OTOH you are currently a student,
so there may be a way to simplify this. If you say you are interested in
the foregoing, we'll immediately check with our accountants about the
route for your best advantage in this regard.




Scenario A
----------

Enable a series of flow regions to be specified for a master page as follows:

	<fo:simple-page-master
		master-name="xxx" 
		....
		>
		<!-- First flow region on page -->
		<fo:region-body
			region-name="FlowA" 
				... position properties ...
		</fo:region-body>

		<!-- Second (auto-connected) flow region on page -->
		<fo:region-body
			region-name="FlowA" 
				... position properties ...
		</fo:region-body>

		<!-- Third (auto-connected) flow region on page -->
		<fo:region-body
			region-name="FlowA" 
			column-count="3"
				... position properties ...
		</fo:region-body>

		...
	
	</fo:simple-page-master>

	<fo:flow flow-name ="FlowA" >

       		content ...

	</fo:flow>

Notes A
-------

A1. The text flow passes through body flow FlowA regions in the order specified by
    the sequencing of the <fo:region-body region-name="FlowA" > elements.

A2. 'Scenario A' is similar to the illustration here:

    https://www.w3.org/TR/xsl11/#d0e7231
    'Mapping flowA to regions R1 and R2'

A3. Each region may have one or more columns.

A4. Each separate region on a master page is regarded as a "column"
    in the context of breaks. I.e. 'break-before'and 'break-after'.

    Alternatively, a new break value could be added, viz. "region":

	break	= page
		Start on next page (& other variants)


	break	= region
		Start at top of next region (on same page)
		If no next region on same page start at top of next page 

	break	= column
		Start at top of next column (same region)
		IF no next column in same region, start at top of next region 
			on same page
		IF no next region on same page, start at top of next page 

    But this alternative is *only* applicable if it does not involve
    significant extra cost.


A5. Each separate region on a master page is regarded as a "column"



Scenario B
----------

'Scenario A' involves a single document flow passing through more than one
body region on a page. 'Scenario B' extends 'Scenario A' to enable a
document to contain two text flows, each assigned to distinct body regions. E.g.

	<fo:simple-page-master
		master-name="xxx" 
		....
		>
		<!-- First flow A region on page -->
		<fo:region-body
			region-name="FlowA" 
				... position properties ...
		</fo:region-body>
		...

		<!-- First flow B region on page -->
		<fo:region-body
			region-name="FlowB" 
				... position properties ...
		</fo:region-body>
		...
	
	</fo:simple-page-master>

	<fo:flow flow-name ="FlowA" >

       		content for flow A ...

	</fo:flow>

	<fo:flow flow-name ="FlowB" >

       		content for flow B ...

	</fo:flow>


'Scenario B' seems like it might be very complicated, maybe too complicated,
to implement in the current context. In this case will be abandoned.

OTOH, some restrictions may make it easier. E.g.

	- One flow is designated as the 'main' flow, let's say 'FlowA'.
	  In this case 'FlowA' controls page generation.

and/or

	- A secondary flow, 'FlowB', is only effective on the first page
	  in a master-page squence.

=============================================================================


TEST 1
++++++


<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
xmlns:mm="http://www.miramo.com/fo/ext"  
  >
    <fo:layout-master-set>
        <fo:simple-page-master master-name="page" page-width="60mm" page-height="80mm">
            <fo:region-body region-name="body" margin="10mm" margin-bottom="40mm"
                            border-width="0.1pt"
                            border-color="rgb(0,0,0)"
                            border-style="solid"
                            padding="0pt"/>
        </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="page">
        <fo:flow flow-name="body">
<fo:block font-size="10pt" font-family="Arial" font-weight="400" font-style="normal"  >This is
some greeking text which will flow over onto the next page This is
some greeking text which will flow over onto the next page This is some
greeking text which will flow over onto the next page This is some
greeking text which will flow over onto the next page This is
some greeking text which will flow over onto the next page This is
some greeking text which will flow over onto the next page </fo:block>
</fo:flow>
    </fo:page-sequence>
</fo:root>



TEST 2
++++++


<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
xmlns:mm="http://www.miramo.com/fo/ext"  
  >
    <fo:layout-master-set>
        <fo:simple-page-master master-name="page" page-width="60mm" page-height="80mm">
            <fo:region-body region-name="flowA" margin="10mm" margin-bottom="55mm"
                            border-width="0.1pt"
                            border-color="rgb(0,0,0)"
                            border-style="solid"
                            padding="0pt"/>
            <fo:region-body region-name="flowA" margin="10mm" margin-top="32mm" margin-bottom="32mm"
                            border-width="0.1pt"
                            border-color="rgb(0,0,0)"
                            border-style="solid"
                            padding="0pt"/>            
                            
            <fo:region-body region-name="flowA" margin="10mm" margin-top="55mm" margin-bottom="10mm"
                            border-width="0.1pt"
                            border-color="rgb(0,0,0)"
                            border-style="solid"
                            padding="0pt"/>
        </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="page">
        <fo:flow flow-name="flowA">
<fo:block font-size="10pt" font-family="Arial" font-weight="400" font-style="normal"  >This
is some greeking text which will flow over onto the next page
This is some greeking text which will flow over onto the next page This is
some greeking text which will flow over onto the next page This is some greeking
text which will flow over onto the next page This is some greeking text which will
flow over onto the next page This is some greeking text which will flow over onto
the next page </fo:block>
</fo:flow>
    </fo:page-sequence>
</fo:root>

