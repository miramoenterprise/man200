@------------------------------------------------------------
@- introduction
@------------------------------------------------------------


@------------------------------------------------------------
<h1>
Introduction
</h1>

<bp>
This document contains a description of the new markup and
features included in the latest version of the fmComposer component
of Miramo Enterprise. The following features apply only in the
case <programName>fmComposer</programName> is run with FrameMaker 12
or later.
See Note <xparanumonly
	id="MiramoXML.note.fmVersion" /> on page <xnpage
	id="MiramoXML.note.fmVersion" />.
</bp>

<bp>
This document is an addendum to the <fI>Miramo Reference Guide</fI>,
i.e. <sq>rguide920.pdf</sq>, also located in the <sq>docs</sq> sub-folder in the
Miramo installation folder.
</bp>


@------------------------------------------------------------
<h1>
New command line options
</h1>


<bp>
Table <xparanumonly id="tbl.fmcaddendum.command_line_options.start" /> lists
@- Table <xparanumonly id="tbl.yy" /> lists
the new <programName>fmComposer</programName> command line options.
Several command line options
may be invoked directly within the <#xMiramoXML> element (see
pages <xnpage
	id="MiramoXML.element.start" /><xphyphen/><xnpage
	id="MiramoXML.element.end" />).
</bp>


<simpleTable
	startIndent="0mm"
	startIndent="10mm"
	widths="40|R"
	tblOpts='fillColor="Black" fillTint="3%" '
	>
@------------------------------------------------------------
<title
	dest="tbl.fmcaddendum.command_line_options"
	@- dest="tbl."
	>List of new command line options</title>
<R type="H" br="Thin" tr="none" >
<head>Option name and value</head>
<head>Description</head>
</R>
<R type="H" height="1mm" />

@------------------------------------------------------------
<cmdOption name="-Oepub" value="<fI>filename</fI>" >
Output EPUB (.epub).
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-Omshelp" value="<fI>filename</fI>" >
Output Microsoft compiled help (.chm).
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-Okindle" value="<fI>filename</fI>" >
Output Amazon Kindle format (.mobi).
See Note <xparanumonly
	id="MiramoXML.note.kindle" /> on page <xnpage
	id="MiramoXML.note.kindle" />.
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-Ohtml5" value="<fI>foldername</fI>" >
Output responsive HTML 5.
If <fI>foldername</fI> already exists, it must be an
empty folder. Otherwise the job will fail.
On job completion <fI>foldername</fI> will contain several
files and sub-folders, e.g. HTML, JavaScript and CSS
files.
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-OwebHelp" value="<fI>foldername</fI>" >
Output web help HTML.
If <fI>foldername</fI> already exists, it must be an
empty folder. Otherwise the job will fail.
On job completion <fI>foldername</fI> will contain several
files and sub-folders, e.g. HTML, JavaScript and CSS
files.
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-stsFile" value="<fI>filename</fI>" >
File name of .sts configuration file saved from FrameMaker.
The <programName>-stsFile</programName> option is ignored
unless one of the foregoing options is used.
See <#xMiramoXML> <pn>stsFile</pn> on page <xnpage
	id="MiramoXML.property.stsFile" /> and Note <xparanumonly
	id="MiramoXML.note.mmaccess_file" /> on page <xnpage
	id="MiramoXML.note.mmaccess_file" />.
</cmdOption>


@------------------------------------------------------------
<cmdOption name="-Bsaveformat" value="MIF | binary" >
Save a generated book file in MIF or binary format.

Ignored unless the <pn>-Bfile</pn> command line option or the
equivalent <#xMiramoXML> <pn>Bfile</pn> option is used to output
to a FrameMaker book.
If one of the FMpublish options,
<pn>-Oepub</pn>,
<pn>-Omshelp</pn>,
<pn>-Okindle</pn> or
<pn>-OwebHelp</pn>, is used,
the default value is <pv>binary</pv>. In all other cases the default is <pv>MIF</pv>.
See <#xMiramoXML> <pn>Bsaveformat</pn> on page <xnpage
	id="MiramoXML.property.Bsaveformat" />.
</cmdOption>

@------------------------------------------------------------
<cmdOption name="-roleMap" value="<fI>filename</fI>" >
When the <pn>-PDFtagged</pn> command line option is used to output structured PDF,
the <pn>-roleMap</pn> option may be used to specify the name of a file containing
mappings from paragraph format definitions to standard PDF structure names.
<MkDest id="roleMapCommandLineOption" />

<cp>
The role mapping file is in XML format with text encoded in UTF-8 and
has the following structure:
</cp>

@- <cp startIndent="8mm" firstLineIndent="8mm"  >
<cp pgfProps='startIndent="8mm" firstLineIndent="8mm"'  >
&lt;?xml version="1.0" encoding="utf-8" ?>
&lt;MiramoRoleMap><br/>
&emsp;&emsp;&emsp; &lt;map<br/>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; name="<fI>paragraphFormatName</fI>"<br/>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; role="<fI>standardName</fI>"<br/>
&emsp;&emsp;&emsp; /><br/>
&lt;/MiramoRoleMap>
</cp>

<cp>
Any number of &lt;map&gt; elements may be included. <pn>name</pn> is the name of
any paragraph format. <pn>role</pn> is the standard PDF structure element to map
the <pn>name</pn> value to.
See Example <xparanumonly id="ex.roleMap1" />.
The XML declaration is optional.
<cp>
</cp>
By default all paragraph formats are mapped to &lt;P&gt; elements unless
they are referenced in the rolemap file.
</cp>

<cp>
Ignored unless the <pn>-PDFtagged</pn> command line option or the
equivalent <#xMiramoXML> <pn>PDFtagged</pn> option is used.
</cp>
</cmdOption>


@------------------------------------------------------------
</simpleTable>
@------------------------------------------------------------

<#fdef> roleMapExample		${mman}/fmcaddendum/rolemap.xml
<#def> roleMapExample		@trim({<#roleMapExample>})
<bp>
Example <xparanumonly id="ex.roleMap1" /> illustrates the structure of
a role map file.
</bp>
<exampleBlock
	tabs="10|20"
	leftIndent="15mm"
	lineNumbers="Y"
	fontFamily="Segoe UI"
	>
<exampleCaption
	dest="ex.roleMap1"
	>
Sample role map file
</exampleCaption><#roleMapExample>
</exampleBlock>


@------------------------------------------------------------
<h1>
New mmprint options
</h1>

<bp>
All the new fmComposer command line options listed in
Table <xparanumonly
	id="tbl.fmcaddendum.command_line_options" /> are
also supported by the <programName>mmprint</programName> utility.
</bp>

<bp>
Table <xparanumonly id="tbl.mmprint.command_line_options.start" /> lists
an additional command line option that applies
to <programName>mmprint</programName> only.
</bp>

<simpleTable
	startIndent="0mm"
	startIndent="10mm"
	widths="40|R"
	tblOpts='fillColor="Black" fillTint="3%" '
	>
@------------------------------------------------------------
<title
	dest="tbl.mmprint.command_line_options"
	@- dest="tbl."
	>mmprint command line option</title>
<R type="H" br="Thin" tr="none" >
<head>Option name and value</head>
<head>Description</head>
</R>
<R type="H" height="1mm" />

@------------------------------------------------------------
<cmdOption name="-colorView" value="<fI>int</fI>" >

Select a color view configuration in the FrameMaker document.

<cp>
<fI>int</fI> is an integer in the range 1 to 6, corresponding
to the six possible color view configurations.
</cp>

<cp>
When the input is a FrameMaker book file, the configuration of the
selected color view may be different in each chapter in the book.
Processing books on network drives is not supported.
</cp>

</cmdOption>

<@skipStart>
@------------------------------------------------------------
<cmdOption name="-roleMap" value="<fI>filename</fI>" >
See the description of the <pn>-roleMap</pn> command line option in
Table <xparanumonly id="tbl.fmcaddendum.command_line_options.start" />

</cmdOption>
<@skipEnd>

@------------------------------------------------------------
</simpleTable>
@------------------------------------------------------------


<bp>
The usage of <programName>mmprint</programName> is illustrated
in Example <xparanumonly
	id="fmaddendum.mmprint.examples" />.
</bp>

<exampleBlock
	startIndent="10mm"
	leftIndent="10mm"
	keepSpaces="Y"
	lineNumbers="Y"
	lineNumberStyle="N"
	>
<exampleCaption
	dest="fmaddendum.mmprint.examples"
	>
Examples of using mmprint
</exampleCaption>
mmprint   -stsFile <xfI>my.sts</xfI>   -Okindle "D:\docs\kindle\<xfI>output</xfI>.mobi"   "D:\docs\FM  files\<xfI>file</xfI>.fm"<xlineRef>ex.fmaddendum.mmprint.quotationMarks1</xlineRef>

mmprint   -stsFile <xfI>my.sts</xfI>   -Oepub <xfI>output</xfI>.epub   <xfI>file</xfI>.fm

mmprint   -Opdf "my output.pdf"   -stsFile "my config.sts"   -Okindle <xfI>output</xfI>.mobi   <xfI>file</xfI>.fm <xlineRef>ex.fmaddendum.mmprint.quotationMarks2</xlineRef>

mmprint   -Omshelp <xfI>output</xfI>.chm   -stsFile <xfI>my.sts</xfI>   -Okindle <xfI>output</xfI>.mobi   -Ofm <xfI>output.fm</xfI>   <xfI>file</xfI>.mif

mmprint   -Opdf <xfI>output</xfI>.pdf   -colorView <xfI>n</xfI>   <xfI>file</xfI>.fm    (<xfI>n</xfI> is color view number)

mmprint   -PDFtagged   -roleMap <xfI>filename</xfI>   -Opdf <xfI>output</xfI>.pdf   <xfI>file</xfI>.fm
</exampleBlock>
@------------------------------------------------------------


<bp>
In Example <xparanumonly
	id="fmaddendum.mmprint.examples" /> the last
argument, <programName><fI>file</fI>.fm</programName>,
 or <programName><fI>file</fI>.mif</programName>, in
all <programName>mmprint</programName> command lines is the source
file to be converted by <programName>mmprint</programName>.
As a general rule, it is safest to surround argument values in
in double quotation marks, as shown in lines <xlinenum
	id="ex.fmaddendum.mmprint.quotationMarks1" /> and <xlinenum
	id="ex.fmaddendum.mmprint.quotationMarks2" /> in
Example <xparanumonly
	id="fmaddendum.mmprint.examples" />.
</bp>

<bp>
Whenever an output file is referenced by basename only, the output is saved
to the current working folder or to a folder within the 
current working folder. In all cases the Miramo RunAs user
account must have write access to the output folder, either the
current working folder or another folder, as applicable&emdash;see
Note <xparanumonly
	id="MiramoXML.note.mmaccess_folder" /> on page <xnpage
	id="MiramoXML.note.mmaccess_folder" />.
</bp>
