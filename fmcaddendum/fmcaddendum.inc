@--------------------------------------------------------------------------------
@- fmcaddendum.inc
@- 
@--------------------------------------------------------------------------------
<@include>	${fmcaddendum}/cover.inc
<@include>      ${refguide}/toc/toc
<chapterStart
	dest="fmcAddendum"
	>fmComposer Addendum
</chapterStart>
<@include>	${fmcaddendum}/introduction
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
@- <@include>	${fmcaddendum}/miramoxml
<@include>	${fmcaddendum}/inline/inline.inc
@- <@include>	${fmcaddendum}/inline/aframe
@- <@include>	${fmcaddendum}/inline/image
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

