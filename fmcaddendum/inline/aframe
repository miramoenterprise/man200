@--------------------------------------------------------------------------------
@- aframe
@--------------------------------------------------------------------------------
<elementStart elementType="xml_no_cont" elementName="AFrame"
	pgfFormat="P_elementName.ExA"
	>

<esp>
The following additional HTML5 hotspot properties are supported on
the <#xAFrame> element.
</esp>


</elementStart>
@----------------------------------------------------------------------------

@-========================================================
<|propertiesStart>


<@include> ${ctext}/hotspots.inc


@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------


<exp>
Example <xparanumonly
	id="ex.AFrame_and_Image_hotspots" /> illustrates the
usage of the <#xAFrame> and <#xImage> <pn>hotspotURL</pn> and
<pn>hotspotToolTip</pn> properties.
</exp>

@- https://commons.wikimedia.org/wiki/Category:Videos_of_volcanic_eruptions
@- https://upload.wikimedia.org/wikipedia/commons/transcoded/2/21/Paroxysm_at_Etna%2C_16-17_November_2013.webm/Paroxysm_at_Etna%2C_16-17_November_2013.webm.480p.webm
@- https://upload.wikimedia.org/wikipedia/commons/transcoded/e/ed/%E5%AF%8C%E5%A3%AB%E5%B1%B1%E9%A0%82%E5%BE%A1%E6%9D%A5%E5%85%89_Go-raikou%28The_sunrise%29_at_summit_of_Mt.Fuji_HDR-CX560V.webm/%E5%AF%8C%E5%A3%AB%E5%B1%B1%E9%A0%82%E5%BE%A1%E6%9D%A5%E5%85%89_Go-raikou%28The_sunrise%29_at_summit_of_Mt.Fuji_HDR-CX560V.webm.480p.webm

<exampleBlock
	lineNumbers="Y"
	tabs="3|6|9"
	>
<exampleCaption
	dest="ex.AFrame_and_Image_hotspots"
	><#xAFrame> and <#xImage> hotspots
</exampleCaption>
<P>
<AFrame 
	@- hotspotURL="https://vimeo.com/110580672"
	@- hotspotToolTip="Airplane landing"
	@- hotspotURL="https://upload.wikimedia.org/wikipedia/commons/transcoded/e/ed/%E5%AF%8C%E5%A3%AB%E5%B1%B1%E9%A0%82%E5%BE%A1%E6%9D%A5%E5%85%89_Go-raikou%28The_sunrise%29_at_summit_of_Mt.Fuji_HDR-CX560V.webm/%E5%AF%8C%E5%A3%AB%E5%B1%B1%E9%A0%82%E5%BE%A1%E6%9D%A5%E5%85%89_Go-raikou%28The_sunrise%29_at_summit_of_Mt.Fuji_HDR-CX560V.webm.480p.webm"
	hotspotURL="http://tinyurl.com/y7u8to3q"
	hotspotToolTip="Sunrise"
	width="60mm" height="40mm" fillColor="Blue"
	>
<Image
	file="${images}/trainAndFuji.jpg"
	width="40mm" height="20mm" left="10mm" top="10mm"
	@- hotspotURL="https://www.videezy.com/animals-and-wildlife/2212-puffins-hd-stock-video"
	@- hotspotToolTip="Puffins"
	@- hotspotURL="https://upload.wikimedia.org/wikipedia/commons/transcoded/2/21/Paroxysm_at_Etna%2C_16-17_November_2013.webm/Paroxysm_at_Etna%2C_16-17_November_2013.webm.480p.webm"
	hotspotURL="http://tinyurl.com/ybp42q2p"
	hotspotToolTip="Volcanic eruption"
	/>
</AFrame>
</P>
</exampleBlock>


@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
