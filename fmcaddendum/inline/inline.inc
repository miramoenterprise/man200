@-----------------------------------------------------------
@- inline.inc
@- include files for fmcAddendum Inline Markup Codes
@-----------------------------------------------------------
<chapterStart
	dest="fmcAddendum.inlineMarkupElements"
	pgfSuffix="ExA"
	>New markup elements and attributes
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE         1
<#def> elementType      inline
@-----------------------------------------------------------
<@include>	${fmcAddendum}/inline/introduction
@-----------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
<@include>	${fmcAddendum}/inline/miramoxml
<@include>	${fmcAddendum}/inline/aframe
<@include>	${fmcAddendum}/inline/image


