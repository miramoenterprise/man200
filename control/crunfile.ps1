#--------------------------------------------------------------------------------
#- runfile.ps1
#--------------------------------------------------------------------------------
#
# $x = miramo -v 2>&1 | foreach-object {$_.tostring()}
# $y = $x | out-string
# $y.gettype().name
# $y.split("`n") | select-string -pattern "ersion"
# $x = miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
# $x.split("`n") | select-string -pattern "ersion" | select-string -pattern "iramo"
#
# miramo version:   9.2.0p72
# 
$mmVersion = miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
$mmVersion = $mmVersion.split("`n") | select-string -pattern "ersion" | select-string -pattern "iramo"
write-host "$mmVersion"

./mkFormatdefs.ps1

$outFolder		= "$env:output"

$inputFile		= "$env:inline\inline.inc"
$tmpFile		= "$env:output\inlineFM.tmp"

$env:pid		= $pid


$env:composer		= "fm"
$env:startTime          = Get-Date -format "yyyy-MMM-dd HH:mm"
$env:composerText	= "FrameMaker"
mmpp -Mfile control.mmp $inputFile | out-file -enc oem $tmpFile
"</MiramoXML>" | out-file -append -enc oem $tmpFile
miramo -composer fm -sendEnv Y -Ppapersize A4 -Opdf $outFolder\inlineFM.pdf -Omif $outFolder\inline.mif $tmpFile 2>&1 | %{ if ($_ -is [System.Management.Automation.ErrorRecord]) { $_.Exception.Message } else { $_ } }


$env:composer		= "mm"
$env:startTime          = Get-Date -format "yyyy-MMM-dd HH:mm"
$tmpFile		= "$env:output\inlineMM.tmp"
$env:composerText	= "mmComposer"
mmpp -Mfile control.mmp $inputFile | out-file -enc oem $tmpFile
"</MiramoXML>" | out-file -append -enc oem $tmpFile
miramo -composer mm -PDFbookmarks 0  -sendenv Y -warn 2 -Opdf $outFolder\inlineMM.pdf $tmpFile
