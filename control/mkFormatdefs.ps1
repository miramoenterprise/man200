#--------------------------------------------------------------------------------
#- mkFormatDefs.ps1
#--------------------------------------------------------------------------------
cat $env:mman\formatdefs\defaults\defaults.mm > $env:mman\formatdefs\tmp\defaults.mm
cat $env:mman\formatdefs\para\*.def > $env:mman\formatdefs\tmp\paraDefs
cat $env:mman\formatdefs\tbl\*.def > $env:mman\formatdefs\tmp\tblDefs
cat $env:mman\formatdefs\font\*.def > $env:mman\formatdefs\tmp\fontDefs
cat $env:mman\formatdefs\var\*.def > $env:mman\formatdefs\tmp\varDefs
cat $env:mman\formatdefs\rule\*.def > $env:mman\formatdefs\tmp\ruleDefs
cat $env:mman\formatdefs\xrefs\*.def > $env:mman\formatdefs\tmp\xrefsDefs
cat $env:mman\formatdefs\color\*.def > $env:mman\formatdefs\tmp\colorDefs



cat $env:mman\formatdefs\defaults\fontdefs.mmp | out-file -enc "oem" $env:mman\formatdefs\tmp\formatdefs.msm
cat $env:mman\formatdefs\defaults\defaults.mm | out-file -append -enc "oem" $env:mman\formatdefs\tmp\formatdefs.msm
cat $env:mman\formatdefs\tmp\*Defs | out-file -append -enc "oem" $env:mman\formatdefs\tmp\formatdefs.msm
