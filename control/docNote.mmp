@--------------------------------------------------------------------------------
@- docNote.mmp
@--------------------------------------------------------------------------------
<@xmacro> - docNote2 {
	@------------------------------------------------------------------------
	<#ifndef> dn2.noteNum		0
	@------------------------------------------------------------------------
	@xgetvals(dn2.)
	@------------------------------------------------------------------------
}{
	@if(<#showDocNotes2>) {
		<#def> dn2.noteNum		@eval(<#dn2.noteNum> + 1)
		<#def> dn2.text			$docNote2
		@print({&ensp;<Font fontFamily="Noto Sans"
			textSize="8pt" 
			fontWeight="Regular" ><Font textColor="Maroon">[</Font><#dn2.noteNum><Font
			textColor="Maroon">]</Font> <Font
			textColor="Maroon"> <#dn2.text></Font></Font>}, n)
		}
}
@--------------------------------------------------------------------------------
<@xmacro> - docNote {
	<#def> dn.docNote		1
	<#def> dn.p			{6.5pt}
	<#def> dn.ff			{Arial}
	<#def> dn.textColor		{Red}
	<#def> dn.baselineOffset	{0.7mm}		@- Upshift
	<#def> dn.sideOffset		{0mm}
	<#def> dn.fillColor		{Dark Grey}
	<#def> dn.fillTint		{30%}
	<#def> dn.fillOpacity		{100%}
	<#def> dn.clm			{2pt}
	<#def> dn.crm			{1pt}
	<#def> dn.border		Y
	<#def> dn.svgDef		{}
	@------------------------------------------------------------------------
	<#ifndef> dn.noteNum		0
	@------------------------------------------------------------------------
	@xgetvals(dn.)
	@------------------------------------------------------------------------
}{
	@if(<#showDocNotes>) {
		<#def> dn.noteNum		@eval(<#dn.noteNum> + 1)
		<#def> dn.text	$docNote
		<#def> dn.text	{
			<TblFrame 
				penWidth="0"
				penOpacity="0"
				position="outsideTextFrame"
				sideOffset="<#dn.sideOffset>"
				baselineOffset="<#dn.baselineOffset>"
				textColor="<#dn.textColor>"
				startRule="none"
				bottomRule="none"
				align="end"
				fillOpacity="0"
				spaceAbove="0"
				>
			<mmDraw>
			<PolyLine
				arrowHeadPosition="start"
				@- arrowWidthRatio="20"
				arrowWidthRatio="16"
				penColor="Blue"
				penWidth="0.4pt"
				>
				<Point left="0"		top="2mm" />
				<Point left="6mm"	top="2mm" />
			</PolyLine>
			</mmDraw>
			<TblColumn width="6mm" />
			<TblColumn width="34mm" />
			@-=======================================================
			<Row>
			@--------------------------------------------------------
			@if(<#dn.border> = Y) {
				<Cell endRule="Thin" startRule="none" topRule="none" fillOpacity="0" />
				}
			@else {
				<Cell endRule="none" startRule="none" topRule="none" fillOpacity="0" />
				}
			@--------------------------------------------------------
			<Cell
				fillColor="<#dn.fillColor>"
				fillTint="<#dn.fillTint>"
				fillOpacity="<#dn.fillOpacity>"
				topMargin="1pt" startMargin="1pt" bottomMargin="1pt"
				@if(<#dn.border> = Y) {
					startRule="Thin"
					topRule="Thin"
					endRule="Thin"
					}
				@else {
					startRule="none"
					topRule="none"
					endRule="none"
					}
				><P fontFamily="Arial"
					fontWeight="Bold"
					textSize="6pt"
					>Note (<#dn.noteNum>)</P></Cell>
			</Row>
			@-=======================================================
			<Row
				>
			@--------------------------------------------------------
			@if(<#dn.border> = Y) {
				<Cell endRule="Thin" startRule="none" bottomRule="none" topRule="none" fillOpacity="0" />
				}
			@else {
				<Cell endRule="none" startRule="none" bottomRule="none" topRule="none" fillOpacity="0" />
				}
			@--------------------------------------------------------
			<Cell
				fillColor="<#dn.fillColor>"
				fillTint="<#dn.fillTint>"
				fillOpacity="<#dn.fillOpacity>"
				startMargin="<#dn.clm>"
				endMargin="<#dn.crm>"
				@if(<#dn.border> = Y) {
					startRule="Thin"
					endRule="Thin"
					bottomRule="Thin"
					}
				@else {
					startRule="none"
					topRule="none"
					endRule="none"
					}
				@if(@len(<#dn.svgDef>)) {
					svgDef="<#dn.svgDef>"
					}
				>
			<P
				fontFamily="<#dn.ff>"
				textColor="<#dn.textColor>"
				textSize="<#dn.p>"
				><#dn.text></P>
			</Cell>
			@--------------------------------------------------------
			</Row>
			@-=======================================================
			</TblFrame>
		}
		@----------------------------------------------------------------
		<#def> dn.text	@trim(<#dn.text>)
		@----------------------------------------------------------------
		@if(<#inProperty> = 1 AND <#inPropertyPgf> = 0 ) {
			<#def> property.dn.text		<#dn.text>
			@- <@write> error {yyyyyyyyyyyyxyx}
			}
		@if(<#inPropertyGroup> = 1 AND <#inPropertyPgf> = 0 ) {
			<#def> propertygroup.dn.text	<#dn.text>
			}
		@----------------------------------------------------------------
		@if(<#inValueKey> = 1 ) {
			<#def> vk.dn.text		<#dn.text>
			@- <#def> inValueKey		0
			}
		@----------------------------------------------------------------
		@if(<#inPropertyPgf> AND <#inValueKey> = 0) {
			@print({<#dn.text>}, n)
			@--- Only 1 docNote per pdp
		 	<#def> inPropertyPgf		0
			}
		@----------------------------------------------------------------
		@print({<#dn.text>}, n)
		@----------------------------------------------------------------
		}
}
@--------------------------------------------------------------------------------
@- <docNote>Hello</docNote>
@--------------------------------------------------------------------------------
