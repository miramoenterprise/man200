@--------------------------------------------------------------------------------
@- splitText.mmp
@--------------------------------------------------------------------------------
@- U+20AC. Takes three bytes to encode, since it is between U+0800 and U+FFFF.
@- def utf8_char_len_1(c):
@--------------------------------------------------------------------------------
<@xmacro>  XXXsplitText {
	@------------------------------------------------------------------------
	@- From aabbccddeeffgg
	@- to   aa|bb|cc|dd|ee|ff|gg
	@------------------------------------------------------------------------
	<#def> sxt.text			{aaaaaa}
	<#def> sxt.separator		{|}
	<#def> sxt.showASCIIchars	{Y}	@- If in ASCII show char, not hex
	<#def> sxt.print		{N}	@- Output format
	<#def> sxt.output		{utf8}	@- Output format
						@- utf8 | utf8hex | ucs2 | charEntity
	@------------------------------------------------------------------------
	@xgetvals(sxt.)
	@------------------------------------------------------------------------
	<#def> sxt.newStringUTF8	{}	@- utf8
	<#def> sxt.newStringUTF8Hex	{}	@- utf8hex
	<#def> sxt.newStringUniNumber	{}	@- ucs2
	<#def> sxt.newStringCharEntity	{}	@- charEntity
	<#def> sxt.inputString		<#sxt.text>
	@------------------------------------------------------------------------
	<#def> sxt.utf8byteCount	@len(<#sxt.text>)
	@while(<#sxt.utf8byteCount> > 0 ) { 
		@----------------------------------------------------------------
		@- Check for (some) ascii chars
		@----------------------------------------------------------------
		<#def> sxt.firstByte		@substr({<#sxt.inputString>}, 1, 1)
		<#def> sxt.firstByteHex 	@bin2hex(<#sxt.firstByte>)
		<#def> sxt.firstByteDec 	@bin2dec(<#sxt.firstByte>)
		@----------------------------------------------------------------
		@if(<#sxt.firstByteDec> <= 65535 ) {
			<#def> sxt.utf8CharLength	3
			}
		@if(<#sxt.firstByteDec> <= 2048 ) {
			<#def> sxt.utf8CharLength	2
			}
		@if(<#sxt.firstByteDec> <= 127 ) {
			<#def> sxt.utf8CharLength	1
			}
		@----------------------------------------------------------------
		@- <@write> error {<#sxt.firstByteDec> (bytes = <#sxt.utf8CharLength>) }
		@----------------------------------------------------------------
		<#def> suffixStringStart	@eval(<#sxt.utf8CharLength> + 1)
		<#def> sxt.nextChar		@substr({<#sxt.inputString>}, 1,
							<#sxt.utf8CharLength>)
		<#def> sxt.nextCharHex		@bin2hex(<#sxt.nextChar>)
		<#def> sxt.inputString		@substr({<#sxt.inputString>},
							<#suffixStringStart>)
		@----------------------------------------------------------------
		@if(<#sxt.firstByteDec> <= 127 AND @match({<#sxt.showASCIIchars>}, {Y})) {
			<#def> sxt.hexString		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.hexString		{<#sxt.newStringUTF8Hex><#sxt.hexString>}
			<#def> sxt.uniNumber		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.uniNumber		{<#sxt.newStringUniNumber><#sxt.uniNumber>}
			<#def> sxt.charEntity		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.charEntity		{<#sxt.newStringCharEntity><#sxt.charEntity>}
			}
		@else {
			<#def> sxt.hexString		{<#sxt.newStringUTF8Hex><#sxt.nextCharHex>}
			<#def> sxt.uniNumber		@utf8_to_ucs2(<#sxt.nextCharHex>)
			<#def> sxt.uniNumber		{<#sxt.newStringUniNumber><#sxt.uniNumber>}
			<#def> sxt.charEntity		@utf8_to_ucs2(<#sxt.nextCharHex>)
			<#def> sxt.charEntity		{&#x<#sxt.charEntity>;}
			<#def> sxt.charEntity		{<#sxt.newStringCharEntity><#sxt.charEntity>}
			}
		@- <@write> error {zzz <#sxt.hexString> ()  <#sxt.uniNumber> }
		@- <@write> error {yy <#sxt.nextCharHex> ()  <#sxt.uniNumber> }
		@----------------------------------------------------------------
		@if(<#sxt.utf8byteCount> <= <#sxt.utf8CharLength>) {
			<#def> sxt.newStringUTF8	{<#sxt.newStringUTF8><#sxt.nextChar>}
			<#def> sxt.newStringUTF8Hex	{<#sxt.hexString>}
			<#def> sxt.newStringUniNumber	{<#sxt.uniNumber>}
			<#def> sxt.newStringCharEntity	{<#sxt.charEntity>}
			}
		@else {
			<#def> sxt.newStringUTF8	{<#sxt.newStringUTF8><#sxt.nextChar><#sxt.separator>}
			<#def> sxt.newStringUTF8Hex	{<#sxt.hexString><#sxt.separator>}
			<#def> sxt.newStringUniNumber	{<#sxt.uniNumber><#sxt.separator>}
			<#def> sxt.newStringCharEntity	{<#sxt.charEntity><#sxt.separator>}
			}
		@----------------------------------------------------------------
		<#def> sxt.utf8byteCount	@eval(<#sxt.utf8byteCount> - <#sxt.utf8CharLength>)
		@----------------------------------------------------------------
		@- <@write> error {remaining bytes = <#sxt.utf8byteCount>}
		@----------------------------------------------------------------
		}
		@----------------------------------------------------------------
		@if(@match({X<#sxt.output>X}, {Xutf8X})) {
			<#def> sxt.output		{<#sxt.newStringUTF8>}
			}
		@if(@match({X<#sxt.output>X}, {Xutf8hexX})) {
			<#def> sxt.output		{<#sxt.newStringUTF8Hex>}
			}
		@if(@match({X<#sxt.output>X}, {Xucs2X})) {
			<#def> sxt.output		{<#sxt.newStringUniNumber>}
			}
		@if(@match({X<#sxt.output>X}, {XcharEntityX})) {
			<#def> sxt.output		{<#sxt.newStringCharEntity>}
			}
		@----------------------------------------------------------------
		@if(@match({x<#sxt.print>x}, {xYx})) {
			@print({<#sxt.output>}, n)
			}
		@----------------------------------------------------------------
		@- @print({A <#sxt.newStringUTF8>})
		@- @print({B <#sxt.newStringUTF8Hex>})
		@- @print({C <#sxt.newStringUniNumber>})
		@- @print({D <#sxt.newStringCharEntity>})
}
@--------------------------------------------------------------------------------
<@xmacro> - splitTextContent {
	@------------------------------------------------------------------------
	@- From aabbccddeeffgg
	@- to   aa|bb|cc|dd|ee|ff|gg
	@------------------------------------------------------------------------
	<#def> sxt.text			{aaaaaa}
	<#def> sxt.separator		{|}
	<#def> sxt.doLineBreaks		{N}
	<#def> sxt.maxCharsPerLine	{12}
	<#def> sxt.showASCIIchars	{Y}	@- If in ASCII show char, not hex
	<#def> sxt.print		{N}	@- Output format
	<#def> sxt.output		{utf8}	@- Output format
						@- utf8 | utf8hex | ucs2 | charEntity
	@------------------------------------------------------------------------
	@xgetvals(sxt.)
	@------------------------------------------------------------------------
}{
	@------------------------------------------------------------------------
	<#def> sxt.inputString			{$splitTextContent}
	@------------------------------------------------------------------------
	<#def> sxt.newStringUTF8		{}	@- utf8
	<#def> sxt.lineStringUTF8		{}	@- utf8
	@------------------------------------------------------------------------
	<#def> sxt.newStringUTF8Hex		{}	@- utf8hex
	<#def> sxt.lineStringUTF8Hex		{}	@- utf8hex
	@------------------------------------------------------------------------
	<#def> sxt.newStringUniNumber		{}	@- ucs2
	<#def> sxt.lineStringUniNumber		{}	@- ucs2
	@------------------------------------------------------------------------
	<#def> sxt.newStringCharEntity		{}	@- charEntity
	<#def> sxt.lineStringCharEntity		{}	@- charEntity
	@------------------------------------------------------------------------
	<#def> sxt.lineCount			0
	<#def> sxt.charCount			0
	<#def> sxt.spaceCharCount		0
	<#def> sxt.lineCharCount		0
	@------------------------------------------------------------------------
	<#def> sxt.utf8byteCount	@len(<#sxt.inputString>)
	@while(<#sxt.utf8byteCount> > 0 ) { 
		<#def> sxt.charCount		@eval(<#sxt.charCount> + 1)
		<#def> sxt.lineCharCount	@eval(<#sxt.lineCharCount> + 1)
		<#def> sxt.lastChar		0
		@----------------------------------------------------------------
		@- Check for (some) ascii chars
		@----------------------------------------------------------------
		<#def> sxt.firstByte		@substr({<#sxt.inputString>}, 1, 1)
		<#def> sxt.firstByteHex 	@bin2hex(<#sxt.firstByte>)
		<#def> sxt.firstByteDec 	@bin2dec(<#sxt.firstByte>)
		@----------------------------------------------------------------
		@if(<#sxt.firstByteDec> <= 65535 ) {
			<#def> sxt.utf8CharLength	3
			}
		@if(<#sxt.firstByteDec> <= 2048 ) {
			<#def> sxt.utf8CharLength	2
			}
		@if(<#sxt.firstByteDec> <= 224 ) {
			<#def> sxt.utf8CharLength	3
			}
		@if(<#sxt.firstByteDec> <= 219 ) {
			<#def> sxt.utf8CharLength	2
			}
		@if(<#sxt.firstByteDec> <= 127 ) {
			<#def> sxt.utf8CharLength	1
			}
		@----------------------------------------------------------------
		@- <@write> error {<#sxt.firstByteDec> (bytes = <#sxt.utf8CharLength>) }
		@----------------------------------------------------------------
		<#def> suffixStringStart	@eval(<#sxt.utf8CharLength> + 1)
		<#def> sxt.nextChar		@substr({<#sxt.inputString>}, 1,
							<#sxt.utf8CharLength>)
		<#def> sxt.nextCharHex		@bin2hex(<#sxt.nextChar>)
		<#def> sxt.inputString		@substr({<#sxt.inputString>},
							<#suffixStringStart>)
		@----------------------------------------------------------------
		@- This where the output gets accumulated
		@----------------------------------------------------------------
		@if(<#sxt.firstByteDec> <= 127 AND @match({<#sxt.showASCIIchars>}, {Y})) {
			<#def> sxt.hexString		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.hexString		{<#sxt.newStringUTF8Hex><#sxt.hexString>}
			<#def> sxt.hexStringLine	@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.hexStringLine	{<#sxt.lineStringUTF8Hex><#sxt.hexStringLine>}
			@--------------------------------------------------------
			<#def> sxt.uniNumber		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.uniNumber		{<#sxt.newStringUniNumber><#sxt.uniNumber>}
			<#def> sxt.uniNumberLine	@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.uniNumberLine	{<#sxt.lineStringUniNumber><#sxt.uniNumberLine>}
			@--------------------------------------------------------
			<#def> sxt.charEntity		@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.charEntity		{<#sxt.newStringCharEntity><#sxt.charEntity>}
			<#def> sxt.charEntityLine	@hex2bin(<#sxt.nextCharHex>)
			<#def> sxt.charEntityLine	{<#sxt.lineStringCharEntity><#sxt.charEntityLine>}
			}
		@else {
			<#def> sxt.hexString		{<#sxt.newStringUTF8Hex><#sxt.nextCharHex>}
			@--------------------------------------------------------
			<#def> sxt.uniNumber		@utf8_to_ucs2(<#sxt.nextCharHex>)
			<#def> sxt.uniNumber		{<#sxt.newStringUniNumber><#sxt.uniNumber>}
			<#def> sxt.uniNumberLine	@utf8_to_ucs2(<#sxt.nextCharHex>)
			<#def> sxt.uniNumberLine	{<#sxt.lineStringUniNumber><#sxt.uniNumberLine>}
			@--------------------------------------------------------
			<#def> sxt.charEntity		@utf8_to_ucs2(<#sxt.nextCharHex>)
			<#def> sxt.charEntity		{&#x<#sxt.charEntity>;}
			<#def> sxt.charEntity		{<#sxt.newStringCharEntity><#sxt.charEntity>}
			}
		@----------------------------------------------------------------
		@- <@write> error {zzz <#sxt.hexString> ()  <#sxt.uniNumber> }
		@- <@write> error {yy <#sxt.nextCharHex> ()  <#sxt.uniNumber> }
		@----------------------------------------------------------------
		@if(<#sxt.utf8byteCount> <= <#sxt.utf8CharLength>) {
			@--------------------------------------------------------
			<#def> sxt.newStringUTF8	{<#sxt.newStringUTF8><#sxt.nextChar>}
			@--------------------------------------------------------
			<#def> sxt.newStringUTF8Hex	{<#sxt.hexString>}
			@--------------------------------------------------------
			<#def> sxt.newStringUniNumber	{<#sxt.uniNumber>}
			<#def> sxt.lineStringUniNumber	{<#sxt.uniNumberLine>}
			@--------------------------------------------------------
			<#def> sxt.newStringCharEntity	{<#sxt.charEntity>}
			}
		@else {
			<#def> sxt.newStringUTF8	{<#sxt.newStringUTF8><#sxt.nextChar><#sxt.separator>}
			@--------------------------------------------------------
			<#def> sxt.newStringUTF8Hex	{<#sxt.hexString><#sxt.separator>}
			@--------------------------------------------------------
			<#def> sxt.newStringUniNumber	{<#sxt.uniNumber><#sxt.separator>}
			<#def> sxt.lineStringUniNumber	{<#sxt.uniNumberLine><#sxt.separator>}
			@--------------------------------------------------------
			<#def> sxt.newStringCharEntity	{<#sxt.charEntity><#sxt.separator>}
			}
		@----------------------------------------------------------------
		@if(<#sxt.firstByteDec> = 32
				AND @match({<#sxt.doLineBreaks>}, {Y})
				AND <#sxt.lineCharCount> >= <#sxt.maxCharsPerLine>
				) {
			@--------------------------------------------------------
			@- Make arrays:
			@--------------------------------------------------------
			<#def> sxt.lineCount		@eval(<#sxt.lineCount> + 1)
			<#def> sxt.spaceCharCount	@eval(<#sxt.spaceCharCount> + 1)
			<#def> sxt.newStringUniNumber	{<#sxt.newStringUniNumber><br/>}
			@--------------------------------------------------------
			<#def> sxt.lineStringUniNumber[<#sxt.lineCount>]	{<#sxt.lineStringUniNumber>}
			@--------------------------------------------------------
			<#def> sxt.lineCharCount	0
			@--------------------------------------------------------
			<#def> sxt.lineStringUniNumber	{}
			<#def> sxt.lineStringCharEntity	{}
			<#def> sxt.uniNumberLine	{}
			<#def> sxt.charEntityLine	{}
			<#def> sxt.lastChar		1
			}
		@----------------------------------------------------------------
		<#def> sxt.utf8byteCount	@eval(<#sxt.utf8byteCount> - <#sxt.utf8CharLength>)
		@----------------------------------------------------------------
		@- <@write> error {remaining bytes = <#sxt.utf8byteCount>}
		@----------------------------------------------------------------
		}
	@------------------------------------------------------------------------
	@if(@match({X<#sxt.output>X}, {Xutf8X})) {
		<#def> sxt.output		{<#sxt.newStringUTF8>}
		}
	@if(@match({X<#sxt.output>X}, {Xutf8hexX})) {
		<#def> sxt.output		{<#sxt.newStringUTF8Hex>}
		}
	@if(@match({X<#sxt.output>X}, {Xucs2X})) {
		<#def> sxt.output		{<#sxt.newStringUniNumber>}
		}
	@if(@match({X<#sxt.output>X}, {XcharEntityX})) {
		<#def> sxt.output		{<#sxt.newStringCharEntity>}
		}
	@------------------------------------------------------------------------
	@if(@match({x<#sxt.print>x}, {xYx})) {
		@print({<#sxt.output>}, n)
		}
	@------------------------------------------------------------------------
	@- @print({A <#sxt.newStringUTF8>})
	@- @print({B <#sxt.newStringUTF8Hex>})
	@- @print({C <#sxt.newStringUniNumber>})
	@- @print({D <#sxt.newStringCharEntity>})
	@------------------------------------------------------------------------
	@if(<#sxt.spaceCharCount> = 0 ) {
		<#def> sxt.lineCount			1
		<#def> sxt.lineStringUniNumber[1]	{<#sxt.newStringUniNumber>}
		}
	@------------------------------------------------------------------------
	@- Last line may not have any spaces, or space may occur
	@- before <#sxt.maxCharsPerLine>
	@- Check for remaining line text.
	@------------------------------------------------------------------------
	@if(<#sxt.lastChar> = 0 AND <#sxt.spaceCharCount> > 0 ) {
		<#def> sxt.lineCount			@eval(<#sxt.lineCount> + 1)
		<#def> sxt.lineStringUniNumber[<#sxt.lineCount>]	{<#sxt.lineStringUniNumber>}
		}
	@------------------------------------------------------------------------
	<@write> error {chars: <#sxt.charCount>}
	<@write> error {lines: <#sxt.lineCount>}
	@------------------------------------------------------------------------
	@for(line= 1 to <#sxt.lineCount>) {
		<@write> error {
text:
----------------- $sxt.lineStringUniNumber[$$line] }
aaa
		}
	}
}
@--------------------------------------------------------------------------------
<@skipStart>
<#def> firstSteps	{الخطوات الأولى.}
<splitTextContent print="Y" ><#firstSteps></splitTextContent>
@--------------------------------------------------------------------------------
@- <#def> firstSteps	{الخطوات الأولى}
@--------------------------------------------------------------------------------
<@skipStart>
الخطوات الأولى
القطار مجلس
إيجاد مقعد
الخطوات المقبلة
كتاب مفتوح
قرأ
<@skipEnd>
@--------------------------------------------------------------------------------
