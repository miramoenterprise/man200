@--------------------------------------------------------------------------------
@- convertTabsToCells
@-
@- External dependencies: None
@-
@- e.g. <|convertTabsToCells> {		AAAA	B		CCCC}
@-
@- will define variable 'tabsToCells' so that it looks like this:
@-
@- <Cell/><Cell/><Cell><P <#cttc.pgfFormat>>AAAA</P></Cell><Cell cs="2"><P>B</P></Cell>
@-		<Cell cs="R" ><P>CCCC</P></Cell>
@-
@- (the above all on one line)
@-
@- Prefix-tabs (series of tabs before the first text element) are replaced
@- by '<Cell/>'s.
@-
@- Text elements immediately followed by a series of two or more adjacent
@- tabs are placed in spanned cells, i.e. <Cell cs="n" >, where 'n' is
@- the number of tabs in the series.
@-
@- The last text element in a line is always placed in a <Cell> with 
@- cs set to 'R'. E.g. <Cell cs="R" ><P>CCCC</P></Cell>
@-
@--------------------------------------------------------------------------------
<#def> nl	{
}
@--------------------------------------------------------------------------------
<@macro> convertTabsToCells  {
	@------------------------------------------------------------------------
	@- <#cb.lineTextDest> defined in codeBlock.mmp (in 'exmacs' )
	@------------------------------------------------------------------------
	<#ifndef> cb.lineTextDest	{}
	<#ifndef> cb.textSize		{}
	@------------------------------------------------------------------------
	<#def> cttc.input		$1		@--- Input line
	<#def> cttc.pos			1		@--- Line character position
	<#def> cttc.pgfFormat		{fmt="P_codeBlock"}
	@if(@len(<#cb.textSize>) > 0 ) {
		<#def> cttc.pgfFormat		{<#cttc.pgfFormat> p="<#cb.textSize>" }
		}
	@- <#def> cttc.pgfNumberFormat	{fmt="P_codeBlock"}
	<#def> cttc.elementNum		0		@--- Element num (tab / text)
	<#def> cttc.tabString		{-t-a-b}
	<#def> cttc.tabStringLen	@len(<#cttc.tabString>)
	<#def> cttc.tabStringLen	@eval(<#cttc.tabStringLen> + 1)
        @------------------------------------------------------------------------
	@- Remove trailing white space, if any (redundant?)
	<#def> cttc.outputText		@rtrim(<#cttc.input>)
	<#def> cttc.lineLength		@len(<#cttc.input>)
	@ifnot(<#cttc.lineLength>) {
		@- <@write> error {AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA}
		}
	<#def> cttc.outputText		@gsub({<#cttc.input>}, {	|<tab/>}, {<#cttc.tabString>})
        @------------------------------------------------------------------------
	@- Remove trailing tabs, if any (redundant?)
@- <@write> error {Line}
	@while(@match({z<#cttc.outputText>}, {<#cttc.tabString>$})) {
		<#def> cttc.outputText             @sub({<#cttc.outputText>}, {<#cttc.tabString>$}, {})
		}
	<#def> cttc.outputText             @rtrim(<#cttc.outputText>)
        @------------------------------------------------------------------------
        @- Replace tabs with col indicators
        @------------------------------------------------------------------------
	<#def> cttc.firstNonTab		0	@--- Array position of first non-tab text
	<#def> cttc.textCount		0	@--- Num tabs before initial text
	@while(<#cttc.pos> >  0 AND <#cttc.lineLength> ) {
@- <@write> error {|<#cttc.outputText>|}
		<#def> cttc.elementNum		@eval(<#cttc.elementNum> + 1)
		<#def> cttc.pos			@match({<#cttc.outputText>}, {<#cttc.tabString>})
@- <@write> error {<#cttc.pos>}
		@if(<#cttc.pos> = 0) {
			@- No tabs or no more tabs
			<#def> le.text				@substr({<#cttc.outputText>}, <#cttc.pos>)
@- <@write> error {|<#le.text>|}
			<#def> element[<#cttc.elementNum>]	{<#le.text>}
			@ifnot(<#cttc.firstNonTab>) {
				<#def> cttc.firstNonTab		<#cttc.elementNum>	@--- First non-tab text
@- <@write> error {			fnt <#cttc.firstNonTab>}
				}
			}
		@else	{
			@if(<#cttc.pos> = 1) {
				@- Tab at the current beginning.
				<#def> element[<#cttc.elementNum>]	{<#cttc.tabString>}
				<#def> cttc.outputText	@substr({<#cttc.outputText>}, <#cttc.tabStringLen>)
				}
			@else	{
				@- Text at the current beginning.
				<#def> cttc.textCount			@eval(<#cttc.textCount> + 1)
				@if(<#cttc.textCount> = 1) {
					@ifnot(<#cttc.firstNonTab>) {
						<#def> 	cttc.firstNonTab		<#cttc.elementNum>	@--- First non-tab text
@- <@write> error {			fnt <#cttc.firstNonTab>}
						}
					}
				@else {
					@- Another tab, not at the current beginning.
					<#def> cttc.pos		@eval(<#cttc.pos> - 1)
					<#def> le.text		@substr({<#cttc.outputText>}, 1, <#cttc.pos>)
					<#def> element[<#cttc.elementNum>]	{<#le.text>}
					<#def> cttc.pos		@eval(<#cttc.pos> + 1)
					<#def> cttc.outputText	@substr({<#cttc.outputText>}, <#cttc.pos>)
@- <@write> error {|<#le.text>|}
					}
				}
			@- $element[<#cttc.elementNum>]
			}
		}
@- <@write> error {			fnt <#cttc.firstNonTab>}
        @------------------------------------------------------------------------
        @- Reverse through the array
        @- If input text contains markup, it should be pre-sanitized
        @------------------------------------------------------------------------
	<#def> cttc.lastElement		<#cttc.elementNum>
	<#def> cttc.spanCount		0
	<#def> cttc.line		{}
	@while(<#cttc.elementNum> > 0 AND <#cttc.lineLength> ) {
		@if(<#cttc.elementNum> = <#cttc.lastElement>) {
        		@--------------------------------------------------------
        		@- Might also be first element
			<#def> cttc.line		{<Cell cs="R"><P <#cttc.pgfFormat>>$element[<#cttc.elementNum>]<#cb.lineTextDest></P></Cell>}
			<#def> cttc.elementNum		@eval(<#cttc.elementNum> - 1)
			}
		@if(<#cttc.elementNum> > 0 AND <#cttc.elementNum> < <#cttc.lastElement> ) {
			@if(@match({$element[<#cttc.elementNum>]}, {<#cttc.tabString>$})
					AND <#cttc.elementNum> > <#cttc.firstNonTab> ) {
				<#def> cttc.spanCount		@eval(<#cttc.spanCount> + 1)
				<#def> cttc.elementNum		@eval(<#cttc.elementNum> - 1)
				}
			@else	{
				@if(<#cttc.elementNum> >= <#cttc.firstNonTab>) {
					@if(<#cttc.spanCount> > 1) {
						@--- Cell spanning
						<#def> cttc.line	{<Cell cs="<#cttc.spanCount>"><P <#cttc.pgfFormat>>$element[<#cttc.elementNum>]</P></Cell><#cttc.line>}
						}
					@else	{
						@--- No cell spanning
						<#def> cttc.line	{<Cell><P <#cttc.pgfFormat>>$element[<#cttc.elementNum>]</P></Cell><#cttc.line>}
						}
				<#def> cttc.elementNum		@eval(<#cttc.elementNum> - 1)
				<#def> cttc.spanCount	0
					}
				}
			}
		@if(<#cttc.elementNum> < <#cttc.firstNonTab> AND <#cttc.elementNum> > 0) {
			<#def> cttc.line		{<Cell/><#cttc.line>}
@- <@write> error {<#cttc.elementNum> <	 <#cttc.firstNonTab>}
			@--- Hack
			@- <#def> cttc.line		{<#cttc.line>}
			<#def> cttc.elementNum		@eval(<#cttc.elementNum> - 1)
			}
		}
	@--- <@write> error {Tabs to cells line: <#cttc.line>}
	<#def> tabsToCells	{<#cttc.line>}
}
@--------------------------------------------------------------------------------
@- <|convertTabsToCells> {		AAAA	B		CCCC	xx 1			}
@- <|convertTabsToCells> {FFF}
@--------------------------------------------------------------------------------
