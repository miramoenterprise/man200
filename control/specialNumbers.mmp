@--------------------------------------------------------------------------------
@- specialNumbers.mmp
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<@xmacro> annoNumberMap {
	@------------------------------------------------------------------------
	@- cnum 	= circled numbers (black on white background)
	@- ncnum 	= negative circled numbers (white on black background)
	@- pnum 	= Parenthised numbers (white background)
	@------------------------------------------------------------------------
	@- apnum 	= TBA Parenthised latin lowercase letters (white background)
	@-			(a) - (z) 249C - 24B5
	@- Acnum 	= TBA Circled latin uppercase letters (white background)
	@-			(A) - (Z) 24B6 - 24CF
	@- acnum 	= TBA Circled latin lowercase letters (white background)
	@-			(a) - (z) 24D0 - 24E9
	@------------------------------------------------------------------------
	@-Unicode circled digits  0 - 20 (white background)
	<#def> cnumCount	21
	<#def> cnum[0]		24EA
	<#def> cnum[1]		2460
	<#def> cnum[2]		2461
	<#def> cnum[3]		2462
	<#def> cnum[4]		2463
	<#def> cnum[5]		2464
	<#def> cnum[6]		2465
	<#def> cnum[7]		2466
	<#def> cnum[8]		2467
	<#def> cnum[9]		2468
	<#def> cnum[10]		2469
	<#def> cnum[11]		246A
	<#def> cnum[12]		246B
	<#def> cnum[13]		246C
	<#def> cnum[14]		246D
	<#def> cnum[15]		246E
	<#def> cnum[16]		246F
	<#def> cnum[17]		2470
	<#def> cnum[18]		2471
	<#def> cnum[19]		2472
	<#def> cnum[20]		2473
	@------------------------------------------------------------------------
	@for(num = 0 to <#cnumCount>) {
		<#def> cnumb[$$num]	@ucs2_to_utf8($cnum[$$num])
		<#def> cnumb[$$num]	@hex2bin($cnumb[$$num])
		}
	@------------------------------------------------------------------------
	<#def> pnumCount	20
	<#def> pnum[1]		2474	@- Parenthesized digit one
	<#def> pnum[2]		2475
	<#def> pnum[3]		2476
	<#def> pnum[4]		2477
	<#def> pnum[5]		2478
	<#def> pnum[6]		2479
	<#def> pnum[7]		247A
	<#def> pnum[8]		247B
	<#def> pnum[9]		247C
	<#def> pnum[10]		247D
	<#def> pnum[11]		247E
	<#def> pnum[12]		247F
	<#def> pnum[13]		2480
	<#def> pnum[14]		2481
	<#def> pnum[15]		2482
	<#def> pnum[16]		2483
	<#def> pnum[17]		2484
	<#def> pnum[18]		2485
	<#def> pnum[19]		2486
	<#def> pnum[20]		2487
	@------------------------------------------------------------------------
	@for(num = 0 to <#pnumCount>) {
		<#def> pnumb[$$num]	@ucs2_to_utf8($pnum[$$num])
		<#def> pnumb[$$num]	@hex2bin($pnumb[$$num])
		}
	@------------------------------------------------------------------------
	<#def> cn0		{$cnumb[0]}
	<#def> cn1		{$cnumb[1]}
	<#def> cn2		{$cnumb[2]}
	<#def> cn3		{$cnumb[3]}
	<#def> cn4		{$cnumb[4]}
	<#def> cn5		{$cnumb[5]}
	<#def> cn6		{$cnumb[6]}
	<#def> cn7		{$cnumb[7]}
	<#def> cn8		{$cnumb[8]}
	<#def> cn9		{$cnumb[9]}
	<#def> cn10		{$cnumb[10]}
	<#def> cn11		{$cnumb[11]}
	<#def> cn12		{$cnumb[12]}
	<#def> cn13		{$cnumb[13]}
	<#def> cn14		{$cnumb[14]}
	<#def> cn15		{$cnumb[15]}
	<#def> cn16		{$cnumb[16]}
	<#def> cn17		{$cnumb[17]}
	<#def> cn18		{$cnumb[18]}
	<#def> cn19		{$cnumb[19]}
	<#def> cn20		{$cnumb[20]}
}{}
@--------------------------------------------------------------------------------
<annoNumberMap/>
@--------------------------------------------------------------------------------
<@xmacro> - pnum {
	<#def> pnum.fontFamily		{Symbola}
	<#def> pnum.fontFamily		{Segoe UI Symbol}
	<#def> pnum.fontFamily		{Simsum}
	<#def> pnum.fontFamily		{Microsoft YaHei}
	<#def> pnum.verticalShift	-10%
	<#def> pnum.verticalShift	0
	<#def> pnum.textSize		-15%
	<#def> pnum.number		{}
	@------------------------------------------------------------------------
	@xgetvals(pnum.)
	@------------------------------------------------------------------------
}{
	<#def> pnum.number	$pnum
	@if(@isInt(<#pnum.number>)) {
		<#def> pnum.number	$pnum[<#pnum.number>]
		<#def> pnum.number	{&#x<#pnum.number>;}
		@- <@write> error {"pnum"  <#pnum.number> mknv !!}
		<#def> pnum.number	{<Font
						fontFamily="<#pnum.fontFamily>"
						verticalShift="<#pnum.verticalShift>"
						textSize="<#pnum.textSize>"
						@- >$pnum[<#pnum.number>]</Font>}
						><#pnum.number></Font>}
		@print({<#pnum.number>}, n)
		}
	@else {
		<@write> error {"pnum" element contains non-integer value!!}
		}
}
@--------------------------------------------------------------------------------
<@xmacro> - cnum {
	@------------------------------------------------------------------------
	@- <cnum>n</cnum>
	@- 'n' may be any number in the range 0 to 20
	@------------------------------------------------------------------------
	<#def> cnum.fontFamily		{European Pi Std 1}
	<#def> cnum.verticalShift	-10%
	<#def> cnum.verticalShift	0
	<#def> cnum.textSize		-15%
	<#def> cnum.number		{}
	@------------------------------------------------------------------------
	@xgetvals(cnum.)
	@------------------------------------------------------------------------
}{
	<#def> cnum.number	$cnum
	@if(@isInt(<#cnum.number>)) {
		<#def> cnum.number	$cnum[<#cnum.number>]
		<#def> cnum.number	{&#x<#cnum.number>;}
		@- <@write> error {"cnum"  <#cnum.number> mknv !!}
		<#def> cnum.number	{<Font
						fontFamily="<#cnum.fontFamily>"
						verticalShift="<#cnum.verticalShift>"
						textSize="<#cnum.textSize>"
						@- >$cnum[<#cnum.number>]</Font>}
						><#cnum.number></Font>}
		@print({<#cnum.number>}, n)
		}
	@else {
		<@write> error {"cnum" element contains non-integer value!!}
		}
}
@--------------------------------------------------------------------------------
<@xmacro> - cnumb {
	@------------------------------------------------------------------------
	@- <cnumb>n</cnumb>
	@- 'n' may be any number in the range 0 to 20
	@------------------------------------------------------------------------
	<#def> cnumb.fontFamily		{European Pi Std 1}
	<#def> cnumb.verticalShift	-10%
	<#def> cnumb.verticalShift	0
	<#def> cnumb.textSize		-15%
	<#def> cnumb.number		{}
	@------------------------------------------------------------------------
	@xgetvals(cnumb.)
	@------------------------------------------------------------------------
}{
	<#def> cnumb.number	$cnumb
	@if(@isInt(<#cnumb.number>)) {
		<#def> cnumb.number	$cnum[<#cnumb.number>]
		<#def> cnumb.number	@ucs2_to_utf8(<#cnumb.number>)
		<#def> cnumb.number	@hex2bin(<#cnumb.number>)
		<#def> cnumb.number	{<Font fontFamily="<#cnumb.fontFamily>" verticalShift="<#cnumb.verticalShift>" textSize="<#cnumb.textSize>" ><#cnumb.number></Font>}
@- <xfEPS1><#euroPi2></xfEPS1>
		@print({<xfEPS1><#cnumb.number></xfEPS1>}, n)
		}
	@else {
		<@write> error {"cnumb" element contains non-integer value!!}
		}
}
@--------------------------------------------------------------------------------
<@xmacro> - xxxcnum {	@- delete
	@------------------------------------------------------------------------
	@- <cnum>n</cnum>
	@- 'n' may be any number in the range 0 to 20
	@------------------------------------------------------------------------
	<#def> cnum.fontFamily		{European Pi Std 1}
	<#def> cnum.verticalShift	-10%
	<#def> cnum.verticalShift	0
	<#def> cnum.textSize		-15%
	<#def> cnum.number		{}
	@------------------------------------------------------------------------
	@xgetvals(cnum.)
	@------------------------------------------------------------------------
}{
	<#def> cnum.number	$cnum
	@if(@isInt(<#cnum.number>)) {
		<#def> cnum.number	{<Font
						fontFamily="<#cnum.fontFamily>"
						verticalShift="<#cnum.verticalShift>"
						textSize="<#cnum.textSize>"
						>$cnum[<#cnum.number>]</Font>}
		@print({<#cnum.number>}, n)
		}
	@else {
		<@write> error {"cnum" element contains non-integer value!!}
		}
}
@--------------------------------------------------------------------------------
