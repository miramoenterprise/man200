@----------------------------------------------------------------------------
@- mmp.stringdefs
@----------------------------------------------------------------------------
@- CRIN made this a macro def + call to allow different font
@- family names using @if (<#OS> = nt) ...
@----------------------------------------------------------------------------
<#def> nl	{
}
<@map> emdash		abcd
@----------------------------------------------------------------------------
<@macro> set.stringdefs {
<#def> runas		{RunAs}
<#def> MRef		{<Font fmt="F_Italic">Miramo Reference Guide</Font>}
<#def> MCRef		{<Font fmt="F_Italic">Miramo Character Reference Guide</Font>}
<#def> mmDrawRef	{<Font fmt="F_Italic">Miramo mmDraw Reference Guide</Font>}
<#def> emGuide		{<Font fmt="F_Italic">Miramo Enterprise Module Guide</Font>}
<#def> URef		{<Font fmt="F_Italic">Miramo User Guide</Font>}
<#def> UGuide		{<Font fmt="F_Italic">Miramo User Guide</Font>}
<#def> MUGuide		{<Font fmt="F_Italic">Miramo User Guide</Font>}
<#def> xM		{<Font fcolor="xMaroon">}
<#def> xnM		{</Font>}
@----------------------------------------------------------------------------
<#def> string		{<Font fmt="F_Italic">string</Font>}
<#def> dim		{<Font fmt="F_Italic">dim</Font>}
<#def> command		{<Font fmt="F_Italic">command</Font>}
<#def> filename		{<Font fmt="F_Italic">filename</Font>}
<#def> name		{<Font fmt="F_Italic">name</Font>}
<#def> Required		{<Font fmt="F_Italic">Required</Font>}
<#def> mmprint		{<FontRef fmt="F_Italic">mmprint</FontRef>}
<#def> mmchart		{<FontRef fmt="F_Italic">mmchart</FontRef>}
<#def> mmChart		{<FontRef fmt="F_Italic">mmChart</FontRef>}
<#def> mmxslt		{<FontRef fmt="F_Italic">mmxslt</FontRef>}
<#def> SectHead		{<Font fmt="F_SectionHeadName" p="8.5pt" >}
<#def> secthead		{<Font fmt="F_SectionHeadName" p="8.5pt" >}
<#def> nSectHead	{</Font>}
<#def> nsecthead	{</Font>}
<#def> fmprint		{<FontRef fmt="F_Italic">fmprint</FontRef>}
<#def> fmbatch		{<FontRef fmt="F_Italic">fmbatch</FontRef>}
<#def> fmps2eps		{<FontRef fmt="F_Italic">fmps2eps</FontRef>}
<#def> astrix   	{<Font Ky="20">*</Font>}
<#def> formatstring	{<Font fmt="F_Input">dcpflLRruvtkxTF</Font>}
<#def> nbsp		{&#xA0;}
<#def> nbsp2		{&nbsp;&nbsp;}
<#def> nbsp3		{&nbsp;&nbsp;&nbsp;}
<#def> nbsp4		{&nbsp;&nbsp;&nbsp;&nbsp;}
<#def> nbsp5		{&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
<#def> nbsp6		{&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
<#def> nbsp7		{&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
<#def> vbar		{<Font fw="Regular" ff="Courier" Ky="-7" p="7pt" fs="80" >|</Font>}
<#def> I		{<FontRef fmt="F_I" >I</FontRef>}

<#def> eq		{&nbsp;=&nbsp;}
<#def> image.types	{most variations of: BMP, GIF, JPEG, PDF, PNG, TIFF and SVG}
@----------------------------------------------------------------------------
@- non breaking hyphen
<#def> nbhy		{&#x2011;}
@----------------------------------------------------------------------------
<#def> fI		{<FontRef fmt="F_Italic">}
<#def> fnI		{</FontRef>}
	@if (<#OS> = nt) {
		<#def> ffI	{<Font fmt="F_Italic" ff="Frutiger 55 Roman" fw="Regular" >}
		<#def> ffnI	{</Font>}
	}
	@else {
		<#def> ffI	<#fI>
		<#def> ffnI	<#fnI>
	}
<#def> fUniName		{<Font ff="Frutiger LT 45 Light" fa="Regular" fw="Regular" fv="Regular" fc="S">}
<#def> fnUniName	{</Font>}
<#def> fSC		{<Font fc="S">}		@- Small caps
<#def> fnSC		{</Font>}		@- Small caps end
<#def> fSC		{<FontRef fmt=F_Smallcaps>}		@- Small caps
<#def> fnSC		{</FontRef>}		@- Small caps end
<#def> fU		{<Font u="Y">}		@- Upshift
<#def> fD		{<Font d="Y">}		@- Downshift
<#def> nF		{</Font>}
<#def> fB		{<FontRef fmt="F_Bold">}
<#def> fnB		{</FontRef>}
<#def> fCode		{<FontRef fmt="F_Code">}
<#def> fC		{<FontRef fmt="F_Code">}
<#def> fnCode		{</FontRef>}
<#def> fnC		{</FontRef>}
<#def> fCCode		{<FontRef fmt="F_Input_Courier">}
<#def> fnCCode		{</FontRef>}
<#def> hairsp		{&#x200A;}
<#def> V		{<FontRef fmt="F_OptValue">}
<#def> nV		{</FontRef>}
<#def> Unix		{Solaris}
<#def> nbh		{���}
<#def> osq		{&#x2018;}		@-- e28098
<#def> csq		{&#x2019;}		@-- e28099
<#def> osqb		{&#x2018;}		@-- e28098
<#def> csqb		{&#x2019;}		@-- e28099
<#def> sq		{&#x27;}		@-- Single quote
<#def> dq		{&#x22;}		@-- Double quote
@----------------------------------------------------------------------------
@--- The following is used in aarframe.inc
<#def> small_spacer_para	{<P fmt="P_Inline_Opt" an="N" fi="12mm" li="12mm" p="2" l="-2" sa="1" sb="1">}
@----------------------------------------------------------------------------
}
<|set.stringdefs>
@----------------------------------------------------------------------------
<@xmacro>  pdfReference1.4	{
	@print({<#fI>PDF Reference, version 1.4<#fnI>, 3rd
Edition, Adobe Systems Incorporated, Addison-Wesley,
Boston, 2001. ISBN 0-201-75839-3}, n)
	}
@----------------------------------------------------------------------------
<#def> PSRef2	{<Font fmt="F_Italic">PostScript Language Reference Manual,
Second Edition</Font>, Adobe Systems
Incorporated, Addison-Wesley, Reading,
Mass. 1990. ISBN 0-201-18127-4}
@----------------------------------------------------------------------------
<#def> PSRef3	{<Font fmt="F_Italic">PostScript Language
Reference Manual, Third Edition</Font>, Adobe Systems
Incorporated, Addison-Wesley, Reading,
Mass. 1999. ISBN 0-201-37922-8}
@----------------------------------------------------------------------------
<#def> pname	{On NT only, <#fI>filename<#fnI> may optionally be
followed by a comma and then the name, <#fI>pname<#fnI>, of the
printer driver to be used. See Note
@- <xparanumonly id="doc.note.pname" />
 on page
@- <xnpage id="doc.note.pname" />.}
}
@----------------------------------------------------------------------------
