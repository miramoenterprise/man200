@---------------------------------------------------------------
@- mmp.xnames
@---------------------------------------------------------------
@- 
@-	This handles every (inline and format definition) markup 
@-	code.
@-	
@-	For every markup code:
@-	
@-		A variable <#yMarkUP> is defined,
@-			e.g. <#yTbl>  --->  </Tbl>	(&lt;/Tbl>)
@-	
@-		A variable <#xMarkUP> is defined,
@-			e.g. <#xTbl>  --->  <Tbl ... >	(&lt;Tbl ... >)
@-	
@-			The definitions for <#xMarkUp> vary
@-			depending on whether $2 is set to:
@-	
@-				atts_and_cont
@-				atts_only
@-				cont_only
@-				standalone
@-				pi
@-				di
@-
@-
@-	codeprops[ n, 1 ]		Base code name, e.g. Tbl
@-
@-
@-	codeprops[ n, 2 ]		Options end string: > or />
@-
@-	codeprops[ n, 3 ]		Options end text:  long stuff
@-
@-	codeprops[ n, 4 ]		Code termination status: R or O or N
@-					viz. Required or Optional or None
@-					eg. </Tbl> is required. OTOH
@-					no explicit termination of <P> is required.
@-					Also <Cell> has implied termination.
@-
@-	codeprops[ n, 5 ]		Code end string:  e.g. </Tbl> for <Tbl ... >
@-					or:
@-					  </Cell>, </Row>, </Tbl>, 
@-					  <Cell>, <Row>
@-					for <Cell>. OR ?> for System.
@-
@-	codeprops[ n, 6 ]		Code end text:  long stuff
@-					(Options end)
@-
@-	codeprops[ n, 7 ]		Code ref. name.
@-					E.g. <Cell ... >
@-	
@-
@-	
@---------------------------------------------------------------
<#def> code_count	0
<#def> code_no		0
<#def> xstrings		{}
<@macro> ch_codename {
	<#def> code_count	@eval(<#code_count> + 1)
	@-------------------------------------------------------
	@for(i = 1 to 10) {
		<#def> codeprops[<#code_count>, $$i]         {}
		}
	@-------------------------------------------------------
	<#def> codeprops[<#code_count>, 2]         {&gt;}
	@-------------------------------------------------------
	@--- $1	is yCode
	@--- $2	is xCode
	@--- $3	is iCode
	@--- $4	is key for format of xCode
	@--- 
	@-------------------------------------------------------
	@--- Set var defs for terminating codes:
	@-------------------------------------------------------
	@--- 
	@--- e.g. so <#yAFrame> expands to </AFrame>
	@--- 
	@-------------------------------------------------------
	<#def> $1	{&lt;/<#xcname>&gt;}
	<#def> ycode	{&lt;/<#xcname>&gt;}
	<#def> bxcode	{&lt;<#xcname>} @-- Beginning of Syntax
	<#def> xstrings	{<#xstrings>
<#xcname>}
	<#def> codeprops[<#code_count>, 3]         {
		<|PRINT> <#ffI>Required<#ffnI>
		<|PRINT> <br/>
		<|PRINT> <Font fmt="F_NoLang" >&lt;<#xcname></Font> options must be terminated by >.
}
	@-------------------------------------------------------
	@-- atts_and_cont :
	@--
	@--  <Code  ... >
	@--
	@-------------------------------------------------------
	@if(@match({$4}, {atts_and_cont})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang"><#hypercmd>&lt;<#xcname> ... &gt;</FontRef>}
	 	<#def> $3	{&lt;<#xcname> ... &gt;}
	 	<#def> xcode	{&lt;<#xcname> ... &gt;}
	 	}
	@-------------------------------------------------------
	@-- mustHaveContent :
	@--
	@--  <List  ... > </List>
	@--
	@-------------------------------------------------------
	@if(@match({$4}, {mustHaveContent})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang"><#hypercmd>&lt;<#xcname> ... &gt;</FontRef>}
	 	<#def> $3	{&lt;<#xcname> ... &gt;}
	 	<#def> xcode	{&lt;<#xcname> ... &gt;}
		<#def> codeprops[<#code_count>, 5]         {<Font lineBreak="N"
		>&gt; <Font fontWeight="Regular" fontAngle="Italic" >content</Font> &lt;/<#xcname>&gt;</Font>}
	 	}
	@--
	@-------------------------------------------------------
	@-- atts_only :
	@--
	@--  <Code  ... />
	@--
	@-------------------------------------------------------
	@if(@match({$4}, {atts_only})) {
		@- The next line stops, e.g., <#xImage> (for <Image ... />) line breaking
		@- between / and >
	 	<#def> $2	{<FontRef
				fmt="F_NoLang"><#hypercmd>&lt;<#xcname> ... <Font
					lineBreak="N" >/&gt;</Font></FontRef>}
				@- fmt="F_NoLang"><#hypercmd>&lt;<#xcname> ... >/&gt;</FontRef>}
	 	@- <#def> $3	{&lt;<#xcname> ... /&gt;}
	 	<#def> $3	{&lt;<#xcname> ... <Font lineBreak="N" >/&gt;</Font>}
		@-----------------------------------------------
		@- Following is used in elementName & TOC (other places?)
		@-----------------------------------------------
	 	<#def> xcode	{&lt;<#xcname> ... /&gt;}
	 	<#def> xcode	{&lt;<#xcname> ... z&gt;}
	 	<#def> xcode	{&lt;<#xcname> ... <Font lineBreak="N" >/&gt;</Font>}
	 	@- <#def> xcode	{&lt;<#xcname> ... <Font lineBreak="N" >/&gt;</Font>}
		@-----------------------------------------------
		<#def> codeprops[<#code_count>, 2]         {/&gt;}
		<#def> codeprops[<#code_count>, 5]         {/&gt;}
		<#def> codeprops[<#code_count>, 2]         {<Font lineBreak="N" >/&gt;</Font>}
		@-----------------------------------------------
		<#def> codeprops[<#code_count>, 3]         {
			<|PRINT> <#ffI>Required<#ffnI>
			<|PRINT> <br/>
			<|PRINT> <Font fmt="F_NoLang" >&lt;<#xcname></Font> options must be terminated by  />.
			<|PRINT> (Alternatively
			<|PRINT> <Font fmt="F_NoLang" >&lt;<#xcname></Font> options may be terminated by >
			<|PRINT> followed by
			<|PRINT> &lt;/<#xcname>> with no text between the
			<|PRINT> &lt;<#xcname> ... > and the
			<|PRINT> &lt;/<#xcname>&gt;.)
}
	 	}
	@--
	@-------------------------------------------------------
	@-- pi : (processing instruction, e.g System
	@--
	@--  <Code  ... />
	@--
	@-------------------------------------------------------
	@if(@match({X$4X}, {XpiX})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang"><#hypercmd>&lt;?<#xcname> ... ?&gt;</FontRef>}
	 	<#def> $3	{&lt;?<#xcname> ... ?&gt;}
	 	<#def> xcode	{&lt;?<#xcname> ... ?&gt;}
		<#def> codeprops[<#code_count>, 2]         {?&gt;}
		<#def> codeprops[<#code_count>, 5]         {?&gt;}
		<#def> bxcode	{&lt;?<#xcname>} @-- Beginning of Syntax
		<#def> codeprops[<#code_count>, 6]         {
			<|PRINT> <#ffI>Required<#ffnI>
			<|PRINT> <br/>
			<|PRINT> <Font fmt="F_NoLang" >&lt;?<#xcname></Font> contents must be terminated by  ?&gt;.
}
	 	}
	@--
	@-------------------------------------------------------
	@-- di : <!DOCTYPE>
	@--
	@-------------------------------------------------------
	@if(@match({X$4X}, {XdiX})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang">&lt;!<#xcname>&gt;</FontRef>}
	 	<#def> $3	{&lt;!<#xcname>&gt;}
	 	<#def> xcode	{&lt;!<#xcname>&gt;}
		<#def> codeprops[<#code_count>, 2]         {&gt;}
		<#def> codeprops[<#code_count>, 5]         {&gt;}
		<#def> bxcode	{&lt;!<#xcname>} @-- Beginning of Syntax
		<#def> codeprops[<#code_count>, 6]         {
			<|PRINT> <#ffI>Required<#ffnI>
			<|PRINT> <br/>
			<|PRINT> <Font fmt="F_NoLang" >&lt;!<#xcname></Font> contents must be terminated by  &gt;.
}
	 	}
	@--
	@-------------------------------------------------------
	@-- standalone :
	@--
	@--  <Code/>
	@--
	@-------------------------------------------------------
	@if(@match({$4}, {standalone})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang"><#hypercmd>&lt;<#xcname>/&gt;</FontRef>}
	 	<#def> $3	{&lt;<#xcname>/&gt;}
	 	@- <#def> xcode	{&lt;<#xcname>/&gt;}
	 	<#def> xcode	{&lt;<#xcname><Font lineBreak="N" >/&gt;</Font>}
		<#def> codeprops[<#code_count>, 2]         {/&gt;}
		<#def> codeprops[<#code_count>, 2]         {<Font lineBreak="N" >/&gt;</Font>}
		@- <#def> codeprops[<#code_count>, 3]         {no_opts}
	 	}
	@--
	@-------------------------------------------------------
	@-- cont_only :
	@--
	@--  <Code>
	@--
	@-------------------------------------------------------
	@if(@match({$4}, {cont_only})) {
	 	<#def> $2	{<FontRef fmt="F_NoLang"><#hypercmd>&lt;<#xcname>&gt;</FontRef>}
	 	<#def> $3	{&lt;<#xcname>&gt;}
	 	<#def> xcode	{&lt;<#xcname>&gt;}
	 	}
	@--
	@-------------------------------------------------------
	<#def> codeprops[<#code_count>, 1]         {<#xcname>}
	<#def> codeprops[<#code_count>, 4]         {$5}
	<#def> codeprops[<#code_count>, 8]         {$4}
	@-------------------------------------------------------
	@if($# > 4 AND @len($6) > 1 AND @len($codeprops[<#code_count>, 5]) = 0 ) {
		<#def> tmp	@gsub({x3x3xx$6}, {<}, {\&lt;})
		<#def> tmp	@sub({<#tmp>}, {x3x3xx}, {})
		<#def> tmp	@sub({<#tmp>}, {OR}, {<Font fw="Light">or</Font>})
		<#def> tmpLeft	@gsub({<#tmp>}, {--}, {<br/>})
		<#def> tmpRight	@gsub({<#tmp>}, {--}, {})
		<#def> codeprops[<#code_count>, 5]         {<#tmpLeft>}
		}
	@else	{
		@if(@len($codeprops[<#code_count>, 5]) = 0) {
			<#def> codeprops[<#code_count>, 5]         {&lt;/<#xcname>&gt;}
			}
		}
	@-------------------------------------------------------
	@if(@match({x$5}, {xR} )) {
		@-- This code termination is REQUIRED
		<#def> codeprops[<#code_count>, 6]         {
			<|PRINT> <#ffI>Required<#ffnI>
			<|PRINT> <br/>
@-- codeprops[ n, 1 ]
@--			@if(@match({X-<#CodeName1>-X}, {X-ChartY-X})) {
			@if(@match({X-$codeprops[<#code_count>, 1]-X}, {X-ChartY-X})) {
				<|PRINT> Every <#xcode> code must include at least
				@- <|PRINT> one &lt;d&gt; code and must be terminated by
				<|PRINT> one <#xd> and <#yd> code pair and must be terminated by
				}
			@else	{
				<|PRINT> Every <Font fmt="F_NoLang" ><#xcode></Font> code must be terminated by
				}
			<|PRINT> a <#ycode> code.
}
		}
	@-------------------------------------------------------
	@if(@match({x$5}, {xO} )) {
		@-- This code termination is OPTIONAL
		<#def> codeprops[<#code_count>, 6]         {
			<|PRINT> A <#xcode> code is terminated by any of the
			<|PRINT> following: <#tmpRight>.
}
		}
	@-------------------------------------------------------
	@- <#def> xcode	@gsub({<#xcode>}, {&gt;}, {x-x})
	@- <#def> xcode	@gsub({<#xcode>}, {&lt;}, {x-x})
	<#def> codeprops[<#code_count>, 7]         {<#xcode>}
	<#def> codeprops[<#code_count>, 9]         {<#bxcode>}
	@- <@write> error {$codeprops[<#code_count>, 9]}
	@- <@write> error {$codeprops[<#code_count>, 7]}
}
@-==============================================================
@---------------------------------------------------------------
<@macro> find_codenum {
	@-------------------------------------------------------
	@--- Used in mmp.inline
	@--- to find the approriate n in codeprops[ n, 1 ]
	@-------------------------------------------------------
	@for(i = 1 to <#code_count>) {
		@if(@match({x$1x}, {x$codeprops[$$i, 1]x})) {
			<#def> code_no	$$i
			}
		}
	@ifnot(<#code_no>) {
		<@write> error {Code <$1>:	NO such code (add it to mmp.xnames file)}
		}
}
@---------------------------------------------------------------
@- Feeder macro: feeds options to 'ch_codename'
@---------------------------------------------------------------
<@macro> def_codename {
	@--- BUG ???
	<#def> hypercmd 	{}
	<#def> hypercmd_prefix	{<HyperCmd jumptodest="}
	<#def> xcname	{@trim($1)}
	@if($# > 4) {
		@if(@len($5) > 2) {
			<#def> hypercmd 	{}
			<#def> hypercmd {<#hypercmd_prefix><#xcname>.code.start" file="$5" />}
			}
		}
	<|ch_codename> y<#xcname>|x<#xcname>|i<#xcname>|@trim($2)|$3|$4
	<#def> codeprops[<#code_count>, 10]	{Starthypercmd jumptodest="<#xcname>.code.start" file="$5" Endhypercmd}
}
@---------------------------------------------------------------
<|def_codename>	MiramoFormatDefs	|cont_only   	|R
@---------------------------------------------------------------
<#def>	xe		{/&gt;}
<|def_codename> AFrame			|atts_and_cont	|R||inline
<|def_codename> MifFrame		|atts_only	|R||inline
<|def_codename>	ARFrame			|atts_only	|R||inline
<|def_codename>	FrameFill		|atts_only	|R||inline
<|def_codename>	xFrameShadow		|atts_only	|R||inline
<|def_codename>	xShadowDef		|atts_only	|R||inline
<|def_codename>	xFrameShadowDef		|atts_only	|R||inline
<|def_codename>	ChangeBarBegin		|atts_only	|R||inline
<|def_codename>	ChangeBarEnd		|atts_only	|R||inline
<|def_codename>	ChangeBarDef		|atts_only	|R||inline
<|def_codename>	ATextFrame		|atts_and_cont	|R||inline
<|def_codename>	FrameTitle		|atts_and_cont	|R||inline
<|def_codename>	ObjectTitle		|atts_and_cont	|R||inline
<|def_codename>	Attribute		|atts_and_cont	|R||inline
<|def_codename>	AutoNum			|atts_and_cont	|R||inline
<|def_codename>	ListLabel		|atts_and_cont	|R||inline
<|def_codename>	xListLabel		|atts_and_cont	|R||inline
<|def_codename>	BGMarker		|atts_and_cont	|R||inline
<|def_codename>	BackgroundMarker	|atts_and_cont	|R||inline
<|def_codename>	RunningTextMarker	|cont_only	|R||inline
<|def_codename>	RunningTextDef		|atts_and_cont	|R||inline
<|def_codename>	RunningText		|atts_and_cont	|R||inline
@- <|def_codename>	BGVar			|atts_only	|R||inline
@- <|def_codename>	RunningTextFormat	|atts_only	|R||inline
@- <|def_codename>	BGVarFmtDef		|atts_and_cont	|R||fdefs
<|def_codename>	br			|standalone   	|R
<|def_codename>	xcounter			|atts_only   	|R
<|def_codename>	counter			|atts_only   	|R
@- <|def_codename>	bgmarker		|atts_only   	|R
<|def_codename>	marker			|atts_only   	|R
<|def_codename>	d			|cont_only   	|R
<|def_codename>	tab			|standalone   	|R
<|def_codename>	Cell			|atts_and_cont	|R|
<|def_codename>	Chapter			|atts_and_cont	|R||inline
<|def_codename>	GenChapter		|atts_and_cont	|R||inline
@-------------
<|def_codename>	GenList			|atts_and_cont	|R||inline
<|def_codename>	GeneratedList		|atts_and_cont	|R||inline
<|def_codename>	Contents		|atts_and_cont	|R||inline
<|def_codename>	contentslabel		|atts_and_cont	|R||inline
<|def_codename>	ContentsDef		|atts_and_cont	|R||inline
<|def_codename>	GeneratedListInclude	|atts_and_cont	|R||inline
<|def_codename>	ContentsInclude		|atts_and_cont	|R||inline
<|def_codename>	GenInclude		|atts_only	|R||inline
<|def_codename>	ListInclude		|atts_only	|R||inline
<|def_codename>	listlabel		|atts_only	|R||inline
<|def_codename>	map			|atts_only	|R||inline
<|def_codename>	Listlabel		|atts_and_cont	|R||inline
<|def_codename>	MiramoRoleMap		|cont_only	|R||inline
<|def_codename>	listtext		|atts_only	|R||inline
<|def_codename>	leader			|atts_and_cont 	|R
<|def_codename>	endTab			|atts_and_cont 	|R
<|def_codename>	label			|atts_and_cont	|R||inline
@-------------
<|def_codename> sav			|atts_and_cont	|R||cht_inline
<|def_codename> AChart			|atts_and_cont	|R||cht_inline
<|def_codename> ChartArea		|atts_only	|R||cht_inline
<|def_codename> Bar			|atts_only	|R||cht_inline
<|def_codename> Pie			|atts_only	|R||cht_inline
<|def_codename> PlotArea		|atts_only	|R||cht_inline
<|def_codename>	Axis			|atts_and_cont	|R||cht_inline
<|def_codename>	AxisTitle		|atts_and_cont	|R||cht_inline
<|def_codename>	ChartLegendTitle	|atts_and_cont	|R||cht_inline
<|def_codename>	avl			|atts_and_cont	|R||cht_inline
<|def_codename>	d			|cont_only	|R||cht_inline
<|def_codename>	ChartFoot		|atts_and_cont	|R||cht_inline
<|def_codename>	ChartHead		|atts_and_cont	|R||cht_inline
<|def_codename>	ChartLegend		|atts_and_cont	|R||cht_inline
<|def_codename>	ChartText		|atts_and_cont	|R||cht_inline
<|def_codename>	ChartX			|atts_and_cont	|R||cht_inline
<|def_codename>	ChartY			|atts_and_cont	|R||cht_inline
<|def_codename>	Chart			|atts_and_cont	|R||cht_inline
@-------------
<|def_codename>	PDFpermissions		|atts_only	|R||fdefs
<|def_codename>	PDFbookmark		|atts_and_cont	|R||inline
@-------------
<|def_codename>	CmdData			|cont_only	|R||inline
<|def_codename>	ColorDef		|atts_only	|R||fdefs
<|def_codename>	PostProcess		|cont_only	|R||fdefs
<|def_codename>	Processor		|atts_only	|R||fdefs
<|def_codename>	ColorViewDef		|atts_only	|R||fdefs
<|def_codename>	CombinedFontDef		|atts_only	|R
<|def_codename>	Comment			|cont_only	|R||inline
<|def_codename>	Com			|cont_only	|R
<|def_codename>	ConditionDef		|atts_only	|R||fdefs
<|def_codename>	Conditional		|atts_only	|R||inline
<|def_codename>	DPage			|atts_only	|R||inline
<|def_codename>	Date			|atts_and_cont	|R||inline
<|def_codename>	DateFmtDef		|atts_and_cont	|R||fdefs
<|def_codename>	DateDef			|atts_and_cont	|R||fdefs
<|def_codename>	Divert			|pi		|X||inline
<|def_codename>	DocDef			|atts_only	|R||fdefs
<|def_codename>	jobOptions		|atts_only	|R||fdefs
<|def_codename>	NumberSequenceDef	|atts_only	|R||fdefs
<|def_codename>	Doc			|atts_and_cont	|O|</Doc> OR <Doc>|inline
<|def_codename>	DOCTYPE			|di	|X
<|def_codename>	EOF			|standalone	|R||inline
<|def_codename>	FNote			|atts_and_cont	|R||inline
<|def_codename>	FNoteDef		|atts_and_cont	|R||inline
<|def_codename>	Footnote		|atts_and_cont	|R||inline
<|def_codename>	FootnoteDef		|atts_and_cont	|R||inline
<|def_codename>	List			|mustHaveContent|R||inline
<|def_codename>	ListDef			|atts_only	|R||inline
<|def_codename>	ParaGroup		|mustHaveContent|R||inline
<|def_codename>	ParaGroupDef		|atts_only	|R||inline
<|def_codename>	FNoteSeparator		|atts_only	|R||inline
<|def_codename>	FileName		|atts_only	|R||inline
<|def_codename>	FileNameFmtDef		|atts_and_cont	|R||fdefs
<|def_codename>	FileNameDef		|atts_and_cont	|R||fdefs
<|def_codename>	Font			|atts_and_cont	|R||inline
<|def_codename>	fI			|cont_only	|R||inline
<|def_codename>	fB			|cont_only	|R||inline
<|def_codename>	fBI			|cont_only	|R||inline
<|def_codename>	FontRef			|atts_and_cont	|R||inline
<|def_codename>	FontDef			|atts_only	|R||fdefs
<|def_codename>	Line			|atts_only	|R||inline
<|def_codename>	Frame			|atts_and_cont	|R||inline
<|def_codename>	FrameImage		|atts_only	|R||inline
<|def_codename>	HyperCmd		|atts_and_cont	|R||inline
<|def_codename>	Index			|atts_and_cont	|R||inline
<|def_codename>	IndexGroupDef		|atts_and_cont	|R||inline
<|def_codename>	IndexLevel		|atts_and_cont	|R||inline
<|def_codename>	IndexPageDef		|atts_and_cont	|R||inline
<|def_codename>	GroupTitle		|atts_and_cont	|R||inline
<|def_codename>	groupTitle		|atts_and_cont	|R||inline
<|def_codename>	groupTitles		|atts_and_cont	|R||inline
<|def_codename>	IndexDef		|atts_and_cont	|R||inline
<|def_codename>	textsuffix		|atts_and_cont	|R||inline
<|def_codename>	pageRangeSeparator	|atts_and_cont	|R||inline
<|def_codename>	pageReferenceSeparator	|atts_and_cont	|R||inline
<|def_codename>	levelFormat		|atts_and_cont	|R||inline
<|def_codename>	entrySuffix		|atts_and_cont	|R||inline
<|def_codename>	IXsub			|atts_only	|R||inline
<|def_codename>	Image			|atts_and_cont	|R||inline
<|def_codename>	imageReplace		|atts_only	|R||inline
<|def_codename>	Import			|atts_only	|R||inline
<|def_codename>	Include			|atts_only	|R||inline
<|def_codename>	IX			|atts_and_cont	|R||inline
<|def_codename>	MapChar			|atts_and_cont	|R||fdefs
<|def_codename>	MasterPageRule		|atts_only	|R||inline
<|def_codename>	MasterPageRuleDef	|atts_only	|R||inline
<|def_codename>	Section			|atts_only	|R||inline
<|def_codename>	SectionDef		|atts_only	|R||inline
<|def_codename>	Marker			|atts_and_cont	|R||inline
<|def_codename>	Mif			|cont_only	|R||inline
<|def_codename>	MiramoXML		|atts_and_cont	|R
@-------------------------------------------------------------------
<|def_codename>	XMPMetaData		|cont_only	|R||fdefs
<|def_codename>	MetaData		|atts_and_cont	|R
@-------------------------------------------------------------------
@--- Footnote sub-elements
@-------------------------------------------------------------------
<|def_codename>	anchorStyle		|atts_only	|R||inline
<|def_codename>	anchorPrefix		|atts_only	|R||inline
<|def_codename>	anchorSuffix		|atts_only	|R||inline
<|def_codename>	anchorSeparator		|atts_only	|R||inline
@-
<|def_codename>	labelStyle		|atts_only	|R||inline
<|def_codename>	labelPrefix		|atts_only	|R||inline
<|def_codename>	footnoteLabelSuffix	|atts_only	|R||inline
@-------------------------------------------------------------------
@--- mmDraw codes
@-------------------------------------------------------------------
<|def_codename>	mmSVG			|cont_only	|R||inline
<|def_codename>	xSVG			|atts_and_cont	|R||inline
<|def_codename>	xSvgDef			|atts_and_cont	|R||inline
@- <|def_codename>	xMathML			|cont_only	|R||inline
<|def_codename>	MathML			|atts_and_cont	|R||inline
<|def_codename>	MathMLDef		|atts_only	|R||inline
<|def_codename>	Equation		|atts_and_cont	|R||inline
<|def_codename>	EquationDef		|atts_only	|R||inline
<|def_codename>	description		|atts_and_cont	|R||inline
<|def_codename>	math			|atts_and_cont	|R||inline
<|def_codename>	mmDraw			|cont_only	|R||inline
<|def_codename>	Rectangle		|atts_only	|R||md_inline
<|def_codename>	Arc			|atts_only	|R||md_inline
<|def_codename>	Circle			|atts_only	|R||md_inline
<|def_codename>	Ellipse			|atts_only	|R||md_inline
<|def_codename>	Point			|atts_only	|R||md_inline
<|def_codename>	PieSlice		|atts_and_cont	|R||md_inline
<|def_codename>	PieLegend		|atts_and_cont	|R||md_inline
<|def_codename>	PieSliceCallout		|atts_and_cont	|R||md_inline
<|def_codename>	PieSliceLegend		|atts_and_cont	|R||md_inline
<|def_codename>	PLineto			|atts_only	|R||md_inline
<|def_codename>	PolyArc			|atts_only	|R||md_inline
<|def_codename>	Polygon			|atts_and_cont	|R||md_inline
<|def_codename>	PolyLine		|atts_and_cont	|R||md_inline
<|def_codename>	ArrowDef		|atts_and_cont	|R||md_inline
<|def_codename>	TextLine		|atts_and_cont	|R||md_inline
<|def_codename>	ALine			|atts_and_cont	|R||md_inline
<|def_codename>	TextFrame		|atts_and_cont	|R||md_inline
@-------------------------------------------------------------------
<|def_codename>	fmPublish		|atts_only	|R||md_inline
@-------------------------------------------------------------------
@--- emGuide codes
@-------------------------------------------------------------------
<|def_codename>	PSRequest		|atts_and_cont	|R||em_inline
<|def_codename>	PSr			|atts_and_cont	|R||em_inline
<|def_codename>	PSKey			|atts_only	|R||em_inline
<|def_codename>	PSsetpagedevice		|atts_only	|R||em_inline
<|def_codename>	JobOptionsDef		|atts_only	|R||inline
@-------------------------------------------------------------------
<|def_codename>	MkAlert			|atts_and_cont	|R||inline
<|def_codename>	MkDest			|atts_only	|R||inline
<|def_codename>	NOhy			|standalone	|R||inline
<|def_codename>	NextTextFrameDef	|atts_only	|R||fdefs
<|def_codename>	currentPageNumber	|atts_only	|R||fdefs
<|def_codename>	currentPage		|atts_only	|R||fdefs
<|def_codename>	lastPageNumber		|atts_only	|R||fdefs
<|def_codename>	lastPage		|atts_only	|R||fdefs
<|def_codename>	P			|atts_and_cont	|R||inline
<|def_codename>	PDFinfo			|atts_and_cont	|R||fdefs
<|def_codename>	PDFFileImport		|atts_only	|R||inline
<|def_codename>	FileImport		|atts_only	|R||inline
<|def_codename>	FileImportDef		|atts_only	|R||fdefs
<|def_codename>	PageDef			|atts_and_cont	|R||fdefs
<|def_codename>	PageNum			|atts_only	|R||inline
<|def_codename>	PageNumFmtDef		|atts_and_cont	|R||fdefs
<|def_codename>	PageNumDef		|atts_and_cont	|R||fdefs
<|def_codename>	PageNumberDef		|atts_and_cont	|R||fdefs
<|def_codename>	ParaDef			|atts_and_cont	|R||fdefs	@-- tabsets, etc
<|def_codename>	Row			|atts_and_cont	|R||inline
<|def_codename>	RuleDef			|atts_only	|R||fdefs
<|def_codename>	System			|pi       	|X||inline
<|def_codename>	TabStops		|atts_and_cont	|R||inline
<|def_codename>	TabStop			|atts_only	|R||inline
<|def_codename>	Tab			|atts_only	|R
<|def_codename>	Tbl			|atts_and_cont	|R||inline
<|def_codename>	TblFrame		|atts_and_cont	|R||inline
<|def_codename>	TblTextFrame		|atts_and_cont	|R||inline
<|def_codename>	TblCont			|standalone	|R||inline
<|def_codename>	TblContinuationText	|atts_only	|R
<|def_codename>	TblContinuation		|atts_only	|R
<|def_codename>	TblContFmtDef		|cont_only	|R||fdefs
<|def_codename>	TblContinuationDef	|cont_only	|R||fdefs
<|def_codename>	TblDef			|atts_and_cont	|R||fdefs
<|def_codename>	TblSheet		|standalone     |R||inline
<|def_codename>	TblSheetFmtDef		|cont_only	|R||fdefs
<|def_codename>	TblSheetDef		|cont_only	|R||fdefs
<|def_codename>	TblTitle		|atts_and_cont	|R||inline
<|def_codename>	Template		|atts_only	|R||inline
<|def_codename>	TextFlow		|atts_and_cont	|R||inline
<|def_codename>	TextFrameDef		|atts_and_cont	|R
<|def_codename>	Text			|atts_and_cont	|R||inline
<|def_codename>	Units			|atts_only	|R||fdefs
<|def_codename>	Var			|atts_only	|R||inline
<|def_codename>	VarDef			|atts_and_cont	|R||inline
<|def_codename>	ImageDef		|atts_only	|R||inline
<|def_codename>	paraFrame		|atts_only	|R||inline
<|def_codename>	rowShading		|atts_only	|R||inline
<|def_codename>	XRef			|atts_only	|R||inline
<|def_codename>	XRefFmtDef		|atts_and_cont	|R||fdefs
<|def_codename>	XRefDef			|atts_and_cont	|R||fdefs
<|def_codename>	XRefDest		|atts_only	|R
<|def_codename>	TblColDef		|atts_and_cont	|R||fdefs
<|def_codename>	TblColumnDef		|atts_and_cont	|R||fdefs
<|def_codename>	TblColWidth		|atts_only	|R||inline
<|def_codename>	TblColumn		|atts_only	|R||inline
<|def_codename>	TblColumnWidth		|atts_only	|R||inline
<|def_codename>	TblColParaDef		|atts_only	|R||fdefs
<|def_codename>	TblColFormat		|atts_only	|R||inline
<|def_codename>	TblColumnFormat		|atts_only	|R||inline
<|def_codename>	Unconditional		|standalone	|R||inline
@- <|def_codename>	xml			|pi		|X
<|def_codename>	hb			|cont_only	|R
<|def_codename>	u			|cont_only	|R
@-----------------------------------------------------------------
@- Building blocks for use in special Ref page text flows
<|def_codename> autorange		|standalone	|R
<|def_codename> numerics		|standalone	|R
<|def_codename> alphabetics		|standalone	|R
<|def_codename> symbols			|standalone	|R
<|def_codename> kana			|standalone	|R
<|def_codename> pagenum			|standalone	|R
<|def_codename> PageNumber		|standalone	|R
<|def_codename> pageNumber		|atts_only	|R
<|def_codename> volnum			|standalone	|R
<|def_codename> chapnum			|standalone	|R
<|def_codename> chapterNumber		|atts_only	|R
<|def_codename> paratext		|standalone	|R
<|def_codename> paragraphText		|standalone	|R
<|def_codename> paranum			|standalone	|R
<|def_codename> paralabel		|standalone	|R
<|def_codename> paragraphLabel		|standalone	|R
<|def_codename> paranumonly		|standalone	|R
<|def_codename> paragraphNumber		|standalone	|R
<|def_codename> paratag			|standalone	|R
<|def_codename> relfilename		|standalone	|R
<|def_codename> ObjectType		|standalone	|R
<|def_codename> ObjectId		|standalone	|R
@-----------------------------------------------------------------
<|def_codename> second			|standalone	|R
<|def_codename> second00		|standalone	|R
<|def_codename> minute			|standalone	|R
<|def_codename> minute00		|standalone	|R
<|def_codename> hour			|standalone	|R
<|def_codename> hour01			|standalone	|R
<|def_codename> hour24			|standalone	|R
<|def_codename> ampm			|standalone	|R
<|def_codename> AMPM			|standalone	|R
<|def_codename> dayNumber		|standalone	|R
<|def_codename> dayNumber01		|standalone	|R
<|def_codename> dayName			|standalone	|R
<|def_codename> shortDayName		|standalone	|R
<|def_codename> monthNumber		|standalone	|R
<|def_codename> monthNumber01		|standalone	|R
<|def_codename> monthName		|standalone	|R
<|def_codename> shortMonthName		|standalone	|R
<|def_codename> year			|standalone	|R
<|def_codename> shortYear		|standalone	|R
@-----------------------------------------------------------------
@- <|find_codenum>	zy
<@macro> xxxxyy {
	@if(<#OS> = nt) {
		@- <@write> -n ${mmtmp}/xcnames.${PID}s {<#xstrings>}
		<@write> -n ${mmtmp}/xcnames {<#xstrings>}
		@- <@system> { sort -r /tmp/xcnames.${PID}s | uniq > /tmp/xcnames.${PID}; rm /tmp/xcnames.${PID}s }
		@- <@system> { cat %mmtmp%/xcnames | sort /reverse }
		<@system> { powershell -file %manbin%/sort-xnames.ps1 }
	}
}
<|xxxxyy>
@-----------------------------------------------------------------
