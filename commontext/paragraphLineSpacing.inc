@------------------------------------------------------------
<!-- file = paragraphLineSpacing.inc (start)  -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Line spacing"
	longTitle="Line spacing"
	dest="cell.optgroup.lineSpacing"
	></propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="spaceAbove"
	value="dim"
	>

<pdp>
Space above paragraph.
<vidx id="paragraph||space above (spaceAbove)" />
<vidx id="space||above paragraph (spaceAbove)" />
</pdp>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space below
the preceding paragraph, not the sum of the two.
See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
</pdp>

<pdp>
<pn>spaceAbove</pn> is ignored when a paragraph is associated
with (immediately follows) a sidehead paragraph.
See Note <xparanumonly
	id="note.sidehead.1" > on page <xnpage
	id="note.sidehead.1" >.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="deltaSpaceAbove"
	value="dim"
	elementExclude="FootnoteDef"
	>

<pdp>
Maximum <fI>additional</fI> space above paragraph allowable
when feathering is enabled for a document section.
<vidx id="paragraph||space above (spaceAbove)" />
<vidx id="space||above paragraph (spaceAbove)" />
</pdp>

<pdp>
See the description of the <pn>feather</pn> property for
the <#xSection> and <#xSectionDef> elements
(on pages <xnpage
	id="Section.property.feather" /> and <xnpage
	id="SectionDef.property.feather" />).
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="spaceBelow"
	value="dim"
	>

<pdp>
Space below.
<vidx id="paragraph||space below (spaceBelow)" />
<vidx id="space||below paragraph (spaceBelow)" />
</pdp>

<pdp>
<pn>spaceBelow</pn> is ignored when a paragraph
is a sidehead.
See Note <xparanumonly
	id="note.sidehead.2" > on page <xnpage
	id="note.sidehead.2" >.
</pdp>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space below
the preceding paragraph, not the sum of the two.
See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
</pdp>
</property>

@------------------------------------------------------------
<property
	name="maximumSpaceBelow"
	value="N | Y"
	>

<pdp>
Setting the <pn>maximumSpaceBelow</pn> property to <pv>Y</pv> ensures
the vertical spacing is as specified by
the <pn>spaceBelow</pn> property regardless of
the <pn>spaceAbove</pn> value of the next object.
<vidx id="paragraph||space below (spaceBelow)" />
<vidx id="space||below paragraph (spaceBelow)" />
</pdp>

<pdp>
The <pn>maximumSpaceBelow</pn> property is ignored in the case
the next object is another instance of the same paragraph format.
</pdp>

</property>


@------------------------------------------------------------
<property short="un" long="pgfUseNext" value="N | Y" 
	condition="fmcGuide"
	>

<pdp>
Use next paragraph.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault v="N" />
</property>

@------------------------------------------------------------
<property short="np" long="pgfNext" value="name" 
	condition="fmcGuide"
	>

<pdp>
Next paragraph tag.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault>Null</propertyDefault>
</property>

<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Interline leading and line spacing mode"
	longTitle="Interline leading and line spacing mode"
	dest="MkDest.optgroup.line_spacing"
/>
@------------------------------------------------------------
<@skipEnd>


@------------------------------------------------------------
<property short="leading" long="leading" value="dim" 
	>

<pdp>
Leading. Default units are points.
<vidx id="paragraph||line spacing (leading)" />
<vidx id="leading, space between lines" />
<vidx id="line spacing" />
</pdp>

<pdp>
Leading is the space <#fI>between<#fnI> lines
within a paragraph. Line spacing
(<#fI>baseline<#fnI> to <#fI>baseline<#fnI>)
is leading plus point size of the text.
@- (see Figure
@- <xparanumonly id="fig.inter_paragraph_spacing" file="textpos" />
@-  on page
@- <xnpage id="fig.inter_paragraph_spacing" file="textpos" />
@-  in the <#UGuide>).
</pdp>

</property>

@------------------------------------------------------------
<property
	long="lineSpacingMode"
	value="fixed | proportional"
	>

<pdp>
Line spacing mode.
<vidx id="paragraph||line spacing mode (fixed)" />
<vidx id="paragraph||line spacing mode (proportional)" />
<vidx id="line spacing in paragraph text||leading" />
<vidx id="line spacing in paragraph text||floating" />
<vidx id="line spacing in paragraph text||proportional" />
<vidx id="line spacing in paragraph text||fixed" />
<vidx id="anchored frames||inline" />
</pdp>

<pdp>
Setting <on>lineSpacingMode</on> to <ov>fixed</ov> results in
no adjustment for extra large characters, superscripts,
inline anchored frames, etc.
When <on>lineSpacingMode</on> is set to <ov>proportional</ov> the
inter-line spacing is adjusted to prevent
contact between text lines in cases where
the text contains either larger than normal
characters or inline anchored frames.
</pdp>
</property>

@------------------------------------------------------------
<!-- file = paragraphLineSpacing.inc (end)  -->
@------------------------------------------------------------
