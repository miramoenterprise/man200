@------------------------------------------------------------
@-  allowLineBreaks.inc
@-  
@------------------------------------------------------------
<!-- FILE: ${ctext}/allowLineBreaks.inc 	START start -->
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="lineBreakBefore"
	value="chars"
	>
<pdp>
Add additional characters that allow a line break before.
The additional characters may be included as a space-separated list
of UTF-8 characters, or as numeric character entities (e.g. &amp;#x<fI>hhhh</fI>;).
</pdp>

<pdp>
By default line breaking is based on
<URL url="http://unicode.org/reports/tr14/">UAX #14: Unicode Line Breaking
Algorithm</URL>. 
</pdp>

</property>


@------------------------------------------------------------
<property
	long="lineBreakAfter"
	value="chars"
	>
<pdp>
Add additional characters that allow a line break after.
Otherwise same as <pn>lineBreakBefore</pn>.
</pdp>

</property>

@------------------------------------------------------------
<!-- FILE: ${ctext}/allowLineBreaks.inc 	END end -->
@------------------------------------------------------------
