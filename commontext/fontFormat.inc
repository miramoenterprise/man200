@------------------------------------------------------------
@- fontFormat.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> fontFormatdefs {
	<#def> shortFormatName	{fmt}
	@if(@match({<#elementName>}, {HyperCmd})) {
		<#def> shortFormatName	{F}
		}
}
@------------------------------------------------------------
<|fontFormatdefs>
@------------------------------------------------------------
<@xmacro> fontFormatXRef {
	@if({<#documentName>} = {mmComposerReferenceGuide.pdf} ) {
(see pages
<xnpage
	id="FontDef.element.start"
	@- file="fdefs"
		/><xphyphen/><xnpage
	id="FontDef.element.end"
	@- file="fdefs"
		/>).
		}
	@else {(see the <#xFontDef> element
in the <HyperCmd file="mmComposerReferenceGuide.pdf"
	jumptodest="FontDef.element.start"
	fontAngle="Italic"
	>mmComposer Reference Guide</HyperCmd>).
		}
}{}
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Reference pre-defined font format"
	longTitle="Reference pre-defined font format"
	dest="<#elementName>.propertyGroup.reference_font_format"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fontDef"
	long="fontDef"
	value="name"
	>

<pdp>
Font format name.
<vidx id="paragraph||font format name" />
<vidx id="font format" />
</pdp>




<pdp>
<#name> is the name of a font format
defined either in a template document
or by using the <#xFontDef> element
@- (see pages
@- <xnpage
@- 	id="FontDef.element.start"
@- 	file="fdefs" /><xphyphen/><xnpage
@- 	id="FontDef.element.end"
@- 	file="fdefs" />).
<fontFormatXRef/>
</pdp>

<pdp element="P|TextLine|ALine" >
See the description of <on><#propertyName></on> in the <#xFont> element.
</pdp>


<pdp element="Font" >
The following:
</pdp>

<exampleBlock
	element="Font"	@--- Include example only if element is 'Font'
>
This is <Font fontDef="F_Bold" >warning</Font> text ...
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>


<pdp element="Font" >
The above example assumes a pre-existing <#osq>F_Bold<#csq> font format
definition.
</pdp>

<pdp element="Font" >
To revert to the default paragraph font format set
the <on>fontDef</on> property to the empty string,
i.e. &lt;Font fontDef="" > (no spaces between the ""). For example<vidx
	id="default paragraph font, reverting to" />
</pdp>

<exampleBlock
	element="Font"
	tabs="5|10|15|20"
>
<Font textSize="10pt">10 point <Font textColor="Red">Red text</Font>
</Font>then back into the default paragraph font.
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>



</property>
@------------------------------------------------------------

