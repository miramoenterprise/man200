@------------------------------------------------------------
@-- svgDef.inc
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="svgDef"
	value="none | name"
	element="AFrame|Frame|ATextFrame|Cell|PageDef|Row|Tbl|TblDef"
	changeBar="Y"
	>

<pdp>
Apply a SVG background as defined by an <#xxSvgDef> element. See pages <xnpage
	id="xSvgDef.element.start" /><xphyphen/><xnpage
	id="xSvgDef.element.end" />.
</pdp>

<pdp
	element="Tbl|TblDef"
	>
The SVG background is applied to the entire table, excluding the table title.
To apply a SVG background to a table header, body or footer section individually,
see the <pn>headerRowSvgDef</pn>, <pn>bodyRowSvgDef</pn> and
 <pn>footerRowSvgDef</pn> properties on pages <xnpage
	id="TblDef.property.headerRowSvgDef" /><xphyphen/><xnpage
	id="TblDef.property.footerRowSvgDef" />).
</pdp>


<propertyDefault>none</propertyDefault>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="svgDef"
	value="none | name"
	element="Image"
	changeBar="Y"
	>

<pdp>
Apply SVG code defined by an <#xxSvgDef> element as an
overlay on top of the image. See the <#xxSvgDef> element on
pages <xnpage
	id="xSvgDef.element.start" /><xphyphen/><xnpage
	id="xSvgDef.element.end" />.
</pdp>

<propertyDefault>none</propertyDefault>

</property>
@------------------------------------------------------------

