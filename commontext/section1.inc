@------------------------------------------------------------
@-  section1.inc
@------------------------------------------------------------
@- 
@-	pageCount
@-  
@-  Included in:
@-  
@-  	DocDef
@-  	Section
@-  	SectionDef
@-  
@------------------------------------------------------------
<!-- FILE: section1.inc START start -->
@------------------------------------------------------------
<@xmacro> section1 {
	<#def> mpr2.defaultOdd		{}
	<#def> mpr2.defaultEven		{}
	<#def> mpr2.new			{new }
	@if(<#elementName> = {DocDef}) {
		<#def> mpr2.new			{}
		<#def> mpr2.defaultOdd		{}
		<#def> mpr2.defaultEven		{}
		<#def> mpr2.defaultAllPages	{}
		<#def> mpr2.defaultBlank	{}
		}
}{}
@------------------------------------------------------------
<section1/>
@------------------------------------------------------------
<@xmacro> sectionDefault {
	<#def> mpd.value		{}
	@----------------------------------------------------
	@xgetvals(mpd.)
	@----------------------------------------------------
	<#property.long>
	@ifnot(<#elementName> = {DocDef}) {
		<propertyDefault>Inherited from <#xDocDef> (see <pv><#property.long></pv> on page <xnpage
			id="DocDef.property.<#property.long>" />)</propertyDefault>
		}
	@else {
		<propertyDefault><#mpd.value></propertyDefault>
		}
}{}
@------------------------------------------------------------


@------------------------------------------------------------
<property short="pageCount" long="pageCount"
	value="any | even | odd"
	>
<pdp>
If set to <pv>even</pv> or <pv>odd</pv> force the number of
pages in the document section to an even or odd number.
</pdp>
<sectionDefault value="any" />
</property>

@------------------------------------------------------------
<@include> ${ctext}/fontHyphenation.inc
@------------------------------------------------------------
<@include> ${ctext}/language.inc
@------------------------------------------------------------

@------------------------------------------------------------
<!-- FILE: section1.inc END end -->
@------------------------------------------------------------
