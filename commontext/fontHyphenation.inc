@------------------------------------------------------------
@-- fontHyphenation.inc
@------------------------------------------------------------


<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Line breaking"
	longTitle="Line breaking"
	dest="p.optgroup.lineBreaking"
	/>
@------------------------------------------------------------
<@skipEnd>

@------------------------------------------------------------
<property
	long="lineBreak"
	value="Y | N"
	element="Font|FontDef"
	>
<pdp>
Suppress line breaks within a word or phrase.
</pdp>

<pdp>
If the <pn>lineBreak</pn> property is set to <pv>N</pv>, all other
hyphenation and line breaking properties are ineffective.
</pdp>

</property>


@------------------------------------------------------------
<property
	short="hyphenate"
	long="hyphenate"
	value="Y | N"
	>

<pdp>
Turn on hyphenation.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="maximumHyphens"
	value="int"
	>

<pdp>
Maximum number of adjacent
hyphenated lines.
<vidx id="paragraph||hyphenation in" />
<vidx id="text||hyphenating" />
<vidx id="hyphenation" />
</pdp>

</property>

@------------------------------------------------------------
<property
	long="minimumBeforeHyphen"
	value="int"
	longStretch=96
	>

<pdp>
Minimum number of characters
before hyphen.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="minimumAfterHyphen"
	value="int"
	longStretch=97
	>

<pdp>
Minimum number of characters
after hyphen.
</pdp>

</property>

@------------------------------------------------------------
<@skipStart>
@------------------------------------------------------------
<property
	name="hyphenChar"
	@- value="default | hyphenMinus | none | tilde | maqaf | yentamna | char"
	value="default | hyphenMinus | none | tilde | yentamna | char"
	>

<pdp>
The hyphenation character for
word hyphenation at end of lines.
@- whether based on SHY or dictionary patterns
</pdp>

<propertyValueKeyList
        @- keyWidth="20mm"
        keyHeading="Value"
        >
	<vk key="default" >
	Align table with start of text direction
	</vk>
	<vk key="hyphenMinus" >
	Use
	</vk>
</propertyValueKeyList>



<pdp>
The defult is <sq>-</sq>, <UniChar>2d</UniChar>,
<UniName>hyphen-minus</UniName>.
</pdp>

</property>
@------------------------------------------------------------
<@skipEnd>
@------------------------------------------------------------


<@skipStart>
@------------------------------------------------------------
<property
	long="minimumWordSize"
	value="int"
	>
<pdp>
Minimum word size
to hyphenate.
</pdp>

</property>
<@skipEnd>

@------------------------------------------------------------
<property
	name="showHyphenationPoints"
	value="N | Y"
	>
<pdp>
Indicate the potential text hyphenation points.
</pdp>

</property>

@------------------------------------------------------------
