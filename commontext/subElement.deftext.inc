@------------------------------------------------------------
@-- subElement.deftext.inc
@------------------------------------------------------------
@-- This is appropriate for inclusion in the following
@-	markup code descriptions:
@-	AutoNum
@-	BGMarker
@-	HyperCmd
@-	Marker
@------------------------------------------------------------

<subElement 
	name="deftext"
	nameFormat="Italic"
	>
@- <pdp>
@- <fI>deftext</fI>
@- </pdp>

<bbp>
<fI>deftext</fI> consists of text and newline characters.
All binary newlines and tabs are ignored.
@- are mapped to
@- space characters. Binary tabs
@- are preserved unless the <#osq><#nbhy>striptabs<#csq>
@- command line option is used.
</bbp>

<bbp>
Special characters
may be encoded using
the character entity name string
(&amp;<fI>name</fI>;), or the 
numeric character reference (&amp;#<fI>nn</fI>;
or &amp;#x<fI>hhhh</fI>;).
If the requested character is not
available in the current font, it is
ignored with a warning message.
</bbp>
</subElement>

@------------------------------------------------------------
@- <br/> 
@------------------------------------------------------------
<@include> ${CTEXT}/subElement.br.inc
@------------------------------------------------------------
@- <tab/> 
@------------------------------------------------------------
@- <@include> ${CTEXT}/subElement.tab.inc
@------------------------------------------------------------
