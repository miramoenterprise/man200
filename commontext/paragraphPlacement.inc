@------------------------------------------------------------
<!-- file = paragraphPlacement.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Paragraph placement"
	longTitle="Paragraph placement"
	XRefID="<#CodeName1>.optgroup.paragraph_placement.start"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	@- short="P"
	long="position"
	value="normal | topOfColumn | topOfPage | topOfEvenPage | topOfOddPage"
       	>


<pdp>
Paragraph start position. The <on>position</on> property is effective
only if the <#xP> element is within the body text flow.
</pdp>

<propertyValueKeyList
	keyWidth="20mm"
	keyHeading="Value"
	>
<vk key="normal" >
Start paragraph immediately after preceding paragraph.
@--- can be 'anywhere'. Maybe better!
</vk>
<vk key="topOfColumn" >
Start paragraph at the top of the next column.
</vk>
<vk key="topOfPage" >
Start paragraph at the top of the next page.
</vk>
<vk key="topOfEvenPage" >
Start paragraph at the top of the next even page.
</vk>
<vk key="topOfOddPage" >
Start paragraph at the top of the next odd page.
</vk>
</propertyValueKeyList>


<pdp>
The <on>position</on> property has no effect if the paragraph will
in any case start in the requested position. For example,
if the <on>position</on> property is set to <ov>topOfPage</ov> on
the first paragraph in a
document then that paragraph will be output at the
beginning of the first page. No preceding blank first page will
be produced. (To get such a blank page include
two paragraphs with the <on>position</on> property set
to <ov>topOfPage</ov>, the first paragraph being empty.)
</pdp>


<pdp
	condition="fmcGuide"
	>
To apply a non-default
master page, that is neither a <#osq>Left<#csq> or <#osq>Right<#csq> master page,
nor the first body page in a template file, to the first
page in a document then the <on>P</on> property must be set to <ov>P</ov>
and the first paragraph in the document must have a <#xP>
element with an associated <#xMasterPageRule>
element with its <on>Page</on> property set to the name of the
desired master page (see pages
<xnpage id="MasterPageRule.element.start" file="inline" />
<xphyphen/>
<xnpage id="MasterPageRule.element.end" file="inline" />
). This will not create an empty first page.
</pdp>


<pdp
	condition="fmcGuide"
	>
<on>position</on> property settings of <ov>normal</ov> and <ov>topOfColumn</ov> are
ignored if the <on>TCN</on> (see below) property is set on the paragraph.
</pdp>

<propertyDefault v="normal" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="TCN"
	long="nextColNum"
	value="int | int(warn)"
	condition="fmcGuide"
	>

<pdp>
Start paragraph at the beginning of the <#fI>n<#fnI>th
auto-connected text column (see Note
<xparanumonly id="note.P.TCN" file="inline" />
 on page 
<xnpage id="note.P.TCN" file="inline" />
) on the current page. <#fI>n<#fnI> may be any
integer greater than 0.
<vidx id="paragraph||top of column (TCN=int)" />
</pdp>

<pdp>
In the second form, int(warn), a warning message
is produced if the paragraph cannot be placed as
requested (see below). The string <#osq>(warn)<#csq> must be
typed exactly as shown.
</pdp>

@- <xOptPara/>
<pdp>
If the paragraph with <on><#optRef></on> set to <#fI>n<#fnI> occurs in a text
column whose sequence number on the page is less than
<#fI>n<#fnI> then that paragraph is moved to the top 
of text column <#fI>n<#fnI>.
</pdp>

<pdp>
If the paragraph with <on><#optRef></on> set to <#fI>n<#fnI> occurs in a text
column whose sequence number on the page is equal to
or greater than <#fI>n<#fnI>, or the value of <#fI>n<#fnI> is
greater than the total number of auto-connected text
columns on the page, no action is taken except
in the case the <on><#optRef></on> setting is followed by the warn keystring
in parenthesis, as for example in,
</pdp>

<pdp>
TCN="3(warn)"
</pdp>

<pdp>
when, unless the paragraph is the first paragraph
in text column <#fI>n<#fnI> (i.e. 3 in the above example)
a warning message will be written to standard error.
</pdp>

<pdp>
The <on><#optRef></on> property cannot be used in a multi-columned
text frame containing objects which straddle
two or more columns
(see Note
<xparanumonly id="note.P.TCN" file="inline" />
 on page 
<xnpage id="note.P.TCN" file="inline" />
).
</pdp>


<pdp>
<on>P</on> property settings of <ov>C</ov> and <ov>A</ov> are ignored when used
in conjunction with the <on><#optRef></on> property. 
<on>P</on> property settings of <ov>P</ov>, <ov>L</ov> and <ov>R</ov>, if
present, take effect <#fI>after<#fnI> the <on><#optRef></on> property
takes effect. The <on>P</on> property is described
on page 
<xnpage id="<#elementName>.property.P" />
.
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Run-in, in-column, side head or straddle"
	longTitle="Run-in, in-column, side head or straddle"
	dest="<#elementName>.optgroup.alignFormat.start"
	condition="fmcGuide"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="Pf"
	long="alignFormat"
	value="N | R | T | F | L | S | SN"
	condition="fmcGuide"
	>

<pdp>
Paragraph placement format.
N<#nbsp>=<#nbsp>normal, R<#nbsp>=<#nbsp>run-in, T<#nbsp>=<#nbsp>side head top, F<#nbsp>=<#nbsp>side head first,
L<#nbsp>=<#nbsp>side head last, S<#nbsp>=<#nbsp>straddle columns
and side heads, SN<#nbsp>=<#nbsp>straddle columns only.
<vidx id="paragraph||run-in (Pf=R)" />
<vidx id="paragraph||side heads (Pf=T,F,L)" />
<vidx id="paragraph||straddle (Pf=S)" />
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="rhp"
	long="endRunInChar"
	value="char"
	condition="fmcGuide"
	>

<pdp>
The default punctuation for ending
a run-in head.<br/>
The default is <#osq>.<#csq> (period). To
have nothing, use the empty string,
ie, <on>rhp</on>=""<#csq>.
</pdp>

</property>


@------------------------------------------------------------
<property
	long="minimumLines"
	value="int"
	>

<pdp>
Minimum number of widow/orphan lines.
<vidx id="paragraph||widow lines in" />
<vidx id="paragraph||orphan lines in" />
<vidx id="orphans" />
<vidx id="widows" />
</pdp>

</property>

@------------------------------------------------------------
<property
	@- short="wn"
	long="withNext"
	short="withNext"
	value="none | all | namelist"
	>

<pdp>
Keep with next paragraph setting.
<vidx id="paragraph||keeping with next" />
</pdp>

<propertyValueKeyList
	keyWidth="20mm"
	keyHeading="Value"
	>
<vk key="none" >
Do not force keeping this paragraph with any following paragraph.
</vk>
<vk key="all" >
Keep this paragraph with the following paragraph.
</vk>
<vk key="namelist" italic="Y" >
If this paragraph is followed by any paragraph in <fI>namelist</fI>,
then keep this paragraph on the same page as that paragraph.
</vk>
</propertyValueKeyList>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="withPrevious"
	value="N | Y"
	>

<pdp>
Keep with previous paragraph.
<vidx id="paragraph||keeping with previous" />
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="keepTogether"
	value="N | Y"
	>

<pdp>
Keep paragraph, and nested objects if any, together as a single
block with no page breaking within.
<vidx id="paragraph||keep together" />
</pdp>
<pdp>
If the paragraph block height, including nested objects, is greater
than the height of the containing text frame, the text will overflow
the flow area and even the page area. In this case a warning message is
issued.
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<!-- file = paragraphPlacement.inc (end) -->
@------------------------------------------------------------

