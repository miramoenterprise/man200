@------------------------------------------------------------
<!-- File =  paragraphSideheads.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
@- In 'Text features' property group
@------------------------------------------------------------

@-===========================================================
<propertyGroup
	title="Sidehead properties"
	dest="<#elementName>.propertyGroup.sideheads"
	indexOnly="0"
>
A sidehead is a text block outside the main flow.
All sidehead properties are ignored unless
the <pn>sidehead</pn> property is set to <pv>Y</pv> <fI>and</fI> the <#xP> element
is at the top level.
See Notes <xparanumonly
	id="note.sidehead.3" /> and <xparanumonly
	id="note.sidehead.4" /> on
page <xnpage id="note.sidehead.3" />.
</propertyGroup>


@------------------------------------------------------------
<property
	name="sidehead"
	value="N | Y"
	>


<pdp>
Set to <pv>Y</pv> to position paragraph as a sidehead.
Setting <pn>sidehead</pn> to <pv>Y</pv> has no effect if the <#xP> element
is not at the top level.
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="sideheadWidth"
	value="dim"
	>
<pdp>
Width of sidehead.
</pdp>


<propertyDefault>90pt</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="sideheadGap"
	value="dim"
	>
<pdp>
Gap between the sidehead area and the main text flow.
</pdp>


<propertyDefault>10pt</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="sideheadAlign"
	value="start | end | inside | outside"
	>
<pdp>
Place sidehead outside the text frame at the side
corresponding to the <pv>start</pv> or <pv>end</pv> of
the horizontal text direction.
If the value of <pn>sideheadAlign</pn> is set to <pv>inside</pv> the
sidehead is located outside the start of the horizontal text direction on
odd pages and outside the end of the text direction on even
pages.
If the value of <pn>sideheadAlign</pn> is set to <pv>outside</pv> the
the location is the positioning is the converse of
that for <pv>inside</pv>.
</pdp>


<propertyDefault>10pt</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="sideheadVerticalAlign"
	value="firstBaselines | textTops | firstCapHeights | firstxHeights | tops"
	>
<pdp>
Reference metric for alignment between the sidehead paragraph and its
associated paragraph (following paragraph, if it's not a sidehead) or table.
</pdp>

<propertyValueKeyList
	keyWidth="17mm"
	>
<vk key="firstBaselines" >
Align with the first baselines.
</vk>

<vk key="textTops" >
Align with the text tops. typo Ascenders.
</vk>

<vk key="firstCapHeights"  span="Y" >
Align with the tops of uppercase letters. typo Ascenders.
</vk>

<vk key="firstxHeights" >
Align with the tops of lowercase letters. typo Ascenders.
</vk>

<vk key="tops" >
If either or both the sidehead paragraph or its associated paragraph
has a <#xparaFrame> sub-element, then align with the top edge of the
<#xparaFrame> <pn>topMargin</pn> area or, if the <#xparaFrame> also has
a <pn>topRule</pn>, the top of the top ruling.
If the paragraphs have no <#xparaFrame> sub-elements, then <pv>tops</pv> is
equivalent to <pv>textTops</pv> above.
</vk>
<vk>
If the sidehead paragraph is immediately followed by a table,
the top of the table, or the table title if present at the top
of the table, is is adjusted to align with the top of the sidehead.
See Note <xparanumonly
	id="note.sidehead.5" /> on page <xnpage
	id="note.sidehead.5" />.
</vk>

</propertyValueKeyList>



<propertyDefault>firstBaselines</propertyDefault>
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<!-- File =  paragraphSideheads.inc (end) -->
@------------------------------------------------------------
