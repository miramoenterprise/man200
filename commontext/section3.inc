@------------------------------------------------------------
@-  section3.inc
@- 
@-  	firstPage
@-  
@-  Included in:
@-  
@-  	Section
@-  	SectionDef
@-  
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="First page"
	dest="<#elementName>.propertyGroup.firstPage"
	indexOnly="0"
>
Specify the master page to be used for the first page in the section.

See Note
<xparanumonly
        id="note.SectionDef.destination_page" /> on page
<xnpage
        id="note.SectionDef.destination_page" /> for the
definition of <sq>destination page</sq>.


<br/><br/>
The rules for resolving conflicts when more than one property
from the following <Font uline="S"><xparatextonly
id="<#elementName>.propertyGroup.firstPage" /></Font> is
used are described in Note
<xparanumonly
        id="note.SectionDef.conflicts" /> on page
<xnpage
        id="note.SectionDef.conflicts" />.


</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="firstPage"
	value="name" >
<pdp>
Apply master page <fI>name</fI> to 
the first page in the section.
</pdp>

</property>

@------------------------------------------------------------
