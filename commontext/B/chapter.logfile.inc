@------------------------------------------------
<xOptGroup
	shortTitle="Log file option"
	longTitle="Log file option"
	XRefID="<#elementName>.propertyGroup.log_file_option"
/>
@------------------------------------------------
@--- FIXED

@------------------------------------------------
<xOption short="logfile" long="logFilename" 
	four="Doc|inline"
	value="filename" >

Write warning and error messages produced
while processing this <#CodeName1>
in file <#fI>filename<#fnI>
rather than to standard error. If the file
<#fI>filename<#fnI> exists it is overwritten; if <#fI>filename<#fnI> does
not exist it is created.
</xOption>
@------------------------------------------------
