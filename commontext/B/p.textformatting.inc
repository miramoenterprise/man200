@---------------------------------------------
@-- p.textformatting.inc
@---------------------------------------------


@-----------------------------
<propertyGroup
	shortTitle="Character and inter-word spacing"
	longTitle="Character and inter-word spacing"
	XRefID="<#elementName>.propertyGroup.inter_word_spacing"
	@- condition="fmcGuide"
	>
</propertyGroup>
@- (For examples showing the effect of the
@- <on>gws</on>,
@- <on>ows</on>,
@- and
@- <on>lws</on> options below see the
@- <#fI>Miramo User Guide<#fnI>, Chapter
@- <xparanumonly id="textpos.chapter.start" file="textpos" />
@- , pages
@- <xnpage id="textpos_inter_word_spacing.start" file="textpos" />
@- <xphyphen/>
@- <xnpage id="textpos_inter_word_spacing.end" file="textpos" />
@- .)
@- </propertyGroup>

<docNote>
Check these for MMC.<br/>
Change property names
</docNote>

<property
	short="cspace"
	long="letterSpacing"
	value="N | Y"
	@- condition="fmcGuide"
	>
<pdp>
Allow additional letter spacing.
<xidxstart id="paragraph||word spacing" />
<xidxstart id="word spacing" />
<vidx id="tracking, in text" />
<vidx id="character spacing" />
<vidx id="letter spacing" />
<vidx id="spread, character" />
</pdp>

<pdp>
Inter-word spacing in justified text
may sometimes have to exceed the maximum
specified by the <on>maximumInterWordSpace</on> property (see below).
In such cases,
setting the <on>letterSpacing</on> property to <ov>Y</ov> will allow additional space
to be added between letters within words
to reduce excessive spacing between words.
</pdp>

</property>


@-----------------------------
<property
	short=""
	long="maximumInterWordSpace"
	value="percent"
	@- condition="fmcGuide"
	>

<pdp>
Maximum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
</pdp>

</property>

@------------------------------------------------------------
<property
	short=""
	long="optimumInterWordSpace"
	value="percent"
	@- condition="fmcGuide"
	>

<pdp>
Optimum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
</pdp>

<pdp >
E.g. the following
</pdp>


<exampleBlock
        >
@- <P p="7pt" optimumInterWordSpace="600" maximumInterWordSpace="300" <hc> fi="5mm" li="5mm" </hc> >
<P p="7pt" optimumInterWordSpace="600" <hc> fi="5mm" li="5mm" </hc> >
@--- The above produces:
@--- font-size="7pt" word-spacing.minimum="0pt" word-spacing.spacing.maximum="8.75pt"
@--- word-spacing.optimum="8.75pt" ... >Like as
Like as the waves make towards the pebbl'd shore, So do our minutes hasten
</P>
<@skipStart>
<P p="7" optimumInterWordSpace="100" minimumInterWordSpace="400" <hc>fi="5mm" li="5mm"</hc> >
@--- The above produces:
@--- font-size="7pt" word-spacing.minimum="5.25cm" word-spacing.maximum="5.25cm"
@--- word-spacing.optimum="5.25cm" ... >Like as
Like as the waves make towards the pebbl'd shore, So do our minutes hasten
</P>
<P p="7pt"
	optimumInterWordSpace="0"
	maximumInterWordSpace="200"
	minimumInterWordSpace="200" <hc>fi="5mm" li="5mm"</hc> >
@--- The above produces:
@--- font-size="7pt" word-spacing.minimum="1.75pt" word-spacing.maximum="1.75pt"
@--- word-spacing.optimum="1.75pt" ... >Like as the waves mak
Like as the waves make towards the pebbl'd shore, So do our minutes hasten
</P>
<@skipEnd>
</exampleBlock>

<pdp>
produces:
</pdp>
<Row>
<Cell/>
<Cell/>
<Cell cs="R">
<#example_output>
</Cell>
</Row>

</property>

@------------------------------------------------------------
<property
	short=""
	long="minmumInterWordSpace"
	value="percent"
	@- condition="fmcGuide"
	>

<pdp>
Minimum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
<xidxend id="paragraph||word spacing" />
<xidxend id="word spacing" />
</pdp>

</property>

