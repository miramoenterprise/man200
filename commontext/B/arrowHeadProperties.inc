@------------------------------------------------------------
@-- penProperties.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> penPropertiesTextDef {
	@if({<#elementName>} = {ArrowDef}) {
		<#def> pp.text1		{Arrow}
		<#def> pp.text2		{arrow}
		<#def> pp.text3		{}
		}
	@else {
		<#def> pp.text1		{Border}
		<#def> pp.text2		{border}
		<#def> pp.text3		{The center of the pen width is aligned with the frame border.}
		}
}
@------------------------------------------------------------
<|penPropertiesTextDef>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Arrowhead size and shape properties"
	longTitle="Arrowhead size and shape properties"
	dest="<#elementName>.propertyGroup.sizeAndShape"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="arrowStyle"
	long="arrowStyle"
	value="filled | hollow | stick"
	skip="Y"
	>

<pdp>
Arrow style.
</pdp>

<propertyDefault>filled</propertyDefault>
</property>

@------------------------------------------------------------
<property short="arrowTipAngle" long="arrowTipAngle" value="angle" >

<pdp>
Angle between arrow wing and arrow shaft at the tip of the arrow.
The value of <on>arrowTipAngle</on> must be at least 10
less than the value of <on>arrowBaseAngle</on>.
</pdp>

<propertyDefault>30</propertyDefault>
</property>

@------------------------------------------------------------
<property short="arrowBaseAngle" long="arrowBaseAngle" value="angle" >

<pdp>
Angle between arrow wing and arrow shaft at the neck of the arrow.
The value of <on>arrowBaseAngle</on> must be in the range
30 to 100.
</pdp>

<propertyDefault>60</propertyDefault>
</property>

@------------------------------------------------------------
<property short="arrowWidthRatio" long="arrowWidthRatio" value="number" >

<pdp>
Total wingspan of the arrowhead divided by the thickness of
the arrow shaft, <on>penWidth</on>.
</pdp>

<propertyDefault>10</propertyDefault>
</property>

@------------------------------------------------------------
<property short="arrowPosition" long="arrowPosition"
	value="start | end | both"
	>

<pdp>
Arrow position: start or end of line or both start and end of line.
</pdp>

<propertyDefault>end</propertyDefault>
</property>
@------------------------------------------------------------




@- *** arrowBaseAngle        angle        (default 60)
@- *** arrowTipAngle        angle        (default 30)
@- *** arrowWidthRatio        number        (default 10) 


