@------------------------------------------------------------
@-- bb.tab.inc
@------------------------------------------------------------
@--- FIXED
<#def> bb.tab.specialText	{}

@------------------------------------------------------------
<@macro> bb.tab	{
	@if(@match({X-<#elementName>-Y}, {X-IX-Y})){
<#def> bb.tab.specialText	{ Effective only if
<on>ix-show</on> option is set to <ov>N</ov>.}
		}
}
@------------------------------------------------------------
<|bb.tab>
@------------------------------------------------------------

<buildingBlock
	blockName="&lt;tab/&gt;"
	>

<bbp>
Inserts tab character in definition text.
</bbp>
</buildingBlock>

@------------------------------------------------------------
