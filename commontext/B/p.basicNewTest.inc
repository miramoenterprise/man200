@---------------------------------------------
@-- p.basicNew.inc
@---------------------------------------------

@-------------------------------------
<propertyGroup
	shortTitle="Basic options"
	longTitle="Basic options"
	dest="cell.optgroup.basic"
	></propertyGroup>

@-----------------------------
<property short="A" long="textAlign" value="start | end | center | justify" >
<propertyOld short="A" long="justify" value="L | R | C | LR" />

<pdp>
Text justification.
<vidx id="paragraph||justification in" />
<vidx id="justification||in paragraphs" />
<vidx id="text alignment||paragraph" />
<vidx id="alignment||paragraph" />
<vidx id="paragraph||centering text (A=C)" />
<vidx id="paragraph||alignment" />
<vidx id="centering text||in paragraphs (A=C)" />
<ov>L</ov><#nbsp>=<#nbsp>left only,
<ov>R</ov><#nbsp>=<#nbsp>right only,
<ov>LR</ov><#nbsp>=<#nbsp>left and right,
<ov>C</ov><#nbsp>=<#nbsp>center.
</pdp>

<skip>
<propertyValueKeyList keyWidth="20mm"
	keyHeading="Value"
	descriptionHeading="Description"
>

<vk key="start" >
Align text lines at the start point.
Ragged line ends.
</vk>

<vk key="end" >
Align text lines at the end point.
Ragged line starts.
</vk>

<vk key="center" >
Align text lines at the center.
Ragged line starts and ends.
</vk>

<vk key="justify" >
Align text lines at start and end.
Text justification at both ends.
</vk>

<vkSubHead>
Compatibility values:
</vkSubHead>

<vk key="L | Left" >
Treated the same as <ov>start</ov>
</vk>

<vk key="R | Right" >
Treated the same as <ov>end</ov>
</vk>

<vk key="C | Center" >
Treated the same as <ov>center</ov>
</vk>

<vk key="C | Center" >
Treated the same as <ov>center</ov>
</vk>

</propertyValueKeyList>
</skip>

</property>

@-----------------------------
<property short="fi" long="firstIndent" value="dim" four="P|inline" >
<propertyOld short="A" long="justify" value="L | R | C | LR" />

<pdp>
First line indent.
<vidx id="paragraph||first indent (fi)" />
<vidx id="paragraph||indentation" />
</pdp>

<pdp>
Distance of
the first line of text from the left side
of a text column or from the left
margin of a table cell.
</pdp>

</property>

@-----------------------------
<property short="li" long="startIndent" value="dim" four="P|inline" >
<propertyOld short="li" long="leftIndent" value="dim" four="P|inline" />

<pdp>
Start indent (excluding the first line
of the paragraph).
<vidx id="paragraph||left indent (li)" />
</pdp>

<pdp>
Distance of the
second and succeeding lines of text from
the starting edge of a text column or from the starting
margin of a table cell.
</pdp>

</property>

@-----------------------------
<property short="ri" long="endIndent" value="dim" four="P|inline" >
<propertyOld short="ri" long="rightIndent" value="dim" four="P|inline" />

<pdp>
End indent.
<vidx id="paragraph||right indent (ri)" />
</pdp>

<pdp>
Distance of lines of text from
the end side of a text column or from the end
margin of a table cell.
</pdp>

</property>

@-----------------------------
<property short="sa" long="spaceBefore" value="dim" four="P|inline" >
<propertyOld short="sa" long="spaceAbove" value="dim" four="P|inline" />

<pdp>
Space before paragraph, usually space above.
<vidx id="paragraph||space above (sa)" />
<vidx id="space||above paragraph (sa)" />
</pdp>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space before the current paragraph and the space after
the preceding paragraph, not the sum of the two.
</pdp>

</property>

@----------------------------
<property short="sb" long="spaceAfter" value="dim" four="P|inline" >
<propertyOld short="sb" long="spaceBelow" value="dim" four="P|inline" />

<pdp>
Space below.
<vidx id="paragraph||space below (sb)" />
<vidx id="space||below paragraph (sb)" />

See note above.
</pdp>

</property>


@-----------------------------
<property short="un" long="pgfUseNext" value="N | Y" four="" >

<pdp>
Use next paragraph.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault v="N" />
</property>

@-----------------------------
<property short="np" long="pgfNext" value="name" four="" >

<pdp>
<vidx id="paragraph||next paragraph (np, un)" />
Next paragraph tag.
</pdp>

<propertyDefault>Null</propertyDefault>
</property>

@-----------------------------
<propertyGroup
	shortTitle="Interline leading and line spacing mode"
	longTitle="Interline leading and line spacing mode"
	dest="MkDest.optgroup.line_spacing"
/>
@-----------------------------


@-----------------------------
<property short="l" long="leading" value="dim" four="P|inline" >

<pdp>
Leading. Default units are points.
<vidx id="paragraph||leading (l)" />
<vidx id="paragraph||line spacing (l)" />
<vidx id="leading, space between lines" />
<vidx id="line spacing" />
</pdp>

<pdp>
Leading is the space <#fI>between<#fnI> lines
within a paragraph. Line spacing
(<#fI>baseline<#fnI> to <#fI>baseline<#fnI>)
is leading plus point size of the text
(see Figure
<xparanumonly id="fig.inter_paragraph_spacing" file="textpos" />
 on page
<xnpage id="fig.inter_paragraph_spacing" file="textpos" />
 in the <#UGuide>).
</pdp>

</property>

@-----------------------------
<property short="ls" long="lineSpacing" value="F | P" four="P|inline" >

<pdp>
Line spacing.
<vidx id="paragraph||line spacing (fixed, ls=F)" />
<vidx id="paragraph||line spacing (leading, l)" />
<vidx id="line spacing in paragraph text||leading, ls=l" />
<vidx id="line spacing in paragraph text||floating, ls=P" />
<vidx id="line spacing in paragraph text||fixed, ls=F" />
<vidx id="line spacing in paragraph text||fixed, ls=F" />
<vidx id="anchored frames||inline" />
</pdp>

<pdp>
<ov>F</ov><#nbsp>=<#nbsp>fixed
(no adjustment for extra large characters, superscripts,
inline anchored frames, etc.),
<ov>P</ov><#nbsp>=<#nbsp>proportional.
Setting the <on><#optRef></on> option to <ov>P</ov> prevents
contact between text lines in cases where
the text contains either larger than normal
characters or inline anchored frames.
</pdp>
</property>
