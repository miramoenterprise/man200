@------------------------------------------------------------
@- chapter.page_start_side.inc
@------------------------------------------------------------
<@include> ${CTEXT}/def.template.inc

@------------------------------------------------------------
<propertyGroup
	shortTitle="Page starting side"
	longTitle="Page starting side"
	dest="<#elementName>.propertyGroup.page_start_side"
	/>


@------------------------------------------------------------
<property short="StartSide" long="startSide"
	four="Chapter|inline"
	value="Left | Right | Next | File" >

<pdp>
Starting page side.
</pdp>

<propertyValueKeyList>

<vk key="Left" >Left page.</vk>
<vk key="Right" >Right page.</vk>
<vk key="Next" >Next available side.</vk>
<vk key="File" >Read from file.</vk>
</propertyValueKeyList>

<pdp>
The <on>StartSide</on> option is ignored (the starting page
side is always <#osq>Right<#csq>) unless the document is 
set to be double-sided, 
either by importing document formats from a 
double-sided template 
file or, if the
<on>type</on> option is set to <ov>STD</ov>, by setting 
the <#xDocDef> <on>pd</on> option to <ov>Y</ov> (see page
<xnpage id="DocDef.option.pd" file="fdefs" />
) or, if the
<on>type</on> option is set to <ov>PDF</ov>, by setting the
<on>pdfDS</on> option to <ov>Y</ov> (see page
<xnpage id="Chapter.option.pdfDS" />
).
</pdp>

<propertyDefault>File</propertyDefault>
</property>
@------------------------------------------------------------
