@------------------------------------------------------------
@-- bb.deftext1.inc
@------------------------------------------------------------
@-- This is appropriate for inclusion in the following
@-	markup code descriptions:
@-	AutoNum
@-	BGMarker
@-	HyperCmd
@-	Marker
@------------------------------------------------------------

<@BBlock> {
<fI>deftext</fI>
}{
<P fmt="P_propertyDesc" >
<fI>deftext</fI> consists of text, newline characters
and binary tabs. All newlines are mapped to
space characters. Binary tabs
are preserved unless the <#osq><#nbhy>striptabs<#csq>
command line option is used.
</P>
<P fmt="P_propertyDesc" >
Special characters
may be encoded using
the character entity name string
(&amp;<fI>name</fI>;), or the 
numeric character reference (&amp;#<fI>nn</fI>;
or &amp;#x<fI>hhhh</fI>;).
However if the requested character is not
available in the current font, it will
be ignored with a warning message.
</P>
}

@------------------------------------------------------------
@- <br/> 
@------------------------------------------------------------
@- temp exclude:
@--- <@include> ${CTEXT}/bb.br.inc
@------------------------------------------------------------
@- <tab/> 
@------------------------------------------------------------
@- temp exclude:
@--- <@include> ${CTEXT}/bb.tab.inc

@- NO FontRef allowed

