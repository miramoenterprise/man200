@------------------------------------------------------------
<!-- File = font.inc (start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> textSizeValue {
	<#def> textSizeValue	{dim}
	@if(<#elementName> = {Font}) {
		<#def> textSizeValue	{dim | rdim}
		}
}{}
@------------------------------------------------------------
<textSizeValue/>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Font selection"
	longTitle="Font selection"
	dest="<#elementName>.propertyGroup.font_selection"
>
See <xchapter id="fonts" /> for
details about supported font types and font selection.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="ff" long="fontFamily" value="name" >

<pdp>
Font family. E.g. <ov>Arial</ov>, <ov>Times New Roman</ov>.
</pdp>


</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="fa" long="fontAngle" value="name" >

<pdp>
Font angle. E.g. <ov>Italic</ov>, <ov>Oblique</ov>.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" >
Some <Font fa="Italic" >italic</Font> text ...
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------



@------------------------------------------------------------
<property short="fw" long="fontWeight" value="name" rs="2">

<pdp>
Font weight.  E.g. <ov>Bold</ov>, <ov>Medium</ov>.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<exampleBlock element="Font" >
Some <Font fw="Bold" >bold</Font> text ...
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Text size and color"
	longTitle="Text size and color"
	dest="<#elementName>.propertyGroup.text_size_and_color.start"
/>


@------------------------------------------------------------
<property
	short="p"
	long="textSize"
	@- value="dim | rdim"
	value="<#textSizeValue>"
	>

<pdp>
Text size.
@- See Note
@- <xparanumonly id="note.Font.textsize" file="inline" />
@- on page
@- <xnpage id="note.Font.textsize" file="inline" />.
</pdp>

<pdp>
Default units are points (1/72 inch).
</pdp>



<pdp element="Font" >
<fI>rdim</fI> values are applicable only when
the <on>textSize</on> property is included directly
within a <#xFont> element, i.e. not within <#xP> or
other elements that support <#xFont> properties.
An <fI>rdim</fI> value may
have any of the following forms: +20%, 120%, 85%,
-4%, +<fI>dim</fI> and -<fI>dim</fI> (e.g. +2pt, -1mm). 
</pdp>

@------------------------------------------------------------
@- Macro <relativeTextPointSize/> is defined in:
@- ${CTEXT}/mmp.textdefs
@------------------------------------------------------------
@- <relativeTextPointSize/>
@------------------------------------------------------------


<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font"
	lineNumbers="N"
	tabs="5|10|15|20"
	>
The next text <Font p="10pt" >is in 10 points.</Font>
This text is back to the default paragraph text size.
</exampleBlock>


<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fcolor" long="textColor" value="name" rs="2" 
	>

<pdp>
Font color.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font" 
	>
Today is a <Font fcolor="Red" >Red</Font> letter
day ...
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Underline and overline"
	longTitle="Underline and overline"
	dest="<#elementName>.propertyGroup.underline_and_overline.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="underline" long="textUnderline" value="none | N | normal | Y | numeric | double" 
	@- condition="skip"
	>

<pdp>
Underline style.
<vidx id="underline||normal (single)" />
<vidx id="underline||low" />
<vidx id="underline||numeric" />
<vidx id="underline||double" />
</pdp>

<propertyValueKeyList
	keyHeading="Value"
	element="P|TextLine|ALine" >
	>
<vk key="none | N" >No underline.</vk>
<vk key="normal | Y" >Normal (single) underline.</vk>
<vk key="numeric" >Numeric (low) underline.</vk>
<vk key="double" >Double underline.</vk>
</propertyValueKeyList>

<propertyValueKeyList
	element="Font"
	>
<vk key="none | N" >No underline.</vk>
<vk key="normal | Y" >Normal (single) underline. By default
the <ov>normal</ov> underline
baseline offset and underline thickness are as specified by the
corresponding values in the <fB>post</fB> table of the current font.
If these values are not present in the current font (.ttf or .otf),
the baseline offset and underline thickness are set to 10% and 6%
of the current font size.
</vk>
<vk key="numeric" >Numeric underline. By default
the thickness of a <ov>numeric</ov> underline is 0.5pt and the
baseline offset to the mid-point of the underline is 2pt, irrespective
of the current font size.
A <ov>numeric</ov> underline is low at small font sizes and
high at large font sizes.
</vk>
<vk key="double" >Double underline. By default
the thickness of each line in a <ov>double</ov> underline is 0.5pt and
the baseline offset to the mid-point of the first underline is 2pt,
irrespective of the current font size. The pitch between the two
underlines is 2pt.
</vk>
</propertyValueKeyList>

<pdp>
By default the underline color is as specified by
the <on>textColor</on> property, i.e. the color of the text above
the underline. 
Underline color may be changed by
setting the <on>underlineColor</on> property.
Underline thicknesses and baseline
offsets may be changed by setting
the <on>underlineThickness</on> and <on>underlineOffset</on> properties.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- @- See the description of <on><#propertyName></on> in the <#xFont> element.
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="normal" >normal underline</Font> 
<Font underline="double" >double underline</Font><br/>
<Font underline="numeric" >numeric underline</Font> 
<Font underline="normal" textColor="Red" >normal underline</Font> 
<Font underline="none" >switch off underline</Font> 
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="underlineColor" long="underlineColor" value="name"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline color.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="normal" 
	>underline<Font textColor="Red">red</Font></Font><br/>
<Font underline="normal" textColor="Red"
	>underline<Font textColor="Black">red</Font></Font><br/>
<Font underline="normal" textColor="Red" underlineColor="Blue"
	>underline<Font textColor="Black">red</Font></Font><br/>
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="underlineOffset" long="underlineOffset" value="dim | percent"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline offset from text baseline to the center of the underline
(or the first underline in the case of a double underline)
as an absolute value or a percentage of the text size.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="normal" underlineOffset="30%" >Low underline
 (30% of text size) <Font p="4pt">small text</Font></Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>


@------------------------------------------------------------
<property short="underlineThickness" long="underlineThickness" value="dim | percent"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline thickness as an absolute value or a percentage of
the text size.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="normal" underlineThickness="14%"
	>Thick underline (14%) </Font> no underline<br/>
<Font underline="normal" underlineThickness="1.2pt" underlineColor="Red"
>Thick underline (1.2pt)</Font> no underline
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property short="underlineGap" long="underlineGap" value="dim | percent"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline gap between the centers of the underlines as an absolute value or
a percentage of the text size.
Applicable only when <on>underline</on> is set to <ov>double</ov>.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="double" underlineThickness="16%"
	underlineColor="Red"
	underlineOffset="56%"
	underlineGap="42%"
	@- >Double underline</Font><br/>&nbsp;
	>Double underline with custom gap between lines</Font><br/>&#x00A0;
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>


@------------------------------------------------------------
<property short="overline" long="textOverline" value="N | Y" >

<pdp>
Overline.
<vidx id="overline (text)" />
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<exampleBlock element="Font" >
<Font overline="Y" >overline</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Strike through and change bar"
	longTitle="Strike through and change bar"
	dest="<#elementName>.propertyGroup.strike_through"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="t" long="strikeThrough" value="N | Y" >

<pdp>
Strike through.
<vidx id="strike through (text)" />
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|ParaDef|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g.
</pdp>

<exampleBlock element="Font" >
<Font strikeThrough="Y" >strike through</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property short="C" long="changeBar" value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Change bar.
<vidx id="change bar" />
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<exampleBlock element="Font" >
<Font C="Y" >change bar</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>


@------------------------------------------------------------
<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Capitalization"
	longTitle="Capitalization"
	dest="<#elementName>.propertyGroup.capitalization.start"
/>
@------------------------------------------------------------

@- <@xmacro> referToFontElement {
@- <pdp element="P|TextLine|ALine" >See the description of <on><#propertyName></on> in the <#xFont> element on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>
@- }{}

@------------------------------------------------------------
<property short="fc" long="textCase" value="asTyped | smallCaps |  lowercase | uppercase" >

<docNote>
BUG mmc
<br/>
Do in C++ ?
<br/>
Or do in ICU4C ?
<br/>
Latter prob. better.
<br/>
(Not done yet.)
<br/>
Add aliases for fmc
</docNote>
<pdp>
Capitalization.
</pdp>

<propertyValueKeyList
	>
<vk key="asTyped" >As typed.</vk>
<vk key="smallCaps" >Small caps.</vk>
<vk key="lowercase" >Lower case.</vk>
<vk key="uppercase" >Uppercase.</vk>
</propertyValueKeyList>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

@------------------------------
<referToFontElement/>
@------------------------------


<pdp element="Font" >
E.g. this input
</pdp>

<exampleBlock element="Font" >
<Font fc="asTyped" >Small caps</Font>
<Font fc="smallCaps" >Small caps</Font>
<Font fc="lowercase" >Small caps</Font>
<Font fc="uppercase" >Small caps</Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="T" />
</property>

<@skipEnd>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Vertical and horizontal shift"
	longTitle="Vertical and horizontal shift"
	dest="<#elementName>.propertyGroup.vertical_horizontal_shift.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="u" long="superscript" value="N | Y" >

<pdp>
Superscript.
<vidx id="superscript (text)" />
</pdp>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g.
</pdp>

<exampleBlock element="Font" >
Normal text<Font superscript="Y" >superscript</Font> normal text
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>

@------------------------------------------------------------
<property short="d" long="subscript" value="N | Y" >

<pdp>
Subscript.
<vidx id="subscript (text)" />
</pdp>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<exampleBlock element="Font" >
Normal text<Font subscript="Y" >subscript</Font> normal text
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="Ky"
	long="verticalShift"
	value="percent"
	>

<pdp>
Vertical displacement of text.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
<fI>percent</fI> is the required text downshift
expressed as a percentage of an em in the current font size.
If <fI>percent</fI> is preceded by a minus
sign the text is upshifted.
</pdp>


<pdp element="Font" >
The upshift or downshift is
relative to the default paragraph baseline.
</pdp>

<pdp element="Font" >
If the paragraph line spacing is set to fixed using
the paragraph properties in a template document or by
setting the <#xParaDef> or <#xP> element <on>lineSpacing</on> property
to <ov>fixed</ov> (see pages <xnpage
	id="ParaDef.property.lineSpacing"  /> and <xnpage
	id="P.property.lineSpacing" file="inline" />),
the shifted text may interfere
with text above or below and may also
extend above or below the border of a
text frame or a table cell.
</pdp>



<pdp element="Font" 
	>
This input
</pdp>
<exampleBlock
	 element="Font" >
Some text <Font p="3mm" ><Font verticalShift="100" >down1
<Font verticalShift="200" >down2<Font verticalShift="-50" >up1
</Font></Font></Font></Font> more text...
</exampleBlock>

<pdp element="Font"
	lineSpacing="P"
	>
produces:<br/>
<#example_output>
</pdp>


<propertyDefault v="0" />
</property>

@------------------------------------------------------------

@------------------------------------------------------------
<property short="Kx"
	long="horizontalShift"
	value="percent"
	>

<pdp>
Horizontal text displacement.
</pdp>


@- <includeCONDITION	elementName="Font">"

<pdp>
<fI>percent</fI>
 is the required text leftshift
expressed as a percentage of the font size.
If 
<fI>percent</fI>
is preceded by a minus
sign, the text is rightshifted.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
For example
</pdp>

<exampleBlock
	element="Font"
	>
This text is shifted<Font p="3mm" >||rightwards<Font
fcolor="Red">|</Font></Font> (not)<br/>
This text is shifted<Font p="3mm" >|<Font horizontalShift="200"
>|rightwards</Font><Font textColor="Red" >|</Font>
</Font>
</exampleBlock>


<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

<pdp element="Font" >
If more text is added immediately after
the input shown above, it would be placed immediately
after the second red vertical bar, <#osq>|<#csq>, super-imposed
on the last few letters of the second <#osq>rightwards<#csq>.
</pdp>



<pdp element="Font" >
For example
</pdp>

<exampleBlock
	element="Font"
	>
<Font p="3mm">|T|<Font fcolor="Red">|</Font>
</Font>the letters <#osq>|T|<#csq> are shifted leftwards. They
have a falsely small left indent. (not)<br/>
<Font p="3mm"><Font horizontalShift="-200" >|T|</Font><Font
fcolor="Red">|</Font>the letters <#osq>|T|<#csq> are shifted
leftwards. They have a falsely small left indent.</Font>
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>


<propertyDefault v="0" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Kerning, ligatures and character spread"
	longTitle="Kerning, ligatures and character spread"
	dest="<#elementName>.propertyGroup.kerning_spread_stretch.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="K"
	long="textKern"
	value="Y | N"
	>


<docNote>
BUG: fix mmc
</docNote>

<pdp>
Enable pair kerning.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------


<pdp element="Font" >
This input
</pdp>

<exampleBlock
	element="Font"
	>
@- <Font p="4mm" ff="Palatino Linotype" K="Y" >
<Font p="3mm" ff="Times New Roman" K="Y" >AWAY</Font> and
 <Font p="3mm" ff="Palatino Linotype" K="N" >AWAY</Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="ligatures"
	long="ligatures"
	value="N | Y"
	>

<pdp>
Enable ligatures.
</pdp>

<pdp element="Font" >
This input
</pdp>

<exampleBlock
	element="Font"
	>
@- <Font p="3mm" ff="Palatino Linotype" fw="Regular" >
<Font p="3mm" ff="Palatino Linotype" >No ligature ffi: Office<Font
@- ligatures="Y" <hc>hyphenate="N"</hc>> | Ligature ffi: Office</Font></Font>
ligatures="Y" <hc>hyphenate="Y"</hc>>  Ligature ffi: Office</Font></Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property
	short="fontFeatures"
	long="fontFeatures"
	value="keylist"
	>

<pdp>
Enable selected OpenType advanced features.
</pdp>

<pdp element="Font" >
<fI>keylist</fI> may comprise one of more <fI>key</fI> values
separated by vertical bars. Each key value must be four alphanumeric
characters long and correspond to the values listed in the OpenType
specification. Invalid features and requests for features not present
in the font are ignored.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g. this input
</pdp>

<exampleBlock
	element="Font"
	>
<Font textSize="3mm" fontFamily="Gabriola" 
	@- >Normal Lorem amat ipsum|<Font fontSize="5.5mm"
	>Normal Lorem amat ipsum|<Font textSize="4mm"
fontFeatures="ss06" > Convoluted Lorem amat ipsum</Font></Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property
	short="Ks"
	long="textSpread"
	value="percent"
	>

<pdp>
Inter-character spread, or tracking.
</pdp>



<pdp>
<fI>percent</fI>
is relative
<Font K="N" ><Font textSpread="20" >increase/
<Font textSpread="-5" >decrease
</Font>
</Font>
</Font>
in character spread. Negative
values reduce character spread.
Spread is expressed as a percentage of
the font size.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
The character spread property in <#xFontDef>
or the <#xFont> <on>K</on> property (below)
must be switched on
for character spread values to affect output.
</pdp>


<@skipStart>
<pdp element="Font" >
Small values of character spread,
e.g. &lt;Font textSpread="20" >
<Font textSpread="20" >increase
</Font>&lt;/Font&gt;
, and especially, 
&lt;Font textSpread="-5" &gt;
<Font textSpread="-5" >decrease
</Font>&lt;/Font&gt;
have a big visual effect.
</pdp>
<@skipEnd>



<@skipStart>
<pdp element="Font" >
(For examples showing the effect of the
&lt;Font... >
<on>textSpread</on> property see the
<fI>Miramo User Guide</fI>, Chapter
<xparanumonly id="textpos.chapter.start" file="textpos" />
, pages 
<xnpage id="textpos_character_spread.start"
	file="textpos" /><xphyphen/><xnpage
	id="textpos_character_spread.end" file="textpos" />
.)
</pdp>
<@skipEnd>


<propertyDefault v="0" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fs" long="textStretch" value="percent" rs="2"
	condition="fmcGuide"
	>


<pdp>
Horizontally expand or compress characters.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
This is <Font fs="150" >expanded</Font> text and
this is <Font fs="50" >compressed</Font> text.
</exampleBlock>


<pdp element="Font" 
	condition="mmcSkip" >
produces:<br/>
<#example_output>
</pdp>

<propertyDefault v="100" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Text background color"
	longTitle="Text background color"
	dest="<#elementName>.text_background_color.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillColor" long="fillColor" value="name"
	>

<docNote>
BUG. Color background too low.
<br/>
Add alias for fmc
</docNote>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp>
Apply a background color to text.
<outputRule condition="fmcGuide" >FrameMaker 10 and above only.</outputRule>
</pdp>



@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
@- Text with a <Font fillColor="Salmon">salmon-colored</Font> background.
@- Text with a <Font fillColor="rgb(0 200 0)">salmon-colored</Font> background.
@- Text with a <Font fillColor="rgb(#x00ff00)">salmon-colored</Font> background.
@-Text with a <Font fillColor="rgb(#00ff00)" >salmon-colored</Font> background. @- Works!
@- Text with a <Font fillColor="rgb(226,212,185)" >cool</Font> background. @- Works!
Text with a <Font fillColor="rgb(426,-212,-185)" >cool</Font> background. @- Works!
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>no background text color</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillTint" long="fillTint" value="percent"
	condition="mmcGuide"
	>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp>
Tint for text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
Text with a <Font fillColor="Blue" fillTint="30%">blue-tint</Font> background.
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>100%</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillOpacity" long="fillOpacity" value="100 | 0"
	condition="mmcGuide"
	condition="skip"
	>

@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp>
Opacity of text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
Text with a <Font fillColor="Blue" fillOpacity="30%">20% opacity</Font> background.
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>100%</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<@include>	${ctext}/fontHyphenation.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@include>	${ctext}/language.inc
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = font.inc (end) -->
@------------------------------------------------------------
