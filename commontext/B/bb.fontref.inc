@--------------------------------------
@-- bb.fontref.inc
@--------------------------------------
@--- FIXED

@-----------------------------
<@BBlock> {
&lt;FontRef fmt=name > ... &lt;/FontRef>
}{
The <#xFontRef> in-text markup code may be used
with its <on>fmt</on> option set to the
name of a pre-defined font format.
<#xFontRef> codes may not be nested.
} {FontRef}
@-----------------------------
