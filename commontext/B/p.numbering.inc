@---------------------------------------------
@-- p.numbering.inc
@---------------------------------------------
@--- FIXED

@-----------------------------
<propertyGroup
	shortTitle="Autonumbering"
	longTitle="Autonumbering"
	dest="P.optgroup.autonumbering"
/>
@-----------------------------



@-----------------------------
<property short="an" long="autoNumber" value="N | Y" >

<pdp>
Enable autonumbering.
<vidx id="paragraph||numbering" />
<vidx id="paragraph||autonumbering" />
<vidx id="numbering||paragraph" />
<vidx id="autonumbering||paragraph" />
</pdp>

<pdp>
See the <#xListLabel> element on page
<xnpage id="ListLabel.element.start" file="inline" />
 for information on how to set up <#osq>autonumber<#csq>
text for a paragraphs.
</pdp>
</property>

