@------------------------------------------------------
@- frameDimensionsWH.inc
@------------------------------------------------------
@--- FIXED

<#def> aframeW.sub {}
<#def> aframeH.sub {}

@------------------------
<@xmacro> aframe.sub {
	@if(<#CodeName1> = AFrame) {
@- <xOptPara/>
<P>
The value of <on><#optRef></on> is ignored if
<on>wrap</on> is set to <ov>Y</ov> (see <on>wrap</on> option below).
		}
}
@------------------------



@------------------------
<xOptGroup
	shortTitle="Frame dimensions"
	longTitle="Frame dimensions"
	XRefID="<#CodeName1>.optgroup.dimensions"
	/>

@-----------------------------
<xOption short="W" long="frameWidth"
	value="dim"
	>
Width of frame.
<aframe.sub/>
</xOption>

@-----------------------------
<xOption short="H" long="frameHeight"
	value="dim"
	>
Height of frame.
<aframe.sub/>
</xOption>
@------------------------------------------------------

