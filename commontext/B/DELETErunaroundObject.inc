@-------------------------------------------------------
@- runaroundObject.inc
@-------------------------------------------------------


<@write> error {
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Using: runaroundObject.inc (<#CodeName1>)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

@-----------------------------
<xOption short="RA" long="textRunAround"
	value="N | B | C" >
Text runaround type.
<|valuekeyhead> 1|16|1|Value|Description
<|valuekey> N|No runaround
<|valuekey> B|Box runaround
<|valuekey> C|Contour runaround

<xOptDefault>N</xOptDefault>
</xOption>

@-----------------------------
<xOption short="RAgap" long="runAroundGap"
	value="dim" >
Specify the gap between the object
and text flowing around it.

<xOptPara/>
Effective only if the <on>RA</on> option (see above) is
set to <ov>B</ov> or <ov>C</ov>.

<xOptDefault>6pt</xOptDefault>
</xOption>

