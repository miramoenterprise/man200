@-------------------------------------------
@-- tf.flowoptions5.inc
@-------------------------------------------
@--- FIXED

@-----------------------------
<xOptGroup
	shortTitle="Column balancing"
	longTitle=""
	XRefID="<#CodeName1>.optgroup.column_balancing"
/>
@-----------------------------


@-----------------------------
<xOption short="cb" long="balanceColumns"
        value="N | Y" >
Balance columns. Adjust the sub-columns in a final text
frame which is not full so that the text is evenly
distributed across the sub-columns.
<MkDest fmt=B id="<#CodeName1>.options.column_balancing.end" />
</xOption>

