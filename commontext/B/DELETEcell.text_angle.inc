@-------------------------------------------------
@-- cell.text_angle.inc
@-------------------------------------------------
@--- FIXED

@-----------------------------
<xOption short="a" long="rotateText" value="0 | 270 | 90 | 180" >

<vidx id="angle||in table cells" />
<vidx id="rotation||in table cells" />
<vidx id="table||rotation of text in" />
<vidx id="table||rotation of cell margins" />
<vidx id="margins||rotating in table cells" />
Angle of text in cell<#tbl_cm1>. Rotation is clockwise.
<P>
Note that the placement orientation of cell
rulings (as specified by the <on>lr</on>,
<on>tr</on>, <on>rr</on> and <on>br</on> options) always remains
fixed regardless of text rotation angle.
<P>
Cell margins, as specified by both
@- CRIN 01-APR-96: changed Cm to clm
the <#xTbl> and <#xCell> cell margin options
(<on>clm</on>,
<on>ctm</on>,
<on>crm</on>
and
<on>cbm</on>)
<#fI>do<#fnI> rotate.

<xOptDefault>0</xOptDefault>
</xOption>
