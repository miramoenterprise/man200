@---------------------------------------------
@-- p.cjk.inc
@---------------------------------------------
@--- FIXED

@--+-+-+-+-+-+-+-+-+-+-+-+-+-+
<xOptGroup
	shortTitle="CJK"
	longTitle="CJK options"
	XRefID="P.optgroup.cjk"
	></xOptGroup>

@-----------------------------
<xOption short="gAls" long="letterSpaceMax" value="percent" >

Maximum Asian font letter space.

</xOption>

@-----------------------------
<xOption short="oAls" long="letterSpaceOpt" value="percent" >

Optimum Asian font letter space.

</xOption>

@-----------------------------
<xOption short="lAls" long="letterSpaceMin" value="percent" >

Minimum Asian font letter space.

</xOption>

@-----------------------------
<xOption short="gARls" long="letterSpaceRomanMax" value="percent" >

Maximum Roman letter space.

</xOption>

@-----------------------------
<xOption short="oARls" long="letterSpaceRomanOpt" value="percent" >

Optimum Roman letter space.

</xOption>

@-----------------------------
<xOption short="lARls" long="letterSpaceRomanMin" value="percent" >

Minimum Roman letter space.

</xOption>

@-----------------------------
<xOption short="yaku" long="yakumono" value="F" >

Yakumono setting.

</xOption>

<xOptGroup/>
@-----------------------------
