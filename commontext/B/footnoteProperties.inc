@------------------------------------------------------------
@- footnoteProperties.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Document footnote properties"
	longTitle="Document footnote properties"
	dest="<#elementName>.propertyGroup.footnote_settings"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="fp"
	long="footnoteParagraphFormat"
	value="name"
	>
<pdp>
Default paragraph format for document footnotes.
</pdp>

<propertyDefault>Footnote</propertyDefault>
</property>


@------------------------------------------------------------
<property short="fn" long="footnoteLabelStart"
	value="int"
	>
<pdp>
Footnote label start number. If custom footnote labels are
used, as specified by the <on>footnoteLabelSequence</on> property,
use the <fI>n</fI>th label in the list.
</pdp>

<propertyDefault>1</propertyDefault>
</property>

<@skipStart>
@------------------------------------------------------------
<property short="fh"
	long="FNoteMaxHeight"
	value="dim"
	>
<pdp>
Maximum height allowed for footnotes.
</pdp>
</property>
<@skipEnd>

@------------------------------------------------------------
<property short="fs" long="footnoteLabelRestart"
	value="perPage | perDocument"
	>
<pdp>
Footnote sequence re-start frequency.

Set to <ov>perPage</ov> to re-start footnote label sequence
on every page.
</pdp>

<propertyDefault>perDocument</propertyDefault>
</property>

@------------------------------------------------------------
<property short="" long="footnoteLabelSequence"
	value="stringlist"
	>
<pdp>
Define set of characters or strings to be used as footnote
labels if the <on>footnoteLabelStyle</on> property is
set to <ov>custom</ov>.
<vidx id="autonumbering||footnotes" />
</pdp>
<pdp>
<fI>stringlist</fI> is a list of single or multi-character strings
separated either by commas or vertical bars.
All characters specified must be present in the footnote font.
</pdp>
@- <#specialchars.inc>

<propertyDefault>"*&nbsp;\xa0 &nbsp;\xe0 "  (star, dagger and double dagger)</propertyDefault>
</property>

@------------------------------------------------------------
<property short="faP" long="footnoteAnchorPosition"
	value="superscript | baseline | subscript"
	>
<pdp>
Position of document footnote label in body text. 
</pdp>

<propertyDefault>superscript</propertyDefault>
</property>

@------------------------------------------------------------
<property short="fnP" long="footnoteLabelPosition"
	value="superscript | baseline | subscript"
	>
<pdp>
Position of document footnote label in footnote text. 
</pdp>

<propertyDefault>baseline</propertyDefault>
</property>

@------------------------------------------------------------
<property short="fnf" long="footnoteLabelStyle"
	value="custom | key"
	>
<pdp>
Footnote label style. If the value is set to <ov>custom</ov> use
the values specified by the <on>footnoteLabelSequence</on> property.
</pdp>

<#numberStyleValues>

<propertyDefault>westernArabic</propertyDefault>
</property>

@------------------------------------------------------------
<property short="fap" long="footnoteAnchorPrefix"
	value="string"
	>
<pdp>
Prefix to appear before the document footnote label in body text.
</pdp>
</property>

@------------------------------------------------------------
<property short="fas" long="footnoteAnchorSuffix"
	value="string"
	>
<pdp>
Suffix to appear after the document footnote label in body text.
</pdp>
</property>

@------------------------------------------------------------
<property short="fnp" long="footnotePrefix"
	value="string"
	>
<pdp>
Prefix to appear before the footnote label in footnote text.
</pdp>
</property>

@------------------------------------------------------------
<property short="fns" long="footnoteSuffix"
	value="string"
	>
<pdp>
Suffix to appear after the footnote label in footnote text.
</pdp>
<propertyDefault>.</propertyDefault>
</property>

@------------------------------------------------------------
