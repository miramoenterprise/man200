@----------------------------------
@- pmulti.option.inc
@----------------------------------
@--- FIXED

@------------------------
<@include> ${CTEXT}/pmulti.txt
<@Option> Pmulti    {
<#fI>key<#fnI> |<br/>
<#fI>key<#fnI>+<#fI>key<#fnI>
}{
<#pmulti.para1>
<xOptPara/>
<#pmulti.para2>
<xOptPara/>
<#pmulti.para3>
<xOptPara/>
<#pmulti.para4>
<xOptPara/>
<#pmulti.para5>
<xOptPara/>
<#pmulti.para6>
<xOptPara/>
<#pmulti.para7>
<xOptPara/>
<#pmulti.para8>
<xOptPara/>
See also the
<xcmdopt opt="PDFjobopts" />
 option on page
<xnpage id="running.option.PDFjobopts" file="running" />
.
}{Doc|inline}
