@------------------------------------------------------------
@-- buildingBlock.deftext.inc
@------------------------------------------------------------
@-- This is appropriate for inclusion in the following
@-	markup code descriptions:
@-	AutoNum
@-	BGMarker
@-	HyperCmd
@-	Marker
@------------------------------------------------------------

<buildingBlock 
	blockName="deftext"
	blockNameFmt="Italic"
	>
@- <pdp>
@- <fI>deftext</fI>
@- </pdp>

<bbp>
<fI>deftext</fI> consists of text, newline characters
and binary tabs. All binary newlines and tabs
are ignored.
@- are mapped to
@- space characters. Binary tabs
@- are preserved unless the <#osq><#nbhy>striptabs<#csq>
@- command line option is used.
</bbp>

<bbp>
Special characters
may be encoded using
the character entity name string
(&amp;<fI>name</fI>;), or the 
numeric character reference (&amp;#<fI>nn</fI>;
or &amp;#x<fI>hhhh</fI>;).
If the requested character is not
available in the current font, it is
ignored with a warning message.
</bbp>
</buildingBlock>

@------------------------------------------------------------
@- <br/> 
@------------------------------------------------------------
<@include> ${CTEXT}/buildingBlock.br.inc
@------------------------------------------------------------
@- <tab/> 
@------------------------------------------------------------
@- <@include> ${CTEXT}/buildingBlock.tab.inc
@------------------------------------------------------------
