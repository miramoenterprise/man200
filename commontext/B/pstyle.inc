@---------------------------------------------
@-- pstyle.inc
@---------------------------------------------
@- Included in:
@- 
@- atextframeNew
@- aframeNew
@---------------------------------------------
@--- FIXED

<#def> Filename		xxxxxxxx
<@macro> pstyle_xref_to_guide {
	@ifnot(<#Filename> = {md_inline}) {
		<#def> pstyle_xref_to_guide	{ in the <#mmDrawRef>}
		}
	@else {
		<#def> pstyle_xref_to_guide	{}
		}
	<#def> pstyle_line_or_border	{border}
	@if(@match({X<#CodeName1>X},{XPolyLineX})) {
		<#def> pstyle_line_or_border	{line}
		}
}
@-----------------------------
<|pstyle_xref_to_guide>
@-----------------------------

<property short="pstyle" long="penStyle"
	value='P | "dim dim ..."'
	four="PolyLine|md_inline"
>

<pdp>
Pen style. Solid or dashed.
</pdp>

<pdp>
Setting <on>pstyle</on> to <ov>P</ov> results in a plain,
solid border or line.
</pdp>

<pdp>
Setting <on>pstyle</on> to a series comprising
an even number of <#fI>dim<#fnI> values sets
the <#pstyle_line_or_border> to have a dashed pattern.
The first <#fI>dim<#fnI> value specifies the
length of a solid line segment, the next 
<#fI>dim<#fnI> value specifies the length
of a white segment, and so on.
</pdp>

<pdp>
The pen pattern specification must be
surrounded by double quotation marks.

See Examples
<xparanumonly id="ex.dashed_patterns1" file="md_inline" />
 and
<xparanumonly id="ex.dashed_patterns2" file="md_inline" />
 on pages
<xnpage id="ex.dashed_patterns1" file="md_inline" />
 and
<xnpage id="ex.dashed_patterns2" file="md_inline" />
<#pstyle_xref_to_guide>.
</pdp>


<propertyDefault>P</propertyDefault>
</property>
