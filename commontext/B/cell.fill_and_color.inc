@-------------------------------------------------
@-- cr.inc  ( <Cell> & <Row> common options )
@-------------------------------------------------
@--- FIXED

<xOption short="fill" long="cellFill" value="0 - 15" >

Fill pattern. When used with the <#xRow> code, sets
the default fill for all cells in the row.

(See Appendix
<xparanumonly id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />
, page
<xnpage id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />
, for a description of fill patterns.)
<vidx id="fill||in table cells" />

<xOptDefault>7</xOptDefault>

</xOption>


<xOption short="color" long="cellColor" value="name" >

Background color.  When used with the <#xRow> code, sets
the default color for all cells in the row.
<vidx id="color||in table cells" />

<xOptPara/>
Note that <on>fill</on> must
be set to a value other than
<ov>7</ov> (opaque) or <ov>15</ov> (no fill, transparent) for
the designated color to print.
(Default value
for <on>fill</on> is <ov>7</ov>).

</xOption>

@-----------------------------
