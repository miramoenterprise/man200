@-------------------------------------------------------
@- vardefs_list.inc
@-------------------------------------------------------
@--- FIXED

<#def> xcol1    29.5
<#def> xcol2    16
<#def> xcol3     0.1
<#def> xcol4    @eval((<#TextWidth> - 10) - (<#xcol1> + <#xcol2> + <#xcol3>))
<P> P_Body sb=-2pt l=-2pt p=2pt
<Tbl fmt=T_formatdefs_list li=1.5cm A=R TP=B ctm=2pt cbm=2pt sa=-2pt crm=0pt wo=2 >
<TblColWidth  W=<#xcol1>mm />
<TblColWidth  W=<#xcol2>mm />
<TblColWidth  W=<#xcol3>mm />
<TblColWidth  W=<#xcol4>mm />
<TblTitle fmt=P_TableTitle TP=A >
<#formatdefs_tbl_title> \vtc
<MkDest fmt=X id=<#formatdefs_tbl_xr> />

# ---	
<Row type=F h=1pt fill=0 >
<Row type=F >
<Cell cs=4 clm=0 >
<P fmt=P_TableFootnote li=4mm >
a.	If a variable is defined more than the maximum number
of instances shown in this column, later definitions
override earlier instances.
<Row type=H >
<Cell fmt=output H=N clm=0 fw=Bold >
Variable definition code
<Cell fmt=output H=N clm=3pt crm=0pt fw=Light p=10pt ctm=0.6 l=0 li=.2mm A=L >
&thinsp;<Font p=8pt fw=Bold >Max. instances</Font><Font u=Y>a
<Cell>
<Cell>
<P fmt=output fw=Bold H=N >
\fbDescription
@- <TblColP> inputCell
<Row fmt=inputCell  type=H h=1pt fill=0 >
<Row fmt=inputCell  type=H h=1mm >
@-------------------------------------------------------------

<@macro> fdef_row {
	<#def> xcode_name	$2
	<#def> fdef_row_Y	1
	@if(@match({<#xcode_name>}, {Combi})) {
		@ifnot(<#incCJK>) {<#def> fdef_row_Y	0}
		}
	@if(<#fdef_row_Y>) {
		@if($# > 5) {
			<Row> h=$6mm
			}
		@else	{
			@- <TblColA> L   C  C L
			<Row ctm=6pt cbm=1pt >
			}
		<Cell fmt=inputCell fi=$1mm clm=0 ctm=6pt cbm=1pt >
<#pdfcolor_start>
\<$2>
@- 		<Marker> name=hy
@- 		@if(<#xcode_name> = {VarD}) {
@- 			<#def> xcode_name	VarDef
@- 			}
@- gotolink @gsub({<#xcode_name>},{[\\ \.]+},{}).code.start
@- 		</Marker>
@- BUG ????
@- <HyperCmd jumptodest="@gsub({<#xcode_name>},{[\\ \.]+},{}).code.start" newwin=Y/>
<HyperCmd jumptodest="@gsub({<#xcode_name>},{[\\ \.]+},{}).code.start" newwin=Y />
<#pdfcolor_end>
		<Cell>
$3
		<Cell>
		<Cell>
$5
	}
}
