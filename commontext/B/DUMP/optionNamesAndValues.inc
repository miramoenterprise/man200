@---------------------------------------------------------
@- optionNamesAndValues.inc
@---------------------------------------------------------
@--- FIXED

<@macro> valueTypeItem {
<Row h=1mm >
<Row>
<Cell>
<Font fmt=F_Italic>$1
<Cell>$2
}
<@macro> valueTypeItemP {
<Row>
<Cell>
<Cell>$1
}


<|Head> Option names

In most cases option names have a short form and an
alternative, mnemonic long form. The long form is shown within
square brackets immediately below the short form.


<P fmt=P_Body>
The two forms are illustrated below.

<nExample fi=15  lineNumbers="N" >
	ff
	[ fontFamily ]
</nExample>

<P fmt=P_Body>
In the above, <on>ff</on> is the short form and <on>fontFamily</on> is
the long form. Both forms may be used interchangeably.


<|Head> Option values and value types
In all cases option values may be enclosed within a pair
of quotation mark, ", or apostrophe
characters, ', <UniChar>0027</UniChar>, <UniName>APOSTROPHE</UniName> or <UniChar>0022</UniChar>, <UniName>QUOTATION MARK</UniName>.

<P fmt=P_Body>
Most of the value types given in the <#SectHead>Syntax<#nSectHead> sections
on the following pages are self-explanatory. Exceptional items
are explained below:

<Tbl  fmt=T_None TP=N A=R li=1.5cm clm=.5mm ctm=.5mm crm=.5mm cbm=.5mm P=A >
<TblColWidth W=30mm/>
<TblColWidth W=7.5cm/>
<TblTitle fmt=P_TableTitle TP=A A=L fi=15mm >
Summary of option value types <TblCont/>
<TblColFormat fmt=P_Output A=L />

<Row type= H>
<Cell>
<Font fmt=F_Bold>Value type
<Cell>
<Font fmt=F_Bold>Description

@----------------------------------------------------------------
<|valueTypeItem> {name}|{
A <#fI>name<#fnI> value may comprise any sequence of
printable characters, excluding newlines or binary tabs. 
If a <#fI>name<#fnI> value contains
any of the following characters: space, <, >, or =,
the <#fI>name<#fnI> value <#fI>must<#fnI> be enclosed
within a pair of quotation mark, ", or
apostrophe characters, ',
<UniChar>0027</UniChar>
<UniName>APOSTROPHE</UniName>
or
<UniChar>0022</UniChar>
<UniName>QUOTATION MARK</UniName>.
}
<|valueTypeItemP> {
For <#fI>name<#fnI> values containing the 
<UniChar>0022</UniChar>
quotation mark character, use apostrophe characters; otherwise, use
quotation mark characters where necessary.
}
<|valueTypeItemP> {
<#specialchars.inc>
}
<|valueTypeItemP> {
A <#fI>name<#fnI> value may be the name of a local, user-specified
formatting object, e.g. a master page format, condition name or a
paragraph format name.
In certain cases a name value may refer to an externally
defined object, e.g. the name of a font or a font variation.
}

<|valueTypeItemP> {
<P fmt=P_Output><TabStops><TabStop A=L pos=1cm/></TabStops>
The following are examples of valid <Font fmt=F_Italic>names:</Font><br/>
	!@#$%<br/>
	'Yellow Butterfly'
	"Ang&egrave;le's address"
}

@----------------------------------------------------------------
<|valueTypeItem> {namelist}|{
A comma separated list of <#fI>name<#fnI> values of a
specified type.
A <#fI>namelist<#fnI> value must be enclosed within a pair of quotation mark, ", or
apostrophe characters, ',
<UniChar>0027</UniChar>
<UniName>APOSTROPHE</UniName>
or
<UniChar>0022</UniChar>
<UniName>QUOTATION MARK</UniName>.
}

@----------------------------------------------------------------
<|valueTypeItem> {text}|{
<#fI>text<#fnI> may be any sequence of characters in UTF-8
encoding.
}

@----------------------------------------------------------------
<|valueTypeItem> {filename}|{
<P fmt=P_Output><TabStops><TabStop A=L pos=1cm/></TabStops>
Unless noted otherwise, any valid file name. If a <#fI>filename<#fnI> value contains
one or more space characters, the <#fI>filename<#fnI> value
<#fI>must<#fnI> be enclosed within
a pair of apostrophe (') or or quotation mark (") characters. 
}

<|valueTypeItemP> {
If the <on>-sendEnv</on> command line option is used, environment
<|idx> <FontRef fmt=input>filename
<|idx> environment variables||in file names
variables included in <#fI>filename<#fnI> are
expanded provided they are enclosed by opening and closing
curly braces. For example<br/>
	$\{IMAGES_DIR\}/logo.eps<br/>
See <#sendEnvXRef>.
}
@----------------------------------------------------------------
<|valueTypeItem> {file}|{
Exactly equivalent to <#fI>filename<#fnI>.
}

@----------------------------------------------------------------
<|valueTypeItem> {cmd}|{
Any valid command-line command string. Windows NT
and Unix shell environment
<|idx> <FontRef fmt=input>cmd
variables included in <Font fmt=F_Italic>cmd</Font> are
expanded.
}

@----------------------------------------------------------------
<|valueTypeItem> {dim}|{
A <Font fmt=F_Italic>dim</Font> may be any number, optionally followed
<|idx> <FontRef fmt=input>dim
<|idx> units
by a units indicator. The units indicator may be
any one of: <#osq>in<#csq>, <#osq>cm<#csq>, <#osq>mm<#csq>, <#osq>pt<#csq>, <#osq>pc<#csq>, <#osq>dd<#csq>, <#osq>cc<#csq>.
(<#osq>in<#csq> is inches,
<#osq>mm<#csq> is millimeters,
<#osq>cm<#csq> is centimeters,
<#osq>pt<#csq> is points,
<#osq>pc<#csq> is picas,
<#osq>dd<#csq> is didots and
<#osq>cc<#csq> is ciceros.)
}
<|valueTypeItemP> {
<P fmt=P_Output><TabStops><TabStop A=L pos=1cm/></TabStops>
The following are examples of using <#fI>dim<#fnI> options:<br/>

	clm = 3.14mm ctm=8pt crm=.01in<br/>
	pw= .2mm<br/>
	ri=12pc A = "R" textColor='Blue'
}

<|valueTypeItemP> {
<P fmt=P_Output><TabStops><TabStop A=L pos=1cm/></TabStops>
Note that white space must precede the option
name, <on>clm</on>, <on>ctm</on>, <on>pw</on>, <on>ri</on>, etc., but is not required
around the <#osq>=<#csq> sign. The units indicator must 
immediately follow the units value.<br/>
	ri=12 pc<br/>
with a space between the 2 and the pc is an error.
}

<|valueTypeItemP> {
<P fmt=P_Output><TabStops><TabStop A=L pos=1cm/></TabStops>
A negative value for <Font fmt=F_Italic>dim</Font> is valid in
many cases. e.g. use<br/>
	sa=-14pt<br/>
to set the space above a paragraph to -14 points.
}

<|valueTypeItemP> {
If no units indicator is specified, then the units
are assumed to be either the value set by the <#xUnits>
inline markup code or by a template file, or in
certain instances, points.
}

@----------------------------------------------------------------
<|valueTypeItem> {real}|{
A <Font fmt=F_Italic>real</Font> may be any number, without a units
<|idx> <FontRef fmt=input>real
indicator.
}

@----------------------------------------------------------------
<|valueTypeItem> {
<#fnI>Multiple<br/>
choice<br/>
values, e.g.<br/>
<#fI>L | C | R<#fnI><br/>
and<br/>
<#fI>Y | N <br/>
}|{
Many options have values consisting of a single character, or in some
rarer 
instances, a pair of characters or an integer, 
selected from a range comprising
two or more possible values. The range of allowable option values
is shown with a <#osq>|<#csq> (vertical bar) character between each value.
}

@----------------------------------------------------------------
<|valueTypeItem> {<#fnI>
Integer<br/>
ranges, e.g.<br/>
<#fI>(0 - 15)<#fnI>
}|{
Any integer between and including the first integer and the last
integer shown.
}

@----------------------------------------------------------------
<|valueTypeItem> {<#fnI>
<#fI>percent<#fnI><br/>
ranges, e.g.<br/>
<#fI>(-21.5% - 222%)<#fnI>
}|{
In the general case, any positive or negative number, optionally
followed by a percent character, %.
In some cases the number range may be restricted
between 0 and 100, or to positive values only.
}

</Tbl>


<|Head> Escape, control and special characters

The following table lists character names and descriptions
for special characters.

<@include> ${INLINE}/intext_tbl.inc

<@InTextTbl2> {3}{30|20}{Escape, control and special characters}{
Character name|Input|Description & notes}


<Row fmt=P_Inline_Opt ctm=6pt clm=0 >
<Cell>
Hard return
<Cell>
<#xbr><br/>
<MkDest fmt=X id=inline.br />
&amp;#10;<br/>
&amp;#xA;<br/>
&amp;#8232;<br/>
&amp;#x2028;
<|idx> hard return||including within text
<|idx> forced return||including within text
<Cell>
Any of the strings <#xbr>
&amp;#10;,
&amp;#xA;,
&amp;#8232; and
&amp;#x2028;
inserts a hard return in the output.
<br/>
<#fSC>line feed<#nF>
<br/>
<#fSC>line separator<#nF>
@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Backslash
<MkDest fmt=X id=inline.backslash />
<Cell>
\
<|idx> backslash||as escape character
<Cell>
A single backslash, <#osq>&#x5C;<#csq>, is the <#mmpp> escape 
character. It has no special meaning within Miramo
and is treated as any other printable character.

@-------------------------------
<Row fmt=P_Inline_Opt ctm=6pt  clm=0 >
<Cell>
Non-breaking space
<MkDest fmt=X id=inline.nbsp />
<|idx> space||non-breaking
<Cell>
&amp;nbsp;<br/>
&amp;#xA0;<br/>
&amp;#160;
<Cell>
Any of the strings 
&amp;nbsp;,
&amp;#xA0;
and &amp;#160;
inserts a non-breaking space.
<br/>
<#fSC>no-break space<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Non-breaking hyphen
<MkDest fmt=X id=inline.nbhy />
<|idx> hyphen||non-breaking
<Cell>
&amp;#x2011;<br/>
&amp;#8209;
<Cell>
Either of the strings 
&amp;#x2011; and
&amp;#8209;
inserts a non-breaking hyphen.
<br/>
<#fSC>non-breaking hyphen<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Em space
<MkDest fmt=X id=inline.nbhy />
<|idx> em space
<Cell>
&amp;#x2003;<br/>
&amp;#8195;
<Cell>
Either of the strings 
&amp;#x2003; and
&amp;#8195;
inserts an em space.
<br/>
<#fSC>em space<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
En space
<MkDest fmt=X id=inline.nbhy />
<|idx> en space
<Cell>
&amp;#x2002;<br/>
&amp;#8194;
<Cell>
Either of the strings 
&amp;#x2002; and
&amp;#8194;
inserts an en space.
<br/>
<#fSC>en space<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Tab
<MkDest fmt=X id=inline.tab />
<Cell>
<#xtab><br/>
&amp;#x9;<br/>
&amp;#9;
<Cell>
Any of the strings 
<#xtab>,
&#9;
and
&amp;#x9;,
inserts a tab character.
Alternatively, if the
command line <#osq>&#x2011;stripInput<#csq> option (see page 
<|npage> running.option.stripInput|running
) is not set to <ov>tab</ov>
and neither the <#xChapter> code nor the <#xDoc> code
<on>stripInput</on> option is set to <ov>tab</ov> (see pages 
<|npage>  Chapter.option.stripInput|inline
 and 
<|npage>  Doc.option.stripInput|inline
),
use the ASCII tab keystroke (binary tab).
<|idx>tabs||including in output
<br/>
<#fSC>horizontal tabulation<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Discretionary hyphen
<|idx> hyphen||discretionary
<|idx> discretionary hyphen
<Cell>
&amp;#x2027;<br/>
&amp;#8231;
<Cell>
Insert either of the strings 
&amp;#x2027; or
&amp;#8231;
to mark a discretionary hyphenation point.
<br/>
<#fSC>hyphenation point<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Thin space
<|idx> thin space||including in text
<Cell>
&amp;#x2009;<br/>
&amp;#8201;
<Cell>
Insert either of the strings 
&amp;#x2009; and
&amp;#8201;
to produce a thin space
(one sixth of an em).
<br/>
<#fSC>thin space<#nF>

@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Hair space
<|idx> hair space||including in text
<Cell>
&amp;#x200A;<br/>
&amp;#8202;
<Cell>
Insert either of the strings 
&amp;#x200A; and
&amp;#8202;
to produce a hair space
(one twelfth of an em).
This is the smallest possible space.
<br/>
<#fSC>hair space<#nF>

@-------------------------------
@-<Row fmt=P_Inline_Opt  ctm=6pt >
@-<Cell>
@-Zero width space
@-<Cell>
@-&amp;#x200B;<br/>
@-&amp;#8203;
@-<Cell>
@-Insert either of the strings 
@-&amp;#x200B; and
@-&amp;#8203;
@-to produce a zero width space.
@-<br/>
@-<#fSC>zero width space<#nF>
@-------------------------------
<Row fmt=P_Inline_Opt  ctm=6pt clm=0 >
<Cell>
Suppress hyphenation
<|idx> hyphenation||suppressing
<Cell>
&lt;NOhy/&gt;
<Cell>
Insert &lt;NOhy/&gt; at the beginning
of the word not to be hyphenated
(see page 
<|npage> NOhy.code.start
).
@-------------------------------
</Tbl>
@-------------------------------

