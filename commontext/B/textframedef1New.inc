@------------------------------------------------------------
@-- textframedef1.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Text frame dimensions"
	longTitle="Text frame dimensions"
	dest="<#elementName>.propertyGroup.dimensions"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="W"
	long="width"
	value="dim"
	>
<pdp>
Width of text frame.
</pdp>

<propertyDefault>40 mm<outputRule condition="fmcGuide">, or inherited
from preceding <#xTextFrameDef> or <#xNextTextFrameDef></outputRule></propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="H"
	long="height"
	value="dim"
	>
<pdp>
Height of text frame.
</pdp>

<propertyDefault>60 mm<outputRule condition="fmcGuide">, or inherited
from preceding <#xTextFrameDef> or <#xNextTextFrameDef></outputRule></propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Text frame position"
	longTitle="Text frame position"
	dest="<#elementName>.propertyGroup.position"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="L"
	long="left"
	value="dim"
	>
<pdp>
Distance of left side of text frame from left side
of enclosing frame or page.
</pdp>

<propertyDefault>0<outputRule condition="fmcGuide">, or inherited
from preceding <#xTextFrameDef> or <#xNextTextFrameDef></outputRule></propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="T"
	long="top"
	value="dim"
	>
<pdp>
Distance of top of text frame from top of enclosing frame or page.
</pdp>

<propertyDefault>0<outputRule condition="fmcGuide">, or inherited
from preceding <#xTextFrameDef> or <#xNextTextFrameDef></outputRule></propertyDefault>
</property>
@------------------------------------------------------------
