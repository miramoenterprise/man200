@--------------------------------------------------------
@- relativePointsize.inc
@--------------------------------------------------------
@--- FIXED

A relative text point size, <#fI>rdim<#fnI>,
value may only be used when the <#osq><#nbhy>tENC<#csq> command
line option (<xnpage id="running.option.tENC" file="running" />),
or the corresponding option on the
<#xDoc> (<xnpage id="Doc.option.tENC" file="inline" />),
<#xChapter> (<xnpage id="Chapter.option.tENC" file="inline" />),
<#xGenChapter> (<xnpage id="GenChapter.option.tENC" file="inline" />),
or <#xMiramoXML> (<xnpage id="MiramoXML.option.tENC" file="xmlcodes" />)
codes is explicitly set.

