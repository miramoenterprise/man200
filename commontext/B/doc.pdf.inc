@----------------------------------
@- doc.pdf.inc
@----------------------------------
@--- FIXED

@-----------------------------
<xOptGroup
	shortTitle="PDF output options"
	longTitle="PDF output options"
	XRefID="<#CodeName1>.optgroup.pdf_output_options"
	indexOnly="0"
        />
@-----------------------------


@------------------------
<xOption short="Opdf" long="Opdf"
        value="filename [ , pname ]"
	valueStraddle="1"
	four="Doc|inline"
	>
Name of PDF file to create.
<P>
@- <#pname>
<pname/>

<vidx id="PDF||<#CodeName1> code option" />
</xOption>

@------------------------
<xOption short="Opdfx" long="Opdfx"
        value="filename [ , pname ]"
	valueStraddle="1"
	four="Doc|inline"
	>
Name of PDF file to create. <#fI>filename<#fnI> will
contain no hypertext links. (Output files
will be smaller than those created using
the <on>Opdf</on> option, and production will be faster.)
<xOptPara/>
@- <#pname>
<pname/>

<vidx id="PDF||<#CodeName1> code option" />
</xOption>


@------------------------
<@include> ${CTEXT}/pmulti.txt
<xOption short="Pmulti" long="Pmulti"
        value="key | key+key"
	four="Doc|inline"
	>
@-- zzz check key+key
<#pmulti.para1>
<xOptPara/>
<#pmulti.para2>
<xOptPara/>
<#pmulti.para3>
<xOptPara/>
<#pmulti.para4>
<xOptPara/>
<#pmulti.para5>
<xOptPara/>
<#pmulti.para6>
<xOptPara/>
<#pmulti.para7>
<xOptPara/>
<#pmulti.para8>
</xOption>


@------------------------
<@include> ${CTEXT}/pdfmode.txt
<xOption short="PDFmode" long="PDFmode"
        value="normal | saveAs"
	four="Doc|inline"
	>
<#pdfmode.para1>
<P>
<#pdfmode.para2>
<xOptPara/>
<#pdfmode.para3>
<xOptPara/>
<#pdfmode.para4>
<xOptPara/>
<#pdfmode.para5>
<xOptDefault><#pdfmode.para6></xOptDefault>
</xOption>

@------------------------
<@include> ${CTEXT}/pdfcmyk.txt
<xOption short="PDFcmyk" long="PDFcmyk"
        value="N | Y"
	four="Doc|inline"
	>
<#pdfcmyk.para1>
<P>
<#pdfcmyk.para2>
<xOptPara/>
<#pdfcmyk.para3>
<xOptPara/>
<#pdfcmyk.para4>
<xOptPara/>
<#pdfcmyk.para5>
<xOptDefault><#pdfcmyk.para6></xOptDefault>
</xOption>

@------------------------
<@include> ${CTEXT}/jobopts.txt
<xOption short="PDFjobopts" long="PDFjobopts"
        value="file |<br/>XSTRETCH=82XfsXfile,fileEndStretch"
	four="Doc|inline"
	>
<#jobopts.para1>
<xOptPara/>
<#jobopts.para1a>
<xOptPara/>
<#jobopts.para2a>
<xOptPara/>
See also the
<xcmdopt opt="PDFjobopts" />
 option on page
<xnpage id="running.option.PDFjobopts" file="running" />
.
<xOptPara/>
<#jobopts.paraLast>
</xOption>

@------------------------
<xOption short="PDFbookmarks" long="PDFbookmarks"
        value="Default | All | None | int"
	four="Doc|inline"
	>
Set bookmark display in output PDF document.
<vidx id="PDF||bookmarks" />
<vidx id="bookmarks, PDF" />
<P>
See the <#osq><#nbhy>PDFbookmarks<#csq> command line option
on page
<xnpage id="running.option.PDFbookmarks" file="running" />
 for a description of the option values.

<xOptDefault>From template, if specified, otherwise Default</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFview" long="PDFview"
        value="Page | Default | Width"
	four="Doc|inline"
	>
Set PDF document view on opening.
<P>
See the <#osq><#nbhy>PDFview<#csq> command line option
on page
<xnpage id="running.option.PDFview" file="running" />
 for a description of the option values.

<xOptDefault>Page</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFthread" long="PDFthread"
        value="N | F | C"
	four="Doc|inline"
	>
Set PDF article threads.
<P>
See the <#osq><#nbhy>PDFthread<#csq> command line option
on page
<xnpage id="running.option.PDFthread" file="running" />
 for a description of the option values.

<xOptDefault>N</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFopenpage" long="PDFopenpage"
        value="int"
	four="Doc|inline"
	>
Set page to display on opening the PDF document.

<xOptDefault>1</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFpdests" long="PDFpdests"
        value="N | Y"
	four="Doc|inline"
	>
Create a named destination for every paragraph (slow).

<xOptDefault>N</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFtagged" long="PDFtagged"
        value="N | Y"
	four="Doc|inline"
	>
Create <#osq>Tagged<#csq> PDF (slow).

<xOptDefault>N</xOptDefault>
</xOption>

@------------------------
<xOption short="PDFpnames" long="PDFpnames"
        value="N | Y"
	four="Doc|inline"
	>
Include paragraph tags in bookmark text during Acrobat PostScript
file generation. Ignored unless the <on>PAfile</on>, <on>Opdf</on> or
<on>Opdfx</on> 
options are used.
</xOption>

