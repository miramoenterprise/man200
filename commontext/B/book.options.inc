@------------------------------------------------------------
@- book.options.inc
@------------------------------------------------------------
@--- FIXED
<@macro> bookopts.checkcode	{
	@if(@match({<#CodeName1>}, {Doc})) {
		<#def> book.options.codename	{<#xDoc>}
		}
	@else	{
		<#def> book.options.codename	{<#xMiramoXML>}
		}
}
@------------------------------------------------------------
<|bookopts.checkcode>
@------------------------------------------------------------

@------------------------
<xOptGroup
	shortTitle="Book options"
	longTitle="Book options"
	XRefID="<#CodeName1>.optgroup.book_options.start"
	indexOnly="0"
/>
@------------------------

@------------------------
<xOption short="Bfile" long="bookName" value="filename" >

Specify name of book file to be created. By convention<vidx
	id="books||creating" /> a book file name has the
suffix <#osq>.book<#csq>, but any valid filename may be used.

<xOptPara/>
The <on>Bfile</on> option cannot be used with
any of the following
<#book.options.codename>
options:
<on>Omif</on>, <on>Ofm</on>, <on>Oviewmif</on> and <on>Oviewfm</on>.

<xOptPara/>
Note that the first <#xChapter> code should be on the next line
immediately after a
code which used the <on>Bfile</on> option.
Any text and markup included between the
<#book.options.codename>
code and the first <#xChapter> code will
be discarded with a warning message.

<xOptPara/>
A fatal error occurs if no <#xChapter> code is included in the input (prior
to the next
<#book.options.codename>
code or to the end of file).

</xOption>

@------------------------
<xOptGroup
	shortTitle="Add chapters to pre-existing book"
	longTitle=""
	XRefID="<#CodeName1>.optgroup.Bappend"
	indexOnly="1"
/>
@------------------------

@------------------------
<xOption short="Bappend" long="Bappend" value="N | Y" >

N = create a new book file,
Y = add to existing book file created by Miramo.

<xOptDefault>N</xOptDefault>

</xOption>


@------------------------
<xOptGroup
	shortTitle="Book update mode"
	longTitle=""
	XRefID="<#CodeName1>.optgroup.Bupdate"
	indexOnly="1"
/>
@------------------------

@------------------------
<xOption short="Bupdate" long="Bupdate" value="Y | N | OQ | OS" >

Set book update mode.

<xvaluekeyhead first="3mm" second="14mm" newRow="1"
	col1="Value"
	col2="Description" />

<vk key="Y" >
Update book chapters and book file, starting with
closed book chapters.
</vk>

<vk key="N" >
No book update.
</vk>

<vk key="OQ" >
Same as <ov>Y</ov> above, except that all chapters in the
book are opened before updating, and all changes to the
chapters and book file after the update are unsaved.
</vk>

<vk key="OS" >
Same as <ov>OQ</ov> above, except that all changes to the
chapters in the book and the book file that result from the
update are saved.
</vk>



<xOptPara/>
See the -Bupdate command-line
option on page <xnpage id="running.option.Bupdate"
file="running" /> for more details.

<xOptDefault>Y</xOptDefault>

</xOption>

@------------------------
<xOptGroup
	shortTitle="Delete book and component files when finished"
	longTitle=""
	XRefID="<#CodeName1>.optgroup.delete_book"
	indexOnly="1"
/>
@------------------------

@------------------------
<xOption short="Bremove" long="deleteBook"
	value="N | Y"
	>

Remove (delete) a book file and its component documents
when finished processing. <on>Brm</on> is an alias for <on>Bremove</on>. See the
<xcmdopt opt="Bremove" />
 command line option, on page
<xnpage id="running.option.Bremove" file="running" />
.

<xOptDefault>N</xOptDefault>
(see page
<xnpage id="running.option.Bremove" file="running" />
).

</xOption>

<xOptGroup/>
