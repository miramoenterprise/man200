@------------------------
@- runaroundTextGap.inc
@------------------------
@--- FIXED

@-----------------------------
<@macro> runaroundTextGapDefs {
	<#def> tprefix			{}
	<#def> includeRA		1
	<#def> runaroundObjName		{<#CodeName1>}
	@-------------------------------------------
	@--- "(If `RA' option set to C or B)"
	@-------------------------------------------
	@- <#def> ifCondition1	{ ( if <#osq><FontRef
	@- 	fmt="F_FR">RA<FontRef
	@- 	fmt="F_FI"><#csq> option set to <FontRef
	@- 	fmt="F_FR">C<FontRef fmt="F_FI"> or <FontRef
	@- 	fmt="F_FR">B<FontRef fmt="F_FI"> )}
	<#def> ifCondition1	{ ( if <on>RA</on> is set to <ov>C</ov> or <ov>B</ov> )}
	@-------------------------------------------
	<#def> rtgd.effective	{The following runaround properties are effective only if the <pn>position</pn> property is set to <pv>runin</pv>.}

	@-------------------------------------------
        @if(<#CodeName1> = Tbl ) {
		<#def> tprefix		{f}
		<#def> runaroundObjName	{<#xTblFrame>}
		<#def> includeRA	0
		@-------------------------------------------
		<#def> rtgd.effective	{The following runaround properties are effective only if the <on>position</on> property is set to <ov>runin</ov>.}
		@-------------------------------------------
		@--- "(If <TblFrame> <on>P</on> property set to R)"
		@-------------------------------------------
		@- <#def> ifCondition1	{ ( if &lt;TblFrame> <#osq><FontRef
		@- 	fmt="F_FR">P<FontRef
		@- 	fmt="F_FI"><#csq> property set to <FontRef
		@- 	fmt="F_FR"><ov>R</ov><FontRef fmt="F_FI"> )}
		<#def> ifCondition1	{ ( if <on>P</on> is set to <ov>R</ov> )}
		}
        @if(<#CodeName1> = AFrame
		OR <#CodeName1> = ARFrame
		OR <#CodeName1> = ATextFrame
			) {
		<#def> includeRA	0
		@-------------------------------------------
		<#def> rtgd.effective	{The following runaround properties are effective only if the <on>position</on> property is set to <ov>runin</ov>.}
		@-------------------------------------------
		@--- "(If `P' property set to R)"
		@-------------------------------------------
		@- <#def> ifCondition1	{ ( if <#osq><FontRef
		@- 	fmt="F_FR">P<FontRef
		@- 	fmt="F_FI"><#csq> property set to <FontRef
		@- 	fmt="F_FR">R<FontRef fmt="F_FI"> )}
		<#def> ifCondition1	{ ( if <on>P</on> is set to <ov>R</ov> )}
		}
        @if(<#includeRA>) {
		@-----------------------------
		<propertyGroup
			shortTitle="Text runaround on/off"
			longTitle="Text runaround"
			XRefID="<#CodeName1>.optgroup.text_runaround_on_off"
			/>

		@-----------------------------
		<property short="RA" long="runAround"
        		value="N | B | C" >Text runaround type.
		<propertyValueKeyList first="3mm" second="16mm" newRow="1"
        		keyHeading="Value"
        		descriptionHeading="Description"
        		optvals="Y"
        		>
		<vk key="N" >No runaround</vk>
		<vk key="B" >Box runaround</vk>
		<vk key="C" >Contour runaround</vk>
		</propertyValueKeyList>

		<propertyDefault>N</propertyDefault>
		</property>
		}
}
@-----------------------------
<|runaroundTextGapDefs>
@-----------------------------




@-----------------------------
@- shortTitle="Text runaround gap <#ifCondition1>"
<propertyGroup
	shortTitle="Text runaround gap"
	longTitle="Text runaround gap"
	XRefID="<#CodeName1>.optgroup.text_runaround"
	@- indexOnly="1"
	><#rtgd.effective></propertyGroup>

@-----------------------------
<property short="RAgap" long="runAroundGap"
	value="dim"
	>
<pdp>
Specify the gap between the <#runaroundObjName> and
text flowing around it.
The value of <#fI>dim<#fnI> may be negative.
</pdp>


<propertyDefault>6pt</propertyDefault>
</property>
@-----------------------------


@-----------------------------
<property short="sideGap" long="sideGap"
	value="dim"
	>
<pdp>
Specify the side gap between the <#runaroundObjName> and
text flowing around it.
The value of <#fI>dim<#fnI> may be negative.
</pdp>


<propertyDefault>value of <on>runAroundGap</on></propertyDefault>
</property>
@-----------------------------


@-----------------------------
<property short="belowGap" long="belowGap"
	value="dim"
	>
<pdp>
Specify the gap below the <#runaroundObjName> and
text flowing around it.
The value of <#fI>dim<#fnI> may be negative.
</pdp>


<propertyDefault>value of <on>runAroundGap</on></propertyDefault>
</property>
@-----------------------------

