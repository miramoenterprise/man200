@--------------------------------------
@-- bb.tab.inc
@--------------------------------------
@--- FIXED
<#def> bb.tab.specialText	{}

@--------------------------------------
<@macro> bb.tab	{
	@if(@match({X-<#CodeName1>-Y}, {X-IX-Y})){
<#def> bb.tab.specialText	{ Effective only if
<on>ix-show</on> option is set to <ov>N</ov>.}
		}
}
@--------------------------------------
<|bb.tab>
@--------------------------------------
<@BBlock> {
&lt;tab/>
}{
Inserts tab character in definition text.
} {tab}
@--------------------------------------
