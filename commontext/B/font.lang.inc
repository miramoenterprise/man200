@----------------------------------------------------------------------
@- font.lang.inc
@----------------------------------------------------------------------
@--- FIXED
<xOption short="lang" long="textLanguage" value="key" four="Font|inline" rs="2" >

Language.
<vidx id="text||language" />
<vidx id="language||text" />
<shortx>
<xOptPara/>
<#fI>key<#fnI> may be any
one of the following:

<xOptPara rowOpts="ctm=2pt cbm=3pt" pgfOpts="ls=P li=5mm fi=5mm" />
<Include file=${CTEXT}/lang.keywords.inc />
<xOptPara/>
Hyphenation and spell checking is suppressed if the <on>lang</on> option is set
to <ov>None</ov> or <ov>NoLanguage</ov>.
<vidx id="hyphenation||suppressing" />

</xOption>
@----------------------------------------------------------------------
