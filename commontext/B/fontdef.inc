@-----------------------------------------------
@-- fontdef.inc
@-----------------------------------------------
@--- FIXED

<@Option> {ff} {string} {
Font family
}
@-----------------------------
<@Option> {fv} {string} {
Font variation
}
@-----------------------------
<@Option> {fw} {string} {
Font weight
}
@-----------------------------
<@Option> {fa} {string} {
Font angle. Examples of
<vidx id="angle||<#osq>angle<#csq> of font" />
font angle are: Regular, Italic, Oblique
}
@-----------------------------
<Comment> FM55
<@Option> lang key {
<vidx id="language||language of font" />
Language used for hyphenation and spell checking. <#fI>key<#fnI> may be:
<P fmt=P_Inline_Opt  nl=Y li=1cm fi=1cm >
<Include file= ${CTEXT}/lang.keywords.inc />
}
</Comment>
@------------------------------
<@Option> {fcolor} {string} {
Font color
<vidx id="color||text" />
}
@------------------------------
@- <@Option> {PSn} {string} {
@- PostScript font name
@- }
@------------------------------
<@Option> {p} {dim} {
Point size
}
@------------------------------
<@Option> {K} {Y | N} {
Enable pair kerning and ligatures
<vidx id="ligatures" />
<vidx id="kerning" />
<vidx id="pair kerning" />
}
@-----------------------------
<@Option> {Ks} {percent} {
Character spread, or tracking. Increase
or decrease spacing between characters.
Use a negative
value to reduce character spread.
Spread is expressed as a percentage of
the font's em space size.
(For examples of using the
<on>Ks</on> option see the
<#fI>Miramo User Guide<#fnI>, Chapter
<xparanumonly id="textpos.chapter.start" file="textpos" />
, pages
<xnpage id="textpos_character_spread.start" file="textpos" />
<xphyphen/>
<xnpage id="textpos_character_spread.end" file="textpos" />
.)

<vidx id="text||spread, tracking" />
<vidx id="tracking, in text" />
<vidx id="character spacing" />
<vidx id="spread, character" />
}

@-----------------------------
<@Option> {uline} {S | D |<br/>M | N} {
S<#nbsp>=<#nbsp>single (normal) underline,
D<#nbsp>=<#nbsp>double underline,
M<#nbsp>=<#nbsp>numeric (low) underline,
N<#nbsp>=<#nbsp>no underline.
<vidx id="underline||single" />
<vidx id="underline||low" />
<vidx id="underline||numeric" />
<vidx id="underline||double" />
}
@-----------------------------
<@Option> {o} {N | Y} {
Overline
<vidx id="overline (text)" />
}
@-----------------------------
<@Option> {t} {N | Y} {
Strike through
<vidx id="strike through (text)" />
}
@-----------------------------
<@Option> {d} {N | Y} {
Subscript
<vidx id="subscript" />
}
@-----------------------------
<@Option> {u} {N | Y} {
Superscript
<vidx id="superscript" />
}
@------------------------------
<@Option> {fc} {T | S |<br/>L | U} {
Capitalization. T = as typed, S = small caps,
L = lower case, U = uppercase
}
@------------------------------
<@Option> {C} {N | Y} {
Change bar
}
@-----------------------------
