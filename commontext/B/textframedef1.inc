@-----------------------------
@-- textframedef1.inc
@-----------------------------
@--- FIXED


@-----------------------------
<xOptGroup
	shortTitle="Text frame dimensions"
	longTitle="Text frame dimensions"
	XRefID="<#elementName>.propertyGroup.dimensions"
	/>
@-----------------------------


@-----------------------------
<xOption short="W" long="frameWidth"
        value="dim" >
Width of text frame.

<xOptDefault>40 mm, or inherited from preceding <#xTextFrameDef> or <#xNextTextFrameDef></xOptDefault>
</xOption>

@-----------------------------
<xOption short="H" long="frameHeight"
        value="dim" >
Height of text frame.

<xOptDefault>60 mm, or inherited from preceding <#xTextFrameDef> or <#xNextTextFrameDef></xOptDefault>
</xOption>

@-----------------------------
<xOptGroup
	shortTitle="Text frame location"
	longTitle="Text frame location"
	XRefID="<#elementName>.propertyGroup.location"
	/>
@-----------------------------

@-----------------------------
<xOption short="L" long="frameLeft"
        value="dim" >
Distance of left side of text frame from left side
of enclosing frame or page.

<xOptDefault>0, or inherited from preceding <#xTextFrameDef> or <#xNextTextFrameDef></xOptDefault>
</xOption>

@-----------------------------
<xOption short="T" long="frameTop"
        value="dim" >
Distance of top of text frame from top of enclosing frame or page.

<xOptDefault>0, or inherited from preceding <#xTextFrameDef> or <#xNextTextFrameDef></xOptDefault>
</xOption>

@-----------------------------
<xOptGroup
	shortTitle="Text frame graphics properties"
	longTitle=""
	XRefID="<#elementName>.propertyGroup.graphics_properties"
/>
@-----------------------------

@-----------------------------
<xOption short="pen" long="borderPen"
        value="0 - 15" >
Sixteen pen patterns for text frame 
border are available,
from <ov>0</ov> (black) to <ov>15</ov> (none). See 
Appendix <xparanumonly id="A_penAndFillPats.chapter.start"
        file="A_penAndFillPats" />, on page
<xnpage
        id="A_penAndFillPats.chapter.start"
        file="A_penAndFillPats" />.
</xOption>

@-----------------------------
<xOption short="pw" long="penWidth"
        value="dim" >
Pen width. 
Note that borders thicken equally about their centers.<br/>
Default units are points
</xOption>

@-----------------------------
<@include> ${CTEXT}/pstyle.inc

@-----------------------------
<xOption short="fill" long="frameFill"
        value="0 - 15" >
Fill pattern. See <on>pen</on> above.
A fill value of <ov>15</ov> represents a fill of <#osq>None<#csq>, 
i.e. transparent.
</xOption>

@-----------------------------
<xOption short="color" long="color"
        value="name" >
Pen and fill color
<vidx id="color||in/around text frame (<#xNextTextFrameDef>)" />
</xOption>


@-----------------------------
<xOption short="a" long="angle"
        value="0 - 360" >
Angle of rotation (counterclockwise) about the center of the
text frame. In this case, the dimension options LTWH
relate to the unrotated text frame.
</xOption>
