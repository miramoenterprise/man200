@------------------------------------------------------------
@-  mpr.pageNumbering.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Page numbering"
	longTitle="Page numbering"
	dest="<#elementName>.propertyGroup.pageNumbering"
	>
For setting changing page numbering style and sequencing
</propertyGroup>


@------------------------------------------------------------
<property
	@- short="N"
	long="pageNumberStart"
	value="continue | restart | int" >

<pdp>
Specifies how to compute sequential page number
values.
</pdp>

<propertyValueKeyList
>

<vk key="continue" >Continue the current numbering sequence.
</vk>

<vk key="restart" >Reset the current numbering sequence to 1.
</vk>

<vk key="int" >Reset the current numbering sequence to int.
</vk>

</propertyValueKeyList>
<propertyDefault>continue</propertyDefault>
</property>

@- <@system> {mmpp -Mfile %control%/subControl.mmp %CTEXT%/numberStyleValues.inc > %mmtmp%/numberStyleValues.inc }
@- <#fdef> numberStyleValues	${mmtmp}/numberStyleValues.inc

@------------------------------------------------------------
<property
	@- short="Nstyle"
	long="pageNumberStyle"
	value="key" >

<pdp>
Sets page number style.
</pdp>

@------------------------------------------------------------
@- Defined in ${CTEXT}/numberStyleValues.inc
@------------------------------------------------------------
<#numberStyleValues>
@------------------------------------------------------------



<propertyDefault>westernArabic</propertyDefault>
</property>

<@skipStart>
@------------------------------------------------------------
<propertyValueKeyList
	keyWidth="22mm"
	>
@------------------------------------------------------------
<vk key="westernArabic" >Western arabic numerals: 1, 2, 3, ...
</vk>
<vk key="uppercaseAlpha" >Uppercase ASCII letter sequence: A, B, C, ...
</vk>
<vk key="lowercaseAlpha" >Lowercase ASCII letter sequence: a, b, c, ...
</vk>
<vk key="uppercaseRoman" >Uppercase roman numerals: I, II, III, ...
</vk>
<vk key="lowercaseRoman" >Lowercase roman numerals: i, ii, iii, ...
</vk>
@------------------------------------------------------------
@- See the following for a good explanation:
@------------------------------------------------------------
@- http://stackoverflow.com/questions/1676460/in-unicode-why-are-there-two-representations-for-the-arabic-digits
@------------------------------------------------------------
<vk key="indicArabic" >Indic Arabic numerals: 
<Font ff="Arabic Typesetting" textSize="9pt" >
&#x0661;, 
&#x0662;, 
&#x0663;, 
&#x0664;, 
&#x0665;, 
&#x0666; ...
</Font>
</vk>
@------------------------------------------------------------
<vk key="Persian | Urdu" >Persian numerals: 
<Font ff="Arabic Typesetting" textSize="9pt" >
&#x06F1;, 
&#x06F2;, 
&#x06F3;, 
&#x06F4;, 
&#x06F5;, 
&#x06F6; ...
</Font>
</vk>
@- <@skipStart>
@------------------------------------------------------------
<vk key="Tamil" >Tamil numerals
<Font ff="Latha" textSize="8pt" >
&#x0BE6;, 
&#x0BE7;, 
&#x0BE8;, 
&#x0BE9;, 
&#x0BEA;, 
&#x0BEB;, 
&#x0BEC;, 
&#x0BED;, 
&#x0BEE;, 
&#x0BEF;, 
&#x0BF0;, ...
&#x0BF1;, ...
&#x0BF2;
</Font>
</vk>
@- <@skipEnd>
@------------------------------------------------------------
<vk key="Thai" >Thai numerals
<Font ff="Browallia New" textSize="14pt" >
&#x0E50;, 
&#x0E51;, 
&#x0E52;, 
&#x0E53;, 
&#x0E54;, 
&#x0E55;, 
&#x0E56;, 
&#x0E57;, 
&#x0E58;, 
&#x0E59; ...
</Font>
</vk>
</propertyValueKeyList>

@------------------------------------------------------------

@- U+0BE6 	TAMIL DIGIT ZERO (U+0BE6) 	? 	code2000
@- U+0BE7 	TAMIL DIGIT ONE (U+0BE7) 	? 	arial_unicode_ms
@- U+0BE8 	TAMIL DIGIT TWO (U+0BE8) 	? 	arial_unicode_ms
@- U+0BE9 	TAMIL DIGIT THREE (U+0BE9) 	? 	arial_unicode_ms
@- U+0BEA 	TAMIL DIGIT FOUR (U+0BEA) 	? 	arial_unicode_ms
@- U+0BEB 	TAMIL DIGIT FIVE (U+0BEB) 	? 	arial_unicode_ms
@- U+0BEC 	TAMIL DIGIT SIX (U+0BEC) 	? 	arial_unicode_ms
@- U+0BED 	TAMIL DIGIT SEVEN (U+0BED) 	? 	arial_unicode_ms
@- U+0BEE 	TAMIL DIGIT EIGHT (U+0BEE) 	? 	arial_unicode_ms
@- U+0BEF 	TAMIL DIGIT NINE (U+0BEF) 	? 	arial_unicode_ms
@- U+0BF0 	TAMIL NUMBER TEN (U+0BF0) 	? 	arial_unicode_ms
@- U+0BF1 	TAMIL NUMBER ONE HUNDRED (U+0BF1) 	? 	arial_unicode_ms
@- U+0BF2 	TAMIL NUMBER ONE THOUSAND (U+0BF2) 	? 	arial_unicode_ms

<@skipEnd>
