@---------------------------------------------
@-- p.tablecell.inc
@---------------------------------------------
@--- FIXED

<xOption short="clm" long="cellMarginLeft"
	value="dim" >
<vidx id="cell||margins" />
<vidx id="table||cell margins" />
Cell left margin
applied to a table cell when
this format is specified for the first
paragraph in a table cell.
The cell margin overrides the default
cell margin specified for the
table containing this paragraph.
<xOptPara/>
Default units are points. Text will not cross
the left side of the cell when a negative dimension is given.
<xOptPara/>
When the first cell margin option (clm, ctm, crm or cbm) is encountered
on a <#xParaDef> code, all other cell margin values for the paragraph 
are set to a default value of 6pt.
</xOption>

@-----------------------------
<xOption short="ctm" long="cellMarginTop"
	value="dim" >
Cell top margin
applied to a table cell when this format
is specified for the first paragraph in a table cell.
See <on>clm</on> above.
<xOptPara/>
Default units are points. A negative top margin
dimension may cause text to move above the top of the cell.

<xOptDefault>6pt</xOptDefault>
</xOption>

@-----------------------------
<xOption short="crm" long="cellMarginRight"
	value="dim" >
Cell right margin
applied to a table cell when this format
is specified for the first paragraph in a table cell.
See <on>clm</on> above.
<xOptPara/>
Default units are points. Text will not cross
the right side of the cell when a negative dimension is given.

<xOptDefault>6pt</xOptDefault>
</xOption>

@-----------------------------
<xOption short="crm" long="cellMarginBottom"
	value="dim" >
Cell bottom margin
applied to a table cell when this format
is specified for the first paragraph in a table cell.
See <on>clm</on> above.
<xOptPara/>
Default units are points. A negative bottom margin
dimension may cause text to move below the bottom of the cell.

<xOptDefault>6pt</xOptDefault>
</xOption>

@-----------------------------
<xOption short="cm" long="cellMarginAll"
	value="dim" >
Set left, top, right and bottom cell margins
when this paragraph format is applied
to the first
paragraph in a table cell.
The cell margins override the default
cell margins specified for the table containing
this paragraph.

<xOptPara/>
Default units are points. Text will not cross
the left or right sides of the cell when a negative 
dimension is given, but it may cross the top
or bottom edges of the cell.
</xOption>

@-----------------------------
<xOption short="Ca" long="cellVerticalAlign"
	value="T | M | B" >
<MkDest fmt=X id=vertalign/>
<vidx id="cell||vertical alignment of text in" />
<vidx id="vertical text alignment||in table cells" />
<vidx id="cell||vertical alignment of text in" />
<vidx id="alignment||table cell, vertical" />
<vidx id="table cell||vertical alignment of text in" />
<vidx id="table||vertical alignment of text in cells" />
<vidx id="centering text||in table cells, vertically (Ca=M)" />
Cell vertical alignment of paragraph
contents within cell: T<#nbsp>=<#nbsp>top of cell,
M<#nbsp>=<#nbsp>middle of cell,
B<#nbsp>=<#nbsp>bottom of cell. 
The orientation for <on>T</on> (top) and <on>B</on> (bottom) changes with cell
rotation. For example, if <#xCell> is followed by the option a=90,
changing <#osq>Ca=T<#csq> to <#osq>Ca=B<#csq> shifts the paragraph contents leftwards.
</xOption>
