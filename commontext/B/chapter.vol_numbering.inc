@------------------------------------------------------------
@- chapter.vol_numbering.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@include> ${CTEXT}/def.template.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle='Volume numbering'
	longTitle='Volume numbering options'
	dest="<#elementName>.propertyGroup.volume_numbering"
>The following options set up the <#xvolnum> building block.

Use the <#xvolnum> building block to add
volume numbers to running headers
and footers (see <#xPageNum> on page <xnpage
	id="PageNum.code.start"
	file="inline" />), paragraph numbering
(see <#xAutoNum> on page <xnpage
	id="AutoNum.code.start" 
	file="inline" />), cross references
(see <#xXRefFmtDef> on page <xnpage
	id="XRefFmtDef.code.start"
	file="fdefs" />), and tables of contents and indexes (see <fI>
<xparatextonly id="books.chapter.start" file="books" />
</fI> on page <xnpage
	id="books.chapter.start"
	file="books" /> in the <#URef>).
<vidx id="building blocks||&lt;volnum/>" />
</propertyGroup>
@---------------------------------



@------------------------------------------------------------
<property
	short="VN"
	long="volNum"
	value="Cont | Same | File"
	>

<pdp>
How to compute volume number.
</pdp>

<propertyValueKeyList
	>
<vk key="Cont" >Continue from previous book component.<contAliases/></vk>
<vk key="Same" >Same as previous book component.</vk>
<vk key="File" >Read from file.</vk>
</propertyValueKeyList>


<pdp>
If the <on>VN</on> option is used, the
<on>VNstyle</on>,
<on>VNstring</on>
and
<on>VNstart</on> options are ignored.
</pdp>


<pdp>
The default is determined
by the
<on>VNstyle</on>,
<on>VNstring</on>
and
<on>VNstart</on> options.
</pdp>


<pdp>
To restart numbering from 1, use the <on>VNstart</on> option.
</pdp>

</property>

@------------------------------------------------------------
<property short="VNstyle"
	long="volNumStyle"
	value="A | UA | LA | UR | LR | C"
	>

<pdp>
Sets volume number style.
</pdp>

<propertyValueKeyList
	>
<vk key="A" >Arabic.</vk>
<vk key="UA" >Uppercase alpha.</vk>
<vk key="LA" >Lowercase alpha.</vk>
<vk key="UR" >Uppercase roman.</vk>
<vk key="LR" >Lowercase roman.</vk>
<vk key="C" >Custom.</vk>
</propertyValueKeyList>


<pdp>
If <on>VNstyle</on> is set to <ov>C</ov> the value of
<on>VNstring</on> is used for the <#xvolnum> building
block.
If <on>VNstyle</on> is set to
<ov>A</ov>, <ov>UA</ov>, <ov>LA</ov>, <ov>UR</ov> or <ov>LR</ov>,
the value of <on>VNstart</on> is used
for the <#xvolnum> building block.
</pdp>

<pdp>
<on>VNstyle</on> is ignored if the <on>VN</on> option is used.
</pdp>

<propertyDefault>A</propertyDefault>

</property>


@------------------------------------------------------------
<property short="VNstring"
	long="volNumString"
	value="string"
	>

<pdp>
Set custom volume number string.
</pdp>

<pdp>
<on>VNstring</on> is ignored if the <on>VN</on> option is used,
Ignored unless <on>VNstyle</on> is set to <ov>C</ov>.
</pdp>

</property>


@------------------------------------------------------------
<property
	short="VNstart"
	long="volNumStart"
	value="int"
	>
<pdp>
Set volume number.
<br/>
<on>VNstart</on> is ignored if the <on>VN</on> option is used,
or if <on>VNstyle</on> is set to <ov>C</ov>.
</pdp>

<propertyDefault>1</propertyDefault>

</property>
@------------------------------------------------------------
