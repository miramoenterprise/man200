@---------------------------------------------
@-- p.rules.inc
@---------------------------------------------

@-----------------------------
<propertyGroup
	shortTitle="Reference frames above and below"
	longTitle="Reference frames above and below"
	XRefID="P.optgroup.reference frames"
/>


@-----------------------------
<property
	short=""
	long="frameAbove"
	value="name" >

<pdp>
Top separator. <#fI>name<#fnI> is
the name of a Reference page graphics frame
in a MFD file saved from FrameMaker, or defined using
the <#xFrame> element within
a <#xPageDef> element whose <on>type</on> property is
set to <ov>reference</ov>.
<vidx id="paragraph||rules above or below" />
<vidx id="paragraph||graphics above or below" />
<vidx id="rules||above paragraphs" />
<vidx id="graphics||above/below every paragraph" />
</pdp>

<propertyDefault>None</propertyDefault>
</property>

@-----------------------------
<property
	short=""
	long="frameBelow"
	value="name"
	>

<pdp>
Bottom separator. <#fI>name<#fnI> is
the name of a Reference page graphics frame
in a MFD file saved from FrameMaker, or defined using
the <#xFrame> element within
a <#xPageDef> element whose <on>type</on> property is
set to <ov>reference</ov>.
<vidx id="rules||below paragraphs" />
<vidx id="graphics||above/below every paragraph" />
<vidx id="reference frames||importing from Frame documents" />
<vidx id="importing graphics||from Frame documents" />
</pdp>

<pdp>
See <on>frameAbove</on> property above.
</pdp>

</property>
@-----------------------------
