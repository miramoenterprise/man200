@----------------------------------------------------
@- jobopts.txt
@----------------------------------------------------
@--- FIXED
<#def> jobopts.para1 {
Change the .joboptions file used for PDF output for a job.
<#fI>file<#fnI> is name of file containing job options
settings.
If <#fI>file<#fnI> contains one or more spaces,
<#fI>file<#fnI> must be surrounded by double
quotes, "<#fI>file name<#fnI>".
}

<#def> jobopts.para1a {
On Windows a PDF joboptions file must always have a <#osqb>.joboptions<#csqb> suffix.
If <#fI>file<#fnI> does not have a <#osqb>.joboptions<#csqb> suffix,
the suffix is silently assumed. For example, if the <#fI>file<#fnI> value of the
<on>PDFjobopts</on> option is set to <#osqb>foo<#csqb> this is taken
as a shorthand for <#osqb>foo.joboptions<#csqb>.
}

<#def> jobopts.para1b {
The <on>PDFjobopts</on> option is only required when 
the PDF .joboptions settings needed for a particular job
are different to the default .joboptions settings used for all jobs.
The default .joboptions file for all jobs is set via the <mmVisor> Control
tab.
@- is defined by
@- the value of the
@- defaultJoboptions attribute in the mmServer.xml configuration
@- file in the %MM_HOME%\config folder.
}

<#def> jobopts.para2 {
If <#fI>file<#fnI> is immediately preceded by
a plus sign, <#fI>file<#fnI> refers to a file located in
the default PDF <#osq>Settings<#csq> directory:
<br/>
<tab/><tab/>\$FMHOME/fminit/pdfsettings
<br/>
on Unix and version
dependent on Windows&emdash;check <#osq>Save<#nbsp>As&hellip;<#csq>.
}

<#def> jobopts.para2a {
In the second form, <#fI>file<#fnI>,<#fI>file<#fnI>,
the <#fI>file<#fnI> following the comma separator is the
name of file containing job options
settings to use when creating separate PDF output for each chapter
in a book, if different settings are required to the settings
used when outputting the book as a single PDF file.
This form is effective only if a
<xcmdopt opt="Pmulti" />
 option is used (see pages <xnpage
	id="running.option.Pmulti" file="running" > and <xnpage
	id="MiramoXML.option.Pmulti" file="xmlcodes" >).
}
<#def> jobopts.paraLast {
For further notes and guidance relating to .joboptions files
see section <xsectionXRef
	id="section.creating_adobe_acrobat_files"
	file="running" /> on page <xnpage
	id="section.creating_adobe_acrobat_files"
        file="running" />.
}

@- <#def> jobopts.para2b {
@- The <#osq>+<#csq> style of PDF joboptions file referencing
@- must be used if
@- the <on>PDFmode</on> option is set to <ov>saveAs</ov>.
@- }

@- <#def> jobopts.para3 {
@- If the argument/value string, e.g. +<#fI>file<#fnI> or
@- <#fI>file<#fnI>,+<#fI>file<#fnI>
@- contains one or more spaces,
@- the entire string must be surrounded by double
@- quotes, e.g. "+<#fI>file name<#fnI>".
@- }

