@------------------------------------------------------------
@-- tf.flowoptions3.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Feathering"
	longTitle="Feathering options"
	dest="<#elementName>.optgroup.feathering"
	condition="fmcGuide"
/>

@------------------------------------------------------------
<property short="f" long="feather" value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Feather text within this text flow.
<vidx id="feathering" />
<vidx id="text frames||feathering in" />
@- <xsideheads2 xref="feathering" />
</pdp>


<propertyDefault>N</propertyDefault>

</property>

@------------------------------------------------------------
<property short="maxp" long="maxPgfPad" value="dim"
	condition="fmcGuide"
	>

<pdp>
Maximum inter-paragraph padding
when feathering is switched on.
</pdp>

<propertyDefault>6pt</propertyDefault>
</property>

@------------------------------------------------------------
<property short="maxl" long="maxLinePad" value="dim"
	condition="fmcGuide"
	>

<pdp>
Maximum inter-line padding
when feathering is switched on.
</pdp>

<propertyDefault>2pt</propertyDefault>
</property>
@------------------------------------------------------------
