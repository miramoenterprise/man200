@------------------------------------------------------------
@-- p.basicNew.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Basic properties"
	longTitle="Basic properties"
	dest="cell.optgroup.basic"
	></propertyGroup>

@------------------------------------------------------------
<property short="align" long="textAlign" value="start | end | center | justify" >

<pdp>
Text alignment and justification.
<vidx id="paragraph||justification in" />
<vidx id="justification||in paragraphs" />
<vidx id="text alignment||paragraph" />
<vidx id="alignment||paragraph" />
<vidx id="paragraph||centering text (A=C)" />
<vidx id="paragraph||alignment" />
<vidx id="centering text||in paragraphs (A=C)" />
</pdp>

<propertyValueKeyList keyWidth="20mm"
	keyHeading="Value"
	descriptionHeading="Description"
>

<vk key="start | left" >
Left-align text lines. Ragged right line ends.
<ov>left</ov> may be used as an alias for <ov>start</ov>.
In the case of right-to-left
writing <ov>left</ov> and <ov>start</ov> produce
right-aligned text lines.
</vk>

<vk key="end | right" >
Right-align text lines. Ragged left line ends.
<ov>right</ov> may be used as an alias for <ov>end</ov>.
In the case of right-to-left
writing <ov>right</ov> and <ov>end</ov> produce
left-aligned text lines.
</vk>

<vk key="center" >
Align text lines at the center.
Ragged line starts and ends.
</vk>

<vk key="justify | both" >
Align text lines at start and end.
Text justification at both ends.
<ov>both</ov> may be used as an alias for <ov>justify</ov>.
</vk>

</propertyValueKeyList>

</property>

@------------------------------------------------------------
<property
	short="fi"
	@- long="firstIndent"
	long="firstLineIndent"
	value="dim"
	>
@- <propertyOld short="A" long="justify" value="L | R | C | LR" />
<docNote>New property name</docNote>

<pdp>
First line indent.
<vidx id="paragraph||first indent (fi)" />
<vidx id="paragraph||indentation" />
</pdp>

<pdp>
Indent of the first line of paragraph text from the start side of
a text column or from the margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="li"
	long2="leftIndent"
	long="startIndent"
	value="dim"
	>
@- <propertyOld short="li" long="leftIndent" value="dim" four="P|inline" />

<pdp>
Start indent (excluding the first line
of the paragraph).
<vidx id="paragraph||left indent (li)" />
</pdp>

<pdp>
Distance of the
second and succeeding lines of text from
the start side of a text column or from the start
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="ri"
	long2="rightIndent"
	long="endIndent"
	value="dim"
	>
@- <propertyOld short="ri" long="rightIndent" value="dim" four="P|inline" />

<pdp>
End indent.
<vidx id="paragraph||right indent (ri)" />
</pdp>

<pdp>
Distance of lines of text from
the end side of a text column or from the end
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="sa"
	long="spaceAbove"
	value="dim"
	>
@- <propertyOld short="sa" long="spaceAbove" value="dim" four="P|inline" />

<pdp>
Space above paragraph.
<vidx id="paragraph||space above (sa)" />
<vidx id="space||above paragraph (sa)" />
</pdp>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space before
the preceding paragraph, not the sum of the two.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="sb"
	long="spaceBelow"
	value="dim"
	>
@- <propertyOld short="sb" long="spaceBelow" value="dim" four="P|inline" />

<pdp>
Space below.
<vidx id="paragraph||space below (sb)" />
<vidx id="space||below paragraph (sb)" />

See note above.
</pdp>

</property>


@------------------------------------------------------------
<property short="un" long="pgfUseNext" value="N | Y" 
	condition="fmcGuide"
	>

<pdp>
Use next paragraph.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault v="N" />
</property>

@------------------------------------------------------------
<property short="np" long="pgfNext" value="name" 
	condition="fmcGuide"
	>

<pdp>
Next paragraph tag.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault>Null</propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Interline leading and line spacing mode"
	longTitle="Interline leading and line spacing mode"
	dest="MkDest.optgroup.line_spacing"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="l" long="leading" value="dim" 
	>

<pdp>
Leading. Default units are points.
<vidx id="paragraph||leading (l)" />
<vidx id="paragraph||line spacing (l)" />
<vidx id="leading, space between lines" />
<vidx id="line spacing" />
</pdp>

<pdp>
Leading is the space <#fI>between<#fnI> lines
within a paragraph. Line spacing
(<#fI>baseline<#fnI> to <#fI>baseline<#fnI>)
is leading plus point size of the text.
@- (see Figure
@- <xparanumonly id="fig.inter_paragraph_spacing" file="textpos" />
@-  on page
@- <xnpage id="fig.inter_paragraph_spacing" file="textpos" />
@-  in the <#UGuide>).
</pdp>

</property>

@------------------------------------------------------------
<property
	short="ls"
	long="lineSpacing"
	value="fixed | proportional"
	>

<pdp>
Line spacing mode.
<vidx id="paragraph||line spacing mode (fixed)" />
<vidx id="paragraph||line spacing mode (leading)" />
<vidx id="line spacing in paragraph text||leading" />
<vidx id="line spacing in paragraph text||floating" />
<vidx id="line spacing in paragraph text||proportional" />
<vidx id="line spacing in paragraph text||fixed" />
<vidx id="anchored frames||inline" />
</pdp>

<pdp>
Setting <on>lineSpacing</on> to <ov>fixed</ov> results in
no adjustment for extra large characters, superscripts,
inline anchored frames, etc.
When <on>lineSpacing</on> is set to <ov>proportional</ov> the
inter-line spacing is adjusted to prevent
contact between text lines in cases where
the text contains either larger than normal
characters or inline anchored frames.
</pdp>
</property>

@------------------------------------------------------------
