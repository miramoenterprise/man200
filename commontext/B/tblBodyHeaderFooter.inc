@------------------------------------------------------------
<!-- File = cellFillColor.inc  (Start) -->
@------------------------------------------------------------

@------------------------------------------------------------
@- Used in:
@- 
@- <TblDef>
@- <Tbl>
@- <Row>
@- <Cell>
@- 
@- <TblFrame>
@------------------------------------------------------------

@------------------------------------------------------------
<#ifndef> headerFooterOrBody	{Body}
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> cellFillColorGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Cell background"
			longTitle="Cell background"
			dest="<#elementName>.propertyGroup.cell_fillAndcolor"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Cell background (default for all cells in row)"
			longTitle="Cell background (default for all cells in row)"
			dest="<#elementName>.propertyGroup.cell_fillAndcolor"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl
			OR <#elementName> = TblDef
			OR <#elementName> = TblFrame ) {
		<propertyGroup
			shortTitle="Cell background (default for all cells in table)"
			longTitle="Cell background (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_fillAndcolor"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellFillColorGroupHead>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillColor" long="fillColor" value="color" >

<pdp>
Background color.
<vidx id="color||in table cells" />
</pdp>


<pdp>
<on>fillOpacity</on> must
be set to a value other than
<ov>0</ov> (transparent) for
the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property short="fillOpacity" long="fillOpacity" value="0 | 100" >

<pdp>
Fill opacity.
</pdp>

<pdp
	condition="skip"
	>
(See Appendix
<xparanumonly id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />, page
<xnpage id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />,
for a description of fill patterns.)
<vidx id="fill||in table cells" />
</pdp>

<propertyDefault>0</propertyDefault>

</property>

@------------------------------------------------------------
<property short="fillTint" long="fillTint" value="percent" >

<pdp>
Fill tint.
</pdp>

<propertyDefault>100%</propertyDefault>

</property>

@------------------------------------------------------------
<#def> headerFooterOrBody	{Body}
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = cellFillColor.inc  (End) -->
@------------------------------------------------------------
