@------------------------------------------------------------
@- chapter.footnote_numbering.inc
@------------------------------------------------------------

<@include> ${CTEXT}/def.template.inc

@------------------------------------------------------------
<propertyGroup
	shortTitle="Footnote (FNote) numbering"
	longTitle="Footnote numbering options"
	dest="<#elementName>.propertyGroup.footnote_numbering"
	idxstart="footnotes||numbering"
	/>


@------------------------------------------------------------
<property short="FN" long="FNoteNum" 
	four="Chapter|inline"
	value="PerPage | PerFlow | Cont | File" >

<pdp>
Specifies how to compute sequential footnote number
values.
</pdp>

<propertyValueKeyList
>

<vk key="PerPage" >Restart footnote numbering on every page,
using the value assigned to the
<on>FNstart</on> option, or 1 if the <on>FNstart</on> option is
not set.</vk>

<vk key="PerFlow" >Restart footnote numbering at the
beginning of every flow,
using the value assigned to the
<on>FNstart</on> option, or 1 if the <on>FNstart</on> option is
not set.</vk>

<vk key="Cont" >Continue from previous chapter.
Both the numeric value and numbering style are
derived from the previous chapter
in the book. The <on>FNstart</on> option is ignored
and footnote numbering styles set up within the
file (template) are ignored.<contAliases/></vk>

<vk key="File" >Read value and numbering style from file 
(template). The <on>FNstart</on> and <on>FNstyle</on> options are ignored.</vk>

</propertyValueKeyList>

<propertyDefault>PerPage</propertyDefault>
</property>

@------------------------------------------------------------
<property short="FNstyle" long="FNoteStyle" 
	four="Chapter|inline"
	value="A | UA | LA | UR | LR | C" >

<pdp>
Sets footnote number style.
If <on>FNstyle</on> is set to <ov>C</ov>,
characters from the string value
of <on>FNstring</on> option (if set, see below)
are used for custom footnote
numbering.
If <on>FNStyle</on> is set to
<ov>A</ov>,
<ov>UA</ov>,
<ov>LA</ov>,
<ov>UR</ov>
or
<ov>LR</ov>,
the (integer) value of the <on>FNstart</on> option
(see below)
is used.
</pdp>

<propertyValueKeyList
	>
<vk key="A" >Arabic.</vk>
<vk key="UA" >Uppercase alpha.</vk>
<vk key="LA" >Lowercase alpha.</vk>
<vk key="UR" >Uppercase roman.</vk>
<vk key="LR" >Lowercase roman.</vk>
<vk key="C" >Custom.</vk>
</propertyValueKeyList>

<pdp>
The <on>FNstyle</on> option is ignored if the <on>FN</on> option
is set to <ov>Cont</ov> or <ov>File</ov>.
</pdp>

<propertyDefault>A</propertyDefault>

</property>

@------------------------------------------------------------
<property short="FNstart" long="FNoteStart" 
	four="Chapter|inline"
	value="int" >

<pdp>
Specifies a fixed numeric value, <fI>int</fI>,
for the footnote starting number or, if <on>FNstyle</on> is
set to <ov>C</ov>, specifies the character offset of the first
character to be used from the custom footnote number
string.
<br/>
The <on>FNstart</on> option is ignored if the <on>FN</on> option is
set to <ov>Cont</ov> or <ov>File</ov>.
(See Notes
<xparanumonly id="note.custom_footnote_string" file="inline" />
 and
<xparanumonly id="note.footnote_start_number" file="inline" />
, starting on page
<xnpage id="note.custom_footnote_string" file="inline" />
).
</pdp>

<propertyDefault>1</propertyDefault>

</property>


@------------------------------------------------------------
<property short="FNstring" long="FNoteSymbols" 
	four="Chapter|inline"
	value="string" >

<pdp>
<fI>string</fI> is an array of characters
to be used for a custom footnote numbering sequence (see Note
<xparanumonly id="note.custom_footnote_string" file="inline" />
 on page
<xnpage id="note.custom_footnote_string" file="inline" />
).
</pdp>


<pdp>
The <on>FNstring</on> option is effective only
<on>FNstyle</on> option is set to <ov>C</ov> (custom) and
the <on>FN</on> option is set to PerPage or PerFlow.
</pdp>

</property>

@------------------------------------------------------------
