@------------------------------------------------------
@- frameOptions.inc
@------------------------------------------------------
@--- FIXED

@-----------------------------
<@include> ${CTEXT}/frameDimensionsWH.inc
@-----------------------------

@------------------------
<xOptGroup
	shortTitle="Frame offset (relative to enclosing graphic object)"
	longTitle="Frame offset"
	XRefID="<#CodeName1>.optgroup.frame_offset"
	/>


@-----------------------------
<xOption short="T" long="frameTop"
	value="dim"
	>
Distance of top of frame from top of enclosing
anchored frame or frame.
</xOption>

@-----------------------------
<xOption short="L" long="frameLeft"
	value="dim"
	>
Distance of left side of frame from left side
of enclosing anchored frame or frame.
</xOption>

@-----------------------------
@- <@include> ${CTEXT}/graphicBorder.inc
@- <@include> ${CTEXT}/pstyle.inc
@- <@include> ${CTEXT}/graphicColorAndFill.inc
@-----------------------------
<@include> ${CTEXT}/frameBorderAndFill.inc
<@include> ${CTEXT}/pstyle.inc
<@include> ${CTEXT}/frameBorderAndFillColor.inc


