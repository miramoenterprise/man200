@------------------------------------------------------------
@-- configFile.inc
@------------------------------------------------------------
@--- FIXED

@------------------------------------------------------------
<propertyGroup
	shortTitle="Specify PDF bookmark hierarchy"
	longTitle=""
	dest="<#elementName>.optgroup.pdfConfig"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="config"
	long="configFile"
	value="filename"
	>

<pdp>
<fI>file</fI> is name of a configuration file
specifying Acrobat bookmark levels.
</pdp>

<pdp>
See the section entitled
<xsectionXRef id="config_bookmarks" file="running" />
 in <#osq>Running Miramo<#csq>, on page
<xnpage id="config_bookmarks" file="running" />,
for a description of how to set up a bookmark
configuration file.
</pdp>

<propertyDefault>from the command line, if the <#osq>&#x2011;config<#csq>
command line option is used (see page
<xnpage id="running.option.config" file="running" />
).</propertyDefault>
</property>
@------------------------------------------------------------
