@--------------------------------------
@-- bgvardef.inc
@--------------------------------------
@--- FIXED

<xOptPara/>
If the <on>select</on> option, shown in the second example
above, is set to last, then the
&lt;paratext ... > building block expands
to the last paragraph on the page or, if
there is no such paragraph, to the next following
paragraph, having format <#fI>name<#fnI>.

<xOptPara/>
If the
&lt;paratext ... > <on>fmt</on> option contains more
than one <#fI>name<#fnI> value, then they are treated as
logical OR values.

