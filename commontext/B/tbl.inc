@------------------------------------------------------------
<!-- File = tbl.inc  (Start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Basic properties"
	longTitle="Basic properties"
	dest="optgroup.Tbl.basic_properties.start"
	/>


@------------------------------------------------------------
<property
	short="li"
	long2="leftIndent"
	long="startIndent"
	value="dim"
	>

<pdp>
Start indent. Or right indent in the case of right-to-left text.
</pdp>

<pdp>
Ignored if the <on>align</on> property is set to <ov>end</ov> (see below).
</pdp>

</property>

@------------------------------------------------------------
<property
	short="ri"
	long2="rightIndent"
	long="endIndent"
	value="dim"
	>

<pdp>
Right indent. Or left indent in the case of right-to-left text.
</pdp>

<pdp>
Ignored if the <on>align</on> property is set to <ov>start</ov> (see below).
</pdp>

</property>

@------------------------------------------------------------
<property short="sa" long="spaceAbove" value="dim" >

<pdp>
Space above.
</pdp>

</property>

@------------------------------------------------------------
<property short="sb" long="spaceBelow" value="dim" >

<pdp>
Space below.
</pdp>

</property>


@- <property short="A" long="tblHorizAlign" value="L | C | R" >
<property short="A" long="align" value="left | center | right" >

<pdp>
Horizontal alignment of table within text column.
<vidx id="table||horizontal placement" />
<vidx id="table||centering" />
<vidx id="table||left aligning" />
<vidx id="table||right aligning" />
</pdp>

<propertyValueKeyList
	>
<vk key="start | left " >
Start
</vk>
<vk key="center" >
Center
</vk>
<vk key="end | right" >
End
</vk>
</propertyValueKeyList>

</property>

@------------------------------------------------------------
@- <property short="P" long="tblPosition" value="A | F | C | P | L | R" >
<property short="P" long="position" value=" normal | topOfColumn 
| topOfPage | topOfLeftPage | topOfRightPage" >

<pdp>
Table placement.
<vidx id="table||placement on page" />
@- <vidx id="table||floating" />
@- <vidx id="floating table" />
</pdp>

<propertyValueKeyList
	keyWidth="20mm"
	>
<vk key="normal" >
Start table at the current position.
</vk>

<vk key="topOfColumn" >
Start table at top of next column
</vk>

<vk key="topOfColumn" >
Start table at top of next page
</vk>

<vk key="topOfLeftPage" >
Start table at top of next left page
</vk>

<vk key="topOfRightPage" >
Start table at top of next right page
</vk>

</propertyValueKeyList>

<pdp
	condition="fmcGuide"
	>
@- If <on>P</on> is set to <ov>F</ov> text will backfill space
@- ahead of the table
</pdp>

</property>

@------------------------------------------------------------
<property short="wo" long="minimumRows" value="int" >

<pdp>
Minimum number of widow/orphan rows.
</pdp>

</property>


@------------------------------------------------------------
<property short="anc" long="tblAutonumAxis" value="R | C"
	condition="fmcGuide"
	>

<pdp>
Autonumbering direction for paragraphs and footnotes
within the table. R = numbering across rows, C = numbering
down columns.
</pdp>

</property>


@------------------------------------------------------------
<@include> ${CTEXT}/cellMargins.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Outside border rulings"
	longTitle="Outside border rulings"
	dest="optgroup.Tbl.outside_rulings.start"
>
(In the following <#name> must be the
name of a ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" />.
Set <fI>name</fI> to "None" for no ruling.)
<MkDest id="Tbl.propertyGroup.border_rulings"/>
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="lr"
	long="leftRule"
	long2="startRule"
	value="name"
	>

<pdp>
Left outside ruling.
<vidx id="table||ruling, outside" />
</pdp>
</property>

@------------------------------------------------------------
<property short="tr" long="topRule" value="name" >

<pdp>
Top outside ruling.
</pdp>
</property>


@------------------------------------------------------------
<property
	short="rr"
	long="rightRule"
	long2="endRule"
	value="name"
	>

<pdp>
Right outside ruling.
</pdp>
</property>


@------------------------------------------------------------
<property long="bottomRule" value="name" >

<pdp>
Bottom outside ruling.
</pdp>
</property>


<@skipStart>
@------------------------------------------------------------
<property long="lastRuleOnly" value="N | Y" >

<pdp>
Use ruling on last row. Y means draw bottom rule on 
last sheet only; N means draw rule on the bottom
of every sheet.
<vidx id="table||ruling, under last row only" />
</pdp>
</property>
<@skipEnd>

@------------------------------------------------------------
@- Fill for all table cells
<@include> ${CTEXT}/cellFillColor.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Header, footer and body row fills"
	longTitle="Header, footer and body row fills"
	dest="propertyGroup.Tbl.row_rulings.start"
	>
</propertyGroup>

@------------------------------------------------------------
<property long="headerRowFillColor" value="name" >

<pdp>
Background color for header rows. <on>headerRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property long="headerRowFillOpacity" value="0 | 100" >

<pdp>
Fill opacity for header rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="headerRowFillTint" value="percent" >

<pdp>
Background tint for header rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillColor" value="name" >

<pdp>
Background color for body rows. <on>bodyRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillOpacity" value="0 | 100" >

<pdp>
Fill opacity for body rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillTint" value="percent" >

<pdp>
Background tint for body rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillColor" value="name" >

<pdp>
Background color for footer rows. <on>footerRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillOpacity" value="0 | 100" >

<pdp>
Fill opacity for footer rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillTint" value="percent" >

<pdp>
Background tint for footer rows.
</pdp>

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Header, footer and body row rulings"
	longTitle="Header, footer and body row rulings"
	dest="propertyGroup.Tbl.row_rulings.start"
	>
(In the following <#name> must be the
name of a Ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" file="fdefs" />.
Set <fI>name</fI> to "none" for no ruling.)
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
@- TBA
@- e.g. exceptionBodyRowRule
@- e.g. exceptionBodyRowNumber
@- 
@- Ditto for colum rules & shadings
@------------------------------------------------------------

<@skipStart>
@------------------------------------------------------------
<property long="headerRowRule" value="name" >

<pdp>
Ruling used between rows in the table header.
</pdp>

</property>
<@skipEnd>


@------------------------------------------------------------
<property long="bodyRowRule" value="name" >

<pdp>
Ruling used between rows in the table body.
</pdp>

</property>

<@skipStart>
@------------------------------------------------------------
<property long="footerRowRule" value="name" >

<pdp>
Ruling used between rows in the table footer.
</pdp>

</property>
<@skipEnd>


@------------------------------------------------------------
<property long="headerSeparatorRule" value="name" >

<pdp>
<fI>name</fI> is the name of the ruling to
use between the last heading row 
and the first body row.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerSeparatorRule" value="name" >

<pdp>
<fI>name</fI> is the name of the ruling to
use between the last body row 
and the first footer row.
</pdp>

</property>


<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Exception (X) body row rulings"
	longTitle="Exception (X) body row rulings"
	dest="optgroup.Tbl.row_rulings.exception"
	condition="fmcGuide"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="xBr" long="bodyRowXRule" value="name"
	condition="fmcGuide"
	>

<pdp>
Exception ruling for body rows. Used 
for every
<Font fmt="F_Italic">n</Font>th
 row (where
<Font fmt="F_Italic">n</Font>
 is specified using the <on>xBrp</on> property).
</pdp>

<propertyDefault>None (i.e. the exception
rule for body rows is to have no ruling between every
<Font fmt="F_Italic">n</Font>th
 body row).</propertyDefault>

</property>

@------------------------------------------------------------
<property short="xBrp"
	long="bodyRowXRulePeriod"
	value="int"
	condition="fmcGuide"
	>

<pdp>
Ruling period. Use the ruling specified with the <on>xBr</on>
property on every
<Font fmt="F_Italic">n</Font>
 rows. If <on>xBrp</on> is set to <ov>0</ov> the exception ruling
is never used.
If <on>xBrp</on> is set to <ov>1</ov> the exception ruling is used between
every body row.
If <on>xBrp</on> is set to <ov>2</ov> the exception ruling is used between
every second body row, and so on. 
</pdp>

<pdp>
Like all other table rulings, 
exception rulings are always overridden by any ruling properties
used with <#xCell> element within a <#xTbl> element.
</pdp>

</property>
<@skipEnd>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Column rulings"
	longTitle="Column rulings"
	dest="propertyGroup.Tbl.column_rulings.start"
>
(In the following <#name> must be the
name of a Ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" file="fdefs" />.
Set <fI>name</fI> to "none" for no ruling.)
<MkDest id="Tbl.element.rulings"/>
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property long="columnRule" value="name"
	>

<pdp>
Default column ruling for columns.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Exception (X) column rulings"
	longTitle="Exception (X) column rulings"
	dest="optgroup.Tbl.column_rulings.exception"
	condition="fmcGuide"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="xcn" long="colXRuleNum" value="int"
	condition="fmcGuide"
	>

<pdp>
Column number with right side which uses table column ruling 
specified with <on>xcr</on> property.
<vidx id="table||ruling, exception column ruling" />
</pdp>

</property>

@------------------------------------------------------------
<property short="xcr" long="colXrule" value="name"
	condition="fmcGuide"
	>

<pdp>
Exception ruling for columns. Ruling style for
column number specified using <on>xcn</on> property.
<vidx id="table||ruling, exception column ruling" />
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Header and footer (HF) fill and color"
	longTitle="Header and footer (HF) fill and color"
	dest="optgroup.Tbl.HF_fill_and_color"
	condition="fmcGuide"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="HFfill" long="HFfill" value="0 - 7, 15"
	condition="fmcGuide"
	>

<pdp>
Header/Footer fill value
</pdp>

</property>

@------------------------------------------------------------
<property short="HFcolor" long="HFcolor" value="name"
	condition="fmcGuide"
	>

<pdp>
Header/Footer color
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Table body fill and color"
	longTitle="Table body fill and color"
	dest="optgroup.body_fill_and_color"
	condition="fmcGuide"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="shadeAxis" long="tblShadeAxis" value="R | C"
	condition="fmcGuide"
	>

<pdp>
Table shading axis. R = shade by row. C = shade by column.
</pdp>

</property>


@------------------------------------------------------------
<property short="Bfill" long="tblBodyFill" value="0 - 7, 15"
	condition="fmcGuide"
	>

<pdp>
Body cells fill value. Default fill pattern for most body cells
(see <on>xBfill</on> for exceptions to this).
</pdp>

</property>

@------------------------------------------------------------
<property short="Bcolor" long="tblBodyColor" value="name"
	condition="fmcGuide"
	>

<pdp>
Body cells color. Default color for most body cells
(see <on>xBcolor</on> for exceptions to this).
</pdp>


</property>

@- <property short="Bfill" long="tblBodyFill" value="0 - 7, 15" >

@------------------------------------------------------------
<property short="Bp" long="bodyShadeNum" value="int"
	condition="fmcGuide"
	>

<pdp>
Body shading period. The number of consecutive
body columns/rows which use the shading values
specified with the <on>Bfill</on> and <on>Bcolor</on> properties
above
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Exception (X) table body fill and color"
	longTitle="Exception (X) body fill and color"
	dest="optgroup.exception_body_fill_and_color"
	condition="fmcGuide"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="xBfill" long="tblXBodyFill" value="0 - 7, 15"
	condition="fmcGuide"
	>

<pdp>
Exception fill pattern for body columns or body rows.
</pdp>

</property>

@------------------------------------------------------------
<property short="xBcolor" long="tblXBodyColor" value="name"
	condition="fmcGuide"
	>

<pdp>
Exception color for body columns or body rows.
</pdp>

</property>

@------------------------------------------------------------
<property short="xBp" long="xBperiod" value="int"
	condition="fmcGuide"
	>

<pdp>
Exception body shading period. Number of consecutive 
columns/rows which use the exception body shading values
specified with the <on>xBfill</on> and <on>xBcolor</on> properties.
Exception column or row shading alternates with default body column
or row shading to form a repeating pattern
</pdp>

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Table title"
	longTitle="Table title properties"
	dest="optgroup.Tbl.tbl_title.start"
	condition="fmcGuide"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="TP" long="titlePosition" value="A | B | N"
	condition="fmcGuide"
	>

<pdp>
Default table title placement. A = above table, B = below table,
N = no title.
<vidx id="table||title placement" />
<vidx id="table title||placement of" />
</pdp>

<pdp>
Only effective if the table instance contains
a <#xTblTitle> element.
</pdp>

</property>

@------------------------------------------------------------
<property short="Tpf" long="titlePgfFormat" value="name"
	condition="fmcGuide"
	>

<pdp>
<vidx id="table title||paragraph format of" />
Paragraph format for title of new
table created with this format
</pdp>

<pdp>
Only effective if the table instance contains
a <#xTblTitle> element.
</pdp>

</property>

@------------------------------------------------------------
<property short="Tgap" long="titleGap" value="dim"
	condition="fmcGuide"
	>

<pdp>
Table title gap
<vidx id="table title||gap" />
</pdp>

<pdp>
Effective only if the table instance contains
a <#xTblTitle> element.
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = tbl.inc  (End) -->
@------------------------------------------------------------
