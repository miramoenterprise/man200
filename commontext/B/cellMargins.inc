@------------------------------------------------------------
@-- cellMargins.inc
@------------------------------------------------------------

<#ifndef> tbl_cm1	{s}
<#ifndef> tbl_cm2	{, for all cells in table}
<#ifndef> tbl_cm3	{, or specified using the 
corresponding <#xTbl> element property}

@- <@write> error {<#elementName>	cellMargins.inc}
@------------------------------------------------------------
<@macro> cellMarginsGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Cell margins"
			longTitle="Cell margins"
			dest="<#elementName>.propertyGroup.cell_margins"
			>(see Note <xparanumonly id="note.P.table_cell_properties"
			/>, on page <xnpage id="note.P.table_cell_properties"
			/>)</propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Cell margins (default for all cells in row)"
			longTitle="Cell margins (default for all cells in row)"
			dest="<#elementName>.propertyGroup.cell_margins"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Cell margins (default for all cells in table)"
			longTitle="Cell margins (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_margins"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellMarginsGroupHead>
@------------------------------------------------------------





@------------------------------------------------------------
<property
	short="cm"
	long="allMargins"
	value="dim"
	>

<pdp>
All cell margins, i.e. start, top, end
and bottom.
<vidx id="cell||margins" />
<vidx id="table||cell margins" />
</pdp>

<pdp>
Overrides the default margin.
specified in the table format, if any.
</pdp>


<pdp>
Default units are points.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="clm"
	long="startMargin"
	long2="leftMargin"
	value="dim"
	>

<pdp>
Cell start / left margin override.
<vidx id="cell||margins" />
<vidx id="table||cell margins" />
</pdp>

</property>

@------------------------------------------------------------
<property
	short="ctm"
	long="topMargin"
	value="dim"
	>

<pdp>
Cell top margin override.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="crm"
	long="endMargin"
	long2="rightMargin"
	value="dim"
	>

<pdp>
Cell end / right margin override.
</pdp>


</property>

@------------------------------------------------------------
<property
	short="cbm"
	long="bottomMargin"
	value="dim"
	>

<pdp>
Cell bottom margin override.
</pdp>


</property>
