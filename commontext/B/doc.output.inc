@-------------------------------------------------------
@- doc.output.inc
@-------------------------------------------------------
@--- FIXED
<xOptGroup
	shortTitle="Single document output"
	longTitle="Single document output"
	XRefID="<#CodeName1>.optgroup.single_document.start"
>
All, or any subset of, the following may be used concurrently.
<MkDest id=single_document_options.start />
</xOptGroup>
@-------------------------------------------------------

@------------------------
<xOption short="Ofm" long="fmFile" value="filename" four="Doc|inline" >

Name of FrameMaker binary document to create.

</xOption>

@------------------------
<xOption short="Omif" long="MIFFile" value="filename" four="Doc|inline" >

Name of FrameMaker MIF (Maker Interchange Format)
document to create.

</xOption>

@------------------------
<xOption short="Oviewfm" long="fmViewFile" value="filename" four="Doc|inline" >

Name of locked (view-only) FrameMaker binary document to create.

</xOption>

@------------------------
<xOption short="Oviewmif" long="mifViewFile" value="filename" four="Doc|inline" >

Name of locked (view-only) FrameMaker MIF (Maker Interchange Format)
document to create.

</xOption>

@------------------------
<xOption short="Ortf" long="rtfFile" value="filename" four="Doc|inline" >

Name of RTF file to create.
<vidx id="RTF||<#CodeName1> code option" />

</xOption>

@------------------------
<xOption short="Ortfj" long="rtfFileJ" value="filename" four="Doc|inline" >
Name of RTF file to create, using Japanese filter version.
<vidx id="RTF||<#CodeName1> code option" />

</xOption>

@------------------------
<xOption short="Ohtml" long="htmlFile" value="filename" four="Doc|inline" >

Name of HTML file to create.
<vidx id="HTML||<#CodeName1> code option" />

</xOption>

@------------------------
<xOption short="Oxml" long="xmlFile" value="filename" four="Doc|inline" >

Name of XML file to create.
<vidx id="XML||<#CodeName1> code option" />

</xOption>
@------------------------

<xOptGroup/>
