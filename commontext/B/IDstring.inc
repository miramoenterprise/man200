@---------------------------------------------------------
@- IDstring.inc
@---------------------------------------------------------
<propertyGroup
	longTitle="Metadata options"
	shortTitle="Metadata"
	dest="<#CodeName1>.optgroup.idstring"
	condition="fmcGuide"
/>
@---------------------------------------------------------

<property short="IDstring" long="IDstring" value="string"
	condition="fmcGuide"
	>

<pdp>
<#fI>string<#fnI> may contain up to 512 bytes of metadata
associated with this instance of the <#CodeName1> code.
</pdp>

@- <xOptPara/>
<pdp>
This metadata is shown in FrameMaker MIF files
in the &lt;UserString <#fI>string<#fnI> &gt; statement
and can be extracted from or viewed in FrameMaker
document (binary) files, or post-processed, using a
custom API to access the FP_UserString property.
</pdp>

<propertyDefault v="null" />
</property>
@---------------------------------------------------------
