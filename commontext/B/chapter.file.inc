@------------------------------------
@--- chapter.file.inc
@------------------------------------
<propertyGroup
        shortTitle="Chapter file name"
        longTitle="Chapter file name"
        dest="<#elementName>.propertyGroup.filename.start"
/>
@------------------------------------

@----------------------------------------------
<property short="file" long="chapterName" value="filename" >

<pdp>
<fI>Required</fI><br/>
Name of document file (chapter) to create and add
to current book.
</pdp>

<pdp>
<fI>filename</fI> is either the <fI>basename</fI> of
the file (in which case it may not contain the file path
separator characters, <#osq>/<#csq> or <#osq>&#x5c;<#csq>) or the
<fI>absolute</fI> (full path) name of the file.
Relative file names are not allowed.
</pdp>


<pdp>
Where the <fI>basename</fI> form of <fI>filename</fI>
is given, the book chapter file <fI>filename</fI> is
created in the directory which 
contains the FrameMaker book, specified using
the <#osq>-Bfile<#csq> command line flag
(see page <xnpage
	id="running.option.Bfile" file="running" />),
or the <on>Bfile</on>
option on the <#xMiramoXML> or <#xDoc> inline markup codes.
</pdp>

</property>

