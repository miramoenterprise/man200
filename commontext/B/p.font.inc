@---------------------------------------------
@-- p.font.inc
@---------------------------------------------
@--- FIXED


@-----------------------------
<xOptGroup
	shortTitle="Font"
	longTitle="Font options"
	XRefID="p.optgroup.font"
	></xOptGroup>


<xOption short="F" long="pgfFont" value="name" >

<vidx id="paragraph||font tag (<#optRef>=name)" />
<vidx id="font tag" />
Font tag. <#fI>name<#fnI> may be either
the name of a font definition
specified using the <#xFontDef> font
definition code (see pages
<xnpage id="FontDef.code.start" file="fdefs" />
<xphyphen/>
<xnpage id="FontDef.code.end" file="fdefs" />
) or a character tag
name assigned in the <#osq>Character Designer<#csq>
of a FrameMaker template file.

<xOptPara/>
Note that the following font options
(below) override the settings of the <on><#optRef></on> option
only if they are placed after the <on><#optRef></on> option
in the sequence of options following the <#xP> code.

</xOption>

