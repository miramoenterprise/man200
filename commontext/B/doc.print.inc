@-------------------------------------------------------
@- doc.print.inc
@-------------------------------------------------------
@--- FIXED

@------------------------
<xOptGroup
	shortTitle="Printing and print file output options"
	longTitle="Printing and print file output options"
	XRefID="<#CodeName1>.optgroup.printing_and print_file_output.start"
/>
@------------------------


@------------------------
<xOption short="P" long="Print" value="N | Y" four="Doc|inline" >

Print to default printer.
<xOptDefault>from <#osq>-P<#csq> command line option, if set, or <ov>N</ov></xOptDefault>

</xOption>

@------------------------
<xOption short="Pdefault" long="defaultPrinter" value="name"
	four="Doc|inline"
	>

Set the default printer to printer
named <#fI>name<#fnI> for this job.
Applicable when producing PostScript or PDF output.
<#fI>name<#fnI> must be surrounded by double
quotation marks if the printer name contains
spaces.

<xOptPara/>
Setting the default printer should be avoided
when high throughput is important. If the Miramo
Enterprise module is installed, then starting mmServer
with the <#osq><#nbhy>pdefault<#csq> option (see page
<xnpage id="mmserver.option.pdefault" file="em_mmserver" />
 in the <#emGuide>) set to a suitable printer should
enable use of the <#osq><#nbhy>Pdefault<#csq> to be minimized.

<xOptDefault>from <#osq>-Pdefault<#csq> command line option, if set</xOptDefault>
</xOption>

@------------------------
<xOption short="Pname" long="printerName"
	value="name"
	four="Doc|inline"
	>

Print to printer <#fI>name<#fnI>.
<xOptDefault>from <#osq>-Pname<#csq> command line option, if set</xOptDefault>

</xOption>

@- <@write> error {--------------------------------}
@------------------------
<xOption short="Pfile"
	long="printFile"
	@- value="filename<BR/>[, pname ]"
	value="filename [, pname ]"
	valueStraddle="1"
	four="Doc|inline" >

Print to named PostScript file.

<xOptPara/>
@- <#pname>
<pname/>

</xOption>

@------------------------
<xOption short="PAfile"
	long="printAFile"
	@- value="filename<BR/>[, pname ]"
	value="filename [, pname ]"
	valueStraddle="1"
	four="Doc|inline" >

Print to named PostScript file, with Acrobat support.
<P>

</xOption>

@------------------------
<xOption short="Pcopies" long="" value="int" four="Doc|inline" >

Number of copies to be printed.
<xOptDefault>from <#osq>-Pcopies<#csq> command line option, if set, or 1</xOptDefault>
@- <#pname>
<pname/>

</xOption>

@------------------------
<xOption short="Pscale" long="" value="percent" four="Doc|inline" >

Scale document to given percentage before printing.
<xOptDefault>from -Pscale command line option, if set, or 100</xOptDefault>

</xOption>

@------------------------
<xOption short="Preg" long="printCropMarks" value="D | N | W | T" four="Doc|inline" >

Print registration and crop marks.
<vidx id="registration marks, printing" />
<vidx id="crop marks, printing" />
<vidx id="Tombo, registration marks" />


<xvaluekeyhead first="3mm" second="16mm" newRow="1"
        col1="Value"
        col2="Description"
/>
<vk key="D" >Default, i.e. as set in template document, if any.</vk>
<vk key="N" >No registration marks. This is the default if there is no template document.</vk>
<vk key="W" >Western registration marks</vk>
<vk key="T" >Tombo registration marks</vk>




<xOptDefault>from <#osq>-Preg<#csq> command line option,
if set, or <ov>D</ov> or <ov>N</ov></xOptDefault>

</xOption>


@------------------------
<xOption short="Pstartpage" long="" value="int" four="Doc|inline" >

Print file starting from given page number. The page number refers
to the page number variable which may appear on the printed page, not
the sheet number within the output document.
<xOptDefault>first page in the document</xOptDefault>

</xOption>

@------------------------
<xOption short="Pendpage" long="" value="int" four="Doc|inline" >

Print file starting ending at given page number. The page number refers
to the page number variable which may appear on the printed page, not
the sheet number within the output document.
<xOptDefault>last page in the document</xOptDefault>

</xOption>

@------------------------
<xOption short="Pstartno" long="" value="int" four="Doc|inline" >

Print file starting from given pagecount. The pagecount refers
to the sequential sheet number within the file, with the
first page numbered as 1, not to the page numbers which
may be printed in the document.
<xOptDefault>1</xOptDefault>

</xOption>


@------------------------
<xOption short="Pendno" long="" value="int" four="Doc|inline" >

Print file ending at given pagecount. The pagecount refers
to the sequential sheet number within the file, with the
first page numbered as 1, not to the page numbers which
may be printed in the document.
<xOptDefault>last page in the document</xOptDefault>

</xOption>


@------------------------
<xOption short="Ppapersize" long=""
	value="default | pagesize | a3 | a4 |<br/>a5 | b5 |<br/>tabloid |<br/>XidxWrapXlegal |<br/>letter |<br/>WuHu"
	four="Doc|inline" >
Select printer paper size either using a paper size code such as <ov>a4</ov>, 
or by specifying a custom papersize width and height using the form
<#fI>WuHu<#fnI>, for example <#osq>9cm20cm<#csq> or <#osq>210mm10in<#csq>. Note that a custom
papersize code may not include any spaces.

<P>
See the description for the <#osq><#nbhy>Ppapersize<#csq> command line
option, on page <xnpage id="running.option.Ppapersize"
	file="running" />, for more details.

<xOptDefault><ov>default</ov></xOptDefault>

</xOption>


@------------------------
<xOption short="PpageOrder" long="PpageOrder" value="F | L" four="Doc|inline" >

Print first page first
<xOptDefault>F if <#osq>-PpageOrder<#csq> command line option is not used,
or as per <#osq>&#x2011;PpageOrder<#csq> command line option
(see page 
<xnpage id="running.option.PpageOrder" file="running" />
).</xOptDefault>

</xOption>

@------------------------
<xOption short="Pseps" long="separations" value="N | Y" four="Doc|inline" >

Print document in 4 CMYK color separated versions.
<xOptDefault>N unless <#osq>-Pseps<#csq> command line option is used
(see page 
<xnpage id="running.option.Pseps" file="running" />
).</xOptDefault>

</xOption>


@------------------------
<xOption short="Pskipblanks" long="printNoBlanks" value="N | Y" four="Doc|inline" >

Suppress printing of blank pages within document.
<xOptDefault>N unless <#osq>-Pskipblanks<#csq> command line option is used
(see page 
<xnpage id="running.option.Pskipblanks" file="running" />
).</xOptDefault>

</xOption>
@------------------------

@------------------------
<xOptGroup
	shortTitle="Fonts to embed in PostScript output (Unix only)"
	longTitle="Fonts to embed in PostScript output (Unix only)"
	XRefID="<#CodeName1>.optgroup.embed_fonts.start"
	indexOnly="1"
/>
@------------------------

@------------------------
<xOption short="Pfonts" long="printFonts"
	value="none | x13 | x35 | all | Asian" four="Doc|inline" >
Fonts to embed in PostScript output. (Unix only)

<xvaluekeyhead first="3mm" second="16mm" newRow="1"
	col1="Value"
	col2="Description"
/>
<vk key="none" >No included fonts (default).</vk>
<vk key="x13" >All except <#osq>Standard 13<#csq> and Asian fonts.</vk>
<vk key="x35" >All except <#osq>Standard 35<#csq> and Asian fonts.</vk>
<vk key="all" >All fonts except Asian fonts.</vk>
<vk key="Asian" >Only Asian fonts.</vk>

<xOptDefault>none, unless <#osq><#nbhy>Pfonts<#csq> command line option is used</xOptDefault>
(see page
<xnpage
        id="running.option.Pfonts"
        file="running" />).
</xOption>
@------------------------


<xOptGroup/>
