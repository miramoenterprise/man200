@------------------------------------------------------------
@- chapter.template.inc
@------------------------------------------------------------
@- Included in:
@- 
@- 	<Chapter>
@- 	<GenChapter>
@- 	<Doc>
@- 	<MiramoXML>
@- 
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> ifChapterCode {
	@--- Definitions for <Chapter> code only
	<#def> ifChapterCode1	{}
	<#def> ifChapterCode2	{}
	@if(<#elementName> = Chapter ) {
		<#def> ifChapterCode1	{ (<FontRef fmt="F_FR" >type &ne; "INC"</FontRef>)}
		<#def> ifChapterCode2	{(All template options are ignored if the
<on>type</on> option is set to <ov>INC</ov>.)}
		}
}
@------------------------------------------------------------
<|ifChapterCode>
@------------------------------------------------------------

@------------------------------------------------------------
@- Template options
@------------------------------------------------------------
<propertyGroup
	shortTitle='Template options<#ifChapterCode1>'
	longTitle='Template options'
	dest="<#elementName>.propertyGroup.template_options"
><#ifChapterCode2></propertyGroup>

@------------------------------------------------------------
<property short="Cfile" long="tplCopyFile"
	four="Doc|inline"
	value="filename" >

<pdp>
Copy all body pages and format specifications
from <fI>filename</fI> to the beginning of the
current document. <fI>filename</fI> must
be a FrameMaker document saved in MIF format.
</pdp>

<pdp>
The <on>Cfile</on> and <on>Tfile</on> options are mutually
exclusive: if both are used,
the later option takes
precedence over the earlier option. 
</pdp>

<pdp>
The <on>Cfile</on> option differs
from the <on>Tfile</on> option in that the contents
of the body pages are copied from the specified
file, and all formatting specifications are always
copied from the specified file. (The <on>Tflow</on>,
<on>Tformats</on> and <on>TXformats</on> options are ignored).
</pdp>


<pdp>
The <on>Cfile</on> option takes precedence over the
<#osq>&#x2011;Cfile<#csq> command line option (described in Chapter
<xparanumonly id="running.chapter.start"
	file="running" />, <#osq>Running Miramo<#csq>, 
<xnpage id="running.option.Cfile" file="running" />
).
</pdp>

<propertyDefault>from <on>-Cfile</on> command line option</propertyDefault>
</property>

@------------------------------------------------------------
<property short="Tfile" long="tplFile"
	four="Doc|inline"
	value="filename" >

<pdp>
Use <fI>filename</fI> as the source for
formatting specifications. <fI>filename</fI> must
be a FrameMaker document saved in MIF format.
</pdp>


<pdp>
The <on>Cfile</on> and <on>Tfile</on> options are mutually
exclusive: if both are used,
the later option takes
precedence over the earlier option. 
</pdp>


<pdp>
If neither the <on>Tformats</on> nor the <on>TXformats</on> option
is used, then <fI>all</fI> formats in the template
are imported.
</pdp>

 
<pdp>
The <on>Tfile</on> option takes precedence over the
<#osq>&#x2011;Tfile<#csq> command line option (described in Chapter
<xparanumonly id="running.chapter.start"
	file="running" />, <#osq>Running Miramo<#csq>, 
<xnpage id="running.option.Tfile" file="running" />).
</pdp>

<propertyDefault>from <on>-Tfile</on> command line option</propertyDefault>
</property>

 
@------------------------------------------------------------
<property short="Toverride" long="tplOverride"
	four="Doc|inline"
	value="N | Y" >

<pdp>
Allow template format definitions to override Miramo 
format definition code markup.
</pdp>

<propertyDefault>from <#osq>&#x2011;Toverride<#csq> command line option,
or N if none given</propertyDefault>
</property>


@------------------------------------------------------------
<property short="Tformats" long="tplImportFormats"
	four="Doc|inline"
	value="key" >

<pdp>
Specify formats to be extracted from the template file
given with the <on>Tfile</on> option. The formats to be included
are specified by a <fI>key</fI> string comprising one or more
characters from the following set:
</pdp>

@-  <P fmt="P_Input" fi="10mm" sa="3pt">

@- <pdp>
@- <#formatstring>
@- </pdp>

<pdp>
The formats denoted by the <fI>key</fI> characters are described
in Chapter
<xparanumonly id="running.chapter.start" file="running" />, <#osq>Running
Miramo<#csq> (see the description of the
<#osq>&#x2011;Tformats<#csq> and <#osq>&#x2011;TXformats<#csq> command
line options on page <xnpage
	id="running.option.Tformats" file="running" />).
</pdp>

 

<pdp>
Note that the <#osq>F<#csq> format flag copies text flow formats
from text flow <#osq>A<#csq> of the template file. To copy text
flow formats from a different text flow, use the <on>Tflow</on>
option described below.
</pdp>

<propertyDefault>from <on>-Tformats</on> command line option</propertyDefault>

</property>

@------------------------------------------------------------
<property short="TXformats" long="tplExFormats"
	four="Doc|inline"
	value="key" >
 
<pdp>
Specify formats to be excluded from the template file
given with the <on>Tfile</on> option. The formats to be excluded
are specified by a <fI>key</fI> string comprising one or more
characters from the following set:
</pdp>
@- <P fmt="P_Input" fi="10mm" sa="3pt" >
<pdp>
<#formatstring>
</pdp>

<pdp>
The formats denoted by the <fI>key</fI> characters are described
in Chapter
<xparanumonly id="running.chapter.start" file="running" />,
<#osq>Running Miramo<#csq> (see the description of
the <on>-Tformats</on> and <on>-TXformats</on> command line options on page 
<xnpage id="running.option.Tformats" file="running" />
).
</pdp>


<propertyDefault>from <on>-TXformats</on> command line option</propertyDefault>

</property>

@--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
@- Template options (2)
@--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
@-x <@include> ${CTEXT}/tflow.text.inc


@------------------------------------------------------------
<propertyGroup
	shortTitle="Template image processing mode"
	longTitle="Template image processing mode"
	dest="<#elementName>.propertyGroup.tplImode.start"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="tplImode" long="tplImode"
	four="Doc|inline"
	value="normal | bypass" >
 
<pdp>
If the <on>-tplImode</on> command-line option is
set to <ov>bypass</ov>, any images in
EPS, JPEG, PNG or TIFF format that are imported by reference
in a template document or that are imported using
the <#xImage> code will not be processed by FrameMaker.
</pdp>


<pdp>
See the description of the <#xImage> code <on>imode</on> option, on page
<xnpage id="Image.option.imode" file="inline" >.
</pdp>

<propertyDefault>normal</propertyDefault>

</property>
@------------------------------------------------------------
