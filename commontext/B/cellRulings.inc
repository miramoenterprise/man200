@------------------------------------------------------------
@-- cellRulings.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> cellRulingsGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Cell border rulings"
			longTitle="Cell border rulings"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Cell border rulings (default for all cells in row)"
			longTitle="Cell border rulings (default for all cells in row)"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Cell border rulings (default for all cells in table)"
			longTitle="Cell border rulings (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellRulingsGroupHead>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="lr" long="leftRule" value="name" >

<pdp>
Left ruling.
<vidx id="ruling||in table cells" />
</pdp>

<pdp>
<fI>name</fI> is the name of a ruling format
in a referenced template, or defined
@- using the <#xRuleDef> format definition element (see pages <xnpage
@- 	id="RuleDef.element.start" /><xphyphen/><xnpage
@- 	id="RuleDef.element.end"  />).
using the <#xRuleDef> format definition element (see page <xnpage
	id="RuleDef.element.start" />).
</pdp>


<propertyDefault>None</propertyDefault>
</property>

@------------------------------------------------------------
<property short="tr" long="topRule" value="name" >

<pdp>
Top ruling. See <on>leftRule</on> above.
</pdp>
</property>


@------------------------------------------------------------
<property short="rr" long="rightRule" value="name" >

<pdp>
Right ruling. See <on>leftRule</on> above.
</pdp>
</property>

@------------------------------------------------------------
<property short="br" long="bottomRule" value="name" >
<pdp>
Bottom ruling. See <on>leftRule</on> above. 
</pdp>
</property>
@------------------------------------------------------------

