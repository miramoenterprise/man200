@------------------------------------------------------------
<!-- File = frameLocationMMC.inc (start) -->
@------------------------------------------------------------
<#ifndef> achart		0
<#ifndef> this.aframe.desc	{xxxxxxx}
<#ifndef> this.aframe.code	{xxxxxxx}

<#def> tblframe.pos.options.start	{}
<#def> tblframe.pos.options.end		{}
<#def> aframe.pos.options.start		{}
<#def> aframe.pos.options.end		{}
<#def> arframe.pos.options.start	{}
<#def> arframe.pos.options.end		{}
<#def> tprefix				{}
<#def> so.option			{so}


@------------------------------------------------------------
<@xmacro> inlineExceptionValue {
	@----------------------------------------------------
	@- Only applies to AFrame
	@----------------------------------------------------
	<#def> aframeValue	{}
	@if({<#elementName>} = {AFrame}) {
		<#def> aframeValue	{inline | }
		}
}{}
@------------------------------------------------------------
<inlineExceptionValue/>
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> inlineExceptionKey {
	@----------------------------------------------------
	@- Only applies to AFrame
	@----------------------------------------------------
	@if({<#elementName>} = {AFrame}) {
<vk key="inline" >Within paragraph text.
When the <on>position</on> property is set to <ov>inline</ov> the
<#xAFrame> element <fI>must</fI> contain an <#xImage> element.
</vk>
		}
}{}
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Frame location"
	longTitle="Frame location properties"
	XRefID="aframe.propertyGroup.wrapping"
	/>


@------------------------
<property short="P" long="position"
	value="above | below | <#aframeValue>runin | outsideTextFrame "
	condition="mmcGuide"
>

<pdp>
Position of
frame
relative to anchor, or
insertion, point.
</pdp>


<propertyValueKeyList
	condition="mmcGuide"
	>
<vk key="above">Inside text
frame, above current <outputRule
	condition="mmcGuide">paragraph</outputRule>
<outputRule condition="fmcGuide">line</outputRule>.
</vk>

@------------------------------------------------------------
<vk key="below">Inside text
frame, below current <outputRule
	condition="mmcGuide">paragraph</outputRule>
<outputRule condition="fmcGuide">line</outputRule>.
</vk>

@- <#aframeValueKey>
<inlineExceptionKey/>
@- xxx

@------------------------------------------------------------
<vk key="runin">Run into paragraph.
@------------------------------------------------------------
@- <runinLimitation/> defined in: control\referenceGuide.mmp
@------------------------------------------------------------
<runinLimitation/>
</vk>

@------------------------------------------------------------
<vk key="outsideTextFrame" span="Y" >
Outside of text frame.
</vk>

<@skipStart>
<vk key="outsideTable" span="Y" 
	condition="green"
	>
Outside of table.
When included in a table cell, position the <#xAFrame> outside
the container table.
</vk>
<@skipEnd>

</propertyValueKeyList>

<pdp>
The lateral
position of the <#xAFrame> is controlled
using the <pn>align</pn> property (see below).
</pdp>

<pdp>
When <pn>position</pn> is set to <pv>outsideTextColumn</pv> the
lateral position of the <#xAFrame> may be 
further adjusted using the <pn>sideOffset</pn> property (see below).
</pdp>

<pdp>
When <pn>position</pn> is set to <pv>runin</pv> the
top of the <#xAFrame> is aligned with
the top of the text (at caps height) in the 
first line of the paragraph.
When <pn>position</pn> is set to <pv>outsideTextColumn</pv> the
top of the <#xAFrame> is aligned with
the top of the text (at caps height) in the 
first line of the paragraph by default.
This position may be adjusted using the <pn>verticalOffset</pn> property
(see below).
</pdp>

</property>


<@skipStart>
@------------------------------------------------------------
<property short="<#tprefix>F" long="frameFloat" value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Allow the <#this.aframe.desc> to float.
</pdp>

<pdp>
Effective only if the <on>P</on> property is 
set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>.
</pdp>

<propertyDefault>N</propertyDefault>
</property>
<@skipEnd>


@------------------------------------------------------------
<property short="A" long="align"
	value="start | center | end"
	>

<pdp>
Lateral alignment of the <#xAFrame>.
</pdp>

<pdp>
Use the <pn>align</pn> property to control lateral
positioning, either within or outside
a text column.
</pdp>


<propertyValueKeyList
	>
@---------------------------------------------------
<vk key="start | left">
Align left or, if paragraph paragraph text is right-to-left,
align right.
<ov>left</ov> may be used as an alias for <ov>start</ov>.
</vk>

<vk key="center">
Center the anchored frame between the left and right
text frame borders.
</vk>

<vk key="end | right">
Align right or, if paragraph paragraph text is right-to-left,
align left.
<ov>right</ov> may be used as an alias for <ov>end</ov>.
</vk>

@- <vkSubHead>
@- Compatibility values:
@- </vkSubHead>
@- 
@- <vk key="L | Left" >
@- Treated the same as <ov>start</ov>
@- </vk>
@- 
@- <vk key="C | Center" >
@- Treated the same as <ov>center</ov>
@- </vk>
@- 
@- <vk key="R | Right" >
@- Treated the same as <ov>end</ov>
@- </vk>

</propertyValueKeyList>


<propertyDefault>end</propertyDefault>

</property>


@---------------------------------------------------
<property short="<#tprefix>so" long="sideOffset" value="dim"
	condition="fmcGuide"
	>
<pdp>
Offset of <#this.aframe.desc> from the alignment position
specified by the <on>align</on> property.
</pdp>

<pdp>
Effective only if the <on>P</on> property is set to <ov>OC</ov> or <ov>OF</ov>. Value may be
positive or negative.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@---------------------------------------------------
<property short="<#tprefix>so" long="sideOffset" value="dim"
	condition="mmcGuide"
	>
<pdp>
Offset of <#this.aframe.desc> from the alignment position
specified by the <on>align</on> property.
</pdp>

<pdp>
Effective only if
the <on>position</on> property is set to <ov>outsideTextFrame</ov>.
</pdp>

<propertyDefault>0</propertyDefault>
</property>


@---------------------------------------------------
<property short="<#tprefix>bo" long="baselineOffset" value="dim"
	condition="fmcGuide"
	>

<pdp>
Offset of <#this.aframe.desc> from text baseline.
</pdp>

<pdp>
Effective only if the <on>P</on> property is 
set to <ov>OC</ov>, <ov>OF</ov> or <ov>I</ov>.
Value may be positive or negative.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@---------------------------------------------------

@---------------------------------------------------
<property short="verticalOffset" long="verticalOffset" value="dim"
	condition="mmcGuide"
	>

<pdp>
Vertical offset of <#this.aframe.desc>.
</pdp>

<pdp>
Effective only if the <on>position</on> property is 
set to <ov>outsideTextFrame</ov>.
Value may be positive or negative.
By default the top of the <#elementName> is aligned
with the top of the text (at caps height) in the first
line of the container paragraph.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@---------------------------------------------------

@-----------------------------
@- <property short="<#tprefix>C" long="frameCrop" value="N | Y" >
@- 
@- <pdp>
@- Crop <#this.aframe.desc> to fit in text column.
@- </pdp>
@- 
@- <pdp>
@- Effective only if the <on>P</on> property is 
@- set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>.
@- </pdp>
@- 
@- <propertyDefault>N</propertyDefault>
@- </property>


@-----------------------------
<@include> ${CTEXT}/runaroundTextGap.inc
@-----------------------------
@------------------------------------------------------------
<!-- File = frameLocationMMC.inc (end) -->
@------------------------------------------------------------
