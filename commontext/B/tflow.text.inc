@----------------------------------------------------------
@--- tflow.text.inc
@----------------------------------------------------------
<@xmacro> xtflow.text.inc {
	<#def> tflow.para1	{Set the name of the main textflow to <#fI>name<#fnI>.<vidx
	id="textflow||<#osq>Tflow<#csq> option" />}

	<#def> tflow.para2	{Specific textflow attributes,
for feathering, side-head settings and baseline synchronization,
are as defined in a template file or by using the <#xTextFrameDef>
code (see pages <xnpage
	id="TextFrameDef.optgroup.feathering" file="fdefs" /><xphyphen/><xnpage
	id="TextFrameDef.options.column_balancing.end" file="fdefs" />).}

	<#def> tflow.para3	{If there is no textflow <#fI>name<#fnI> defined either
in a referenced template file or within a <#xTextFrameDef> code,
a warning message is issued and a new 25&nbsp;mm &times; 25&nbsp;mm
flow is created at the top left corner of the page.}

	@-----------------------------------------------------
	@- Do all the stuff for 'Running' chapter
	@-----------------------------------------------------
	@if({<#Filename>} = {running}) {
<@optrow> {-Tflow <#fI>name</Font>} {
<vidx id="-Tflow, command line option" />
<vidx id="command line options||-Tflow" />
<MkDest fmt=X id=Running.option.Tflow />
<#tflow.para1> <#tflow.para2>
}
<@runoptpara> {
<#tflow.para3>
}
<@runoptpara> {
<#fI>Default<#fnI>:
<@code> A
}
		}
	@-----------------------------------------------------
	@- Define all the default vals for NOT 'running'
	@-----------------------------------------------------
	@else	{
	@if({<#CodeName1>} = {GenChapter}) {
		<#def> tflow.defaultVal	{<ov>A</ov> (or from the value set using the <on>Tflow</on>
option on the <#xDoc> code or the <#xMiramoXML> code or set
by the <on>-Tflow</on> command line option)}
		}

	@if({<#CodeName1>} = {Chapter}) {
		<#def> tflow.defaultVal	{<ov>A</ov> (or from the value set using the <on>Tflow</on>
option on the <#xDoc> code or the <#xMiramoXML> code or set
by the <on>-Tflow</on> command line option)}
		}

	@if({<#CodeName1>} = {Doc}) {
		<#def> tflow.defaultVal	{<ov>A</ov> (or from the value set using the <on>Tflow</on>
option on the <#xMiramoXML> code or set
by the <on>-Tflow</on> command line option)}
		}

	@if({<#CodeName1>} = {MiramoXML}) {
		<#def> tflow.defaultVal	{<ov>A</ov> (or from the value set 
by the <on>-Tflow</on> command line option)}
		}
@- <@write> error	{BBBBBBBBBBBBBBBBBBBBBBBBB <#CodeName1> }
	@-----------------------------------------------------
	@- Output everything for NOT 'running'
	@-----------------------------------------------------
	<xOption short="Tflow" long="textFlow" value="name" >
<#tflow.para1> <#tflow.para2>
<xOptPara/>
<#tflow.para3>
<xOptDefault><#tflow.defaultVal></xOptDefault>
</xOption>
	}
}
@----------------------------------------------------------
<xtflow.text.inc/>
@----------------------------------------------------------
