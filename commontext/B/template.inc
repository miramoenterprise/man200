@----------------------------------------------------
@- def.template.inc
@----------------------------------------------------
@--- FIXED
@- This file is linked to def.template.inc and template.inc
@----------------------------------------------------

<#def> template.inc {
See the <#osq><#nbhy>Tfile<#csq> and <#osq><#nbhy>Cfile<#csq>
command line options on page
<xnpage id="running.option.Tfile" file="running" />
@- , the <#xTemplate> code on page
@- <xnpage id="Template.code.start" file="inline" />
, and the <#xChapter> and <#xDoc> 
code <on>Tfile</on> options on pages
<xnpage id="Chapter.code.start" file="inline" />
 and 
<xnpage id="Doc.code.start" file="inline" />
 for information on importing
format definitions from Frame template files.
}
