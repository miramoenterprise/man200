@------------------------------------------------------------
@- frameFill.inc
@------------------------------------------------------------

@------------------------------------------------------------
@------------------------------------------------------------
@- <|tbl.p2.xref>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Frame fill"
	longTitle="Frame fill"
	dest="<#elementName>.propertyGroup.fills"
	></propertyGroup>

@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillColor"
	long="fillColor" value="color"
	>

<pdp>
Fill color.
</pdp>

@- <propertyDefault><ov>Black</ov></propertyDefault>
</property>

@------------------------------------------------------------
<property short="fillOpacity"
	long="fillOpacity" value="0 | 100"
	>

<pdp>
<ov>0</ov> is transparent.
<ov>100</ov> is solid fill.
</pdp>

<propertyDefault>0</propertyDefault>
</property>



@------------------------------------------------------------
<property short="fillTint"
	long="fillTint" value="percent"
	>

<pdp>
Fill tint value, in the range
<ov>100</ov> to 
<ov>0</ov>.
</pdp>

<propertyDefault>100</propertyDefault>
</property>

