@---------------------------------------------------------
@- GOid.inc
@---------------------------------------------------------
@--- FIXED
<propertyGroup
	shortTitle="Metadata for graphic object (<#CodeName1>)"
	longTitle="Metadata for graphic object (<#CodeName1>)"
	dest="<#elementName>.optgroup.GOid"
	condition="fmcGuide"
/>
@---------------------------------------------------------

<property
	short="GOid"
	long="GOid"
	value="string"
	condition="fmcGuide"
	>

<pdp>
<#fI>string<#fnI> may contain up to 512 bytes of metadata
associated with this instance of the <#CodeName1> code.
</pdp>

<pdp>
This metadata is shown in FrameMaker MIF files
in the &lt;ObjectAttributes  &gt; statement
and can be extracted from or viewed in FrameMaker
document (binary) files, or post-processed, using a
custom API to access the FP_ObjectAttributes property.
</pdp>

<pdp>
The <on><#optRef></on> option is shorthand for &lt;Attribute
name="GOid" &gt;<#fI>string<#fnI>&lt;/Attribute&gt;. See
the <#xAttribute> code, described on pages
<xnpage id="Attribute.code.start" file="inline" />
<xphyphen><xnpage id="Attribute.code.end" file="inline" />.
</pdp>

<propertyDefault>none</propertyDefault>
</property>
