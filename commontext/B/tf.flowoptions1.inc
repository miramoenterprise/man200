@------------------------------------------------------------
@-- tf.flowoptions1.inc
@------------------------------------------------------------

<@xmacro> if.atextframe {
	@if ( <#elementName> = {ATextFrame} ) {
		<propertyDefault>2</propertyDefault>
		}
	@else {
		<propertyDefault>1 <fI>(1 subcolumn)</fI> </propertyDefault>
		}
}

@------------------------------------------------------------
<propertyGroup
	shortTitle="Multi-column layout"
	longTitle="Multi-column layout options"
	dest="ATextFrame.optgroup.multi_column_layout"
	condition="fmcGuide"
/>


@------------------------------------------------------------
<property short="cn" long="numColumns" value="int"
	condition="fmcGuide"
	>

<pdp>
Number of equally sized sub-columns within this 
text frame.
If this option has a value of <ov>2</ov> or greater the
gap between sub-columns is determined by the
<on>cg</on> option (see below).
@- <#tf.cb2>
</pdp>

<if.atextframe/>
</property>

@------------------------------------------------------------
<property short="cg" long="columnGap" value="dim"
	condition="fmcGuide"
	>

<pdp>
Gap between sub-columns within this text frame.
This value is only effective if the value of
the <on>cn</on> option (see above) is set to <ov>2</ov> or greater.
</pdp>

<propertyDefault>18pt</propertyDefault>
</property>
