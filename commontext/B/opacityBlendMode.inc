<@macro> obmConditions {
	<#def> obmFill		{}
	<#def> obmPara1		{}
	@if({<#CodeName1>} = {FrameFill}) {
		<#def> obmPara1		{Set opacity
for <#xFrameFill> object. 100% is solid color,
less than 100% is for transparent fill; 0% opacity is a
colorless, completely transparent fill.}
		<#def> obmFill		{fill}
		}
	@if({<#CodeName1>} = {Image}) {
		<#def> obmPara1		{Set image opacity.}
		<#def> obmFill		{image}
		}
}
<|obmConditions>

@------------------------------
<property short="opacity" long="opacity"
	value="percent"
	>
<pdp>
<#obmPara1>
</pdp>

<pdp>
If <on>opacity</on> is set to a value less than 100% and the<br/>
/AllowTransparency PDF joboptions parameter is not set to <#fI>true<#fnI>,
the job will fail. See Note <xparanumonly
        id="note.FrameFill.pdf_joboptions" /> on page <xnpage
        id="note.FrameFill.pdf_joboptions" />.
</pdp>

<propertyDefault><ov>100%</ov></propertyDefault>

</property>


@------------------------------
<property short="blendMode" long="blendMode"
	value="key"
	>

<pdp>
Set <#obmFill> blend mode. Ignored if opacity is 100%.
</pdp>


<pdp>
The value of <#fI>key<#fnI> may be any of the following:
<ov>Normal</ov>,
<ov>ColorDodge</ov>,
<ov>ColorBurn</ov>,
<ov>Darken</ov>,
<ov>Difference</ov>,
<ov>Exclusion</ov>,
<ov>HardLight</ov>,
<ov>Lighten</ov>,
<ov>Multiply</ov>,
<ov>Overlay</ov>,
<ov>Screen</ov>,
<ov>SoftLight</ov>.
</pdp>

<pdp>
The blend mode setting determines
how partly transparent layers are composited.
See Note <xparanumonly
	id="note.FrameFill.blendmode" /> on page <xnpage
	id="note.FrameFill.blendmode" /> for references
to documents that provide information about the effect of
blend mode settings.
</pdp>

<propertyDefault><ov>Normal</ov></propertyDefault>

</property>
