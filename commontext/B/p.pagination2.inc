@-----------------------------
@- p.pagination2.inc
@-----------------------------
@--- FIXED

<propertyGroup
	shortTitle="Run-in, in-column, side head or straddle"
	longTitle="Run-in, in-column, side head or straddle"
	dest="<#elementName>.optgroup.alignFormat.start"
	condition="fmcGuide"
/>

@-----------------------------
<property
	short="Pf"
	long="alignFormat"
	value="N | R | T | F | L | S | SN"
	condition="fmcGuide"
	>

<pdp>
Paragraph placement format.
N<#nbsp>=<#nbsp>normal, R<#nbsp>=<#nbsp>run-in, T<#nbsp>=<#nbsp>side head top, F<#nbsp>=<#nbsp>side head first,
L<#nbsp>=<#nbsp>side head last, S<#nbsp>=<#nbsp>straddle columns
and side heads, SN<#nbsp>=<#nbsp>straddle columns only.
<vidx id="paragraph||run-in (Pf=R)" />
<vidx id="paragraph||side heads (Pf=T,F,L)" />
<vidx id="paragraph||straddle (Pf=S)" />
</pdp>

</property>

@-----------------------------
<property
	short="rhp"
	long="endRunInChar"
	value="char"
	condition="fmcGuide"
	>

<pdp>
The default punctuation for ending
a run-in head.<br/>
The default is <#osq>.<#csq> (period). To
have nothing, use the empty string,
ie, <on>rhp</on>=""<#csq>.
</pdp>

</property>

@-----------------------------
<propertyGroup
	shortTitle="Widow/orphan lines"
	longTitle="Widow/orphan lines"
	dest="P.optgroup.widow_orphan_lines.start"
/>


@-----------------------------
<property short="wo" long="minLines" value="int" >

<pdp>
Minimum number of widow/orphan lines.
<vidx id="paragraph||widow lines in" />
<vidx id="paragraph||orphan lines in" />
<vidx id="orphans" />
<vidx id="widows" />
</pdp>

</property>

@-----------------------------
<propertyGroup
	shortTitle="Keep with next or previous paragraph"
	longTitle="Keep with next or previous paragraph"
	dest="P.optgroup.keep_with_next.start"
/>

@-----------------------------
<property short="wn" long="withNext" value="N | Y" >

<pdp>
Keep with next paragraph.
<vidx id="paragraph||keeping with next" />
</pdp>

</property>

@-----------------------------
<property short="wp" long="withPrevious" value="N | Y" >

<pdp>
Keep with previous paragraph.
<vidx id="paragraph||keeping with previous" />
</pdp>

</property>

@--================================================
