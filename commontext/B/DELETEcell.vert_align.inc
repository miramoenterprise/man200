@-------------------------------------------------
@-- cell.vert_align.inc
@-------------------------------------------------
@--- FIXED

<xOptGroup
	shortTitle="Vertical alignment of cell text"
	longTitle="Cell text vertical alignment"
	XRefID="Cell.optgroup.vertical_alignment.start"
	/>


<xOption short="Ca" long="verticalAlign" value="T | M | B" >

<MkDest fmt=X id=vertalign/>
<vidx id="cell||vertical alignment of text in" />
<vidx id="vertical text alignment||in table cells" />
<vidx id="cell||vertical alignment of text in" />
<vidx id="alignment||table cell, vertical" />
<vidx id="table cell||vertical alignment of text in" />
<vidx id="table||vertical alignment of text in cells" />
<vidx id="centering text||in table cells, vertically (Ca=M)" />
Cell vertical alignment of paragraph
contents within cell: T<#nbsp>=<#nbsp>top of cell,
M<#nbsp>=<#nbsp>middle of cell,
B<#nbsp>=<#nbsp>bottom of cell. 
The orientation for <on>T</on> (top) and <on>B</on> (bottom) changes with cell
rotation. For example, if <#xCell> code <on>a</on> option is set to <ov>90</ov>,
changing the <on>Ca</on> option value from <ov>T</ov> to <ov>B</ov> shifts the
paragraph contents leftwards.

<xOptPara/>
See Chapter
<xparanumonly id="chapter.Tables" file="tables" />,
<#osq>Tables<#csq>, in <#fI>Miramo User Guide<#fnI>, page 
<xnpage id="tables.text_vertalign" file="tables" />,
for an example of using the <on>Ca</on> option.

</xOption>
