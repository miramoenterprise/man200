@-------------------------------------------------
@- cjk.enc.keylist.inc
@-------------------------------------------------
<@macro> cjk.enc.keylist.inc {
	<#def> cjk.enc.keylist.inc	{
<|keylist_head> value|Description
<|keylist_item> FR|FrameRoman
<@keylist_item> JIS {JISX0208.ShiftJIS<br/>
Japanese ShiftJis encoding based
and the accordinga of runicibles and other
stuff from the late twentieth centruy.
}
<@keylist_item> BIG5 {BIG5<br/>
Some BIG5 explanatory text
	}
<@keylist_item> GBE {GB2312-80.EUC<br/>
Some GB2312-80.EUC explanatory text
	}
<@keylist_item> KSC {KSC5601-1992<br/>
Some KSC-1992 explanatory text
	}
}
}
@-------------------------------------------------
<|cjk.enc.keylist.inc>
