@------------------------------------------------------------
@-- chapter.process.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@include> ${CTEXT}/configFile.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Operating mode"
	longTitle="Operating mode options"
	dest="<#elementName>.propertyGroup.operating_mode.start"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="stripInput"
	long="stripInput"
	value="N | tab | nl | all" >

<pdp>
Enable special processing of text containing
binary tab, newline and carriage return characters.
</pdp>


<propertyDefault>As per the <#osq>&#x2011;stripInput<#csq>
command line option (see page
<xnpage id="running.option.stripInput" file="running" />),
or N if not set.</propertyDefault>

</property>

@------------------------------------------------------------
<@include> ${CTEXT}/imageCache.txt
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="imageCache"
	long="imageCache"
	value="N | Y" >

<pdp>
<#ic.para1>
</pdp>


<pdp>
<#ic.para2>
</pdp>

<propertyValueKeyList
	>
<vk key="N"><#ic.para2.N></vk>
<vk key="Y"><#ic.para2.Y></vk>
</propertyValueKeyList>

<pdp>
<#ic.para3>
</pdp>


<pdp>
<#ic.para4>
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property short="tENC"
	long="textEncoding"
	value="utf-8 | wcp-1252 | us-ascii | iso-8859-1"
	>

<pdp>
Specify the default document text processing mode.
The option values may be in either uppercase or
lowercase. See the description of the
<xcmdopt opt="tENC" />
 command line option on page
<xnpage id="running.option.tENC" file="running" />
 for details.
<vidx id="Character encoding" />
<vidx id="Text encoding" />
</pdp>

<propertyDefault>utf-8</propertyDefault>
</property>

@------------------------------------------------------------
<property short="userapi"
	long="userapi"
	value="string"
	>

<pdp>
Name of user API client to run prior to print/save.
See the <#osq>&#x2011;userapi<#csq> command line option on page 
<xnpage id="running.option.userapi" file="running" />.
</pdp>

<propertyDefault>from -userapi command line option</propertyDefault>
</property>
@------------------------------------------------------------
