@---------------------------------------------------
@- aarframe1.inc
@---------------------------------------------------
@--- FIXED
<#ifndef> achart		0
<#ifndef> this.aframe.desc	{xxxxxxx}
<#ifndef> this.aframe.code	{xxxxxxx}

<#def> tblframe.pos.options.start	{}
<#def> tblframe.pos.options.end		{}
<#def> aframe.pos.options.start		{}
<#def> aframe.pos.options.end		{}
<#def> arframe.pos.options.start	{}
<#def> arframe.pos.options.end		{}
<#def> tprefix				{}
<#def> so.option			{so}
@---------------------------------------------------
<@macro> Xtbl.p2.xref	{
	<#def> x.framelist	{(or anchored reference frame, anchored
table frame or anchored text frame)}
	@if(<#CodeName1> = Tbl) {
		<#def> tbl.a2.xref		{<MkDest fmt="X" id="TblFrame.option.A2"></MkDest>}
		<#def> tbl.p2.xref		{<MkDest fmt="X" id=T"blFrame.option.P2"></MkDest>}
		<#def> tblframe.pos.options.start	{<MkDest fmt="X" id="TblFrame.pos.options.start"></MkDest>}
		<#def> tblframe.pos.options.end		{<MkDest fmt="X" id="TblFrame.pos.options.end"></MkDest>}
		<#def> tprefix				{f}
		<#def> so.option			{fso}
		}
	@else	{
		<#def> tbl.a2.xref	{}
		<#def> tbl.p2.xref	{}
		}
	@if(<#CodeName1> = TblTextFrame) {
		<#def> tprefix				{f}
		}
	@if(<#CodeName1> = AFrame) {
		<#def> aframe.pos.options.start		{<MkDest fmt="X" id="AFrame.pos.options.start"></MkDest>}
		<#def> aframe.pos.options.end		{<MkDest fmt="X" id="AFrame.pos.options.end"></MkDest>}
		}
	@if(<#CodeName1> = ARFrame) {
		<#def> arframe.pos.options.start	{<MkDest fmt="X" id="ARFrame.pos.options.start"></MkDest>}
		<#def> arframe.pos.options.end		{<MkDest fmt="X" id="ARFrame.pos.options.end"></MkDest>}
		}
	@if(<#CodeName1> = MifFrame) {
		<#def> x.framelist	{}
		}
}
@---------------------------------------------------
<@macro> Ytbl.p2.xref	{
	<#def> x.framelist	{(or anchored reference frame, anchored
table frame or anchored text frame)}
	@if(<#CodeName1> = MifFrame) {
		<#def> x.framelist	{}
		}
}
@---------------------------------------------------
<|tbl.p2.xref>
@---------------------------------------------------

@------------------------
<xOptGroup
	shortTitle="Frame location"
	longTitle="Frame location options"
	XRefID="aframe.optgroup.wrapping"
	/>


@------------------------
<xOption short="P" long="framePosition"
	value="b | T | B | I | R | OC | OF"
	four="AFrame|inline"
>

<oP>
Position of
anchored frame
@- (or anchored reference frame, anchored
@- table frame or anchored text frame)
<#x.framelist>
<#this.aframe.code>
relative to anchor, or
insertion, point.
<#tblframe.pos.options.start>
<#aframe.pos.options.start>
<#arframe.pos.options.start>
<#tbl.p2.xref>
@- <shortx>
</oP>


<optionValueKeyList first="3mm"
	second="12mm"
	newRow="1"
	optvals="Y"
	col1="Value"
	col2="Description" >
<vk key="b">Inside text column, below current line</vk>
<vk key="T">Inside text column, top of text column</vk>
<vk key="B">Inside text column, bottom of text column</vk>
<vk key="I">At insertion point</vk>
<vk key="R">Run into paragraph</vk>
<vk key="OC">Outside of text column</vk>
<vk key="OF">Outside of text frame</vk>
@- <vkEnd  rowOpts="" />
</optionValueKeyList>

@- </optionValueKeyList>
@- <xOptDefault>b, below current line</xOptDefault>

@- <xOptPara/>
<oP>
When the <on>P</on> option is set to any of the above values
apart from <ov>I</ov>, the horizontal
position of the <#this.aframe.desc> may be controlled
using the <on>A</on>, alignment, option
(see below).
</oP>

@- <xOptPara/>
<oP>
When <on>P</on> is set to <ov>I</ov>, <ov>OC</ov> or <ov>OF</ov> the horizontal
position of the <#this.aframe.desc> may be 
further adjusted using the <on><#so.option></on> option (see below).
</oP>

@- <xOptPara/>
<oP>
When <on>P</on> is set to <ov>R</ov> the top of the
<#this.aframe.desc> is aligned with
the top of the text (at caps height) in the 
first line of the paragraph.
</oP>


@- <xOptPara/>
<oP>
When the <on>P</on> option is set
to <ov>I</ov>, <ov>OF</ov> or <ov>OC</ov> the
bottom of the <#this.aframe.desc> is
aligned with the baseline
of the text containing the anchor, plus or minus
the distance specified using the <on>bo</on> baseline
offset option (see below), unless
this would cause the top of
the <#this.aframe.desc> to be higher than the top
of the text column containing the anchor.
In the latter case the top of the text
column acts as a ceiling. An exception
to this rule occurs when <on>P</on> is set to <ov>I</ov> and
the paragraph line spacing is set
to be <ov>fixed</ov>. In this case top of the
<#this.aframe.desc> may extend
above the top of the text column.
</oP>
</xOption>



@-----------------------------
<xOption short="<#tprefix>F" long="frameFloat" value="N | Y" >

<oP>
Allow the <#this.aframe.desc> to float.
</oP>

<oP>
Effective only if the <on>P</on> option is 
set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>.
</oP>

<xOptDefault>N</xOptDefault>
</xOption>


@-----------------------------
<xOption short="A" long="horizAlign"
	value="L | C | R | nb | fb | np | fp"
	four="AFrame|inline"
	>

<oP>
Horizontal alignment of <#this.aframe.desc>.
<#tbl.a2.xref>
</oP>

<oP>
Use the <on><#optRef></on> option to control horizontal
positioning, either within or outside
text columns and text frames.
</oP>

<oP>
@- <xOptPara/>
<on>P</on> option values have different effects,
depending on the value of the <on>P</on> option
(see above). The <on>P</on> option is ignored if
the <on>P</on> option is set to <ov>I</ov>.
<vidx id="alignment||in anchored frames" />
<vidx id="anchored frames||alignment of" />
</oP>

@- <xOptPara/>

<optionValueKeyList first="3mm"
	second="12mm"
	newRow="1"
	optvals="Y"
	col1="Value"
	col2="Horizontal alignment" >
@---------------------------------------------------
<vk key="L">If <on>P</on> set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>:<br/>
at left, inside text column(s)</vk>

@- <vk key="&nbsp;">If <on>P</on> set to <ov>OF</ov>:<br/>
<vk key="">If <on>P</on> set to <ov>OF</ov>:<br/>
at left, outside text frame</vk>

@---------------------------------------------------
@--- <#small_spacer_para> is defined in mmp.stringdefs
@---------------------------------------------------
<vk key="">If <on>P</on> set to <ov>OC</ov>:<br/>
at left, outside text column</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="R">If <on>P</on> set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>:<br/>
at right, inside text column(s)</vk>

@---------------------------------------------------
<vk key="">If <on>P</on> set to <ov>OF</ov>:<br/>
at right, outside text frame</vk>

@---------------------------------------------------
<vk key="">If <on>P</on> set to <ov>OC</ov>:<br/>
at right, outside text column</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="C">If <on>P</on> set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>:<br/>
centered, inside text column(s)</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="nb">If <on>P</on> <#fI>is set to any value other than<#fnI> <ov>I</ov>:<br/>
align on side nearest page binding</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="fb">If <on>P</on> <#fI>is set to any value other than<#fnI> <ov>I</ov>:<br/>
align on side farthest from page binding</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="np">If <on>P</on> is set to <ov>OF</ov> or <ov>OC</ov>:<br/>
align on side nearest page edge</vk>
@- <#small_spacer_para>

@---------------------------------------------------
<vk key="fp">If <on>P</on> is set to <ov>OF</ov> or <ov>OC</ov>:<br/>
align on side farthest from page edge</vk>

</optionValueKeyList>
@- <vkEnd/>


<xOptDefault><ov>C</ov>, or <ov>L</ov> when <on>P</on> is set
to <ov>R</ov>, <ov>OF</ov> or <ov>OL</ov>.
<#tblframe.pos.options.end>
<#aframe.pos.options.end>
<#arframe.pos.options.end>
</xOptDefault>

</xOption>


@-----------------------------
<xOption short="<#tprefix>so" long="sideOffset" value="dim" >
<oP>
Offset of <#this.aframe.desc> from side of text column or
text frame.
</oP>

<oP>
Effective only if the <on>P</on> option is 
set to <ov>OC</ov> or <ov>OF</ov>.
Value may be positive or negative.
</oP>

<xOptDefault>0</xOptDefault>
</xOption>


@-----------------------------
<xOption short="<#tprefix>bo" long="baselineOffset" value="dim" >

<oP>
Offset of <#this.aframe.desc> from text baseline.
</oP>

<oP>
Effective only if the <on>P</on> option is 
set to <ov>OC</ov>, <ov>OF</ov> or <ov>I</ov>.
Value may be positive or negative.
</oP>

<xOptDefault>0</xOptDefault>
</xOption>

@-----------------------------
<xOption short="<#tprefix>C" long="frameCrop" value="N | Y" >

<oP>
Crop <#this.aframe.desc> to fit in text column.
</oP>

<oP>
Effective only if the <on>P</on> option is 
set to <ov>b</ov>, <ov>T</ov> or <ov>B</ov>.
</oP>

<xOptDefault>N</xOptDefault>
</xOption>


@-----------------------------
<@include> ${CTEXT}/runaroundTextGap.inc
@-----------------------------
