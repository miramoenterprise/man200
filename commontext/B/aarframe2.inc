@----------------------------------------------------
@- aarframe2.inc
@----------------------------------------------------
@--- FIXED
<#ifndef> achart	0
<#ifndef> achart.desc	{chart frame}
@-----------------------------
@---------------------------------------------------
<|tbl.p2.xref>
@---------------------------------------------------

@- <@write> error {Included file zxvc:      aarframe2.inc  (<#Filename>) <#CodeName1>}
@------------------------
<xOptGroup
	shortTitle="Frame border and fill styles"
	longTitle="Frame border and fill styles"
	XRefID="aframe.optgroup.borders"
	></xOptGroup>


<xOption short="<#tprefix>pen" long="borderPen" value="0 - 15" four="AFrame|inline" >

Pen pattern number.
<MkDest id="AFrame.option.<#tprefix>pen" />
<shortx>
<P>
Sixteen pen patterns for
borders around anchored frame objects
are available,
from 0 (black) to 15 (none).
(See Appendix
<xparanumonly id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />, page
<xnpage id="A_penAndFillPats.chapter.start" file="A_penAndFillPats" />,
for a description of fill patterns.)

<xOptDefault>15</xOptDefault>
</xOption>




@-----------------------------
<xOption short="<#tprefix>pw" long="<#tprefix>penWidth" value="dim" four="AFrame|inline" >

Pen width for border.
<shortx>

<xOptPara/>
Borders around all types of anchored frame
thicken inwards towards the
center of the anchored frame.
<vidx id="anchored frames||how borders <#osq>thicken<#csq>" />
(Note that this is different to the case of
borders around a <#xFrame> and <#xImage>,
which thicken equally about their centers.)
Default units are points.

<xOptDefault>0</xOptDefault>
</xOption>


@-----------------------------
<xOption short="<#tprefix>fill" long="<#tprefix>frameFill" value="0 - 15" >

Fill pattern. As for <on>pen</on> above.
A fill value of <ov>15</ov> makes the frame transparent.

<xOptDefault>7 (opaque no fill)</xOptDefault>
</xOption>

@-----------------------------
