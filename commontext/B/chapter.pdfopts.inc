@------------------------------------------------------------
@- chapter.pdfopts.inc
@------------------------------------------------------------
@- Included in:
@- 
@- 		<Chapter>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle='PDF import (type = "PDF" <fI>only</fI>)'
	longTitle='PDF import (type = "PDF" <fI>only</fI>)'
	dest="<#elementName>.propertyGroup.pdf_options.start"
	>The following options are ignored unless
the <on>type</on> option is set to <ov>PDF</ov>.</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="pdfFile" long="pdfFilename" value="filename" >

<pdp>
<fI>filename</fI> is the name of a PDF file to include
as this chapter in the book.
</pdp>

</property>

@------------------------------------------------------------
<property short="pdfL" long="pdfLeftMargin" value="dim" >

<pdp>
Left offset of left edge of included PDF document from left
of page.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="pdfT" long="pdfTopMargin" value="dim" >

<pdp>
Top offset of top edge of included PDF document from top
of page.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="pdfW" long="pdfWidth" value="dim" >

<pdp>
Width of included PDF document.
</pdp>

<pdp>
If neither the <on>pdfW</on> option nor the <on>pdfScale</on> option
is set, the width of the included PDF document
is unchanged from its original size unless the
<on>pdfH</on> option is set, in which case the width
of the included PDF document is set to the height
of the included PDF document times its aspect ratio.
</pdp>

</property>

@------------------------------------------------------------
<property short="pdfH" long="pdfHeight" value="dim" >

<pdp>
Height of included PDF document.
</pdp>

<pdp>
If neither the <on>pdfH</on> option nor the <on>pdfScale</on> option
is set, the height of the included PDF document
is unchanged from its original size unless the
<on>pdfW</on> option is set, in which case the height
of the included PDF document is set to the width
of the included PDF document divided by its aspect ratio.
</pdp>

</property>

@------------------------------------------------------------
<property short="pdfScale" long="pdfScale" value="percent" >

<pdp>
Scale the width and height of the
included PDF document.
</pdp>

<pdp>
The <on>pdfScale</on> option is ignored if either
the <on>pdfW</on> option or the <on>pdfH</on> option is set.
</pdp>

</property>

@------------------------------------------------------------
<property short="pdfStart" long="pdfStartPageNum" value="int" 
	longStretch="97"
	>

<pdp>
The number of the first page in the PDF document
to include.
</pdp>

<pdp>
If the value given to <on>pdfStart</on> is greater
than the number of pages in the PDF file, the
last page in the file is assumed.

The first page in a PDF document is always 1.
</pdp>

<propertyDefault>1</propertyDefault>
</property>

@------------------------------------------------------------
<property short="pdfEnd" long="pdfEndPageNum" value="int" >

<pdp>
The number of the last page in the PDF document
to include.
</pdp>


<pdp>
If the value given to <on>pdfEnd</on> is greater
than the number of pages in the PDF file, the
last page in the file is assumed.
</pdp>

<propertyDefault>last page in PDF document</propertyDefault>
</property>

@------------------------------------------------------------
<property short="pdfDS" long="pdfDoubleSided" value="N | Y" >

<pdp>
Set to <ov>Y</ov> to include PDF document as double-sided.
Ignored unless the <on>type</on> option is set to <ov>PDF</ov>
(see page
<xnpage id="Chapter.option.type" />
).
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------
