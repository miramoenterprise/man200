@------------------------------------------------------------
@- chapter.tbl_footnote_numbering.inc
@------------------------------------------------------------
<@include> ${CTEXT}/def.template.inc

@------------------------------------------------------------
<propertyGroup
	shortTitle="Table footnote (tblFNote) numbering"
	longTitle="Table footnote numbering options"
	dest="<#elementName>.propertyGroup.table_footnote_numbering"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="TFN" long="tblFNoteNum"
	four="Chapter|inline"
        value="Restart | File"
>

<pdp>
Specifies how to compute sequential table footnote number
values.
</pdp>

<propertyValueKeyList
>

<vk key="Restart" >Restart table footnote numbering using
the values specified by the <on>TFNstyle</on> option, or its default.</vk>

<vk key="File" >Read footnote numbering style from file 
(template).</vk>

</propertyValueKeyList>

<propertyDefault>Restart</propertyDefault>
</property>


@------------------------------------------------------------
<property short="TFNstyle" long="tblFNoteStyle"
	four="Chapter|inline"
        value="A | UA | LA | UR | LR | C" >

<pdp>
Sets footnote number style.
If <on>TFNstyle</on> is set to <ov>C</ov>, the value of the <on>TFNstring</on>
option, or its default, is used for the table footnote numbering style.
(See Notes
<xparanumonly id="note.custom_footnote_string" file="inline" />
 and
<xparanumonly id="note.footnote_start_number" file="inline" />
, starting on page
<xnpage id="note.custom_footnote_string" file="inline" />
).
</pdp>

<propertyValueKeyList>
<vk key="A" >Arabic.</vk>
<vk key="UA" >Uppercase alpha.</vk>
<vk key="LA" >Lowercase alpha.</vk>
<vk key="UR" >Uppercase roman.</vk>
<vk key="LR" >Lowercase roman.</vk>
<vk key="C" >Custom.</vk>
</propertyValueKeyList>

<pdp>
The <on>TFNstyle</on> option is effective only if the <on>TFN</on> option is
set to <ov>Restart</ov>.
</pdp>

<propertyDefault>A</propertyDefault>

</property>

@------------------------------------------------------------
<property short="TFNstring" long="tblFNoteSymbols"
	four="Chapter|inline"
        value="string" >

<pdp>
<fI>string</fI> is an array of characters
to be used for sequential table footnote numbering
(see Note
<xparanumonly id="note.custom_footnote_string" file="inline" />
 on page
<xnpage id="note.custom_footnote_string" file="inline" />
).
</pdp>


<pdp>
Ignored if the <on>TFNstyle</on> option is set to <ov>C</ov>
or the <on>TFN</on> option is set to <ov>File</ov>.
</pdp>

<propertyDefault>"*\xa0 \e0 "</propertyDefault>

</property>
@------------------------------------------------------------
