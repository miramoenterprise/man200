@---------------------------------------------
@-- p.hyphenation.inc
@---------------------------------------------
@--- FIXED


@-----------------------------
<propertyGroup
	shortTitle="Hyphenation"
	longTitle="Hyphenation options"
	dest="p.optgroup.hyphenation"
	/>


@-----------------------------
<property short="H" long="hyphenate" value="Y | N" >

<pdp>
Turn on hyphenation.
</pdp>

</property>

@-----------------------------
<property short="Hml" long="maxHyphens" value="int" >

<pdp>
<vidx id="paragraph||hyphenation in" />
<vidx id="text||hyphenating" />
<vidx id="hyphenation" />
Maximum number of adjacent
hyphenated lines.
</pdp>

</property>

@-----------------------------
<property short="Hmp" long="minBeforeHyphen"
	value="int"
	longStretch=96
	>

<pdp>
Minimum number of characters
before hyphen.
</pdp>

</property>

@-----------------------------
<property short="Hms" long="minAfterHyphen"
	value="int"
	longStretch=97
	>

<pdp>
Minimum number of characters
after hyphen.
</pdp>

</property>

@-----------------------------

<property short="Hmw" long="minWordSize" value="int" >
<pdp>
Minimum word size
to hyphenate.
</pdp>

</property>

<@skipStart>
@-----------------------------
<propertyGroup
	shortTitle="Language"
	longTitle="Language"
	dest="p.optgroup.hyphenation"
	></propertyGroup>

@-----------------------------
<property short="lang" long="textLanguage" value="key" four="P|inline" >

<pdp>
Paragraph default language.
<vidx id="paragraph||language" />
<vidx id="language||paragraph" />
</pdp>
@- <shortx>

<pdp>
<Font fmt="F_Italic">key</Font> may be any
one of the following:
</pdp>

@- <xOptPara rowOpts="ctm=2pt cbm=3pt" pgfOpts="ls=P li=5mm fi=5mm" />
@- <Include file=${CTEXT}/lang.keywords.inc />
@- <xOptPara/>
<pdp>
Hyphenation and spell checking is suppressed if the <on>lang</on> option is set
to <ov>None</ov> or <ov>NoLanguage</ov>.
<vidx id="hyphenation||suppressing" />
</pdp>

</property>
<@skipEnd>
