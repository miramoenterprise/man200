@------------------------------------------------------------
@- frameBorder.inc
@------------------------------------------------------------

@------------------------------------------------------------
@- <|tbl.p2.xref>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Frame border"
	longTitle="Frame border"
	dest="aframe.propertyGroup.borders"
	></propertyGroup>

@------------------------------------------------------------
<property short="penWidth"
	long="penWidth" value="dim"
	>
<docNote>
How do the properties interact?
Default values?
</docNote>

<pdp>
Pen width for border.
</pdp>

<pdp>
Borders around all types of anchored frame
thicken inwards towards the
center of the anchored frame.
<vidx id="anchored frames||how borders thicken" />
(In contrast with
borders around a <#xFrame> and <#xImage>,
which thicken uniformly about their centers.)
Default units are points.
</pdp>

<propertyDefault><ov>0</ov></propertyDefault>
</property>

@------------------------------------------------------------
<property short="penColor"
	long="penColor" value="color"
	>

<pdp>
Pen color for border.
</pdp>

<propertyDefault><ov>Black</ov></propertyDefault>
</property>

@------------------------------------------------------------
<property short="penOpacity"
	long="penOpacity" value="0 | 100"
	>

<pdp>
<ov>0</ov> is transparent.
<ov>100</ov> is solid fill.
</pdp>

<propertyDefault>0</propertyDefault>
</property>



@------------------------------------------------------------
<property short="penTint"
	long="penTint" value="percent"
	>

<pdp>
Border pen tint value, in the range
<ov>100</ov> to 
<ov>0</ov>.
</pdp>

<propertyDefault>100</propertyDefault>
</property>

