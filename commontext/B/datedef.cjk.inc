@--------------------------------------
@-- datedef.cjk.inc
@--------------------------------------
@--- FIXED

@-----------------------------
<@BBlock> {
&lt;daynumkanjikazu/>
}{
Kanji day number.
} {daynumkanjikazu}

@-----------------------------
<@BBlock> {
&lt;daynumkanjinumeric/>
}{
Kanji numeric day number.
} {daynumkanjinumeric}

@-----------------------------
<@BBlock> {
&lt;monthnumkanjikazu/>
}{
Kanji month number.
} {monthnumkanjikazu}

@-----------------------------
<@BBlock> {
&lt;monthnumkanjinumeric/>
}{
Kanji numeric month number.
} {monthnumkanjinumeric}

<!-- XXXCRIN
@-----------------------------
<@BBlock> {
&lt;imperialera/>
}{
Imperial era.
The current imperial era is:
} {imperialera}
-->

@-----------------------------
<@BBlock> {
&lt;imperialyear01/>
}{
Imperial year. Number of years
since the accession of the current Emperor of Japan.
} {imperialyear01}

<!--
@-----------------------------
<@BBlock> {
&lt;imperialyearkanjikazu/>
}{
}

@-----------------------------
<@BBlock> {
&lt;imperialyearspecialkanjikazu/>
}{
}

@-----------------------------
<@BBlock> {
&lt;imperialyearspecialkanjinumeric/>
}{
E.g.
}

@-----------------------------
<@BBlock> {
&lt;imperialyearkanjinumeric/>
}{
E.g.
}
-->

@-----------------------------
