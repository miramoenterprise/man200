@------------------------------------------------------------
@-- fillProperties.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Background fill properties"
	longTitle="Background fill properties"
	dest="<#elementName>.optgroup.fillProperties"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="fillColor" long="fillColor" value="name" >

<pdp>
Background fill color.
<fI>name</fI> is a color defined
using <#xColorDef>.

</pdp>

</property>

@------------------------------------------------------------
<property short="fillTint" long="fillTint" value="percent" >

<pdp>
Background fill tint as a <fI>percent</fI> value in the range 0&endash;100.
A tint value of 0 (zero) is the color white irrespective of
the setting of the <on>fillColor</on> property.
</pdp>

</property>

@------------------------------------------------------------
<property short="fillOpacity" long="fillOpacity" value="0 | 100" >

<pdp>
A value of 100 sets the fill color to 100% opaque.
A value of 0 sets the fill color to 100% transparent.
</pdp>

</property>
@------------------------------------------------------------
