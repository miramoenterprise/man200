@--------------------------------------
@-- bb.fontref.inc
@--------------------------------------
@--- FIXED

@-----------------------------
<@BBlock> {
&lt;FontRef fmt=string > ... &lt;/FontRef>
}{
The <#xFontRef> in-text markup code may be used
with the <on>fmt</on> option set to the
name of a pre-defined font format.
<#xFontRef> codes may not be nested.
} {FontRef}
@-----------------------------
