@------------------------------------------------------------
@-- penProperties.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> penPropertiesTextDef {
	<#def> pp.text1				{Border}
	<#def> pp.text2				{border}
	<#def> pp.text3				{The center of the pen width
	<#def> pp.penWidthDefault		{}
is aligned with the frame border.}
	@----------------------------------------------------
	@if({<#elementName>} = {ArrowDef}) {
		<#def> pp.text1			{Arrow}
		<#def> pp.text2			{arrow}
		<#def> pp.text3			{}
		}
	@----------------------------------------------------
	@if({<#elementName>} = {Line}) {
		<#def> pp.text1			{Line}
		<#def> pp.text2			{line}
		<#def> pp.text3			{}
		<#def> pp.penWidthDefault	{<propertyDefault v="0.5pt" />}
		}
	@----------------------------------------------------
}
@------------------------------------------------------------
<|penPropertiesTextDef>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="<#pp.text1> pen properties"
	longTitle="<#pp.text1> pen properties"
	dest="<#elementName>.optgroup.penProperties"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="pw" long="penWidth" value="dim" >

<pdp>
Width of <#pp.text2> pen.
<#pp.text3>
</pdp>

<#pp.penWidthDefault>

</property>

@------------------------------------------------------------
<property short="" long="penStyle" value="solid | dotted | dashed" >

<pdp>
<#pp.text1> pen style.
</pdp>

<propertyDefault>solid</propertyDefault>

</property>

@------------------------------------------------------------
<property short="penColor" long="penColor" value="name" >

<pdp>
Pen color. <fI>name</fI> is a color defined
using <#xColorDef>.
</pdp>

</property>

@------------------------------------------------------------
<property short="penTint" long="penTint" value="percent" >

<pdp>
Pen tint as a <fI>percent</fI> value in the range 0&endash;100.
A tint value of 0 (zero) is the color white irrespective of
the setting of the <on>penColor</on> property.
</pdp>

</property>

@------------------------------------------------------------
<property short="penOpacity" long="penOpacity" value="0 | 100" >

<pdp>
A value of 100 sets the pen color to 100% opaque.
A value of <ov>0</ov> sets the pen color to 100% transparent.
(A value of <ov>0</ov> is the same
as setting <on>penWidth</on> to <ov>0</ov>.)
</pdp>

</property>
