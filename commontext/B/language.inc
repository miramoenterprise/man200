@------------------------------------------------------------
<!-- File = language.inc (start) -->
@------------------------------------------------------------
@- <@include> ${CTEXT}/def.template.inc
@------------------------------------------------------------
<@xmacro> languageContext {
	<#def> languageContext	{context}
	@if({<#elementName>} = {Font} ) {
		<#def> languageContext	{font}
		}
	@if({<#elementName>} = {P} ) {
		<#def> languageContext	{paragraph}
		}
	@if({<#elementName>} = {ParaDef} ) {
		<#def> languageContext	{paragraph}
		}
	@if({<#elementName>} = {MiramoXML} ) {
		<#def> languageContext	{document}
		}
	@- @print({<#languageContext>}, n)
}{}
@------------------------------------------------------------
<languageContext/>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Language context"
	longTitle="Language context"
	dest="<#elementName>.propertyGroup.language_context"
	/>


@------------------------------------------------------------
<property
	long="language"
	value="key" >

<pdp>
Set the <#languageContext> language to <fI>key</fI>.
The <fI>key</fI> values for the <on>language</on> property are
@- listed in Table <xparanumonly
listed in <xparalabel
	id="tbl.list_of_language_values.start" />
on pages <xnpage
	id="tbl.list_of_language_values.start" /><xphyphen><xnpage
	id="tbl.list_of_language_values.end" />.
<vidx id="text||language" />
<vidx id="language||text" />
<vidx id="<#languageContext> language" />
</pdp>

<pdp>
The role of the <on>language</on> property is described
in <xchapter id="language_property" />.
</pdp>

@- <propertyDefault>File</propertyDefault>
</property>
@------------------------------------------------------------
<!-- File = language.inc (end) -->
@------------------------------------------------------------
