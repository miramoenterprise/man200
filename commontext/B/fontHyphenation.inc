@---------------------------------------------
@-- fontHyphenation.inc
@---------------------------------------------


@-----------------------------
<propertyGroup
	shortTitle="Hyphenation"
	longTitle="Hyphenation options"
	dest="p.optgroup.hyphenation"
	/>


@-----------------------------
<property short="H" long="hyphenate" value="Y | N"
	condition="mmcGuide"
	>

<pdp>
Set <on>hyphenate</on> to <ov>N</ov> to suppress hyphenation
when the parent <#xP> element has <on>hyphenate</on> set to <ov>Y</ov>.
</pdp>

<pdp>
If the parent <#xP> element has <on>hyphenate</on> set to <ov>N</ov>.
the <#xFont> <on>hyphenate</on> property has no effect.
In this case long words may be broken across lines (without a hyphen) using the ZWNJ
character.
</pdp>

<propertyDefault>Inherited from parent <#xP> element</propertyDefault>
</property>

<@skipStart>
@-----------------------------
<property short="Hml" long="maximumHyphenLines" value="int" >

<pdp>
<vidx id="paragraph||hyphenation in" />
<vidx id="text||hyphenating" />
<vidx id="hyphenation" />
Maximum number of adjacent
hyphenated lines.
</pdp>

</property>

@-----------------------------
<property short="Hmp" long="minimumBeforeHyphen"
	value="int"
	longStretch=96
	>

<pdp>
Minimum number of characters
before hyphen.
</pdp>

</property>

@-----------------------------
<property short="Hms" long="minimumAfterHyphen"
	value="int"
	longStretch=97
	>

<pdp>
Minimum number of characters after hyphen.
</pdp>

</property>

@-----------------------------

<property short="Hmw" long="minimumWordSize" value="int" >
<pdp>
Minimum word size to hyphenate.
</pdp>

</property>

@-----------------------------
<propertyGroup
	shortTitle="Language"
	longTitle="Language"
	dest="p.optgroup.hyphenation"
	></propertyGroup>

@-----------------------------
<property short="lang" long="textLanguage" value="key" four="P|inline" >

<pdp>
Paragraph default language.
<vidx id="paragraph||language" />
<vidx id="language||paragraph" />
</pdp>

<pdp>
<Font fmt="F_Italic">key</Font> may be any
one of the following:
</pdp>

<pdp>
Hyphenation and spell checking is suppressed if the <on>lang</on> option is set
to <ov>None</ov> or <ov>NoLanguage</ov>.
<vidx id="hyphenation||suppressing" />
</pdp>

</property>
<@skipEnd>
