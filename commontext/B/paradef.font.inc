@---------------------------------------------
@-- paradef.font.inc
@---------------------------------------------
@--- FIXED


@-----------------------------
<xOptGroup
	shortTitle="Font options"
	longTitle=""
	XRefID="<#CodeName1>.optgroup.font_options"
	>
(In addition to the <on>F</on> font option listed
below, the <#xParaDef> code may also
use all <#xFontDef> options &emdash;
 see page 
<xnpage id="FontDef.code.start" file="fdefs" />.)
<MkDest id="ParaDef.code.font options" />
</xOptGroup>

@-----------------------------
<xOption short="F" long="fontFormat"
	four="P|inline"
        value="name" >
<vidx id="paragraph||font tag (F=name)" />
<vidx id="font tag" />
Font format name.
<shortx>
<#fI>name<#fnI> may be either
the name of a font definition
specified using the <#xFontDef> font
definition code or a character tag
name assigned in the <#osq>Character Designer<#csq>
of the template file.

<xOptPara/>
Note that other <#xFontDef> options
override the settings of the <on>F</on> option
only if they are placed after the <on>F</on> option
in the sequence of options following the <#xParaDef>
code.
</xOption>

