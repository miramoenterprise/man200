@-------------------------------------------------
@-- cellTextRotate.inc
@-------------------------------------------------
@- The following defs define text for when used
@--- FIXED

@-----------------------------
<@macro> cellTextRotateGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Cell text and margin rotation"
			longTitle="Cell text and margin rotation"
			dest="<#elementName>.optgroup.cell_rotation"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Cell text and margin rotation<br/>(default for all cells in row)"
			longTitle="Cell text and margin rotation (default for all cells in row)"
			dest="<#elementName>.optgroup.cell_rotation"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Cell text and margin rotation<br/>(default for all cells in table)"
			longTitle="Cell text and margin rotation (default for all cells in table)"
			dest="<#elementName>.optgroup.cell_rotation"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellTextRotateGroupHead>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="a"
	long="cellOrientation"
	value="0 | 270 | 90 | 180"
	condition="fmcGuide"
	>

<pdp>
Orientation of text in cell. Rotation is clockwise.
<vidx id="angle||in table cells" />
<vidx id="rotation||in table cells" />
<vidx id="table||rotation of text in" />
<vidx id="table||rotation of cell margins" />
<vidx id="margins||rotating in table cells" />
</pdp>


<pdp>
Note that the placement orientation of cell
rulings (as specified by the <on>lr</on>,
<on>tr</on>, <on>rr</on> and <on>br</on> options) always remains
fixed regardless of cell text orientation.
</pdp>


<pdp>
Cell margins, as specified by both
the <#xTbl> and <#xCell> cell margin options
(<on>clm</on>,
<on>ctm</on>,
<on>crm</on>
and
<on>cbm</on>)
<Font fmt="F_Italic">do</Font> rotate
</pdp>

<propertyDefault>none</propertyDefault>
</property>

@------------------------------------------------------------
