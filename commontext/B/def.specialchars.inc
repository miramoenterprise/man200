@---------------------------------------
@- def.specialchars.inc
@---------------------------------------
@--- FIXED
<#def> specialchars.inc {
Special characters
@- may be encoded as described on 
@- page
@- <xnpage id="Character encoding types" file="ch_builtin" />
@- , using the 8-bit ISO Latin-1 character,
may be encoded using
the character entity name string
(&amp;<#fI>name<#fnI>;), or the 
numeric character reference (&amp;#<#fI>nn<#fnI>;
or &amp;#x<#fI>hh<#fnI>;).
However if the requested character is not
available in the current font, it will
be ignored with a warning message.
}
