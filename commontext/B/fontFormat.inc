@------------------------------------------------------------
@- fontFormat.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> fontFormatdefs {
	<#def> shortFormatName	{fmt}
	@if(@match({<#elementName>}, {HyperCmd})) {
		<#def> shortFormatName	{F}
		}
}
@------------------------------------------------------------
<|fontFormatdefs>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Reference pre-defined font format"
	longTitle="Reference pre-defined font format"
	dest="<#elementName>.propertyGroup.reference_font_format"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fmt"
	long="fontFormat"
	value="name"
	>

<pdp>
Font format name.
<vidx id="paragraph||font format name" />
<vidx id="font format" />
</pdp>




<pdp>
<#name> is the name of a font format
defined either in a template file using the
Character Designer in a FrameMaker template file
or by using the <#xFontDef> element
(see pages
<xnpage
	id="FontDef.element.start"
	file="fdefs" /><xphyphen/><xnpage
	id="FontDef.element.end"
	file="fdefs" />).
</pdp>

<pdp element="P|TextLine|ALine" >
See the description of <on><#propertyName></on> in the <#xFont> element.
</pdp>


<pdp element="Font" >
The following:
</pdp>

<exampleBlock
	element="Font"	@--- Include example only if element is 'Font'
>
This is <Font fmt="F_Bold" >warning</Font> text ...
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>


<pdp element="Font" >
The above example assumes a pre-existing <#osq>F_Bold<#csq> font format
definition.
</pdp>

<pdp element="Font" >
To revert to the default paragraph font format set
the <on>fmt</on> property to the empty string,
i.e. &lt;Font fmt="" > (no spaces between the ""). For example<vidx
	id="default paragraph font, reverting to" />
</pdp>

<exampleBlock
	element="Font"
	tabs="5|10|15|20"
>
<Font p="10pt">10 point <Font fcolor="Red">Red text</Font>
</Font>then back into the default paragraph font.
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>



</property>
@------------------------------------------------------------

