@-------------------------------------------------
@-- cell.horiz_align.inc
@-------------------------------------------------
@--- FIXED

<xOption short="A" long="pgfAlign" value="L | C | R" >

<vidx id="paragraph alignment||in table cells" />
<vidx id="text alignment||in table cells" />
<vidx id="alignment||table cell, horizontal" />
Left, center or right (horizontal) cell paragraph
text alignment.
Overrides default paragraph alignment
and &lt;TblColA> for all paragraphs in cell,
unless overridden by a subsequent <#xP>
code with a different <on>A</on> setting.
<xOptPara/>
(Vertical text alignment may be controlled using
the <on>Ca</on> option. See page
<xnpage id="Cell.option.Ca" />
.)

</xOption>
