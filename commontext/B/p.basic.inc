@---------------------------------------------
@-- p.basic.inc
@---------------------------------------------
@--- FIXED

@-------------------------------------
<xOptGroup
	shortTitle="Basic options"
	longTitle="Basic options"
	XRefID="cell.optgroup.basic"
	></xOptGroup>

@-----------------------------
<xOption short="A" long="justify" value="L | R | LR | C" >

<oP>
Justification.
<vidx id="paragraph||justification in" />
<vidx id="justification||in paragraphs" />
<vidx id="text alignment||paragraph" />
<vidx id="alignment||paragraph" />
<vidx id="paragraph||centering text (A=C)" />
<vidx id="paragraph||alignment" />
<vidx id="centering text||in paragraphs (A=C)" />
<ov>L</ov><#nbsp>=<#nbsp>left only,
<ov>R</ov><#nbsp>=<#nbsp>right only,
<ov>LR</ov><#nbsp>=<#nbsp>left and right,
<ov>C</ov><#nbsp>=<#nbsp>center.
</oP>

</xOption>

@-----------------------------
<xOption short="fi" long="firstIndent" value="dim" four="P|inline" >

<oP>
First line indent.
<vidx id="paragraph||first indent (fi)" />
<vidx id="paragraph||indentation" />
</oP>
@- <shortx>
<oP>
Distance of
the first line of text from the left side
of a text column or from the left
margin of a table cell.
</oP>

</xOption>

@-----------------------------
<xOption short="li" long="leftIndent" value="dim" four="P|inline" >

<oP>
Left indent (excluding the first line
of the paragraph).
<vidx id="paragraph||left indent (li)" />
@- <shortx>
</oP>
<oP>
Distance of the
second and succeeding lines of text from
the left side of a text column or from the left
margin of a table cell.
</oP>

</xOption>

@-----------------------------
<xOption short="ri" long="rightIndent" value="dim" four="P|inline" >

<oP>
Right indent.
<vidx id="paragraph||right indent (ri)" />
</oP>
@- <shortx>
<oP>
Distance of lines of text from
the right side of a text column or from the right
margin of a table cell.
</oP>

</xOption>

@-----------------------------
<xOption short="sa" long="spaceAbove" value="dim" four="P|inline" >

<oP>
Space above.
<vidx id="paragraph||space above (sa)" />
<vidx id="space||above paragraph (sa)" />
</oP>
@- <shortx>
<oP>
Note that actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space below
the preceding paragraph, not the sum of the two.
(This assumes that feathering between paragraphs is turned off.)
</oP>

</xOption>

@----------------------------
<xOption short="sb" long="spaceBelow" value="dim" four="P|inline" >

<oP>
Space below.
<vidx id="paragraph||space below (sb)" />
<vidx id="space||below paragraph (sb)" />
@- <shortx>
See note above.
</oP>

</xOption>


@-----------------------------
<xOption short="un" long="pgfUseNext" value="N | Y" four="" >

<oP>
Use next paragraph.
<vidx id="paragraph||next paragraph (np, un)" />
</oP>

<odefault v="N" />
</xOption>

@-----------------------------
<xOption short="np" long="pgfNext" value="name" four="" >

<oP>
<vidx id="paragraph||next paragraph (np, un)" />
Next paragraph tag.
</oP>

<odefault>Null</odefault>
</xOption>

@-----------------------------
<xOptGroup
	shortTitle="Interline leading and line spacing mode"
	longTitle="Interline leading and line spacing mode"
	XRefID="MkDest.optgroup.line_spacing"
/>
@-----------------------------


@-----------------------------
<xOption short="l" long="leading" value="dim" four="P|inline" >

<oP>
Leading. Default units are points.
<vidx id="paragraph||leading (l)" />
<vidx id="paragraph||line spacing (l)" />
<vidx id="leading, space between lines" />
<vidx id="line spacing" />
@- <shortx>
</oP>

@- <xOptPara/>
<oP>
Leading is the space <#fI>between<#fnI> lines
within a paragraph. Line spacing
(<#fI>baseline<#fnI> to <#fI>baseline<#fnI>)
is leading plus point size of the text
(see Figure
<xparanumonly id="fig.inter_paragraph_spacing" file="textpos" />
 on page
<xnpage id="fig.inter_paragraph_spacing" file="textpos" />
 in the <#UGuide>).
</oP>

</xOption>

@-----------------------------
<xOption short="ls" long="lineSpacing" value="F | P" four="P|inline" >

<oP>
Line spacing.
<vidx id="paragraph||line spacing (fixed, ls=F)" />
<vidx id="paragraph||line spacing (leading, l)" />
<vidx id="line spacing in paragraph text||leading, ls=l" />
<vidx id="line spacing in paragraph text||floating, ls=P" />
<vidx id="line spacing in paragraph text||fixed, ls=F" />
<vidx id="line spacing in paragraph text||fixed, ls=F" />
<vidx id="anchored frames||inline" />
</oP>
@- <shortx>
<oP>
<ov>F</ov><#nbsp>=<#nbsp>fixed
(no adjustment for extra large characters, superscripts,
inline anchored frames, etc.),
<ov>P</ov><#nbsp>=<#nbsp>proportional.
Setting the <on><#optRef></on> option to <ov>P</ov> prevents
contact between text lines in cases where
the text contains either larger than normal
characters or inline anchored frames.
</oP>

</xOption>
@-----------------------------
<xOptGroup/>
