@------------------------------------------------------------
<!-- File = chapter.paragraphNumbering.inc (start) -->
@------------------------------------------------------------
<@include> ${CTEXT}/def.template.inc

@------------------------------------------------------------
<propertyGroup
	shortTitle="Paragraph numbering"
	longTitle="Paragraph numbering options"
	dest="<#elementName>.propertyGroup.paragraph_numbering"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	@- short="PN"
	long="paragraphNumberStart" 
	value="increment | int " >

<pdp>
Set start number for autonumbered paragraphs.
</pdp>

<@skipStart>
<propertyValueKeyList
>
<vk key="Cont" >Continue.</vk>
<vk key="Restart" >Restart from 1.</vk>
</propertyValueKeyList>
<propertyDefault>Restart</propertyDefault>
<@skipEnd>

</property>
@------------------------------------------------------------
<!-- File = chapter.paragraphNumbering.inc (end) -->
@------------------------------------------------------------
