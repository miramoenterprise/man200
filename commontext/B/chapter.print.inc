@------------------------------------------------------------
@- chapter.print.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Pre- and post-processing"
	longTitle="Pre- and post-processing options"
	dest="<#elementName>.propertyGroup.pre_and_postprocessing"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="print" long="printThisFile" value="Y | N" >

<pdp>
Include this chapter file in the
list of files to be printed.
</pdp>

<propertyDefault>Y</propertyDefault>
</property>
@------------------------------------------------------------
