@------------------------------------------------------------
@-- taggedPDF.inc
@------------------------------------------------------------
<!-- Start START <#propertyName>.optgroup.taggedPDF -->

@------------------------------------------------------------
<@xmacro> - ifVersion {
	@--- How to do nesting ???
	<#ifndef> ifV.version		2
	<#def> ifV.comment		{}	@--- Optional parameter
	<#def> ifV.writeComment		0	@--- Optional parameter
	@----------------------------------------------------
	<#def> ifV.nestLevel		0
	<#ifndef> elementName		{}
	<#def> ifV.commentPrefix	{<<<Excluding:	}	
	@----------------------------------------------------
	@xgetvals(ifV.)
	@----------------------------------------------------
	<#def> ifV.nestLevel		@eval(<#ifV.nestLevel> + 1)
	@----------------------------------------------------
	@if(<#ifV.version> >= 2) {
		<#def> ifV.commentPrefix	{>>>Including:	}	
		}
	@if(@len(<#ifV.comment>) >= 2) {
		<@write> error {<#ifV.commentPrefix><#ifV.comment>	(<<#elementName>>)}
		}
}{
	<#def> ifV.contents	{}
	@if(<#ifV.version> >= 2) {
		@- <#def> ifV.contents[<#ifV.nestLevel>]		{$ifVersion}
		<#def> ifV.contents		{$ifVersion}
		}
	<#ifV.contents>
	@- $ifV.contents[<#ifV.nestLevel>]
	<#def> ifV.nestLevel		@eval(<#ifV.nestLevel> - 1)
}
@------------------------------------------------------------
<ifVersion version="2" comment="Tagged PDF properties" >
@------------------------------------------------------------
<propertyGroup
	title="Tagged PDF properties"
	dest="<#propertyName>.optgroup.taggedPDF"
>The following properties are ignored unless
the <pn>pdfType</pn> <#xMiramoXML> property is set
to <pv>UA</pv> or the <pn>-pdfType</pn> command line
option is set to <pv>UA</pv>. See page <xnpage
	id="commandline.option.-pdfType" /> and page <xnpage
id="MiramoXML.property.pdfType" />.
<vidx id="tagged pdf||<#elementName>" />
@- See the <#xListLabel> element on page
@- <xnpage id="ListLabel.element.start" />
</propertyGroup>
@------------------------------------------------------------

<@skipStart>
 Issues:  A table is often used just to place an image, ie.
 	not really a table.
	tpPresentation=[N | Y]
		none

 tpRoleMap="Test Title"
 tpTitle="Test Title"
 tpActualText="Nothing"
 tpAltText="Some alt text"
 tpExpandedText="Expanded text"
 tpId="MyID"
 tpLang="EN"

 The following describe the differences between some of the above:

 https://answers.acrobatusers.com/What-difference-title-actual-text-alternative-text-figure-properties-image-q16218.aspx

 https://taggedpdf.com/508-pdf-help-center/actual-text-alt-text-expansion-text-contents-key/

 https://commonlook.com/resources/manuals/docs-commonlook-pdf-semantics/docs-commonlook-pdf-figures/
<@skipEnd>

@------------------------------------------------------------
<property
	name="tpRoleMap"
	value="key" >
<pdp>
Map format to appropriate tagged PDF structure element.
</pdp>

@------------------------------------------------------------
<pdp element="Image" >
May be any of the following: 
<pv>Figure</pv>.
</pdp>

@------------------------------------------------------------
<pdp element="P" >
May be any of the following: 
<pv>P</pv>,
<pv>H</pv>,
<pv>H<Font fontWeight="Regular" fontAngle="Italic" >n</Font></pv>,
<pv>Title</pv>,
<pv>TOC</pv>,
<pv>TOCI</pv> or
<pv>Caption</pv>,
@- where <fI>n</fI> is an integer > 0 without preceding 0 digits.
 where, in the case of <pv>H<Font fontWeight="Regular"
	 fontAngle="Italic" >n</Font></pv>, <fI>n</fI> must be an
integer > 0 without preceding 0 (zero) digits.
</pdp>

@------------------------------------------------------------
<pdp element="Font" >
May only be the following: 
<pv>Span</pv>
</pdp>

<propertyDefault element="Image" >Figure</propertyDefault>
<propertyDefault element="P" >P</propertyDefault>
<propertyDefault element="Font" >Span</propertyDefault>
<propertyDefault element="Tbl" >Table</propertyDefault>
<propertyDefault element="Row" >TR</propertyDefault>
<propertyDefault element="Cell" >TD</propertyDefault>
<propertyDefault element="ListLabel" >TD</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="tpTitle"
	value="text"
	>
<pdp>
Figure caption or title
</pdp>
<@skipStart>
<@skipEnd>
</property>

@------------------------------------------------------------
<property
	name="tpActualText"
	value="text" >
<pdp>
The actual text
when an image represents one or more text characters
or standard typographical marks.
</pdp>

@- <propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="tpAltText"
	elementExclude="P"
	value="text" >
<pdp>
Text describing an image or icon.
A black and white photo of Albert Einstein fishing on
a bank of the river Orinoco, with snow-clad mountain
peaks and thunder clouds in the background.
</pdp>

<pdp>
The <pn>tpAltText</pn> property must always be present
when a Figure structure element is present.
</pdp>


@- <propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="tpExpandedText"
	value="text" >
<pdp>
Text that expands an abbreviation, acronym or jargon.
@- E.g. snafu.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="tpLang"
	value="key" >
<pdp>
Language
</pdp>

@- <propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="tpId"
	value="string" >
<pdp>
Id.
</pdp>

@- <propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------
<!-- End END  <#propertyName>.optgroup.taggedPDF -->
@------------------------------------------------------------
</ifVersion>
