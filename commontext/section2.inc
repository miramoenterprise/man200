@------------------------------------------------------------
@-  section2.inc
@- 
@-  	oddPages
@-  	evenPages
@-  	allPages
@-  	lastPage
@-  	emptyPage
@-	emptyEvenPage
@-	emptyOddPage
@-  
@-  Included in:
@-  
@-  	DocDef
@-  	Section
@-  	SectionDef
@-  
@------------------------------------------------------------
<@xmacro> section2 {
	<#def> mpr2.defaultOdd		{}
	<#def> mpr2.defaultEven		{}
	<#def> mpr2.new			{new }
	@if(<#elementName> = {DocDef}) {
		<#def> mpr2.new			{}
		<#def> mpr2.defaultOdd		{}
		<#def> mpr2.defaultEven		{}
		<#def> mpr2.defaultAllPages	{}
		<#def> mpr2.defaultBlank	{}
		}
}{}
@------------------------------------------------------------
<section2/>
@------------------------------------------------------------
<@xmacro> sectionDefault {
	<#def> mpd.value		{}
	@----------------------------------------------------
	@xgetvals(mpd.)
	@----------------------------------------------------
	<#property.long>
	@ifnot(<#elementName> = {DocDef}) {
		<propertyDefault>Inherited from <#xDocDef> (see <pv><#property.long></pv> on page <xnpage
			id="DocDef.property.<#property.long>" />)</propertyDefault>
		}
	@else {
		<propertyDefault><#mpd.value></propertyDefault>
		}
}{}
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Default master page usage properties"
	longTitle="Default master page usage properties"
	dest="<#elementName>.propertyGroup.default_page_formats"
	indexOnly="0"
>

Specify <#mpr2.new>default rules for applying master pages to
the destination page and to subsequent matching pages.
See Note
<xparanumonly
        id="note.SectionDef.destination_page" /> on page
<xnpage
        id="note.SectionDef.destination_page" /> for the
definition of <sq>destination page</sq>.

<br/>
The rules for resolving conflicts when more than one property
from the following set
of <Font uline="S"><xparatextonly
id="<#elementName>.propertyGroup.default_page_formats" /></Font> is
used are described in Note
<xparanumonly
        id="note.SectionDef.conflicts" /> on page
<xnpage
        id="note.SectionDef.conflicts" />.


</propertyGroup>



@------------------------------------------------------------
<property short="oddPages" long="oddPages"
	value="name" >

<pdp>
Apply master page <fI>name</fI> to the first destination page, if
it is an odd page,
and to all subsequent pages in the document that are odd
pages, counting the first page as 1.
See Note
<xparanumonly 
	id="note.SectionDef.even_and_odd" /> on page 
<xnpage 
	id="note.SectionDef.even_and_odd" />.
</pdp>

<sectionDefault value="Right" />

</property>

@------------------------------------------------------------
<property short="evenPages" long="evenPages"
	value="name" >
<pdp>
If the document is double-sided,
apply master page <fI>name</fI> to the first even destination page,
and to all subsequent pages in the document that are even
pages, counting the first page as 1.
See Note
<xparanumonly 
	id="note.SectionDef.even_and_odd" /> on page 
<xnpage 
	id="note.SectionDef.even_and_odd" />.
</pdp>

<sectionDefault value="Left" />

</property>

@------------------------------------------------------------
<property short="allPages" long="allPages"
	@- value="name | Default" >
	value="name" >
<pdp>
Apply master page <fI>name</fI> to the first destination page
and all subsequent pages in the document.
</pdp>

<sectionDefault value="Right" />

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Last-page master page usage properties"
	longTitle="Last-page master page usage properties"
	dest="<#elementName>.propertyGroup.last_page_format"
>
Specify a rule for applying an exception master page to
the last page in a document, or to the last page in a
chapter in a book, that contains
non-background text or graphics.

<br/><br/>
All the properties in the following set of <Font uline="S"><xparatextonly
id="<#elementName>.propertyGroup.last_page_format" /></Font> are ignored
if the document contains only one page.

</propertyGroup>


@------------------------------------------------------------
<property short="lastPage" long="lastPage"
	value="name" >
<pdp>
Apply background from master page <fI>name</fI> to
the last non-empty page in the document.
If the document section is a single page and
the <pn>firstPage</pn> property is set, the value of
the <pn>firstPage</pn> property overrides the value of <pn>lastPage</pn>.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Empty-page master page usage properties"
	longTitle="Empty-page master page usage properties"
	dest="<#elementName>.propertyGroup.empty_page_format"
>
Specify which master pages to apply to empty pages.
An empty page is a page that has no content in its main
text flow.
An empty page may occur when an element is set to start at
the top of a page, or at the end of the document section
when the <pn>pageCount</pn> property is set
to <ov>odd</ov> or <ov>even</ov>.
</propertyGroup>


@------------------------------------------------------------
<property short="emptyPage" long="emptyPage"
	value="name" >
<pdp>
Apply background from master page <fI>name</fI> to
all empty pages.
</pdp>
<sectionDefault value="Blank" />
</property>

@------------------------------------------------------------
<property short="emptyEvenPage" long="emptyEvenPage"
	value="name" >
<pdp>
Apply background from master page <fI>name</fI> to
empty even pages only.
</pdp>
<sectionDefault value="Blank" />
</property>

@------------------------------------------------------------
<property short="emptyOddPage" long="emptyOddPage"
	value="name" >
<pdp>
Apply background from master page <fI>name</fI> to
empty odd pages only.
</pdp>
<sectionDefault value="Blank" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
@- <@include> ${CTEXT}/mpr.pageNumbering.inc
@------------------------------------------------------------
