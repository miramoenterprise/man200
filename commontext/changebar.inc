@------------------------------------------------------------
@- changebar.inc
@------------------------------------------------------------
<propertyGroup
	shortTitle="Size and position properties"
	longTitle="Size and position properties"
	dest="<#elementName>.sizeAndPosition"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="position"
	value="start | end"
	>

<pdp>
Position of the change bar.
</pdp>

<propertyDefault>start</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="style"
	value="solid | groove | double | ridge "
	>

<pdp>
Change bar style.
</pdp>

<propertyDefault>solid</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="width"
	value="dim"
	>

<pdp>
Overall width of the change bar.<br/>
In the case <pn>style</pn> is set to <pv>double</pn> the change
bar comprises two vertical rules each of which has a width equal to one
third the value of <pn>width</pn>. The gap between the rules is one third
the value of <pn>width</pn>.
</pdp>

<propertyDefault>3pt</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="colorDef"
	value="dim"
	>

<pdp>
Color of the change bar.
</pdp>

<propertyDefault>Red</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="opacity"
	value="percent"
	>

<pdp>
Opacity of the change bar.
</pdp>

<propertyDefault>Red</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="offset"
	value="dim"
	>

<pdp>
Offset to the center of the change bar width from the start or end edge
of the container text column.
The text column edge anchor point depends on the setting of the
<pn>position</pn> property.
Positive values move the changebar outside away from the container edge.
Negative values move the changebar inside away from the container edge.
</pdp>

<propertyDefault>6pt</propertyDefault>
</property>
@------------------------------------------------------------
