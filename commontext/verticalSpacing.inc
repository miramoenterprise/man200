@------------------------------------------------------------
<!-- file = verticalSpacing.inc (start)  -->
@------------------------------------------------------------


@------------------------------------------------------------


Actual inter-object vertical spacing is determined by the larger
of the space above the current object and the space below
the preceding object, not the sum of the two.
An exception to this rule occurs when the current
object follows immediately after a paragraph with
the <pn>maximumSpaceBelow</pn> set to <pv>Y</pv>.

See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
<vidx id="paragraph||space above (spaceAbove)" />
<vidx id="space||above paragraph (spaceAbove)" />

