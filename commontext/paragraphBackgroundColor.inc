@------------------------------------------------------------
<!-- File =  paragraphBackgroundColor.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
@- In 'Text features' property group
@------------------------------------------------------------

@-===========================================================
<propertyGroup
	shortTitle="Text background color"
	longTitle="Text background color"
	dest="<#elementName>.propertyGroup.backgroundFill"
	indexOnly="0"
>
For an alternative method of specifying paragraph background fills
(and border rulings), see the <#xparaFrame> sub-element
on page <xnpage
	@- id="ParaDef.subelement.paraFrame.start" />.
	@- id="ParaDef.subelement.paraFrame" />.
	id="<#elementName>.subelement.paraFrame" />.
</propertyGroup>


@------------------------------------------------------------
<property
	short="fillColor"
	long="fillColor"
	value="name"
	>


<pdp>
Apply a background color to the paragraph text.
</pdp>

<pdp element="P" >
The background color is applied to the rectangle that
has vertical sides at the values
of <on>startIndent</on> and <on>endIndent</on> and horizontal
sides at the top of the first line of text and at the bottom
of the last line of text.
</pdp>


@------------------------------
<referToPElement/>
@------------------------------

<pdp element="P" >
E.g. the following
</pdp>

<exampleBlock
	element="P"
	>
@- <P paragraphFormat="Body" firstLineIndent="20mm" startIndent="10mm" endIndent="15mm"
<P firstLineIndent="20mm" startIndent="10mm" endIndent="15mm"
fillColor="Black" fillTint="12%" textSize="7pt" fillOpacity="100" >
Arma virumque cano, Troiae qui primus ab oris. Italiam, fato profugus
</P>
</exampleBlock>


<pdp element="P" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>no background text color</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="fillTint" long="fillTint" value="percent"
	>


<pdp>
Tint for text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

@------------------------------
<referToPElement/>
@------------------------------

<pdp element="P" >
E.g. the following
</pdp>

<exampleBlock
	element="P"
	>
Text with a <Font fillColor="Blue" fillTint="30%">blue-tint</Font> background.
</exampleBlock>


<pdp element="P" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>100%</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fillOpacity"
	long="fillOpacity"
	value="100 | 0"
	>

@------------------------------
<referToPElement/>
@------------------------------

<pdp>
Opacity of text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

<pdp element="P" >
E.g. the following
</pdp>

<exampleBlock
	element="P"
	>
Text with a <Font fillColor="Blue" fillOpacity="30%">20% opacity</Font> background.
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>100%</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="paraFrame"
	value="Y | N"
	>

<pdp>
Set the <pn>paraFrame</pn> property to <pv>N</pv> to disable the
background fill and borders specified by a <#xparaFrame> sub-element,
if any.
</pdp>

@------------------------------
@- <referToPElement/>
@------------------------------

<propertyDefault>Y</propertyDefault>
</property>


@------------------------------------------------------------
<!-- File =  paragraphBackgroundColor.inc (end) -->
@------------------------------------------------------------
