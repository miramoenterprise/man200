@------------------------------------------------------------
<!-- file = feather.inc (start)  -->
@------------------------------------------------------------

<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Feathering"
	longTitle="Feathering"
	dest="<#elementName>.optgroup.feathering"
	></propertyGroup>
@------------------------------------------------------------
<@skipEnd>

@------------------------------------------------------------
<property
	short="feather"
	long="feather"
	value="N | Y"
	changeBar="Y"
	>

<pdp>
Enable inter-paragraph feathering in this section.
Feathering is only applied to paragraphs which have
the <pn>deltaSpaceAbove</pn> property set to a value greater
than <pv>0</pv>.
</pdp>

<pdp>
See the <#xP> and <#xParaDef> <pn>deltaSpaceAbove</pn> properties
on pages <xnpage
	id="P.property.deltaSpaceAbove" /> and <xnpage
	id="ParaDef.property.deltaSpaceAbove" />.
</pdp>
<@skipStart>
<@skipEnd>

<propertyDefault>N</propertyDefault>

</property>
@------------------------------------------------------------
