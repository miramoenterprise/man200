@------------------------------------------------------------
@- File: 'hotspots.inc'
@- 
@- For Miramo Enterprise 9.2
@- 
@------------------------------------------------------------

<#ifndef> tbl_cm1	{s}
<#ifndef> tbl_cm2	{, for all cells in table}
<#ifndef> tbl_cm3	{, or specified using the 
corresponding <#xTbl> element property}

@- <@write> error {<#elementName>	cellMargins.inc}
@------------------------------------------------------------
<@macro> hotspotDefs {
	@if(<#elementName> = AFrame ) {
		<propertyGroup
			shortTitle="AFrame hotspots"
			longTitle="AFrame hotspots"
			dest="<#elementName>.propertyGroup.hotspots"
			>
			</propertyGroup>
		}
	@if(<#elementName> = Image ) {
		<propertyGroup
			shortTitle="Image hotspots"
			longTitle="Image hotspots"
			dest="<#elementName>.propertyGroup.hotspots"
			>
			</propertyGroup>
		}
}
@------------------------------------------------------------
<|hotspotDefs>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="hotspotURL"
	value="URL"
	>
<pdp>
URL target opened when the hotspot area is clicked.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="hotspotToolTip"
	value="text"
	>
<pdp>
Tooltip text displayed when hovering over the hotspot area.
The <pn>hotspotToolTip</pn> property applies only to output in HTML5.
</pdp>

</property>

@------------------------------------------------------------
