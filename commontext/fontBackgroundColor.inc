@------------------------------------------------------------
<!-- File =  fontBackgroundColor.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
@- In 'Text features' property group
@------------------------------------------------------------

@-===========================================================
<propertyGroup
        shortTitle="Text background color"
	longTitle="Text background color"
	dest="<#elementName>.propertyGroup.backgroundFill"
	indexOnly="0"
	>
For an alternative method of specifying text background fills
(and border rulings), see the <#xtextBox> sub-element
on page <xnpage
	@- id="ParaDef.subelement.paraFrame.start" />.
	@- id="ParaDef.subelement.paraFrame" />.
	id="<#elementName>.subelement.textBox" />.
</propertyGroup>


@------------------------------------------------------------
<property
	name="fillColor"
	value="name"
	>

<pdp>
Apply a background color to text.
<outputRule condition="fmcGuide" >FrameMaker 10 and above only.</outputRule>
</pdp>



@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
@- Text with a <Font fillColor="Salmon">salmon-colored</Font> background.
@- Text with a <Font fillColor="rgb(0 200 0)">salmon-colored</Font> background.
@- Text with a <Font fillColor="rgb(#x00ff00)">salmon-colored</Font> background.
@-Text with a <Font fillColor="rgb(#00ff00)" >salmon-colored</Font> background. @- Works!
Text with a <Font fillColor="rgb(226,212,185)" >cool</Font> background. @- Works!
@- Text with a <Font fillColor="rgb(426,-212,-185)" >cool</Font> background. @- Works!
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>no background text color</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fillTint"
	long="fillTint"
	value="percent"
	condition="mmcGuide"
	>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp>
Tint for text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
Text with a <Font fillColor="Blue" fillTint="30%">blue-tint</Font> background.
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>


<propertyDefault>100%</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fillOpacity"
	long="fillOpacity"
	value="100 | 0"
	>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp>
Opacity of text background color. Applicable only if <on>fillColor</on> is
set.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
Text with a <Font fillColor="Blue" fillOpacity="10%" >10% opacity</Font> background.
@- <Font fillColor="Blue" fillOpacity="100%">background.</Font> xx
</exampleBlock>


<pdp element="Font" 
       	>
produces:
<#example_output>
</pdp>
<@skipStart>
<@skipEnd>


<propertyDefault>100%</propertyDefault>
@- The following is a hack to get around fillOpacity BUG ...
@- <propertyDefault><Font fillColor="White" fillOpacity="100%">100%</Font></propertyDefault>
</property>
@------------------------------------------------------------

<@skipStart>
<@skipEnd>
@------------------------------------------------------------
<property
	name="textBox"
	value="Y | N"
	>

<pdp>
Set the <pn>textBox</pn> property to <pv>N</pv> to disable the
background fill and borders specified by a <#xtextBox> sub-element,
if any.
</pdp>

@------------------------------
@- <referToPElement/>
@------------------------------

<propertyDefault>Y</propertyDefault>
</property>



@------------------------------------------------------------
<!-- File =  fontBackgroundColor.inc (end) -->
@------------------------------------------------------------
