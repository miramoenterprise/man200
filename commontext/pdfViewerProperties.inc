@------------------------------------------------------------
<!-- file = pdfViewerProperties.inc (start) -->
@------------------------------------------------------------
 

@------------------------------------------------------------
<propertyGroup
	title="PDF viewer properties"
	dest="<#elementName>.optgroup.acrobat_properties"
	>
See also: <#xListLabel> element <on>showInPDFbookmark</on> on
page <xnpage
	id="ListLabel.property.showInPDFbookmark" />.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="PDFbookmarkLevel"
        value="int" >
<pdp>
PDF bookmark level applied to the paragraph format
during conversion to PDF. If the bookmark level is zero,
the paragraph is not included as an PDF bookmark
in the resulting PDF file. 
<vidx id="bookmarks, PDF" />
<vidx id="PDF||bookmarks" />
</pdp>

</property>

@------------------------------------------------------------
<property
	name="PDFbookmarkShift"
        value="dim" >
<pdp>
Vertically shift the location of the book mark target. Negative values
move the target upwards. Positive values move the target downwards (i.e.
in the initial page display the target will be located above the top of
the page display.)
</pdp>

<pdp>
By default the bookmark target is located slightly
above the referenced paragraph text.
@- The distance
@- above corresponds to the value of the paragraph <on>spaceAbove</on> property.
<vidx id="bookmarks, PDF" />
<vidx id="PDF||bookmarks" />
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="linkTargetShift"
        value="dim | percent"
	>
<pdp>
Vertically shift the location of the link target. Negative values
move the target upwards. <fI>percent</fI> values refer to the
paragraph text size.
</pdp>

</property>

@------------------------------------------------------------
<!-- file = pdfViewerProperties.inc (end) -->
@------------------------------------------------------------
