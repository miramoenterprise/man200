@------------------------------------------------------------
@- objectSpaceAboveBelow.inc
@------------------------------------------------------------
@- Used in <AFrame>, <MathML> and <MathMLDef> elements
@------------------------------------------------------------

@------------------------------------------------------------
<!-- START file: spaceAboveBelow.inc (<#elementName>) -->
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> spaceAboveBelowTextDefs {
	@----------------------------------------------------
	@- Default is for anchored frame
	@----------------------------------------------------
	<#def> sabtb.element		{<#xAFrame>}
	<#def> sabtb.text		{anchored frame}
	<#def> sabtb.belowText		{}
	@if(@match({Y<#elementName>Y}, {YMathMLY})) {
		<#def> sabtb.element	{<#xMathML>}
		<#def> sabtb.text	{equation}
		<#def> sabtb.belowText	{Ignored unless the <#xMathML> element
 is included at the top level.}
		}
	@if(@match({Y<#elementName>Y}, {YMathMLDefY})) {
		<#def> sabtb.element	{<#xMathML>}
		<#def> sabtb.text	{equation}
		<#def> sabtb.belowText	{Ignored unless the <#xMathML> element
 is included at the top level.}
		}
}{}
@------------------------------------------------------------
<spaceAboveBelowTextDefs/>
@------------------------------------------------------------

<propertyGroup
	title="Vertical spacing properties"
	dest="optgroup.<#elementName>.vertical_spacing.start"
	>
The following properties apply only if the <#sabtb.element> element
is included at the top level, i.e. outside a <#xP> element <fI>or</fI> the
<pn>position</pn> element is set to <pv>below</pv>.
</propertyGroup>

@------------------------------------------------------------
<property
	short="spaceAbove"
	long="spaceAbove"
	value="dim"
	>

<pdp>
Space above <#sabtb.text>.
<vidx id="<#sabtb.text>||space above (spaceAbove)" />
<vidx id="space||<#sabtb.text> paragraph (spaceAbove)" />
</pdp>

<pdp>
In the case the <#sabtb.element> is at the top level, the
actual spacing
is determined by the larger of the
space above the <#sabtb.text> and the space below
the preceding paragraph, not the sum of the two (see Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />).
If the <#sabtb.element> is within a <#xP> element <pn>spaceAbove</pn> is
the sole determinant of the vertical space
between the <#sabtb.text> and the preceding text.
</pdp>

</property>


@------------------------------------------------------------
<property
	short="spaceBelow"
	long="spaceBelow"
	value="dim"
	>

<pdp>
Space below <#sabtb.text>.
<#sabtb.belowText>
<vidx id="<#sabtb.text>||space below (spaceBelow)" />
<vidx id="space||below <#sabtb.text> (spaceBelow)" />
</pdp>

</property>

<pdp>
Actual spacing
is determined by the larger of the
space below the <#sabtb.text> (or the <#sabtb.text> title, if present)
and the space above the following paragraph, not the sum of the two.
See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
</pdp>

@------------------------------------------------------------
<!-- END file: spaceAboveBelow.inc (<#elementName>) -->
@------------------------------------------------------------
