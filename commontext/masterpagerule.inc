@------------------------------------------------------------
@-- masterpagerule.inc
@------------------------------------------------------------
@-
@- Used in: 
@-
@-	<MasterPageRule>
@-	<MasterPageRuleDef>
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> masterPagerRuleDef1 {
<esp>
Unless otherwise specified by settings in the <#xDocDef> element
(see pages <xnpage
	id="MasterPageRuleDef.element.start" /><xphyphen/><xnpage
	id="MasterPageRuleDef.element.end" />) all
pages in single-sided documents have the layout of
the <sq>Right</sq> master page.
Similarly double-sided documents use
alternating <sq>Right</sq> and <sq>Left</sq> master pages,
starting with the <sq>Right</sq> page layout for the first page
of the document.
</esp>
}{}
@------------------------------------------------------------
