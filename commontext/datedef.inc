@--------------------------------------------------------------------------------
@- datedef.inc
@--------------------------------------------------------------------------------
<@xmacro> - bbpz {
	@xgetvals()
}{
	<#def> bbpz		$bbpz
	<bbp @xputvals() >
		<#bbpz>
	</bbp>
}
@--------------------------------------------------------------------------------
<@xmacro> - dateDefSubElement {
	<#ifndef> subElementCount	0
	<#def> subElementCount		@eval(<#subElementCount> + 1)
	@xgetvals()
}{
	<#def> dDSE		$dateDefSubElement
	@- <@write> error {
	@- 	---------------------------------
	@- 	@- $dateDefSubElement
	@- 	<#dDSE>
	@- 	---------------------------------
	@- 	}
	@if(<#elementName> = DateDef) {
		<#def> subElementName[<#subElementCount>]	{<#name>}
		<subElement @xputvals() >
<#dDSE>
		</subElement>
		}
	@else {
		<#def> subElementName[<#subElementCount>]	{<#name>}
		}
}
@--------------------------------------------------------------------------------
<@xmacro> dateDefShowNames {
	<#def> subElementNameList	{}
	@for(count = 1 to <#subElementCount>) {
		<#def> seSecondLast	@eval(<#subElementCount> - 1)
		<#def> seName		$subElementName[$$count]
		<#def> seName		{<<#seName>/>}
		@if(<#subElementCount> = 2) {
			<#def> seName			{<#seName> }
			}
		@if($$count < <#seSecondLast> AND <#subElementCount> > 2 ) {
			<#def> seName			{<#seName>, }
			}
		@if($$count = <#seSecondLast>) {
			<#def> seName			{<#seName> }
			}
		@if($$count = <#subElementCount> )  {
			<#def> seName			{and <#seName>}
			}
		<#def> subElementNameList	{<#subElementNameList><#seName>}
			@- <@write> error {
			@- 	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
			@- 	$subElementName[$$count]
			@- 	}
		}
	<#def> subElementNameList	{<#subElementNameList>.}
	@- <@write> error {<#subElementNameList>}
	<@write> -n  ${mmtmp}/dateDefSubElementList {<#subElementNameList>}
}{}

@------------------------------------------------------------
@- <subElement
<dateDefSubElement
	name="second"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Second of the minute.
</bbpz>
</dateDefSubElement>
@- </subElement>

@------------------------------------------------------------
<dateDefSubElement
	name="Font"
	withPrevious="N"
	withNext="Y"
	@- content="Y"
	contentAndProperties="Y"
	>
<bbpz wp="Y" wn="N" >
One or more <#xFont> elements (see pages <xnpage
	id="Font.element.start" /><xphyphen/><xnpage
	id="Font.element.end" />) may be included anywhere within
a <#xDateDef> element.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="second00"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Second of the minute, zero padded if the second is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="minute"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Minute of the hour.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="minute00"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Minute of the hour, zero padded if the minute is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="hour"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Hour of the day, using a 12 hour clock.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="hour01"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Hour of the day, using a 12 hour clock.
Zero padded if the hour is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="hour24"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Hour of the day, using a 24-hour clock.
Zero padded if the hour is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="ampm"
	withPrevious="N"
	withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Expand to am if the time is before midday, otherwise
expand to pm.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="AMPM"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Expand to AM if the time is before midday, otherwise
expand to PM.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="dayNumber"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Day of the month number.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="dayNumber01"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Day of the month number, zero padded if the day number
is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="dayName"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Day name.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="shortDayName"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Short day name.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="monthNumber"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Month number.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="monthNumber01"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Month number. Zero padded if month number is below 10.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="monthName"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Month name.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="shortMonthName"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wp="Y" wn="N" >
Short month name.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="year"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wn="Y" >
Year, expressed as four digits, e.g. 2028.
</bbpz>
</dateDefSubElement>

@------------------------------------------------------------
<dateDefSubElement
	name="shortYear"
	withPrevious="N"
	@- withNext="Y"
	>
<bbpz wn="Y" >
Short year, expressed as two digits, e.g. 28.
</bbpz>
</dateDefSubElement>
@------------------------------------------------------------

@------------------------------------------------------------
<dateDefShowNames/>
@------------------------------------------------------------
