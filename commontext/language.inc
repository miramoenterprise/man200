@------------------------------------------------------------
<!-- File = language.inc (start) -->
@------------------------------------------------------------
@- <@include> ${CTEXT}/def.template.inc
@------------------------------------------------------------
<@xmacro> languageContext {
	<#def> languageContext	{context}
	@if({<#elementName>} = {Font} ) {
		<#def> languageContext	{font}
		}
	@if({<#elementName>} = {P} ) {
		<#def> languageContext	{paragraph}
		}
	@if({<#elementName>} = {ParaDef} ) {
		<#def> languageContext	{paragraph}
		}
	@if({<#elementName>} = {MiramoXML} ) {
		<#def> languageContext	{document}
		}
	@- @print({<#languageContext>}, n)
}{}
@------------------------------------------------------------
<@xmacro> languageXRef {
	@if({<#documentName>} = {mmComposerReferenceGuide.pdf} ) {
The <fI>key</fI> values for the <on>language</on> property are
listed in <xparalabel
	id="tbl.list_of_language_values.start" />
on pages <xnpage
	id="tbl.list_of_language_values.start" /><xphyphen><xnpage
	id="tbl.list_of_language_values.end" />.
		}
	@else {
The <fI>key</fI> values for the <on>language</on> property are
listed in the <HyperCmd file="mmComposerReferenceGuide.pdf"
	jumptodest="tbl.list_of_language_values.start"
	fontAngle="Italic"
	>mmComposer Reference Guide</HyperCmd>.
		}
}{}
@------------------------------------------------------------
<languageContext/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="language"
	value="key"
        elementExclude="MathML|MathMLDef|Equation|EquationDef"
	>

<pdp>
Set the <#languageContext> language to <fI>key</fI>.
<languageXRef/>
<vidx id="text||language" />
<vidx id="document||language" />
<vidx id="language||text" />
<vidx id="language||document" />
<vidx id="<#languageContext> language" />
</pdp>

<pdp>
The role of the <on>language</on> property is described
in <xchapter id="language_property" />.
</pdp>

@- <propertyDefault>File</propertyDefault>
</property>
@------------------------------------------------------------
<!-- File = language.inc (end) -->
@------------------------------------------------------------
