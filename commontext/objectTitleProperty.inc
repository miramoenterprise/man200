@------------------------------------------------------------
@-  objectTitleProperty.inc
@------------------------------------------------------------
@- Used in <AFrame>, <ATextFrame>, <MathML> and <MathMLDef> elements
@------------------------------------------------------------

@------------------------------------------------------------
<!-- START file:  objectTitleProperty.inc (<#elementName>) -->
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> objectTitleTextDefs {
	@----------------------------------------------------
	@- Default is for anchored frame
	@----------------------------------------------------
	<#def> sabtb.element		{<#xAFrame>}
	<#def> sabtb.text		{anchored frame}
	<#def> sabtb.belowText		{}
	@if(@match({Y<#elementName>Y}, {YMathMLY})) {
		<#def> sabtb.element	{<#xMathML>}
		<#def> sabtb.text	{equation}
		<#def> sabtb.belowText	{Ignored unless the <#xMathML> element
 is included at the top level.}
		}
	@if(@match({Y<#elementName>Y}, {YMathMLDefY})) {
		<#def> sabtb.element	{<#xMathML>}
		<#def> sabtb.text	{equation}
		<#def> sabtb.belowText	{Ignored unless the <#xMathML> element
 is included at the top level.}
		}
}{}
@------------------------------------------------------------
<objectTitleTextDefs/>
@------------------------------------------------------------

<propertyGroup
	shortTitle="Include title"
	longTitle="Include title"
	dest="optgroup.<#elementName>.includeTitle.start"
	>
</propertyGroup>

@------------------------------------------------------------
<property
	name="objectTitle"
	@- long="objectTitle"
	@- short="objectTitle"
	value="N | Y"
	>

<pdp>
Include title/caption for <#sabtb.text>.
See the <#xObjectTitle> element on pages <xnpage
	id="ObjectTitle.element.start" /><xphyphen/><xnpage
	id="ObjectTitle.element.end" />.
<vidx id="captions" />
</pdp>

</property>

@------------------------------------------------------------
<!-- END file:  objectTitleProperty.inc (<#elementName>) -->
@------------------------------------------------------------
