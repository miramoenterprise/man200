@------------------------------------------------------------
<!-- File =  fontTextFeatures.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	@- shortTitle="Text capitalization"
	@- longTitle="Text capitalization"
	title="Text capitalization"
	dest="<#elementName>.propertyGroup.textCapitalization.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="capitalization"
	value="normal | smallCaps | uppercase | lowercase"
	changeBar="Y"
	>

<pdp>
Apply special text casing.
<vidx id="small caps" />
<vidx id="capitalization" />
</pdp>

@------------------------------------------
<propertyValueKeyList
	keyWidth="20mm"
	element="Font"
	>
@------------------------------------------
<vk key="normal" >Process text unchanged</vk>
@------------------------------------------
<vk key="smallCaps" >
Translate lowercase text into smallcaps. The relative size of the smallcaps
text depends on the setting of the <pn>smallCapsStyle</pn> and
the <pn>smallCapsSize</pn> properties.
@- If the applicable font does
@- not include the small caps feature (smcp), then a warning message is output
@- and artificial small caps are used instead. As if <pv>simulatedSmallCaps</pv> is
@- specified.
</vk>

@------------------------------------------
<vk key="uppercase" >
Convert lowercase text to uppercase.
</vk>
@------------------------------------------
<vk key="lowercase" >
Convert uppercase text to lowercase.
</vk>
@------------------------------------------
</propertyValueKeyList>
@------------------------------------------

@------------------------------------------
<referToFontElement/>
@------------------------------------------
<propertyDefault>normal</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="smallCapsStyle"
	value="simulated | fromFont"
	changeBar="Y"
	>

<pdp>
If <pn>smallCapsStyle</pn> is set to <pv>simulated</pv> the size
of the smallcaps text is set to the percentage value specified by
the <pn>smallCapsSize</pn> property regardless of whether the
font supports the smcp feature.
<vidx id="small caps" />
<vidx id="capitalization" >
</pdp>

<pdp>
If <pn>smallCapsStyle</pn> is set to <pv>fromFont</pv> <fI>and</fI> the
font supports the smcp feature, the internal font smallcaps size is
used and the value of the <pn>smallCapsSize</pn> property is ignored.
</pdp>

<propertyDefault>fromFont</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="smallCapsSize"
	value="percent"
	changeBar="Y"
	>

<pdp>
Relative size of small caps when <pn>capitalization</pv> is set
to <pv>smallCaps</pv>. Ignored if the font contains small caps (smcp feature) and
the <pn>smallCapsStyle</pn> property is set to <pv>fromFont</pv>.
<vidx id="small caps" />
<vidx id="capitalization" />
</pdp>

<propertyDefault>80%</propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Font features"
	longTitle="Font features"
	dest="<#elementName>.propertyGroup.text_features.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fontFeatures"
	long="fontFeatures"
	value="N | Y"
	>

<pdp>
Enable text and font features.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="fontFeaturesList"
	value="keylist"
	>

<pdp>
Enable selected OpenType advanced features.
</pdp>

<pdp element="Font" >
<fI>keylist</fI> may comprise one of more <fI>key</fI> values
separated by vertical bars. Each key value must be four alphanumeric
characters long and correspond to the values listed in the OpenType
specification. Invalid features and requests for features not present
in the font are ignored.
@- See Table A<paranumonly
See <xparalabel
	id="tbl.appendix.font_features" /> on pages <xnpage
	id="tbl.appendix.font_features.start" /><xphyphen><xnpage
	id="tbl.appendix.font_features.end" /> for a list of
all possible font feature key values.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g. this input
</pdp>

<exampleBlock
	element="Font"
	>
<Font textSize="3mm" fontFamily="Gabriola" 
	>Normal Lorem amat ipsum|<Font textSize="4mm" fontFeatures="Y"
fontFeaturesList="ss06" > Convoluted Lorem amat ipsum</Font></Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

<pdp element="Font" >
The <pn>fontFeaturesList</pn> property is ignored unless
the <pn>fontFeatures</pn> property is set to <pv>Y</pv>.
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	title="Underline, overline and strikethrough"
	dest="<#elementName>.propertyGroup.underline_and_overline.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="textUnderline"
	value="none | single | numeric | double" 
	>

<pdp>
Underline style.
<vidx id="underline||single" />
<vidx id="underline||low" />
<vidx id="underline||numeric" />
<vidx id="underline||double" />
</pdp>

<propertyValueKeyList
	keyHeading="Value"
	element="P|TextLine|ALine" >
	>
<vk key="none | N" >No underline.</vk>
<vk key="single | Y" >Normal underline.</vk>
<vk key="numeric" >Numeric (low) underline.</vk>
<vk key="double" >Double underline.</vk>
</propertyValueKeyList>

<propertyValueKeyList
	element="Font"
	>
<vk key="none" >No underline.</vk>
<vk key="single" >Normal (single) underline. By default
the <ov>single</ov> underline
baseline offset and underline thickness are as specified by the
corresponding values in the <fB>post</fB> table of the current font.
If these values are not present in the current font (.ttf or .otf),
the baseline offset and underline thickness are set to 10% and 6%
of the current font size.
</vk>
<vk key="numeric" >Numeric underline. By default
the thickness of a <ov>numeric</ov> underline is 0.5pt and the
baseline offset to the mid-point of the underline is 2pt, irrespective
of the current font size.
A <ov>numeric</ov> underline is low at small font sizes and
high at large font sizes.
</vk>
<vk key="double" >Double underline. By default
the thickness of each line in a <ov>double</ov> underline is 0.5pt and
the baseline offset to the mid-point of the first underline is 2pt,
irrespective of the current font size. The pitch between the two
underlines is 2pt.
</vk>
</propertyValueKeyList>

<pdp>
By default the underline color is as specified by
the <on>textColor</on> property, i.e. the color of the text above
the underline. 
Underline color may be changed by
setting the <on>underlineColor</on> property.
Underline thicknesses and baseline
offsets may be changed by setting
the <on>underlineThickness</on> and <on>underlineOffset</on> properties.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- @- See the description of <on><#propertyName></on> in the <#xFont> element.
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="single" >normal underline</Font> 
<Font underline="double" >double underline</Font><br/>
<Font underline="numeric" >numeric underline</Font> 
<Font underline="single" textColor="Red" >normal underline</Font> 
<Font underline="none" >switch off underline</Font> 
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------



@------------------------------------------------------------
<property short="underlineOffset" long="underlineOffset" value="dim | percent"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline offset from text baseline to the center of the underline
(or the first underline in the case of a double underline)
as an absolute value or a percentage of the text size.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="single" underlineOffset="30%" >Low underline
 (30% of text size) <Font textSize="4pt">small text</Font></Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>


@------------------------------------------------------------
<property short="underlineThickness" long="underlineThickness" value="dim | percent"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline thickness as an absolute value or a percentage of
the text size.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="single" underlineThickness="14%"
	>Thick underline (14%) </Font> no underline<br/>
<Font underline="single" underlineThickness="1.2pt" underlineColor="Red"
>Thick underline (1.2pt)</Font> no underline
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property short="underlineGap" long="underlineGap" value="dim | percent"
	condition="mmcGuide"
	>

<pdp>
Gap between lines of a double underline.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------
<pdp element="P|Font" >
Underline gap between the centers of the underlines as an absolute value or
a percentage of the text size.
Applicable only when <on>underline</on> is set to <ov>double</ov>.
</pdp>

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="double" underlineThickness="16%"
	underlineColor="Red"
	underlineOffset="56%"
	underlineGap="42%"
	@- >Double underline</Font><br/>&nbsp;
	>Double underline with custom gap between lines</Font><br/>&#x00A0;
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>


@------------------------------------------------------------
<property short="underlineColor" long="underlineColor" value="name"
	condition="mmcGuide"
	>
<pdp element="P|Font" >
Underline color.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g. the following
</pdp>

<exampleBlock element="Font" 
	>
<Font underline="single" 
	>underline<Font textColor="Red">red</Font></Font><br/>
<Font underline="single" textColor="Red"
	>underline<Font textColor="Black">red</Font></Font><br/>
<Font underline="single" textColor="Red" underlineColor="Blue"
	>underline<Font textColor="Black">red</Font></Font><br/>
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="textOverline"
	long="textOverline"
	value="N | Y"
	>

<pdp>
Overline.
<vidx id="overline (text)" />
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<exampleBlock element="Font" >
<Font textOverline="Y" >overline</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>
@------------------------------------------------------------


@------------------------------------------------------------
@- <propertyGroup
@- 	shortTitle="Strike through and change bar"
@- 	longTitle="Strike through and change bar"
@- 	dest="<#elementName>.propertyGroup.strike_through"
@- />
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="strikeThrough"
	long="strikeThrough"
	value="N | Y"
	>

<pdp>
Strike through.
<vidx id="strike through (text)" />
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|ParaDef|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>


<pdp element="Font" >
E.g.
</pdp>

<exampleBlock element="Font" >
<Font strikeThrough="Y" >strike through</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property short="C" long="changeBar" value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Change bar.
<vidx id="change bar" />
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<exampleBlock element="Font" >
<Font C="Y" >change bar</Font>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>



@------------------------------------------------------------
<!-- File =  fontTextFeatures.inc (end) -->
@------------------------------------------------------------
