@------------------------------------------------------------
@-- p.basicNew.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Basic properties"
	longTitle="Basic properties"
	dest="cell.optgroup.basic"
	></propertyGroup>

@------------------------------------------------------------
<property
	short="textAlign"
	long="textAlign"
	value="start | end | center | justify"
	>

<pdp>
Text alignment and justification.
<vidx id="paragraph||justification in" />
<vidx id="justification||in paragraphs" />
<vidx id="text alignment||paragraph" />
<vidx id="alignment||paragraph" />
<vidx id="paragraph||centering text (A=C)" />
<vidx id="paragraph||alignment" />
<vidx id="centering text||in paragraphs (A=C)" />
</pdp>

<propertyValueKeyList keyWidth="11mm"
	keyHeading="Value"
	descriptionHeading="Description"
>

@- <vk key="start | left" >
<vk key="start" >
Align text at the beginning of lines.
Ragged line ends.
@- <ov>left</ov> may be used as an alias for <ov>start</ov>.
@- In the case of right-to-left
@- writing <ov>left</ov> and <ov>start</ov> produce
@- right-aligned text lines.
</vk>

@- <vk key="end | right" >
<vk key="end" >
Align text at line ends.
Ragged line starts.
@- <ov>right</ov> may be used as an alias for <ov>end</ov>.
@- In the case of right-to-left
@- writing <ov>right</ov> and <ov>end</ov> produce
@- left-aligned text lines.
</vk>

<vk key="center" >
Align text lines at the center.
Ragged line starts and ends.
</vk>

<vk key="justify" >
Align text lines at start and end.
Text justification at both ends.
@- <ov>both</ov> may be used as an alias for <ov>justify</ov>.
</vk>

</propertyValueKeyList>

</property>

@------------------------------------------------------------
<property
	short="firstLineIndent"
	long="firstLineIndent"
	value="dim"
	>
@- <propertyOld short="A" long="justify" value="L | R | C | LR" />

<pdp>
First line indent.
<vidx id="paragraph||first indent (firstLineIndent)" />
<vidx id="paragraph||indentation" />
</pdp>

<pdp>
Indent of the first line of paragraph text from the start side of
a text column or from the margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="startIndent"
	long="startIndent"
	value="dim"
	>

<pdp>
Start indent (excluding the first line
of the paragraph).
<vidx id="paragraph||start indent (startIndent)" />
</pdp>

<pdp>
Distance of the
second and succeeding lines of text from
the start side of a text column or from the start
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="endIndent"
	long="endIndent"
	value="dim"
	>

<pdp>
End indent.
<vidx id="paragraph||end indent (endIndent)" />
</pdp>

<pdp>
Distance of lines of text from
the end side of a text column or from the end
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="spaceAbove"
	long="spaceAbove"
	value="dim"
	>

<pdp>
Space above paragraph.
<vidx id="paragraph||space above (spaceAbove)" />
<vidx id="space||above paragraph (spaceAbove)" />
</pdp>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space before
the preceding paragraph, not the sum of the two.
See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="spaceBelow"
	long="spaceBelow"
	value="dim"
	>

<pdp>
Space below.
<vidx id="paragraph||space below (spaceBelow)" />
<vidx id="space||below paragraph (spaceBelow)" />
</pdp>

</property>

<pdp>
Actual inter-paragraph spacing
is determined by the larger of the
space above the current paragraph and the space before
the preceding paragraph, not the sum of the two.
See Note <xparanumonly
	id="P.note.spaceAboveBelow" /> on page <xnpage
	id="P.note.spaceAboveBelow" />.
</pdp>


@------------------------------------------------------------
<property short="un" long="pgfUseNext" value="N | Y" 
	condition="fmcGuide"
	>

<pdp>
Use next paragraph.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault v="N" />
</property>

@------------------------------------------------------------
<property short="np" long="pgfNext" value="name" 
	condition="fmcGuide"
	>

<pdp>
Next paragraph tag.
<vidx id="paragraph||next paragraph (np, un)" />
</pdp>

<propertyDefault>Null</propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Interline leading and line spacing mode"
	longTitle="Interline leading and line spacing mode"
	dest="MkDest.optgroup.line_spacing"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property short="leading" long="leading" value="dim" 
	>

<pdp>
Leading. Default units are points.
<vidx id="paragraph||line spacing (leading)" />
<vidx id="leading, space between lines" />
<vidx id="line spacing" />
</pdp>

<pdp>
Leading is the space <#fI>between<#fnI> lines
within a paragraph. Line spacing
(<#fI>baseline<#fnI> to <#fI>baseline<#fnI>)
is leading plus point size of the text.
@- (see Figure
@- <xparanumonly id="fig.inter_paragraph_spacing" file="textpos" />
@-  on page
@- <xnpage id="fig.inter_paragraph_spacing" file="textpos" />
@-  in the <#UGuide>).
</pdp>

</property>

@------------------------------------------------------------
<property
	long="lineSpacingMode"
	value="fixed | proportional"
	>

<pdp>
Line spacing mode.
<vidx id="paragraph||line spacing mode (fixed)" />
<vidx id="paragraph||line spacing mode (proportional)" />
<vidx id="line spacing in paragraph text||leading" />
<vidx id="line spacing in paragraph text||floating" />
<vidx id="line spacing in paragraph text||proportional" />
<vidx id="line spacing in paragraph text||fixed" />
<vidx id="anchored frames||inline" />
</pdp>

<pdp>
Setting <on>lineSpacingMode</on> to <ov>fixed</ov> results in
no adjustment for extra large characters, superscripts,
inline anchored frames, etc.
When <on>lineSpacingMode</on> is set to <ov>proportional</ov> the
inter-line spacing is adjusted to prevent
contact between text lines in cases where
the text contains either larger than normal
characters or inline anchored frames.
</pdp>
</property>

@------------------------------------------------------------
