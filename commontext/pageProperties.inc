@------------------------------------------------------------
@- pageProperties.inc
@------------------------------------------------------------

<@xmacro> pageProperties.inc {
	@if(<#elementName> = {DocDef}) {
		<#def> pp.width.desc1	{Default width for all pages in document.}
		<#def> pp.height.desc1	{Default height for all pages in document.}
		}
	@if(<#elementName> = {PageDef}) {
		<#def> pp.width.desc1	{Page width.
Overrides the value of the <#xDocDef> <pn textSize="7.8pt" >width</pn> property (see page <xnpage
	id="DocDef.property.width" />).}
		<#def> pp.height.desc1	{Page height.
Overrides the value of the <#xDocDef> <pn textSize="7.8pt" >height</pn> property (see page <xnpage
	id="DocDef.property.height" />).}
		}
}{}
@------------------------------------------------------------
<pageProperties.inc/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	@- short="W"
	long="width"
	value="dim"
	>

<pdp>
<#pp.width.desc1>. If the value of the <on>width</on> property
is less than the value of the <on>height</on> property (see below)
the document is portrait; otherwise it is landscape.
The minimum width allowed is 1 mm (ca. 0.04 inches).
<vidx id="page size||minimum width" />
<vidx id="minimum page width" />
</pdp>

</property>


@------------------------------------------------------------
<property
	@- short="H"
	long="height"
	value="dim"
	>
<pdp>
<#pp.height.desc1>. If the value of the <on>height</on> property
is greater than the value of the <on>page</on> property (see above)
the document is portrait; otherwise it is landscape.
The minimum height allowed is 1 mm (ca. 0.04 inches).
<vidx id="page size||minimum height" />
<vidx id="minimum page height" />
</pdp>

</property>

@------------------------------------------------------------
