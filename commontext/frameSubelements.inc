@------------------------------------------------------------
@- frameSubelements.inc
@-
@- Included in:
@-
@-	mathml
@-
@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart/>
@------------------------------------------------------------


@------------------------------------------------------------
<subElement
	name="ObjectTitle"
	dest="<#elementName>.subelement.ObjectTitle.start"
	content="Y"
	>
<subElementProperties>
<fI>properties</fI>
</subElementProperties>


<bbp>
Include a caption for the <#descriptionText>.
See the <#xObjectTitle> element on pages <xnpage
        id="ObjectTitle.element.start" /><xphyphen/><xnpage
        id="ObjectTitle.element.end" /> for <fI>properties</fI>.
Not applicable when <pn>position</pn> is set
to <pv>inline</pv> (see page <xnpage
	id="<#elementName>.property.position" />).
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="description"
	dest="<#elementName>.subelement.description.start"
	content="Y"
	>
<subElementProperties>
language ="<fI>name</fI>"
</subElementProperties>

<bbp>
Include a description of the <#descriptionText>. This is
for a read out loud application. The description text is not included in
the displayed output.
</bbp>

</subElement>

@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------
