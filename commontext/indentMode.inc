@------------------------------------------------------------
<!-- file = indentMode.inc (start) -->
<!-- Used in <P> and <ParaDef> elements	             -->
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="indentMode"
	value="relative | absolute"
	elementExclude="FootnoteDef"
	>

<pdp>
Set indent from edge of container or relative to
the effective <pn>startIndent</pn> of the parent paragraph.
<vidx id="indents||relative" />
<vidx id="relative indents||paragraphs" />
@- labelIndent, startINdent
</pdp>

<pdp>
Ignored unless the <#elementName> element is a child of a <#xP> element.
</pdp>
<pdp>
See <xsection
	id="ParaDef.ex.relativeIndent.start" /> on page <xnpage
	id="ParaDef.ex.relativeIndent.start" />.
</pdp>

<propertyDefault>relative</propertyDefault>

</property>

@------------------------------------------------------------
<!-- file = indentMode.inc (end) -->
@------------------------------------------------------------
