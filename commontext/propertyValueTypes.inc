@------------------------------------------------
<!-- File = propertyValueTypes.inc (Start) in $ctext -->
@------------------------------------------------
<bp>
See <nxsection
	psize="7.3pt"
	id="chapter.propertyValueTypes.start" /> on pages <xnpage
	id="chapter.propertyValueTypes.start" /><xphyphen/><xnpage
	id="chapter.propertyValueTypes.end" /> for a description
of the main property value types.
</bp>
@------------------------------------------------
<!-- File = propertyValueTypes.inc (End) in $ctext -->
@------------------------------------------------
