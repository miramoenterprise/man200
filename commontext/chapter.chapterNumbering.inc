@------------------------------------------------------------
<!-- File = chapter.chapterNumbering.inc (start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Chapter numbering"
	longTitle="Chapter numbering properties"
	dest="<#elementName>.propertyGroup.chapter_numbering"
>
The following properties set up
the <#xchapterNumber> sub-element.
<vidx id="building blocks||&lt;chapterNumber" />
<@skipStart>
<br/>
Use the <#xchapterNumber> sub-element
to add chapter numbers to running headers
and footers, add paragraph numbering (see <#xListLabel> on page
<xnpage
	id="ListLabel.element.start"
	file="inline" />), cross references (see <#xXRefDef> on page
<xnpage
	id="XRefFmtDef.code.start"
	file="fdefs" />), and tables of contents and indexes (see
<xparatextonly  id="books.chapter.start" file="books" />
on page <xnpage
	id="books.chapter.start"
	file="books" />).
<@skipEnd>
</propertyGroup>



@------------------------------------------------------------
<property
	@- short="CNstyle"
	long="chapterNumberStyle"
	value="key"
	>

<pdp>
Sets chapter number style.
</pdp>


@------------------------------------------------------------
@- Defined in ${control}/numberStyleValues.inc
@------------------------------------------------------------
<numberStyleValues propertyName="chapterNumberStyle" />
@------------------------------------------------------------

</property>


@------------------------------------------------------------
<property
	@- short="CNstring"
	long="chapterNumberStart"
	value="increment | int"
	>

<pdp>
Set chapter number.
</pdp>

</property>

@------------------------------------------------------------
<!-- File = chapter.chapterNumbering.inc (end) -->
@------------------------------------------------------------
