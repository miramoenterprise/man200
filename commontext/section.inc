@------------------------------------------------------------
@-- section.inc
@------------------------------------------------------------
@-
@- Used in: 
@-
@-	<Section>
@-	<SectionDef>
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> sectionDef1 {
<esp>
Unless otherwise specified by settings in the <#xDocDef> element
(see pages <xnpage
	id="SectionDef.element.start" /><xphyphen/><xnpage
	id="SectionDef.element.end" />) all
pages in single-sided documents have the layout of
the <sq>Right</sq> master page.
Similarly double-sided documents use
alternating <sq>Right</sq> and <sq>Left</sq> master pages,
starting with the <sq>Right</sq> page layout for the first page
of the document.
</esp>
}{}
@------------------------------------------------------------
