@------------------------------------------------------------
@-- cellVerticalAlign.inc
@------------------------------------------------------------

<@macro> cellVerticalAlignGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Vertical alignment of cell text"
			longTitle="Vertical alignment of cell text"
			dest="<#elementName>.optgroup.cell_vertAlign"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Vertical alignment of cell text (default for all cells in row)"
			longTitle="Vertical alignment of cell text (default for all cells in row)"
			dest="<#elementName>.optgroup.cell_vertAlign"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Vertical alignment of cell text (default for all cells in table)"
			longTitle="Vertical alignment of cell text (default for all cells in table)"
			dest="<#elementName>.optgroup.cell_vertAlign"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellVerticalAlignGroupHead>
@------------------------------------------------------------

<property
	name="verticalTextAlign"
	value="top | middle | bottom" >

<pdp>
Vertical alignment of the cell text block
within the <pn>startMargin</pn> and <pn>endMargin</pn> cell margins.
<vidx id="cell||vertical alignment of text in" />
<vidx id="vertical text alignment||in table cells" />
<vidx id="cell||vertical alignment of text in" />
<vidx id="alignment||table cell, vertical" />
<vidx id="table cell||vertical alignment of text in" />
<vidx id="table||vertical alignment of text in cells" />
<vidx id="centering text||in table cells, vertically (Ca=M)" />
</pdp>

<pdp>
If the <pn>cellOrientation</pn> property is set to <pv>90</pn> or <pv>-90</pn>,
the vertical text block is aligned horizontally.
</pdp>


@- <pdp>
@- See Chapter
@- <xparanumonly id="tables.chapter.start" file="tables" />,
@- <#osq>Tables<#csq>, in <fI>Miramo User Guide</fI>, page 
@- <xnpage id="tables.text_vertalign" file="tables" />,
@- for an example of using the <on>Ca</on> option.
@- </pdp>

</property>
@------------------------------------------------------------

