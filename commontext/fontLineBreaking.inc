@------------------------------------------------------------
@-- fontLineBreaking.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Line breaking"
	longTitle="Line breaking"
	dest="p.optgroup.lineBreaking"
	/>
@------------------------------------------------------------

<@include> ${CTEXT}/fontHyphenation.inc
<@include> ${CTEXT}/allowHyphenation.inc
<@include> ${CTEXT}/allowLineBreaks.inc
@------------------------------------------------------------

