@------------------------------------------------------------
<!-- File =  fontCapitalizationDefaults.inc (start) -->
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	@- shortTitle="Default text capitalization"
	@- longTitle="Default text capitalization"
	title="Default text capitalization"
	dest="<#elementName>.propertyGroup.textCapitalization.start"
/>
@------------------------------------------------------------
<@xmacro> setFontCapitalizationExtent {
	@if(<#elementName> = DocDef ) {
		<#def> fontCapitalizationExtent	{document}
		<#def> fcdDefault	{<#defaultSmallCapsSize>}
		}
	@if(@match({<#elementName>}, {Section})) {
		<#def> fontCapitalizationExtent	{section}
		<#def> fcdDefault	{as specified by
the <#xDocDef> <pn>smallCapsSize</pn> property (see page <xnpage
	id="DocDef.property.smallCapsSize" />).
}
		}
}{
}
@------------------------------------------------------------
<setFontCapitalizationExtent/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="smallCapsSize"
	value="percent"
	>

<pdp>
Specify default relative size of small caps for
the <#fontCapitalizationExtent> when <pn>capitalization</pv> is set
to <pv>smallCaps</pv> and <pn>smallCapsStyle</pv> is set
to <pv>simulated</pv>.
<vidx id="small caps" />
<vidx id="capitalization" />
</pdp>

<propertyDefault><#fcdDefault></propertyDefault>
</property>



@------------------------------------------------------------
<!-- File =  fontCapitalizationDefaults.inc (end) -->
@------------------------------------------------------------
