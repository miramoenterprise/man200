@------------------------------------------------------------
@- chapter.pageNumbering.inc
@------------------------------------------------------------
<@include> ${CTEXT}/def.template.inc

@------------------------------------------------------------
<propertyGroup
	shortTitle="Page numbering"
	longTitle="Page numbering options"
	dest="<#elementName>.propertyGroup.page_numbering"
	>
For setting up the document starting page
number.
</propertyGroup>


@------------------------------------------------------------
<property
	@- short="Nstyle"
	long="pageNumberStyle"
	value="key" >

<pdp>
Sets the chapter page number style.
</pdp>

@------------------------------------------------------------
@- Defined in ${CTEXT}/numberStyleValues.inc
@------------------------------------------------------------
@- <#numberStyleValues>
@------------------------------------------------------------

@------------------------------------------------------------
@- Defined in ${control}/numberStyleValues.inc
@------------------------------------------------------------
<numberStyleValues propertyName="pageNumberStyle" />
@------------------------------------------------------------


@- <propertyDefault>westernArabic</propertyDefault>
</property>

@------------------------------------------------------------
<property
	@- short="Nstart"
	long="pageNumberStart"
	value="increment | int" >

<pdp>
Specifies a fixed numeric value, <fI>int</fI>,
for the <#xpageNumber> sub-element, or whether to increment, the
starting page number for this book component.
</pdp>

<propertyDefault>1</propertyDefault>
</property>

