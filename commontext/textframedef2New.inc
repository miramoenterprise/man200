@------------------------------------------------------------
@-- textframedef2New.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Text frame dimensions"
	longTitle="Text frame dimensions"
	dest="<#elementName>.propertyGroup.dimensions"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	long="width"
	value="auto | dim"
	>
<pdp>
Width of text frame.
If <pn>width</pn> is set to <pv>auto</pv> the width of the
text frame is the container width <fI>minus</fI> the value
of the <pn>left</pn> property.
</pdp>

<propertyDefault>auto</propertyDefault> 

</property>

@------------------------------------------------------------
<property
	long="height"
	value="dim"
	>
<pdp>
Height of text frame.
</pdp>

<propertyDefault>60 mm</propertyDefault>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Text frame position"
	longTitle="Text frame position"
	dest="<#elementName>.propertyGroup.position"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="left"
	value="auto | dim"
	>
<pdp>
Distance of left side of text frame from left side
of enclosing container.
In the general case setting the <pn>left</pn> property
to <pv>auto</pv> is equivalent to setting it to <pv>0</pv>.
</pdp>

<pdp>
In the case the <#xTextFrameDef> element is referenced by
the <#xP> element <pn>frameAbove</pn> or <pn>frameBelow</pn> properties
setting the <pn>left</pn> property to <pv>auto</pv> results
in the left indent of the text frame being the same as
the <pn>startIndent</pn> of the associated <#xP> element.
See page <xnpage id="P.property.frameAbove" /> and see Note <xparanumonly
	id="note.TextFrameDef.frameAboveBelow" /> on page <xnpage
	id="note.TextFrameDef.frameAboveBelow" />.
</pdp>

<propertyDefault>auto</propertyDefault>

</property>

@------------------------------------------------------------
<property
	long="top"
	value="dim"
	>
<pdp>
Distance of top of text frame from top of enclosing frame or page.
</pdp>

<pdp>
Ignored when the <#xTextFrameDef> is referenced by
the <#xP> element <pn>frameAbove</pn> or <pn>frameBelow</pn> properties
See page <xnpage id="P.property.frameAbove" /> and see Note <xparanumonly
	id="note.TextFrameDef.frameAboveBelow" /> on page <xnpage
	id="note.TextFrameDef.frameAboveBelow" />.
</pdp>

<propertyDefault>0</propertyDefault>

</property>
@------------------------------------------------------------
