@------------------------------------------------------------
@-- fillProperties.inc
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	elementExclude="Equation|EquationDef|Image"
	title="Background fill properties"
	dest="<#elementName>.optgroup.fillProperties"
/>
@------------------------------------------------------------

@------------------------------------------------------------
@- Applies only if element is Equation|EquationDef
@------------------------------------------------------------
<propertyGroup
	element="Equation|EquationDef"
	title="Background fill properties"
	dest="<#elementName>.optgroup.fillProperties"
>
Background fill properties apply uniformly to both the equation bounding
box area and to the margin areas. If the &lt;math> markup
includes <pn>mathbackground</pn>, e.g. on the &lt;math> or &lt;mstyle> elements,
the maximum area of application for <pn>mathbackground</pn> is the equation
bounding box, excluding the margin areas.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
@- Applies only if element is Image
@------------------------------------------------------------
<propertyGroup
	element="Image"
	title="Background fill and overlay"
	dest="<#elementName>.optgroup.fillProperties"
>
Background fill properties apply only in the case
of images with transparency.
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="fillColor"
	value="name"
	>

<pdp>
Background fill color.
<fI>name</fI> is a color defined
using <#xColorDef>.

</pdp>

</property>

@------------------------------------------------------------
<property short="fillTint" long="fillTint" value="percent" >

<pdp>
Background fill tint as a <fI>percent</fI> value in the range 0&endash;100.
A tint value of 0 (zero) is the color white irrespective of
the setting of the <on>fillColor</on> property.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="fillOpacity"
	value="0 | 100" 
	>

<pdp>
A value of 100 sets the fill color to 100% opaque.
A value of 0 sets the fill color to 100% transparent.
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<@include>	${ctext}/svgDef.inc
@------------------------------------------------------------
