@------------------------------------------------------------
@- paraFrameTextBox.inc
@------------------------------------------------------------
<#def> endTab		{&emsp;&emsp;<endTab/>}
<@macro> paraFrame.switch {
	<#def> pftb.subElementName	{paraFrame}
	<#def> pftb.endTab		{<#endTab>}
	<#def> endTab			{&emsp;&emsp;<endTab/>}
	<#def> pftb.percent		{}
	@----------------------------------------------------
	@if(<#elementName> = {Font}
		OR <#elementName> = {FontDef}
		) {
		<#def> pftb.subElementName	{textBox}
		<#def> pftb.subElementRef	{<#xtextBox>}
		<#def> pftb.percent		{ | percent}
		<#def> pftb.endTab		{<br/>}
		@- <#def> endTab		{&emsp;<endTab/>}
		<#def> endTab		{<endTab/>}
	}
}
@------------------------------------------------------------
<|paraFrame.switch>
@------------------------------------------------------------
<@xmacro> paraFrame.details {
	@----------------------------------------------------
	<#def> pftb.subElementRef	{<#xparaFrame>}
	<#def> pftb.objectDesc		{a paragraph}
	<#def> pftb.pText1		{A paragraph containing
a <#pftb.subElementRef> sub-element cannot be split across columns or pages.}
	<#def> pftb.pText2		{paragraph text}
	<#def> pftb.pText3		{paragraph}
	<#def> pftb.pText4		{for the paragraph}
	<#def> pftb.fText1		{}
	@----------------------------------------------------
	@if(<#elementName> = {Font}
		OR <#elementName> = {FontDef}
		) {
		<#def> pftb.subElementRef	{<#xtextBox>}
		<#def> pftb.objectDesc		{text}
		<#def> pftb.pText1		{}
		<#def> pftb.pText2		{text}
		<#def> pftb.pText3		{around the text}
		<#def> pftb.pText4		{for the boxed text area}
		<#def> pftb.fText1		{Margin <fI>percent</fI> values
are a percentage of the boxed text size.}
		}
	@----------------------------------------------------
<bbp>
The <#pftb.subElementRef> sub-element specifies a background fill in and around
<#pftb.objectDesc>, and/or rulings around <#pftb.objectDesc>.
<#pftb.pText1><#pftb.fText1>
</bbp>
	@----------------------------------------------------
	@if(<#elementName> = {P} 
		) {
<bbp>
<!-- <P> element XRef to <ParaDef> zzp1 -->
See the <#xParaDef> <#xparaFrame> sub-element on page <xnpage
	id="ParaDef.subelement.paraFrame.start" />
for a detailed description of <#xparaFrame> properties.
</bbp>
	}
	@if(<#elementName> = {Font} 
		) {
<bbp>
<!-- <Font> element XRef to <FontDef> zzf1 -->
See the <#xFontDef> <#xtextBox> sub-element on page <xnpage
	id="FontDef.subelement.textBox.start" />
for a detailed description of <#xtextBox> properties.
</bbp>
	}
	@----------------------------------------------------
	@if(<#elementName> = ParaDef 
		OR <#elementName> = {FontDef}
		) {
@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="fillColor, fillTint, fillOpacity, svgDef" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the background fill color, tint and opacity <#pftb.pText4>, or
an <pn>svgDef</pn> name.
Default is transparent.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
@- topMargin and bottomMargin
@------------------------------------------------------------
	@if(<#elementName> = ParaDef ) {
<bbp labelWidth="60mm" labelText="topMargin, bottomMargin" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Margin area above and below <#pftb.pText2> to include in the background
fill area and/or for adjusting the positions of the top and bottom
rules.
The values of the top and bottom margins add to the vertical
positioning of the current <#pftb.pText2>, and the vertical positioning
of the following paragraphs.
Default value is <pv>0</pv>.
</bbp>
@------------------------------------------------------------
	}

@------------------------------------------------------------
	@if(<#elementName> = FontDef ) {
<bbp labelWidth="60mm" labelText="topMargin, bottomMargin" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Margin area above and below <#pftb.pText2> to include in the background
fill area and/or for adjusting the positions of the top and bottom
rules.
The values of the top and bottom margins have no effect on the vertical
positioning of text.
Default value is <pv>0</pv>.
</bbp>
	}
@------------------------------------------------------------

@------------------------------------------------------------
@- startMargin and endMargin
@------------------------------------------------------------
	@if(<#elementName> = ParaDef ) {
<bbp labelWidth="60mm" labelText="startMargin, endMargin" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Margin area at the start and end <#pftb.pText2> to include in the background
fill area and/or for adjusting the positions of the start and end
rules. The start and end margins extend outwards from the <#pftb.pText2>,
without affecting the positioning of the text.
In other words, the position
of the <#pftb.pText2> <pn>firstLineIndent</pn>, <pn>startIndent</pn> 
and <pn>endIndent</pn> relative to its start and end container borders
is not changed by by the values of <pn>startMargin</pn> and
<pn>endIndent</pn>.
The start margin anchor point is
the <fI>lesser</fI> of <pn>firstLineIndent</pn> or <pn>startIndent</pn>.
Default value is <pv>0</pv>.
</bbp>
	}
	@if(<#elementName> = FontDef ) {
<bbp labelWidth="60mm" labelText="startMargin, endMargin" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Margin area at the start and end <#pftb.pText2> to include in the background
fill area and/or for adjusting the positions of the start and end
rules. The start and end margins extend outwards from the <#pftb.pText2>.
Default value is <pv>0</pv>.
</bbp>
	}
@------------------------------------------------------------

@------------------------------------------------------------
@- topRule and bottomRule
@------------------------------------------------------------
	@if(<#elementName> = ParaDef ) {
<bbp labelWidth="60mm" labelText="topRule, bottomRule" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the names of rulings defined by the <#xRuleDef> element (see page <xnpage
	id="RuleDef.element.start" />) to apply horizontally
above and below the top and bottom margins. The widths of the top and
bottom rules add to the vertical positioning of the current <#pftb.pText2>.
Default value is <pv>none</pv>.
</bbp>
	}
	@if(<#elementName> = FontDef ) {
<bbp labelWidth="60mm" labelText="topRule, bottomRule" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the names of rulings defined by the <#xRuleDef> element (see page <xnpage
	id="RuleDef.element.start" />) to apply horizontally
above and below the top and bottom margins.
The thicknesses of the top and bottom rules have no effect on the vertical
positioning of text.
Default value is <pv>none</pv>.
</bbp>
	}
@------------------------------------------------------------

@------------------------------------------------------------
@- startRule and endRule
@------------------------------------------------------------
	@if(<#elementName> = ParaDef ) {
<bbp labelWidth="60mm" labelText="startRule, endRule" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the names of rulings defined by the <#xRuleDef> element (see page <xnpage
	id="RuleDef.element.start" />) to apply vertically
outside the start and end of the start and end margins.
Default value is <pv>none</pv>.
</bbp>
	}
	@if(<#elementName> = FontDef ) {
<bbp labelWidth="60mm" labelText="startRule, endRule" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the names of rulings defined by the <#xRuleDef> element (see page <xnpage
	id="RuleDef.element.start" />) to apply vertically
outside the start and end of the start and end margins.
The thicknesses of the start and end rules add to the lateral
spacing of text before and after the <#xtextBox>.
Default value is <pv>none</pv>.
</bbp>
	}
@------------------------------------------------------------

@------------------------------------------------------------
@- Rounded corners options
@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="roundedCorners" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Select rounded corners.
<br/>
Default value is <pv>N</pv>.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="cornerRadius" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
The value for all corner radii. If two vertical bar or 
whitespace-separated <fI>dim</fI> values are present, the first <fI>dim</fI> value
is the horizontal radius and the second <fI>dim</fI> value is the vertical
radius. In the case of a single <fI>dim</fI> value, the horizontal and vertical
radii are the same and the corner is circular rather than elliptical.
<br/>
Default value is <pv>3pt</pv> (if <pn>roundedCorners</pn> is
set to <pv>Y</pv>, zero otherwise).
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp labelWidth="80mm" labelText="topStartRadius, topEndRadius, bottomStartRadius, bottomEndRadius" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Override the value of <pn>cornerRadius</pn> for each corner.
</bbp>
@------------------------------------------------------------


@if(<#elementName> = P
	OR <#elementName> = {ParaDef}
	) {
@------------------------------------------------------------
<bbp>
In the case a paragraph contains a <#xparaFrame> sub-element
the <#xP> element <pn>spaceAbove</pn> and <pn>spaceBelow</pn> property
values specify offset distances from the tops and bottoms of top and
bottom margins <fI>plus</fI> the corresponding ruling thicknesses.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp>
See pages <xnpage
	id="paraFrame.ex.rulings.start"/><xphyphen/><xnpage
	id="paraFrame.ex.paraFrameUsage.end" /> for further details
on the operation of the <#xparaFrame> sub-element.
</bbp>
@------------------------------------------------------------
	}
@------------------------------------------------------------
	}
}{}
@------------------------------------------------------------


<@skipStart>
@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	fontProperties="N"
	onlyOneSubElement="1"
	/>
<@skipEnd>

<#def> paraFrame.string	{<fI>The following properties apply only when <pn textSize="7.3pt" >roundedCorners</pn> is set to</fI> <pv>Y</pv>:}
@- <#def> endTab		{&emsp;&emsp;<endTab/>}
<subElement
	name="<#pftb.subElementName>"
	@- dest="<#elementName>.subelement.paraFrame.start"
	dest="<#elementName>.subelement.<#pftb.subElementName>.start"
	fontProperties="N"
	>
<subElementProperties tabFix="Y" >
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="fillColor" value= "name" desc="Background fill color" /><MkDest
	id="<#elementName>.<#pftb.subElementName>.svgDef" /> ]
<#endTab>
[ <subProperty name="fillTint" value= "percent" desc="Background fill tint" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="fillOpacity" value= "percent" desc="Background fill opacity" /> ]
<#endTab>
[ <subProperty name="svgDef" value= "name" desc="Background SVG style" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="startMargin" value= "dim<#pftb.percent>" desc="Margin at start of paragraph body" /> ]
<#endTab>
[ <subProperty name="endMargin" value= "dim<#pftb.percent>" desc="Margin at the end side of paragraph body" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="topMargin" value= "dim<#pftb.percent>" desc="Margin above paragraph body" /> ]
<#endTab>
[ <subProperty name="bottomMargin" value= "dim<#pftb.percent>" desc="Margin below paragraph body" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="startRule" value= "name" desc="Rule at start of paraFrame" /> ]
<#endTab>
[ <subProperty name="topRule" value= "name" desc="Rule at top of paraFrame" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="endRule" value= "name" desc="Rule at end of paraFrame" /> ]
<#endTab>
[ <subProperty name="bottomRule" value= "name" desc="Rule at bottom of paraFrame" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="roundedCorners" value= "N | Y" desc="Enable rounded corners" /> ]
<#endTab>
<br/>
@--------------------------------------------------------------------------------
<#paraFrame.string>
<br/>
@--------------------------------------------------------------------------------
[ <subProperty name="cornerRadius" value= "dim [dim]" desc="Set for all corners. Both radii, or x radius and y radius" /> ]
<#endTab>
[ <subProperty name="topStartRadius" value= "dim [dim]" desc="Both radii, or x radius and y radius" /> ]
<br/>
[ <subProperty name="topEndRadius" value= "dim [dim]" desc="Both radii, or x radius and y radius" /> ]
<#endTab>
<br/>
[ <subProperty name="bottomStartRadius" value= "dim [dim]" desc="Both radii, or x radius and y radius" /> ]
<#endTab>
<br/>
[ <subProperty name="bottomEndRadius" value= "dim [dim]" desc="Both radii, or x radius and y radius" /> ]
<#endTab>
<br/>
</subElementProperties>

@------------------------------------------------------------
<paraFrame.details/>
@------------------------------------------------------------


</subElement>

@------------------------------------------------------------
@- <subElementsEnd/>
@------------------------------------------------------------
