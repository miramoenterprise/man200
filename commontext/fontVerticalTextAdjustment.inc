@------------------------------------------------------------
<!-- File = fontVerticalTextAdjustment.inc (start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Vertical text adjustment"
	longTitle="Vertical text adjustment"
	dest="<#elementName>.propertyGroup.vertical_horizontal_shift.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="superscript"
	long="superscript"
	value="N | Y"
	>

<pdp>
Superscript.
<vidx id="superscript (text)" />
</pdp>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
E.g.
</pdp>

<exampleBlock element="Font" >
Normal text<Font superscript="Y" >superscript</Font> normal text
</exampleBlock>

<pdp element="Font" >
produces:<br/>
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>

@------------------------------------------------------------
<property
	short="subscript"
	long="subscript"
	value="N | Y"
	>

<pdp>
Subscript.
<vidx id="subscript (text)" />
</pdp>

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<exampleBlock element="Font" >
Normal text<Font subscript="Y" >subscript</Font> normal text
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>

<propertyDefault v="N" />
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="verticalShift"
	long="verticalShift"
	value="percent"
	>

<pdp>
Vertical displacement of text.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

<pdp element="Font" >
<fI>percent</fI> is the required text downshift
expressed as a percentage of an em in the current font size.
If <fI>percent</fI> is preceded by a minus
sign the text is upshifted.
</pdp>


<pdp element="Font" >
The upshift or downshift is
relative to the default paragraph baseline.
</pdp>

<pdp element="Font" >
If the paragraph line spacing is set to fixed using
the paragraph properties in a template document or by
setting the <#xParaDef> or <#xP> element <on>lineSpacingMode</on> property
to <ov>fixed</ov> (see pages <xnpage
	id="ParaDef.property.lineSpacingMode"  /> and <xnpage
	id="P.property.lineSpacingMode" file="inline" />),
the shifted text may interfere
with text above or below and may also
extend above or below the border of a
text frame or a table cell.
</pdp>



<pdp element="Font" 
	>
This input
</pdp>
<exampleBlock
	 element="Font" >
Some text <Font textSize="3mm" ><Font verticalShift="100" >down1
<Font verticalShift="200" >down2<Font verticalShift="-50" >up1
</Font></Font></Font></Font> more text...
</exampleBlock>

<pdp element="Font"
	lineSpacingMode="proportional"
	>
produces:<br/>
<#example_output>
</pdp>


<propertyDefault v="0" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = fontVerticalTextAdjustment.inc (end) -->
@------------------------------------------------------------
