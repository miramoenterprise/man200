@------------------------------------------------------------
@- contents.inc
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="position"
	value=" normal | topOfColumn
| topOfPage | topOfEvenPage | topOfOddPage" >

<pdp>
Contents placement.
<vidx id="contents||placement on page" />
</pdp>

<propertyValueKeyList
	keyWidth="20mm"
	>
<vk key="normal" >
Start contents at the current position.
</vk>

<vk key="topOfColumn" >
Start contents at top of next column
</vk>

<vk key="topOfPage" >
Start contents at top of next page
</vk>

<vk key="topOfEvenPage" >
Start contents at top of next even page
</vk>

<vk key="topOfOddPage" >
Start contents at top of next odd page
</vk>
</propertyValueKeyList>

<@skipStart>
<pdp>
For single-sided documents (see the <#xDocDef> <pn>doubleSided</pn> property
on page <xnpage
	id="DocDef.property.doubleSided" >),
setting the <pn>position</pn> property to any of <pv>topOfPage</pv>,
<pv>topOfLeftPage</pv>, or <pv>topOfRightPage</pv>, has the same effect, which is to
force the paragraph to start at the top of a page.
</pdp>
<@skipEnd>


</property>


@------------------------------------------------------------
<property
	short="chapterOnly"
	long="chapterOnly"
	value="N | Y"
	>

<pdp>
If <pn>chapterOnly</pn> is set to <pv>Y</pv> and the <#xContents> element
occurs within a <#xChapter> element (see pages <xnpage
	id="Chapter.element.start" /><xphyphen/><xnpage
	id="Chapter.element.end" />), the generated TOC will only
include paragraphs present within the <#xChapter> element.
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------
