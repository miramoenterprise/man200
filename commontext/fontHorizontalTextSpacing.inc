@------------------------------------------------------------
<!-- File = fontHorizontalTextSpacing.inc (start) -->
@------------------------------------------------------------

@------------------------------------------------------------
@-
@- textKern
@- ligatures
@- textSpread
@-
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Horizontal text spacing"
	longTitle="Horizontal text spacing"
	dest="<#propertyName>.optgroup.horizontalTextSpacing.start"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="textKern"
	long="textKern"
	value="Y | N"
	>


<docNote>
BUG: fix mmc
</docNote>

<pdp>
Enable pair kerning.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------


<pdp element="Font" >
This input
</pdp>

<exampleBlock
	element="Font"
	>
@- <Font textSize="4mm" ff="Palatino Linotype" K="Y" >
<Font textSize="3mm" fontFamily="Times New Roman" textKern="Y" >AWAY</Font> and
 <Font textSize="3mm" fontFamily="Palatino Linotype" textKern="N" >AWAY</Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="ligatures"
	long="ligatures"
	value="N | Y"
	>

<pdp>
Enable ligatures.
</pdp>

<pdp element="Font" >
This input
</pdp>

<exampleBlock
	element="Font"
	>
@- <Font textSize="3mm" ff="Palatino Linotype" fontWeight="Regular" >
<Font textSize="3mm" fontFamily="Palatino Linotype" >No ligature ffi: Office<Font
@- ligatures="Y" <hc>hyphenate="N"</hc>> | Ligature ffi: Office</Font></Font>
ligatures="Y" <hc>hyphenate="Y"</hc>>  Ligature ffi: Office</Font></Font>
</exampleBlock>


<pdp element="Font" >
produces:
<#example_output>
</pdp>

</property>

@------------------------------------------------------------
<property
	short="textSpread"
	long="textSpread"
	value="percent"
	element="Font|FontDef"
	>

<pdp>
Inter-character spread, or tracking.
</pdp>



<pdp>
<fI>percent</fI>
is relative
<Font K="N" ><Font textSpread="20" >increase/
<Font textSpread="-5" >decrease
</Font>
</Font>
</Font>
in character spread. Negative
values reduce character spread.
Spread is expressed as a percentage of
the font size.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------
@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp element="Font" >
The character spread property in <#xFontDef>
or the <#xFont> <on>textKern</on> property (above)
must be switched on
for character spread values to affect output.
</pdp>


<@skipStart>
<pdp element="Font" >
Small values of character spread,
e.g. &lt;Font textSpread="20" >
<Font textSpread="20" >increase
</Font>&lt;/Font&gt;
, and especially, 
&lt;Font textSpread="-5" &gt;
<Font textSpread="-5" >decrease
</Font>&lt;/Font&gt;
have a big visual effect.
</pdp>
<@skipEnd>



<@skipStart>
<pdp element="Font" >
(For examples showing the effect of the
&lt;Font... >
<on>textSpread</on> property see the
<fI>Miramo User Guide</fI>, Chapter
<xparanumonly id="textpos.chapter.start" file="textpos" />
, pages 
<xnpage id="textpos_character_spread.start"
	file="textpos" /><xphyphen/><xnpage
	id="textpos_character_spread.end" file="textpos" />
.)
</pdp>
<@skipEnd>


<propertyDefault v="0" />
</property>
@------------------------------------------------------------


@------------------------------------------------------------

<property
	long="letterSpacing"
	value="N | Y"
	>
<pdp>
Allow additional letter spacing.
<xidxstart id="paragraph||word spacing" />
<xidxstart id="word spacing" />
<vidx id="tracking, in text" />
<vidx id="character spacing" />
<vidx id="letter spacing" />
<vidx id="spread, character" />
</pdp>

<pdp>
Inter-word spacing in justified text
may sometimes have to exceed the maximum
specified by the <on>maximumWordSpace</on> property (see below).
In such cases,
setting the <on>letterSpacing</on> property to <ov>Y</ov> will allow additional space
to be added between letters within words
to reduce excessive spacing between words.
</pdp>

</property>


@-----------------------------
<property
	short=""
	long="maximumWordSpace"
	value="percent"
	@- condition="fmcGuide"
	>

<pdp>
Maximum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
</pdp>

</property>

@------------------------------------------------------------
<property
	short=""
	long="optimumWordSpace"
	value="percent"
	@- condition="fmcGuide"
	>

<pdp>
Optimum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------


<pdp element="Font" >
E.g. the following
</pdp>


<exampleBlock
	element="Font"
        >
<P textSize="7pt" optimumWordSpace="500" <hc> fi="5mm" li="5mm" </hc> >Like as the waves
make towards the pebbl'd shore,<Font optimumWordSpace="50" > So do our
minutes hasten</Font></P>
</exampleBlock>

<pdp element="Font" >
produces:
<#example_output>
</pdp>
<@skipStart>
<Row>
<Cell/>
<Cell/>
<Cell cs="R">
<#example_output>
</Cell>
</Row>
<@skipEnd>

</property>

@------------------------------------------------------------
<property
	name="minmumWordSpace"
	value="percent"
	>

<pdp>
Minimum inter-word spacing. A value
of 100 is equivalent to one quarter
of an em space.
<xidxend id="paragraph||word spacing" />
<xidxend id="word spacing" />
</pdp>

</property>

<@skipStart>
@------------------------------------------------------------
<property
	name="preserveSpaces"
	value="N | Y"
	>

<pdp>
Series of contiguous space characters are treated as a single space by default.
Setting the <pn>preserveSpaces</pn> property to <pv>Y</pv> ensures that all
space characters are included in the output.
@- What is the set of 'space characters' in this context???
</pdp>

</property>
<@skipEnd>

@------------------------------------------------------------
<!-- File = fontHorizontalTextSpacing.inc (end) -->
@------------------------------------------------------------
