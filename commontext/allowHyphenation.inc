@------------------------------------------------------------
@-  allowHyphenation.inc
@-  
@------------------------------------------------------------
<!-- FILE: ${ctext}/allowHyphenation.inc 	START start -->
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="hphenationBefore"
	value="chars"
	>
<pdp>
Add additional characters that allow a hyphenation before.
The additional characters may be included as a space-separated list
of UTF-8 characters, or as numeric character entities (e.g. &amp;#x<fI>hhhh</fI>;).
</pdp>

</property>

@------------------------------------------------------------
<property
	long="hphenationAfter"
	value="chars"
	>
<pdp>
Add additional characters that allow a hyphenation after.
</pdp>

</property>

@------------------------------------------------------------
<!-- FILE: ${ctext}/allowHyphenation.inc 	END end -->
@------------------------------------------------------------
