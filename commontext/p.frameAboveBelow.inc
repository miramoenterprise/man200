@---------------------------------------------
@-- p.frameAboveBelow.inc
@---------------------------------------------

@-----------------------------
<propertyGroup
	shortTitle="Reference frames above and below"
	longTitle="Reference frames above and below"
	XRefID="P.optgroup.reference frames"
>
See in the description of the <#xTextFrameDef> element, <xsectionXRef
	id="TextframeDef.frameAboveBelow.start" /> on
pages <xnpage
	id="TextframeDef.frameAboveBelow.start" /><xphyphen/><xnpage
	id="TextframeDef.frameAboveBelow.end" />.
@- Annotated output from Examples <xparanumonly
@-       id="ex.TextFrameDef.frameAboveBelow1" /> and <xparanumonly
@-       id="ex.TextFrameDef.frameAboveBelow2" />
<vidx id="paragraph||rules above or below" />
<vidx id="paragraph||graphics above or below" />
<vidx id="rules||above paragraphs" />
<vidx id="graphics||above/below every paragraph" />
</propertyGroup>


@-----------------------------
<property
	short=""
	long="frameAbove"
	value="none | name" >

<pdp>
Top or above separator frame. <#fI>name<#fnI> refers
to the value of a <#xTextFrameDef> element <pn>textFrameDef</pn> property.
</pdp>

<propertyDefault>none</propertyDefault>
</property>

@-----------------------------
<property
	short=""
	long="frameBelow"
	value="none | name"
	>

<pdp>
Bottom or below separator frame. <#fI>name<#fnI> refers
to the value of a <#xTextFrameDef> element <pn>textFrameDef</pn> property.
</pdp>

<pdp>
See <on>frameAbove</on> property above.
</pdp>

</property>
@-----------------------------
