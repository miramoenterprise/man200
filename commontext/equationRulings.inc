@--------------------------------------------------------------------------------
@- equationRulings.inc
@--------------------------------------------------------------------------------
<!-- equationRulings.inc START start -->


@-===========================================================
<propertyGroup
	shortTitle="Border rulings"
	longTitle="Border rulings"
	dest="optgroup.<#elementName>.outside_rulings.start"
>
In the following <fI>name</fI>> must be the
name of a ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" />.
Set <fI>name</fI> to "none" for no ruling.
<MkDest id="<#elementName>.rulings"/>
</propertyGroup>
@-===========================================================


@------------------------------------------------------------
<property
	long="startRule"
	value="name"
	>

<pdp>
Start-side ruling.
<vidx id="equation||border rulings" />
</pdp>
<propertyDefault>none</propertyDefault>
</property>

@------------------------------------------------------------
<property
	@- short="tr"
	long="topRule"
	value="name"
	>

<pdp>
Top-side ruling.
</pdp>
<propertyDefault>none</propertyDefault>
</property>


@------------------------------------------------------------
<property
	long="endRule"
	value="name"
	>

<pdp>
End-side ruling.
</pdp>
<propertyDefault>none</propertyDefault>
</property>


@------------------------------------------------------------
<property long="bottomRule" value="name" >

<pdp>
Bottom-side outside ruling.
</pdp>
<propertyDefault>none</propertyDefault>
</property>

@------------------------------------------------------------
<!-- equationRulings.inc END end -->
@------------------------------------------------------------
