@------------------------------------------------------------
<!-- File = frameIndentMMC.inc (start) -->
@------------------------------------------------------------
@- Included in:
@- 
@- 	AFrame
@- 	MathML
@------------------------------------------------------------

@------------------------------------------------------------
@- Defined in ${control}/referenceGuide.mmp
@------------------------------------------------------------
<descriptionTextDef/>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	title="Indentation properties"
	/>


@------------------------------------------------------------
<property
	name="startIndent"
	value="dim"
	>

<pdp>
Position of <#descriptionText> relative to the start side of the container.
Ignored unless the <pn>align</pn> property is set to <pv>start</pv> and
the <pn>position</pn> property is set to <pv>below</pv>.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="endIndent"
	value="dim"
	>

<pdp>
Position of <#descriptionText> relative to the end side of the container.
Ignored unless the <pn>align</pn> property is set to <pv>end</pv> and
the <pn>position</pn> property is set to <pv>below</pv>.
</pdp>

</property>

@------------------------------------------------------------
<@include>	${ctext}/indentMode.inc
@------------------------------------------------------------


@------------------------------------------------------------
<!-- File = frameIndentMMC.inc (end) -->
@------------------------------------------------------------
