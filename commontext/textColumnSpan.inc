@------------------------------------------------------------
<!-- start START textColumnSpan.inc -->
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> tblOrParagraph {
	<#def> tblOrParagraph	{paragraph}
	@if(@match({X<#elementName>X}, {XTblX|XTblDefX})) {
		<#def> tblOrParagraph	{table}
		}
}
@------------------------------------------------------------
<|tblOrParagraph>
@------------------------------------------------------------

<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Multi-column layout"
	longTitle="Multi-column layout"
	dest="ATextFrame.optgroup.multi_column_layout"
	condition="fmcGuide"
/>
<@skipEnd>


@------------------------------------------------------------
<property
	name="textColumnSpan"
	value="none | all "
	>

<pdp>
If the <#tblOrParagraph> is located within a text frame with multiple
text columns (see the <#xTextFrameDef> <pn>columns</pn> property
on page <xnpage
	id="TextFrameDef.property.columns" />) and
the <pn>textColumnSpan</pn> is set
to <pv>all</pv>, then the <#tblOrParagraph> spans all the text columns.
</pdp>

<@skipStart>
<pdp>
See the <#xTextFrameDef> <pn>columns</pn> property on page <xnpage
	id="TextFramdDef.property.columns" />
</pdp>
<@skipEnd>

<pdp>
The <pn>textColumnSpan</pn> property has no effect within single-column
text frames.
</pdp>

<pdp element="ParaDef|P" >
Footnotes included within paragraphs that are spanned across multiple
text columns are located at the bottom of, and within, the first
text column.
</pdp>

<propertyDefault v="none" />
</property>

@------------------------------------------------------------
<!-- end END textColumnSpan.inc -->
@------------------------------------------------------------
