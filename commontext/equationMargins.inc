@--------------------------------------------------------------------------------
@- equationMargins.inc
@--------------------------------------------------------------------------------
<!-- equationMargins.inc START start -->


@-===========================================================
<propertyGroup
	shortTitle="Margins"
	longTitle="Margins"
	dest="optgroup.<#elementName>.margins.start"
>
Specify margins around the equation bounding box.
Equation margin properties are inapplicable when <pn>position</pn> is
is set to <pn>inline</pn>.
</propertyGroup>
@-===========================================================


@------------------------------------------------------------
<property
	long="startMargin"
	value="dim"
	>

<pdp>
Margin at the beginning of the equation.
<vidx id="equation, MathML||margin at start of" />
<vidx id="equation, MathML||start margin" />
</pdp>
<propertyDefault>0pt</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="topMargin"
	value="dim"
	>

<pdp>
Margin above the equation.
<vidx id="equation, MathML||top margin" />
</pdp>
<propertyDefault>0pt</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="endMargin"
	value="dim"
	>

<pdp>
Margin at the end of the equation.
<vidx id="equation, MathML||margin at end of" />
</pdp>
<propertyDefault>0pt</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="bottomMargin"
	value="dim"
	>

<pdp>
Margin below the equation.
<vidx id="equation, MathML||bottom margin" />
</pdp>
<propertyDefault>0pt</propertyDefault>
</property>

@------------------------------------------------------------
<!-- equationMargins.inc END end -->
@------------------------------------------------------------
