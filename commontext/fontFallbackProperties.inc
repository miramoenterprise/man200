@------------------------------------------------------------
<!-- File = fontFallbackProperties.inc (start) -->
@------------------------------------------------------------
@- Used in MathML & MathMLDef
@- Used in Equation & EquationDef
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Fallback font properties"
	dest="<#elementName>.propertyGroup.fallback_font_selection"
>
When <pn>type</pn> is set to <pv>mathml</pv>, specify the fallback font to use for
characters in the equation that
are not contained in the currently-applicable font.

<P spaceAbove="7pt" >
The <sq>currently applicable font</sq> is either the font used in the current
text context, or the font specified by the <#xMathML> or the <#xMathMLDef> element
<pn>fontFamily</pn> property.
</P>

</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="fallbackFontFamily"
	value="name"
	>

<pdp>
Fallback font family. E.g. <pv>Cambria Math</pv>, <pv>Quivira</pv>.
<pn>fallbackFontFamily</pn> is ignored when <pn>type</pn> is set to <pv>latex</pv>.
See Note <xparanumonly
	id="note.EquationDef.mathjax1" /> on page <xnpage
	id="note.EquationDef.mathjax1" />.
</pdp>

<propertyDefault><pv>Quivira</pv></propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = fontFallbackProperties.inc (end) -->
@------------------------------------------------------------

