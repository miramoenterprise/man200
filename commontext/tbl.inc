@------------------------------------------------------------
<!-- File = tbl.inc  (Start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Basic properties"
	longTitle="Basic properties"
	dest="optgroup.Tbl.basic_properties.start"
	/>


@------------------------------------------------------------
<property
	long="startIndent"
	value="dim"
	>

<pdp>
Start indent. Or right indent in the case of right-to-left text.
</pdp>

<pdp>
Ignored if the <on>align</on> property is set to <ov>end</ov> (see below).
</pdp>

</property>

@------------------------------------------------------------
<property
	long="endIndent"
	value="dim"
	>

<pdp>
End indent. Or left indent in the case of right-to-left text.
</pdp>

<pdp>
Ignored if the <on>align</on> property is set to <ov>start</ov> (see below).
</pdp>

</property>

@------------------------------------------------------------
<@include>	${ctext}/indentMode.inc
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="spaceAbove"
	value="dim" >

<pdp>
Space above.
</pdp>

</property>

@------------------------------------------------------------
<property
	name="spaceBelow"
	value="dim"
	>

<pdp>
Space below.
</pdp>

</property>


@------------------------------------------------------------
<property
	name="align"
	value="start | center | end" >

<pdp>
Horizontal alignment of table within text column.
<vidx id="table||horizontal placement" />
<vidx id="table||centering" />
<vidx id="table||left aligning" />
<vidx id="table||right aligning" />
</pdp>

<propertyValueKeyList
	>
<vk key="start" >
Align table with start of text direction.
</vk>
<vk key="center" >
Center table
</vk>
<vk key="end" >
Align table with end of text direction.
</vk>
</propertyValueKeyList>

</property>

@------------------------------------------------------------
<property
	name="position"
	value=" normal | topOfColumn 
| topOfPage | topOfEvenPage | topOfOddPage" >

<pdp>
Table placement.
<vidx id="table||placement on page" />
@- <vidx id="table||floating" />
@- <vidx id="floating table" />
</pdp>

<propertyValueKeyList
	keyWidth="20mm"
	>
<vk key="normal" >
Start table at the current position.
</vk>

<vk key="topOfColumn" >
Start table at top of next column
</vk>

<vk key="topOfPage" >
Start table at top of next page
</vk>

<vk key="topOfEvenPage" >
Start table at top of next even page
</vk>

<vk key="topOfOddPage" >
Start table at top of next odd page
</vk>

</propertyValueKeyList>

<pdp
	condition="fmcGuide"
	>
@- If <on>P</on> is set to <ov>F</ov> text will backfill space
@- ahead of the table
</pdp>

</property>

@------------------------------------------------------------
<@include>	${CTEXT}/textColumnSpan.inc
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="minimumRows"
	value="int"
	>

<pdp>
Minimum number of widow/orphan rows.
</pdp>

</property>


@------------------------------------------------------------
<property short="anc" long="tblAutonumAxis" value="R | C"
	condition="fmcGuide"
	>

<pdp>
Autonumbering direction for paragraphs and footnotes
within the table. R = numbering across rows, C = numbering
down columns.
</pdp>

</property>


@------------------------------------------------------------
<@include> ${CTEXT}/cellMargins.inc
@------------------------------------------------------------

@------------------------------------------------------------
@- Fill for all table cells
<@include> ${CTEXT}/cellFillColor.inc
@------------------------------------------------------------


@============================================================
<propertyGroup
	title="Header, footer and body paragraph formats"
	dest="propertyGroup.Tbl.row_paragraphs.start"
	>
</propertyGroup>

@------------------------------------------------------------
<property name="headerRowParaDef" value="name" >

<pdp>
Paragraph format for header rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="bodyRowParaDef" value="name" >

<pdp>
Paragraph format for body rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="footerRowParaDef" value="name" >

<pdp>
Paragraph format for footer rows.
</pdp>

</property>


@============================================================
<propertyGroup
	title="Header, footer and body fills"
	dest="propertyGroup.Tbl.row_fills.start"
	>
</propertyGroup>

@------------------------------------------------------------
<property name="headerRowFillColor" value="name" >

<pdp>
Background color for header rows. <on>headerRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property name="headerRowFillOpacity" value="0 - 100" >

<pdp>
Fill opacity for header rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="headerRowFillTint" value="percent" >

<pdp>
Background tint for header rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="headerRowSvgDef" value="name" >

<pdp>
Apply a SVG background as defined by an <#xxSvgDef> element to
@- a block of one or more header rows. See pages <xnpage
the entire set of header rows. See pages <xnpage
        id="xSvgDef.element.start" /><xphyphen/><xnpage
        id="xSvgDef.element.end" />.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillColor" value="name" >

<pdp>
Background color for body rows. <on>bodyRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillOpacity" value="0 - 100" >

<pdp>
Fill opacity for body rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="bodyRowFillTint" value="percent" >

<pdp>
Background tint for body rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="bodyRowSvgDef" value="name" >

<pdp>
Apply a SVG background as defined by an <#xxSvgDef> element to
the entire set of body rows. See pages <xnpage
        id="xSvgDef.element.start" /><xphyphen/><xnpage
        id="xSvgDef.element.end" />.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillColor" value="name" >

<pdp>
Background color for footer rows. <on>footerRowFillOpacity</on> must be
set to <ov>100</ov> for the specified color to display.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillOpacity" value="0 - 100" >

<pdp>
Fill opacity for footer rows.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerRowFillTint" value="percent" >

<pdp>
Background tint for footer rows.
</pdp>

</property>

@------------------------------------------------------------
<property name="footerRowSvgDef" value="name" >

<pdp>
Apply a SVG background as defined by an <#xxSvgDef> element to
entire set of footer rows. See pages <xnpage
        id="xSvgDef.element.start" /><xphyphen/><xnpage
        id="xSvgDef.element.end" />.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	title="All rulings"
	dest="optgroup.Tbl.all_rulings.start"
>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="allRules"
	value="name"
	>

<pdp>
Default for <fI>all</fI> rules in table.
</pdp>
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	title="Outside border rulings"
	dest="optgroup.Tbl.outside_rulings.start"
>
(In the following <#name> must be the
name of a ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" />.
Set <fI>name</fI> to "none" for no ruling.)
<MkDest id="Tbl.propertyGroup.border_rulings"/>
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="startRule"
	value="name"
	>

<pdp>
Start outside ruling.
<vidx id="table||ruling, outside" />
</pdp>
</property>

@------------------------------------------------------------
<property
	name="topRule"
	value="name"
	>

<pdp>
Top outside ruling.
</pdp>
</property>


@------------------------------------------------------------
<property
	long="endRule"
	value="name"
	>

<pdp>
End outside ruling.
</pdp>
</property>


@------------------------------------------------------------
<property long="bottomRule" value="name" >

<pdp>
Bottom outside ruling.
</pdp>
</property>


<@skipStart>
@------------------------------------------------------------
<property long="lastRuleOnly" value="N | Y" >

<pdp>
Use ruling on last row. Y means draw bottom rule on 
last sheet only; N means draw rule on the bottom
of every sheet.
<vidx id="table||ruling, under last row only" />
</pdp>
</property>
<@skipEnd>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Header, footer and body row rulings"
	longTitle="Header, footer and body row rulings"
	dest="propertyGroup.Tbl.row_rulings.start"
	>
(In the following <#name> must be the
name of a Ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" />.
Set <fI>name</fI> to "none" for no ruling.)
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<property long="bodyRowRule" value="name" >

<pdp>
Ruling used between rows in the table body.
</pdp>

</property>

<@skipStart>
@------------------------------------------------------------
<property long="footerRowRule" value="name" >

<pdp>
Ruling used between rows in the table footer.
</pdp>

</property>
<@skipEnd>


@------------------------------------------------------------
<property long="headerSeparatorRule" value="name" >

<pdp>
<fI>name</fI> is the name of the ruling to
use between the last heading row 
and the first body row.
</pdp>

</property>

@------------------------------------------------------------
<property long="footerSeparatorRule" value="name" >

<pdp>
<fI>name</fI> is the name of the ruling to
use between the last body row 
and the first footer row.
</pdp>

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Column rulings"
	longTitle="Column rulings"
	dest="propertyGroup.Tbl.column_rulings.start"
>
(In the following <#name> must be the
name of a Ruling defined in a
template file or by using the <#xRuleDef> element, see page <xnpage
	id="RuleDef.element.start" />.
Set <fI>name</fI> to "none" for no ruling.)
<MkDest id="Tbl.element.rulings"/>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="columnRule"
	value="name"
	>

<pdp>
<fI>name</fI> is the name of the ruling to
use between columns.
</pdp>

</property>

@------------------------------------------------------------
<@include> ${CTEXT}/cellVerticalAlign.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Footnote properties"
	longTitle="Footnote properties"
	dest="propertyGroup.Tbl.footnoteProperties.start"
>
When <#xFNote> elements (see pages <xnpage
	id="FNote.element.start" /><xphyphen><xnpage
	id="FNote.element.end" />) are contained within a table the
output footnotes may be located either at the end of the
table or repeated after the table footer on every page.
Footnote numbering within tables is always restarted from 1.
@- (In the following <#name> must be the
@- name of a Ruling defined in a
@- template file or by using the <#xRuleDef> element, see page <xnpage
@-	id="RuleDef.element.start" />.
@- Set <fI>name</fI> to "none" for no ruling.)
@- <MkDest id="Tbl.element.rulings"/>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="footnotePosition"
	value="atEnd | atFoot"
	changeBar="Y"
	>

<pdp>
Locate all table footnotes either at the end of the table or
repeated at the bottom of every page of the table.
<vidx id="table footnotes" />
<vidx id="footnotes||in tables" />
</pdp>

<propertyValueKeyList
        >
<vk key="atEnd" >
Locate all footnotes within the table at the end of the table.
</vk>
<vk key="atFoot" >
Locate all footnotes within the table at foot of the table.
</vk>
</propertyValueKeyList>

<propertyDefault>atEnd</propertyDefault>

</property>

@------------------------------------------------------------
<property
	name="fNoteDef"
	value="name"
	changeBar="Y"
	>

<pdp>
Name of the <#xFNoteDef> format definition to use for this
table format. See pages <xnpage
	id="FNoteDef.element.start" /><xphyphen><xnpage
	id="FNoteDef.element.end" />).
</pdp>


<propertyDefault>tableFootnote</propertyDefault>

</property>


@------------------------------------------------------------
<@include> ${CTEXT}/tblcontinuationdef.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@include> ${CTEXT}/tblRowProperties.inc
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = tbl.inc  (End) -->
@------------------------------------------------------------
