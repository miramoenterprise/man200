@------------------------------------------------------------
<!-- file = frameDecorations.inc (start) (<#elementName>) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Drop shadow"
	dest="<#elementName>.optgroup.objectDecorations"
	></propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="shadowDef"
	value="none | name"
	changeBar="Y"
	>

<pdp>
Apply a drop shadow as defined by an <#xxShadowDef> element. See pages <xnpage
	id="xShadowDef.element.start" /><xphyphen/><xnpage
	id="xShadowDef.element.end" />.
</pdp>

<pdp
	element="Tbl|TblDef"
	>
The shadow is applied to the entire table, excluding the table title.
</pdp>


<propertyDefault>none</propertyDefault>

</property>

<@skipStart>
@------------------------------------------------------------
<property
	name="svgDef"
	value="none | name"
	changeBar="Y"
	>

<pdp>
Apply a SVG background as defined by an <#xxSvgDef> element. See pages <xnpage
	id="xSvgDef.element.start" /><xphyphen/><xnpage
	id="xSvgDef.element.end" />.
</pdp>


<propertyDefault>none</propertyDefault>

</property>
<@skipEnd>
@------------------------------------------------------------
