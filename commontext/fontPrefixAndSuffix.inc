@------------------------------------------------------------
<!-- File = fontPrefixAndSuffix.inc (start) -->
@------------------------------------------------------------
@- Included in FontDef only.
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Prefix and suffix text"
	dest="<#elementName>.propertyGroup.prefixAndSuffix.start"
/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="prefix"
	value="string"
	>

<pdp>
Prefix text.
</pdp>

<pdp element="FontDef" 
	>
This input
</pdp>

<exampleBlock
	 element="FontDef" >
@- The variable below is defined at the beginning of the
@- 'fontdef' file
<#fontPrefixAndSuffixDef>
</exampleBlock>

<exampleBlock
	 element="FontDef" >
This: <Font fontDef="exSB" >AGRA</Font>, and that:
<Font fontDef="exCB" >ORA</Font>
</exampleBlock>

<pdp element="FontDef"
	lineSpacingMode="proportional"
	>
produces:<br/>
<#example_output>
</pdp>


<propertyDefault v="empty" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="suffix"
	value="string"
	>

<pdp>
Suffix text.
</pdp>

<pdp>
See example in the description of the <pn>prefix</pn> property
above.
</pdp>


<propertyDefault v="empty" />
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = fontPrefixAndSuffix.inc (end) -->
@------------------------------------------------------------
