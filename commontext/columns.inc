@---------------------------------------------------------
<@xmacro> columnsDefDescription {
	@if(<#elementName> = ColumnsDef ) {
<#fI>name<#fnI> is a columns format definition name. For
referencing by
the <#xColumns> element's <pn>columnsDef</pn> property (see
pages <xnpage
	id="Columns.element.start" /><xphyphen/><xnpage
	id="Columns.element.end" />).
		}
	@else {		@- 'Columns'
<#fI>name<#fnI> is a columns format definition name, defined
by the <#xColumns> element's <pn>columnsDef</pn> property (see
pages <xnpage
	id="ColumnsDef.element.start" /><xphyphen/><xnpage
	id="ColumnsDef.element.end" />).
		}
}
@---------------------------------------------------------
<property
	name="columnsDef"
	value="name"
	>

<pdp>
<columnsDefDescription/>
</pdp>

</property>

@---------------------------------------------------------
<property
	name="columns"
	value="int"
	>

<pdp>
Number of columns.
</pdp>

<propertyDefault v="2"/>
</property>

@---------------------------------------------------------
<property
	name="columnGap"
	value="dim"
	>
<pdp>
Gap between columns.
</pdp>

<propertyDefault v="10pt"/>
</property>

@---------------------------------------------------------
<property
	name="spaceAbove"
	value="dim"
	>
<pdp>
Space above columns.
<Font textColor="Maroon" >Ignored if new columns starts at top of page</Font>
</pdp>

<propertyDefault v="20pt"/>
</property>

<@skipStart>
@---------------------------------------------------------
<property
	name="spaceBelow"
	value="dim"
	>
<pdp>
Space below columns.
</pdp>

<propertyDefault v="20pt"/>
</property>
<@skipEnd>


@-===========================================================
<propertyGroup
	title="Ruling properties"
	dest="<#elementName>.optgroup.rulings"
	>
<Font textColor="Red" >
Maybe should be in sub-elements: &lt;topRuling&gt; and  &lt;bottomRuling&gt;
<br/>
to enable more properties: indent, width (dim, percent) and so on ...
</Font>
</propertyGroup>

@---------------------------------------------------------
<property
	name="ruleAbove"
	value="name"
	>
<pdp>
Ruling above new columns layout.
<br/>
</pdp>

<propertyDefault v="none"/>
</property>

@---------------------------------------------------------
<property
	name="ruleAboveWhen"
	value="always|notTopOfPage"
	>
<pdp>
Rule for ruling placement.
</pdp>

<propertyDefault v="none"/>
</property>

@---------------------------------------------------------
<property
	name="ruleAboveGap"
	value="dim"
	>
<pdp>
Space between the top of the new columns layout and
the centerline of the ruling specified by
the <pn>ruleAbove</pn>.
</pdp>

<propertyDefault v="5pt"/>
</property>

<@skipStart>
@---------------------------------------------------------
<property
	name="ruleBelow"
	value="name"
	>
<pdp>
Ruling below new columns layout.
</pdp>

<propertyDefault v="none"/>
</property>

@---------------------------------------------------------
<property
	name="ruleBelowGap"
	value="dim"
	>
<pdp>
Space between the bottom of the new columns layout and
the centerline of the ruling specified by
the <pn>ruleBelow</pn> property.
</pdp>

<propertyDefault v="5pt"/>
</property>
<@skipEnd>

@------------------------------------------------------------
