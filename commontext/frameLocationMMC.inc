@------------------------------------------------------------
<!-- File = frameLocationMMC.inc (start) -->
@------------------------------------------------------------
@- Used in:
@-
@- inline\aframe
@- inline\atextframe
@- inline\mathml
@-
@------------------------------------------------------------
<#ifndef> achart		0
<#ifndef> this.aframe.desc	{xxxxxxx}
<#ifndef> this.aframe.code	{xxxxxxx}

<#def> tblframe.pos.options.start	{}
<#def> tblframe.pos.options.end		{}
<#def> aframe.pos.options.start		{}
<#def> aframe.pos.options.end		{}
<#def> arframe.pos.options.start	{}
<#def> arframe.pos.options.end		{}
<#def> tprefix				{}
<#def> so.option			{so}


@--------------------------------------------------------------------------------
@- BEGIN text definition macros
@--------------------------------------------------------------------------------
@- Defined in ${control}/referenceGuide.mmp
@--------------------------------------------------------------------------------
<descriptionTextDef/>
@--------------------------------------------------------------------------------
<@xmacro> inlineExceptionValue {
	@------------------------------------------------------------------------
	@- position="inline" only applies to <AFrame> and <MathML>
	@- NOT <ATextFrame> and <MathML>
	@------------------------------------------------------------------------
	<#def> extraPositionValue	{}
	@if(@match({X<#elementName>X}, {XAFrameX|XMathMLX})) {
		<#def> extraPositionValue	{inline | }
		@- <@write> error {zzzzzzzzzzzzzzzzzzzzzzzzzzz}
		}
}{}
@--------------------------------------------------------------------------------
<inlineExceptionValue/>
@--------------------------------------------------------------------------------
@-	@if({<#elementName>} = {AFrame}) {
@--------------------------------------------------------------------------------
<@xmacro> inlineExceptionKey {
	@------------------------------------------------------------------------
	@- Only applies to AFrame
	@------------------------------------------------------------------------
	<#def> inlineText	{Inline at the insertion point within paragraph text.}
	@if({<#elementName>} = {AFrame}) {
		<#def> inlineText	{Within paragraph text.
When the <on>position</on> property is set to <ov>inline</ov> the
<#xAFrame> element <fI>must</fI> contain an <#xImage> element.
If <pn>position</pn> is set to <pv>inline</pv> the <pn>align</pn> property
is ignored.}
		}
	@if({<#elementName>} = {MathML}) {
		<#def> inlineText	{Within paragraph text.
If <pn>position</pn> is set to <pv>inline</pv> the <pn>align</pn> property
is ignored.}
		}
}{}
@--------------------------------------------------------------------------------
<inlineExceptionKey/>
@--------------------------------------------------------------------------------
@- END text definition macros
@--------------------------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="<#DescriptionText> location"
	longTitle="<#DescriptionText> location"
	XRefID="aframe.propertyGroup.wrapping"
	/>


@------------------------
<property
	@- short="position"
	@- long="position"
	@- value="below | <#extraPositionValue>runin | outsideTextFrame "
	name="position"
	value="inline | below | runin | outsideTextFrame "
	condition="mmcGuide"
	>

<pdp>
Position of <#descriptionText> relative to anchor or insertion point.
The <pn>position</pn> property is applicable only when the <#descriptionText> is
within a <#xP> element.
</pdp>


<propertyValueKeyList
	>

@------------------------------------------------------------
<vk key="inline"><#inlineText>
</vk>

@- <#extraPositionValue>
@- <inlineExceptionKey/>
@- xxx

@------------------------------------------------------------
<vk key="below">Inside text
frame, below current <outputRule
	condition="mmcGuide">paragraph</outputRule>
<outputRule condition="fmcGuide">line</outputRule>.
</vk>

@- <#extraPositionValue>
<inlineExceptionKey/>
@- xxx

@------------------------------------------------------------
<vk key="runin">Run into paragraph.
@------------------------------------------------------------
@- <runinLimitation/> defined in: control\referenceGuide.mmp
@------------------------------------------------------------
<runinLimitation/>
</vk>

@------------------------------------------------------------
<vk key="outsideTextFrame" span="Y" >
Outside of text frame.
</vk>

</propertyValueKeyList>

<pdp>
If the &lt;<#elementName>&gt; element is at the top level, outside a <#xP> element,
the <pv>below</pv>, <pv>inline</pv> and <pv>runin</pv> values are not
applicable.
</pdp>

<pdp>
If <pn>position</pn> is not set to <pv>inline</pv> the
lateral alignment of the <#descriptionText> is specified
using the <pn>align</pn> property (see below).
</pdp>

<pdp>
When <pn>position</pn> is set to <pv>outsideTextFrame</pv> the
lateral position of the <#descriptionText> may be 
further adjusted using the <pn>sideOffset</pn> property (see below).
</pdp>

<pdp>
When <pn>position</pn> is set to <pv>runin</pv> the
top of the <#descriptionText> is aligned with
the top of the text (at caps height) in the 
first line of the paragraph.
When <pn>position</pn> is set to <pv>outsideTextFrame</pv> the
top of the <#descriptionText> is aligned with
the top of the text (at caps height) in the 
first line of the paragraph by default.
This position may be adjusted using the <pn>verticalOffset</pn> property
(see below).
</pdp>

<@skipStart>
<pdp
	element="MathML"
	>
The <pn>position</pn> is overridden by the &lt;math> element <pn>display</pn> property.
See Note <xparanumonly
	id="note.<#elementName>.mathDisplayAttribute" /> on page <xnpage
	id="note.<#elementName>.mathDisplayAttribute" />.
</pdp>
<@skipEnd>

<propertyDefault element="MathML|MathMLDef" >inline</propertyDefault>
<propertyDefault element="AFrame" >below</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="positionOverride"
	long="positionOverride"
	value="none | fromInput"
	element="MathML|MathMLDef"
	>

<pdp>
If <pn>positionOverride</pn> is set to <pv>fromInput</pv> use the setting
specified by the equation's &lt;math&gt; element <pn>display</pn> attribute,
</pdp>

<propertyDefault element="AFrame" >xxxx</propertyDefault>
</property>



@------------------------------------------------------------
<property
	short="align"
	long="align"
	value="start | center | end"
	>

<pdp>
Lateral alignment of the <#descriptionText>.
The <pn>align</pn> property is ignored if <pn>position</pn> is set to <pv>inline</pv>.
</pdp>

<pdp>
Use the <pn>align</pn> property to control lateral
positioning, either within or outside
a text column.
</pdp>


<propertyValueKeyList
	>
@---------------------------------------------------
<vk key="start">
Align left or, if paragraph text is right-to-left,
align right.
@- <ov>left</ov> may be used as an alias for <ov>start</ov>.
</vk>

<vk key="center">
Center the <#descriptionText> between the left and right
text frame borders. Ignored if the <pn>position</pn> is
set to <pv>runin</pv>.
</vk>

<vk key="end">
Align right or, if paragraph text is right-to-left,
align left.
@- <ov>right</ov> may be used as an alias for <ov>end</ov>.
</vk>

@- <vkSubHead>
@- Compatibility values:
@- </vkSubHead>
@- 
@- <vk key="L | Left" >
@- Treated the same as <ov>start</ov>
@- </vk>
@- 
@- <vk key="C | Center" >
@- Treated the same as <ov>center</ov>
@- </vk>
@- 
@- <vk key="R | Right" >
@- Treated the same as <ov>end</ov>
@- </vk>

</propertyValueKeyList>


<propertyDefault>end</propertyDefault>

</property>


<@skipStart>
@---------------------------------------------------
<property
	short="<#tprefix>so"
	long="sideOffset"
	value="dim"
	>
<pdp>
Offset of <#this.aframe.desc> from the alignment position
specified by the <on>align</on> property.
</pdp>

<pdp>
Effective only if the <on>position</on> property is set to <ov>OC</ov> or <ov>OF</ov>. Value may be
positive or negative.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
<@skipEnd>

@---------------------------------------------------
<property
	name="sideOffset"
	value="dim"
	>
<pdp>
Offset of <#this.aframe.desc> from the alignment position
specified by the <on>align</on> property.
</pdp>

<pdp>
Effective only if
the <on>position</on> property is set to <ov>outsideTextFrame</ov>.
</pdp>

<propertyDefault>0</propertyDefault>
</property>


@---------------------------------------------------
<property short="<#tprefix>bo" long="baselineOffset" value="dim"
	condition="fmcGuide"
	>

<pdp>
Offset of <#this.aframe.desc> from text baseline.
</pdp>

<pdp>
Effective only if the <on>P</on> property is 
set to <ov>OC</ov>, <ov>OF</ov> or <ov>I</ov>.
Value may be positive or negative.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@---------------------------------------------------

@---------------------------------------------------
<property short="verticalOffset" long="verticalOffset" value="dim"
	condition="mmcGuide"
	>

<pdp>
Vertical offset of <#this.aframe.desc>.
</pdp>

<pdp>
Effective only if the <on>position</on> property is 
set to <ov>outsideTextFrame</ov>.
Value may be positive or negative.
By default the top of the <#descriptionText> is aligned
with the top of the text (at caps height) in the first
line of the container paragraph.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@---------------------------------------------------

@-----------------------------
<@include> ${CTEXT}/runaroundTextGap.inc
@-----------------------------
@------------------------------------------------------------
<!-- File = frameLocationMMC.inc (end) -->
@------------------------------------------------------------
