@------------------------------------------------------------
@-- p.numbering.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="List label &amp; autonumbering"
	longTitle="List label &amp; autonumbering"
	dest="P.optgroup.autonumbering"
/>
@------------------------------------------------------------



@------------------------------------------------------------
<property short="listLabel" long="listLabel" value="Y | N" >

<pdp>
Enable/disable list label.
If <pn>listLabel</pn> property is set to <pv>N</pv> and the paragraph
has an associated <#xListLabel> element the
contents of the list label are not displayed. In this case any counters
that the list label may contain are not incremented.
<vidx id="paragraph||list label" />
<vidx id="paragraph||numbering" />
<vidx id="paragraph||autonumbering" />
<vidx id="numbering||paragraph" />
<vidx id="autonumbering||paragraph" />
</pdp>

<pdp>
See the <#xListLabel> element on page
<xnpage id="ListLabel.element.start" />
 for information on how to set up list labels
for paragraphs.
</pdp>
<propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------

@------------------------------------------------------------
<property short="numberValue" long="numberValue" value="increment | int" >

<pdp>
If the <#xP> element contains a <#xListLabel> with a <#xcounter>
element, replace the current value of the counter with the
value of the <on>numberValue</on> property when the <on>numberValue</on> property
is set to <fI>int</fI>.
See Example A<xparanumonly
	id="ex.appendix.autonumbering.ex1A_input"
	/> on page <xnpage
	id="ex.appendix.autonumbering.ex1A_input" />.
</pdp>
<propertyDefault>increment</propertyDefault>
</property>

@------------------------------------------------------------
