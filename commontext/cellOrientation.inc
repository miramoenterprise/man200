@------------------------------------------------------------
@-- cellOrientation.inc
@------------------------------------------------------------

<@macro> cellcellOrientationGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Rotation of cell text"
			longTitle="Rotation of cell text"
			dest="<#elementName>.optgroup.Orientation"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Rotation of cell text (default for all cells in row)"
			longTitle="Rotation of cell text (default for all cells in row)"
			dest="<#elementName>.optgroup.Orientation"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Rotation of cell text (default for all cells in table)"
			longTitle="Rotation of cell text (default for all cells in table)"
			dest="<#elementName>.optgroup.cellOrientation"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellcellOrientationGroupHead>
@------------------------------------------------------------

<property
	@- short="Ca"
	long="cellOrientation"
	value="0 | +90 | -90" >

<pdp>
Rotation of text block and cell margins. When the
value of <pn>cellOrientation</pn> is <pv>0</pv> the
cell text is horizontal.
<vidx id="cell||text rotation" />
<vidx id="text rotation||in table cells" />
<vidx id="table cell||rotation" />
<vidx id="table||text rotation" />
</pdp>

<pdp>
See Notes <xparanumonly
	id="note.Cell.rotatedCellHeight" /> and <xparanumonly
	id="note.Cell.cellMarginRotation" />, starting on page <xnpage
	id="note.Cell.rotatedCellHeight" />.
</pdp>


@- <pdp>
@- If the <pn>cellOrientation</pn> property is set to <pv>90</pn> or <pv>-90</pn>,
@- the vertical text block is aligned horzontally.
@- </pdp>

<propertyDefault>0</propertyDefault>


</property>
@------------------------------------------------------------

