@------------------------------------------------------------
@- objectTitleSubElement.inc
@-
@- Included in:
@-
@-	mathml, Equation
@-
@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart/>
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> contentFlag {
	<#def> contentFlag		Y
	<#def> showDescriptionFlag	Y
	<#def> OTsubElementDesc		{Include a caption for}
	@if(@match({X<#elementName>X}, {XMathMLDefX|XEquationDefX})) {
		<#def> contentFlag		N
		<#def> showDescriptionFlag	N
		<#def> OTsubElementDesc		{Define caption properties for}
		}
	<@write> error {<#elementName> contentFlag: <#contentFlag>}
}{}
@------------------------------------------------------------
<contentFlag/>
@------------------------------------------------------------

<@skipStart>
<Row>
<Cell>
<P>Hello</P>
</Cell>
</Row>
<@skipEnd>
@------------------------------------------------------------
<subElement
	name="ObjectTitle"
	dest="<#elementName>.subelement.ObjectTitle.start"
	content="<#contentFlag>"
	@- content="Y"
	>
<subElementProperties>
@- <fI>properties</fI>
<subProperty name="properties" value= "" desc="See &lt;ObjectTitle&gt; element" />
</subElementProperties>


<bbp>
<#OTsubElementDesc> the <#descriptionText>.
See the <#xObjectTitle> element on pages <xnpage
        id="ObjectTitle.element.start" /><xphyphen/><xnpage
        id="ObjectTitle.element.end" /> for <fI>properties</fI>.
Not applicable when <pn>position</pn> is set
to <pv>inline</pv> (see page <xnpage
	id="<#elementName>.property.position" />).
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="description"
	dest="<#elementName>.subelement.description.start"
	content="Y"
	elementExclude="MathMLDef"
	show="<#showDescriptionFlag>"
	>
<subElementProperties>
<subProperty name="language" value= "key" desc="Description language" />
</subElementProperties>

<bbp>
Include a description of the <#descriptionText>. This is
for a read out loud application. The description text is not included in
the displayed output.
</bbp>

</subElement>

@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------
