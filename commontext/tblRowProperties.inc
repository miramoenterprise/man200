@------------------------------------------------------------
<!-- File = tblRowProperties.inc  (Start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Row properties"
	dest="optgroup.tblRowSplitting.start"
	/>

@------------------------------------------------------------
<property
	@- short="rshade"
	name="rowShading"
	value="N | Y "
	>

<pdp>
Enable row shading as specified by the <#xrowShading> sub-element.
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------
<property
	@- short="sr"
	name="splitRows"
	value="N | Y "
	>

<pdp>
Allow within-row splitting between pages and columns.
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property
	@- short="ssr"
	name="splitStraddledRows"
	value="N | Y "
	>

<pdp>
Allow rows which are vertically straddled to be split between pages and columns.
No within-row splitting occurs unless the <pn>splitRow</pn> property
is set to <pv>Y</pv>.
</pdp>

<pdp>
Applicable only when the <#xRow> contains a <#xCell> element with
a <pn>rowSpan</pn> property set to a value greater
than <pv>0</pv> (see page <xnpage
       id="Cell.property.rowSpan" />).
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = tblRowSplitting.inc  (End) -->
@------------------------------------------------------------
