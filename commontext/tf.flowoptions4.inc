@------------------------------------------------------------
@-- tf.flowoptions4.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Baseline synchronization"
	longTitle="Baseline synchronization"
	dest="ATextFrame.optgroup.baseline"
	condition="fmcGuide"
/>

@------------------------------------------------------------
<property short="b" long="alignBaselines" value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Baseline synchronization of lines across multiple
sub-columns. If b<#nbsp>=<#nbsp><ov>Y</ov>,
the baselines of text in adjacent
columns are synchronized.
The <on>b</on> option is effective only if the
<on>cn</on> option has a value of <ov>2</ov> or
greater.
<vidx id="baseline synchronization" />
<vidx id="text frames||baseline synchronization in" />
</pdp>

</property>

@------------------------------------------------------------
<property short="bl" long="baselineSpace" value="dim"
	condition="fmcGuide"
	>

<pdp>
Line spacing for synchronized baselines.
</pdp>
</property>

@------------------------------------------------------------
<property short="bh" long="maxCharHeight" value="dim"
	condition="fmcGuide"
	>

<pdp>
Maximum character height for synchronization of the
first line in the text column; if characters exceed
this height, the first line is not synchronized
</pdp>
</property>
@------------------------------------------------------------
