@------------------------------------------------------------
@-- cellMargins.inc
@------------------------------------------------------------
<!-- cellMargins.inc	Start -->

<#ifndef> tbl_cm1	{s}
<#ifndef> tbl_cm2	{, for all cells in table}
<#ifndef> tbl_cm3	{, or specified using the 
corresponding <#xTbl> element property}

@- <@write> error {<#elementName>	cellMargins.inc}
@------------------------------------------------------------
<@macro> cellMarginDefs {
	<#def> pdTextAll	{All cell margins, i.e. start, top, end and bottom.}
	<#def> pdTextStart	{Cell start margin}
	<#def> pdTextTop	{Cell top margin}
	<#def> pdTextEnd	{Cell end margin}
	<#def> pdTextBottom	{Cell bottom margin}
	@if(<#elementName> = TblDef ) {
		<#def> pdTextAll	{<#pdTextAll>}
		<#def> pdTextStart	{<#pdTextStart>}
		<#def> pdTextTop	{<#pdTextTop>}
		<#def> pdTextEnd	{<#pdTextEnd>}
		<#def> pdTextBottom	{<#pdTextBottom>}
		<propertyGroup
			title="Cell margins (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_margins"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<#def> pdTextAll	{<#pdTextAll>s. Override the table format margin values
for all cells in table.}
		<#def> pdTextStart	{<#pdTextStart>s. Override start margin values for all cells in table.}
		<#def> pdTextTop	{<#pdTextTop>s. Override top margin values for all cells in table.}
		<#def> pdTextEnd	{<#pdTextEnd>s. Override end margin values for all cells in table.}
		<#def> pdTextBottom	{<#pdTextBottom>s. Override bottom margin values for all cells in table.}
		<propertyGroup
			title="Cell margins (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_margins"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<#def> pdTextAll	{<#pdTextAll>s. Override all margin values for all cells in row.}
		<#def> pdTextStart	{<#pdTextStart>s. Override start margin values for all cells in row.}
		<#def> pdTextTop	{<#pdTextTop>s. Override top margin values for all cells in row.}
		<#def> pdTextEnd	{<#pdTextEnd>s. Override end margin values for all cells in row.}
		<#def> pdTextBottom	{<#pdTextBottom>s. Override bottom margin values for all cells in row.}
		<propertyGroup
			title="Cell margins (default for all cells in row)"
			dest="<#elementName>.propertyGroup.cell_margins"
			>
			</propertyGroup>
		}
	@if(<#elementName> = Cell ) {
		<#def> pdTextAll	{<#pdTextAll>. Override all margin values for cell.}
		<#def> pdTextStart	{<#pdTextStart>. Override cell start margin.}
		<#def> pdTextTop	{<#pdTextTop>. Override cell top margin.}
		<#def> pdTextEnd	{<#pdTextEnd>. Override cell end margin.}
		<#def> pdTextBottom	{<#pdTextBottom>. Override cell bottom margin.}
		<propertyGroup
			title="Cell margins"
			dest="<#elementName>.propertyGroup.cell_margins"
			>
			</propertyGroup>
		}
}
@------------------------------------------------------------
<|cellMarginDefs>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="allMargins"
	value="dim"
	>

<pdp>
<#pdTextAll>
<vidx id="cell||margins" />
<vidx id="table||cell margins" />
</pdp>

</property>

@------------------------------------------------------------
<property
	name="startMargin"
	value="dim"
	>

<pdp>
<#pdTextStart>
<vidx id="cell||margins" />
<vidx id="table||cell margins" />
</pdp>

</property>

@------------------------------------------------------------
<property
	name="topMargin"
	value="dim"
	>
<pdp>
<#pdTextTop>
</pdp>

</property>

@------------------------------------------------------------
<property
	name="endMargin"
	value="dim"
	>
<pdp>
<#pdTextEnd>
</pdp>

</property>

@------------------------------------------------------------
<property
	name="bottomMargin"
	value="dim"
	>
<pdp>
<#pdTextBottom>
</pdp>

</property>
@------------------------------------------------------------
<!-- cellMargins.inc	End -->
@------------------------------------------------------------
