@------------------------------------------------------------
<!-- file = tblcontinuationdef.inc (start)  -->
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> tcdText {
	<#def> tcd.docText		{}
	@if(<#elementName> = DocDef) {
		<#def> tcd.docText		{as default for all tables in document}
		}
	@if(<#elementName> = TblDef) {
		<#def> tcd.docText		{as default for all tables with this format}
		}
	@if(<#elementName> = Tbl) {
		<#def> tcd.docText		{with this table}
		}
}{}
@------------------------------------------------------------
<tcdText/>
@------------------------------------------------------------

@- In <DocDef>, after 'Footnote properties'
@- In <Tbl>, after 'fNoteDef'

@------------------------------------------------------------
<propertyGroup
	title="Table continuation format"
	dest="<#elementName>.optgroup.tableContinuationDef"
	></propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="tblContinuationDef"
	value="name"
	>

<pdp>
<fI>name</fI> is the name of a <#xTblContinuationDef> format
definition to use <#tcd.docText>.
</pdp>

<pdp>
See the <#xTblContinuationDef> element
on pages <xnpage
	id="TblContinuationDef.element.start" /><xphyphen/><xnpage
	id="TblContinuationDef.element.end" />.
</pdp>

<propertyDefault>default</propertyDefault>
</property>
@------------------------------------------------------------
