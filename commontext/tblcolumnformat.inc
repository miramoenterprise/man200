@------------------------------------------------
<!-- File = tblcolumnformat.inc (Start) in $ctext -->
@------------------------------------------------
<@macro> tblcolumnformatDefs {
	<#def> tcf.parent	{<#xTblColumnDef>}
	@if(<#elementName> = TblColumn ) {
		<#def> tcf.parent	{<#xTblColumn>}
		}
}
@------------------------------------------------------------
<|tblcolumnformatDefs>
@------------------------------------------------------------

@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	fontProperties="N"
	@- dest="<#elementName>.subelement.TblColumnFormat.start"
	>
<br/>
The <#xTblColumnFormat> sub-element is used
to specify the default paragraph format, fill properties and
end ruling property in
header, body and footer rows for a table column.
</subElementsStart>
@------------------------------------------------------------
@-- RD rd tmp hack
<Row height="0" />
@------------------------------------------------------------

@------------------------------------------------------------
@- <groupTitles ../>
@------------------------------------------------------------
<subElement
	name="TblColumnFormat"
	content="N"
	dest="TblColumnDef.subelement.TblColumnFormat"
	>
<subElementProperties>
@- rowType = "header | body | footer"
<subProperty name="rowType" value= "header | body | footer" desc="yyyy" />
<br/>
@- paraDef="<fI>name</fI>"
<subProperty name="paraDef" value= "name" desc="yyyy" />
@- textAlign = "start | center | end | justify"
<subProperty name="textAlign" value= "start | center | end | justify" desc="yyyy" />
@- textColor="<fI>name</fI>"
<subProperty name="textColor" value= "name" desc="yyyy" />
<br/>
@- fillColor="<fI>name</fI>"
<subProperty name="fillColor" value= "name" desc="yyyy" />
<br/>
@- fillTint="<fI>percent</fI>"
<subProperty name="fillTint" value= "percent" desc="yyyy" />
@- fillOpacity="<fI>percent</fI>"
<subProperty name="fillOpacity" value= "percent" desc="yyyy" />
<br/>
@- endRule="<fI>name</fI>"
<subProperty name="endRule" value= "name" desc="yyyy" />
</subElementProperties>

<bbp>
Up to three <#xTblColumnFormat> sub-elements may be included within
a <#tcf.parent> element, one each for header, body or footer rows.
If no <pn>rowType</pn> property
is specified, the property settings for the column are applied
to all header, body and footer rows.
@- <br/>
@- USPTO
</bbp>

<bbp>
The default values for all properties other
than <pn>rowType</pn> is <pv>inherit</pv>.
</bbp>


</subElement>

@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------
