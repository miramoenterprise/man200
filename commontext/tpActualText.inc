@------------------------------------------------------------
@-- tpActualText.inc
@------------------------------------------------------------
@- <Font> and <Image> elements
<!-- Start @tpActualText attribute -->
@------------------------------------------------------------





@------------------------------------------------------------
<property
	name="tpActualText"
	value="text"
	>

<pdp>
Specify actual Text. For read out loud applications.
<sq>Actual Text</sq> is also referred to as <sq>Replacement Text</sq>.
<vidx id="tagged PDF||actual text" />
<vidx id="tagged PDF||replacement text" />
</pdp>

<@skipStart>
Discussion in ISO 32000-1:
Section 14.9.3 for Alternate Descriptions ("Alt Text")
Section 14.9.4 Replacement Text ("Actual Text")
Section 14.9.5 Expansion of Abbreviations and Acronyms

For an ISO approved copy of the ISO 32000-1 Standards document:
http://www.adobe.com/devnet/acrobat/pdfs/PDF32000_2008.pdf
<@skipEnd>

<pdp>
For example, in the case one or more text characters are displayed
in ornamentalized form as an inline image and these characters
form part of the text, then these characters should be assigned
as the value of the <pn>tpActualText</pn> property.
</pdp>

<pdp>
For example, to insure that a series of contiguous digits
are spoken as a series of digits, not as a numeric value.
</pdp>

<@skipStart>
Abbreviations
<@skipEnd>


<@skipStart>
http://www.renderx.com/reference.html#Actual_Text
The syntax of the rx:actual-text attribute is shown in the example below:

<fo:inline rx:actual-text="5,O,8">508</fo:inline>

The following shows two ways of formatting some numbers that appear exactly the same in context of the PDF. If you allow a screen reader to read them out loud, you will see the difference:

    No actual text: 12345678910

    With actual text: 1,2,3,4,5,6,7,8,9,10.

Note:

The example with no actual text will attempt to read as a number. You would hear twelve billion, three hundred forty five million, six hundred seventy eight thousand nine hundred ten. The example with actual text is formatted with rx:actual-text = "1,2,3,4,5,6,7,8,9,10." and would be read exactly like it is intended ... one, two, three, ... Actual text is used throughout this document within content that is being read to clarify how it should be read, like for the terms Section 508 and RenderX.

<@skipEnd>

<pdp>
The <pn>tpActualText</pn> property is ignored unless
the <pn>pdfType</pn> <#xMiramoXML> property is set
to <pv>UA&#x2011;1</pv> or the <pn>-pdfType</pn> command line
option is set to <pv>UA&#x2011;1</pv>. See page <xnpage
	id="commandline.option.-pdfType" /> and page <xnpage 
	id="MiramoXML.property.pdfType" />.
</pdp>


<pdp>
The default is empty, i.e. no actual text is specified.
</pdp>

</property>
@------------------------------------------------------------
<!-- End @tpActualText attribute -->
@------------------------------------------------------------
