@------------------------------------------------------------
<!-- File = fontPrimaryProperties.inc (start) -->
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> textSizeValue {
	<#def> textSizeValue	{dim}
	@if(<#elementName> = {Font}) {
		<#def> textSizeValue	{dim | rdim}
		}
}{}
@------------------------------------------------------------
<textSizeValue/>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Primary font properties"
	dest="<#elementName>.propertyGroup.font_selection"
>
See <xchapter id="fonts"
		/> for
details about supported font types and font selection.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="fontFamily"
	value="name"
	>

<pdp>
Font family. E.g. <ov>Arial</ov>, <ov>Times New Roman</ov>.
</pdp>

<pdp
	element="Equation|EquationDef"
	>
The <pn>fontFamily</pn> property is ignored if
the <pn>type</pn> property is set to <pv>latex</pv> (see pages <xnpage
	id="Equation.property.type" /> and <xnpage
	id="EquationDef.property.type" />).
See Note <xparanumonly
	id="note.EquationDef.mathjax1" /> on page <xnpage
	id="note.EquationDef.mathjax1" />.
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="fontAngle"
	long="fontAngle"
	value="name"
	elementExclude="MathML|MathMLDef|Equation|EquationDef"
	>

<pdp>
Font angle. E.g. <ov>Italic</ov>, <ov>Oblique</ov>.
</pdp>

@------------------------------
<referToFontElement/>
@------------------------------

@- <pdp element="P|TextLine|ALine" >
@- See the description of <on><#propertyName></on> in the <#xFont> element
@- on page <xnpage id="Font.property.<#property.long>" />.
@- </pdp>

<pdp
	element="Font"
	>
E.g. the following
</pdp>

<exampleBlock
	element="Font"
	>
Some <Font fontAngle="Italic" >italic</Font> text ...
</exampleBlock>


<pdp
	element="Font"
	>
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------



@------------------------------------------------------------
<property
	name="fontWeight"
	value="name" rs="2"
	elementExclude="MathML|MathMLDef|Equation|EquationDef"
	>

<pdp>
Font weight.  E.g. <ov>Bold</ov>, <ov>Medium</ov>.
</pdp>


@------------------------------
<referToFontElement/>
@------------------------------

<exampleBlock
	element="Font"
	>
Some <Font fontWeight="Bold" >bold</Font> text ...
</exampleBlock>

<pdp
	element="Font"
	>
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="textSize"
	value="<#textSizeValue>"
	>

<pdp>
Text size.
</pdp>

<pdp>
Default units are points (1/72 inch).
</pdp>



<pdp
	element="Font"
	>
<fI>rdim</fI> values are applicable only when
the <on>textSize</on> property is included directly
within a <#xFont> element, i.e. not within <#xP> or
other elements that support <#xFont> properties.
An <fI>rdim</fI> value may
have any of the following forms: +20%, 120%, 85%,
-4%, +<fI>dim</fI> and -<fI>dim</fI> (e.g. +2pt, -1mm). 
</pdp>

@------------------------------------------------------------
@- Macro <relativeTextPointSize/> is defined in:
@- ${CTEXT}/mmp.textdefs
@------------------------------------------------------------
@- <relativeTextPointSize/>
@------------------------------------------------------------


<pdp
	element="Font"
	>
E.g. the following
</pdp>

<exampleBlock element="Font"
	lineNumbers="N"
	tabs="5|10|15|20"
	>
The next text <Font textSize="10pt" >is in 10 points.</Font>
This text is back to the default paragraph text size.
</exampleBlock>


<pdp
	element="Font"
	elementExclude="MathML|MathMLDef|Equation|EquationDef"
	>
produces:<br/>
<#example_output>
</pdp>

</property>
@------------------------------------------------------------



@------------------------------------------------------------
<property
	name="textColor"
	value="name"
	>

<pdp>
Text color.
</pdp>

<pdp
	element="Font"
	>
E.g. the following
</pdp>

<exampleBlock
	element="Font" 
	>
Today is a <Font textColor="Red" >Red</Font> letter
day ...
</exampleBlock>

<pdp
	element="Font"
	>
produces:
<#example_output>
</pdp>

</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="textOpacity"
	value="100 | 0"
	elementExclude="MathML|MathMLDef|Equation|EquationDef"
	>

<pdp>
Text opacity.
</pdp>

@-----------------------------------
@- In: ${control}/referenceGuide.mmp
@-----------------------------------
<referToFontElement/>
@-----------------------------------

<pdp
	element="Font"
	>
If <on>textOpacity</on> is set to <ov>0</ov> the text is
included invisibly in the PDF output, and may be copied and searched.
</pdp>

<pdp
	element="Font"
	>
E.g. the following
</pdp>

<exampleBlock
	element="Font" 
	>
This is <Font textOpacity="0" >transparent</Font> text
</exampleBlock>

<pdp
	element="Font"
	>
produces:
<#example_output>
</pdp>

<propertyDefault>100</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	element="FontDef|ParaDef"
	name="compositeFontDef"
	value="none | name" rs="2"
	>

<pdp>
Apply different properties depending of the value of
the <pn>language</pn> property.
</pdp>

<pdp>
See the <#xCompositeFontDef> element on pages <xnpage
        id="CompositeFontDef.element.start" /><xphyphen/><xnpage
        id="CompositeFontDef.element.end" />.
</pdp>


<propertyDefault>none</propertyDefault>
</property>


@------------------------------------------------------------
<@include>	${CTEXT}/language.inc
@------------------------------------------------------------

@------------------------------------------------------------
<!-- File = fontPrimaryProperties.inc (end) -->
@------------------------------------------------------------

