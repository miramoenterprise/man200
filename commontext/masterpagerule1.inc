@------------------------------------------------------------
@-- masterpagerule1.inc
@------------------------------------------------------------

@------------------------------------------------------------
@-- masterpagerule2.inc
@------------------------------------------------------------
<@xmacro> masterPageRule1 {
	<#def> mpr2.new		{new }
	@if(<#elementName> = {DocDef}) {
		<#def> mpr2.new		{}
		}
}{}
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Master page rule format name"
	longTitle="Master page rule format name "
	dest="<#elementName>.propertyGroup.formatname"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="masterPageRuleDef"
	long="masterPageRuleDef"
	value="string"
	>
<pdp>
<fI>Required</fI>
<br/>
Name of Master page rule format definition.
</pdp>

</property>

