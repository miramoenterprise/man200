@------------------------------------------------------------
@-- tf.flowoptions2.inc
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Feathering and side-heads"
	longTitle="Feathering and side-head settings"
	dest="<#elementName>.optgroup.feathering_and_sideheads.start"
	condition="fmcGuide"
	></propertyGroup>

@------------------------------------------------------------
<property short="sh"
	long="sideheads"
	@- skipelementName="NextTextFrameDef"
	value="N | Y"
	condition="fmcGuide"
	>

<pdp>
Allow room for side-heads in this text flow.
The side-head width, gap and
location are specified using the <on>sg</on>,
<on>sw</on> and <on>sp</on> options above.
<vidx id="text frames||side-heads in" />
</pdp>

<pdp>
For details of how to specify paragraphs
as side-head or run-in
heads, see the <#xP> code <on>Pf</on> option
(described on page
<xnpage id="P.property.Pf" file="inline" />
).

@- <xsideheads2 xref="sideheads" />

</pdp>

<propertyDefault>N</propertyDefault>
</property>


@------------------------------------------------------------
<property short="sw" long="sideheadWidth" value="dim"
	condition="fmcGuide"
	>

<pdp>
Side-head column width.
Effective only when room for side-heads is
switched on
@- (<#sh.text3><#sideheads1>).
</pdp>

</property>

@------------------------------------------------------------
<property short="sg" long="sideheadGap" value="dim"
	condition="fmcGuide"
	>

<pdp>
Gap between side-head column and main text 
column(s) within text frame.
Effective only when room for side-heads is
switched on
@- (<#sh.text3><#sideheads1>).
</pdp>

</property>

@------------------------------------------------------------
<property short="sp" long="sideheadPosition" value="L | R | I | O"
	condition="fmcGuide"
	>

<pdp>
Specify side-head placement within text frame. 
L="left" side, R<#nbsp>="<#nbsp>"Right side, I<#nbsp>=<#nbsp>"Inside
and O<#nbsp>=<#nbsp>"Outside of text frame". Outside refers to the side nearest
the page edge in the case of double-sided documents.
Effective only when room for side-heads is
switched on
@- (<#sh.text3><#sideheads1>).
</pdp>

</property>
