@------------------------------------------------------------
@- tblRowShading.inc
<!-- File: tblRowShading.inc start START -->
@------------------------------------------------------------
<subElement
	name="rowShading"
	@- dest="<#elementName>.subelement.rowShading.start"
	dest2="rowShading.element.start"
	fontProperties="N"
	>
<subElementProperties>
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="type" value= "body | header | footer" desc="Margin at start of paragraph body" /> ]
<br/>
[ <subProperty name="shadingPrecedence" value= "row | column" desc="Margin at the end side of paragraph body" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="shadingInterval" value= "int" desc="Margin at start of paragraph body" /> ]
<br/>
[ <subProperty name="shadingCount" value= "int" desc="Margin at the end side of paragraph body" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="fillColor" value= "name" desc="Background fill color" /> ]
<br/>
[ <subProperty name="fillTint" value= "percent" desc="Background fill tint" /> ]
@--------------------------------------------------------------------------------
<br/>
[ <subProperty name="fillOpacity" value= "percent" desc="Background fill opacity" /> ]
<br/>
[ <subProperty name="svgDef" value= "name" desc="Background SVG style" /> ]

@--------------------------------------------------------------------------------
</subElementProperties>
@--------------------------------------------------------------------------------

@------------------------------------------------------------
<bbp>
Up to three <#xrowShading> sub-elements may be used to specify the
row shading for table body, header and footer sections, as per the
value of the <pn>type</pn> property. If the <pn>type</pn> property
is omitted, the default is <pv>body</pv>.
<#xrowShading> sub-elements are ignored unless
the <pn>rowShading</pn> property is set to <pv>Y</pv>.
</bbp>
<bbp>
The default values of <pn>shadingInterval</pn> and <pn>shadingCount</pn> result
in alternate shaded rows, with the first row in a section being unshaded.
</bbp>

@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="shadingPrecedence" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
If <pn>shadingPrecedence</pn> is set to <pv>row</pv> row shading overrides column shading.
If <pn>shadingPrecedence</pn> is set to <pv>column</pv> column shading overwrites row shading.
Column shading is specified using the <#xTblColumn> <#xTblColumnFormat> sub-element (see
page <xnpage
	id="TblColumn.subelement.TblColumnFormat" />).
The default value is <pv>row</pv>.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="shadingInterval" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Number of of unshaded rows, starting at the first row in the applicable section,
as specified by the <pn>type</pn> property.
The value of <pn>shadingInterval</pn> must be great than or equal to <pv>1</pv>.
The default value is <pv>1</pv>.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="shadingCount" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Number of of shaded rows, starting at the first row after
the <pn>shadingInterval</pn> in the applicable section,
as specified by the <pn>type</pn> property.
The value of <pn>shadingCount</pn> must be great than or equal to <pv>1</pv>.
The default value is <pv>1</pv>.
</bbp>
@------------------------------------------------------------

@------------------------------------------------------------
<bbp labelWidth="60mm" labelText="fillColor, fillTint, fillOpacity, svgDef" spaceBelow="1.4pt" wn="Y" >
</bbp>
<bbp labelWidth="09mm" labelText="" spaceAbove="0" >
Specify the background fill color, tint and opacity for the paragraph, or
an <pn>svgDef</pn> name. If the <pn>svgDef</pn> property is specified,
the <pn>fillColor</pn>, <pn>fillTint</pn> and <pn>fillOpacity</pn> are
ignored.
</bbp>
@------------------------------------------------------------


@--------------------------------------------------------------------------------
</subElement>
@--------------------------------------------------------------------------------
<!-- File: tblRowShading.inc end END -->
