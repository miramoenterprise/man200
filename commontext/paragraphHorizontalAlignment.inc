@------------------------------------------------------------
<!-- file = paragraphHorizontalAlignment.inc (start) -->
<!-- Used in <P> and <ParaDef> elements	             -->
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Horizontal alignment"
	longTitle="Horizontal alignment"
	dest="cell.optgroup.horizontalAlignment"
	></propertyGroup>

@------------------------------------------------------------
<property
	short="textAlign"
	long="textAlign"
	value="start | end | center | justify"
	>

<pdp>
Text alignment and justification.
<vidx id="paragraph||justification in" />
<vidx id="justification||in paragraphs" />
<vidx id="text alignment||paragraph" />
<vidx id="alignment||paragraph" />
<vidx id="paragraph||centering text (A=C)" />
<vidx id="paragraph||alignment" />
<vidx id="centering text||in paragraphs (A=C)" />
</pdp>

<propertyValueKeyList keyWidth="11mm"
	keyHeading="Value"
	descriptionHeading="Description"
>

@- <vk key="start | left" >
<vk key="start" >
Align text at the beginning of lines.
Ragged line ends.
@- <ov>left</ov> may be used as an alias for <ov>start</ov>.
@- In the case of right-to-left
@- writing <ov>left</ov> and <ov>start</ov> produce
@- right-aligned text lines.
</vk>

@- <vk key="end | right" >
<vk key="end" >
Align text at line ends.
Ragged line starts.
@- <ov>right</ov> may be used as an alias for <ov>end</ov>.
@- In the case of right-to-left
@- writing <ov>right</ov> and <ov>end</ov> produce
@- left-aligned text lines.
</vk>

<vk key="center" >
Align text lines at the center.
Ragged line starts and ends.
</vk>

<vk key="justify" >
Align text lines at start and end.
Text justification at both ends.
@- <ov>both</ov> may be used as an alias for <ov>justify</ov>.
</vk>

</propertyValueKeyList>

</property>

@------------------------------------------------------------
<property
	short="firstLineIndent"
	long="firstLineIndent"
	value="dim"
	>
@- <propertyOld short="A" long="justify" value="L | R | C | LR" />

<pdp>
First line indent.
<vidx id="paragraph||first indent (firstLineIndent)" />
<vidx id="paragraph||indentation" />
</pdp>

<pdp>
Indent of the first line of paragraph text from the start side of
a text column or from the margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="startIndent"
	long="startIndent"
	value="dim"
	>

<pdp>
Start indent (excluding the first line
of the paragraph).
<vidx id="paragraph||start indent (startIndent)" />
</pdp>

<pdp>
Distance of the
second and succeeding lines of text from
the start side of a text column or from the start
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="endIndent"
	long="endIndent"
	value="dim"
	>

<pdp>
End indent.
<vidx id="paragraph||end indent (endIndent)" />
</pdp>

<pdp>
Distance of lines of text from
the end side of a text column or from the end
margin of a table cell.
</pdp>

</property>

@------------------------------------------------------------
<@include>	${ctext}/indentMode.inc
@------------------------------------------------------------

@------------------------------------------------------------
<!-- file = paragraphHorizontalAlignment.inc (end) -->
@------------------------------------------------------------
