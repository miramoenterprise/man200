@------------------------------------------------------------
@-- cellRulings.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@macro> cellRulingsGroupHead {
	@if(<#elementName> = Cell ) {
		<propertyGroup
			shortTitle="Cell border rulings"
			longTitle="Cell border rulings"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
	@if(<#elementName> = Row ) {
		<propertyGroup
			shortTitle="Cell border rulings (default for all cells in row)"
			longTitle="Cell border rulings (default for all cells in row)"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
	@if(<#elementName> = Tbl ) {
		<propertyGroup
			shortTitle="Cell border rulings (default for all cells in table)"
			longTitle="Cell border rulings (default for all cells in table)"
			dest="<#elementName>.propertyGroup.cell_rulings"
			></propertyGroup>
		}
}
@------------------------------------------------------------
<|cellRulingsGroupHead>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	@- short="lr"
	long="startRule"
	value="name"
	>

<pdp>
Start ruling.
<vidx id="ruling||in table cells" />
</pdp>

<pdp>
<fI>name</fI> is the name of a ruling format
in a referenced template, or defined
@- using the <#xRuleDef> format definition element (see pages <xnpage
@- 	id="RuleDef.element.start" /><xphyphen/><xnpage
@- 	id="RuleDef.element.end"  />).
using the <#xRuleDef> format definition element (see page <xnpage
	id="RuleDef.element.start" />).
</pdp>


<propertyDefault>none</propertyDefault>
</property>

@------------------------------------------------------------
<property
	@- short="tr"
	long="topRule"
	value="name"
	>

<pdp>
Top ruling. See <on>startRule</on> above.
</pdp>
</property>


@------------------------------------------------------------
<property
	@- short="rr"
	long="endRule"
	value="name"
	>

<pdp>
End ruling. See <on>startRule</on> above.
</pdp>
</property>

@------------------------------------------------------------
<property
	@- short="br"
	long="bottomRule"
	value="name"
	>
<pdp>
Bottom ruling. See <on>startRule</on> above. 
</pdp>
</property>
@------------------------------------------------------------

