@------------------------------------------------------------
@-  masterpagerule4.inc
@- 
@-  	pageCount
@-  
@-  Included in:
@-  
@-  	DocDef
@-  	MasterPageRuleDef
@-  	MasterPageRule
@-  
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Document section page rounding"
	longTitle="Document section page rounding"
	dest="<#elementName>.propertyGroup.pageCount"
>
</propertyGroup>


@------------------------------------------------------------
<property short="pageCount" long="pageCount"
	value="any | even | odd"
	>
<pdp>
If set to <pv>even</pv> or <pv>odd</pv> force the number of
pages in the document section to an even or odd number.
</pdp>
<masterPageDefault value="any" />
</property>


@------------------------------------------------------------
<@include> ${CTEXT}/mpr.pageNumbering.inc
@------------------------------------------------------------

@------------------------------------------------------------
<@include> ${CTEXT}/chapter.paragraphNumbering.inc
@------------------------------------------------------------

@------------------------------------------------------------
<property
	long="allowLineBreaksAfter"
	value="chars"
	>
<pdp>
OK to break lines at these characters.
FIX THIS
</pdp>

@- <propertyDefault>"/ - \xd0 &nbsp;\xd1 " (slash, minus, endash, emdash)</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="language"
	value="xx"
	>
<pdp>
FIX THIS
</pdp>

@- <propertyDefault>"/ - \xd0 &nbsp;\xd1 " (slash, minus, endash, emdash)</propertyDefault>
</property>

@------------------------------------------------------------
<property
	long="hyphenate"
	value="xx"
	>
<pdp>
FIX THIS
</pdp>
@- <propertyDefault>"/ - \xd0 &nbsp;\xd1 " (slash, minus, endash, emdash)</propertyDefault>
</property>


@------------------------------------------------------------
<property
	long="footNote properties"
	value="xx"
	>
<pdp>
FIX THIS
<br/>
FNoteAnchor | faP [ aFNoteAnchorPositionId ]
<br/>
FNoteNumPos | fnP [ aFNoteNumPosId ]
<br/>
FNoteAnchorPrefix | fap [ aFNoteAnchorPrefixId ]
</pdp>
@- <propertyDefault>"/ - \xd0 &nbsp;\xd1 " (slash, minus, endash, emdash)</propertyDefault>
</property>
