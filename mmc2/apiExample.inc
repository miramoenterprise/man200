@------------------------------------------------------------
@- apiExample.inc
@------------------------------------------------------------

<exampleBlock
	tabs="3|6|9|12|15|18|21|24"
	codeType="mmpp"
	codeBlockWidth="300mm"
	leftIndent="8mm"
	lineNumbers="Y"
	lineNumberStyle="N"
	>
<exampleCaption
	dest="apiExample"
	>Sample Java API
</exampleCaption>
package com.miramo.api.sample;

import com.miramo.api.JobProcessor;
import com.miramo.api.JobResult;
import com.miramo.api.exceptions.MiramoException;

import java.io.IOException;

public class Main
{
	public static void main(String[] args)
	{
		try
		{
			if (args.length < 1)
			{
				System.err.println("specify the Miramo installation directory");
			}
			else
			{
				String installationDirectory = args[0];

				// <xfI>These files (including test/sample.mfd) are included with this sample in the root folder</xfI>
				String inputXML = "test/sample.xml";
				String outputPDF = "test/sample.pdf";

				// <xfI>Create a job processor using the installation directory passed to this program</xfI>
				JobProcessor processor = JobProcessor.newProcessor(<xfI>installDir</xfI>);
				processor.setTemplateFile("test/sample.mfd");

				// <xfI>Configure the job processor so that PDFs generated have showProperties enabled</xfI>
				processor.setOption("showProperties", "Y");

				// <xfI>Create the PDF, this might take some time depending on the size of the input XML and template file</xfI>
				JobResult result = processor.createPdf(inputXML, outputPDF);

				// <xfI>As soon as createPdf returns, the job is complete</xfI>
				if (result.isSuccessful())
				{
					System.out.println("Job successful!");
					System.out.println("Start date &amp; time   : " + result.getStartTime());
					System.out.println("End date &amp; time : " + result.getStartTime());
					System.out.println("Full command : " + result.getFullCommand());
					System.out.println("Input file : " + result.getInputFile());
					System.out.println("Input file size : " + result.getInputFileSize());
					System.out.println("Output file : " + result.getFullCommand());
					System.out.println("Output file size : " + result.getOutputFileSize());
					System.out.println("PDF page count : " + result.getPdfPageCount());
					System.out.println("Processing time : " + result.getProcessingTime());
				}
				else
				{
					System.err.println("Failed with exit code " + result.getExitCode());
					System.err.println("Messages:");
					for (String msg : result.getMessages())
					{
						System.err.println(msg);
					}
				}
			}

		}
		catch (MiramoException | InterruptedException | IOException e)
		{
			e.printStackTrace();
		}
	}
}
</exampleBlock>

