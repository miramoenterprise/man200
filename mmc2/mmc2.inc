@--------------------------------------------------------------------------------
@- mmc2.inc
@- 
@--------------------------------------------------------------------------------
<#def> releaseVersion	{2.0 Alpha}
@- <@include>	${mmc2}/listTest
<@include>	${mmc2}/cover.inc
<@include>      ${refguide}/toc/toc
<chapterStart
	dest="chapter.introduction"
	>Introduction
</chapterStart>
<@include>	${mmc2}/introduction
@--------------------------------------------------------------------------------
<SectionDef allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${mmc2}/commandline
<@include>	${mmc2}/envars
<@include>	${mmc2}/newapi
<@include>	${mmc2}/install
<@include>	${mmc2}/elements
<Section allPages="MP_referenceGuide" />
<@include>	${mmc2}/miramoxml
<@include>	${mmc2}/image
<@include>	${mmc2}/miramodesigner
@--------------------------------------------------------------------------------
<@include>	${mmc2}/cachingAppendix
@--------------------------------------------------------------------------------
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

