@------------------------------------------------------------
@- newapi
@------------------------------------------------------------


@--------------------------------------------------------------------------------
<chapterStart
	dest="newapi"
	>New Java APIs
</chapterStart>
@--------------------------------------------------------------------------------


@-===========================================================
<h1>Java New 64-bit job-processing APIs for Linux and Windows</h1>

<bp>
There are special 64-bit Java job-processing APIs for Linux
and Windows. These provide
a subset of the functions available in the corresponding 32-bit Windows API.
They command-line wrappers with some added capabilities. The added
capabilities include:
</bp>

<xList
	type="dashed"
	labelIndent="8mm"
	>
<listItem>Input file size</listItem>
<listItem>Job start time</listItem>
<listItem>Job end time</listItem>
<listItem>Job elapsed time</listItem>
<listItem>PDF page count</listItem>
<listItem>PDF file size</listItem>
</xList>

<bp>
More features will be added as needed. E.g. later versions may support
job logging.
</bp>

<h2>API download for Linux</h2>
<bp>
The new Linux Java API documentation is available here:
<URL
	url="https://www.miramo.com/download/miramo/java-x64-api-sample.tgz"
	textColor="Blue"
	>Miramo Linux job-processing API</URL>.
</bp>

<h2>API download for Windows</h2>
<bp>
The new Windows Java API documentation is available here:
<URL

	url="https://www.miramo.com/download/miramo/java-x64-api-sample.zip"
	textColor="Blue"
	>Miramo Windows job-processing API</URL>.
</bp>


<h1>Example API implementation</h1>


<bp>
This project uses <URL
	url="https://en.wikipedia.org/wiki/IntelliJ_IDEA"
	textColor="Blue"
	>IntelliJ</URL> and <URL
	url="https://gradle.org/install/"
	textColor="Blue"
	>Gradle</URL>.
</bp>

<bp>
To build the sample from the command line:
</bp>

<xList
	labelIndent="8mm"
	labelSuffix=")"
	>
<listItem>
CD into this directory
</listItem>

<listItem>
Run "gradle shadowJar" - this builds build/libs/java-api-sample-&lt;vs&gt;-all.jar
</listItem>

<listItem>
Copy the testfiles folder from this folder to the machine with Miramo installed [Miramo server]
</listItem>

<listItem>
Copy build/libs/*.jar to the testfiles folder on the Miramo Server
</listItem>

<listItem>
Log into the Miramo server and cd to the testfiles folder
</listItem>

<listItem>
Run "java -jar &lt;jar name&gt; &lt;absolute miramo install path&gt;"
</listItem>

</xList>

<bp>
This should generate sample.pdf and output various information to the command line.
</bp>

<bp>
From IntelliJ (with local MiramoPDF installation):
</bp>

<xList
	labelIndent="8mm"
	>
<listItem>
Open the root folder with IntelliJ
</listItem>

<listItem>
Press Edit Configurations in the top right corner
</listItem>

<listItem>
Add a new Application run configuration
<lp>
	<xList
		labelIndent="8mm"
		type="alpha"
	>
	<listItem>
	set the "Main class:" field to com.miramo.api.sample.Main
	</listItem>
	<listItem>
	set the "Program arguments:" field to include the absolute path to the Miramo installation folder with double quotes if necessary
	</listItem>
	<listItem>
	set "Use classpath of module:" to java-api-sample.main
	</listItem>
	<listItem>
	if not set, set the JRE to the JRE of your choice
	</listItem>
	</xList>
</lp>
</listItem>

</xList>
<bp>
</bp>
<@include> 	${mmc2}/apiExample.inc
<bp>
</bp>
<@skipStart>
<@skipEnd>
