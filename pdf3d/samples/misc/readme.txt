-- 3dvia.com   --

The zip file KR5 sixx R650_1.U3D.zip contains the following files :
- readme.txt
- KR5 sixx R650_1.U3D


-- Model information --

Model Name : KR5 sixx R650_1.U3D
Author : ROCKER-23
Publisher : ROCKER-23

You can view this model here :
http://www.3dvia.com/content/5C0309526476485A
More models about this author :
http://www.3dvia.com/ROCKER-23


-- Attached license --

A license is attached to the KR5 sixx R650_1.U3D model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
