#---------------------------------------------------------------------------------
# .psprofile.ps1
# 
# .profile for man
#---------------------------------------------------------------------------------
$errorView		= 'categoryview'
#---------------------------------------------------------------------------------
$env:manhome		=  "C:\u\miramo\man"
$env:mman		=  "$env:manhome\$env:username"	#- e.g. ./miramo/man/man100
$mmUserName		=  "$env:username"		#- e.g. man100, man200
#---------------------------------------------------------------------------------
$env:PATH		+= ";$env:mman\bin"
#---------------------------------------------------------------------------------
$env:refguide		=  "$env:mman\refguide"
$env:appendices		=  "$env:mman\refguide\appendices"
$env:inline		=  "$env:mman\refguide\inline"
$env:control		=  "$env:mman\control"
$env:inline		=  "$env:mman\refguide\inline"
$env:finline		=  "$env:mman\formsguide\finline"
$env:mfd		=  "$env:mman\refguide\mfd"
$env:annots		=  "$env:mman\refguide\annotsdef"
$env:annotsdef		=  "$env:mman\refguide\annotsdef"
$env:fmfd		=  "$env:mman\formsguide\fmfd"
$env:forms		=  "$env:mman\formsguide"
$env:formsguide		=  "$env:mman\formsguide"
$env:mmdraw		=  "$env:mman\drawguide"
$env:xelements		=  "$env:mman\xelements"
$env:drawguide		=  "$env:mman\drawguide"
$env:mmServer		=  "$env:mman\mmServer"
$env:mmServerGuide	=  "$env:mman\mmServerGuide"
$env:output		=  "$env:mman\output"
$env:ctext		=  "$env:mman\commontext"
$env:ftext		=  "$env:mman\commontext\forms"
$env:mmtmp		=  "$env:mman\mmtmp"
$env:manbin		=  "$env:mman\manbin"
$env:mantmp		=  "$env:mman\mantmp"
$env:mmppUtil		=  "$env:control\mmppUtil"
$env:mmppmacros		=  "$env:control\mmppmacros"
$env:images		=  "$env:manhome\images"
$env:IMG		=  "$env:manhome\images"
$env:formatdefs		=  "$env:mman\formatdefs"
$env:svgtests		=  "$env:mman\tests\svg"
$env:tests		=  "$env:mman\tests"
$env:SVGmacros		=  "$env:mman\\SVGmacros"
$env:mantests		=  "$env:mman\tests\mantests"
$env:fmcrelnotes	=  "$env:mman\fmcrelnotes"
$env:fmcaddendum	=  "$env:mman\fmcaddendum"
$env:mmc2		=  "$env:mman\mmc2"
$env:covers		=  "$env:mman\covers"
$env:unicode		=  "$env:mman\unicode"
$env:mmucd		=  "$env:unicode\unicodeDatabase800"
$env:entities		=  "$env:appendices\characterEntities"
#---------------------------------------------------------------------------------
$env:pdhelp		=  "$env:mman\mmdesigner\pdhelp"
# $env:pdhelp		=  "$env:mman\pdhelp"
$env:mmDefaults		=  "$env:mman\defaultFormats"
#---------------------------------------------------------------------------------
$env:COMPOSERPDF_HOME	=  "\\BUZZ\dev\MiramoPDF"
#---------------------------------------------------------------------------------
$env:path += ";$env:manbin"
$env:path += ";$env:pdhelp\bin"
$env:path += ";$env:mmDefaults\bin"
#---------------------------------------------------------------------------------
$env:ftmp		=  "$env:formatdefs\tmp"
[Environment]::SetEnvironmentVariable("images", "$env:images", "User")
[Environment]::SetEnvironmentVariable("tests", "$env:tests", "User")

#---------------------------------------------------------------------------------
function prompt {
	$str = $pwd.Path
	$nextId = (h -count 1).Id + 1;
	$Host.Ui.Rawui.WindowTitle = $env:username + " " + $str + " - Windows PowerShell"
	Write-Host "(" -nonewline -foregroundcolor $parenthesisColor
	Write-Host ${env:computername} -nonewline -foregroundcolor $computerNameColor
	Write-Host " " -nonewline
	Write-Host $(get-date -f hh:mm:ss) -nonewline -foregroundcolor $userAndDateColor
	Write-Host ")" -nonewline -foregroundcolor $parenthesisColor
	Write-Host " " -nonewline
	Write-Host $(get-location).providerpath
	Write-Host $env:username -nonewline
	Write-Host "  " -nonewline
	Write-Host $nextID  -nonewline
	Write-Host " %" -nonewline
	return " "
	}
#---------------------------------------------------------------------------------
function startTime {
	$global:startTime	= $(get-date)
	$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
	write-host		"Start time:	$nowTime"
	}
function endTime {
	$nowTime		= $(Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
	$elapsedTime		= $(get-date) - $startTime
	$totalTime		= "{0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
	write-host		"End time:	$nowTime"
	write-host		"Elapsed time:	$totalTime"
	}
#---------------------------------------------------------------------------------
. "$env:mman\powershell\wideConsole.ps1"
#---------------------------------------------------------------------------------
#- Use . rps   or . rehash to re-source .psprofile
function rps		{. $env:mman\.psprofile.ps1 }
function rehash		{. $env:mman\.psprofile.ps1 }
#=========================
# Try this one day:
#
Function global:fff ($Name) {
    # Checks and exception throwing are omitted
    $script = get-content "$name"
    $Script -replace '\s*function\s(\w+)','function Global:$1'
    # .([scriptblock]::Create($script))
	Invoke-Expression -Command $Script.ToString()

}
function global:zk {
	. "$env:mman\.psprofile.ps1"
}


function global:bbb {
	$iii = {
		. $env:mman\.psprofile.ps1
		}
	# Invoke-Expression -Command $iii.ToString()
.([scriptblock]::Create($iii))
} 
function aaa {
# lRunspaceInvoke = new RunspaceInvoke(mRunspace);
# lRunspaceInvoke.Invoke("$env:mman\\.psprofile.ps1");
$script = get-content "$env:mman\\.psprofile.ps1"
.([scriptblock]::Create($script))
}
#=========================
#---------------------------------------------------------------------------------
$env:composer		= "FrameMaker"
#---------------------------------------------------------------------------------
function docnotes	{vi $env:mman\docnotes\docnotes.txt }
function helpnotes	{vi $env:mman\docnotes\helpnotes.txt }
function escapes	{vi $env:mman\docnotes\mmdShortcuts.txt }
function mmescapes	{vi $env:mman\docnotes\mmdShortcuts.txt }
function shortcuts	{vi $env:mman\docnotes\mmdShortcuts.txt }
function mmclist	{vi $env:mman\docnotes\mmcList.txt }
function mmdlist	{vi $env:mman\docnotes\mmdList.txt }
#---------------------------------------------------------------------------------
$env:sourceCode		=  "\\horse\d\miramo\xdev\src"
$env:mmDesigner		=  "$env:sourceCode\mmDesigner"
$env:mmDesignerApp	=  "$env:mmDesigner\mmDesignerApp"
# function iqrc		{vi \\horse\d\miramo\xdev\src\mmDesigner\mmDesignerApp\icons.qrc }
function iqrc		{vi $env:mmDesignerApp\icons.qrc }
#---------------------------------------------------------------------------------
function pdefs		{ .  getCurrentConsoleSize ; . setWideConsoleSize
		vi $env:mman\formatdefs\para\paradefs.def
		. revert
}
function pdefs1		{ .  getCurrentConsoleSize ; . setWideConsoleSize
		vi $env:mman\formatdefs\defaults\fontdefs.mmp ; . revert
}
function pdefs2		{ .  getCurrentConsoleSize ; . setWideConsoleSize
		vi $env:mman\formatdefs\para\paradefs.def ; . revert
}
function tdefs		{cd $env:mman\formatdefs\tbl}
function idefs		{cd $env:mman\formatdefs\index}
function fndefs		{cd $env:mman\formatdefs\footnote}
function rdefs		{vi $env:mman\formatdefs\rule\rules.def}
function adefs		{vi $env:mman\formatdefs\arrows\arrows.def}
function fontdefs	{vi $env:mman\formatdefs\font\fontFormats.def}
function cdefs		{vi $env:mman\formatdefs\color\color.def}
function vdefs		{vi $env:mman\formatdefs\var\vardefs.def}
function xrefs		{cd $env:mman\formatdefs\xrefs}
function fontdefs	{cd $env:mman\formatdefs\font}
function defaultdefs	{cd $env:mman\formatdefs\defaults}
function ftmp		{cd $env:mman\formatdefs\tmp}
function mmtmp		{cd $env:mman\mmtmp}
function mantmp		{cd $env:mman\mantmp}
function fdefs		{cd $env:mman\formatdefs}
function indexdefs	{cd $env:mman\formatdefs\index}
function fmcrelnotes	{cd $env:fmcrelnotes}
function fmrelnotes	{cd $env:fmcrelnotes}
function fmcaddendum	{cd $env:fmcaddendum}
function fmaddendum	{cd $env:fmcaddendum}
function mmsg		{cd $env:mmServerGuide}
function covers		{cd $env:covers}
#---------------------------------------------------------------------------------
function control	{cd $env:mman\control}
function pdfout		{cd $env:mman\pdfout}
function tests		{cd $env:mman\tests}
function rindex		{cd $env:mman\refguide\indexes}
function indexes	{cd $env:mman\refguide\indexes}
function manbin		{cd $env:manbin}
function mmucd		{cd $env:mmucd}
function ucd		{cd $env:mmucd}
function unicode	{cd $env:unicode}
function entities	{cd $env:entities}
#---------------------------------------------------------------------------------
function refguide	{cd $env:refguide}
function appendices	{cd $env:appendices}
function fonts		{cd $env:appendices\fonts}
function autonumbering	{cd $env:appendices\autonumbering}
function inline		{cd $env:mman\refguide\inline}
function finline	{cd $env:mman\formsguide\finline}
function toc		{cd $env:refguide\toc}
function contents	{cd $env:refguide\toc}
function running	{cd $env:refguide\running}
function rintro		{cd $env:refguide\rintro}
function language	{cd $env:refguide\language}
function thai		{cd $env:refguide\language\thai}
function arabic		{cd $env:refguide\language\arabic}
function miramoxml	{cd $env:refguide\miramoxml}
function qa		{cd $env:mman\qa}
function color		{cd $env:refguide\color}
function lists		{cd $env:refguide\lists}
function mfd		{cd $env:mfd}
function annots		{cd $env:annots}
function annotsdef	{cd $env:annotsdef}
function fmfd		{cd $env:fmfd}
function forms		{cd $env:forms}
function formsguide	{cd $env:forms}
function msm		{cd $env:refguide\miramoSimpleMarkup}
function propertytypes	{cd $env:refguide\propertyTypes}
function types		{cd $env:refguide\propertyTypes}
function mfds		{cd $env:mfd}
#---------------------------------------------------------------------------------
function mmdraw		{cd $env:mman\drawguide}
function drawguide	{cd $env:mman\drawguide}
function xelements	{cd $env:mman\xelements}
function output		{cd $env:output}
function images		{cd $env:images}
function ctext		{cd $env:ctext}
function ftext		{cd $env:ftext}
function mmutil		{cd $env:mmppUtil}
function mmppmacros	{cd $env:mmppmacros}
function mmppmacs	{cd $env:mmppmacros}
function svgtests	{cd $env:svgtests}
function SVGmacros	{cd $env:SVGmacros}
function SVGmacs	{cd $env:SVGmacros}
function mantests	{cd $env:mantests}
function exmacs		{cd $env:control\exampleProcessing}
function abc		{"success"}
#---------------------------------------------------------------------------------
function mandocs	{cd $env:mnotes\tnotes\tnote_mmdocs}
function dnotes		{cd $env:mnotes\dnotes}
function fnotes		{cd $env:mnotes\fnotes}
function tnotes		{cd $env:mnotes\tnotes}
function xnotes		{cd $env:mnotes\xnotes}
#---------------------------------------------------------------------------------
#- mmPageDesigner help folders
#---------------------------------------------------------------------------------
function pdhelp		{cd $env:pdhelp}
function pddata		{cd $env:pdhelp\dataFiles}
function pdenglish	{cd $env:pdhelp\dataFiles\english}
function pdmacros	{cd $env:pdhelp\mmppMacros}
function pdmacs		{cd $env:pdhelp\mmppMacros}
function pdimages	{cd $env:pdhelp\images}
function pdbin		{cd $env:pdhelp\bin}
#---------------------------------------------------------------------------------
function pdnotes	{vi $env:pdhelp\pdnotes }
#---------------------------------------------------------------------------------
function consoleColor	{

	$bgColorHex		= 0x000058ae	# Amber
	$bgColorHex		= 0x00000000	# Black
	Push-Location
	Set-Location HKCU:\Console
	New-Item ".\%SystemRoot%_system32_WindowsPowerShell_v1.0_powershell.exe"
	Set-Location ".\%SystemRoot%_system32_WindowsPowerShell_v1.0_powershell.exe"

	# New-ItemProperty . ColorTable00 -type DWORD -value 0x00562401 
	# New-ItemProperty . ColorTable00 -type DWORD -value 0x00f1 
	# Set-ItemProperty . ColorTable00 -value 0x004f0000 -Type DWORD
	# Set-ItemProperty . ColorTable00 13395660 -Type DWORD
	# Set-ItemProperty . ColorTable00 6737100 -Type DWORD
	
  	Set-ItemProperty . ColorTable00 -value $bgColorHex -Type DWORD


		$a			= (Get-Host).UI.RawUI
		#-----------------------------------------------------------------
		$whbuffer		= $a.BufferSize
		$whbuffer.Width		= 120
		$whbuffer.Height	= 700
		$a.BufferSize		= $whbuffer
		#-----------------------------------------------------------------
		$maxwh			= $a.MaxWindowSize
		$maxwh.Width		= 150
		$maxwh.Height		= 150
		$a.MaxWindowSize	= $maxwh
		#-----------------------------------------------------------------
		$wh			= $a.WindowSize
		$wh.Width		= 80
		$wh.Height		= 25
		$a.WindowSize		= $wh
		#-----------------------------------------------------------------
		$a.BackgroundColor	= "Black"
		$a.ForegroundColor	= "Blue"
		$a.WindowTitle		= "Hello"
		clear-host
Pop-Location
}

function sps	{
	cmd /c start powershell -NoExit -Command  { consoleColor ; exit }
	cmd /c start powershell -NoExit -Command  { consoleColor }
	 # powershell.exe -NoExit -Command  "& { sss}"
}
#---------------------------------------------------------------------------------
Set-Alias edit	editSpecialManFile
#---------------------------------------------------------------------------------
