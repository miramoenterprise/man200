#--------------------------------------------------------------------------------
#- runy.ps1
#--------------------------------------------------------------------------------
#
#  e.g. runx inline
#  e.g. runx drawguide
#
#--------------------------------------------------------------------------------
$env:releaseVersion	= "1.3"
#--------------------------------------------------------------------------------
# Copy $args array to $runxArgs
$runxArgs	= $args
#--------------------------------------------------------------------------------
$env:ttxFileName 		= "NotoSerifCJKsc-Regular.ttx"
$env:documentInitiator 		= "runx"
$env:documentName 		= "mmDrawGuide.pdf"
$env:documentName		= "mmComposerReferenceGuide.pdf"
$env:composerText		= "mmComposer"
$env:characterListMacros	= "\u\miramo\man\man100\fontCharacters\macros"
$env:setLandscape		= "0"
$env:documentFooterTitle	= "Footer text"
$env:includeFootnoteDef		= "1"
#--------------------------------------------------------------------------------
$env:mkfmcRelnotes	= "N"
$env:mkfmcAddendum	= "N"
$sp			= "N"	# showProperties switch
#--------------------------------------------------------------------------------
$OrigOutputEncoding = [Console]::OutputEncoding
$fmc	= 0	#--- Use mmComposer
$docType		= "fontCharacters\macros"
$env:docName		= "fontCharacters\macros"
# "</MiramoXML>" | out-file -append -enc "utf8" $tmpFile
#--------------------------------------------------------------------------------
$mmVersion	= miramo -v 2>&1 | foreach-object {$_.tostring()} | out-string
$mmVersion	= $mmVersion.split("`n") | select-string -pattern "ersion"
$mmVersion	= $mmVersion | select-string -pattern "iramo"
write-host	"$mmVersion"
$env:mmVersion	= "$mmVersion"
$xxyz	= "$mmVersion"
$env:startTime	= Get-Date -format "yyyy-MMM-dd HH:mm"
$env:docDate	= Get-Date -format "ddMMyy"
$env:thisYear	= Get-Date -format "yyyy"
write-host	"$env:thisYear"
# Set-Content -Path "$env:temp\Newtext.txt" -Value (get-content -Path "c:\Temp\Newtext.txt" | Select-String -Pattern 'H\|159' -NotMatch)
# Set-Content -Path "$env:ttxFileName" -Value (get-content -Path "$env:ttxFileName" | Select-String -Pattern '<?xml ' -NotMatch)
#--------------------------------------------------------------------------------
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::utf8
		#----------------------------------------------------------------
# mmpp -Mfile $env:control\control.mmp $inFile | out-file  $tmpFile -enc utf8
mmpp control.mmp | out-file -enc utf8 zz
# mmpp control.mmp | out-file -enc oem zz
		#----------------------------------------------------------------
		[System.Console]::OutputEncoding = [System.Text.Encoding]::ascii
		#----------------------------------------------------------------
# $env:pid                = $pid

exit

#--------------------------------------------------------------------------------
# mkFormatdefs		# In 'manbin'
#--------------------------------------------------------------------------------
