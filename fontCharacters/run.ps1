#--------------------------------------------------------------------------------
#- run.ps1
#- 
#- Print characters in a font
#--------------------------------------------------------------------------------

# GLYPH_SIZE="7pt" ; export GLYPH_SIZE
#--------------------------------------------------------------------------------
$env:fontCharacters	=  "$env:mman\fontCharacters"
$env:fontDir		=  "$env:fontCharacters\fontfiles"
$env:charmacs		=  "$env:fontCharacters\macros"
$env:unicode		=  "$env:fontCharacters\unicode"
#--------------------------------------------------------------------------------

$env:ttxFileName	= ""
$optionCount		= 0
$argString		= ""
$ttxArgs		= " -t GlyphOrder -t cmap -t head -t hhea -t maxp -t name "

#--------------------------------------------------------------------------------
function getOpts {
	Param([int]$optsCount)

	For ($i=0; $i -lt $optsCount ; $i++)  {
		$optionCount += 1
		$paramNext	= $i + 1

		switch -casesensitive ($argList[$i]) {
			-glyphTopMargin		{}
			-glyphSize		{}
			-fontDir		{$env:thisFontDir	= "$($argList[$paramNext])"}
			-isSymbol		{}		# Value: 0 or 1. (Symbol Encoding)
			-fontTblTitle		{}
			-fontIndexName		{}
			-showCharCount 		{}
			-charDestinations 	{}
			-numCharsVar 		{}
			-fontFileName 		{$env:fontFileName	= "$($argList[$paramNext])"}
			-includeDescription 	{}
			-charRange		{}
			-outputFont		{}
			-t			{$ttxArgs      = " $ttxArgs -t $($argList[$paramNext]) "}
		}

	}
}
#--------------------------------------------------------------------------------
$argList	= $args
#--------------------------------------------------------------------------------


if ($args.count -gt 1) {
  	. getOpts $($args.count - 0)
write-host "$ttxArgs"
write-host "$env:fontFileName"
}
#--------------------------------------------------------------------------------
# $env:fontFileBasename	= (Get-Item '"$env:fontFileName"').Basename
$env:fontFileBasename	= $env:fontFileName -replace '\.[^.\\/]+$'
$env:ttxFileName	= "$env:fontDir\$env:thisFontDir\$env:fontFileBaseName.ttx"

# $env:thisFontDir	= "$env:fontDir\$env:thisFontDir"
write-host 		"fontFileBaseName $env:fontFileBaseName"

write-host "TTX $ttxArgs $env:fontDir\$env:thisFontDir\$env:fontFileName"

#--------------------------------------------------------------------------------
remove-item "$env:ttxFileName" -ErrorAction SilentlyContinue
#--------------------------------------------------------------------------------
invoke-expression "ttx $ttxArgs $env:fontDir\$env:thisFontDir\$env:fontFileName"
#--------------------------------------------------------------------------------




#--------------------------------------------------------------------------------
exit
$command	= "$ttxArgs", "$env:fontDir\$env:thisFontDir\$env:fontFileName"
cmd /c "ttx $command"
exit
& ttx $command
#--------------------------------------------------------------------------------
exit
$allArgs	= @($ttxArgs, "$env:fontDir\$env:thisFontDir\$env:fontFileName")
& ttx $allArgs

ttx $ttxArgs "$env:fontDir\$env:thisFontDir\$env:fontFileName"

# mmpp ${CHARMACS}/mmp.listUnicodeChars | grep -v "<?xml version" > ${FONT_FILE_BASE}.mmo

#  	. getOpts $($args.count - 1)
# write-host "$argString"

write-host "yes $args"
exit
