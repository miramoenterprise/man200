@----------------------------------------------------------------
@- entitylist.map
@----------------------------------------------------------------
<@include> ${MBIN}/macros/mkUnicodeCharArray.mmp
<@include> ${MBIN}/macros/uni_heads.mmp			@--- Gets Unicode char blocks
@----------------------------------------------------------------
<#def> prev.uniheading		{}
<#def> prev.uniheading.1	{}
<#def> prev.uniheading.2	{}
@----------------------------------------------------------------
<#def> FList_col[01]	16
<#def> FList_col[02]	10
<#def> FList_col[03]	10
<#def> FList_col[04]	10
<#def> FList_col[05]	70
@----------------------------------------------------------------
<#def> Wingdings	{
2308|
2329|

260E|
}
<#def> Wingdings	@gsub({<#Wingdings>}, {[ 	<#nl>]}, {})
<#def> Wingdings	@sub({<#Wingdings>}, {\|$}, {})
@- <@write> error {<#Wingdings>}
@----------------------------------------------------------------
<|uni_headings_array> 32|70000
@----------------------------------------------------------------
<@macro> writeTblHeader {
	<Tbl fmt="T_CJKchars" ctm=1.4pt cbm=1.4pt sb=-6pt clm=0 crm=0 P=A  sa=-2pt >
	<TblColWidth W=$FList_col[01]mm />
	<TblColWidth W=$FList_col[02]mm />
	<TblColWidth W=$FList_col[03]mm />
	<TblColWidth W=$FList_col[04]mm />
	<TblColWidth W=$FList_col[05]mm />
	@- <TblTitle fmt=P_fontList_Head1
	<TblTitle fmt=P_TableTitle A="L"
		@- Tgap=2pt >List of built-in character entities <TblCont/><IX
		@- fmt=unilist ix-show=N >List of built-in character entities</IX>
		Tgap=2pt >List of built-in character entities <TblCont/><MkDest
		fmt=B id="list_of_builtin_character_entities" />
	<Row type="H" >
	<Cell fmt="P_CJKchars_RowNo" p="6pt" crm=4mm A=R >Decimal
	<Cell fmt="P_CJKchars_RowNo" p="6pt" >Hex
	<Cell fmt="P_CJKchars_RowNo" p="6pt" >Name
	<Cell fmt="P_CJKchars_RowNo" p="6pt" A=C >Glyph
	@--- Add font-used here ($3)
	<Cell fmt="P_CJKchars_RowNo" p="6pt" >Description
}
@----------------------------------------------------------------
<@macro> entityRow {
	<Row>
	<Cell fmt="P_charNum" cbm="0.5pt" A=R crm=4mm ><#uniDecimal>
	<Cell fmt="P_charNum" cbm="0.5pt" ><#hexNum>
	<Cell fmt="P_charNum" cbm="0.5pt" ><#entityName><IX
		fmt=charEntity pageType="Single"  ix-show=N
		><#entityName><tab
		/><FontRef fmt="F_Courier"><#hexNum></FontRef><tab
		/><FontRef fmt="F_<#fontFamily>"><#entityGlyph></FontRef></IX>
	<Cell fmt="P_charNum" ff="<#fontFamily>" cbm="0.5pt" A=C ><#entityGlyph>
	<Cell fmt="P_UniCharName" cbm="0.5pt"
		Ca=T
		p=5.5pt A="L" li="04mm"
		>@trim(<#uniName>)
}
@----------------------------------------------------------------
<@macro> blockHeads {
	<#ifndef> unihead.first		1
	<#def> unihead.1		@sub({$UniHeadArray[<#uniDecimal>]}, {\|.*}, {})
	<#def> unihead.2		@sub({$UniHeadArray[<#uniDecimal>]}, {^.*\|}, {})
	@--<#uniDecimal>	$UniHeadArray[<#uniDecimal>]
	@--<#uniDecimal>	<#unihead.1>
	@if({<#unihead.1>} <> {<#prev.uniheading.1>}) {
		<#def> prev.uniheading.1	{<#unihead.1>}
		@if(<#unihead.first>) {
			@--- START TABLE
			@--- startTable
			<|writeTblHeader>
			<#def> unihead.first	0
			}
			@--- FIRST LEVEL 	<#unihead.1>
			<Row wp=N wn=Y >
			<Cell cs=R fmt=P_fontList_Head1 ctm=5.5pt ><#unihead.1></Cell>
		}
	@if({<#unihead.2>} <> {<#prev.uniheading.2>}) {
		<#def> prev.uniheading.2        {<#unihead.2>}
		@if(@len(<#unihead.2>)) {
			@--- SECOND LEVEL 	<#unihead.2>
			<Row wp=N wn=Y >
			<Cell cs=R fmt=P_fontList_Head2 ctm=2.5pt ><#unihead.2></Cell>
			}
		}
	<#def> unihead.first	0
}
@----------------------------------------------------------------
<@macro> entity {
	<#def> uniDecimal	$1
	<#def> entityName	$2
	@if($# > 2) {
		<#def> fontFamily	$3
		}
	@else	{
		<#def> fontFamily	{Arial}
		}
	<#ifndef> entityCount	0
	<#def> entityCount	@eval(<#entityCount> + 1)
	<#def> uniDecimal	@sub(<#uniDecimal>, {0*}, {})
	<#def> hexNum		@dec2hex(<#uniDecimal>)
	@while(@len(<#hexNum>) < 4 ) {
		<#def> hexNum		0<#hexNum>
		}
	<#def> hexNum		@toupper(<#hexNum>)
	<#def> entityGlyph	&<#entityName>;
	<#def> uniName		{$uniCharText[<#uniDecimal>] }
	<|blockHeads>
	<|entityRow>
	@- <#uniDecimal>	<#hexNum>	<#entityName>	<#entityGlyph>	<#uniName>
}
@----------------------------------------------------------------
<|entity> 00032|space
<|entity> 00033|excl
<|entity> 00034|quot
<|entity> 00034|quotd
<|entity> 00035|num
<|entity> 00036|dollar
<|entity> 00037|percnt
<|entity> 00038|amp
<|entity> 00039|apos
<|entity> 00039|quot
<|entity> 00040|lpar
<|entity> 00041|rpar
<|entity> 00042|ast
<|entity> 00043|plus
<|entity> 00044|comma
<|entity> 00045|hyphen
<|entity> 00046|period
<|entity> 00047|sol
<|entity> 00058|colon
<|entity> 00059|semi
<|entity> 00060|lt
<|entity> 00061|equals
<|entity> 00062|gt
<|entity> 00063|quest
<|entity> 00064|commat
<|entity> 00091|lsqb
<|entity> 00092|bsol
<|entity> 00093|rsqb
<|entity> 00094|circ
<|entity> 00095|lowbar
<|entity> 00096|grave
<|entity> 00123|lcub
<|entity> 00124|verbar
<|entity> 00125|rcub
<|entity> 00160|nbsp
<|entity> 00161|iexcl
<|entity> 00162|cent
<|entity> 00163|pound
<|entity> 00164|curren
<|entity> 00165|yen
<|entity> 00166|brvbar
<|entity> 00167|sect
<|entity> 00168|Dot
<|entity> 00168|die
<|entity> 00168|uml
<|entity> 00169|copy
<|entity> 00170|ordf
<|entity> 00171|laquo
<|entity> 00172|not
<|entity> 00173|shy
<|entity> 00174|reg
<|entity> 00175|macr
<|entity> 00176|deg
<|entity> 00177|plusmn
<|entity> 00178|sup2
<|entity> 00179|sup3
<|entity> 00180|acute
<|entity> 00181|micro
<|entity> 00182|para
<|entity> 00183|middot
<|entity> 00184|cedil
<|entity> 00185|sup1
<|entity> 00186|ordm
<|entity> 00187|raquo
<|entity> 00188|frac14
<|entity> 00188|fract14
<|entity> 00189|frac12
<|entity> 00189|fract12
<|entity> 00189|half
<|entity> 00190|frac34
<|entity> 00190|fract34
<|entity> 00191|iquest
<|entity> 00192|Agrave
<|entity> 00193|Aacute
<|entity> 00194|Acirc
<|entity> 00195|Atilde
<|entity> 00196|Auml
<|entity> 00197|Aring
<|entity> 00198|AElig
<|entity> 00199|Ccedil
<|entity> 00200|Egrave
<|entity> 00201|Eacute
<|entity> 00202|Ecirc
<|entity> 00203|Euml
<|entity> 00204|Igrave
<|entity> 00205|Iacute
<|entity> 00206|Icirc
<|entity> 00207|Iuml
<|entity> 00208|ETH
<|entity> 00209|Ntilde
<|entity> 00210|Ograve
<|entity> 00211|Oacute
<|entity> 00212|Ocirc
<|entity> 00213|Otilde
<|entity> 00214|Ouml
<|entity> 00215|times
<|entity> 00216|Oslash
<|entity> 00217|Ugrave
<|entity> 00218|Uacute
<|entity> 00219|Ucirc
<|entity> 00220|Uuml
<|entity> 00221|Yacute
<|entity> 00222|THORN
<|entity> 00223|szlig
<|entity> 00224|agrave
<|entity> 00225|aacute
<|entity> 00226|acirc
<|entity> 00227|atilde
<|entity> 00228|auml
<|entity> 00229|aring
<|entity> 00230|aelig
<|entity> 00231|ccedil
<|entity> 00232|egrave
<|entity> 00233|eacute
<|entity> 00234|ecirc
<|entity> 00235|euml
<|entity> 00236|igrave
<|entity> 00237|iacute
<|entity> 00238|icirc
<|entity> 00239|iuml
<|entity> 00240|eth
<|entity> 00241|ntilde
<|entity> 00242|ograve
<|entity> 00243|oacute
<|entity> 00244|ocirc
<|entity> 00245|otilde
<|entity> 00246|ouml
<|entity> 00247|divide
<|entity> 00248|oslash
<|entity> 00249|ugrave
<|entity> 00250|uacute
<|entity> 00251|ucirc
<|entity> 00252|uuml
<|entity> 00253|yacute
<|entity> 00254|thorn
<|entity> 00255|yuml
<|entity> 00256|Amacr
<|entity> 00257|amacr
<|entity> 00258|Abreve
<|entity> 00259|abreve
<|entity> 00260|Aogon
<|entity> 00261|aogon
<|entity> 00262|Cacute
<|entity> 00263|cacute
<|entity> 00264|Ccirc
<|entity> 00265|ccirc
<|entity> 00266|Cdot
<|entity> 00267|cdot
<|entity> 00268|Ccaron
<|entity> 00269|ccaron
<|entity> 00270|Dcaron
<|entity> 00271|dcaron
<|entity> 00272|Dstrok
<|entity> 00273|dstrok
<|entity> 00274|Emacr
<|entity> 00275|emacr
<|entity> 00278|Edot
<|entity> 00279|edot
<|entity> 00280|Eogon
<|entity> 00281|eogon
<|entity> 00282|Ecaron
<|entity> 00283|ecaron
<|entity> 00284|Gcirc
<|entity> 00285|gcirc
<|entity> 00286|Gbreve
<|entity> 00287|gbreve
<|entity> 00288|Gdot
<|entity> 00289|gdot
<|entity> 00290|Gcedil
<|entity> 00292|Hcirc
<|entity> 00293|hcirc
<|entity> 00294|Hstrok
<|entity> 00295|hstrok
<|entity> 00296|Itilde
<|entity> 00297|itilde
<|entity> 00298|Imacr
<|entity> 00299|imacr
<|entity> 00302|Iogon
<|entity> 00303|iogon
<|entity> 00304|Idot
<|entity> 00305|inodot
<|entity> 00306|IJlig
<|entity> 00307|ijlig
<|entity> 00308|Jcirc
<|entity> 00309|jcirc
<|entity> 00310|Kcedil
<|entity> 00311|kcedil
<|entity> 00312|kgreen
<|entity> 00313|Lacute
<|entity> 00313|Lmidot
<|entity> 00314|lacute
<|entity> 00315|Lcedil
<|entity> 00316|lcedil
<|entity> 00317|Lcaron
<|entity> 00318|lcaron
<|entity> 00320|lmidot
<|entity> 00321|Lstrok
<|entity> 00322|lstrok
<|entity> 00323|Nacute
<|entity> 00324|nacute
<|entity> 00325|Ncedil
<|entity> 00326|ncedil
<|entity> 00327|Ncaron
<|entity> 00328|ncaron
<|entity> 00329|napos
<|entity> 00330|ENG
<|entity> 00331|eng
<|entity> 00332|Omacr
<|entity> 00333|omacr
<|entity> 00336|Odblac
<|entity> 00337|odblac
<|entity> 00338|OElig
<|entity> 00339|oelig
<|entity> 00340|Racute
<|entity> 00341|racute
<|entity> 00342|Rcedil
<|entity> 00343|rcedil
<|entity> 00344|Rcaron
<|entity> 00344|Sacute
<|entity> 00345|rcaron
<|entity> 00347|sacute
<|entity> 00348|scirc
<|entity> 00349|Scirc
<|entity> 00350|Scedil
<|entity> 00351|scedil
<|entity> 00352|Scaron
<|entity> 00353|scaron
<|entity> 00354|tcedil
<|entity> 00355|Tcedil
<|entity> 00356|Tcaron
<|entity> 00357|tcaron
<|entity> 00358|Tstrok
<|entity> 00359|tstrok
<|entity> 00360|Utilde
<|entity> 00361|utilde
<|entity> 00362|Umacr
<|entity> 00363|umacr
<|entity> 00364|Ubreve
<|entity> 00365|ubreve
<|entity> 00366|Uring
<|entity> 00367|uring
<|entity> 00368|Udblac
<|entity> 00369|udblac
<|entity> 00370|Uogon
<|entity> 00371|uogon
<|entity> 00372|Wcirc
<|entity> 00373|wcirc
<|entity> 00374|Ycirc
<|entity> 00375|ycirc
<|entity> 00376|Yuml
<|entity> 00377|Zacute
<|entity> 00378|zacute
<|entity> 00379|Zdot
<|entity> 00380|zdot
<|entity> 00382|Zcaron
<|entity> 00402|fnof
<|entity> 00501|gacute
<|entity> 00509|zcaron
<|entity> 00728|breve
<|entity> 00729|dot
<|entity> 00730|ring
<|entity> 00732|tilde
<|entity> 00733|dblac
<|entity> 00913|Agr
<|entity> 00913|Alpha
<|entity> 00914|Beta
<|entity> 00914|Bgr
<|entity> 00915|Gamma
<|entity> 00915|Ggr
<|entity> 00916|Dgr
<|entity> 00917|Egr
<|entity> 00917|Epsilon
<|entity> 00918|Zeta
<|entity> 00918|Zgr
<|entity> 00919|EEgr
<|entity> 00920|THgr
<|entity> 00920|Theta
<|entity> 00921|Igr
<|entity> 00921|Iota
<|entity> 00922|Kappa
<|entity> 00922|Kgr
<|entity> 00923|Lambda
<|entity> 00923|Lgr
<|entity> 00924|Mgr
<|entity> 00925|Ngr
<|entity> 00925|Nu
<|entity> 00926|Xgr
<|entity> 00926|Xi
<|entity> 00927|Ogr
<|entity> 00927|Omicron
<|entity> 00928|Pgr
<|entity> 00928|Pi
<|entity> 00929|Rgr
<|entity> 00929|Rho
<|entity> 00931|Sgr
<|entity> 00931|Sigma
<|entity> 00932|Tau
<|entity> 00932|Tgr
<|entity> 00933|Ugr
<|entity> 00933|Upsilon
<|entity> 00934|PHgr
<|entity> 00934|Phi
<|entity> 00935|Chi
<|entity> 00935|KHgr
<|entity> 00936|PSgr
<|entity> 00936|Psi
<|entity> 00937|OHgr
<|entity> 00937|Omega
<|entity> 00945|agr
<|entity> 00945|alpha
<|entity> 00946|beta
<|entity> 00946|bgr
<|entity> 00947|gamma
<|entity> 00947|ggr
<|entity> 00948|delta
<|entity> 00948|dgr
<|entity> 00949|egr
<|entity> 00949|epsilon
<|entity> 00950|zeta
<|entity> 00950|zgr
<|entity> 00951|eegr
<|entity> 00952|theta
<|entity> 00952|thgr
<|entity> 00953|igr
<|entity> 00953|iota
<|entity> 00954|kappa
<|entity> 00954|kgr
<|entity> 00955|lambda
<|entity> 00955|lgr
<|entity> 00956|mgr
<|entity> 00957|ngr
<|entity> 00957|nu
<|entity> 00958|xgr
<|entity> 00958|xi
<|entity> 00959|ogr
<|entity> 00959|omicron
<|entity> 00960|pgr
<|entity> 00960|pi
<|entity> 00961|rgr
<|entity> 00961|rho
<|entity> 00962|sfgr
<|entity> 00962|sigmaf
<|entity> 00963|sgr
<|entity> 00963|sigma
<|entity> 00964|tau
<|entity> 00964|tgr
<|entity> 00965|ugr
<|entity> 00965|upsilon
<|entity> 00966|phgr
<|entity> 00966|phi
<|entity> 00967|chi
<|entity> 00967|khgr
<|entity> 00968|psgr
<|entity> 00968|psi
<|entity> 00969|ohgr
<|entity> 00969|omega
<|entity> 00977|thetasym
<|entity> 00977|thetav
<|entity> 00978|upsih
<|entity> 00981|phiv
<|entity> 00982|piv
<|entity> 01025|IOcy
<|entity> 01026|DJcy
<|entity> 01027|GJcy
<|entity> 01028|Jukcy
<|entity> 01029|DScy
<|entity> 01030|Iukcy
<|entity> 01031|YIcy
<|entity> 01032|Jsercy
<|entity> 01033|LJcy
<|entity> 01034|NJcy
<|entity> 01035|TSHcy
<|entity> 01036|KJcy
<|entity> 01038|Ubrcy
<|entity> 01039|DZcy
<|entity> 01040|Acy
<|entity> 01041|Bcy
<|entity> 01042|Vcy
<|entity> 01043|Gcy
<|entity> 01044|Dcy
<|entity> 01045|IEcy
<|entity> 01046|ZHcy
<|entity> 01047|Zcy
<|entity> 01048|Icy
<|entity> 01049|Jcy
<|entity> 01050|Kcy
<|entity> 01051|Lcy
<|entity> 01052|Mcy
<|entity> 01053|Ncy
<|entity> 01054|Ocy
<|entity> 01055|Pcy
<|entity> 01056|Rcy
<|entity> 01057|Scy
<|entity> 01058|Tcy
<|entity> 01059|Ucy
<|entity> 01060|Fcy
<|entity> 01061|KHcy
<|entity> 01062|TScy
<|entity> 01063|CHcy
<|entity> 01064|SHcy
<|entity> 01065|SHCHcy
<|entity> 01066|Hardcy
<|entity> 01067|Ycy
<|entity> 01068|SOFTcy
<|entity> 01069|Ecy
<|entity> 01070|YUcy
<|entity> 01071|YAcy
<|entity> 01072|acy
<|entity> 01073|bcy
<|entity> 01074|vcy
<|entity> 01075|gcy
<|entity> 01076|dcy
<|entity> 01077|iecy
<|entity> 01078|zhcy
<|entity> 01079|zcy
<|entity> 01080|icy
<|entity> 01081|jcy
<|entity> 01082|kcy
<|entity> 01083|lcy
<|entity> 01084|mcy
<|entity> 01085|ncy
<|entity> 01086|ocy
<|entity> 01087|pcy
<|entity> 01088|rcy
<|entity> 01089|scy
<|entity> 01090|tcy
<|entity> 01091|ucy
<|entity> 01092|fcy
<|entity> 01093|khcy
<|entity> 01094|tscy
<|entity> 01095|chcy
<|entity> 01096|shcy
<|entity> 01097|chchcy
<|entity> 01098|hardcy
<|entity> 01099|ycy
<|entity> 01100|softcy
<|entity> 01101|ecy
<|entity> 01102|yucy
<|entity> 01103|yacy
<|entity> 01105|iocy
<|entity> 01106|djcy
<|entity> 01107|gjcy
<|entity> 01108|jukcy
<|entity> 01109|dscy
<|entity> 01110|iukcy
<|entity> 01111|yicy
<|entity> 01112|jsercy
<|entity> 01113|licy
<|entity> 01114|njcy
<|entity> 01115|tshcy
<|entity> 01116|kjcy
<|entity> 01118|ubrcy
<|entity> 01119|dzcy
@- <|entity> 01168|0490
@- <|entity> 01169|0491
<|entity> 08194|ensp
<|entity> 08195|emsp
<|entity> 08196|emsp13
<|entity> 08197|emsp14
<|entity> 08199|numsp
<|entity> 08200|puncsp
<|entity> 08201|thinsp
<|entity> 08202|hairsp
<|entity> 08204|zwnj
<|entity> 08205|zwj
<|entity> 08206|lrm
<|entity> 08207|rlm
<|entity> 08208|dash
@- <|entity> 08209|2011
<|entity> 08211|endash
<|entity> 08211|ndash
<|entity> 08212|emdash
<|entity> 08212|mdash
<|entity> 08213|horbar
<|entity> 08214|Verbar
<|entity> 08216|lsquo
<|entity> 08217|rsquo
<|entity> 08217|rsquor
<|entity> 08218|lsquor
<|entity> 08220|ldquo
<|entity> 08221|ddquo
<|entity> 08221|rdquo
<|entity> 08221|rdquor
<|entity> 08222|bdquo
<|entity> 08222|ldquor
<|entity> 08224|dagger
<|entity> 08225|Dagger
<|entity> 08226|bull
<|entity> 08229|mldr
<|entity> 08229|nldr|MinionPro
<|entity> 08230|hellip
@- <|entity> 08231|2027
<|entity> 08233|pgfsep
<|entity> 08240|permil
<|entity> 08242|prime
<|entity> 08243|Prime
<|entity> 08244|tprime
@- <|entity> 08249|2039
<|entity> 08249|lsaquo
@- <|entity> 08250|203A
<|entity> 08250|rsaquo
<|entity> 08254|oline
<|entity> 08254|radx
<|entity> 08257|caret
<|entity> 08259|hybull
@- <|entity> 08260|2044
<|entity> 08260|frasl
<|entity> 08364|euro
<|entity> 08411|tdot
@- <|entity> 08412|DotDot
<|entity> 08453|incare
<|entity> 08459|hamilt
<|entity> 08465|image|SymbolStd
<|entity> 08466|lagran
<|entity> 08471|copysr
<|entity> 08472|weierp|SymbolStd
<|entity> 08476|real|SymbolStd
<|entity> 08478|rx
<|entity> 08482|trade
<|entity> 08486|ohm
<|entity> 08491|angst|KozukaMinchoPro
@- <|entity> 08492|bernou
<|entity> 08499|phmmat
<|entity> 08500|order
<|entity> 08501|alefsym|SymbolStd
<|entity> 08501|aleph|SymbolStd
<|entity> 08531|frac13
<|entity> 08532|frac23
<|entity> 08533|frac15
<|entity> 08534|frac25|KozukaMinchoPro
<|entity> 08535|frac35|KozukaMinchoPro
<|entity> 08536|frac45|KozukaMinchoPro
<|entity> 08537|frac16|KozukaMinchoPro
<|entity> 08538|frac56|KozukaMinchoPro
<|entity> 08539|frac18
<|entity> 08540|frac38
<|entity> 08541|frac58
<|entity> 08542|frac78
<|entity> 08592|larr
<|entity> 08593|uarr
<|entity> 08594|rarr
<|entity> 08595|darr
<|entity> 08596|harr
<|entity> 08629|crarr|SymbolStd
<|entity> 08629|cret|SymbolStd
<|entity> 08656|lArr|SymbolStd
<|entity> 08657|uArr|SymbolStd
<|entity> 08658|rArr|SymbolStd
<|entity> 08659|dArr|SymbolStd
<|entity> 08660|hArr|SymbolStd
<|entity> 08660|iff|SymbolStd
<|entity> 08704|forall|SymbolStd
<|entity> 08706|part
<|entity> 08707|exist|SymbolStd
<|entity> 08709|empty|SymbolStd
<|entity> 08710|Delta
<|entity> 08711|nabla|SymbolStd
<|entity> 08712|isin|SymbolStd
<|entity> 08713|notin|SymbolStd
<|entity> 08715|ni|SymbolStd
<|entity> 08719|prod
<|entity> 08721|sum
<|entity> 08722|minus
<|entity> 08723|mnplus|SymbolStd
<|entity> 08725|divslash
<|entity> 08727|astmath|SymbolStd
<|entity> 08727|lowast|SymbolStd
<|entity> 08728|compfn|SymbolStd
<|entity> 08730|radic
<|entity> 08733|prop|SymbolStd
<|entity> 08734|infin
<|entity> 08735|ang90
<|entity> 08736|ang|SymbolStd
<|entity> 08738|angsph|SymbolStd
<|entity> 08741|par|AdobeSongStd
<|entity> 08743|and|SymbolStd
<|entity> 08744|or|SymbolStd
<|entity> 08745|cap
<|entity> 08746|cup|SymbolStd
<|entity> 08747|int
<|entity> 08750|conint|AdobeSongStd
<|entity> 08756|there4|SymbolStd
<|entity> 08757|becaus|KozukaMinchoPro
<|entity> 08764|sim|SymbolStd
<|entity> 08764|thksim|SymbolStd
<|entity> 08771|sime|KozukaMinchoPro
<|entity> 08773|cong|SymbolStd
<|entity> 08776|asymp
<|entity> 08776|thkap
<|entity> 08777|ap|SymbolStd
<|entity> 08793|wedgeq|SymbolStd
<|entity> 08800|ne
<|entity> 08801|equiv
<|entity> 08804|le
<|entity> 08804|les
<|entity> 08805|ge
<|entity> 08834|sub|SymbolStd
<|entity> 08835|sup|SymbolStd
<|entity> 08836|nsub|SymbolStd
<|entity> 08838|sube|SymbolStd
<|entity> 08839|supe|SymbolStd
<|entity> 08853|oplus|SymbolStd
<|entity> 08855|otimes|SymbolStd
<|entity> 08869|bottom|SymbolStd
<|entity> 08869|perp|SymbolStd
<|entity> 08901|sdot|SymbolStd
<|entity> 08942|vellip|AdobeMyungjoStd
@- <|entity> 08968|lceil|Wingdings
@- <|entity> 08969|rceil|Wingdings
@- <|entity> 08970|lfloor|Wingdings
@- <|entity> 08971|rfloor|Wingdings
@- <|entity> 08972|drcrop|Wingdings
@- <|entity> 08973|dlcrop|Wingdings
@- <|entity> 08974|urcrop|Wingdings
@- <|entity> 08975|ulcrop|Wingdings
@- <|entity> 08981|telrec|Wingdings
@- <|entity> 08982|target|Wingdings
<|entity> 09001|lang|SymbolStd
<|entity> 09002|rang|SymbolStd

@- <|entity> 09251|blank|Wingdings
<|entity> 09600|uhblk
<|entity> 09604|lhblk
<|entity> 09608|block
<|entity> 09612|dtrif
<|entity> 09617|blk14
<|entity> 09618|blk12
<|entity> 09619|blk34
<|entity> 09632|squf
<|entity> 09633|squ|AdobeSongStd
<|entity> 09633|square|AdobeSongStd
@- <|entity> 09645|rect|Wingdings
@- <|entity> 09646|marker|Wingdings
@- <|entity> 09650|utrif
@- <|entity> 09653|utri|Wingdings
@- <|entity> 09656|rtrif|Wingdings
@- <|entity> 09657|rtri|Wingdings
@- <|entity> 09663|dtri|Wingdings
@- <|entity> 09666|ltrif|Wingdings
@- <|entity> 09667|ltri|Wingdings
<|entity> 09674|loz
<|entity> 09675|cir
<|entity> 09733|starf|ZapfDingbatsStd
<|entity> 09734|star|ZapfDingbatsStd
<|entity> 09742|phone|ZapfDingbatsStd
<|entity> 09792|female
<|entity> 09794|male
<|entity> 09824|spades
<|entity> 09827|clubs
<|entity> 09829|heart
<|entity> 09829|hearts
<|entity> 09830|diamond
<|entity> 09830|diams
<|entity> 09834|sung
@- <|entity> 09837|flat|Wingdings
@- <|entity> 09838|natur|Wingdings
<|entity> 09839|sharp
<|entity> 10003|check|ZapfDingbatsStd
<|entity> 10007|cross|ZapfDingbatsStd
<|entity> 10016|malt|ZapfDingbatsStd
<|entity> 10022|lozf|ZapfDingbatsStd
<|entity> 10038|sext|ZapfDingbatsStd
<|entity> 10038|sextile|ZapfDingbatsStd
@- <|entity> 126  See U732 */|/* addCharEnt("tilde
<|entity> 64256|fflig|MinionPro
<|entity> 64257|filig
<|entity> 64258|fllig
<|entity> 64259|ffilig|MinionPro
<|entity> 64260|ffllig|MinionPro
@- <|entity> 8482a|tradeser
@- <|entity> 8482b|tradesan
@- <|entity> 8594a|rarrZ
@- <|entity> 8596a|harrZ
<Row>
<Cell fmt="P_charNum" cbm="0.5pt" A=L crm=4mm cs=R >(<#entityCount> entities)
</Tbl>
