#!/bin/sh
#----------------------------------------------------------------
#- runTTX.sh
#----------------------------------------------------------------

# GLYPH_SIZE="7pt" ; export GLYPH_SIZE
for OPTION in $@
do
        case "${OPTION}" in

		-glyphTopMargin )
			GLYPH_TOPMARGIN="${2}" ;
			export GLYPH_TOPMARGIN
			shift 2 ;;

		-glyphSize )
			GLYPH_SIZE="${2}" ;
			export GLYPH_SIZE
			shift 2 ;;

		-fontDir )
			FONT_DIR="${2}" ;
			export FONT_DIR
			shift 2 ;;

		-isSymbol )		# Value: 0 or 1. (Symbol Encoding)
			SYMBOL="${2}" ;
			export SYMBOL
			shift 2 ;;

		-fontTblTitle )
			FONT_TBL_TITLE="${2}" ;
			export FONT_TBL_TITLE
			shift 2 ;;

		-fontIndexName )
			FONT_INDEX_NAME="${2}" ;
			export FONT_INDEX_NAME
			shift 2 ;;

		-showCharCount )
			SHOW_CHAR_COUNT="${2}" ;
			export SHOW_CHAR_COUNT
			shift 2 ;;

		-charDestinations )
			CHAR_DESTINATIONS="${2}" ;
			export CHAR_DESTINATIONS
			shift 2 ;;

		-numCharsVar )
			numCharsVar="${2}" ;
			export numCharsVar
			shift 2 ;;

		-fontFileName )
			FONT_FILE_NAME="${2}" ;
			export FONT_FILE_NAME
			shift 2 ;;

		-includeDescription )
			INCLUDE_DESCRIPTION="${2}" ;
			export INCLUDE_DESCRIPTION
			shift 2 ;;

		-charRange )
			CHAR_RANGE="${2}" ;
			export CHAR_RANGE
			shift 2 ;;

		-outputFont )
			OUTPUT_FONT="${2}" ;
			export OUTPUT_FONT
			shift 2 ;;

	esac

done


FONT_FILE_BASE=`expr "${FONT_FILE_NAME}" : '\(.*\)\..*'`
#- echo "xxxxxxxxxx $FONT_FILE_BASE" > /tmp/fb
export FONT_FILE_BASE


FONT_DIR=${CHARDIRS}/${FONT_DIR}
export FONT_DIR
CHARMACS=${CHARMACS}			; export CHARMACS

cd $FONT_DIR
# echo "${FONT_DIR}/${FONT_FILE_NAME}" > /tmp/tx
# pwd >> /tmp/tx
#----------------------------------------------------------------
#- Don't change 'INFILE' -- it's used by
#-   
#-   $CHARMACS/mmp.listUnicodeChars
#----------------------------------------------------------------
TTX_FILE_NAME=$FONT_FILE_BASE
export TTX_FILE_NAME
INFILE=${TTX_FILE_NAME}.ttx		; export INFILE

if [ ! -r  ${FONT_FILE_NAME} ]
then
	echo "No such file (or unreadable): ${FONT_DIR}/${FONT_FILE_NAME}" > /tmp/tx

	exit
fi

# pwd >> /tmp/tx
#----------------------------------------------------------------

#----------------------------------------------------------------
#- Remove .ttx file, so ttx creates a new one rather than a
#- file called fontname#n.ttx
#----------------------------------------------------------------
if [ -r  ${TTX_FILE_NAME}.ttx ]
then
	#- echo "---- Removing ${TTX_FILE_NAME}.ttx" 2>
	rm ${TTX_FILE_NAME}.ttx
fi


TTX_OPTS_A='-t GlyphOrder -t cmap -t head -t hhea -t maxp -t name'
TTX_OPTS_B="${TTX_OPTS_A} -t \"OS/2\" "



ttx $TTX_OPTS_B ${FONT_FILE_NAME} > /dev/null


#-----------------------------------------------------------------
#--- Make Mif file with @hex2bin(utf8char)
#-----------------------------------------------------------------

# echo FONT_PREFIX $FONT_PREFIX
# echo INFILE $INFILE

mmpp ${CHARMACS}/mmp.listUnicodeChars | grep -v "<?xml version" > ${FONT_FILE_BASE}.mmo

exit

MM_VERSION="8.1.0p25"; export MM_VERSION
MIRAMO="${MIRAMOBIN}/$MM_VERSION/miramo"
MIRAMO="miramo"

		-Tfile ${TPL_DIR}/chars.tpl ${CHARMACS}/mmp.listUnicodeChars

# echo ${INFILE}



# exit
mmprint -PDFjobopts +eBook.joboptions -Opdf ${FONT_PREFIX}.pdf ${FONT_PREFIX}.mif
