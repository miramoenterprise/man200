@-----------------------------------------------------------
@- miramoSimpleMarkup.inc
@- include files for Miramo Simple Markup
@-----------------------------------------------------------
<chapterStart
	dest="miramoSimpleMarkup"
	>Miramo Simple Markup
</chapterStart>
@-----------------------------------------------------------
<@include>	${refguide}/miramoSimpleMarkup/miramoSimpleMarkup
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
