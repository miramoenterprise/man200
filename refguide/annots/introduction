@------------------------------------------------------------
@- introduction (Format definitions)
@------------------------------------------------------------

<h1>Introduction</h1>

<bp>
Format definition elements specify reusable, named objects, e.g.
table, paragraph, TOC and master page formats.
</bp>

<bp>
The name identifier property for each format definition element is unique
to the element. The identifier property name always has the same name
as the element name itself, except with the first character in lower-case. 
This is illustrated in Example <xparanumonly
	id="ex.formatDefinitionsIdentifiers" />.
</bp>

<exampleBlock
	tabs="10|15"
	lineNumbers="N"
	leftIndent="10mm"
	leading="5pt"
	>
<exampleCaption
	dest="ex.formatDefinitionsIdentifiers"
	>
Format definition identifier property names
</exampleCaption>
<PageDef <xfB>pageDef</xfB>="<xfI>name</xfI>"  <xfI>specification properties</xfI> > <xfI>sub-elements</xfI> </PageDef>
<ContentsDef <xfB>contentsDef</xfB>="<xfI>name</xfI>"  <xfI>specification properties</xfI> > <xfI>sub-elements</xfI> </ContentsDef>
<ParaDef <xfB>paraDef</xfB>="<xfI>name</xfI>"  <xfI>specification properties</xfI> > <xfI>sub-elements</xfI> </ParaDef>
<FontDef <xfB>fontDef</xfB>="<xfI>name</xfI>"  <xfI>specification properties</xfI> />
</exampleBlock>

<bp>
The <fI>MiramoDesigner</fI> GUI save format (.mfd) comprises a
subset of format definition elements and properties described in this chapter.
See the '-mfd' command line option on page <xnpage
	id="commandline.option.-mfd" /> and
the <#xMiramoXML> <pn>mfd</pn> property on page <xnpage
	id="MiramoXML.property.mfd" />.
</bp>

<bp>
Like inline markup elements, format definition elements, along with their properties/attributes
and content, conform with XML syntax.<FNote>See
<HyperCmd fmt="F_URL"
	openURL="http://www.w3schools.com/xml/xml_syntax.asp" >
http://www.w3schools.com/xml/xml_syntax.asp</HyperCmd>
 for a summary of XML syntax rules.</FNote>
</bp>

@------------------------------------------------------------
@- XRef to 'Property value types' Appendix
<@include> ${ctext}/propertyValueTypes.inc
@------------------------------------------------------------

<@skipStart>
Some format definitions depend on the existence of other format definitions.
E.g. the <pn>color</pn> property associated with several format definition
elements depends on there being a <#xColorDef> element with the same tag name.
<@skipEnd>

<@skipStart>
<bp>
Many element properties have both a primary name and a shorthand
name. For example the <#xAFrame> element <on>width</on> property
has a shorthand alias <on>W</on>. Primary names and shorthand names
@- has a shorthand alias <on>W</on> (see page <xnpage
@-      id="" />). Primary names and shorthand names
may be used interchangeably and are treated as the same property
name.
</bp>
<@skipEnd>

