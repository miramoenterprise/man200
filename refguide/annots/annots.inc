@--------------------------------------------------------------------------------
@- annots.inc
@- include files for Miramo Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> inMFD		1
<#def> elementType	mfd
<@include>	${annots}/introduction
<Section allPages="MP_referenceGuide" />
<Section
	@- Needs <DocDef> doubleSided="Y" in pagelayouts.mmp
	oddPages="MP_referenceGuide"
	evenPages="MP_referenceGuideLeft"
	/>
<@include>	${annots}/annonote
<@include>	${annots}/objectannotation
@--------------------------------------------------------------------------------

