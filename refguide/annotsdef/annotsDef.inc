@--------------------------------------------------------------------------------
@- annotsdef.inc
@- include files for annotation Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Annotation inline markup and formats
</chapterStart>
<#def> inMFD		1
<#def> elementType	mfd
<@include>	${annotsDef}/introduction
<Section allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@skipStart>
<Section
	@- Needs <DocDef> doubleSided="Y" in pagelayouts.mmp
	oddPages="MP_referenceGuide"
	evenPages="MP_referenceGuideLeft"
	/>
<@skipEnd>
@--------------------------------------------------------------------------------
@- <@include>	${annotsDef}/objectannotationdef
<@include>	${annotsDef}/annoNote
<@include>	${annotsDef}/annoNoteDef
@--------------------------------------------------------------------------------
