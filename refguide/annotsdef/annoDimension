@------------------------------------------------------------
@-- annoNoteDef
@------------------------------------------------------------
<@xmacro> - nnote {
}{<Font
	@- textColor="DarkBlue"
	@- textColor="Maroon"
	textColor="Blue"
	fontFamily="Noto Sans"
	fontWeight="Regular">$nnote</Font>}
@------------------------------------------------------------
<elementStart
	elementName="annoNote"
	elementType="xml_cont"
	subElements="N"
	@- subElementsList="popupContent"
	>

<esp>
Use the <#xannoNote> element to include a note annotation
format defined by the <#xannoNoteDef> element.
</esp>


</elementStart>
@------------------------------------------------------------

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="annoNoteDef"
	value="name"
	>

<pdp>
<fI>name</fI> of <#xannoNote> format definition.
</pdp>
@- <propertyDefault>1 (no straddle)</propertyDefault>
</property>

@------------------------------------------------------------
@- See Note <xparanumonly
@- 	id="note.ObjectAnnotation.note.noteIconTypes1" /> on
@------------------------------------------------------------

@-===========================================================
<propertyGroup
	title="General properties"
	>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="type"
	value="note|comment|key|help|newParagraph|paragraph|insert"
	>
<pdp>
Note annotation type and icon style.
</pdp>
</property>

@------------------------------------------------------------
<property
	name="left"
	value="dim"
	>
<pdp>
When an <#xannoNote> element is used within a <#xPageDef>, the distance of the left
edge of the note icon from the left edge of the page.
</pdp>

<pdp>
When an <#xannoNote> occurs within the main text flow, the horizontal
distance from the current point.
Positive values shift the note icon to the right.
Negative values shift the note icon to the left.
</pdp>

</property>

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------


<@skipStart>
@------------------------------------------------------------
<#def> pdfIconList {
	checkMark|
	circle|
	comment|
	cross|
	crossHairs|
	help|
	insertText|
	key|
	newParagraph|
	textNote|
	paragraph|
	rightPointer|
	star|
	rightArrow|
	upArrow|
	upLeftArrow|
	}
<@skipEnd>
@--------------------------------------------------------------------------------

@------------------------------------------------------------
<elementNote
	dest="note.ObjectAnnotation.note.noteIconTypes1"
	>
<enp>

@- Figure <xparanumonly
@- 	id="fig.ObjectAnnotation.note.noteIconTypes1" /> shows
examples of the different icon types specified

</enp>

</elementNote>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
