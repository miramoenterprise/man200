@-----------------------------------------------------------
@- miramoxml.inc
@- include files for <MiramoXML>
@-----------------------------------------------------------
<chapterStart
	dest="miramoXML"
	@--- Why doesn't the next line work ?? (too large!)
	@- ><#xMiramoXML> root element
	>Root element: &lt;MiramoXML&gt;
</chapterStart>
<#def> elementType	miramoxml
@-----------------------------------------------------------
<@include>	${refguide}/miramoxml/intro
@-----------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@-----------------------------------------------------------
<@include>	${refguide}/miramoxml/miramoxml
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
