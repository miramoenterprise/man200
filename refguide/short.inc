@--------------------------------------------------------------------------------
@- short.inc
@- include files for Inline Markup Codes
@--------------------------------------------------------------------------------
<@include>	${refguide}/cover/cover.inc
<@include>	${refguide}/toc/toc
<@include>	${refguide}/running/running.inc
<@include>	${refguide}/msm/msm.inc
<@include>	${refguide}/miramoxml/miramoxml.inc
<@include>	${inline}/short.inc
<@include>	${mfd}/short.inc
<@include>	${appendices}/fonts/fonts.inc
<@include>	${appendices}/elementSummary/elementSummary.inc
<@include>	${refguide}/index/index
@- <@include>	${refguide}/language/language.inc
<@skipStart>
<@skipEnd>
