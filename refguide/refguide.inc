@--------------------------------------------------------------------------------
@- refguide.inc
@- include files for Inline Markup Codes
@--------------------------------------------------------------------------------
<@include>	${refguide}/cover/cover.inc
<@include>	${refguide}/toc/toc.inc
<@include>	${refguide}/running/running.inc
@- <@include>	${refguide}/miramoSimpleMarkup/miramoSimpleMarkup.inc
@- <@include>	${refguide}/propertyTypes/propertyTypes.inc
<@include>	${refguide}/miramoxml/miramoxml.inc
<@include>	${inline}/inline.inc
<@include>	${mfd}/mfd.inc
<@include>	${appendices}/appendices.inc
<@include>	${refguide}/indexes/index.inc
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------
