@-----------------------------------------------------------
@- capitalization.inc
@-----------------------------------------------------------
@- capitalization.inc
@- include files for capitalization appendix
@-----------------------------------------------------------
<chapterStart
	dest="capitalization"
	type="appendix"
	>Text capitalization@- and case folding
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/capitalization/capitalization

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

