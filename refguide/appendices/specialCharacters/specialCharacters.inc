@-----------------------------------------------------------
@- specialCharacters.inc
@-----------------------------------------------------------
@- specialCharacters.inc
@- include files for color appendix
@-----------------------------------------------------------
<chapterStart
	dest="specialCharacters"
	type="appendix"
	>Special characters
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/specialCharacters/specialCharacters

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

