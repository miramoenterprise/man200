@-----------------------------------------------------------
<!--  File = autonumbering.inc (Start) -->
@-----------------------------------------------------------
@- autonumbering.inc
@- include files for autonumbering appendix
@-----------------------------------------------------------
<chapterStart
	dest="positioning"
	type="appendix"
	>Object positioning
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/positioning/introduction
<@include>	${appendices}/positioning/aframe-inparagraph
<@include>	${appendices}/positioning/atextframe-inparagraph
<@include>	${appendices}/positioning/mathml-inparagraph
<@include>	${appendices}/positioning/table-inparagraph

<@include>	${appendices}/positioning/aframe-toplevel
<@include>	${appendices}/positioning/atextframe-toplevel
<@include>	${appendices}/positioning/mathml-toplevel
<@include>	${appendices}/positioning/table-toplevel
<@skipStart>
<@skipEnd>

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

@-----------------------------------------------------------
<!--  File = autonumbering.inc (End) -->
@-----------------------------------------------------------
