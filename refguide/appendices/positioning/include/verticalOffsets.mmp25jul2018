@-===========================================================
@- verticalOffsets.mmp
@-===========================================================
<@xmacro> annotate {
	@- <#ifndef> a.top			<#outputTopMargin>
	<#ifndef> a.left		30mm
	<#ifndef> a.left		33mm
	<#ifndef> a.horizontalLength	14mm
	@- <#ifndef> a.annotations		{}
	<#def> a.lineColor		{Blue}
	<#def> a.textColor		{Blue}
	<#def> a.length			10mm
	<#def> a.text			{text}
	<#def> a.textAlign		{left}
	<#def> a.show			1
	<#def> a.lastAnnotation		0
	@----------------------------------------------------
	@xgetvals(a.)
	@----------------------------------------------------
	<#def> a.annotations		{<#a.annotations>
		@if(<#a.show>) {
			<mmDraw>
				@- Horizontal indicator line
				<ALine
					top="<#a.top>"
					left="@eval(<#a.left> - <#a.horizontalLength>)"
					lineLength="@eval(<#a.horizontalLength> + 2)"
					penColor="Red"
					arrowHead="none"
					lineAngle="90"
					penWidth="0.3pt"
					penStyle="solid"
					/>
			
			@if(<#a.lastAnnotation>) {
				@- Horizontal indicator line
				<ALine
					top="@eval(<#a.top> + <#a.length>)"
					left="@eval(<#a.left> - <#a.horizontalLength>)"
					lineLength="@eval(<#a.horizontalLength> + 2)"
					penColor="Red"
					arrowHead="none"
					lineAngle="90"
					penWidth="0.3pt"
					penStyle="solid"
					/>
			}
				@- Vertical dimension line
				<ALine
					top="<#a.top>"
					left="<#a.left>"
					lineLength="<#a.length>"
					penColor="<#a.lineColor>"
					arrowHead="both"
					textPosition="center"
					textAlign="<#a.textAlign>"
					textAngle="90"
					textColor="<#a.textColor>"
					fontDef="F_AnnoSmallLight"
					lineAngle="180" penWidth="0.3pt"
					penStyle="solid"
					>&emsp;<#a.text></ALine>
			</mmDraw>
			}
		}
	<#def> a.top			@eval(<#a.top> + <#a.length>)
}{}
@-===========================================================

@-===========================================================
<@xmacro> setParameters {

	<#def> includeLeading			1
	<#def> inParagraph			1
	@----------------------------------------------------
	<#def> outputWidth			35mm
	<#def> outputWidth			36mm
	<#def> outputLeftMargin			2mm
	<#def> outputTopMargin			2mm
	<#def> outputRightMargin		07mm
	<#def> outputRightMargin		08mm
	@----------------------------------------------------
	<#def> a.top				<#outputTopMargin>
	<#def> a.annotations			{}	@- Reset annotations
	@----------------------------------------------------
	<#def> spaceBelowPgfA			6mm
	<#def> textSizePgfA			4mm
	<#def> leadingPgfA			2mm
	@----------------------------------------------------
	<#def> objectWidth			18mm
	<#def> objectHeight			14mm
	<#def> objectSpaceAbove			6mm
	<#def> objectSpaceBelow			18mm
	<#def> objectTitleGap			1mm
	<#def> objectTitleTextSize		2mm
	<#def> objectTitleLeading		5mm
	<#def> objectTitleMarginAbove		0.5mm
	<#def> objectTitleMarginBelow		0.5mm
	@----------------------------------------------------
	<#def> spaceAbovePgfB			6mm
	<#def> textSizePgfB			4mm
	<#def> leadingPgfB			2mm
	@----------------------------------------------------
	@xgetvals()
	@----------------------------------------------------
	<#def> lowerSpaceBetween		0
	<#def> upperSpaceBetween		0
	@----------------------------------------------------
	@if(<#includeLeading>) {
		<#def> textHeightA		@eval(<#textSizePgfA>
							+ <#leadingPgfA>)
		<#def> objectHeightTotal	@eval(<#objectHeight>
							+ <#objectTitleGap>
							+ <#objectTitleMarginAbove>
							+ <#objectTitleTextSize>
							+ <#objectTitleMarginBelow>
							@- + <#objectTitleLeading>
							)
		}
	@else {
		<#def> textHeightA		<#textSizePgfA>
		<#def> objectHeightTotal	@eval(<#objectHeight>
							+ <#objectTitleGap>
							+ <#objectTitleMarginAbove>
							+ <#objectTitleTextSize>
							+ <#objectTitleMarginBelow>
							)
		}
	<@write> error {textHeightA		<#textHeightA>}
	@----------------------------------------------------
	@if(<#inParagraph>) {
		@- Assumes object is in <P>, position="below"
		<#def> upperSpaceBetween	<#objectSpaceAbove>
	@if(<#spaceAbovePgfB> > <#spaceBelowPgfA>) {
		<#def> lowerSpaceBetween	<#spaceAbovePgfB>
		}
	@else {
		<#def> lowerSpaceBetween	<#spaceBelowPgfA>
		}
		}
	@else {
		@- Assumes object is included at top level (outside <P>)
		@if(<#spaceBelowPgfA> > <#objectSpaceAbove>) {
			<#def> upperSpaceBetween	<#spaceBelowPgfA>
			}
		@else {
			<#def> upperSpaceBetween	<#objectSpaceAbove>
			}
	@----------------------------------------------------
	@- Space between object and next paragraph
	@- (same whether inParagraph or not)
	@----------------------------------------------------
	@if(<#spaceAbovePgfB> > <#objectSpaceBelow>) {
		<#def> lowerSpaceBetween	<#spaceAbovePgfB>
		}
	@else {
		<#def> lowerSpaceBetween	<#objectSpaceBelow>
		}
		}
	@----------------------------------------------------
	<annotate length="<#textHeightA>" text="a" />
	<annotate length="<#upperSpaceBetween>" text="b" />
	<annotate length="<#objectHeightTotal>" text="c" />
	<annotate length="<#lowerSpaceBetween>" text="d" lastAnnotation="1" />
	@----------------------------------------------------
}
@------------------------------------------------------------
