@-----------------------------------------------------------
@- propertyTypes.inc
@-----------------------------------------------------------
<chapterStart
	dest="propertyValueTypes"
	type="appendix"
	>Property value types
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/propertyValueTypes/propertyTypes
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
