@-----------------------------------------------------------
<!--  File = autonumbering.inc (Start) -->
@-----------------------------------------------------------
@- autonumbering.inc
@- include files for autonumbering appendix
@-----------------------------------------------------------
<chapterStart
	dest="autonumbering"
	type="appendix"
	>Autonumbering
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/autonumbering/introduction
<@include>	${appendices}/autonumbering/simpleAutonumberedLists
<@include>	${appendices}/autonumbering/complexAutonumbering.mmp
@- <@include>	${appendices}/autonumbering/autonumbering2
<@include>	${appendices}/autonumbering/complexAutonumberedLists
<@include>	${appendices}/autonumbering/numberAlignment
<@include>	${appendices}/autonumbering/indentedNumbering
<@include>	${appendices}/autonumbering/arabic
<@include>	${appendices}/autonumbering/numberStylesList

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

@-----------------------------------------------------------
<!--  File = autonumbering.inc (End) -->
@-----------------------------------------------------------
