@-----------------------------------------------------------
@- imageSizing.inc
@-----------------------------------------------------------
@- imageSizing.inc
@- include files for imageSizing appendix
@-----------------------------------------------------------
<chapterStart
	dest="imageSizing"
	type="appendix"
	>Rules for image sizing
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/imageSizing/imageSizing

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

