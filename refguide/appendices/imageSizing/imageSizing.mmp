@--------------------------------------------------------------------------------
@- imageSizing.mmp
@--------------------------------------------------------------------------------
<#def> fullHeaders	Y
<#def> numProps		8
<#def> numCols		9
@------------------------------------------------------------------------
<#def> propName[1]	W
<#def> propName[2]	H
<#def> propName[3]	maxW
<#def> propName[4]	maxH
<#def> propName[5]	ppi
<#def> propName[6]	scale
<#def> propName[7]	kAR
<#def> propName[8]	s2F
@------------------------------------------------------------------------
@- Mark them all as absent (value=A)
@------------------------------------------------------------------------
<#def> isTick		{&#x2714;}	@- Heavy check mark
<#def> isTick		{&#x2713;}	@- Check mark
<#def> isTick		{<Font fontFamily="Source Sans Pro"
				textColor="Maroon"
				fontWeight="Bold" ><#isTick></Font>}
@------------------------------------------------------------------------
<#def> isCross		{&#x274C;}	@- Cross mark
<#def> isCross		{&#x2717;}	@- Ballot X / Cross mark /
<#def> isCross		{&#x2718;}	@- Heavy ballot X / Deja Vu Sans / Linux Libertine /
					@- Segoe UI Symbol
<#def> isCross		{x}	@- Cross mark
<#def> isCross		{<Font fontFamily="Source Sans Pro"
				fontWeight="Regular" ><#isCross></Font>}
@--------------------------------------------------------------------------------
<@macro> setDefaults {
	@for(prop = 1 to <#numProps>) {
		<#def> propName			$propName[$$prop]
		<#def> propValue[$$prop]	A
		}
}
@--------------------------------------------------------------------------------
<@macro> isColumnHeadings {
	@for(h = 1 to <#numProps>) {
		<#def> propHeader[$$h]	$propName[$$h]
		}
	@if(<#fullHeaders> = Y) {
		@for(h = 1 to <#numProps>) {
			<#def> xPropHeader	$propHeader[$$h]
			@if(@match({-<#xPropHeader>-}, {-maxW-})) {
			 	<#def> propHeader[$$h]	{maximumWidth}
				}
			@if(@match({-<#xPropHeader>-}, {-W-})) {
			 	<#def> propHeader[$$h]	{width}
				}
			@if(@match({-<#xPropHeader>-}, {-maxH-})) {
			 	<#def> propHeader[$$h]	{maximumHeight}
				}
			@if(@match({-<#xPropHeader>-}, {-H-})) {
			 	<#def> propHeader[$$h]	{height}
				}
			@if(@match({-<#xPropHeader>-}, {-kAR-})) {
			 	<#def> propHeader[$$h]	{keepAspectRatio}
				}
			@if(@match({-<#xPropHeader>-}, {-s2F-})) {
			 	<#def> propHeader[$$h]	{shrinkToFit}
				}
			}
		}
		<@skipStart>
		<@skipEnd>
	<Cell/>
	@for(col = 1 to <#numProps>) {
		@if(<#fullHeaders> = Y) {
			<#def> isHeaderVal	$propHeader[$$col]
			<Cell cellOrientation="-90"
				verticalTextAlign="middle"
				><pn textSize="7pt" ><#isHeaderVal></pn></Cell>
				@- >$propHeader[$$col]</Cell>
			}
		@else {
			<Cell
				verticalTextAlign="middle"
				><pn textSize="7pt" ><#isHeaderVal></pn></Cell>
				@- >$propHeader[$$col]</Cell>
			}
		}
}
@--------------------------------------------------------------------------------
<@macro> isColumnProps {
	<TblColumn width="6mm" />
	@for(col = 1 to <#numProps>) {
		@if(<#fullHeaders> = Y) {
			<TblColumn width="6mm" />
			}
		@else {
			<TblColumn width="10.5mm" />
			}
		}
	<TblColumn width="75mm" />
}
@--------------------------------------------------------------------------------
<@macro> isFooterRows {
	<@skipStart>
	<Row type="footer" >
	<Cell columnSpan="remainder"
		startRule="none"
		endRule="none"
		/>
	</Row>
	<@skipEnd>
	@------------------------------------------------------------------------
	<Row type="footer"
		@- topRule="none"
		topMargin="2mm"
		>
	@- <Cell/>
	<Cell columnSpan="remainder" startRule="none" >
<P paraDef="P_propertyDescription" textAlign="center" ><#isTick>
&ensp;
Property is set
&emsp;
&emsp;
<#isCross>
&ensp;
Property is ignored when
property is set
(<#isTick>)
</P>
	</Cell>
	</Row>
}
@--------------------------------------------------------------------------------
<@macro> startImgSizeTable {
	<#def> is.tblTitle	$1
	<#def> inProperty	1
	<#def> inPropertyGroup	1
	<#def> inSimpleTable	1
	<#def> inEquation	1
	<Tbl tblDef="thinRulings"
		startRule="none"
		topRule="none"
		endRule="none"
		bottomRule="none"
		>
		@----------------------------------------------------------------
		@- Add table title with xref dest !!!!
		@----------------------------------------------------------------
		<TblTitle><tableTitle><#is.tblTitle></tableTitle></TblTitle>
		@----------------------------------------------------------------
		<|isColumnProps>
		@----------------------------------------------------------------
		@- Header header row
		@----------------------------------------------------------------
		<Row type="header" >
			<|isColumnHeadings>
		</Row>
		<|isFooterRows>
}
@--------------------------------------------------------------------------------
<@macro> endImgSizeTable {
	<#def> inProperty	0
	<#def> inPropertyGroup	0
	<#def> inSimpleTable	0
	<#def> inEquation	0
	</Tbl>
}
@--------------------------------------------------------------------------------
<@macro> getValue {
	@------------------------------------------------------------------------
	@- Get new value
	@------------------------------------------------------------------------
	<#def> propNameLen	@len(<#propName>)
	<#def> matchStartPos	@match({<#newValues>}, {<#propName>})
	<#def> matchStartPos	@eval(<#matchStartPos> + <#propNameLen>)
	<#def> matchVal		@substr({<#newValues>}, <#matchStartPos>)
	<#def> matchStartPos2	@match({<#matchVal>}, {"})
	<#def> matchStartPos2	@eval(<#matchStartPos2> + 1)
	<#def> matchVal		@substr({<#matchVal>}, <#matchStartPos2>)
	<#def> matchStartPos3	@match({<#matchVal>}, {"})
	<#def> matchStartPos3	@eval(<#matchStartPos3> - 1)
	<#def> matchVal		@substr({<#matchVal>}, 1, <#matchStartPos3>)
	<#def> matchVal		@trim({<#matchVal>})
	@- <@write> error {matched: <#propName> |<#matchVal>| (<#matchStartPos>)}
}
@--------------------------------------------------------------------------------
<@xmacro> - incRow {
	<#ifndef> rowCount	0
	<#def> rowCount		@eval(<#rowCount> + 1)
	<#def> dest		{}
	@------------------------------------------------------------------------
	@- should be prefixed by isr.
	<|setDefaults>
	@------------------------------------------------------------------------
	@xgetvals()
	@------------------------------------------------------------------------
	@- E.g. W="Y" maxW="I"
	@------------------------------------------------------------------------
	@if(@len({x<#dest>}) > 1) {
		<#def> rowDest		{<MkDest id="isr.<#dest>" />}
		}
	@else {
		<#def> rowDest		{}
		}
}{
	<#def> newValues	{@xputvals()}
	@------------------------------------------------------------------------
	@-
	@------------------------------------------------------------------------
	@for(prop = 1 to <#numProps>) {
		<#def> propName		$propName[$$prop]
		@----------------------------------------------------------------
		@if(@match({<#newValues>}, {<#propName>})) {
			<|getValue>
			<#def> propValue[$$prop]	{<#matchVal>}
			}
			@- <@write> error {$propName[$$prop]="$propValue[$$prop]"}
		}
	@------------------------------------------------------------------------
	@- Write out the row
	@------------------------------------------------------------------------
	<Row>
		<Cell><pnum><#rowCount></pnum><#rowDest></Cell>
	@for(prop = 1 to <#numProps>) {
		@if($propValue[$$prop] = A ) {
			<Cell/>
			}
		@if($propValue[$$prop] = Y ) {
		<Cell textAlign="center" ><#isTick></Cell>
			}
		@if($propValue[$$prop] = I ) {
		<Cell textAlign="center" ><#isCross></Cell>
			}
		}
		<Cell><P paraDef="P_propertyDescription" >$incRow</P></Cell>
		@- <Cell><P paraDef="P_propertyDesc" >$incRow</P></Cell>
	</Row>
}
@--------------------------------------------------------------------------------
