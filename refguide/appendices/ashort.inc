@------------------------------------------------------------
@- ashort.inc
@------------------------------------------------------------
<@include>	${appendices}/imageSizing/imageSizing.inc
<@skipStart>
<@include>	${appendices}/appendicesStart
<@include>	${appendices}/fonts/fonts.inc
<@include>	${appendices}/textlanguage/langAndHyphenation.inc
<@include>	${appendices}/autonumbering/autonumbering.inc
<@include>	${appendices}/capitalization/capitalization.inc
<@include>	${appendices}/elementSummary/elementSummary.inc
<@skipEnd>

