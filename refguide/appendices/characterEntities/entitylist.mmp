@----------------------------------------------------------------
@- entitylist.map
@----------------------------------------------------------------
@- <@include> ${MBIN}/macros/mkUnicodeCharArray.mmp
@- <@include> ${MBIN}/macros/uni_heads.mmp			@--- Gets Unicode char blocks
@----------------------------------------------------------------
@- <#def> prev.uniheading		{}
<#def> previousBlockTitle	{}
<#def> previousBlockSubTitle	{}
@----------------------------------------------------------------
<#def> FList_col[01]	12
<#def> FList_col[02]	12
<#def> FList_col[03]	12
<#def> FList_col[04]	12
<#def> FList_col[05]	70
@----------------------------------------------------------------
@- <|uni_headings_array> 32|70000
@----------------------------------------------------------------
<@macro> writeTblHeader {
	@- <Tbl fmt="T_characterList" ctm="1.4pt" cbm="1.4pt" sb="-6pt" clm="0" crm="0" P="A"  sa="-2pt" >
	<Tbl fmt="T_characterList" 
		@- position="topOfPage"
		@- spaceAbove="0"
		startID="characterEntityList.Start"
		endID="characterEntityList.End"
		>
	<TblColumn width="$FList_col[01]mm" />
	<TblColumn width="$FList_col[02]mm" />
	<TblColumn width="$FList_col[03]mm" />
	<TblColumn width="$FList_col[04]mm" />
	@- <TblColumn W="$FList_col[05]mm" />
	<TblColumn width="1*" />
	<TblTitle @- fmt="P_characterListTblTitle"
	@- <TblTitle fmt="P_appendixTableTitle"
	@-	Tgap="2pt" >List of built-in character entities <TblCont/><IX
	@-	fmt="unilist" ix-show="N" >List of built-in character entities</IX>
		Tgap="2pt"
		><P fmt="P_characterListTblTitle" >List of built-in character entities <TblContinuation fmt="TC_simpleTable" /></P>
	</TblTitle>
	<Row type="H" >
	<Cell leftMargin="0mm" ><P fmt="P_characterListHeader" textAlign="end" >Decimal</P></Cell>
	<Cell leftMargin="2mm" ><P fmt="P_characterListHeader" >Hex</P></Cell>
	<Cell leftMargin="0mm" ><P fmt="P_characterListHeader" >Name</P></Cell>
	<Cell leftMargin="2mm" ><P fmt="P_characterListHeader" >Glyph</P></Cell>
	<Cell leftMargin="2mm" ><P fmt="P_characterListHeader" >Description</P></Cell>
	</Row>
	@- <Row type="footer" maximumHeight="0.1pt" topRule="<#tblRules>" />
}
@----------------------------------------------------------------
<@macro> entityRow {
	<#def> decNum		@hex2dec(<#hexNum>)
	<Row>
	<Cell><P fmt="P_charNum" textAlign="end" ><#uniDecimal></P></Cell>
	<Cell leftMargin="2mm" ><P fmt="P_charNum" textAlign="start" ><#hexNum></P></Cell>
	<Cell leftMargin="0mm" ><P fmt="P_charNum" textAlign="start" ><#entityName></P></Cell>
	@- <Cell><P fmt="P_charNum" fontFamily="Arial" textAlign="center" ><#entityGlyph></P></Cell>
	@- <Cell><P fmt="P_charNum" fontFamily="Noto Sans" textAlign="center" ><#entityGlyph></P></Cell>
	@if(@match({<#entityFont>}, {default})) {
		<Cell><P fmt="P_charNum" fontFamily="Source Sans Pro" textAlign="center" ><#entityGlyph></P></Cell>
		}
	@else  {	
		<Cell><P fmt="P_charNum" fontFamily="<#entityFont>" textAlign="center" ><#entityGlyph></P></Cell>
		}
	<Cell bottomMargin="0.5pt"
		Ca="T"
		@- p="5.5pt" A="L" li="04mm"
		startMargin="2mm"
		><P fmt="P_charNum" textSize="5.5pt" textAlign="start" >$unicodeCharacterName[<#decNum>]</P></Cell>
	</Row>
}
@----------------------------------------------------------------
<@macro> blockTitles {
	<#ifndef> unihead.first		1
	<#def> blockTitle		$unicodeBlockTitle[<#uniDecimal>]
	<#def> blockSubTitle		$unicodeBlockSubTitle[<#uniDecimal>]
	@if({<#blockTitle>} <> {<#previousBlockTitle>}) {
		<#def> previousBlockTitle	{<#blockTitle>}
		@if(<#unihead.first>) {
			@--- START TABLE
			@--- startTable
			@- <|writeTblHeader>
			<#def> unihead.first	0
			}
			@--- FIRST LEVEL 	<#blockTitle>
			<Row wp="N" wn="Y" topMargin="7pt" >
			<Cell cs="remainder"
				><P fmt="P_characterBlockTitle" ><#blockTitle></P></Cell>
			</Row>
		}
	@if({<#blockSubTitle>} <> {<#previousBlockSubTitle>}) {
		<#def> previousBlockSubTitle        {<#blockSubTitle>}
		@- @if(@len(<#blockSubTitle>)) {
			@--- SECOND LEVEL 	<#blockSubTitle>
			<Row withPrevious="N" withNext="Y" >
			<Cell cs="remainder"  topMargin="3.5pt"
				><P fmt="P_characterBlockSubTitle" ><#blockSubTitle></P></Cell>
			</Row>
		@- 	}
		}
	<#def> unihead.first	0
}
@----------------------------------------------------------------
<@macro> entity {
	<#def> uniDecimal	$1
	<#def> entityName	$2
	<#def> entityFont	{default}
	@if($# > 2 ) {
		<#def> entityFont	{$3}
		@if(@len(<#entityFont>)) {
			@if(@match({<#entityFont>}, {SUIS})) {
				<#def> entityFont	{Segoe UI Symbol}
		 		}
			@else {
				<#def> entityFont	{default}
		 		}
		 	}
		}
	<#def> entityCount	0
	<#def> uniDecimal	@sub(<#uniDecimal>, {0*}, {})
	<#def> hexNum		@dec2hex(<#uniDecimal>)
	@while(@len(<#hexNum>) < 4 ) {
		<#def> hexNum		0<#hexNum>
		}
	<#def> hexNum		@toupper(<#hexNum>)
	<#def> entityGlyph	&<#entityName>;
	<#def> uniName		{$uniCharText[<#uniDecimal>] }
	<|blockTitles>
	<|entityRow>
	@- <#uniDecimal>	<#hexNum>	<#entityName>	<#entityGlyph>	<#uniName>
}
@----------------------------------------------------------------
<|writeTblHeader>
@----------------------------------------------------------------
<|entity> 00032|space
<|entity> 00033|excl
<|entity> 00034|quot
<|entity> 00034|quotd
<|entity> 00035|num
<|entity> 00036|dollar
<|entity> 00037|percnt
<|entity> 00038|amp
<|entity> 00039|apos
<|entity> 00039|quot
<|entity> 00040|lpar
<|entity> 00041|rpar
<|entity> 00042|ast
<|entity> 00043|plus
<|entity> 00044|comma
<|entity> 00045|hyphen
<|entity> 00046|period
<|entity> 00047|sol
<|entity> 00058|colon
<|entity> 00059|semi
<|entity> 00060|lt
<|entity> 00061|equals
<|entity> 00062|gt
<|entity> 00063|quest
<|entity> 00064|commat
<|entity> 00091|lsqb
<|entity> 00092|bsol
<|entity> 00093|rsqb
<|entity> 00094|circ
<|entity> 00095|lowbar
<|entity> 00096|grave
<|entity> 00123|lcub
<|entity> 00124|verbar
<|entity> 00125|rcub
<|entity> 00160|nbsp
<|entity> 00161|iexcl
<|entity> 00162|cent
<|entity> 00163|pound
<|entity> 00164|curren
<|entity> 00165|yen
<|entity> 00166|brvbar
<|entity> 00167|sect
<|entity> 00168|Dot
<|entity> 00168|die
<|entity> 00168|uml
<|entity> 00169|copy
<|entity> 00170|ordf
<|entity> 00171|laquo
<|entity> 00172|not
<|entity> 00173|shy
<|entity> 00174|reg
<|entity> 00175|macr
<|entity> 00176|deg
<|entity> 00177|plusmn
<|entity> 00178|sup2
<|entity> 00179|sup3
<|entity> 00180|acute
<|entity> 00181|micro
<|entity> 00182|para
<|entity> 00183|middot
<|entity> 00184|cedil
<|entity> 00185|sup1
<|entity> 00186|ordm
<|entity> 00187|raquo
<|entity> 00188|frac14
<|entity> 00188|fract14
<|entity> 00189|frac12
<|entity> 00189|fract12
<|entity> 00189|half
<|entity> 00190|frac34
<|entity> 00190|fract34
<|entity> 00191|iquest
<|entity> 00192|Agrave
<|entity> 00193|Aacute
<|entity> 00194|Acirc
<|entity> 00195|Atilde
<|entity> 00196|Auml
<|entity> 00197|Aring
<|entity> 00198|AElig
<|entity> 00199|Ccedil
<|entity> 00200|Egrave
<|entity> 00201|Eacute
<|entity> 00202|Ecirc
<|entity> 00203|Euml
<|entity> 00204|Igrave
<|entity> 00205|Iacute
<|entity> 00206|Icirc
<|entity> 00207|Iuml
<|entity> 00208|ETH
<|entity> 00209|Ntilde
<|entity> 00210|Ograve
<|entity> 00211|Oacute
<|entity> 00212|Ocirc
<|entity> 00213|Otilde
<|entity> 00214|Ouml
<|entity> 00215|times
<|entity> 00216|Oslash
<|entity> 00217|Ugrave
<|entity> 00218|Uacute
<|entity> 00219|Ucirc
<|entity> 00220|Uuml
<|entity> 00221|Yacute
<|entity> 00222|THORN
<|entity> 00223|szlig
<|entity> 00224|agrave
<|entity> 00225|aacute
<|entity> 00226|acirc
<|entity> 00227|atilde
<|entity> 00228|auml
<|entity> 00229|aring
<|entity> 00230|aelig
<|entity> 00231|ccedil
<|entity> 00232|egrave
<|entity> 00233|eacute
<|entity> 00234|ecirc
<|entity> 00235|euml
<|entity> 00236|igrave
<|entity> 00237|iacute
<|entity> 00238|icirc
<|entity> 00239|iuml
<|entity> 00240|eth
<|entity> 00241|ntilde
<|entity> 00242|ograve
<|entity> 00243|oacute
<|entity> 00244|ocirc
<|entity> 00245|otilde
<|entity> 00246|ouml
<|entity> 00247|divide
<|entity> 00248|oslash
<|entity> 00249|ugrave
<|entity> 00250|uacute
<|entity> 00251|ucirc
<|entity> 00252|uuml
<|entity> 00253|yacute
<|entity> 00254|thorn
<|entity> 00255|yuml
<|entity> 00256|Amacr
<|entity> 00257|amacr
<|entity> 00258|Abreve
<|entity> 00259|abreve
<|entity> 00260|Aogon
<|entity> 00261|aogon
<|entity> 00262|Cacute
<|entity> 00263|cacute
<|entity> 00264|Ccirc
<|entity> 00265|ccirc
<|entity> 00266|Cdot
<|entity> 00267|cdot
<|entity> 00268|Ccaron
<|entity> 00269|ccaron
<|entity> 00270|Dcaron
<|entity> 00271|dcaron
<|entity> 00272|Dstrok
<|entity> 00273|dstrok
<|entity> 00274|Emacr
<|entity> 00275|emacr
<|entity> 00278|Edot
<|entity> 00279|edot
<|entity> 00280|Eogon
<|entity> 00281|eogon
<|entity> 00282|Ecaron
<|entity> 00283|ecaron
<|entity> 00284|Gcirc
<|entity> 00285|gcirc
<|entity> 00286|Gbreve
<|entity> 00287|gbreve
<|entity> 00288|Gdot
<|entity> 00289|gdot
<|entity> 00290|Gcedil
<|entity> 00292|Hcirc
<|entity> 00293|hcirc
<|entity> 00294|Hstrok
<|entity> 00295|hstrok
<|entity> 00296|Itilde
<|entity> 00297|itilde
<|entity> 00298|Imacr
<|entity> 00299|imacr
<|entity> 00302|Iogon
<|entity> 00303|iogon
<|entity> 00304|Idot
<|entity> 00305|inodot
<|entity> 00306|IJlig
<|entity> 00307|ijlig
<|entity> 00308|Jcirc
<|entity> 00309|jcirc
<|entity> 00310|Kcedil
<|entity> 00311|kcedil
<|entity> 00312|kgreen
<|entity> 00313|Lacute
<|entity> 00313|Lmidot
<|entity> 00314|lacute
<|entity> 00315|Lcedil
<|entity> 00316|lcedil
<|entity> 00317|Lcaron
<|entity> 00318|lcaron
<|entity> 00320|lmidot
<|entity> 00321|Lstrok
<|entity> 00322|lstrok
<|entity> 00323|Nacute
<|entity> 00324|nacute
<|entity> 00325|Ncedil
<|entity> 00326|ncedil
<|entity> 00327|Ncaron
<|entity> 00328|ncaron
<|entity> 00329|napos
<|entity> 00330|ENG
<|entity> 00331|eng
<|entity> 00332|Omacr
<|entity> 00333|omacr
<|entity> 00336|Odblac
<|entity> 00337|odblac
<|entity> 00338|OElig
<|entity> 00339|oelig
<|entity> 00340|Racute
<|entity> 00341|racute
<|entity> 00342|Rcedil
<|entity> 00343|rcedil
<|entity> 00344|Rcaron
<|entity> 00344|Sacute
<|entity> 00345|rcaron
<|entity> 00347|sacute
<|entity> 00348|scirc
<|entity> 00349|Scirc
<|entity> 00350|Scedil
<|entity> 00351|scedil
<|entity> 00352|Scaron
<|entity> 00353|scaron
<|entity> 00354|tcedil
<|entity> 00355|Tcedil
<|entity> 00356|Tcaron
<|entity> 00357|tcaron
<|entity> 00358|Tstrok
<|entity> 00359|tstrok
<|entity> 00360|Utilde
<|entity> 00361|utilde
<|entity> 00362|Umacr
<|entity> 00363|umacr
<|entity> 00364|Ubreve
<|entity> 00365|ubreve
<|entity> 00366|Uring
<|entity> 00367|uring
<|entity> 00368|Udblac
<|entity> 00369|udblac
<|entity> 00370|Uogon
<|entity> 00371|uogon
<|entity> 00372|Wcirc
<|entity> 00373|wcirc
<|entity> 00374|Ycirc
<|entity> 00375|ycirc
<|entity> 00376|Yuml
<|entity> 00377|Zacute
<|entity> 00378|zacute
<|entity> 00379|Zdot
<|entity> 00380|zdot
<|entity> 00382|Zcaron
<|entity> 00402|fnof
<|entity> 00501|gacute
<|entity> 00509|zcaron
<|entity> 00728|breve
<|entity> 00729|dot
<|entity> 00730|ring
<|entity> 00732|tilde
<|entity> 00733|dblac
<|entity> 00913|Agr
<|entity> 00913|Alpha
<|entity> 00914|Beta
<|entity> 00914|Bgr
<|entity> 00915|Gamma
<|entity> 00915|Ggr
<|entity> 00916|Dgr
<|entity> 00917|Egr
<|entity> 00917|Epsilon
<|entity> 00918|Zeta
<|entity> 00918|Zgr
<|entity> 00919|EEgr
<|entity> 00920|THgr
<|entity> 00920|Theta
<|entity> 00921|Igr
<|entity> 00921|Iota
<|entity> 00922|Kappa
<|entity> 00922|Kgr
<|entity> 00923|Lambda
<|entity> 00923|Lgr
<|entity> 00924|Mgr
<|entity> 00925|Ngr
<|entity> 00925|Nu
<|entity> 00926|Xgr
<|entity> 00926|Xi
<|entity> 00927|Ogr
<|entity> 00927|Omicron
<|entity> 00928|Pgr
<|entity> 00928|Pi
<|entity> 00929|Rgr
<|entity> 00929|Rho
<|entity> 00931|Sgr
<|entity> 00931|Sigma
<|entity> 00932|Tau
<|entity> 00932|Tgr
<|entity> 00933|Ugr
<|entity> 00933|Upsilon
<|entity> 00934|PHgr
<|entity> 00934|Phi
<|entity> 00935|Chi
<|entity> 00935|KHgr
<|entity> 00936|PSgr
<|entity> 00936|Psi
<|entity> 00937|OHgr
<|entity> 00937|Omega
<|entity> 00945|agr
<|entity> 00945|alpha
<|entity> 00946|beta
<|entity> 00946|bgr
<|entity> 00947|gamma
<|entity> 00947|ggr
<|entity> 00948|delta
<|entity> 00948|dgr
<|entity> 00949|egr
<|entity> 00949|epsilon
<|entity> 00950|zeta
<|entity> 00950|zgr
<|entity> 00951|eegr
<|entity> 00952|theta
<|entity> 00952|thgr
<|entity> 00953|igr
<|entity> 00953|iota
<|entity> 00954|kappa
<|entity> 00954|kgr
<|entity> 00955|lambda
<|entity> 00955|lgr
<|entity> 00956|mgr
<|entity> 00957|ngr
<|entity> 00957|nu
<|entity> 00958|xgr
<|entity> 00958|xi
<|entity> 00959|ogr
<|entity> 00959|omicron
<|entity> 00960|pgr
<|entity> 00960|pi
<|entity> 00961|rgr
<|entity> 00961|rho
<|entity> 00962|sfgr
<|entity> 00962|sigmaf
<|entity> 00963|sgr
<|entity> 00963|sigma
<|entity> 00964|tau
<|entity> 00964|tgr
<|entity> 00965|ugr
<|entity> 00965|upsilon
<|entity> 00966|phgr
<|entity> 00966|phi
<|entity> 00967|chi
<|entity> 00967|khgr
<|entity> 00968|psgr
<|entity> 00968|psi
<|entity> 00969|ohgr
<|entity> 00969|omega
<|entity> 00977|thetasym
<|entity> 00977|thetav
<|entity> 00978|upsih
<|entity> 00981|phiv
<|entity> 00982|piv
<|entity> 01025|IOcy
<|entity> 01026|DJcy
<|entity> 01027|GJcy
<|entity> 01028|Jukcy
<|entity> 01029|DScy
<|entity> 01030|Iukcy
<|entity> 01031|YIcy
<|entity> 01032|Jsercy
<|entity> 01033|LJcy
<|entity> 01034|NJcy
<|entity> 01035|TSHcy
<|entity> 01036|KJcy
<|entity> 01038|Ubrcy
<|entity> 01039|DZcy
<|entity> 01040|Acy
<|entity> 01041|Bcy
<|entity> 01042|Vcy
<|entity> 01043|Gcy
<|entity> 01044|Dcy
<|entity> 01045|IEcy
<|entity> 01046|ZHcy
<|entity> 01047|Zcy
<|entity> 01048|Icy
<|entity> 01049|Jcy
<|entity> 01050|Kcy
<|entity> 01051|Lcy
<|entity> 01052|Mcy
<|entity> 01053|Ncy
<|entity> 01054|Ocy
<|entity> 01055|Pcy
<|entity> 01056|Rcy
<|entity> 01057|Scy
<|entity> 01058|Tcy
<|entity> 01059|Ucy
<|entity> 01060|Fcy
<|entity> 01061|KHcy
<|entity> 01062|TScy
<|entity> 01063|CHcy
<|entity> 01064|SHcy
<|entity> 01065|SHCHcy
<|entity> 01066|Hardcy
<|entity> 01067|Ycy
<|entity> 01068|SOFTcy
<|entity> 01069|Ecy
<|entity> 01070|YUcy
<|entity> 01071|YAcy
<|entity> 01072|acy
<|entity> 01073|bcy
<|entity> 01074|vcy
<|entity> 01075|gcy
<|entity> 01076|dcy
<|entity> 01077|iecy
<|entity> 01078|zhcy
<|entity> 01079|zcy
<|entity> 01080|icy
<|entity> 01081|jcy
<|entity> 01082|kcy
<|entity> 01083|lcy
<|entity> 01084|mcy
<|entity> 01085|ncy
<|entity> 01086|ocy
<|entity> 01087|pcy
<|entity> 01088|rcy
<|entity> 01089|scy
<|entity> 01090|tcy
<|entity> 01091|ucy
<|entity> 01092|fcy
<|entity> 01093|khcy
<|entity> 01094|tscy
<|entity> 01095|chcy
<|entity> 01096|shcy
<|entity> 01097|chchcy
<|entity> 01098|hardcy
<|entity> 01099|ycy
<|entity> 01100|softcy
<|entity> 01101|ecy
<|entity> 01102|yucy
<|entity> 01103|yacy
<|entity> 01105|iocy
<|entity> 01106|djcy
<|entity> 01107|gjcy
<|entity> 01108|jukcy
<|entity> 01109|dscy
<|entity> 01110|iukcy
<|entity> 01111|yicy
<|entity> 01112|jsercy
<|entity> 01113|licy
<|entity> 01114|njcy
<|entity> 01115|tshcy
<|entity> 01116|kjcy
<|entity> 01118|ubrcy
<|entity> 01119|dzcy
@- <|entity> 01168|0490
@- <|entity> 01169|0491
<|entity> 08194|ensp
<|entity> 08195|emsp
<|entity> 08196|emsp13
<|entity> 08197|emsp14
<|entity> 08199|numsp
<|entity> 08200|puncsp
<|entity> 08201|thinsp
<|entity> 08202|hairsp
<|entity> 08204|zwnj
<|entity> 08205|zwj
<|entity> 08206|lrm
<|entity> 08207|rlm
<|entity> 08208|dash
@- <|entity> 08209|2011
<|entity> 08211|endash
<|entity> 08211|ndash
<|entity> 08212|emdash
<|entity> 08212|mdash
<|entity> 08213|horbar
<|entity> 08214|Verbar
<|entity> 08216|lsquo
<|entity> 08217|rsquo
<|entity> 08217|rsquor
<|entity> 08218|lsquor
<|entity> 08220|ldquo
<|entity> 08221|ddquo
<|entity> 08221|rdquo
<|entity> 08221|rdquor
<|entity> 08222|bdquo
<|entity> 08222|ldquor
<|entity> 08224|dagger
<|entity> 08225|Dagger
<|entity> 08226|bull
<|entity> 08229|mldr
<|entity> 08229|nldr
<|entity> 08230|hellip
@- <|entity> 08231|2027
<|entity> 08233|pgfsep
<|entity> 08240|permil
<|entity> 08242|prime
<|entity> 08243|Prime
<|entity> 08244|tprime
@- <|entity> 08249|2039
<|entity> 08249|lsaquo
@- <|entity> 08250|203A
<|entity> 08250|rsaquo
<|entity> 08254|oline
<|entity> 08254|radx
<|entity> 08257|caret
<|entity> 08259|hybull
@- <|entity> 08260|2044
<|entity> 08260|frasl
<|entity> 08364|euro
<|entity> 08411|tdot
<|entity> 08412|DotDot
<|entity> 08453|incare
<|entity> 08459|hamilt
<|entity> 08465|image
<|entity> 08466|lagran
<|entity> 08471|copysr
<|entity> 08472|weierp
<|entity> 08476|real
<|entity> 08478|rx
<|entity> 08482|trade
<|entity> 08486|ohm
<|entity> 08491|angst
<|entity> 08492|bernou
<|entity> 08499|phmmat
<|entity> 08500|order
<|entity> 08501|alefsym
<|entity> 08501|aleph
<|entity> 08531|frac13
<|entity> 08532|frac23
<|entity> 08533|frac15
<|entity> 08534|frac25
<|entity> 08535|frac35
<|entity> 08536|frac45
<|entity> 08537|frac16|SUIS
<|entity> 08538|frac56|SUIS
<|entity> 08539|frac18
<|entity> 08540|frac38
<|entity> 08541|frac58
<|entity> 08542|frac78
<|entity> 08592|larr|SUIS
<|entity> 08593|uarr|SUIS
<|entity> 08594|rarr|SUIS
<|entity> 08595|darr|SUIS
<|entity> 08596|harr|SUIS
<|entity> 08629|crarr|SUIS
<|entity> 08629|cret|SUIS
<|entity> 08656|lArr|SUIS
<|entity> 08657|uArr|SUIS
<|entity> 08658|rArr|SUIS
<|entity> 08659|dArr|SUIS
<|entity> 08660|hArr|SUIS
<|entity> 08660|iff|SUIS
<|entity> 08704|forall|SUIS
<|entity> 08706|part|SUIS
<|entity> 08707|exist|SUIS
<|entity> 08709|empty|SUIS
<|entity> 08710|Delta|SUIS
<|entity> 08711|nabla|SUIS
<|entity> 08712|isin|SUIS
<|entity> 08713|notin|SUIS
<|entity> 08715|ni|SUIS
<|entity> 08719|prod|SUIS
<|entity> 08721|sum|SUIS
<|entity> 08722|minus|SUIS
<|entity> 08723|mnplus|SUIS
<|entity> 08725|divslash|SUIS
<|entity> 08727|astmath|SUIS
<|entity> 08727|lowast|SUIS
<|entity> 08728|compfn|SUIS
<|entity> 08730|radic|SUIS
<|entity> 08733|prop|SUIS
<|entity> 08734|infin|SUIS
<|entity> 08735|ang90|SUIS
<|entity> 08736|ang|SUIS
<|entity> 08738|angsph|SUIS
<|entity> 08741|par|SUIS
<|entity> 08743|and|SUIS
<|entity> 08744|or|SUIS
<|entity> 08745|cap|SUIS
<|entity> 08746|cup|SUIS
<|entity> 08747|int|SUIS
<|entity> 08750|conint|SUIS
<|entity> 08756|there4|SUIS
<|entity> 08757|becaus|SUIS
<|entity> 08764|sim|SUIS
<|entity> 08764|thksim|SUIS
<|entity> 08771|sime|SUIS
<|entity> 08773|cong|SUIS
<|entity> 08776|asymp|SUIS
<|entity> 08776|thkap|SUIS
<|entity> 08777|ap|SUIS
<|entity> 08793|wedgeq|SUIS
<|entity> 08800|ne|SUIS
<|entity> 08801|equiv|SUIS
<|entity> 08804|le|SUIS
<|entity> 08804|les|SUIS
<|entity> 08805|ge|SUIS
<|entity> 08834|sub|SUIS
<|entity> 08835|sup|SUIS
<|entity> 08836|nsub|SUIS
<|entity> 08838|sube|SUIS
<|entity> 08839|supe|SUIS
<|entity> 08853|oplus|SUIS
<|entity> 08855|otimes|SUIS
<|entity> 08869|bottom|SUIS
<|entity> 08869|perp|SUIS
<|entity> 08901|sdot|SUIS
<|entity> 08942|vellip|SUIS
<|entity> 08968|lceil|SUIS
<|entity> 08969|rceil|SUIS
<|entity> 08970|lfloor|SUIS
<|entity> 08971|rfloor|SUIS
<|entity> 08972|drcrop|SUIS
<|entity> 08973|dlcrop|SUIS
<|entity> 08974|urcrop|SUIS
<|entity> 08975|ulcrop|SUIS
<|entity> 08981|telrec|SUIS
<|entity> 08982|target|SUIS
<|entity> 09001|lang|SUIS
<|entity> 09002|rang|SUIS
<|entity> 09251|blank|SUIS
<|entity> 09600|uhblk|SUIS
<|entity> 09604|lhblk|SUIS
<|entity> 09608|block|SUIS
<|entity> 09612|dtrif|SUIS
<|entity> 09617|blk14|SUIS
<|entity> 09618|blk12|SUIS
<|entity> 09619|blk34|SUIS
<|entity> 09632|squf|SUIS
<|entity> 09633|squ|SUIS
<|entity> 09633|square|SUIS
<|entity> 09645|rect|SUIS
<|entity> 09646|marker|SUIS
<|entity> 09650|utrif|SUIS
<|entity> 09653|utri|SUIS
<|entity> 09656|rtrif|SUIS
<|entity> 09657|rtri|SUIS
<|entity> 09663|dtri|SUIS
<|entity> 09666|ltrif|SUIS
<|entity> 09667|ltri|SUIS
<|entity> 09674|loz|SUIS
<|entity> 09675|cir|SUIS
<|entity> 09733|starf|SUIS
<|entity> 09734|star|SUIS
<|entity> 09742|phone|SUIS
<|entity> 09792|female|SUIS
<|entity> 09794|male|SUIS
<|entity> 09824|spades|SUIS
<|entity> 09827|clubs|SUIS
<|entity> 09829|heart|SUIS
<|entity> 09829|hearts|SUIS
<|entity> 09830|diamond|SUIS
<|entity> 09830|diams|SUIS
<|entity> 09834|sung|SUIS
<|entity> 09837|flat|SUIS
<|entity> 09838|natur|SUIS
<|entity> 09839|sharp|SUIS
<|entity> 10003|check|SUIS
<|entity> 10007|cross|SUIS
<|entity> 10016|malt|SUIS
<|entity> 10022|lozf|SUIS
<|entity> 10038|sext|SUIS
<|entity> 10038|sextile|SUIS
@- <|entity> 126  See U732 */|/* addCharEnt("tilde
<|entity> 64256|fflig
<|entity> 64257|filig
<|entity> 64258|fllig
<|entity> 64259|ffilig
<|entity> 64260|ffllig
@- <|entity> 8482a|tradeser
@- <|entity> 8482b|tradesan
@- <|entity> 8594a|rarrZ
@- <|entity> 8596a|harrZ
</Tbl>
