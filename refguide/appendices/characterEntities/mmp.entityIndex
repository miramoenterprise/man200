@-----------------------------------------------------------
@- mmp.entityIndex
@------------------------------------------------------------
<@include> ${CONTROL}/mmp.formatdef.idx_toc
@------------------------------------------------------------
<#def> entityIndex_ColGap	8
<#def> specialIndexNumCols	4
<#def> columnHeadingsTop	19	@--- 'T' val for index column headings
<#def> entityIndexHH		07	@--- Columm header column height
<#def> CTH			<#columnHeadingsTop>
<#def> entityIndexDS		6.5
<#def> FirstDip	25
<#def> firstDip	55
<#def> LIndent	0
<#def> RIndent	0
@- <#def> IBRPLM	@eval(<#RPLM> + <#LIndent>)@- Right page left margin
@- <#def> IBLPLM	@eval(<#LPLM> + <#LIndent>)@- Left page left margin
@- <#def> IBTW	@eval(<#TW> - <#LIndent> - <#RIndent>)@- Body text width
<#def> six.colWidth		@eval((<#TW> - <#entityIndex_ColGap>) / 2)
<#def> ThirdTab			<#six.colWidth>
@------------------------------------------------------------
	@----------------------------------------------------
<@xmacro> indexColHeads		{
	@----------------------------------------------------
	@--- Number of columns
	@--- Header column widths
	@--- Header column heading text
	@--- Body column tab positions
	@--- Body column tab alignments
	@----------------------------------------------------
	<#def> ich.numColumns	4
	<#def> ich.colWidths	"15.0|12.0|14.0"	@--- Values are mm
	<#def> ich.colHeads	"Entity|Unicode|Glyph|Page"
	<#def> ich.tabPositions	"15.0|12.0|14.0"	@--- Values are mm
	<#def> ich.tabAlign	"15.0|12.0|14.0"	@--- 
	@----------------------------------------------------
	@xgetvals(ich.)
	@----------------------------------------------------
}
<#def> sixCW[01]	15.0
<#def> sixCW[02]	12.0

<#def> sixCW[03]	14.0
<#def> sixCW[04]	@eval(
					<#six.colWidth>
					- (
						$sixCW[01]
						+ $sixCW[02]
						+ $sixCW[03]
						)
					)

<#def> sixTabPos[01]	$sixCW[01]
<#def> sixTabPos[02]	@eval(
					$sixCW[01]
					+ $sixCW[02]
					@--- + ($sixCW[03] / 2)
					+ 3
					)
@------------------------------------------------------------
@-===========================================================
<FontDef fmt=i 	@- Italics
	fa=Italic
/>
@-===========================================================
<ParaDef fmt=P_GenTitle 	@- Title of generated documents
	an=N
	p=22pt
	A=L
	fs=90
	ff=Frutiger
	fv=Regular
	fw=Bold
	fa=Regular
/>
@----------------------------------
<ParaDef fmt=GroupTitlesIX	@- eg. `Specials', `A', `B', ... `Z'
	an=N
	sa=6pt
	sb=0
	wn=Y
	p=9pt
	l=2pt
	fs=90
	A=L
	ff=Frutiger
	fv=Regular
	fw=Bold
	fa=Regular
/>
@----------------------------------
<ParaDef fmt=Level1IX	@- 
	an=N
	sa=0.2pt
	sb=0pt
	p=9pt
	wn=N
	l=1.0pt
	ls=F
	A=L
	ff=Frutiger
	fw=Light
>
<TabStops>
	<@macro> entityIndexTabs {
		<TabStop A=L pos=$sixTabPos[01]mm />
		<TabStop A=C pos=$sixTabPos[02]mm /> 
		<TabStop A=R pos=<#ThirdTab>mm leader=" "/>
}
<|entityIndexTabs>
</TabStops>
</ParaDef>
@----------------------------------
<ParaDef fmt="charEntityIX"	@- 
	an=N
	sa=0pt
	sb=0pt
	p=9pt
	l=2pt
	wn=N
	A=L
	ff=TimesNewRoman
	fw=Regular
/>
@----------------------------------
<ParaDef fmt="CharCodesIX"	@- 
	an=N
	sa=0pt
	sb=0pt
	p=9pt
	l=2pt
	wn=N
	A=L
	ff=TimesNewRoman
	fw=Regular
/>
@----------------------------------
<ParaDef fmt="Type 12IX"	@- 
	an=N
	sa=0pt
	sb=0pt
	p=9pt
	l=2pt
	wn=N
	A=L
	ff=TimesNewRoman
	fw=Regular
/>
@----------------------------------
@-------------------------------------------------------------
<@macro> entityIndex_TblColWidths {
<TblColWidth W=$sixCW[01]mm />
<TblColWidth W=$sixCW[02]mm />

<TblColWidth W=$sixCW[03]mm />
<TblColWidth W=$sixCW[04]mm />
}
@-------------------------------------------------------------
<#def> sixIndex_tbl_head {
<P fmt=P_Body P=P p=2 sb=-2pt wp=N >
<Tbl fmt=T_CJKchars sa=-2pt sb=-5pt ctm=0.5 cbm=0.5 clm=0pt crm=0pt warn=N >
<|entityIndex_TblColWidths>
<Row>
<Cell cs=1 Ca=T >
<P fmt=P_CJKchars_RowNo A=L p=6.0 >
Entity
<Cell fmt=P_CJKchars_RowNo cs=1 A=L Ca=T p=6.0 >
Unicode
<Cell fmt=P_CJKchars_RowNo cs=1 A=L Ca=T p=6.0 >
Glyph
<Cell fmt=P_CJKchars_RowNo cs=1 A=R Ca=T p=6.0 >
Page
</Row>
</Tbl>
}
@-------------------------------------------------------------
<@macro> entityIndex_page_headers  {
	@----------------------------------------------------
	@---- Left Page Headers
	@----------------------------------------------------
	<#def> pageSide		$1
	<#def> entityHeadDelta	0
	@if(<#pageSide> = Right OR <#pageSide> = First) {
		<#def> eIPH.L1	<#RPLM>mm
		<#def> eIPH.L2	@eval(<#RPLM> + <#FList_tblwidth> + <#entityIndex_ColGap>)mm
		}
	@if(<#pageSide> = Left) {
		<#def> eIPH.L1	<#LPLM>mm
		<#def> eIPH.L2	@eval(<#LPLM> + <#FList_tblwidth> + <#entityIndex_ColGap>)mm
		}
	@if(<#pageSide> = First) {
		<#def> entityHeadDelta	<#firstDip>
		}
	<TextFrameDef type=B
		L=<#eIPH.L1>
		T=@eval(<#CTH> + <#entityHeadDelta>)
		W=<#FList_tblwidth>mm
		H=<#entityIndexHH>mm <#ColDef_opts> cn=1 pen=15  >
		<#sixIndex_tbl_head>
	</TextFrameDef>
	<TextFrameDef type=B
		@- L=@eval(<#LPLM> + <#FList_tblwidth> + <#entityIndex_ColGap>)mm
		L=<#eIPH.L2>
		T=@eval(<#CTH> + <#entityHeadDelta>)
		W=<#FList_tblwidth>mm
		H=<#entityIndexHH>mm <#ColDef_opts> cn=1 pen=15  >
		<#sixIndex_tbl_head>
</TextFrameDef>
}
@------------------------------------------------------------
<@macro> entityIndexBodyColumns       {
	<#def> eiDS	<#entityIndexDS>
	<#def> pageSide		$1
	@if(<#pageSide> = Right) {
		<#def> eIBC.L	<#RPLM>mm
		<#def> eIBC.T	@eval(<#BTM> + <#entityIndexDS>)mm
		}
	@if(<#pageSide> = Left) {
		<#def> eIBC.L	<#LPLM>mm
		<#def> eIBC.T	@eval(<#LBTM> + <#entityIndexDS>)mm
		}
	@if(<#pageSide> = First) {
		<#def> eiDS		@eval(<#entityIndexDS> + <#firstDip>)
		<#def> eIBC.L		<#RPLM>mm
		<#def> eIBC.T		@eval(<#BTM> + <#eiDS>)mm
		@--------------------------------------------
		@-- <#def> TY	@eval(<#BTM> + <#FirstDip>)
		@-- <#def> IxH	@eval(<#BH> - <#FirstDip>)
		@--- Header text frame (to contain index title
		@--- and other text) AND first page index body frame.
		<TextFrameDef type=F
			L=<#RPLM>mm
			T=<#TM>mm
			W=<#TW>mm
			H=@eval(<#HH> + <#firstDip>)mm
			fill=15 >
		<NextTextFrameDef
			L=<#eIBC.L>
			T=<#eIBC.T>
			W=<#TW>mm
			H=@eval(<#BH> - <#eiDS>)mm
			<#ColDef_opts> cb=Y cn=2 cg=<#entityIndex_ColGap>mm
			/>
		</TextFrameDef>
		@--------------------------------------------
		}
	@- @if(<#do_FList_page_headers>) {
		@--- Defined in file: mmp.unilist_def, in 'control' directory
		<|entityIndex_page_headers> <#pageSide>
	@- 	}
	@if(<#pageSide> = Right OR <#pageSide> = Left ) {
		<TextFrameDef
			L=<#eIBC.L>
			T=<#eIBC.T>
			W=<#TW>mm
			H=@eval(<#BH> - <#eiDS>)mm
			<#ColDef_opts> cb=Y cn=2 cg=<#entityIndex_ColGap>mm >
		</TextFrameDef>
		}
}
@-------------------------------------------------------------
<@macro> doindex {
<PageDef fmt=IX type=R >
	<TextFrameDef flow=IX L=<#LPLM>mm T=22mm W=<#TW>mm H=120mm fill=15 >
		<P fmt=Level1IX >
<#genPageNum>
		<P fmt="Type 12IX" >
	<#genPageNum>

@-		<P fmt="CharCodesIX" >
@-	<#genPageNum>

		<P fmt="charEntityIX" >
	<#genPageNum>

		<P fmt=SeparatorsIX >
 1, 2-3

		<P fmt=ActiveIX >
openObjectId <relfilename/>:<ObjectType/> <ObjectId/>

		<P fmt=GroupTitlesIX >
In-text codes[\ ];Numerics[0];A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z
		<P fmt=SortOrderIX >
<symbols/><numerics/><alphabetics/>

		<P fmt=IgnoreCharsIX >
----

 	</TextFrameDef>
</PageDef>
}
@------------------------------------------------------------
<@macro> GenIndexFirst_page	{
	<PageDef fmt=entityIndex_First >
		<|entityIndexBodyColumns> First
		<|RFooterCol>
		<verticalRule pageSide="R" topDip="<#firstDip>" />
	</PageDef>
}
@-------------------------------------------------------------
<@macro> GenIndexRight_page	{
	<PageDef fmt=Right >
		<|Legend> Right
		<|TopRule> Right
		<|RHeaderCol>
		<|entityIndexBodyColumns> Right
		<|RFooterCol>
		@- <|FList_Vertical_Rule> R
		<verticalRule pageSide="R" />
		</PageDef>
}
@-------------------------------------------------------------
<@macro> GenIndexLeft_page	{
	<PageDef fmt=Left >
		<|Legend> Left
		@--- <|ChsIndexCols> Left
		<|TopRule> Left
		<|LHeaderCol>
		@- <|LfontListSubHead>
		<|entityIndexBodyColumns> Left
		<|LFooterCol>
		@- <|FList_Vertical_Rule> L
		<verticalRule pageSide="L" />
	</PageDef>
}
@-------------------------------------------------------------
@------------------------------------------------------------
<|GenIndexFirst_page>
<|GenIndexRight_page>
<|GenIndexLeft_page>
