#include <stdio.h>
#include "./phashFR.h"
#include "./fmchars.h"
#include "../utils/memory.h"

fmCharsHandle *
initUnicodeMapFR(void)
{
	fmCharsHandle *
		UnicodeMapFR=(fmCharsHandle *)smalloc(sizeof(fmCharsHandle) * PHASHNKEYS_phashFR);

	/*  VERSION 7.3.0p72: the characters b0 (degree) b1 (plus minus) b5 (micro) */
	/*  d7 (multiplication) f7 (division) are all available in Windows codepage */
	/*  1252, so we don't need to map to Symbol font for Windows. Changed */
	/*  mappings accordingly (not sure why they were ever mapped??) */
	/*  ------	 8232 hard return  */
	*(UnicodeMapFR+phashFR(10)) = newFmChars("\\r", 0, 10); 
	*(UnicodeMapFR+phashFR(8232)) = newFmChars("\\r", 0, 8232); 
	/*  ------	 9 tab  */
	*(UnicodeMapFR+phashFR(9)) = newFmChars("\\x08 ", 0, 9); 
	/*  ------	 Unicode numbers 32 [space] to 125 [}] map directly onto fmhex  */
	/*  ------	 VERSION 7.2.0p40: map tilde (126) onto \\x7e */
	*(UnicodeMapFR+phashFR(32)) = newFmChars("\\x20 ", 0, 32); 
	*(UnicodeMapFR+phashFR(33)) = newFmChars("\\x21 ", 0, 33); 
	*(UnicodeMapFR+phashFR(34)) = newFmChars("\\x22 ", 0, 34); 
	*(UnicodeMapFR+phashFR(35)) = newFmChars("\\x23 ", 0, 35); 
	*(UnicodeMapFR+phashFR(36)) = newFmChars("\\x24 ", 0, 36); 
	*(UnicodeMapFR+phashFR(37)) = newFmChars("\\x25 ", 0, 37); 
	*(UnicodeMapFR+phashFR(38)) = newFmChars("\\x26 ", 0, 38); 
	*(UnicodeMapFR+phashFR(39)) = newFmChars("\\x27 ", 0, 39); 
	*(UnicodeMapFR+phashFR(40)) = newFmChars("\\x28 ", 0, 40); 
	*(UnicodeMapFR+phashFR(41)) = newFmChars("\\x29 ", 0, 41); 
	*(UnicodeMapFR+phashFR(42)) = newFmChars("\\x2a ", 0, 42); 
	*(UnicodeMapFR+phashFR(43)) = newFmChars("\\x2b ", 0, 43); 
	*(UnicodeMapFR+phashFR(44)) = newFmChars("\\x2c ", 0, 44); 
	*(UnicodeMapFR+phashFR(45)) = newFmChars("\\x2d ", 0, 45); 
	*(UnicodeMapFR+phashFR(46)) = newFmChars("\\x2e ", 0, 46); 
	*(UnicodeMapFR+phashFR(47)) = newFmChars("\\x2f ", 0, 47); 
	*(UnicodeMapFR+phashFR(48)) = newFmChars("\\x30 ", 0, 48); 
	*(UnicodeMapFR+phashFR(49)) = newFmChars("\\x31 ", 0, 49); 
	*(UnicodeMapFR+phashFR(50)) = newFmChars("\\x32 ", 0, 50); 
	*(UnicodeMapFR+phashFR(51)) = newFmChars("\\x33 ", 0, 51); 
	*(UnicodeMapFR+phashFR(52)) = newFmChars("\\x34 ", 0, 52); 
	*(UnicodeMapFR+phashFR(53)) = newFmChars("\\x35 ", 0, 53); 
	*(UnicodeMapFR+phashFR(54)) = newFmChars("\\x36 ", 0, 54); 
	*(UnicodeMapFR+phashFR(55)) = newFmChars("\\x37 ", 0, 55); 
	*(UnicodeMapFR+phashFR(56)) = newFmChars("\\x38 ", 0, 56); 
	*(UnicodeMapFR+phashFR(57)) = newFmChars("\\x39 ", 0, 57); 
	*(UnicodeMapFR+phashFR(58)) = newFmChars("\\x3a ", 0, 58); 
	*(UnicodeMapFR+phashFR(59)) = newFmChars("\\x3b ", 0, 59); 
	*(UnicodeMapFR+phashFR(60)) = newFmChars("\\x3c ", 0, 60); 
	*(UnicodeMapFR+phashFR(61)) = newFmChars("\\x3d ", 0, 61); 
	*(UnicodeMapFR+phashFR(62)) = newFmChars("\\x3e ", 0, 62); 
	*(UnicodeMapFR+phashFR(63)) = newFmChars("\\x3f ", 0, 63); 
	*(UnicodeMapFR+phashFR(64)) = newFmChars("\\x40 ", 0, 64); 
	*(UnicodeMapFR+phashFR(65)) = newFmChars("\\x41 ", 0, 65); 
	*(UnicodeMapFR+phashFR(66)) = newFmChars("\\x42 ", 0, 66); 
	*(UnicodeMapFR+phashFR(67)) = newFmChars("\\x43 ", 0, 67); 
	*(UnicodeMapFR+phashFR(68)) = newFmChars("\\x44 ", 0, 68); 
	*(UnicodeMapFR+phashFR(69)) = newFmChars("\\x45 ", 0, 69); 
	*(UnicodeMapFR+phashFR(70)) = newFmChars("\\x46 ", 0, 70); 
	*(UnicodeMapFR+phashFR(71)) = newFmChars("\\x47 ", 0, 71); 
	*(UnicodeMapFR+phashFR(72)) = newFmChars("\\x48 ", 0, 72); 
	*(UnicodeMapFR+phashFR(73)) = newFmChars("\\x49 ", 0, 73); 
	*(UnicodeMapFR+phashFR(74)) = newFmChars("\\x4a ", 0, 74); 
	*(UnicodeMapFR+phashFR(75)) = newFmChars("\\x4b ", 0, 75); 
	*(UnicodeMapFR+phashFR(76)) = newFmChars("\\x4c ", 0, 76); 
	*(UnicodeMapFR+phashFR(77)) = newFmChars("\\x4d ", 0, 77); 
	*(UnicodeMapFR+phashFR(78)) = newFmChars("\\x4e ", 0, 78); 
	*(UnicodeMapFR+phashFR(79)) = newFmChars("\\x4f ", 0, 79); 
	*(UnicodeMapFR+phashFR(80)) = newFmChars("\\x50 ", 0, 80); 
	*(UnicodeMapFR+phashFR(81)) = newFmChars("\\x51 ", 0, 81); 
	*(UnicodeMapFR+phashFR(82)) = newFmChars("\\x52 ", 0, 82); 
	*(UnicodeMapFR+phashFR(83)) = newFmChars("\\x53 ", 0, 83); 
	*(UnicodeMapFR+phashFR(84)) = newFmChars("\\x54 ", 0, 84); 
	*(UnicodeMapFR+phashFR(85)) = newFmChars("\\x55 ", 0, 85); 
	*(UnicodeMapFR+phashFR(86)) = newFmChars("\\x56 ", 0, 86); 
	*(UnicodeMapFR+phashFR(87)) = newFmChars("\\x57 ", 0, 87); 
	*(UnicodeMapFR+phashFR(88)) = newFmChars("\\x58 ", 0, 88); 
	*(UnicodeMapFR+phashFR(89)) = newFmChars("\\x59 ", 0, 89); 
	*(UnicodeMapFR+phashFR(90)) = newFmChars("\\x5a ", 0, 90); 
	*(UnicodeMapFR+phashFR(91)) = newFmChars("\\x5b ", 0, 91); 
	*(UnicodeMapFR+phashFR(92)) = newFmChars("\\x5c ", 0, 92); 
	*(UnicodeMapFR+phashFR(93)) = newFmChars("\\x5d ", 0, 93); 
	*(UnicodeMapFR+phashFR(94)) = newFmChars("\\x5e ", 0, 94); 
	*(UnicodeMapFR+phashFR(95)) = newFmChars("\\x5f ", 0, 95); 
	*(UnicodeMapFR+phashFR(96)) = newFmChars("\\x60 ", 0, 96); 
	*(UnicodeMapFR+phashFR(97)) = newFmChars("\\x61 ", 0, 97); 
	*(UnicodeMapFR+phashFR(98)) = newFmChars("\\x62 ", 0, 98); 
	*(UnicodeMapFR+phashFR(99)) = newFmChars("\\x63 ", 0, 99); 
	*(UnicodeMapFR+phashFR(100)) = newFmChars("\\x64 ", 0, 100); 
	*(UnicodeMapFR+phashFR(101)) = newFmChars("\\x65 ", 0, 101); 
	*(UnicodeMapFR+phashFR(102)) = newFmChars("\\x66 ", 0, 102); 
	*(UnicodeMapFR+phashFR(103)) = newFmChars("\\x67 ", 0, 103); 
	*(UnicodeMapFR+phashFR(104)) = newFmChars("\\x68 ", 0, 104); 
	*(UnicodeMapFR+phashFR(105)) = newFmChars("\\x69 ", 0, 105); 
	*(UnicodeMapFR+phashFR(106)) = newFmChars("\\x6a ", 0, 106); 
	*(UnicodeMapFR+phashFR(107)) = newFmChars("\\x6b ", 0, 107); 
	*(UnicodeMapFR+phashFR(108)) = newFmChars("\\x6c ", 0, 108); 
	*(UnicodeMapFR+phashFR(109)) = newFmChars("\\x6d ", 0, 109); 
	*(UnicodeMapFR+phashFR(110)) = newFmChars("\\x6e ", 0, 110); 
	*(UnicodeMapFR+phashFR(111)) = newFmChars("\\x6f ", 0, 111); 
	*(UnicodeMapFR+phashFR(112)) = newFmChars("\\x70 ", 0, 112); 
	*(UnicodeMapFR+phashFR(113)) = newFmChars("\\x71 ", 0, 113); 
	*(UnicodeMapFR+phashFR(114)) = newFmChars("\\x72 ", 0, 114); 
	*(UnicodeMapFR+phashFR(115)) = newFmChars("\\x73 ", 0, 115); 
	*(UnicodeMapFR+phashFR(116)) = newFmChars("\\x74 ", 0, 116); 
	*(UnicodeMapFR+phashFR(117)) = newFmChars("\\x75 ", 0, 117); 
	*(UnicodeMapFR+phashFR(118)) = newFmChars("\\x76 ", 0, 118); 
	*(UnicodeMapFR+phashFR(119)) = newFmChars("\\x77 ", 0, 119); 
	*(UnicodeMapFR+phashFR(120)) = newFmChars("\\x78 ", 0, 120); 
	*(UnicodeMapFR+phashFR(121)) = newFmChars("\\x79 ", 0, 121); 
	*(UnicodeMapFR+phashFR(122)) = newFmChars("\\x7a ", 0, 122); 
	*(UnicodeMapFR+phashFR(123)) = newFmChars("\\x7b ", 0, 123); 
	*(UnicodeMapFR+phashFR(124)) = newFmChars("\\x7c ", 0, 124); 
	*(UnicodeMapFR+phashFR(125)) = newFmChars("\\x7d ", 0, 125); 
	*(UnicodeMapFR+phashFR(126)) = newFmChars("\\x7e ", 0, 126); 
	/*  ------	 126 007E tilde */
	/*  ------	 VERSION 7.2.0p40: remove tilde mapping  */
	/*  126	"\\xf7 " - */
	/*  ------	 160 00A0 non-breaking space  */
	*(UnicodeMapFR+phashFR(160)) = newFmChars("\\x11 ", 0, 160); 
	/*  ------	 161 00A1 inverted exclamation mark  */
	*(UnicodeMapFR+phashFR(161)) = newFmChars("\\xc1 ", 0, 161); 
	/*  ------	 162 00A2 cent sign  */
	*(UnicodeMapFR+phashFR(162)) = newFmChars("\\xa2 ", 0, 162); 
	/*  ------	 163 00A3 pound sign  */
	*(UnicodeMapFR+phashFR(163)) = newFmChars("\\xa3 ", 0, 163); 
	/*  ------	 164 00A4 currency sign  */
	*(UnicodeMapFR+phashFR(164)) = newFmChars("\\xdb ", 0, 164); 
	/*  ------	 165 00A5 yen sign  */
	*(UnicodeMapFR+phashFR(165)) = newFmChars("\\xb4 ", 0, 165); 
	/*  ------	 166 00A6 broken vertical bar  */
	*(UnicodeMapFR+phashFR(166)) = newFmChars("\\xad ", 0, 166); 
	/*  ------	 167 00A7 section sign  */
	*(UnicodeMapFR+phashFR(167)) = newFmChars("\\xa4 ", 0, 167); 
	/*  ------	 168 00A8 spacing diaeresis  */
	*(UnicodeMapFR+phashFR(168)) = newFmChars("\\xac ", 0, 168); 
	/*  ------	 169 00A9 copyright sign  */
	*(UnicodeMapFR+phashFR(169)) = newFmChars("\\xa9 ", 0, 169); 
	/*  ------	 170 00AA feminine ordinal indicator  */
	*(UnicodeMapFR+phashFR(170)) = newFmChars("\\xbb ", 0, 170); 
	/*  ------	 171 00AB left pointing guillemet  */
	*(UnicodeMapFR+phashFR(171)) = newFmChars("\\xc7 ", 0, 171); 
	/*  ------	 172 00AC not sign  */
	*(UnicodeMapFR+phashFR(172)) = newFmChars("\\xc2 ", 0, 172); 
	/*  ------	 173 00AD soft hyphen  */
	/*  VERSION 7.1.0p75: map #xAD onto Frame character \x04 (discretionary */
	/*  hyphen) rather than \x06 (Unix automatic hyphen) */
	*(UnicodeMapFR+phashFR(173)) = newFmChars("\\x04 ", 0, 173); 
	/*  ------	 174 00AE registered trade mark sign  */
	*(UnicodeMapFR+phashFR(174)) = newFmChars("\\xa8 ", 0, 174); 
	/*  ------	 175 00AF spacing macron  */
	*(UnicodeMapFR+phashFR(175)) = newFmChars("\\xf8 ", 0, 175); 
	/*  ------	 176 00B0 degree sign  */
	/*  VERSION 6.2.0p02: apply mapping for Unix only */
	/*  VERSION 6.2.0p03: apply mapping for Unix AND Windows only */
	/*  VERSION 7.3.0p72: don't map b0 (degree) to Symbol font for Windows */
#ifndef WIN32
	*(UnicodeMapFR+phashFR(176)) = newFmChars("\\xb0 ", 1, 176); 
#endif
	/*  VERSION 8.1.0p12: map b0 (degree) to \xbc for Windows, not \xb0 */
#ifdef WIN32
	*(UnicodeMapFR+phashFR(176)) = newFmChars("\\xbc ", 0, 176); 
#endif
	/*  ------	 177 00B1 plus-or-minus sign  */
	/*  VERSION 6.2.0p02: apply mapping for Unix only */
#ifndef WIN32
	*(UnicodeMapFR+phashFR(177)) = newFmChars("\\xb1 ", 1, 177); 
#endif
#ifdef WIN32
	*(UnicodeMapFR+phashFR(177)) = newFmChars("\\xb1 ", 0, 177); 
#endif
	/*  ------	 178 00B2 superscript digit two  */
	*(UnicodeMapFR+phashFR(178)) = newFmChars("\\xb7 ", 0, 178); 
	/*  ------	 179 00B3 superscript digit three  */
	*(UnicodeMapFR+phashFR(179)) = newFmChars("\\xb8 ", 0, 179); 
	/*  ------	 180 00B4 spacing acute  */
	*(UnicodeMapFR+phashFR(180)) = newFmChars("\\xab ", 0, 180); 
	/*  ------	 181 00B5 micro sign  */
	/*  VERSION 6.2.0p02: remove mapping for b5 */
	/*  VERSION 6.2.0p04: re-add mapping for b5 */
	/*  VERSION 7.3.0p72: don't map b5 (micro) to Symbol font for Windows */
#ifndef WIN32
	*(UnicodeMapFR+phashFR(181)) = newFmChars("\\x6d ", 1, 181); 
#endif
#ifdef WIN32
	*(UnicodeMapFR+phashFR(181)) = newFmChars("\\xb5 ", 0, 181); 
#endif
	/*  ------	 182 00B6 paragraph sign  */
	*(UnicodeMapFR+phashFR(182)) = newFmChars("\\xa6 ", 0, 182); 
	/*  ------	 183 00B7 middle dot  */
	*(UnicodeMapFR+phashFR(183)) = newFmChars("\\xe1 ", 0, 183); 
	/*  ------	 184 00B8 spacing cedilla  */
	*(UnicodeMapFR+phashFR(184)) = newFmChars("\\xfc ", 0, 184); 
	/*  ------	 185 00B9 superscript digit one  */
	*(UnicodeMapFR+phashFR(185)) = newFmChars("\\xb6 ", 0, 185); 
	/*  ------	 186 00BA masculine ordinal indicator  */
	*(UnicodeMapFR+phashFR(186)) = newFmChars("\\xbc ", 0, 186); 
	/*  ------	 187 00BB right pointing guillemet  */
	*(UnicodeMapFR+phashFR(187)) = newFmChars("\\xc8 ", 0, 187); 
	/*  ------	 188 00BC fraction one quarter  */
	*(UnicodeMapFR+phashFR(188)) = newFmChars("\\xb9 ", 0, 188); 
	/*  ------	 189 00BD fraction one half  */
	*(UnicodeMapFR+phashFR(189)) = newFmChars("\\xba ", 0, 189); 
	/*  ------	 190 00BE fraction three quarters  */
	*(UnicodeMapFR+phashFR(190)) = newFmChars("\\xbd ", 0, 190); 
	/*  ------	 191 00BF inverted question mark  */
	*(UnicodeMapFR+phashFR(191)) = newFmChars("\\xc0 ", 0, 191); 
	/*  ------	 192 00C0 latin capital letter a grave  */
	*(UnicodeMapFR+phashFR(192)) = newFmChars("\\xcb ", 0, 192); 
	/*  ------	 193 00C1 latin capital letter a acute  */
	*(UnicodeMapFR+phashFR(193)) = newFmChars("\\xe7 ", 0, 193); 
	/*  ------	 194 00C2 latin capital letter a circumflex  */
	*(UnicodeMapFR+phashFR(194)) = newFmChars("\\xe5 ", 0, 194); 
	/*  ------	 195 00C3 latin capital letter a tilde  */
	*(UnicodeMapFR+phashFR(195)) = newFmChars("\\xcc ", 0, 195); 
	/*  ------	 196 00C4 latin capital letter a diaeresis  */
	*(UnicodeMapFR+phashFR(196)) = newFmChars("\\x80 ", 0, 196); 
	/*  ------	 197 00C5 latin capital letter a ring  */
	*(UnicodeMapFR+phashFR(197)) = newFmChars("\\x81 ", 0, 197); 
	/*  ------	 198 00C6 latin capital letter a e  */
	*(UnicodeMapFR+phashFR(198)) = newFmChars("\\xae ", 0, 198); 
	/*  ------	 199 00C7 latin capital letter c cedilla  */
	*(UnicodeMapFR+phashFR(199)) = newFmChars("\\x82 ", 0, 199); 
	/*  ------	 200 00C8 latin capital letter e grave  */
	*(UnicodeMapFR+phashFR(200)) = newFmChars("\\xe9 ", 0, 200); 
	/*  ------	 201 00C9 latin capital letter e acute  */
	*(UnicodeMapFR+phashFR(201)) = newFmChars("\\x83 ", 0, 201); 
	/*  ------	 202 00CA latin capital letter e circumflex  */
	*(UnicodeMapFR+phashFR(202)) = newFmChars("\\xe6 ", 0, 202); 
	/*  ------	 203 00CB latin capital letter e diaeresis  */
	*(UnicodeMapFR+phashFR(203)) = newFmChars("\\xe8 ", 0, 203); 
	/*  ------	 204 00CC latin capital letter i grave  */
	*(UnicodeMapFR+phashFR(204)) = newFmChars("\\xed ", 0, 204); 
	/*  ------	 205 00CD latin capital letter i acute  */
	*(UnicodeMapFR+phashFR(205)) = newFmChars("\\xea ", 0, 205); 
	/*  ------	 206 00CE latin capital letter i circumflex  */
	*(UnicodeMapFR+phashFR(206)) = newFmChars("\\xeb ", 0, 206); 
	/*  ------	 207 00CF latin capital letter i diaeresis  */
	*(UnicodeMapFR+phashFR(207)) = newFmChars("\\xec ", 0, 207); 
	/*  ------	 208 00D0 latin capital letter eth  */
	*(UnicodeMapFR+phashFR(208)) = newFmChars("\\xc3 ", 0, 208); 
	/*  ------	 209 00D1 latin capital letter n tilde  */
	*(UnicodeMapFR+phashFR(209)) = newFmChars("\\x84 ", 0, 209); 
	/*  ------	 210 00D2 latin capital letter o grave  */
	*(UnicodeMapFR+phashFR(210)) = newFmChars("\\xf1 ", 0, 210); 
	/*  ------	 211 00D3 latin capital letter o acute  */
	*(UnicodeMapFR+phashFR(211)) = newFmChars("\\xee ", 0, 211); 
	/*  ------	 212 00D4 latin capital letter o circumflex  */
	*(UnicodeMapFR+phashFR(212)) = newFmChars("\\xef ", 0, 212); 
	/*  ------	 213 00D5 latin capital letter o tilde  */
	*(UnicodeMapFR+phashFR(213)) = newFmChars("\\xcd ", 0, 213); 
	/*  ------	 214 00D6 latin capital letter o diaeresis  */
	*(UnicodeMapFR+phashFR(214)) = newFmChars("\\x85 ", 0, 214); 
	/*  ------	 215 00D7 multiplication sign  */
#ifdef WIN32
	*(UnicodeMapFR+phashFR(215)) = newFmChars("\\xb0 ", 0, 215); 
#endif
#ifndef WIN32
	*(UnicodeMapFR+phashFR(215)) = newFmChars("\\xb4 ", 1, 215); 
#endif
	/*  ------	 216 00D8 latin capital letter o slash  */
	*(UnicodeMapFR+phashFR(216)) = newFmChars("\\xaf ", 0, 216); 
	/*  ------	 217 00D9 latin capital letter u grave  */
	*(UnicodeMapFR+phashFR(217)) = newFmChars("\\xf4 ", 0, 217); 
	/*  ------	 218 00DA latin capital letter u acute  */
	*(UnicodeMapFR+phashFR(218)) = newFmChars("\\xf2 ", 0, 218); 
	/*  ------	 219 00DB latin capital letter u circumflex  */
	*(UnicodeMapFR+phashFR(219)) = newFmChars("\\xf3 ", 0, 219); 
	/*  ------	 220 00DC latin capital letter u diaeresis  */
	*(UnicodeMapFR+phashFR(220)) = newFmChars("\\x86 ", 0, 220); 
	/*  ------	 221 00DD latin capital letter y acute  */
	*(UnicodeMapFR+phashFR(221)) = newFmChars("\\xc5 ", 0, 221); 
	/*  ------	 222 00DE latin capital letter thorn  */
	*(UnicodeMapFR+phashFR(222)) = newFmChars("\\xd7 ", 0, 222); 
	/*  ------	 223 00DF latin small letter sharp s  */
	*(UnicodeMapFR+phashFR(223)) = newFmChars("\\xa7 ", 0, 223); 
	/*  ------	 224 00E0 latin small letter a grave  */
	*(UnicodeMapFR+phashFR(224)) = newFmChars("\\x88 ", 0, 224); 
	/*  ------	 225 00E1 latin small letter a acute  */
	*(UnicodeMapFR+phashFR(225)) = newFmChars("\\x87 ", 0, 225); 
	/*  ------	 226 00E2 latin small letter a circumflex  */
	*(UnicodeMapFR+phashFR(226)) = newFmChars("\\x89 ", 0, 226); 
	/*  ------	 227 00E3 latin small letter a tilde  */
	*(UnicodeMapFR+phashFR(227)) = newFmChars("\\x8b ", 0, 227); 
	/*  ------	 228 00E4 latin small letter a diaeresis  */
	*(UnicodeMapFR+phashFR(228)) = newFmChars("\\x8a ", 0, 228); 
	/*  ------	 229 00E5 latin small letter a ring  */
	*(UnicodeMapFR+phashFR(229)) = newFmChars("\\x8c ", 0, 229); 
	/*  ------	 230 00E6 latin small letter a e  */
	*(UnicodeMapFR+phashFR(230)) = newFmChars("\\xbe ", 0, 230); 
	/*  ------	 231 00E7 latin small letter c cedilla  */
	*(UnicodeMapFR+phashFR(231)) = newFmChars("\\x8d ", 0, 231); 
	/*  ------	 232 00E8 latin small letter e grave  */
	*(UnicodeMapFR+phashFR(232)) = newFmChars("\\x8f ", 0, 232); 
	/*  ------	 233 00E9 latin small letter e acute  */
	*(UnicodeMapFR+phashFR(233)) = newFmChars("\\x8e ", 0, 233); 
	/*  ------	 234 00EA latin small letter e circumflex  */
	*(UnicodeMapFR+phashFR(234)) = newFmChars("\\x90 ", 0, 234); 
	/*  ------	 235 00EB latin small letter e diaeresis  */
	*(UnicodeMapFR+phashFR(235)) = newFmChars("\\x91 ", 0, 235); 
	/*  ------	 236 00EC latin small letter i grave  */
	*(UnicodeMapFR+phashFR(236)) = newFmChars("\\x93 ", 0, 236); 
	/*  ------	 237 00ED latin small letter i acute  */
	*(UnicodeMapFR+phashFR(237)) = newFmChars("\\x92 ", 0, 237); 
	/*  ------	 238 00EE latin small letter i circumflex  */
	*(UnicodeMapFR+phashFR(238)) = newFmChars("\\x94 ", 0, 238); 
	/*  ------	 239 00EF latin small letter i diaeresis  */
	*(UnicodeMapFR+phashFR(239)) = newFmChars("\\x95 ", 0, 239); 
	/*  ------	 240 00F0 latin small letter i eth  */
	*(UnicodeMapFR+phashFR(240)) = newFmChars("\\xb2 ", 0, 240); 
	/*  ------	 241 00F1 latin small letter n tilde  */
	*(UnicodeMapFR+phashFR(241)) = newFmChars("\\x96 ", 0, 241); 
	/*  ------	 242 00F2 latin small letter o grave  */
	*(UnicodeMapFR+phashFR(242)) = newFmChars("\\x98 ", 0, 242); 
	/*  ------	 243 00F3 latin small letter o acute  */
	*(UnicodeMapFR+phashFR(243)) = newFmChars("\\x97 ", 0, 243); 
	/*  ------	 244 00F4 latin small letter o circumflex  */
	*(UnicodeMapFR+phashFR(244)) = newFmChars("\\x99 ", 0, 244); 
	/*  ------	 245 00F5 latin small letter o tilde  */
	*(UnicodeMapFR+phashFR(245)) = newFmChars("\\x9b ", 0, 245); 
	/*  ------	 246 00F6 latin small letter o diaeresis  */
	*(UnicodeMapFR+phashFR(246)) = newFmChars("\\x9a ", 0, 246); 
	/*  ------	 247 00F7 division sign  */
#ifdef WIN32
	*(UnicodeMapFR+phashFR(247)) = newFmChars("\\xd6 ", 0, 247); 
#endif
#ifndef WIN32
	*(UnicodeMapFR+phashFR(247)) = newFmChars("\\xb8 ", 1, 247); 
#endif
	/*  ------	 248 00F8 latin small letter o slash  */
	*(UnicodeMapFR+phashFR(248)) = newFmChars("\\xbf ", 0, 248); 
	/*  ------	 249 00F9 latin small letter u grave  */
	*(UnicodeMapFR+phashFR(249)) = newFmChars("\\x9d ", 0, 249); 
	/*  ------	 250 00FA latin small letter u acute  */
	*(UnicodeMapFR+phashFR(250)) = newFmChars("\\x9c ", 0, 250); 
	/*  ------	 251 00FB latin small letter u circumflex  */
	*(UnicodeMapFR+phashFR(251)) = newFmChars("\\x9e ", 0, 251); 
	/*  ------	 252 00FC latin small letter u diaeresis  */
	*(UnicodeMapFR+phashFR(252)) = newFmChars("\\x9f ", 0, 252); 
	/*  ------	 253 00FD latin small letter y acute  */
	*(UnicodeMapFR+phashFR(253)) = newFmChars("\\xc6 ", 0, 253); 
	/*  ------	 254 00FE latin small letter thorn  */
	*(UnicodeMapFR+phashFR(254)) = newFmChars("\\xca ", 0, 254); 
	/*  ------	 255 00FF latin small letter y diaeresis  */
	*(UnicodeMapFR+phashFR(255)) = newFmChars("\\xd8 ", 0, 255); 
	/*  ------	 VERSION 5.1.0p44: support for &Yuml;  */
	*(UnicodeMapFR+phashFR(376)) = newFmChars("\\xd9 ", 0, 376); 
	/*  ------	 305 0131 latin small letter dotless i  */
	*(UnicodeMapFR+phashFR(305)) = newFmChars("\\xf5 ", 0, 305); 
	/*  ------	 402 0192 latin small letter script f  */
	*(UnicodeMapFR+phashFR(402)) = newFmChars("\\xc4 ", 0, 402); 
	/*  ------	 728 02D8 spacing breve  */
	*(UnicodeMapFR+phashFR(728)) = newFmChars("\\xf9 ", 0, 728); 
	/*  ------	 729 02D9 spacing dot above  */
	*(UnicodeMapFR+phashFR(729)) = newFmChars("\\xfa ", 0, 729); 
	/*  ------	 730 02DA spacing ring above  */
	*(UnicodeMapFR+phashFR(730)) = newFmChars("\\xfb ", 0, 730); 
	/*  ------	 731 02DB spacing ogonek  */
	*(UnicodeMapFR+phashFR(731)) = newFmChars("\\xfe ", 0, 731); 
	/*  ------	 732 02DC spacing tilde (ISO 10646-1 = `small tilde'   */
	/*  ------	VERSION 7.2.0p40: map small tilde to \\xf7 (was \\x7e) */
	*(UnicodeMapFR+phashFR(732)) = newFmChars("\\xf7 ", 0, 732); 
	/*  ------	 913 0391 greek capital letter alpha  */
	*(UnicodeMapFR+phashFR(913)) = newFmChars("\\x41 ", 1, 913); 
	/*  ------	 914 0392 greek capital letter beta  */
	*(UnicodeMapFR+phashFR(914)) = newFmChars("\\x42 ", 1, 914); 
	/*  ------	 915 0393 greek capital letter gamma  */
	*(UnicodeMapFR+phashFR(915)) = newFmChars("\\x47 ", 1, 915); 
	/*  ------	 916 0394 greek capital letter delta  */
	*(UnicodeMapFR+phashFR(916)) = newFmChars("\\x44 ", 1, 916); 
	/*  ------	 917 0395 greek capital letter epsilon  */
	*(UnicodeMapFR+phashFR(917)) = newFmChars("\\x45 ", 1, 917); 
	/*  ------	 918 0396 greek capital letter zeta  */
	*(UnicodeMapFR+phashFR(918)) = newFmChars("\\x5a ", 1, 918); 
	/*  ------	 919 0397 greek capital letter eta  */
	*(UnicodeMapFR+phashFR(919)) = newFmChars("\\x48 ", 1, 919); 
	/*  ------	 920 0398 greek capital letter theta  */
	*(UnicodeMapFR+phashFR(920)) = newFmChars("\\x51 ", 1, 920); 
	/*  ------	 921 0399 greek capital letter iota  */
	*(UnicodeMapFR+phashFR(921)) = newFmChars("\\x49 ", 1, 921); 
	/*  ------	 922 039A greek capital letter kappa  */
	*(UnicodeMapFR+phashFR(922)) = newFmChars("\\x4b ", 1, 922); 
	/*  ------	 923 039B greek capital letter lambda  */
	*(UnicodeMapFR+phashFR(923)) = newFmChars("\\x4c ", 1, 923); 
	/*  ------	 924 039C greek capital letter mu  */
	*(UnicodeMapFR+phashFR(924)) = newFmChars("\\x4d ", 1, 924); 
	/*  ------	 925 039D greek capital letter nu  */
	*(UnicodeMapFR+phashFR(925)) = newFmChars("\\x4e ", 1, 925); 
	/*  ------	 926 039E greek capital letter xi  */
	*(UnicodeMapFR+phashFR(926)) = newFmChars("\\x58 ", 1, 926); 
	/*  ------	 927 039F greek capital letter omicron  */
	*(UnicodeMapFR+phashFR(927)) = newFmChars("\\x4f ", 1, 927); 
	/*  ------	 928 03A0 greek capital letter pi  */
	*(UnicodeMapFR+phashFR(928)) = newFmChars("\\x50 ", 1, 928); 
	/*  ------	 929 03A1 greek capital letter rho  */
	*(UnicodeMapFR+phashFR(929)) = newFmChars("\\x52 ", 1, 929); 
	/*  ------	 931 03A3 greek capital letter sigma  */
	*(UnicodeMapFR+phashFR(931)) = newFmChars("\\x53 ", 1, 931); 
	/*  ------	 932 03A4 greek capital letter tau  */
	*(UnicodeMapFR+phashFR(932)) = newFmChars("\\x54 ", 1, 932); 
	/*  ------	 933 03A5 greek capital letter upsilon  */
	*(UnicodeMapFR+phashFR(933)) = newFmChars("\\x55 ", 1, 933); 
	/*  ------	 934 03A6 greek capital letter phi  */
	*(UnicodeMapFR+phashFR(934)) = newFmChars("\\x46 ", 1, 934); 
	/*  ------	 935 03A7 greek capital letter chi  */
	*(UnicodeMapFR+phashFR(935)) = newFmChars("\\x43 ", 1, 935); 
	/*  ------	 936 03A8 greek capital letter psi  */
	*(UnicodeMapFR+phashFR(936)) = newFmChars("\\x59 ", 1, 936); 
	/*  ------	 937 03A9 greek capital letter omega  */
	*(UnicodeMapFR+phashFR(937)) = newFmChars("\\x57 ", 1, 937); 
	/*  ------	 945 03B1 greek small letter alpha  */
	*(UnicodeMapFR+phashFR(945)) = newFmChars("\\x61 ", 1, 945); 
	/*  ------	 946 03B2 greek small letter beta  */
	*(UnicodeMapFR+phashFR(946)) = newFmChars("\\x62 ", 1, 946); 
	/*  ------	 947 03B3 greek small letter gamma  */
	*(UnicodeMapFR+phashFR(947)) = newFmChars("\\x67 ", 1, 947); 
	/*  ------	 948 03B4 greek small letter delta  */
	*(UnicodeMapFR+phashFR(948)) = newFmChars("\\x64 ", 1, 948); 
	/*  ------	 949 03B5 greek small letter epsilon  */
	*(UnicodeMapFR+phashFR(949)) = newFmChars("\\x65 ", 1, 949); 
	/*  ------	 950 03B6 greek small letter zeta  */
	*(UnicodeMapFR+phashFR(950)) = newFmChars("\\x7a ", 1, 950); 
	/*  ------	 951 03B7 greek small letter eta  */
	*(UnicodeMapFR+phashFR(951)) = newFmChars("\\x68 ", 1, 951); 
	/*  ------	 952 03B8 greek small letter theta  */
	*(UnicodeMapFR+phashFR(952)) = newFmChars("\\x71 ", 1, 952); 
	/*  ------	 953 03B9 greek small letter iota  */
	*(UnicodeMapFR+phashFR(953)) = newFmChars("\\x69 ", 1, 953); 
	/*  ------	 954 03BA greek small letter kappa  */
	*(UnicodeMapFR+phashFR(954)) = newFmChars("\\x6b ", 1, 954); 
	/*  ------	 955 03BB greek small letter lambda  */
	*(UnicodeMapFR+phashFR(955)) = newFmChars("\\x6c ", 1, 955); 
	/*  ------	 956 03BC greek small letter mu  */
	*(UnicodeMapFR+phashFR(956)) = newFmChars("\\x6d ", 1, 956); 
	/*  ------	 957 03BD greek small letter nu  */
	*(UnicodeMapFR+phashFR(957)) = newFmChars("\\x6e ", 1, 957); 
	/*  ------	 958 03BE greek small letter xi  */
	*(UnicodeMapFR+phashFR(958)) = newFmChars("\\x78 ", 1, 958); 
	/*  ------	 959 03BF greek small letter omicron  */
	*(UnicodeMapFR+phashFR(959)) = newFmChars("\\x6f ", 1, 959); 
	/*  ------	 960 03C0 greek small letter pi  */
	*(UnicodeMapFR+phashFR(960)) = newFmChars("\\x70 ", 1, 960); 
	/*  ------	 961 03C1 greek small letter rho  */
	*(UnicodeMapFR+phashFR(961)) = newFmChars("\\x72 ", 1, 961); 
	/*  ------	 962 03C2 greek small letter final sigma  */
	*(UnicodeMapFR+phashFR(962)) = newFmChars("\\x56 ", 1, 962); 
	/*  ------	 963 03C3 greek small letter sigma  */
	*(UnicodeMapFR+phashFR(963)) = newFmChars("\\x73 ", 1, 963); 
	/*  ------	 964 03C4 greek small letter tau  */
	*(UnicodeMapFR+phashFR(964)) = newFmChars("\\x74 ", 1, 964); 
	/*  ------	 965 03C5 greek small letter upsilon  */
	*(UnicodeMapFR+phashFR(965)) = newFmChars("\\x75 ", 1, 965); 
	/*  ------	 966 03C6 greek small letter phi  */
	*(UnicodeMapFR+phashFR(966)) = newFmChars("\\x66 ", 1, 966); 
	/*  ------	 967 03C7 greek small letter chi  */
	*(UnicodeMapFR+phashFR(967)) = newFmChars("\\x63 ", 1, 967); 
	/*  ------	 968 03C8 greek small letter psi  */
	*(UnicodeMapFR+phashFR(968)) = newFmChars("\\x79 ", 1, 968); 
	/*  ------	 969 03C9 greek small letter omega  */
	*(UnicodeMapFR+phashFR(969)) = newFmChars("\\x77 ", 1, 969); 
	/*  ------	 977 03D1 greek small letter script theta  */
	*(UnicodeMapFR+phashFR(977)) = newFmChars("\\x4a ", 1, 977); 
	/*  ------	 978 03D2 greek capital letter upsilon hook  */
	*(UnicodeMapFR+phashFR(978)) = newFmChars("\\xa1 ", 1, 978); 
	/*  ------	 981 03D5 greek small letter script phi  */
	*(UnicodeMapFR+phashFR(981)) = newFmChars("\\x6a ", 1, 981); 
	/*  ------	 982 03D6 greek small letter omega pi  */
	*(UnicodeMapFR+phashFR(982)) = newFmChars("\\x76 ", 1, 982); 
	/*  ------	 8194 2002 en space  */
	*(UnicodeMapFR+phashFR(8194)) = newFmChars("\\x13 ", 0, 8194); 
	/*  ------	 8195 2003 em space  */
	*(UnicodeMapFR+phashFR(8195)) = newFmChars("\\x14 ", 0, 8195); 
	/*  ------	 8199 2007 figure space (numeric space   */
	*(UnicodeMapFR+phashFR(8199)) = newFmChars("\\x10 ", 0, 8199); 
	/*  ------	 8201 2009 thin space (1/6 em)  */
	*(UnicodeMapFR+phashFR(8201)) = newFmChars("\\x12 \\x12 ", 0, 8201); 
	/*  ------	 8202 200A hair space (1/12 em)  */
	*(UnicodeMapFR+phashFR(8202)) = newFmChars("\\x12 ", 0, 8202); 
	/*  ------	 8209 2011 non-breaking hyphen  */
	*(UnicodeMapFR+phashFR(8209)) = newFmChars("\\x15 ", 0, 8209); 
	/*  ------	 8211 2013 en dash  */
	*(UnicodeMapFR+phashFR(8211)) = newFmChars("\\xd0 ", 0, 8211); 
	/*  ------	 8212 2014 em dash  */
	*(UnicodeMapFR+phashFR(8212)) = newFmChars("\\xd1 ", 0, 8212); 
	/*  ------	 8216 2018 single turned comma quotation mark  */
	*(UnicodeMapFR+phashFR(8216)) = newFmChars("\\xd4 ", 0, 8216); 
	/*  ------	 8217 2019 single comma quotation mark  */
	*(UnicodeMapFR+phashFR(8217)) = newFmChars("\\xd5 ", 0, 8217); 
	/*  ------	 8218 201A low single comma quotation mark  */
	*(UnicodeMapFR+phashFR(8218)) = newFmChars("\\xe2 ", 0, 8218); 
	/*  ------	 8220 201C double turned comma quotation mark  */
	*(UnicodeMapFR+phashFR(8220)) = newFmChars("\\xd2 ", 0, 8220); 
	/*  ------	 8221 201D double comma quotation mark  */
	*(UnicodeMapFR+phashFR(8221)) = newFmChars("\\xd3 ", 0, 8221); 
	/*  ------	 8222 201E low double comma quotation mark  */
	*(UnicodeMapFR+phashFR(8222)) = newFmChars("\\xe3 ", 0, 8222); 
	/*  ------	 8224 2020 dagger  */
	*(UnicodeMapFR+phashFR(8224)) = newFmChars("\\xa0 ", 0, 8224); 
	/*  ------	 8225 2021 double dagger  */
	*(UnicodeMapFR+phashFR(8225)) = newFmChars("\\xe0 ", 0, 8225); 
	/*  ------	 8226 2022 bullet  */
	*(UnicodeMapFR+phashFR(8226)) = newFmChars("\\xa5 ", 0, 8226); 
	/*  ------	 8230 2026 horizontal ellipsis  */
	*(UnicodeMapFR+phashFR(8230)) = newFmChars("\\xc9 ", 0, 8230); 
	/*  ------	 8231 2027 hyphenation point  */
	/*  ------	 VERSION 5.4.1p38: added #x2027; for discretionary hyphen  */
	*(UnicodeMapFR+phashFR(8231)) = newFmChars("\\x04 ", 0, 8231); 
	/*  ------	 8233 2029 paragraph separator  */
	*(UnicodeMapFR+phashFR(8233)) = newFmChars("\\x0a ", 0, 8233); 
	/*  ------	 8240 2030 per mille sign  */
	*(UnicodeMapFR+phashFR(8240)) = newFmChars("\\xe4 ", 0, 8240); 
	/*  ------	 8242 2032 prime  */
	*(UnicodeMapFR+phashFR(8242)) = newFmChars("\\xa2 ", 1, 8242); 
	/*  ------	 8243 2033 double prime  */
	*(UnicodeMapFR+phashFR(8243)) = newFmChars("\\xb2 ", 1, 8243); 
	/*  ------	 8249 2039 left pointing single guillemet  */
	*(UnicodeMapFR+phashFR(8249)) = newFmChars("\\xdc ", 0, 8249); 
	/*  ------	 8250 203A right pointing single guillemet  */
	*(UnicodeMapFR+phashFR(8250)) = newFmChars("\\xdd ", 0, 8250); 
	/*  ------	 8254 203E spacing overscore  */
	*(UnicodeMapFR+phashFR(8254)) = newFmChars("\\x60 ", 1, 8254); 
	/*  ------	 8260 2044 fraction slash  */
	*(UnicodeMapFR+phashFR(8260)) = newFmChars("\\xda ", 0, 8260); 
	/*  VERSION 6.0.0p53: support for euro */
	/*  ------	 8364 20AC euro */
#ifndef WIN32
	*(UnicodeMapFR+phashFR(8364)) = newFmChars("E", 3, 8364); 
#endif
#ifdef WIN32
	*(UnicodeMapFR+phashFR(8364)) = newFmChars("\\xf5 ", 0, 8364); 
#endif
	/*  ------	 8465 2111 black-letter 1  */
	*(UnicodeMapFR+phashFR(8465)) = newFmChars("\\xc1 ", 1, 8465); 
	/*  ------	 8472 2118 script p  */
	*(UnicodeMapFR+phashFR(8472)) = newFmChars("\\xc3 ", 1, 8472); 
	/*  ------	 8476 211C black-letter r  */
	*(UnicodeMapFR+phashFR(8476)) = newFmChars("\\xc2 ", 1, 8476); 
	/*  ------	 8482 2122 trademark  */
	*(UnicodeMapFR+phashFR(8482)) = newFmChars("\\xaa ", 0, 8482); 
	/*  ------	 8486 2126 ohm  */
	*(UnicodeMapFR+phashFR(8486)) = newFmChars("\\x57 ", 1, 8486); 
	/*  ------	 8501 2135 first traSMSfinite cardinal  */
	*(UnicodeMapFR+phashFR(8501)) = newFmChars("\\xc0 ", 1, 8501); 
	/*  ------	 8592 2190 left arrow  */
	*(UnicodeMapFR+phashFR(8592)) = newFmChars("\\xac ", 1, 8592); 
	/*  ------	 8593 2191 up arrow  */
	*(UnicodeMapFR+phashFR(8593)) = newFmChars("\\xad ", 1, 8593); 
	/*  ------	 8594 2192 right arrow  */
	*(UnicodeMapFR+phashFR(8594)) = newFmChars("\\xae ", 1, 8594); 
	/*  ------	 8595 2193 down arrow  */
	*(UnicodeMapFR+phashFR(8595)) = newFmChars("\\xaf ", 1, 8595); 
	/*  ------	 8596 2194 left right arrow  */
	*(UnicodeMapFR+phashFR(8596)) = newFmChars("\\xab ", 1, 8596); 
	/*  ------	 8597 2195 up down arrow  */
	*(UnicodeMapFR+phashFR(8597)) = newFmChars("\\xd7 ", 2, 8597); 
	/*  ------	 8629 21B5 down arrow with corner left  */
	*(UnicodeMapFR+phashFR(8629)) = newFmChars("\\xbf ", 1, 8629); 
	/*  ------	 8656 21D0 left double arrow  */
	*(UnicodeMapFR+phashFR(8656)) = newFmChars("\\xdc ", 1, 8656); 
	/*  ------	 8657 21D1 up double arrow  */
	*(UnicodeMapFR+phashFR(8657)) = newFmChars("\\xdd ", 1, 8657); 
	/*  ------	 8658 21D2 right double arrow  */
	*(UnicodeMapFR+phashFR(8658)) = newFmChars("\\xde ", 1, 8658); 
	/*  ------	 8659 21D3 down double arrow  */
	*(UnicodeMapFR+phashFR(8659)) = newFmChars("\\xdf ", 1, 8659); 
	/*  ------	 8660 21D4 left right double arrow  */
	*(UnicodeMapFR+phashFR(8660)) = newFmChars("\\xdb ", 1, 8660); 
	/*  ------	 8704 2200 for all  */
	*(UnicodeMapFR+phashFR(8704)) = newFmChars("\\x22 ", 1, 8704); 
	/*  ------	 8706 2202 partial differential  */
	*(UnicodeMapFR+phashFR(8706)) = newFmChars("\\xb6 ", 1, 8706); 
	/*  ------	 8707 2203 there exists  */
	*(UnicodeMapFR+phashFR(8707)) = newFmChars("\\x24 ", 1, 8707); 
	/*  ------	 8709 2205 empty set  */
	*(UnicodeMapFR+phashFR(8709)) = newFmChars("\\xc6 ", 1, 8709); 
	/*  ------	 8710 2206 increment  */
	*(UnicodeMapFR+phashFR(8710)) = newFmChars("\\x44 ", 1, 8710); 
	/*  ------	 8711 2207 nabla  */
	*(UnicodeMapFR+phashFR(8711)) = newFmChars("\\xd1 ", 1, 8711); 
	/*  ------	 8712 2208 element of  */
	*(UnicodeMapFR+phashFR(8712)) = newFmChars("\\xce ", 1, 8712); 
	/*  ------	 8713 2209 not an element of  */
	*(UnicodeMapFR+phashFR(8713)) = newFmChars("\\xcf ", 1, 8713); 
	/*  ------	 8715 220B contaiSMS as member  */
	*(UnicodeMapFR+phashFR(8715)) = newFmChars("\\x27 ", 1, 8715); 
	/*  ------	 8719 220F n-ary product  */
	*(UnicodeMapFR+phashFR(8719)) = newFmChars("\\xd5 ", 1, 8719); 
	/*  ------	 8721 2211 n-ary summation  */
	*(UnicodeMapFR+phashFR(8721)) = newFmChars("\\xe5 ", 1, 8721); 
	/*  ------	 8722 2212 minus sign  */
	*(UnicodeMapFR+phashFR(8722)) = newFmChars("\\x2d ", 1, 8722); 
	/*  ------	 8725 2215 division slash  */
	*(UnicodeMapFR+phashFR(8725)) = newFmChars("\\xa4 ", 1, 8725); 
	/*  ------	 8727 2217 asterisk operator  */
	*(UnicodeMapFR+phashFR(8727)) = newFmChars("\\x2a ", 1, 8727); 
	/*  ------	 8730 221A square root  */
	*(UnicodeMapFR+phashFR(8730)) = newFmChars("\\xd6 ", 1, 8730); 
	/*  ------	 8733 221D proportional to  */
	*(UnicodeMapFR+phashFR(8733)) = newFmChars("\\xb5 ", 1, 8733); 
	/*  ------	 8734 221E infinity  */
	*(UnicodeMapFR+phashFR(8734)) = newFmChars("\\xa5 ", 1, 8734); 
	/*  ------	 8736 2220 angle  */
	*(UnicodeMapFR+phashFR(8736)) = newFmChars("\\xd0 ", 1, 8736); 
	/*  ------	 8743 2227 logical and  */
	*(UnicodeMapFR+phashFR(8743)) = newFmChars("\\xd9 ", 1, 8743); 
	/*  ------	 8744 2228 logical or  */
	*(UnicodeMapFR+phashFR(8744)) = newFmChars("\\xda ", 1, 8744); 
	/*  ------	 8745 2229 intersection  */
	*(UnicodeMapFR+phashFR(8745)) = newFmChars("\\xc7 ", 1, 8745); 
	/*  ------	 8746 222A union  */
	*(UnicodeMapFR+phashFR(8746)) = newFmChars("\\xc8 ", 1, 8746); 
	/*  ------	 8747 222B integral  */
	*(UnicodeMapFR+phashFR(8747)) = newFmChars("\\xf2 ", 1, 8747); 
	/*  ------	 8756 2234 therefore  */
	*(UnicodeMapFR+phashFR(8756)) = newFmChars("\\x5c ", 1, 8756); 
	/*  ------	 8764 223C tilde operator  */
	*(UnicodeMapFR+phashFR(8764)) = newFmChars("\\x7e ", 1, 8764); 
	/*  ------	 8773 2245 approximately equal to  */
	*(UnicodeMapFR+phashFR(8773)) = newFmChars("\\x40 ", 1, 8773); 
	/*  ------	 8776 2248 almost equal to  */
	*(UnicodeMapFR+phashFR(8776)) = newFmChars("\\xbb ", 1, 8776); 
	/*  ------	 8800 2260 not equal to  */
	*(UnicodeMapFR+phashFR(8800)) = newFmChars("\\xb9 ", 1, 8800); 
	/*  ------	 8801 2261 identical to  */
	*(UnicodeMapFR+phashFR(8801)) = newFmChars("\\xba ", 1, 8801); 
	/*  ------	 8804 2264 less than or equal to  */
	*(UnicodeMapFR+phashFR(8804)) = newFmChars("\\xa3 ", 1, 8804); 
	/*  ------	 8805 2265 greater than or equal to  */
	*(UnicodeMapFR+phashFR(8805)) = newFmChars("\\xb3 ", 1, 8805); 
	/*  ------	 8834 2282 subset of  */
	*(UnicodeMapFR+phashFR(8834)) = newFmChars("\\xcc ", 1, 8834); 
	/*  ------	 8835 2283 superset of  */
	*(UnicodeMapFR+phashFR(8835)) = newFmChars("\\xc9 ", 1, 8835); 
	/*  ------	 8836 2284 not a subset of  */
	*(UnicodeMapFR+phashFR(8836)) = newFmChars("\\xcb ", 1, 8836); 
	/*  ------	 8838 2286 subset of or equal to  */
	*(UnicodeMapFR+phashFR(8838)) = newFmChars("\\xcd ", 1, 8838); 
	/*  ------	 8839 2287 superset of or equal to  */
	*(UnicodeMapFR+phashFR(8839)) = newFmChars("\\xca ", 1, 8839); 
	/*  ------	 8853 2295 circled plus  */
	*(UnicodeMapFR+phashFR(8853)) = newFmChars("\\xc5 ", 1, 8853); 
	/*  ------	 8855 2297 circled times  */
	*(UnicodeMapFR+phashFR(8855)) = newFmChars("\\xc4 ", 1, 8855); 
	/*  ------	 8869 22A5 up tack  */
	*(UnicodeMapFR+phashFR(8869)) = newFmChars("\\x5e ", 1, 8869); 
	/*  ------	 8901 22C5 dot operator  */
	*(UnicodeMapFR+phashFR(8901)) = newFmChars("\\xd7 ", 1, 8901); 
	/*  ------	 8992 2320 top half integral  */
	*(UnicodeMapFR+phashFR(8992)) = newFmChars("\\xf3 ", 1, 8992); 
	/*  ------	 8993 2321 bottom half integral  */
	*(UnicodeMapFR+phashFR(8993)) = newFmChars("\\xf5 ", 1, 8993); 
	/*  ------	 9001 2329 bra  */
	*(UnicodeMapFR+phashFR(9001)) = newFmChars("\\xe1 ", 1, 9001); 
	/*  ------	 9002 232A ket  */
	*(UnicodeMapFR+phashFR(9002)) = newFmChars("\\xf1 ", 1, 9002); 
	/*  ------	 9312 2460 circled digit one  */
	*(UnicodeMapFR+phashFR(9312)) = newFmChars("\\xac ", 2, 9312); 
	/*  ------	 9313 2461 circled digit two  */
	*(UnicodeMapFR+phashFR(9313)) = newFmChars("\\xad ", 2, 9313); 
	/*  ------	 9314 2462 circled digit three  */
	*(UnicodeMapFR+phashFR(9314)) = newFmChars("\\xae ", 2, 9314); 
	/*  ------	 9315 2463 circled digit four  */
	*(UnicodeMapFR+phashFR(9315)) = newFmChars("\\xaf ", 2, 9315); 
	/*  ------	 9316 2464 circled digit five  */
	*(UnicodeMapFR+phashFR(9316)) = newFmChars("\\xb0 ", 2, 9316); 
	/*  ------	 9317 2465 circled digit six  */
	*(UnicodeMapFR+phashFR(9317)) = newFmChars("\\xb1 ", 2, 9317); 
	/*  ------	 9318 2466 circled digit seven  */
	*(UnicodeMapFR+phashFR(9318)) = newFmChars("\\xb2 ", 2, 9318); 
	/*  ------	 9319 2467 circled digit eight  */
	*(UnicodeMapFR+phashFR(9319)) = newFmChars("\\xb3 ", 2, 9319); 
	/*  ------	 9320 2468 circled digit nine  */
	*(UnicodeMapFR+phashFR(9320)) = newFmChars("\\xb4 ", 2, 9320); 
	/*  ------	 9321 2469 circled number ten  */
	*(UnicodeMapFR+phashFR(9321)) = newFmChars("\\xb5 ", 2, 9321); 
	/*  ------	 9612 258C black down pointing triangle  */
	*(UnicodeMapFR+phashFR(9612)) = newFmChars("\\x74 ", 2, 9612); 
	/*  ------	 9632 25A0 black square  */
	*(UnicodeMapFR+phashFR(9632)) = newFmChars("\\x6e ", 2, 9632); 
	/*  ------	 9650 25B2 black up pointing triangle  */
	*(UnicodeMapFR+phashFR(9650)) = newFmChars("\\x73 ", 2, 9650); 
	/*  ------	 9670 25C6 black diamond  */
	*(UnicodeMapFR+phashFR(9670)) = newFmChars("\\x75 ", 2, 9670); 
	/*  ------	 9674 25CA lozenge  */
	*(UnicodeMapFR+phashFR(9674)) = newFmChars("\\xe0 ", 1, 9674); 
	/*  ------	 9733 2605 black star  */
	*(UnicodeMapFR+phashFR(9733)) = newFmChars("\\x48 ", 2, 9733); 
	/*  ------	 9742 260E black telephone  */
	*(UnicodeMapFR+phashFR(9742)) = newFmChars("\\x25 ", 2, 9742); 
	/*  ------	 9755 261B black right pointing index  */
	*(UnicodeMapFR+phashFR(9755)) = newFmChars("\\x2a ", 2, 9755); 
	/*  ------	 9758 261E white right pointing index  */
	*(UnicodeMapFR+phashFR(9758)) = newFmChars("\\x2b ", 2, 9758); 
	/*  ------	 9824 2660 black spade suit  */
	*(UnicodeMapFR+phashFR(9824)) = newFmChars("\\xaa ", 1, 9824); 
	/*  ------	 9824 2660 black spade suit  */
	/* 9824 "\\xab " ZapfDingbats  */
	/*  ------	 9827 2663 black club suit  */
	*(UnicodeMapFR+phashFR(9827)) = newFmChars("\\xa7 ", 1, 9827); 
	/*  ------	 9827 2663 black club suit  */
	/* 9827 "\\xa8 " ZapfDingbats  */
	/*  ------	 9829 2665 black heart suit  */
	*(UnicodeMapFR+phashFR(9829)) = newFmChars("\\xa9 ", 1, 9829); 
	/*  ------	 9829 2665 black heart suit  */
	/* 9829 "\\xaa " ZapfDingbats  */
	/*  ------	 9830 2666 black diamond suit  */
	*(UnicodeMapFR+phashFR(9830)) = newFmChars("\\xa8 ", 1, 9830); 
	/*  ------	 9830 2666 black diamond suit  */
	/* 9830 "\\xa9 " ZapfDingbats  */
	/*  ------	 9985 2701 upper blade scissors  */
	*(UnicodeMapFR+phashFR(9985)) = newFmChars("\\x21 ", 2, 9985); 
	/*  ------	 9986 2702 black scissors  */
	*(UnicodeMapFR+phashFR(9986)) = newFmChars("\\x22 ", 2, 9986); 
	/*  ------	 9987 2703 lower blade scissors  */
	*(UnicodeMapFR+phashFR(9987)) = newFmChars("\\x23 ", 2, 9987); 
	/*  ------	 9988 2704 white scissors  */
	*(UnicodeMapFR+phashFR(9988)) = newFmChars("\\x24 ", 2, 9988); 
	/*  ------	 9990 2706 telephone location sign  */
	*(UnicodeMapFR+phashFR(9990)) = newFmChars("\\x26 ", 2, 9990); 
	/*  ------	 9991 2707 tape drive  */
	*(UnicodeMapFR+phashFR(9991)) = newFmChars("\\x27 ", 2, 9991); 
	/*  ------	 9992 2708 airplane  */
	*(UnicodeMapFR+phashFR(9992)) = newFmChars("\\x28 ", 2, 9992); 
	/*  ------	 9993 2709 envelope  */
	*(UnicodeMapFR+phashFR(9993)) = newFmChars("\\x29 ", 2, 9993); 
	/*  ------	 9996 270C victory hand  */
	*(UnicodeMapFR+phashFR(9996)) = newFmChars("\\x2c ", 2, 9996); 
	/*  ------	 9997 270D writing hand  */
	*(UnicodeMapFR+phashFR(9997)) = newFmChars("\\x2d ", 2, 9997); 
	/*  ------	 9998 270E lower right pencil  */
	*(UnicodeMapFR+phashFR(9998)) = newFmChars("\\x2e ", 2, 9998); 
	/*  ------	 9999 270F pencil  */
	*(UnicodeMapFR+phashFR(9999)) = newFmChars("\\x2f ", 2, 9999); 
	/*  ------	 10000 2710 upper right pencil  */
	*(UnicodeMapFR+phashFR(10000)) = newFmChars("\\x30 ", 2, 10000); 
	/*  ------	 10001 2711 white nib  */
	*(UnicodeMapFR+phashFR(10001)) = newFmChars("\\x31 ", 2, 10001); 
	/*  ------	 10002 2712 black nib  */
	*(UnicodeMapFR+phashFR(10002)) = newFmChars("\\x32 ", 2, 10002); 
	/*  ------	 10003 2713 check mark  */
	*(UnicodeMapFR+phashFR(10003)) = newFmChars("\\x33 ", 2, 10003); 
	/*  ------	 10004 2714 heavy check mark  */
	*(UnicodeMapFR+phashFR(10004)) = newFmChars("\\x34 ", 2, 10004); 
	/*  ------	 10005 2715 multiplication x  */
	*(UnicodeMapFR+phashFR(10005)) = newFmChars("\\x35 ", 2, 10005); 
	/*  ------	 10006 2716 heavy multiplication x  */
	*(UnicodeMapFR+phashFR(10006)) = newFmChars("\\x36 ", 2, 10006); 
	/*  ------	 10007 2717 ballot x  */
	*(UnicodeMapFR+phashFR(10007)) = newFmChars("\\x37 ", 2, 10007); 
	/*  ------	 10008 2718 heavy ballot x  */
	*(UnicodeMapFR+phashFR(10008)) = newFmChars("\\x38 ", 2, 10008); 
	/*  ------	 10009 2719 outlined greek cross  */
	*(UnicodeMapFR+phashFR(10009)) = newFmChars("\\x39 ", 2, 10009); 
	/*  ------	 10010 271A heavy greek cross  */
	*(UnicodeMapFR+phashFR(10010)) = newFmChars("\\x3a ", 2, 10010); 
	/*  ------	 10011 271B open center cross  */
	*(UnicodeMapFR+phashFR(10011)) = newFmChars("\\x3b ", 2, 10011); 
	/*  ------	 10012 271C heavy open center cross  */
	*(UnicodeMapFR+phashFR(10012)) = newFmChars("\\x3c ", 2, 10012); 
	/*  ------	 10013 271D latin cross  */
	*(UnicodeMapFR+phashFR(10013)) = newFmChars("\\x3d ", 2, 10013); 
	/*  ------	 10014 271E shadowed white latin cross  */
	*(UnicodeMapFR+phashFR(10014)) = newFmChars("\\x3e ", 2, 10014); 
	/*  ------	 10015 271F outlined latin cross  */
	*(UnicodeMapFR+phashFR(10015)) = newFmChars("\\x3f ", 2, 10015); 
	/*  ------	 10016 2720 maltese cross  */
	*(UnicodeMapFR+phashFR(10016)) = newFmChars("\\x40 ", 2, 10016); 
	/*  ------	 10017 2721 star of david  */
	*(UnicodeMapFR+phashFR(10017)) = newFmChars("\\x41 ", 2, 10017); 
	/*  ------	 10018 2722 four teardrop-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10018)) = newFmChars("\\x42 ", 2, 10018); 
	/*  ------	 10019 2723 four balloon-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10019)) = newFmChars("\\x43 ", 2, 10019); 
	/*  ------	 10020 2724 heavy four balloon-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10020)) = newFmChars("\\x44 ", 2, 10020); 
	/*  ------	 10021 2725 four club-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10021)) = newFmChars("\\x45 ", 2, 10021); 
	/*  ------	 10022 2726 black four pointed star  */
	*(UnicodeMapFR+phashFR(10022)) = newFmChars("\\x46 ", 2, 10022); 
	/*  ------	 10025 2729 stress outlined white star  */
	*(UnicodeMapFR+phashFR(10025)) = newFmChars("\\x49 ", 2, 10025); 
	/*  ------	 10026 272A circled white star  */
	*(UnicodeMapFR+phashFR(10026)) = newFmChars("\\x4a ", 2, 10026); 
	/*  ------	 10027 272B open center black star  */
	*(UnicodeMapFR+phashFR(10027)) = newFmChars("\\x4b ", 2, 10027); 
	/*  ------	 10028 272C black center white star  */
	*(UnicodeMapFR+phashFR(10028)) = newFmChars("\\x4c ", 2, 10028); 
	/*  ------	 10029 272D outlined black star  */
	*(UnicodeMapFR+phashFR(10029)) = newFmChars("\\x4d ", 2, 10029); 
	/*  ------	 10030 272E heavy outlined black star  */
	*(UnicodeMapFR+phashFR(10030)) = newFmChars("\\x4e ", 2, 10030); 
	/*  ------	 10031 272F pinwheel star  */
	*(UnicodeMapFR+phashFR(10031)) = newFmChars("\\x4f ", 2, 10031); 
	/*  ------	 10032 2730 shadowed white star  */
	*(UnicodeMapFR+phashFR(10032)) = newFmChars("\\x50 ", 2, 10032); 
	/*  ------	 10033 2731 heavy asterisk  */
	*(UnicodeMapFR+phashFR(10033)) = newFmChars("\\x51 ", 2, 10033); 
	/*  ------	 10034 2732 open center asterisk  */
	*(UnicodeMapFR+phashFR(10034)) = newFmChars("\\x52 ", 2, 10034); 
	/*  ------	 10035 2733 eight spoked asterisk  */
	*(UnicodeMapFR+phashFR(10035)) = newFmChars("\\x53 ", 2, 10035); 
	/*  ------	 10036 2734 eight pointed black star  */
	*(UnicodeMapFR+phashFR(10036)) = newFmChars("\\x54 ", 2, 10036); 
	/*  ------	 10037 2735 eight pointed pinwheel star  */
	*(UnicodeMapFR+phashFR(10037)) = newFmChars("\\x55 ", 2, 10037); 
	/*  ------	 10038 2736 six pointed black star  */
	*(UnicodeMapFR+phashFR(10038)) = newFmChars("\\x56 ", 2, 10038); 
	/*  ------	 10039 2737 eight pointed rectilinear black star  */
	*(UnicodeMapFR+phashFR(10039)) = newFmChars("\\x57 ", 2, 10039); 
	/*  ------	 10040 2738 heavy eight pointed rectilinear black star  */
	*(UnicodeMapFR+phashFR(10040)) = newFmChars("\\x58 ", 2, 10040); 
	/*  ------	 10041 2739 twelve pointed black star  */
	*(UnicodeMapFR+phashFR(10041)) = newFmChars("\\x59 ", 2, 10041); 
	/*  ------	 10042 273A sixteen pointed asterisk  */
	*(UnicodeMapFR+phashFR(10042)) = newFmChars("\\x5a ", 2, 10042); 
	/*  ------	 10043 273B teardrop-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10043)) = newFmChars("\\x5b ", 2, 10043); 
	/*  ------	 10044 273C open center teardrop-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10044)) = newFmChars("\\x5c ", 2, 10044); 
	/*  ------	 10045 273D heavy teardrop-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10045)) = newFmChars("\\x5d ", 2, 10045); 
	/*  ------	 10046 273E six petalled black and white florette  */
	*(UnicodeMapFR+phashFR(10046)) = newFmChars("\\x5e ", 2, 10046); 
	/*  ------	 10047 273F black florette  */
	*(UnicodeMapFR+phashFR(10047)) = newFmChars("\\x5f ", 2, 10047); 
	/*  ------	 10048 2740 white florette  */
	*(UnicodeMapFR+phashFR(10048)) = newFmChars("\\x60 ", 2, 10048); 
	/*  ------	 10049 2741 eight petalled outlined black florette  */
	*(UnicodeMapFR+phashFR(10049)) = newFmChars("\\x61 ", 2, 10049); 
	/*  ------	 10050 2742 circled open center eight pointed star  */
	*(UnicodeMapFR+phashFR(10050)) = newFmChars("\\x62 ", 2, 10050); 
	/*  ------	 10051 2743 heavy teardrop-spoked pinwheel asterisk  */
	*(UnicodeMapFR+phashFR(10051)) = newFmChars("\\x63 ", 2, 10051); 
	/*  ------	 10052 2744 snowflake  */
	*(UnicodeMapFR+phashFR(10052)) = newFmChars("\\x64 ", 2, 10052); 
	/*  ------	 10053 2745 tight trifoliate snowflake  */
	*(UnicodeMapFR+phashFR(10053)) = newFmChars("\\x65 ", 2, 10053); 
	/*  ------	 10054 2746 heavy chevron snowflake  */
	*(UnicodeMapFR+phashFR(10054)) = newFmChars("\\x66 ", 2, 10054); 
	/*  ------	 10055 2747 sparkle  */
	*(UnicodeMapFR+phashFR(10055)) = newFmChars("\\x67 ", 2, 10055); 
	/*  ------	 10056 2748 heavy sparkle  */
	*(UnicodeMapFR+phashFR(10056)) = newFmChars("\\x68 ", 2, 10056); 
	/*  ------	 10057 2749 balloon-spoked asterisk  */
	*(UnicodeMapFR+phashFR(10057)) = newFmChars("\\x69 ", 2, 10057); 
	/*  ------	 10058 274A eight teardrop-spoked propeller asterisk  */
	*(UnicodeMapFR+phashFR(10058)) = newFmChars("\\x6a ", 2, 10058); 
	/*  ------	 10059 274B heavy eight teardrop-spoked propeller asterisk  */
	*(UnicodeMapFR+phashFR(10059)) = newFmChars("\\x6b ", 2, 10059); 
	/*  ------	 10061 274D shadowed white circle  */
	*(UnicodeMapFR+phashFR(10061)) = newFmChars("\\x6d ", 2, 10061); 
	/*  ------	 10063 274F lower right drop-shadowed white square  */
	*(UnicodeMapFR+phashFR(10063)) = newFmChars("\\x6f ", 2, 10063); 
	/*  ------	 10064 2750 upper right drop-shadowed white square  */
	*(UnicodeMapFR+phashFR(10064)) = newFmChars("\\x70 ", 2, 10064); 
	/*  ------	 10065 2751 lower right shadowed white square  */
	*(UnicodeMapFR+phashFR(10065)) = newFmChars("\\x71 ", 2, 10065); 
	/*  ------	 10066 2752 upper right shadowed white square  */
	*(UnicodeMapFR+phashFR(10066)) = newFmChars("\\x72 ", 2, 10066); 
	/*  ------	 10070 2756 black diamond minus white x  */
	*(UnicodeMapFR+phashFR(10070)) = newFmChars("\\x76 ", 2, 10070); 
	/*  ------	 10072 2758 light vertical bar  */
	*(UnicodeMapFR+phashFR(10072)) = newFmChars("\\x78 ", 2, 10072); 
	/*  ------	 10073 2759 medium vertical bar  */
	*(UnicodeMapFR+phashFR(10073)) = newFmChars("\\x79 ", 2, 10073); 
	/*  ------	 10074 275A heavy vertical bar  */
	*(UnicodeMapFR+phashFR(10074)) = newFmChars("\\x7a ", 2, 10074); 
	/*  ------	 10075 275B heavy single turned comma quotation mark ornament  */
	*(UnicodeMapFR+phashFR(10075)) = newFmChars("\\x7b ", 2, 10075); 
	/*  ------	 10076 275C heavy single comma quotation mark ornament  */
	*(UnicodeMapFR+phashFR(10076)) = newFmChars("\\x7c ", 2, 10076); 
	/*  ------	 10077 275D heavy double turned comma quotation mark ornament  */
	*(UnicodeMapFR+phashFR(10077)) = newFmChars("\\x7d ", 2, 10077); 
	/*  ------	 10078 275E heavy double comma quotation mark ornament  */
	*(UnicodeMapFR+phashFR(10078)) = newFmChars("\\x7e ", 2, 10078); 
	/*  ------	 10081 2761 curved stem paragraph sign ornament  */
	*(UnicodeMapFR+phashFR(10081)) = newFmChars("\\xa1 ", 2, 10081); 
	/*  ------	 10082 2762 heavy exclamation mark ornament  */
	*(UnicodeMapFR+phashFR(10082)) = newFmChars("\\xa2 ", 2, 10082); 
	/*  ------	 10083 2763 heavy heart exclamation mark  */
	*(UnicodeMapFR+phashFR(10083)) = newFmChars("\\xa3 ", 2, 10083); 
	/*  ------	 10084 2764 heavy black heart  */
	*(UnicodeMapFR+phashFR(10084)) = newFmChars("\\xa4 ", 2, 10084); 
	/*  ------	 10085 2765 rotated heavy black heart bullet  */
	*(UnicodeMapFR+phashFR(10085)) = newFmChars("\\xa5 ", 2, 10085); 
	/*  ------	 10086 2766 floral heart  */
	*(UnicodeMapFR+phashFR(10086)) = newFmChars("\\xa6 ", 2, 10086); 
	/*  ------	 10087 2767 rotated floral heart bullet  */
	*(UnicodeMapFR+phashFR(10087)) = newFmChars("\\xa7 ", 2, 10087); 
	/*  ------	 10102 2776 inverse circled digit one  */
	*(UnicodeMapFR+phashFR(10102)) = newFmChars("\\xb6 ", 2, 10102); 
	/*  ------	 10103 2777 inverse circled digit two  */
	*(UnicodeMapFR+phashFR(10103)) = newFmChars("\\xb7 ", 2, 10103); 
	/*  ------	 10104 2778 inverse circled digit three  */
	*(UnicodeMapFR+phashFR(10104)) = newFmChars("\\xb8 ", 2, 10104); 
	/*  ------	 10105 2779 inverse circled digit four  */
	*(UnicodeMapFR+phashFR(10105)) = newFmChars("\\xb9 ", 2, 10105); 
	/*  ------	 10106 277A inverse circled digit five  */
	*(UnicodeMapFR+phashFR(10106)) = newFmChars("\\xba ", 2, 10106); 
	/*  ------	 10107 277B inverse circled digit six  */
	*(UnicodeMapFR+phashFR(10107)) = newFmChars("\\xbb ", 2, 10107); 
	/*  ------	 10108 277C inverse circled digit seven  */
	*(UnicodeMapFR+phashFR(10108)) = newFmChars("\\xbc ", 2, 10108); 
	/*  ------	 10109 277D inverse circled digit eight  */
	*(UnicodeMapFR+phashFR(10109)) = newFmChars("\\xbd ", 2, 10109); 
	/*  ------	 10110 277E inverse circled digit nine  */
	*(UnicodeMapFR+phashFR(10110)) = newFmChars("\\xbe ", 2, 10110); 
	/*  ------	 10111 277F inverse circled number ten  */
	*(UnicodeMapFR+phashFR(10111)) = newFmChars("\\xbf ", 2, 10111); 
	/*  ------	 10112 2780 circled saSMS-serif digit one  */
	*(UnicodeMapFR+phashFR(10112)) = newFmChars("\\xc0 ", 2, 10112); 
	/*  ------	 10113 2781 circled saSMS-serif digit two  */
	*(UnicodeMapFR+phashFR(10113)) = newFmChars("\\xc1 ", 2, 10113); 
	/*  ------	 10114 2782 circled saSMS-serif digit three  */
	*(UnicodeMapFR+phashFR(10114)) = newFmChars("\\xc2 ", 2, 10114); 
	/*  ------	 10115 2783 circled saSMS-serif digit four  */
	*(UnicodeMapFR+phashFR(10115)) = newFmChars("\\xc3 ", 2, 10115); 
	/*  ------	 10116 2784 circled saSMS-serif digit five  */
	*(UnicodeMapFR+phashFR(10116)) = newFmChars("\\xc4 ", 2, 10116); 
	/*  ------	 10117 2785 circled saSMS-serif digit six  */
	*(UnicodeMapFR+phashFR(10117)) = newFmChars("\\xc5 ", 2, 10117); 
	/*  ------	 10118 2786 circled saSMS-serif digit seven  */
	*(UnicodeMapFR+phashFR(10118)) = newFmChars("\\xc6 ", 2, 10118); 
	/*  ------	 10119 2787 circled saSMS-serif digit nine  */
	*(UnicodeMapFR+phashFR(10119)) = newFmChars("\\xc7 ", 2, 10119); 
	/*  ------	 10121 2789 circled saSMS-serif number ten  */
	*(UnicodeMapFR+phashFR(10121)) = newFmChars("\\xc9 ", 2, 10121); 
	/*  ------	 10122 278A inverse circled saSMS-serif digit one  */
	*(UnicodeMapFR+phashFR(10122)) = newFmChars("\\xca ", 2, 10122); 
	/*  ------	 10123 278B inverse circled saSMS-serif digit two  */
	*(UnicodeMapFR+phashFR(10123)) = newFmChars("\\xcb ", 2, 10123); 
	/*  ------	 10124 278C inverse circled saSMS-serif digit three  */
	*(UnicodeMapFR+phashFR(10124)) = newFmChars("\\xcc ", 2, 10124); 
	/*  ------	 10125 278D inverse circled saSMS-serif digit four  */
	*(UnicodeMapFR+phashFR(10125)) = newFmChars("\\xcd ", 2, 10125); 
	/*  ------	 10126 278E inverse circled saSMS-serif digit five  */
	*(UnicodeMapFR+phashFR(10126)) = newFmChars("\\xce ", 2, 10126); 
	/*  ------	 10127 278F inverse circled saSMS-serif digit six  */
	*(UnicodeMapFR+phashFR(10127)) = newFmChars("\\xcf ", 2, 10127); 
	/*  ------	 10129 2791 inverse circled saSMS-serif digit eight  */
	*(UnicodeMapFR+phashFR(10129)) = newFmChars("\\xd1 ", 2, 10129); 
	/*  ------	 10130 2792 inverse circled saSMS-serif digit nine  */
	*(UnicodeMapFR+phashFR(10130)) = newFmChars("\\xd2 ", 2, 10130); 
	/*  ------	 10131 2793 inverse circled saSMS-serif number ten  */
	*(UnicodeMapFR+phashFR(10131)) = newFmChars("\\xd3 ", 2, 10131); 
	/*  ------	 10132 2794 heavy wide-headed right arrow  */
	*(UnicodeMapFR+phashFR(10132)) = newFmChars("\\xd4 ", 2, 10132); 
	/*  ------	 10139 279B drafting point right arrow  */
	*(UnicodeMapFR+phashFR(10139)) = newFmChars("\\xdb ", 2, 10139); 
	/*  ------	 10136 2798 heavy lower right arrow  */
	*(UnicodeMapFR+phashFR(10136)) = newFmChars("\\xd8 ", 2, 10136); 
	/*  ------	 10137 2799 heavy right arrow  */
	*(UnicodeMapFR+phashFR(10137)) = newFmChars("\\xd9 ", 2, 10137); 
	/*  ------	 10138 279A heavy upper right arrow  */
	*(UnicodeMapFR+phashFR(10138)) = newFmChars("\\xda ", 2, 10138); 
	/*  ------	 10140 279C heavy round-tipped right arrow  */
	*(UnicodeMapFR+phashFR(10140)) = newFmChars("\\xdc ", 2, 10140); 
	/*  ------	 10141 279D triangle-headed right arrow  */
	*(UnicodeMapFR+phashFR(10141)) = newFmChars("\\xdd ", 2, 10141); 
	/*  ------	 10142 279E heavy triangle-headed right arrow  */
	*(UnicodeMapFR+phashFR(10142)) = newFmChars("\\xde ", 2, 10142); 
	/*  ------	 10143 279F dashed triangle-headed right arrow  */
	*(UnicodeMapFR+phashFR(10143)) = newFmChars("\\xdf ", 2, 10143); 
	/*  ------	 10144 27A0 heavy dashed triangle-headed right arrow  */
	*(UnicodeMapFR+phashFR(10144)) = newFmChars("\\xe0 ", 2, 10144); 
	/*  ------	 10145 27A1 black right arrow  */
	*(UnicodeMapFR+phashFR(10145)) = newFmChars("\\xe1 ", 2, 10145); 
	/*  ------	 10146 27A2 three-d top-lighted right arrowhead  */
	*(UnicodeMapFR+phashFR(10146)) = newFmChars("\\xe2 ", 2, 10146); 
	/*  ------	 10147 27A3 three-d bottom-lighted right arrowhead  */
	*(UnicodeMapFR+phashFR(10147)) = newFmChars("\\xe3 ", 2, 10147); 
	/*  ------	 10148 27A4 black right arrowhead  */
	*(UnicodeMapFR+phashFR(10148)) = newFmChars("\\xe4 ", 2, 10148); 
	/*  ------	 10149 27A5 heavy black curved down and right arrow  */
	*(UnicodeMapFR+phashFR(10149)) = newFmChars("\\xe5 ", 2, 10149); 
	/*  ------	 10150 27A6 heavy black curved up and right arrow  */
	*(UnicodeMapFR+phashFR(10150)) = newFmChars("\\xe6 ", 2, 10150); 
	/*  ------	 10151 27A7 squat black right arrow  */
	*(UnicodeMapFR+phashFR(10151)) = newFmChars("\\xe7 ", 2, 10151); 
	/*  ------	 10152 27A8 heavy concave-pointed black right arrow  */
	*(UnicodeMapFR+phashFR(10152)) = newFmChars("\\xe8 ", 2, 10152); 
	/*  ------	 10153 27A9 right-shaded white right arrow  */
	*(UnicodeMapFR+phashFR(10153)) = newFmChars("\\xe9 ", 2, 10153); 
	/*  ------	 10154 27AA left-shaded white right arrow  */
	*(UnicodeMapFR+phashFR(10154)) = newFmChars("\\xea ", 2, 10154); 
	/*  ------	 10155 27AB back-tilted shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10155)) = newFmChars("\\xeb ", 2, 10155); 
	/*  ------	 10156 27AC front-tilted shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10156)) = newFmChars("\\xec ", 2, 10156); 
	/*  ------	 10157 27AD heavy lower right-shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10157)) = newFmChars("\\xed ", 2, 10157); 
	/*  ------	 10158 27AE heavy upper right-shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10158)) = newFmChars("\\xee ", 2, 10158); 
	/*  ------	 10159 27AF notched lower right-shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10159)) = newFmChars("\\xef ", 2, 10159); 
	/*  ------	 10161 27B1 notched upper right-shadowed white right arrow  */
	*(UnicodeMapFR+phashFR(10161)) = newFmChars("\\xf1 ", 2, 10161); 
	/*  ------	 10162 27B2 circled heavy white right arrow  */
	*(UnicodeMapFR+phashFR(10162)) = newFmChars("\\xf2 ", 2, 10162); 
	/*  ------	 10163 27B3 white-feathered right arrow  */
	*(UnicodeMapFR+phashFR(10163)) = newFmChars("\\xf3 ", 2, 10163); 
	/*  ------	 10164 27B4 black-feathered lower right arrow  */
	*(UnicodeMapFR+phashFR(10164)) = newFmChars("\\xf4 ", 2, 10164); 
	/*  ------	 10165 27B5 black-feathered right arrow  */
	*(UnicodeMapFR+phashFR(10165)) = newFmChars("\\xf5 ", 2, 10165); 
	/*  ------	 10166 27B6 black-feathered upper right arrow  */
	*(UnicodeMapFR+phashFR(10166)) = newFmChars("\\xf6 ", 2, 10166); 
	/*  ------	 10167 27B7 heavy black-feathered lower right arrow  */
	*(UnicodeMapFR+phashFR(10167)) = newFmChars("\\xf7 ", 2, 10167); 
	/*  ------	 10168 27B8 heavy black-feathered right arrow  */
	*(UnicodeMapFR+phashFR(10168)) = newFmChars("\\xf8 ", 2, 10168); 
	/*  ------	 10169 27B9 heavy black-feathered upper right arrow  */
	*(UnicodeMapFR+phashFR(10169)) = newFmChars("\\xf9 ", 2, 10169); 
	/*  ------	 10170 27BA teardrop-barbed right arrow  */
	*(UnicodeMapFR+phashFR(10170)) = newFmChars("\\xfa ", 2, 10170); 
	/*  ------	 10171 27BB heavy teardrop-shanked right arrow  */
	*(UnicodeMapFR+phashFR(10171)) = newFmChars("\\xfb ", 2, 10171); 
	/*  ------	 10172 27BC wedge-tailed right arrow  */
	*(UnicodeMapFR+phashFR(10172)) = newFmChars("\\xfc ", 2, 10172); 
	/*  ------	 10173 27BD heavy wedge-tailed right arrow  */
	*(UnicodeMapFR+phashFR(10173)) = newFmChars("\\xfd ", 2, 10173); 
	/*  ------	 10174 27BE open-outlined right arrow  */
	*(UnicodeMapFR+phashFR(10174)) = newFmChars("\\xfe ", 2, 10174); 
	/*  ------	 733 02DD spacing double acute  */
	*(UnicodeMapFR+phashFR(733)) = newFmChars("\\xfd ", 0, 733); 
	/*  ------	 338 0152 latin capital letter O E  */
	*(UnicodeMapFR+phashFR(338)) = newFmChars("\\xce ", 0, 338); 
	/*  ------	 339 0153 latin small letter o e  */
	*(UnicodeMapFR+phashFR(339)) = newFmChars("\\xcf ", 0, 339); 
	/*  ------	 352 0160 latin capital letter s hacek  */
	*(UnicodeMapFR+phashFR(352)) = newFmChars("\\xf0 ", 0, 352); 
	/*  ------	 VERSION 6.0.0p38: added filig and fllig mappings */
	/*  ------	filig */
	*(UnicodeMapFR+phashFR(64257)) = newFmChars("\\x7a ", 0, 64257); 
	/*  ------	fllig */
	*(UnicodeMapFR+phashFR(64258)) = newFmChars("\\xdf ", 0, 64258); 

	return(UnicodeMapFR);
}
