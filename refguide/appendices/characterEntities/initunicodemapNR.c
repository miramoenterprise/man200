#include <stdio.h>
#include "./phashNR.h"
#include "./fmchars.h"
#include "../utils/memory.h"

fmCharsHandle *
initUnicodeMapNR(void)
{
	fmCharsHandle *
		UnicodeMapNR=(fmCharsHandle *)smalloc(sizeof(fmCharsHandle) * PHASHNKEYS_phashNR);

	/*  This file contains a number of entries which map unicode numbers */
	/*  onto their MIF string equivalents. */
	/*  */
	/* 	{entry} */
	/* 		: {unumspec}? {fmhex} {ff} */
	/*  */
	/* 	{unumspec} */
	/* 		: {unum}[UW]? */
	/* 		| {unum},{unum} */
	/*  */
	/* 	{unum} */
	/* 		: [0-9]+ */
	/*  */
	/* 	{fmhex} */
	/* 		: "\\xhh " | = */
	/*  */
	/* 	{ff} */
	/* 		: ZapfDingbats */
	/* 		| Symbol */
	/*  */
	/* 	Column 1 specifies unicode numbers (decimal). */
	/* 	A single unicode number may be followed by U or W for */
	/* 	platform specific mapping */
	/* 	A ranges of unicode numbers is specified as comma separated decimal values. */
	/* 	  */
	/* 	Column 2 gives the MIF hex string used by FrameMaker to represent the */
	/* 	character, or = to map the unicode number directly onto its hex equivalent. */
	/*  */
	/* 	Column 3 specifies the font family required to switch to for that character */
	/*  */
	/*  ------	 8232 hard return  */
	*(UnicodeMapNR+phashNR(10)) = newFmChars("\\r", 0, 10); 
	*(UnicodeMapNR+phashNR(8232)) = newFmChars("\\r", 0, 8232); 
	/*  ------	 9 tab  */
	*(UnicodeMapNR+phashNR(9)) = newFmChars("\\x08 ", 0, 9); 
	/*  ------	 Unicode numbers 32 [space] to 125 [}] map directly onto fmhex  */
	/*  ------         VERSION 7.2.0p40: map tilde (126) to \\x7e  */
	*(UnicodeMapNR+phashNR(32)) = newFmChars("\\x20 ", 0, 32); 
	*(UnicodeMapNR+phashNR(33)) = newFmChars("\\x21 ", 0, 33); 
	*(UnicodeMapNR+phashNR(34)) = newFmChars("\\x22 ", 0, 34); 
	*(UnicodeMapNR+phashNR(35)) = newFmChars("\\x23 ", 0, 35); 
	*(UnicodeMapNR+phashNR(36)) = newFmChars("\\x24 ", 0, 36); 
	*(UnicodeMapNR+phashNR(37)) = newFmChars("\\x25 ", 0, 37); 
	*(UnicodeMapNR+phashNR(38)) = newFmChars("\\x26 ", 0, 38); 
	*(UnicodeMapNR+phashNR(39)) = newFmChars("\\x27 ", 0, 39); 
	*(UnicodeMapNR+phashNR(40)) = newFmChars("\\x28 ", 0, 40); 
	*(UnicodeMapNR+phashNR(41)) = newFmChars("\\x29 ", 0, 41); 
	*(UnicodeMapNR+phashNR(42)) = newFmChars("\\x2a ", 0, 42); 
	*(UnicodeMapNR+phashNR(43)) = newFmChars("\\x2b ", 0, 43); 
	*(UnicodeMapNR+phashNR(44)) = newFmChars("\\x2c ", 0, 44); 
	*(UnicodeMapNR+phashNR(45)) = newFmChars("\\x2d ", 0, 45); 
	*(UnicodeMapNR+phashNR(46)) = newFmChars("\\x2e ", 0, 46); 
	*(UnicodeMapNR+phashNR(47)) = newFmChars("\\x2f ", 0, 47); 
	*(UnicodeMapNR+phashNR(48)) = newFmChars("\\x30 ", 0, 48); 
	*(UnicodeMapNR+phashNR(49)) = newFmChars("\\x31 ", 0, 49); 
	*(UnicodeMapNR+phashNR(50)) = newFmChars("\\x32 ", 0, 50); 
	*(UnicodeMapNR+phashNR(51)) = newFmChars("\\x33 ", 0, 51); 
	*(UnicodeMapNR+phashNR(52)) = newFmChars("\\x34 ", 0, 52); 
	*(UnicodeMapNR+phashNR(53)) = newFmChars("\\x35 ", 0, 53); 
	*(UnicodeMapNR+phashNR(54)) = newFmChars("\\x36 ", 0, 54); 
	*(UnicodeMapNR+phashNR(55)) = newFmChars("\\x37 ", 0, 55); 
	*(UnicodeMapNR+phashNR(56)) = newFmChars("\\x38 ", 0, 56); 
	*(UnicodeMapNR+phashNR(57)) = newFmChars("\\x39 ", 0, 57); 
	*(UnicodeMapNR+phashNR(58)) = newFmChars("\\x3a ", 0, 58); 
	*(UnicodeMapNR+phashNR(59)) = newFmChars("\\x3b ", 0, 59); 
	*(UnicodeMapNR+phashNR(60)) = newFmChars("\\x3c ", 0, 60); 
	*(UnicodeMapNR+phashNR(61)) = newFmChars("\\x3d ", 0, 61); 
	*(UnicodeMapNR+phashNR(62)) = newFmChars("\\x3e ", 0, 62); 
	*(UnicodeMapNR+phashNR(63)) = newFmChars("\\x3f ", 0, 63); 
	*(UnicodeMapNR+phashNR(64)) = newFmChars("\\x40 ", 0, 64); 
	*(UnicodeMapNR+phashNR(65)) = newFmChars("\\x41 ", 0, 65); 
	*(UnicodeMapNR+phashNR(66)) = newFmChars("\\x42 ", 0, 66); 
	*(UnicodeMapNR+phashNR(67)) = newFmChars("\\x43 ", 0, 67); 
	*(UnicodeMapNR+phashNR(68)) = newFmChars("\\x44 ", 0, 68); 
	*(UnicodeMapNR+phashNR(69)) = newFmChars("\\x45 ", 0, 69); 
	*(UnicodeMapNR+phashNR(70)) = newFmChars("\\x46 ", 0, 70); 
	*(UnicodeMapNR+phashNR(71)) = newFmChars("\\x47 ", 0, 71); 
	*(UnicodeMapNR+phashNR(72)) = newFmChars("\\x48 ", 0, 72); 
	*(UnicodeMapNR+phashNR(73)) = newFmChars("\\x49 ", 0, 73); 
	*(UnicodeMapNR+phashNR(74)) = newFmChars("\\x4a ", 0, 74); 
	*(UnicodeMapNR+phashNR(75)) = newFmChars("\\x4b ", 0, 75); 
	*(UnicodeMapNR+phashNR(76)) = newFmChars("\\x4c ", 0, 76); 
	*(UnicodeMapNR+phashNR(77)) = newFmChars("\\x4d ", 0, 77); 
	*(UnicodeMapNR+phashNR(78)) = newFmChars("\\x4e ", 0, 78); 
	*(UnicodeMapNR+phashNR(79)) = newFmChars("\\x4f ", 0, 79); 
	*(UnicodeMapNR+phashNR(80)) = newFmChars("\\x50 ", 0, 80); 
	*(UnicodeMapNR+phashNR(81)) = newFmChars("\\x51 ", 0, 81); 
	*(UnicodeMapNR+phashNR(82)) = newFmChars("\\x52 ", 0, 82); 
	*(UnicodeMapNR+phashNR(83)) = newFmChars("\\x53 ", 0, 83); 
	*(UnicodeMapNR+phashNR(84)) = newFmChars("\\x54 ", 0, 84); 
	*(UnicodeMapNR+phashNR(85)) = newFmChars("\\x55 ", 0, 85); 
	*(UnicodeMapNR+phashNR(86)) = newFmChars("\\x56 ", 0, 86); 
	*(UnicodeMapNR+phashNR(87)) = newFmChars("\\x57 ", 0, 87); 
	*(UnicodeMapNR+phashNR(88)) = newFmChars("\\x58 ", 0, 88); 
	*(UnicodeMapNR+phashNR(89)) = newFmChars("\\x59 ", 0, 89); 
	*(UnicodeMapNR+phashNR(90)) = newFmChars("\\x5a ", 0, 90); 
	*(UnicodeMapNR+phashNR(91)) = newFmChars("\\x5b ", 0, 91); 
	*(UnicodeMapNR+phashNR(92)) = newFmChars("\\x5c ", 0, 92); 
	*(UnicodeMapNR+phashNR(93)) = newFmChars("\\x5d ", 0, 93); 
	*(UnicodeMapNR+phashNR(94)) = newFmChars("\\x5e ", 0, 94); 
	*(UnicodeMapNR+phashNR(95)) = newFmChars("\\x5f ", 0, 95); 
	*(UnicodeMapNR+phashNR(96)) = newFmChars("\\x60 ", 0, 96); 
	*(UnicodeMapNR+phashNR(97)) = newFmChars("\\x61 ", 0, 97); 
	*(UnicodeMapNR+phashNR(98)) = newFmChars("\\x62 ", 0, 98); 
	*(UnicodeMapNR+phashNR(99)) = newFmChars("\\x63 ", 0, 99); 
	*(UnicodeMapNR+phashNR(100)) = newFmChars("\\x64 ", 0, 100); 
	*(UnicodeMapNR+phashNR(101)) = newFmChars("\\x65 ", 0, 101); 
	*(UnicodeMapNR+phashNR(102)) = newFmChars("\\x66 ", 0, 102); 
	*(UnicodeMapNR+phashNR(103)) = newFmChars("\\x67 ", 0, 103); 
	*(UnicodeMapNR+phashNR(104)) = newFmChars("\\x68 ", 0, 104); 
	*(UnicodeMapNR+phashNR(105)) = newFmChars("\\x69 ", 0, 105); 
	*(UnicodeMapNR+phashNR(106)) = newFmChars("\\x6a ", 0, 106); 
	*(UnicodeMapNR+phashNR(107)) = newFmChars("\\x6b ", 0, 107); 
	*(UnicodeMapNR+phashNR(108)) = newFmChars("\\x6c ", 0, 108); 
	*(UnicodeMapNR+phashNR(109)) = newFmChars("\\x6d ", 0, 109); 
	*(UnicodeMapNR+phashNR(110)) = newFmChars("\\x6e ", 0, 110); 
	*(UnicodeMapNR+phashNR(111)) = newFmChars("\\x6f ", 0, 111); 
	*(UnicodeMapNR+phashNR(112)) = newFmChars("\\x70 ", 0, 112); 
	*(UnicodeMapNR+phashNR(113)) = newFmChars("\\x71 ", 0, 113); 
	*(UnicodeMapNR+phashNR(114)) = newFmChars("\\x72 ", 0, 114); 
	*(UnicodeMapNR+phashNR(115)) = newFmChars("\\x73 ", 0, 115); 
	*(UnicodeMapNR+phashNR(116)) = newFmChars("\\x74 ", 0, 116); 
	*(UnicodeMapNR+phashNR(117)) = newFmChars("\\x75 ", 0, 117); 
	*(UnicodeMapNR+phashNR(118)) = newFmChars("\\x76 ", 0, 118); 
	*(UnicodeMapNR+phashNR(119)) = newFmChars("\\x77 ", 0, 119); 
	*(UnicodeMapNR+phashNR(120)) = newFmChars("\\x78 ", 0, 120); 
	*(UnicodeMapNR+phashNR(121)) = newFmChars("\\x79 ", 0, 121); 
	*(UnicodeMapNR+phashNR(122)) = newFmChars("\\x7a ", 0, 122); 
	*(UnicodeMapNR+phashNR(123)) = newFmChars("\\x7b ", 0, 123); 
	*(UnicodeMapNR+phashNR(124)) = newFmChars("\\x7c ", 0, 124); 
	*(UnicodeMapNR+phashNR(125)) = newFmChars("\\x7d ", 0, 125); 
	*(UnicodeMapNR+phashNR(126)) = newFmChars("\\x7e ", 0, 126); 
	/*  ------	 126 007E tilde */
	/*  ------         VERSION 7.2.0p40: don't map tilde to \\xf7 */
	/*  126   "\\xf7 " - */
	/*  ------	 160 00A0 non-breaking space  */
	*(UnicodeMapNR+phashNR(160)) = newFmChars("\\x11 ", 0, 160); 
	/*  ------	 167 00A7 section sign  */
	*(UnicodeMapNR+phashNR(167)) = newFmChars("\\xa4 ", 0, 167); 
	/*  ------	 169 00A9 copyright sign  */
	*(UnicodeMapNR+phashNR(169)) = newFmChars("\\xa9 ", 0, 169); 
	/*  ------	 171 00AB left pointing guillemet  */
	*(UnicodeMapNR+phashNR(171)) = newFmChars("\\xc7 ", 0, 171); 
	/*  ------	 172 00AC not sign  */
	*(UnicodeMapNR+phashNR(172)) = newFmChars("\\xc2 ", 0, 172); 
	/*  ------	 173 00AD soft hyphen  */
	*(UnicodeMapNR+phashNR(173)) = newFmChars("\\x06 ", 0, 173); 
	/*  ------	 174 00AE registered trade mark sign  */
	*(UnicodeMapNR+phashNR(174)) = newFmChars("\\xa8 ", 0, 174); 
	/*  ------	 176 00B0 degree sign  */
	/*  VERSION 6.2.0p02: apply mapping for Unix only */
	/*  VERSION 6.2.0p03: apply mapping for Unix AND Windows only */
	*(UnicodeMapNR+phashNR(176)) = newFmChars("\\xb0 ", 1, 176); 
	/*  ------	 177 00B1 plus-or-minus sign  */
	/*  VERSION 6.2.0p02: apply mapping for Unix only */
#ifndef WIN32
	*(UnicodeMapNR+phashNR(177)) = newFmChars("\\xb1 ", 1, 177); 
#endif
#ifdef WIN32
	*(UnicodeMapNR+phashNR(177)) = newFmChars("\\xb1 ", 0, 177); 
#endif
	/*  ------	 181 00B5 micro sign  */
	/*  VERSION 6.2.0p02: remove mapping for b5 */
	/*  VERSION 6.2.0p04: re-add mapping for b5 */
#ifdef WIN32
	*(UnicodeMapNR+phashNR(181)) = newFmChars("\\x6d ", 1, 181); 
#endif
#ifndef WIN32
	*(UnicodeMapNR+phashNR(181)) = newFmChars("\\xb5 ", 0, 181); 
#endif
	/*  ------	 182 00B6 paragraph sign  */
	*(UnicodeMapNR+phashNR(182)) = newFmChars("\\xa6 ", 0, 182); 
	/*  ------	 183 00B7 middle dot  */
	*(UnicodeMapNR+phashNR(183)) = newFmChars("\\xe1 ", 0, 183); 
	/*  ------	 913 0391 greek capital letter alpha  */
	*(UnicodeMapNR+phashNR(913)) = newFmChars("\\x41 ", 1, 913); 
	/*  ------	 914 0392 greek capital letter beta  */
	*(UnicodeMapNR+phashNR(914)) = newFmChars("\\x42 ", 1, 914); 
	/*  ------	 915 0393 greek capital letter gamma  */
	*(UnicodeMapNR+phashNR(915)) = newFmChars("\\x47 ", 1, 915); 
	/*  ------	 916 0394 greek capital letter delta  */
	*(UnicodeMapNR+phashNR(916)) = newFmChars("\\x44 ", 1, 916); 
	/*  ------	 917 0395 greek capital letter epsilon  */
	*(UnicodeMapNR+phashNR(917)) = newFmChars("\\x45 ", 1, 917); 
	/*  ------	 918 0396 greek capital letter zeta  */
	*(UnicodeMapNR+phashNR(918)) = newFmChars("\\x5a ", 1, 918); 
	/*  ------	 919 0397 greek capital letter eta  */
	*(UnicodeMapNR+phashNR(919)) = newFmChars("\\x48 ", 1, 919); 
	/*  ------	 920 0398 greek capital letter theta  */
	*(UnicodeMapNR+phashNR(920)) = newFmChars("\\x51 ", 1, 920); 
	/*  ------	 921 0399 greek capital letter iota  */
	*(UnicodeMapNR+phashNR(921)) = newFmChars("\\x49 ", 1, 921); 
	/*  ------	 922 039A greek capital letter kappa  */
	*(UnicodeMapNR+phashNR(922)) = newFmChars("\\x4b ", 1, 922); 
	/*  ------	 923 039B greek capital letter lambda  */
	*(UnicodeMapNR+phashNR(923)) = newFmChars("\\x4c ", 1, 923); 
	/*  ------	 924 039C greek capital letter mu  */
	*(UnicodeMapNR+phashNR(924)) = newFmChars("\\x4d ", 1, 924); 
	/*  ------	 925 039D greek capital letter nu  */
	*(UnicodeMapNR+phashNR(925)) = newFmChars("\\x4e ", 1, 925); 
	/*  ------	 926 039E greek capital letter xi  */
	*(UnicodeMapNR+phashNR(926)) = newFmChars("\\x58 ", 1, 926); 
	/*  ------	 927 039F greek capital letter omicron  */
	*(UnicodeMapNR+phashNR(927)) = newFmChars("\\x4f ", 1, 927); 
	/*  ------	 928 03A0 greek capital letter pi  */
	*(UnicodeMapNR+phashNR(928)) = newFmChars("\\x50 ", 1, 928); 
	/*  ------	 929 03A1 greek capital letter rho  */
	*(UnicodeMapNR+phashNR(929)) = newFmChars("\\x52 ", 1, 929); 
	/*  ------	 931 03A3 greek capital letter sigma  */
	*(UnicodeMapNR+phashNR(931)) = newFmChars("\\x53 ", 1, 931); 
	/*  ------	 932 03A4 greek capital letter tau  */
	*(UnicodeMapNR+phashNR(932)) = newFmChars("\\x54 ", 1, 932); 
	/*  ------	 933 03A5 greek capital letter upsilon  */
	*(UnicodeMapNR+phashNR(933)) = newFmChars("\\x55 ", 1, 933); 
	/*  ------	 934 03A6 greek capital letter phi  */
	*(UnicodeMapNR+phashNR(934)) = newFmChars("\\x46 ", 1, 934); 
	/*  ------	 935 03A7 greek capital letter chi  */
	*(UnicodeMapNR+phashNR(935)) = newFmChars("\\x43 ", 1, 935); 
	/*  ------	 936 03A8 greek capital letter psi  */
	*(UnicodeMapNR+phashNR(936)) = newFmChars("\\x59 ", 1, 936); 
	/*  ------	 937 03A9 greek capital letter omega  */
	*(UnicodeMapNR+phashNR(937)) = newFmChars("\\x57 ", 1, 937); 
	/*  ------	 945 03B1 greek small letter alpha  */
	*(UnicodeMapNR+phashNR(945)) = newFmChars("\\x61 ", 1, 945); 
	/*  ------	 946 03B2 greek small letter beta  */
	*(UnicodeMapNR+phashNR(946)) = newFmChars("\\x62 ", 1, 946); 
	/*  ------	 947 03B3 greek small letter gamma  */
	*(UnicodeMapNR+phashNR(947)) = newFmChars("\\x67 ", 1, 947); 
	/*  ------	 948 03B4 greek small letter delta  */
	*(UnicodeMapNR+phashNR(948)) = newFmChars("\\x64 ", 1, 948); 
	/*  ------	 949 03B5 greek small letter epsilon  */
	*(UnicodeMapNR+phashNR(949)) = newFmChars("\\x65 ", 1, 949); 
	/*  ------	 950 03B6 greek small letter zeta  */
	*(UnicodeMapNR+phashNR(950)) = newFmChars("\\x7a ", 1, 950); 
	/*  ------	 951 03B7 greek small letter eta  */
	*(UnicodeMapNR+phashNR(951)) = newFmChars("\\x68 ", 1, 951); 
	/*  ------	 952 03B8 greek small letter theta  */
	*(UnicodeMapNR+phashNR(952)) = newFmChars("\\x71 ", 1, 952); 
	/*  ------	 953 03B9 greek small letter iota  */
	*(UnicodeMapNR+phashNR(953)) = newFmChars("\\x69 ", 1, 953); 
	/*  ------	 954 03BA greek small letter kappa  */
	*(UnicodeMapNR+phashNR(954)) = newFmChars("\\x6b ", 1, 954); 
	/*  ------	 955 03BB greek small letter lambda  */
	*(UnicodeMapNR+phashNR(955)) = newFmChars("\\x6c ", 1, 955); 
	/*  ------	 956 03BC greek small letter mu  */
	*(UnicodeMapNR+phashNR(956)) = newFmChars("\\x6d ", 1, 956); 
	/*  ------	 957 03BD greek small letter nu  */
	*(UnicodeMapNR+phashNR(957)) = newFmChars("\\x6e ", 1, 957); 
	/*  ------	 958 03BE greek small letter xi  */
	*(UnicodeMapNR+phashNR(958)) = newFmChars("\\x78 ", 1, 958); 
	/*  ------	 959 03BF greek small letter omicron  */
	*(UnicodeMapNR+phashNR(959)) = newFmChars("\\x6f ", 1, 959); 
	/*  ------	 960 03C0 greek small letter pi  */
	*(UnicodeMapNR+phashNR(960)) = newFmChars("\\x70 ", 1, 960); 
	/*  ------	 961 03C1 greek small letter rho  */
	*(UnicodeMapNR+phashNR(961)) = newFmChars("\\x72 ", 1, 961); 
	/*  ------	 962 03C2 greek small letter final sigma  */
	*(UnicodeMapNR+phashNR(962)) = newFmChars("\\x56 ", 1, 962); 
	/*  ------	 963 03C3 greek small letter sigma  */
	*(UnicodeMapNR+phashNR(963)) = newFmChars("\\x73 ", 1, 963); 
	/*  ------	 964 03C4 greek small letter tau  */
	*(UnicodeMapNR+phashNR(964)) = newFmChars("\\x74 ", 1, 964); 
	/*  ------	 965 03C5 greek small letter upsilon  */
	*(UnicodeMapNR+phashNR(965)) = newFmChars("\\x75 ", 1, 965); 
	/*  ------	 966 03C6 greek small letter phi  */
	*(UnicodeMapNR+phashNR(966)) = newFmChars("\\x66 ", 1, 966); 
	/*  ------	 967 03C7 greek small letter chi  */
	*(UnicodeMapNR+phashNR(967)) = newFmChars("\\x63 ", 1, 967); 
	/*  ------	 968 03C8 greek small letter psi  */
	*(UnicodeMapNR+phashNR(968)) = newFmChars("\\x79 ", 1, 968); 
	/*  ------	 969 03C9 greek small letter omega  */
	*(UnicodeMapNR+phashNR(969)) = newFmChars("\\x77 ", 1, 969); 
	/*  ------	 977 03D1 greek small letter script theta  */
	*(UnicodeMapNR+phashNR(977)) = newFmChars("\\x4a ", 1, 977); 
	/*  ------	 978 03D2 greek capital letter upsilon hook  */
	*(UnicodeMapNR+phashNR(978)) = newFmChars("\\xa1 ", 1, 978); 
	/*  ------	 981 03D5 greek small letter script phi  */
	*(UnicodeMapNR+phashNR(981)) = newFmChars("\\x6a ", 1, 981); 
	/*  ------	 982 03D6 greek small letter omega pi  */
	*(UnicodeMapNR+phashNR(982)) = newFmChars("\\x76 ", 1, 982); 
	/*  ------	 8194 2002 en space  */
	*(UnicodeMapNR+phashNR(8194)) = newFmChars("\\x13 ", 0, 8194); 
	/*  ------	 8195 2003 em space  */
	*(UnicodeMapNR+phashNR(8195)) = newFmChars("\\x14 ", 0, 8195); 
	/*  ------	 8199 2007 figure space (numeric space   */
	*(UnicodeMapNR+phashNR(8199)) = newFmChars("\\x10 ", 0, 8199); 
	/*  ------	 8201 2009 thin space (1/6 em)  */
	*(UnicodeMapNR+phashNR(8201)) = newFmChars("\\x12 \\x12 ", 0, 8201); 
	/*  ------	 8202 200A hair space (1/12 em)  */
	*(UnicodeMapNR+phashNR(8202)) = newFmChars("\\x12 ", 0, 8202); 
	/*  ------	 8209 2011 non-breaking hyphen  */
	*(UnicodeMapNR+phashNR(8209)) = newFmChars("\\x15 ", 0, 8209); 
	/*  ------	 8211 2013 en dash  */
	*(UnicodeMapNR+phashNR(8211)) = newFmChars("\\xd0 ", 0, 8211); 
	/*  ------	 8212 2014 em dash  */
	*(UnicodeMapNR+phashNR(8212)) = newFmChars("\\xd1 ", 0, 8212); 
	/*  ------	 8216 2018 single turned comma quotation mark  */
	*(UnicodeMapNR+phashNR(8216)) = newFmChars("\\xd4 ", 0, 8216); 
	/*  ------	 8217 2019 single comma quotation mark  */
	*(UnicodeMapNR+phashNR(8217)) = newFmChars("\\xd5 ", 0, 8217); 
	/*  ------	 8218 201A low single comma quotation mark  */
	*(UnicodeMapNR+phashNR(8218)) = newFmChars("\\xe2 ", 0, 8218); 
	/*  ------	 8220 201C double turned comma quotation mark  */
	*(UnicodeMapNR+phashNR(8220)) = newFmChars("\\xd2 ", 0, 8220); 
	/*  ------	 8221 201D double comma quotation mark  */
	*(UnicodeMapNR+phashNR(8221)) = newFmChars("\\xd3 ", 0, 8221); 
	/*  ------	 8222 201E low double comma quotation mark  */
	*(UnicodeMapNR+phashNR(8222)) = newFmChars("\\xe3 ", 0, 8222); 
	/*  ------	 8224 2020 dagger  */
	*(UnicodeMapNR+phashNR(8224)) = newFmChars("\\xa0 ", 0, 8224); 
	/*  ------	 8225 2021 double dagger  */
	*(UnicodeMapNR+phashNR(8225)) = newFmChars("\\xe0 ", 0, 8225); 
	/*  ------	 8226 2022 bullet  */
	*(UnicodeMapNR+phashNR(8226)) = newFmChars("\\xa5 ", 0, 8226); 
	/*  ------	 8231 2027 hyphenation point  */
	/*  ------	 VERSION 5.4.1p38: added #x2027; for discretionary hyphen  */
	*(UnicodeMapNR+phashNR(8231)) = newFmChars("\\x04 ", 0, 8231); 
	/*  ------	 8233 2029 paragraph separator  */
	*(UnicodeMapNR+phashNR(8233)) = newFmChars("\\x0a ", 0, 8233); 
	/*  ------	 8240 2030 per mille sign  */
	*(UnicodeMapNR+phashNR(8240)) = newFmChars("\\xe4 ", 0, 8240); 
	/*  ------	 8242 2032 prime  */
	*(UnicodeMapNR+phashNR(8242)) = newFmChars("\\xa2 ", 1, 8242); 
	/*  ------	 8243 2033 double prime  */
	*(UnicodeMapNR+phashNR(8243)) = newFmChars("\\xb2 ", 1, 8243); 
	/*  ------	 8249 2039 left pointing single guillemet  */
	*(UnicodeMapNR+phashNR(8249)) = newFmChars("\\xdc ", 0, 8249); 
	/*  ------	 8250 203A right pointing single guillemet  */
	*(UnicodeMapNR+phashNR(8250)) = newFmChars("\\xdd ", 0, 8250); 
	/*  ------	 8254 203E spacing overscore  */
	*(UnicodeMapNR+phashNR(8254)) = newFmChars("\\x60 ", 1, 8254); 
	/*  ------	 8260 2044 fraction slash  */
	*(UnicodeMapNR+phashNR(8260)) = newFmChars("\\xda ", 0, 8260); 
	/*  VERSION 6.0.0p53: support for euro */
	/*  ------	 8364 20AC euro */
#ifndef WIN32
	*(UnicodeMapNR+phashNR(8364)) = newFmChars("E", 3, 8364); 
#endif
#ifdef WIN32
	*(UnicodeMapNR+phashNR(8364)) = newFmChars("\\xf5 ", 0, 8364); 
#endif
	/*  ------	 8465 2111 black-letter 1  */
	*(UnicodeMapNR+phashNR(8465)) = newFmChars("\\xc1 ", 1, 8465); 
	/*  ------	 8472 2118 script p  */
	*(UnicodeMapNR+phashNR(8472)) = newFmChars("\\xc3 ", 1, 8472); 
	/*  ------	 8476 211C black-letter r  */
	*(UnicodeMapNR+phashNR(8476)) = newFmChars("\\xc2 ", 1, 8476); 
	/*  ------	 8482 2122 trademark  */
	*(UnicodeMapNR+phashNR(8482)) = newFmChars("\\xaa ", 0, 8482); 
	/*  ------	 8486 2126 ohm  */
	*(UnicodeMapNR+phashNR(8486)) = newFmChars("\\x57 ", 1, 8486); 
	/*  ------	 8501 2135 first traSMSfinite cardinal  */
	*(UnicodeMapNR+phashNR(8501)) = newFmChars("\\xc0 ", 1, 8501); 
	/*  ------	 8592 2190 left arrow  */
	*(UnicodeMapNR+phashNR(8592)) = newFmChars("\\xac ", 1, 8592); 
	/*  ------	 8593 2191 up arrow  */
	*(UnicodeMapNR+phashNR(8593)) = newFmChars("\\xad ", 1, 8593); 
	/*  ------	 8594 2192 right arrow  */
	*(UnicodeMapNR+phashNR(8594)) = newFmChars("\\xae ", 1, 8594); 
	/*  ------	 8595 2193 down arrow  */
	*(UnicodeMapNR+phashNR(8595)) = newFmChars("\\xaf ", 1, 8595); 
	/*  ------	 8596 2194 left right arrow  */
	*(UnicodeMapNR+phashNR(8596)) = newFmChars("\\xab ", 1, 8596); 
	/*  ------	 8597 2195 up down arrow  */
	*(UnicodeMapNR+phashNR(8597)) = newFmChars("\\xd7 ", 2, 8597); 
	/*  ------	 8629 21B5 down arrow with corner left  */
	*(UnicodeMapNR+phashNR(8629)) = newFmChars("\\xbf ", 1, 8629); 
	/*  ------	 8656 21D0 left double arrow  */
	*(UnicodeMapNR+phashNR(8656)) = newFmChars("\\xdc ", 1, 8656); 
	/*  ------	 8657 21D1 up double arrow  */
	*(UnicodeMapNR+phashNR(8657)) = newFmChars("\\xdd ", 1, 8657); 
	/*  ------	 8658 21D2 right double arrow  */
	*(UnicodeMapNR+phashNR(8658)) = newFmChars("\\xde ", 1, 8658); 
	/*  ------	 8659 21D3 down double arrow  */
	*(UnicodeMapNR+phashNR(8659)) = newFmChars("\\xdf ", 1, 8659); 
	/*  ------	 8660 21D4 left right double arrow  */
	*(UnicodeMapNR+phashNR(8660)) = newFmChars("\\xdb ", 1, 8660); 
	/*  ------	 8704 2200 for all  */
	*(UnicodeMapNR+phashNR(8704)) = newFmChars("\\x22 ", 1, 8704); 
	/*  ------	 8706 2202 partial differential  */
	*(UnicodeMapNR+phashNR(8706)) = newFmChars("\\xb6 ", 1, 8706); 
	/*  ------	 8707 2203 there exists  */
	*(UnicodeMapNR+phashNR(8707)) = newFmChars("\\x24 ", 1, 8707); 
	/*  ------	 8709 2205 empty set  */
	*(UnicodeMapNR+phashNR(8709)) = newFmChars("\\xc6 ", 1, 8709); 
	/*  ------	 8710 2206 increment  */
	*(UnicodeMapNR+phashNR(8710)) = newFmChars("\\x44 ", 1, 8710); 
	/*  ------	 8711 2207 nabla  */
	*(UnicodeMapNR+phashNR(8711)) = newFmChars("\\xd1 ", 1, 8711); 
	/*  ------	 8712 2208 element of  */
	*(UnicodeMapNR+phashNR(8712)) = newFmChars("\\xce ", 1, 8712); 
	/*  ------	 8713 2209 not an element of  */
	*(UnicodeMapNR+phashNR(8713)) = newFmChars("\\xcf ", 1, 8713); 
	/*  ------	 8715 220B contaiSMS as member  */
	*(UnicodeMapNR+phashNR(8715)) = newFmChars("\\x27 ", 1, 8715); 
	/*  ------	 8719 220F n-ary product  */
	*(UnicodeMapNR+phashNR(8719)) = newFmChars("\\xd5 ", 1, 8719); 
	/*  ------	 8721 2211 n-ary summation  */
	*(UnicodeMapNR+phashNR(8721)) = newFmChars("\\xe5 ", 1, 8721); 
	/*  ------	 8722 2212 minus sign  */
	*(UnicodeMapNR+phashNR(8722)) = newFmChars("\\x2d ", 1, 8722); 
	/*  ------	 8725 2215 division slash  */
	*(UnicodeMapNR+phashNR(8725)) = newFmChars("\\xa4 ", 1, 8725); 
	/*  ------	 8727 2217 asterisk operator  */
	*(UnicodeMapNR+phashNR(8727)) = newFmChars("\\x2a ", 1, 8727); 
	/*  ------	 8730 221A square root  */
	*(UnicodeMapNR+phashNR(8730)) = newFmChars("\\xd6 ", 1, 8730); 
	/*  ------	 8733 221D proportional to  */
	*(UnicodeMapNR+phashNR(8733)) = newFmChars("\\xb5 ", 1, 8733); 
	/*  ------	 8734 221E infinity  */
	*(UnicodeMapNR+phashNR(8734)) = newFmChars("\\xa5 ", 1, 8734); 
	/*  ------	 8736 2220 angle  */
	*(UnicodeMapNR+phashNR(8736)) = newFmChars("\\xd0 ", 1, 8736); 
	/*  ------	 8743 2227 logical and  */
	*(UnicodeMapNR+phashNR(8743)) = newFmChars("\\xd9 ", 1, 8743); 
	/*  ------	 8744 2228 logical or  */
	*(UnicodeMapNR+phashNR(8744)) = newFmChars("\\xda ", 1, 8744); 
	/*  ------	 8745 2229 intersection  */
	*(UnicodeMapNR+phashNR(8745)) = newFmChars("\\xc7 ", 1, 8745); 
	/*  ------	 8746 222A union  */
	*(UnicodeMapNR+phashNR(8746)) = newFmChars("\\xc8 ", 1, 8746); 
	/*  ------	 8747 222B integral  */
	*(UnicodeMapNR+phashNR(8747)) = newFmChars("\\xf2 ", 1, 8747); 
	/*  ------	 8756 2234 therefore  */
	*(UnicodeMapNR+phashNR(8756)) = newFmChars("\\x5c ", 1, 8756); 
	/*  ------	 8764 223C tilde operator  */
	*(UnicodeMapNR+phashNR(8764)) = newFmChars("\\x7e ", 1, 8764); 
	/*  ------	 8773 2245 approximately equal to  */
	*(UnicodeMapNR+phashNR(8773)) = newFmChars("\\x40 ", 1, 8773); 
	/*  ------	 8776 2248 almost equal to  */
	*(UnicodeMapNR+phashNR(8776)) = newFmChars("\\xbb ", 1, 8776); 
	/*  ------	 8800 2260 not equal to  */
	*(UnicodeMapNR+phashNR(8800)) = newFmChars("\\xb9 ", 1, 8800); 
	/*  ------	 8801 2261 identical to  */
	*(UnicodeMapNR+phashNR(8801)) = newFmChars("\\xba ", 1, 8801); 
	/*  ------	 8804 2264 less than or equal to  */
	*(UnicodeMapNR+phashNR(8804)) = newFmChars("\\xa3 ", 1, 8804); 
	/*  ------	 8805 2265 greater than or equal to  */
	*(UnicodeMapNR+phashNR(8805)) = newFmChars("\\xb3 ", 1, 8805); 
	/*  ------	 8834 2282 subset of  */
	*(UnicodeMapNR+phashNR(8834)) = newFmChars("\\xcc ", 1, 8834); 
	/*  ------	 8835 2283 superset of  */
	*(UnicodeMapNR+phashNR(8835)) = newFmChars("\\xc9 ", 1, 8835); 
	/*  ------	 8836 2284 not a subset of  */
	*(UnicodeMapNR+phashNR(8836)) = newFmChars("\\xcb ", 1, 8836); 
	/*  ------	 8838 2286 subset of or equal to  */
	*(UnicodeMapNR+phashNR(8838)) = newFmChars("\\xcd ", 1, 8838); 
	/*  ------	 8839 2287 superset of or equal to  */
	*(UnicodeMapNR+phashNR(8839)) = newFmChars("\\xca ", 1, 8839); 
	/*  ------	 8853 2295 circled plus  */
	*(UnicodeMapNR+phashNR(8853)) = newFmChars("\\xc5 ", 1, 8853); 
	/*  ------	 8855 2297 circled times  */
	*(UnicodeMapNR+phashNR(8855)) = newFmChars("\\xc4 ", 1, 8855); 
	/*  ------	 8869 22A5 up tack  */
	*(UnicodeMapNR+phashNR(8869)) = newFmChars("\\x5e ", 1, 8869); 
	/*  ------	 8901 22C5 dot operator  */
	*(UnicodeMapNR+phashNR(8901)) = newFmChars("\\xd7 ", 1, 8901); 
	/*  ------	 8992 2320 top half integral  */
	*(UnicodeMapNR+phashNR(8992)) = newFmChars("\\xf3 ", 1, 8992); 
	/*  ------	 8993 2321 bottom half integral  */
	*(UnicodeMapNR+phashNR(8993)) = newFmChars("\\xf5 ", 1, 8993); 
	/*  ------	 9001 2329 bra  */
	*(UnicodeMapNR+phashNR(9001)) = newFmChars("\\xe1 ", 1, 9001); 
	/*  ------	 9002 232A ket  */
	*(UnicodeMapNR+phashNR(9002)) = newFmChars("\\xf1 ", 1, 9002); 
	/*  ------	 9312 2460 circled digit one  */
	*(UnicodeMapNR+phashNR(9312)) = newFmChars("\\xac ", 2, 9312); 
	/*  ------	 9313 2461 circled digit two  */
	*(UnicodeMapNR+phashNR(9313)) = newFmChars("\\xad ", 2, 9313); 
	/*  ------	 9314 2462 circled digit three  */
	*(UnicodeMapNR+phashNR(9314)) = newFmChars("\\xae ", 2, 9314); 
	/*  ------	 9315 2463 circled digit four  */
	*(UnicodeMapNR+phashNR(9315)) = newFmChars("\\xaf ", 2, 9315); 
	/*  ------	 9316 2464 circled digit five  */
	*(UnicodeMapNR+phashNR(9316)) = newFmChars("\\xb0 ", 2, 9316); 
	/*  ------	 9317 2465 circled digit six  */
	*(UnicodeMapNR+phashNR(9317)) = newFmChars("\\xb1 ", 2, 9317); 
	/*  ------	 9318 2466 circled digit seven  */
	*(UnicodeMapNR+phashNR(9318)) = newFmChars("\\xb2 ", 2, 9318); 
	/*  ------	 9319 2467 circled digit eight  */
	*(UnicodeMapNR+phashNR(9319)) = newFmChars("\\xb3 ", 2, 9319); 
	/*  ------	 9320 2468 circled digit nine  */
	*(UnicodeMapNR+phashNR(9320)) = newFmChars("\\xb4 ", 2, 9320); 
	/*  ------	 9321 2469 circled number ten  */
	*(UnicodeMapNR+phashNR(9321)) = newFmChars("\\xb5 ", 2, 9321); 
	/*  ------	 9612 258C black down pointing triangle  */
	*(UnicodeMapNR+phashNR(9612)) = newFmChars("\\x74 ", 2, 9612); 
	/*  ------	 9632 25A0 black square  */
	*(UnicodeMapNR+phashNR(9632)) = newFmChars("\\x6e ", 2, 9632); 
	/*  ------	 9650 25B2 black up pointing triangle  */
	*(UnicodeMapNR+phashNR(9650)) = newFmChars("\\x73 ", 2, 9650); 
	/*  ------	 9670 25C6 black diamond  */
	*(UnicodeMapNR+phashNR(9670)) = newFmChars("\\x75 ", 2, 9670); 
	/*  ------	 9674 25CA lozenge  */
	*(UnicodeMapNR+phashNR(9674)) = newFmChars("\\xe0 ", 1, 9674); 
	/*  ------	 9733 2605 black star  */
	*(UnicodeMapNR+phashNR(9733)) = newFmChars("\\x48 ", 2, 9733); 
	/*  ------	 9742 260E black telephone  */
	*(UnicodeMapNR+phashNR(9742)) = newFmChars("\\x25 ", 2, 9742); 
	/*  ------	 9755 261B black right pointing index  */
	*(UnicodeMapNR+phashNR(9755)) = newFmChars("\\x2a ", 2, 9755); 
	/*  ------	 9758 261E white right pointing index  */
	*(UnicodeMapNR+phashNR(9758)) = newFmChars("\\x2b ", 2, 9758); 
	/*  ------	 9824 2660 black spade suit  */
	*(UnicodeMapNR+phashNR(9824)) = newFmChars("\\xaa ", 1, 9824); 
	/*  ------	 9824 2660 black spade suit  */
	/* 9824 "\\xab " ZapfDingbats  */
	/*  ------	 9827 2663 black club suit  */
	*(UnicodeMapNR+phashNR(9827)) = newFmChars("\\xa7 ", 1, 9827); 
	/*  ------	 9827 2663 black club suit  */
	/* 9827 "\\xa8 " ZapfDingbats  */
	/*  ------	 9829 2665 black heart suit  */
	*(UnicodeMapNR+phashNR(9829)) = newFmChars("\\xa9 ", 1, 9829); 
	/*  ------	 9829 2665 black heart suit  */
	/* 9829 "\\xaa " ZapfDingbats  */
	/*  ------	 9830 2666 black diamond suit  */
	*(UnicodeMapNR+phashNR(9830)) = newFmChars("\\xa8 ", 1, 9830); 
	/*  ------	 9830 2666 black diamond suit  */
	/* 9830 "\\xa9 " ZapfDingbats  */
	/*  ------	 9985 2701 upper blade scissors  */
	*(UnicodeMapNR+phashNR(9985)) = newFmChars("\\x21 ", 2, 9985); 
	/*  ------	 9986 2702 black scissors  */
	*(UnicodeMapNR+phashNR(9986)) = newFmChars("\\x22 ", 2, 9986); 
	/*  ------	 9987 2703 lower blade scissors  */
	*(UnicodeMapNR+phashNR(9987)) = newFmChars("\\x23 ", 2, 9987); 
	/*  ------	 9988 2704 white scissors  */
	*(UnicodeMapNR+phashNR(9988)) = newFmChars("\\x24 ", 2, 9988); 
	/*  ------	 9990 2706 telephone location sign  */
	*(UnicodeMapNR+phashNR(9990)) = newFmChars("\\x26 ", 2, 9990); 
	/*  ------	 9991 2707 tape drive  */
	*(UnicodeMapNR+phashNR(9991)) = newFmChars("\\x27 ", 2, 9991); 
	/*  ------	 9992 2708 airplane  */
	*(UnicodeMapNR+phashNR(9992)) = newFmChars("\\x28 ", 2, 9992); 
	/*  ------	 9993 2709 envelope  */
	*(UnicodeMapNR+phashNR(9993)) = newFmChars("\\x29 ", 2, 9993); 
	/*  ------	 9996 270C victory hand  */
	*(UnicodeMapNR+phashNR(9996)) = newFmChars("\\x2c ", 2, 9996); 
	/*  ------	 9997 270D writing hand  */
	*(UnicodeMapNR+phashNR(9997)) = newFmChars("\\x2d ", 2, 9997); 
	/*  ------	 9998 270E lower right pencil  */
	*(UnicodeMapNR+phashNR(9998)) = newFmChars("\\x2e ", 2, 9998); 
	/*  ------	 9999 270F pencil  */
	*(UnicodeMapNR+phashNR(9999)) = newFmChars("\\x2f ", 2, 9999); 
	/*  ------	 10000 2710 upper right pencil  */
	*(UnicodeMapNR+phashNR(10000)) = newFmChars("\\x30 ", 2, 10000); 
	/*  ------	 10001 2711 white nib  */
	*(UnicodeMapNR+phashNR(10001)) = newFmChars("\\x31 ", 2, 10001); 
	/*  ------	 10002 2712 black nib  */
	*(UnicodeMapNR+phashNR(10002)) = newFmChars("\\x32 ", 2, 10002); 
	/*  ------	 10003 2713 check mark  */
	*(UnicodeMapNR+phashNR(10003)) = newFmChars("\\x33 ", 2, 10003); 
	/*  ------	 10004 2714 heavy check mark  */
	*(UnicodeMapNR+phashNR(10004)) = newFmChars("\\x34 ", 2, 10004); 
	/*  ------	 10005 2715 multiplication x  */
	*(UnicodeMapNR+phashNR(10005)) = newFmChars("\\x35 ", 2, 10005); 
	/*  ------	 10006 2716 heavy multiplication x  */
	*(UnicodeMapNR+phashNR(10006)) = newFmChars("\\x36 ", 2, 10006); 
	/*  ------	 10007 2717 ballot x  */
	*(UnicodeMapNR+phashNR(10007)) = newFmChars("\\x37 ", 2, 10007); 
	/*  ------	 10008 2718 heavy ballot x  */
	*(UnicodeMapNR+phashNR(10008)) = newFmChars("\\x38 ", 2, 10008); 
	/*  ------	 10009 2719 outlined greek cross  */
	*(UnicodeMapNR+phashNR(10009)) = newFmChars("\\x39 ", 2, 10009); 
	/*  ------	 10010 271A heavy greek cross  */
	*(UnicodeMapNR+phashNR(10010)) = newFmChars("\\x3a ", 2, 10010); 
	/*  ------	 10011 271B open center cross  */
	*(UnicodeMapNR+phashNR(10011)) = newFmChars("\\x3b ", 2, 10011); 
	/*  ------	 10012 271C heavy open center cross  */
	*(UnicodeMapNR+phashNR(10012)) = newFmChars("\\x3c ", 2, 10012); 
	/*  ------	 10013 271D latin cross  */
	*(UnicodeMapNR+phashNR(10013)) = newFmChars("\\x3d ", 2, 10013); 
	/*  ------	 10014 271E shadowed white latin cross  */
	*(UnicodeMapNR+phashNR(10014)) = newFmChars("\\x3e ", 2, 10014); 
	/*  ------	 10015 271F outlined latin cross  */
	*(UnicodeMapNR+phashNR(10015)) = newFmChars("\\x3f ", 2, 10015); 
	/*  ------	 10016 2720 maltese cross  */
	*(UnicodeMapNR+phashNR(10016)) = newFmChars("\\x40 ", 2, 10016); 
	/*  ------	 10017 2721 star of david  */
	*(UnicodeMapNR+phashNR(10017)) = newFmChars("\\x41 ", 2, 10017); 
	/*  ------	 10018 2722 four teardrop-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10018)) = newFmChars("\\x42 ", 2, 10018); 
	/*  ------	 10019 2723 four balloon-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10019)) = newFmChars("\\x43 ", 2, 10019); 
	/*  ------	 10020 2724 heavy four balloon-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10020)) = newFmChars("\\x44 ", 2, 10020); 
	/*  ------	 10021 2725 four club-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10021)) = newFmChars("\\x45 ", 2, 10021); 
	/*  ------	 10022 2726 black four pointed star  */
	*(UnicodeMapNR+phashNR(10022)) = newFmChars("\\x46 ", 2, 10022); 
	/*  ------	 10025 2729 stress outlined white star  */
	*(UnicodeMapNR+phashNR(10025)) = newFmChars("\\x49 ", 2, 10025); 
	/*  ------	 10026 272A circled white star  */
	*(UnicodeMapNR+phashNR(10026)) = newFmChars("\\x4a ", 2, 10026); 
	/*  ------	 10027 272B open center black star  */
	*(UnicodeMapNR+phashNR(10027)) = newFmChars("\\x4b ", 2, 10027); 
	/*  ------	 10028 272C black center white star  */
	*(UnicodeMapNR+phashNR(10028)) = newFmChars("\\x4c ", 2, 10028); 
	/*  ------	 10029 272D outlined black star  */
	*(UnicodeMapNR+phashNR(10029)) = newFmChars("\\x4d ", 2, 10029); 
	/*  ------	 10030 272E heavy outlined black star  */
	*(UnicodeMapNR+phashNR(10030)) = newFmChars("\\x4e ", 2, 10030); 
	/*  ------	 10031 272F pinwheel star  */
	*(UnicodeMapNR+phashNR(10031)) = newFmChars("\\x4f ", 2, 10031); 
	/*  ------	 10032 2730 shadowed white star  */
	*(UnicodeMapNR+phashNR(10032)) = newFmChars("\\x50 ", 2, 10032); 
	/*  ------	 10033 2731 heavy asterisk  */
	*(UnicodeMapNR+phashNR(10033)) = newFmChars("\\x51 ", 2, 10033); 
	/*  ------	 10034 2732 open center asterisk  */
	*(UnicodeMapNR+phashNR(10034)) = newFmChars("\\x52 ", 2, 10034); 
	/*  ------	 10035 2733 eight spoked asterisk  */
	*(UnicodeMapNR+phashNR(10035)) = newFmChars("\\x53 ", 2, 10035); 
	/*  ------	 10036 2734 eight pointed black star  */
	*(UnicodeMapNR+phashNR(10036)) = newFmChars("\\x54 ", 2, 10036); 
	/*  ------	 10037 2735 eight pointed pinwheel star  */
	*(UnicodeMapNR+phashNR(10037)) = newFmChars("\\x55 ", 2, 10037); 
	/*  ------	 10038 2736 six pointed black star  */
	*(UnicodeMapNR+phashNR(10038)) = newFmChars("\\x56 ", 2, 10038); 
	/*  ------	 10039 2737 eight pointed rectilinear black star  */
	*(UnicodeMapNR+phashNR(10039)) = newFmChars("\\x57 ", 2, 10039); 
	/*  ------	 10040 2738 heavy eight pointed rectilinear black star  */
	*(UnicodeMapNR+phashNR(10040)) = newFmChars("\\x58 ", 2, 10040); 
	/*  ------	 10041 2739 twelve pointed black star  */
	*(UnicodeMapNR+phashNR(10041)) = newFmChars("\\x59 ", 2, 10041); 
	/*  ------	 10042 273A sixteen pointed asterisk  */
	*(UnicodeMapNR+phashNR(10042)) = newFmChars("\\x5a ", 2, 10042); 
	/*  ------	 10043 273B teardrop-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10043)) = newFmChars("\\x5b ", 2, 10043); 
	/*  ------	 10044 273C open center teardrop-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10044)) = newFmChars("\\x5c ", 2, 10044); 
	/*  ------	 10045 273D heavy teardrop-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10045)) = newFmChars("\\x5d ", 2, 10045); 
	/*  ------	 10046 273E six petalled black and white florette  */
	*(UnicodeMapNR+phashNR(10046)) = newFmChars("\\x5e ", 2, 10046); 
	/*  ------	 10047 273F black florette  */
	*(UnicodeMapNR+phashNR(10047)) = newFmChars("\\x5f ", 2, 10047); 
	/*  ------	 10048 2740 white florette  */
	*(UnicodeMapNR+phashNR(10048)) = newFmChars("\\x60 ", 2, 10048); 
	/*  ------	 10049 2741 eight petalled outlined black florette  */
	*(UnicodeMapNR+phashNR(10049)) = newFmChars("\\x61 ", 2, 10049); 
	/*  ------	 10050 2742 circled open center eight pointed star  */
	*(UnicodeMapNR+phashNR(10050)) = newFmChars("\\x62 ", 2, 10050); 
	/*  ------	 10051 2743 heavy teardrop-spoked pinwheel asterisk  */
	*(UnicodeMapNR+phashNR(10051)) = newFmChars("\\x63 ", 2, 10051); 
	/*  ------	 10052 2744 snowflake  */
	*(UnicodeMapNR+phashNR(10052)) = newFmChars("\\x64 ", 2, 10052); 
	/*  ------	 10053 2745 tight trifoliate snowflake  */
	*(UnicodeMapNR+phashNR(10053)) = newFmChars("\\x65 ", 2, 10053); 
	/*  ------	 10054 2746 heavy chevron snowflake  */
	*(UnicodeMapNR+phashNR(10054)) = newFmChars("\\x66 ", 2, 10054); 
	/*  ------	 10055 2747 sparkle  */
	*(UnicodeMapNR+phashNR(10055)) = newFmChars("\\x67 ", 2, 10055); 
	/*  ------	 10056 2748 heavy sparkle  */
	*(UnicodeMapNR+phashNR(10056)) = newFmChars("\\x68 ", 2, 10056); 
	/*  ------	 10057 2749 balloon-spoked asterisk  */
	*(UnicodeMapNR+phashNR(10057)) = newFmChars("\\x69 ", 2, 10057); 
	/*  ------	 10058 274A eight teardrop-spoked propeller asterisk  */
	*(UnicodeMapNR+phashNR(10058)) = newFmChars("\\x6a ", 2, 10058); 
	/*  ------	 10059 274B heavy eight teardrop-spoked propeller asterisk  */
	*(UnicodeMapNR+phashNR(10059)) = newFmChars("\\x6b ", 2, 10059); 
	/*  ------	 10061 274D shadowed white circle  */
	*(UnicodeMapNR+phashNR(10061)) = newFmChars("\\x6d ", 2, 10061); 
	/*  ------	 10063 274F lower right drop-shadowed white square  */
	*(UnicodeMapNR+phashNR(10063)) = newFmChars("\\x6f ", 2, 10063); 
	/*  ------	 10064 2750 upper right drop-shadowed white square  */
	*(UnicodeMapNR+phashNR(10064)) = newFmChars("\\x70 ", 2, 10064); 
	/*  ------	 10065 2751 lower right shadowed white square  */
	*(UnicodeMapNR+phashNR(10065)) = newFmChars("\\x71 ", 2, 10065); 
	/*  ------	 10066 2752 upper right shadowed white square  */
	*(UnicodeMapNR+phashNR(10066)) = newFmChars("\\x72 ", 2, 10066); 
	/*  ------	 10070 2756 black diamond minus white x  */
	*(UnicodeMapNR+phashNR(10070)) = newFmChars("\\x76 ", 2, 10070); 
	/*  ------	 10072 2758 light vertical bar  */
	*(UnicodeMapNR+phashNR(10072)) = newFmChars("\\x78 ", 2, 10072); 
	/*  ------	 10073 2759 medium vertical bar  */
	*(UnicodeMapNR+phashNR(10073)) = newFmChars("\\x79 ", 2, 10073); 
	/*  ------	 10074 275A heavy vertical bar  */
	*(UnicodeMapNR+phashNR(10074)) = newFmChars("\\x7a ", 2, 10074); 
	/*  ------	 10075 275B heavy single turned comma quotation mark ornament  */
	*(UnicodeMapNR+phashNR(10075)) = newFmChars("\\x7b ", 2, 10075); 
	/*  ------	 10076 275C heavy single comma quotation mark ornament  */
	*(UnicodeMapNR+phashNR(10076)) = newFmChars("\\x7c ", 2, 10076); 
	/*  ------	 10077 275D heavy double turned comma quotation mark ornament  */
	*(UnicodeMapNR+phashNR(10077)) = newFmChars("\\x7d ", 2, 10077); 
	/*  ------	 10078 275E heavy double comma quotation mark ornament  */
	*(UnicodeMapNR+phashNR(10078)) = newFmChars("\\x7e ", 2, 10078); 
	/*  ------	 10081 2761 curved stem paragraph sign ornament  */
	*(UnicodeMapNR+phashNR(10081)) = newFmChars("\\xa1 ", 2, 10081); 
	/*  ------	 10082 2762 heavy exclamation mark ornament  */
	*(UnicodeMapNR+phashNR(10082)) = newFmChars("\\xa2 ", 2, 10082); 
	/*  ------	 10083 2763 heavy heart exclamation mark  */
	*(UnicodeMapNR+phashNR(10083)) = newFmChars("\\xa3 ", 2, 10083); 
	/*  ------	 10084 2764 heavy black heart  */
	*(UnicodeMapNR+phashNR(10084)) = newFmChars("\\xa4 ", 2, 10084); 
	/*  ------	 10085 2765 rotated heavy black heart bullet  */
	*(UnicodeMapNR+phashNR(10085)) = newFmChars("\\xa5 ", 2, 10085); 
	/*  ------	 10086 2766 floral heart  */
	*(UnicodeMapNR+phashNR(10086)) = newFmChars("\\xa6 ", 2, 10086); 
	/*  ------	 10087 2767 rotated floral heart bullet  */
	*(UnicodeMapNR+phashNR(10087)) = newFmChars("\\xa7 ", 2, 10087); 
	/*  ------	 10102 2776 inverse circled digit one  */
	*(UnicodeMapNR+phashNR(10102)) = newFmChars("\\xb6 ", 2, 10102); 
	/*  ------	 10103 2777 inverse circled digit two  */
	*(UnicodeMapNR+phashNR(10103)) = newFmChars("\\xb7 ", 2, 10103); 
	/*  ------	 10104 2778 inverse circled digit three  */
	*(UnicodeMapNR+phashNR(10104)) = newFmChars("\\xb8 ", 2, 10104); 
	/*  ------	 10105 2779 inverse circled digit four  */
	*(UnicodeMapNR+phashNR(10105)) = newFmChars("\\xb9 ", 2, 10105); 
	/*  ------	 10106 277A inverse circled digit five  */
	*(UnicodeMapNR+phashNR(10106)) = newFmChars("\\xba ", 2, 10106); 
	/*  ------	 10107 277B inverse circled digit six  */
	*(UnicodeMapNR+phashNR(10107)) = newFmChars("\\xbb ", 2, 10107); 
	/*  ------	 10108 277C inverse circled digit seven  */
	*(UnicodeMapNR+phashNR(10108)) = newFmChars("\\xbc ", 2, 10108); 
	/*  ------	 10109 277D inverse circled digit eight  */
	*(UnicodeMapNR+phashNR(10109)) = newFmChars("\\xbd ", 2, 10109); 
	/*  ------	 10110 277E inverse circled digit nine  */
	*(UnicodeMapNR+phashNR(10110)) = newFmChars("\\xbe ", 2, 10110); 
	/*  ------	 10111 277F inverse circled number ten  */
	*(UnicodeMapNR+phashNR(10111)) = newFmChars("\\xbf ", 2, 10111); 
	/*  ------	 10112 2780 circled saSMS-serif digit one  */
	*(UnicodeMapNR+phashNR(10112)) = newFmChars("\\xc0 ", 2, 10112); 
	/*  ------	 10113 2781 circled saSMS-serif digit two  */
	*(UnicodeMapNR+phashNR(10113)) = newFmChars("\\xc1 ", 2, 10113); 
	/*  ------	 10114 2782 circled saSMS-serif digit three  */
	*(UnicodeMapNR+phashNR(10114)) = newFmChars("\\xc2 ", 2, 10114); 
	/*  ------	 10115 2783 circled saSMS-serif digit four  */
	*(UnicodeMapNR+phashNR(10115)) = newFmChars("\\xc3 ", 2, 10115); 
	/*  ------	 10116 2784 circled saSMS-serif digit five  */
	*(UnicodeMapNR+phashNR(10116)) = newFmChars("\\xc4 ", 2, 10116); 
	/*  ------	 10117 2785 circled saSMS-serif digit six  */
	*(UnicodeMapNR+phashNR(10117)) = newFmChars("\\xc5 ", 2, 10117); 
	/*  ------	 10118 2786 circled saSMS-serif digit seven  */
	*(UnicodeMapNR+phashNR(10118)) = newFmChars("\\xc6 ", 2, 10118); 
	/*  ------	 10119 2787 circled saSMS-serif digit nine  */
	*(UnicodeMapNR+phashNR(10119)) = newFmChars("\\xc7 ", 2, 10119); 
	/*  ------	 10121 2789 circled saSMS-serif number ten  */
	*(UnicodeMapNR+phashNR(10121)) = newFmChars("\\xc9 ", 2, 10121); 
	/*  ------	 10122 278A inverse circled saSMS-serif digit one  */
	*(UnicodeMapNR+phashNR(10122)) = newFmChars("\\xca ", 2, 10122); 
	/*  ------	 10123 278B inverse circled saSMS-serif digit two  */
	*(UnicodeMapNR+phashNR(10123)) = newFmChars("\\xcb ", 2, 10123); 
	/*  ------	 10124 278C inverse circled saSMS-serif digit three  */
	*(UnicodeMapNR+phashNR(10124)) = newFmChars("\\xcc ", 2, 10124); 
	/*  ------	 10125 278D inverse circled saSMS-serif digit four  */
	*(UnicodeMapNR+phashNR(10125)) = newFmChars("\\xcd ", 2, 10125); 
	/*  ------	 10126 278E inverse circled saSMS-serif digit five  */
	*(UnicodeMapNR+phashNR(10126)) = newFmChars("\\xce ", 2, 10126); 
	/*  ------	 10127 278F inverse circled saSMS-serif digit six  */
	*(UnicodeMapNR+phashNR(10127)) = newFmChars("\\xcf ", 2, 10127); 
	/*  ------	 10129 2791 inverse circled saSMS-serif digit eight  */
	*(UnicodeMapNR+phashNR(10129)) = newFmChars("\\xd1 ", 2, 10129); 
	/*  ------	 10130 2792 inverse circled saSMS-serif digit nine  */
	*(UnicodeMapNR+phashNR(10130)) = newFmChars("\\xd2 ", 2, 10130); 
	/*  ------	 10131 2793 inverse circled saSMS-serif number ten  */
	*(UnicodeMapNR+phashNR(10131)) = newFmChars("\\xd3 ", 2, 10131); 
	/*  ------	 10132 2794 heavy wide-headed right arrow  */
	*(UnicodeMapNR+phashNR(10132)) = newFmChars("\\xd4 ", 2, 10132); 
	/*  ------	 10139 279B drafting point right arrow  */
	*(UnicodeMapNR+phashNR(10139)) = newFmChars("\\xdb ", 2, 10139); 
	/*  ------	 10136 2798 heavy lower right arrow  */
	*(UnicodeMapNR+phashNR(10136)) = newFmChars("\\xd8 ", 2, 10136); 
	/*  ------	 10137 2799 heavy right arrow  */
	*(UnicodeMapNR+phashNR(10137)) = newFmChars("\\xd9 ", 2, 10137); 
	/*  ------	 10138 279A heavy upper right arrow  */
	*(UnicodeMapNR+phashNR(10138)) = newFmChars("\\xda ", 2, 10138); 
	/*  ------	 10140 279C heavy round-tipped right arrow  */
	*(UnicodeMapNR+phashNR(10140)) = newFmChars("\\xdc ", 2, 10140); 
	/*  ------	 10141 279D triangle-headed right arrow  */
	*(UnicodeMapNR+phashNR(10141)) = newFmChars("\\xdd ", 2, 10141); 
	/*  ------	 10142 279E heavy triangle-headed right arrow  */
	*(UnicodeMapNR+phashNR(10142)) = newFmChars("\\xde ", 2, 10142); 
	/*  ------	 10143 279F dashed triangle-headed right arrow  */
	*(UnicodeMapNR+phashNR(10143)) = newFmChars("\\xdf ", 2, 10143); 
	/*  ------	 10144 27A0 heavy dashed triangle-headed right arrow  */
	*(UnicodeMapNR+phashNR(10144)) = newFmChars("\\xe0 ", 2, 10144); 
	/*  ------	 10145 27A1 black right arrow  */
	*(UnicodeMapNR+phashNR(10145)) = newFmChars("\\xe1 ", 2, 10145); 
	/*  ------	 10146 27A2 three-d top-lighted right arrowhead  */
	*(UnicodeMapNR+phashNR(10146)) = newFmChars("\\xe2 ", 2, 10146); 
	/*  ------	 10147 27A3 three-d bottom-lighted right arrowhead  */
	*(UnicodeMapNR+phashNR(10147)) = newFmChars("\\xe3 ", 2, 10147); 
	/*  ------	 10148 27A4 black right arrowhead  */
	*(UnicodeMapNR+phashNR(10148)) = newFmChars("\\xe4 ", 2, 10148); 
	/*  ------	 10149 27A5 heavy black curved down and right arrow  */
	*(UnicodeMapNR+phashNR(10149)) = newFmChars("\\xe5 ", 2, 10149); 
	/*  ------	 10150 27A6 heavy black curved up and right arrow  */
	*(UnicodeMapNR+phashNR(10150)) = newFmChars("\\xe6 ", 2, 10150); 
	/*  ------	 10151 27A7 squat black right arrow  */
	*(UnicodeMapNR+phashNR(10151)) = newFmChars("\\xe7 ", 2, 10151); 
	/*  ------	 10152 27A8 heavy concave-pointed black right arrow  */
	*(UnicodeMapNR+phashNR(10152)) = newFmChars("\\xe8 ", 2, 10152); 
	/*  ------	 10153 27A9 right-shaded white right arrow  */
	*(UnicodeMapNR+phashNR(10153)) = newFmChars("\\xe9 ", 2, 10153); 
	/*  ------	 10154 27AA left-shaded white right arrow  */
	*(UnicodeMapNR+phashNR(10154)) = newFmChars("\\xea ", 2, 10154); 
	/*  ------	 10155 27AB back-tilted shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10155)) = newFmChars("\\xeb ", 2, 10155); 
	/*  ------	 10156 27AC front-tilted shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10156)) = newFmChars("\\xec ", 2, 10156); 
	/*  ------	 10157 27AD heavy lower right-shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10157)) = newFmChars("\\xed ", 2, 10157); 
	/*  ------	 10158 27AE heavy upper right-shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10158)) = newFmChars("\\xee ", 2, 10158); 
	/*  ------	 10159 27AF notched lower right-shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10159)) = newFmChars("\\xef ", 2, 10159); 
	/*  ------	 10161 27B1 notched upper right-shadowed white right arrow  */
	*(UnicodeMapNR+phashNR(10161)) = newFmChars("\\xf1 ", 2, 10161); 
	/*  ------	 10162 27B2 circled heavy white right arrow  */
	*(UnicodeMapNR+phashNR(10162)) = newFmChars("\\xf2 ", 2, 10162); 
	/*  ------	 10163 27B3 white-feathered right arrow  */
	*(UnicodeMapNR+phashNR(10163)) = newFmChars("\\xf3 ", 2, 10163); 
	/*  ------	 10164 27B4 black-feathered lower right arrow  */
	*(UnicodeMapNR+phashNR(10164)) = newFmChars("\\xf4 ", 2, 10164); 
	/*  ------	 10165 27B5 black-feathered right arrow  */
	*(UnicodeMapNR+phashNR(10165)) = newFmChars("\\xf5 ", 2, 10165); 
	/*  ------	 10166 27B6 black-feathered upper right arrow  */
	*(UnicodeMapNR+phashNR(10166)) = newFmChars("\\xf6 ", 2, 10166); 
	/*  ------	 10167 27B7 heavy black-feathered lower right arrow  */
	*(UnicodeMapNR+phashNR(10167)) = newFmChars("\\xf7 ", 2, 10167); 
	/*  ------	 10168 27B8 heavy black-feathered right arrow  */
	*(UnicodeMapNR+phashNR(10168)) = newFmChars("\\xf8 ", 2, 10168); 
	/*  ------	 10169 27B9 heavy black-feathered upper right arrow  */
	*(UnicodeMapNR+phashNR(10169)) = newFmChars("\\xf9 ", 2, 10169); 
	/*  ------	 10170 27BA teardrop-barbed right arrow  */
	*(UnicodeMapNR+phashNR(10170)) = newFmChars("\\xfa ", 2, 10170); 
	/*  ------	 10171 27BB heavy teardrop-shanked right arrow  */
	*(UnicodeMapNR+phashNR(10171)) = newFmChars("\\xfb ", 2, 10171); 
	/*  ------	 10172 27BC wedge-tailed right arrow  */
	*(UnicodeMapNR+phashNR(10172)) = newFmChars("\\xfc ", 2, 10172); 
	/*  ------	 10173 27BD heavy wedge-tailed right arrow  */
	*(UnicodeMapNR+phashNR(10173)) = newFmChars("\\xfd ", 2, 10173); 
	/*  ------	 10174 27BE open-outlined right arrow  */
	*(UnicodeMapNR+phashNR(10174)) = newFmChars("\\xfe ", 2, 10174); 

	return(UnicodeMapNR);
}
