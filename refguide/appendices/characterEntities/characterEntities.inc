@--------------------------------------------------------------------------------
@- characterEntities.inc
@- 
@-----------------------------------------------------------
<@include>	${appendices}/characterEntities/pageLayoutsCharguide.mmp
@-----------------------------------------------------------
<chapterStart
	dest="characterEntities"
	type="appendix"
	>Character Entities
</chapterStart>
@--------------------------------------------------------------------------------
	<@skipStart>
	<MasterPageRule
		page="MP_specialIndexFirst"
		allPages="MP_specialIndexRight"
		/>
	<P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P>
	<P fmt="P_chapterTitle"
		span="all"
		listLabel="N"
		spaceBelow="30mm"
		>Index</P>
	<P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P>
	<@skipEnd>
@--------------------------------------------------------------------------------

@-----------------------------------------------------------
<@include>	${appendices}/characterEntities/characterEntities

<@skipStart>
<@skipEnd>
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
