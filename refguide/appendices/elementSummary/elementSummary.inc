@-----------------------------------------------------------
<!--  File = elementSummary.inc (Start) -->
@-----------------------------------------------------------
@- elementSummary.inc
@- include files for elementSummary appendix
@-----------------------------------------------------------
<chapterStart
	dest="elementSummary"
	type="appendix"
	@- >Summary of element properties &amp; values
	>Summary of element properties &amp; values
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/elementSummary/elementSummary

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

@-----------------------------------------------------------
<!--  File = elementSummary.inc (End) -->
@-----------------------------------------------------------
