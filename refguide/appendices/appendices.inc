@------------------------------------------------------------
@- appedices.inc
@------------------------------------------------------------
<@include>	${appendices}/appendicesStart
<@include>	${appendices}/propertyValueTypes/propertyTypes.inc
<@include>	${appendices}/fonts/fonts.inc
@- <@include>	${appendices}/textlanguage/textlanguage.inc
<@include>	${appendices}/textlanguage/langAndHyphenation.inc
<@include>	${appendices}/autonumbering/autonumbering.inc
<@include>	${appendices}/capitalization/capitalization.inc
<@include>	${appendices}/urlCaching/urlCaching.inc
@- <@include>	${appendices}/characterEntities/characterEntities.inc
@- <@include>	${appendices}/color/color.inc
@- <@include>	${appendices}/specialCharacters/specialCharacters.inc
<@include>	${appendices}/elementSummary/elementSummary.inc
<@skipStart>
<@skipEnd>

