@--------------------------------------------------------------------------------
@- langAndHyphenation.inc
@--------------------------------------------------------------------------------
<chapterStart
	dest="language_property"
	type="appendix"
	>Text language &amp; hyphenation
</chapterStart>
@-----------------------------------------------------------
<@include>      ${appendices}/textlanguage/textlanguage
<@include>      ${appendices}/textlanguage/hyphenation
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

