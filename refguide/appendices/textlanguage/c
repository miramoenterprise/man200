#include "mmLanguages.h"
#include <QSet>

// See https://xmlgraphics.apache.org/fop/trunk/complexscripts.html
// and http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
// and D:\miramo\mmComposerJars\src\java\org\apache\fop\complexscripts\util\CharScript.java
// and D:\miramo\mmComposerJars\hyph\*.xml for mmfop hyphenation codes

namespace MmLanguages {

	static mmLanguages lmap;
    static mmCollations collationsMap;
    static const mmLanguage emptyLanguage("", "", "", "");
    
    bool languageRequiresForcedHyphenation(const QString language) 
    {
        if (language == "Thai" || language == "Khmer" || language == "Lao")
            return true;
        return false;
    }
    
    bool languageRequiresKerning(const QString &strMmLanguage)
	{
		bool bRes = false;

        if (isRightToLeft(strMmLanguage) || matchesHindi(strMmLanguage))
			bRes = true;

		return bRes;
	}
    
    bool isRightToLeft(const QString &strMmLanguageName)
	{
        // UNUSED: bool bRes = false;

		// Supported RTL language names: 
		static QSet<QString> rtlLanguageList; 

		// Based on the lower case mmLanguage name
		if(rtlLanguageList.empty()) {
			rtlLanguageList.insert("arabic");
			rtlLanguageList.insert("hebrew");
			rtlLanguageList.insert("urdu");
			rtlLanguageList.insert("persian");
		}
        QString strLowerCaseName = strMmLanguageName.toLower();


		return rtlLanguageList.find(strLowerCaseName) != rtlLanguageList.end();
	}

	// VERSION 1.2.0p405: change from isoLanguage to hyphenationLanguage (as per OFFO)

	// Miramo language maps:
	// mmLanguage(hyphenationLanguage, localeName, fontLanguage, fontScript)
	//		Default hyphenationLanguage = ""
	//		Default localeName = hyphenationLanguage
	//		Default fontLanguage = "" (dflt)
	//		Default fontScript = "" (auto)
	mmLanguages::mmLanguages() {
		// From list of OFFO hyphenation patterns
		languageMap_["usEnglish"] = mmLanguage("en");	// American english
		languageMap_["ukEnglish"] = mmLanguage("en_GB");
		languageMap_["Afrikaans"] = mmLanguage("af");
        languageMap_["Amharic"] = mmLanguage("am");
        languageMap_["Assamese"]	= mmLanguage("as");
        languageMap_["Azerbaijani"] = mmLanguage("az");
        languageMap_["Belarusian"] = mmLanguage("be");
		languageMap_["Bulgarian"]	= mmLanguage("bg");
		languageMap_["Bengali"]		= mmLanguage("bn");
        languageMap_["Bosnian"] = mmLanguage("bs");
		languageMap_["Catalan"]		= mmLanguage("ca");
		//languageMap_["Coptic"]		= mmLanguage("cop");
		languageMap_["Czech"]		= mmLanguage("cs");
		languageMap_["Welsh"]		= mmLanguage("cy");
		languageMap_["Danish"] = mmLanguage("da");
		languageMap_["Dansk"] = mmLanguage("da");
		languageMap_["Dutch"] = mmLanguage("nl");
        languageMap_["Dzongkha"] = mmLanguage("dz");
        languageMap_["Ewe"] = mmLanguage("ee");
        languageMap_["Spanish"] = mmLanguage("es");
		languageMap_["French"] = mmLanguage("fr");
		languageMap_["français"] = mmLanguage("fr");
		languageMap_["German"] = mmLanguage("de_1901");	// traditional orthography
		languageMap_["newGerman"] = mmLanguage("de");	// reformed orthographcy
		languageMap_["Hungarian"] = mmLanguage("hu");
		languageMap_["Magyar"] = mmLanguage("hu");
		//languageMap_["SwissGerman"] = mmLanguage("de_CH");
		languageMap_["Greek"]	= mmLanguage("el"); // uni-accent (monotonic) Modern Greek
		//languageMap_["PoluGreek"]		= mmLanguage("el_Polyton"); //multi-accent (polytonic) Modern Greek
		// languageMap_["English"] = mmLanguage("en");	// American english
		//languageMap_["Esperanto"]	= mmLanguage("eo");
		languageMap_["Estonian"]	= mmLanguage("et");
		// languageMap_["Basque"]		= mmLanguage("eu");
		languageMap_["Finnish"]		= mmLanguage("fi");
        languageMap_["Faroese"] = mmLanguage("fo");
		// languageMap_["Friulan"] = mmLanguage("fur");
		languageMap_["Irish"] = mmLanguage("ga");
		languageMap_["Galician"] = mmLanguage("gl");
		// languageMap_["AncientGreek"] = mmLanguage("grc");
		languageMap_["Croatian"] = mmLanguage("hr");
		// languageMap_["UpperSorbian"] = mmLanguage("hsb");
		languageMap_["Armenian"] = mmLanguage("hy");
		// languageMap_["Interlingua"] = mmLanguage("ia");
		languageMap_["Indonesian"] = mmLanguage("id");
        languageMap_["Igbo"] = mmLanguage("ig");
		languageMap_["Icelandic"] = mmLanguage("is");
		languageMap_["Italian"] = mmLanguage("it");
		languageMap_["Japanese"] = mmLanguage("", "ja");
		languageMap_["Korean"] = mmLanguage("", "ko");
		languageMap_["Georgian"] = mmLanguage("ka");
        languageMap_["Kurdish"] = mmLanguage("ku");
        languageMap_["Kyrgyz"] = mmLanguage("ky");
		// languageMap_["Kurmanji"] = mmLanguage("kmr");
		// languageMap_["Latin"] = mmLanguage("la");
        languageMap_["Lingala"] = mmLanguage("ln");
        languageMap_["Lao"] = mmLanguage("lo");
		languageMap_["Lithuanian"] = mmLanguage("lt");
		languageMap_["Latvian"] = mmLanguage("lv");
        languageMap_["Macedonian"] = mmLanguage("mk");
		languageMap_["Mongolian"] = mmLanguage("mn"); // (New) Mongolian
		languageMap_["Marathi"] = mmLanguage("mr");
        languageMap_["Malay"] = mmLanguage("ms");
        languageMap_["Maltese"] = mmLanguage("mt");
        languageMap_["Burmese"] = mmLanguage("my");
		// languageMap_["Ethiopic"] = mmLanguage("mul_ET"); // Pan-Ethiopic hyphenation
		languageMap_["norwegianBokmål"] = mmLanguage("nb");
        languageMap_["Nepali"] = mmLanguage("ne");
		languageMap_["NorwegianNynorsk"] = mmLanguage("nn");	// Norwegian Nynorsk hyphenation
        languageMap_["Oromo"] = mmLanguage("om");
		// languageMap_["Panjabi"] = mmLanguage("pa");
		languageMap_["Polish"] = mmLanguage("pl");
		languageMap_["polski"] = mmLanguage("pl");
		// languageMap_["Piedmontese"] = mmLanguage("pms");
        languageMap_["Pashto"] = mmLanguage("ps");
		languageMap_["Portuguese"] = mmLanguage("pt");
		// languageMap_["Romansh"] = mmLanguage("rm");
		// 150p16, MIR-756: support for Romanian while we're at it ... cf turkish
		languageMap_["Romanian"] = mmLanguage("ro", "ro", "ROM", "latn");
		languageMap_["Russian"] = mmLanguage("ru");
        languageMap_["Sami"] = mmLanguage("se");
        languageMap_["Sinhalese"] = mmLanguage("si");
		// 150p16, MIR-756: support for Seribian cyrillic while we're at it ... cf turkish
		languageMap_["Serbian"] = mmLanguage("sr_Cyrl", "sr", "SRB", "cyrl");	// Serbian Cyrillic script
		languageMap_["latinSerbian"] = mmLanguage("sr_Latn");	// Serbian Latin script
		languageMap_["simplifiedChinese"] = mmLanguage("", "zh_CN");
		languageMap_["Slovak"] = mmLanguage("sk");
		languageMap_["Slovenian"] = mmLanguage("sl");
        languageMap_["Swahili"] = mmLanguage("sw");
        languageMap_["Albanian"] = mmLanguage("sq");
		languageMap_["Swedish"] = mmLanguage("sv");
		languageMap_["Svenska"] = mmLanguage("sv");
		// languageMap_["Telugu"] = mmLanguage("te");
		languageMap_["Thai"] = mmLanguage("th", "th", "", "thai"); // script is thai 
		languageMap_["Turkmen"] = mmLanguage("tk");
		languageMap_["traditionalChinese"] = mmLanguage("", "zh_TW"); // no hyphenation pattern
		// 150p16, MIR-756: for turkish, default to fontLanguage to TRK, font script to latn
		languageMap_["Turkish"] = mmLanguage("tr", "tr", "TRK", "latn");
		languageMap_["Ukrainian"] = mmLanguage("uk");
        languageMap_["Uzbek"] = mmLanguage("uz");
        languageMap_["Vietnamese"] = mmLanguage("vi");
		// languageMap_["Pinyin"] = mmLanguage("zh_Latn"); // CJK 4.8.0

		// VERSION 1.2.0p588: Indic scripts, use hi for hyphenation language for Hindi
		// Use extended script codes where given: see https://xmlgraphics.apache.org/fop/trunk/complexscripts.html
		// languageMap_["Bengali"] = mmLanguage("bn", "bn", "", "bng2"); // hyphenation=bn, locale=bn, language=default, may need font-script="beng" override
		languageMap_["Gujarati"] = mmLanguage("gu", "gu", "", "gjr2"); // Gujarati: may need font-script "gujr" override 
        languageMap_["Hausa"] = mmLanguage("ha");
        languageMap_["Hebrew"]	= mmLanguage("", "he", "", "hebr"); // script is hebr 
		languageMap_["Hindi"] = mmLanguage("hi", "hi", "", "dev2"); // may need font-script="deva" override
		languageMap_["Kannada"] = mmLanguage("kn", "kn", "", "knd2"); // may need font-script="knda" override
        languageMap_["Khmer"] = mmLanguage("km");
		languageMap_["Malayalam"] = mmLanguage("ml", "ml", "", "mlm2"); // Malayalam: may need font-script="mlym" override
		languageMap_["Oriya"] = mmLanguage("or", "or", "", "ory2"); // may need font-script="orya" override
		languageMap_["Punjabi"] = mmLanguage("", "pa", "", "gur2"); // Gurmukhi: No hyphenation rules, locale="pa", fontlanguage=default, fontscript="guru"
		// languageMap_["Sanskrit"] = mmLanguage("sa", "sa", "SAN", "dev2"); // Mangal font has SAN for language, dev2 for script. Hyphenation is "sa"
		languageMap_["Tamil"] = mmLanguage("ta", "ta", "", "tml2"); // May need font-script="taml" override
		languageMap_["Telugu"] = mmLanguage("te", "te", "", "tel2"); // hyphenation="te", May need font-script="telu" override
        languageMap_["Tonga"] = mmLanguage("to");

		// The following languages are supported in some way (to determine rtl, fontlanguage or fontScript) but
		// don't have hyphenation patterns (hyphenation language = "")
		// languageMap_["unicode"] = mmLanguage("", "root");
		// languageMap_["None"] = mmLanguage("", "");
		languageMap_["Arabic"] = mmLanguage("", "ar", "", "arab");
        languageMap_["Uyghur"] = mmLanguage("ug");
		languageMap_["Urdu"]	= mmLanguage("", "ur", "URD", "arab"); // fontLanguage is URDU, fontScript is arab
        languageMap_["Wolof"] = mmLanguage("wo");
        languageMap_["Xhosa"] = mmLanguage("xh");
        languageMap_["Yoruba"] = mmLanguage("yo");
        languageMap_["Zulu"] = mmLanguage("zu");

		languageMap_["Persian"]	= mmLanguage("", "fa"); // Farsi
		// languageMap_["Brazilian"] = mmLanguage("", "pt_BR");
		// languageMap_["Norwegian"] = mmLanguage("", "no"); // See nowegianBokmal and Nyorsk
		languageMap_["Tibetan"] = mmLanguage("", "bo");




mmCollations::mmCollations()
{
    // af
//collations_["Afrikaans"] = QStringList() << "standard" ;
    // am
//collations_["Amharic"] = QStringList() << "standard" ;
    // ar
collations_["Arabic"] = QStringList() << "compat" << "standard" ;
    // as
//collations_["Assamese"] = QStringList() << "standard" ;
    // az
collations_["Azerbaijani"] = QStringList() << "search" << "standard" ;
    // be
//collations_["Belarusian"] = QStringList() << "standard" ;
    // bg
//collations_["Bulgarian"] = QStringList() << "standard" ;
    // bn
collations_["Bengali"] = QStringList() << "standard" << "traditional" ;
    // bo
//collations_["Tibetan"] = QStringList() << "standard" ;
    // bs
collations_["Bosnian"] = QStringList() << "search" << "standard" ;
    // bs
//collations_["Bosnian"] = QStringList() << "standard" ;
    // ca
collations_["Catalan"] = QStringList() << "search" << "standard" ;
    // cs
collations_["Czech"] = QStringList() << "standard" << "digits-after" ;
    // cu Old Church Slavonic, Old Bulgarian
//collations_[""] = QStringList() << "standard" ;
    // cy
//collations_["Welsh"] = QStringList() << "standard" ;
    // da
collations_["Danish"] = QStringList() << "search" << "standard" ;
    // de
collations_["newGerman"] = QStringList() << "search" << "phonebook" << "eor" ;
    // dz
//collations_["Dzongkha"] = QStringList() << "standard" ;
    // ee
//collations_["Ewe"] = QStringList() << "standard" ;
    // el
//collations_["Greek"] = QStringList() << "standard" ;
    // en
//collations_["usEnglish"] = QStringList() << "standard" ;
    // eo Esperanto
//collations_["Esperanto"] = QStringList() << "standard" ;
    // es
collations_["Spanish"] = QStringList() << "search" << "standard" << "traditional" << "traditional" ;
    // et
//collations_["Estonian"] = QStringList() << "standard" ;
    // fa
//collations_["Persian"] = QStringList() << "standard" ;
    // fi
collations_["Finnish"] = QStringList() << "search" << "traditional" << "standard" ;
    // fo
collations_["Faroese"] = QStringList() << "search" << "standard" << "standard" ;
    // fr
//collations_["French"] = QStringList() << "standard" ;
    // gl
collations_["Galician"] = QStringList() << "search" << "standard" ;
    // gu
collations_["Gujarati"] = QStringList() << "standard" << "standard" ;
    // ha
//collations_["Hausa"] = QStringList() << "standard" ;
    // he
collations_["Hebrew"] = QStringList() << "search" << "standard" ;
    // hi
//collations_["Hindi"] = QStringList() << "standard" ;
    // hr
collations_["Croatian"] = QStringList() << "search" << "standard" ;
    // hu
collations_["Hungarian"] = QStringList() << "standard" << "standard" ;
    // hy
//collations_["Armenian"] = QStringList() << "standard" ;
    // ig
//collations_["Igbo"] = QStringList() << "standard" ;
    // is
collations_["Icelandic"] = QStringList() << "search" << "standard" ;
    // ja
collations_["Japanese"] = QStringList() << "private-kana" << "standard" << "unihan" ;
    // ka
//collations_["Georgian"] = QStringList() << "standard" ;
    // kk
//collations_["Russian"] = QStringList() << "standard" ;
    // kl
collations_["Greenlandic"] = QStringList() << "search" << "standard" ;
    // km
//collations_["Khmer"] = QStringList() << "standard" ;
    // kn
collations_["Kannada"] = QStringList() << "standard" << "traditional" ;
    // ko
collations_["Korean"] = QStringList() << "standard" << "search" << "searchjl" << "unihan" ;
    // ku
//collations_["Kurdish"] = QStringList() << "standard" ;
    // ky
//collations_["Kyrgyz"] = QStringList() << "standard" ;
    // ln
collations_["Lingala"] = QStringList() << "standard" << "phonetic" ;
    // lo
//collations_["Lao"] = QStringList() << "standard" ;
    // lt
//collations_["Lithuanian"] = QStringList() << "standard" ;
    // lv
//collations_["Latvian"] = QStringList() << "standard" ;
    // mk
//collations_["Macedonian"] = QStringList() << "standard" ;
    // ml
//collations_["Malayalam"] = QStringList() << "standard" ;
    // mn
//collations_["Mongolian"] = QStringList() << "standard" ;
    // mr
//collations_["Marathi"] = QStringList() << "standard" ;
    // mt
//collations_["Maltese"] = QStringList() << "standard" ;
    // my
//collations_["Burmese"] = QStringList() << "standard" ;
    // nb
collations_["norwegianBokmål"] = QStringList() << "search" << "standard" ;
    // ne
//collations_["Nepali"] = QStringList() << "standard" ;
    // nn
collations_["NorwegianNynorsk"] = QStringList() << "search" << "standard" ;
    // om
//collations_["Oromo"] = QStringList() << "standard" ;
    // or
//collations_["Oriya"] = QStringList() << "standard" ;
    // pa
//collations_["Punjabi"] = QStringList() << "standard" ;
    // pl
//collations_["Polish"] = QStringList() << "standard" ;
    // ps
//collations_["Pashto"] = QStringList() << "standard" ;
    // ro
//collations_["Romanian"] = QStringList() << "standard" ;
    // root ?
// collations_[""] = QStringList() << "standard" << "search" << "eor" << "private-unihan" << "emoji" ;
    // ru
//collations_["Russian"] = QStringList() << "standard" ;
    // se
collations_["Sami"] = QStringList() << "search" << "standard" ;
    // si
collations_["Sinhalese"] = QStringList() << "standard" << "dictionary" ;
    // sk
collations_["Slovak"] = QStringList() << "search" << "standard" ;
    // sl
collations_["Slovenian"] = QStringList() << "standard" << "standard" ;
    // sq
collations_["Albanian"] = QStringList() << "standard" << "standard" ;
    // sr
//collations_["Serbian"] = QStringList() << "standard" ;
    // sr
collations_["Serbian"] = QStringList() << "search" << "standard" ;
    // sv
collations_["Swedish"] = QStringList() << "standard" << "search" << "reformed" ;
    // ta
//collations_["Tamil"] = QStringList() << "standard" ;
    // te
//collations_["Telugu"] = QStringList() << "standard" ;
    // th
//collations_["Thai"] = QStringList() << "standard" ;
    // tk
//collations_["Turkmen"] = QStringList() << "standard" ;
    // to
//collations_["Tonga"] = QStringList() << "standard" ;
    // tr
collations_["Turkish"] = QStringList() << "standard" << "search" ;
    // ug
//collations_["Uyghur"] = QStringList() << "standard" ;
    // uk
//collations_["Ukrainian"] = QStringList() << "standard" ;
    // ur
//collations_["Urdu"] = QStringList() << "standard" ;
    // uz
//collations_["Uzbek"] = QStringList() << "standard" ;
    // vi
collations_["Vietnamese"] = QStringList() << "standard" << "traditional" ;
    // vo ?
//collations_[""] = QStringList() << "standard" ;
    // wo
//collations_["Wolof"] = QStringList() << "standard" ;
    // yi ?
//collations_[""] = QStringList() << "search" << "standard" ;
    // yo
//collations_["Yoruba"] = QStringList() << "standard" ;
    // zh
collations_["simplifiedChinese"] = QStringList() << "pinyin" << "gb2312han" << "stroke" << "zhuyin" << "big5han" << "unihan" << "private-pinyin"  ;
    
standardCollation_ = QStringList() << "standard";    
}

