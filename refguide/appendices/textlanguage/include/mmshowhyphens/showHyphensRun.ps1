#--------------------------------------------------------------------------------
#- showHyphensRun.ps1
#--------------------------------------------------------------------------------
$showHyphensHomeDir	= "$env:appendices\textlanguage\include\mmshowhyphens"
cd "$showHyphensHomeDir"

# write-host	"xxxxxxxxxxxxxxxxxxxxxx $env:minimumAfterHyphen"
write-host	"xxxxxxxxxxxxxxxxxxxxxx "

# mmshowhyphens -d hyph_de_DE.dic -i testWords1.in -o testWords5.out
# mmShowhyphens -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile $env:minimumAfterHyphen
# mmShowhyphens -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile --right-hyphen-min 1
# mmShowhyphens.exe -d hyph_de_DE.dic  -i deutschText1.in -o zzz.out -mah 3
# mmShowhyphens.exe -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile $env:minimumAfterHyphen
mmShowhyphens.exe -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile $env:minimumAfterHyphen
