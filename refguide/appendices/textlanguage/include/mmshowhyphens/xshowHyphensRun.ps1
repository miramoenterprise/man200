#--------------------------------------------------------------------------------
#- showHyphensRun.ps1
#--------------------------------------------------------------------------------
$showHyphensHomeDir	= "$env:appendices\textlanguage\include\mmshowhyphens"
cd "$showHyphensHomeDir"

write-host	"xxxxxxxxxxxxxxxxxxxxxx $env:minimumAfterHyphen"
$env:shz	"--right-hyphen-min 2"

# mmshowhyphens -d hyph_de_DE.dic -i testWords1.in -o testWords5.out
# mmShowhyphens -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile $env:minimumAfterHyphen
# mmShowhyphens -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile --right-hyphen-min 1
mmShowhyphens -d $env:dictionaryFile -i $env:inputWordsFile -o $env:showHyphensFile # "$env:shz"
