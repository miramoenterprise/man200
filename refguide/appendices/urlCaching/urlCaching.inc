@-----------------------------------------------------------
@- capitalization.inc
@-----------------------------------------------------------
@- urlCaching.inc
@- include files for urlCaching appendix
@-----------------------------------------------------------
<chapterStart
	dest="urlCaching"
        type="appendix"
        changeBar="Y"
	>URL media and template caching
</chapterStart>
@-----------------------------------------------------------
<@include>	${appendices}/urlCaching/urlCaching

@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
