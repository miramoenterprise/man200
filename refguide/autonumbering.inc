@--------------------------------------------------------------------------------
@- autonumbering.inc
@- include files for Inline Markup Codes
@--------------------------------------------------------------------------------
<@skipStart>
<@include>	${refguide}/cover/cover.inc
<@include>	${refguide}/toc/toc
<@include>	${refguide}/running/running.inc
@- <@include>	${refguide}/msm/msm.inc
<@include>	${refguide}/miramoxml/miramoxml.inc
<@include>	${inline}/inline.inc
<@include>	${mfd}/mfd.inc
<@include>	${appendices}/appendicesStart
<@include>	${appendices}/fonts/fonts.inc
<@include>	${appendices}/textlanguage/textlanguage.inc
<@skipEnd>
<@include>	${appendices}/autonumbering/autonumbering.inc
