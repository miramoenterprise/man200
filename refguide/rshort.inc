@--------------------------------------------------------------------------------
@- rhort.inc
@- 
@--------------------------------------------------------------------------------
@- <@include>	${refguide}/cover/cover.inc
@- <@include>	${refguide}/toc/toc.inc
@- <@include>	${refguide}/running/running.inc
@- <@include>	${refguide}/miramoSimpleMarkup/miramoSimpleMarkup.inc
@- <@include>	${refguide}/propertyTypes/propertyTypes.inc
@- <@include>	${refguide}/miramoxml/miramoxml.inc
@--------------------------------------------------------------------------------
@- inline
@--------------------------------------------------------------------------------
<chapterStart
	dest="inlineMarkupElements"
	pgfSuffix="ExA"
	>Inline markup elements
	</chapterStart>
<#def> inINLINE         1
<#def> elementType      inline
<Section allPages="MP_referenceGuide" />
@------------------------------------------------
<@include>	${inline}/image
@------------------------------------------------
<#def> inINLINE         0
<chapterEnd/>
<@skipStart>
@--------------------------------------------------------------------------------
@- mfd
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> inMFD            1
<#def> elementType      mfd
<Section allPages="MP_referenceGuide" />
@------------------------------------------------
<@include>	${mfd}/docdef
@------------------------------------------------
<#def> inMFD            0
<chapterEnd/>
@--------------------------------------------------------------------------------
@- <@include>	${appendices}/appendices.inc
@- <@include>	${refguide}/indexes/index.inc

<@skipEnd>
@--------------------------------------------------------------------------------
