@--------------------------------------------------------------------------------
@- index.inc
@- 
@--------------------------------------------------------------------------------
@- <chapterStart
@- 	dest="refguideIndex"
@- 	>Index
@- </chapterStart>
<@macro> includeIndex1 {
	@if(@match({X<#includeIndex1>X}, {XYX})) {
		<Section
			feather="Y"
			page="MP_indexFirst"
			allPages="MP_indexRight"
			@- page="MP_specialIndexFirst"
			@- allPages="MP_specialIndexRight"
			@- pageNumber="1"
			@- pageNumberStyle="lowercaseRoman"
			/>
		<!-- CK: 11/02/18: commented out excess paragraphs which were included to get around space-below issue with spanned paragraphs <P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P>-->
		<!-- CK: changed from 30mm space below to 5mm -->
		<P fmt="P_chapterTitle"
			textColumnSpan="all"
			listLabel="N"
			spaceBelow="5mm"
			>Index</P>
		<!-- CK: 11/02/18: commented out <P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P> -->
		}
}
@--------------------------------------------------------------------------------
<|includeIndex1>
@--------------------------------------------------------------------------------
@---x <P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P>
@---x <P fmt="Body" span="all" spaceBelow="20mm" >&nbsp;</P>
<@include>	${refguide}/indexes/index
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------
@- <chapterEnd/>
@--------------------------------------------------------------------------------

