@--------------------------------------------------------------------------------
@- sideheaddef.inc
@--------------------------------------------------------------------------------
<#def> sideheadsHomeDir		{${mfd}/include/paradef/sideheads/}
@--------------------------------------------------------------------------------
<#def> sideheadExampleFile1	{sideheadExampleFile1.pdf}
<@include>			{<#sideheadsHomeDir>/shHiddenText.mmp}
<@include>			{<#sideheadsHomeDir>/shAnnotations.mmp}
@--------------------------------------------------------------------------------
<exp pgfProps="position='topOfPage'" >
yyyycbm
</exp>
@------------------------------------------------------------------------
@- sidehead example 1 parameters
@------------------------------------------------------------------------
<#def> pageWidth		74mm
<#def> pageHeight		105mm
<#def> pageHeight		95mm
@------------------------------------------------------------------------
@- fg properties for foreground/main text flow
@------------------------------------------------------------------------
<#def> fgLeft			44mm
<#def> fgTop			08mm
<#def> fgWidth			43mm
<#def> fgHeight			107mm
<#def> fgBorder			{penWidth="1.8pt" penColor="Red" penStyle="dotted" }
<#def> fgFillColor		{fillColor="Green" fillOpacity="100" fillTint="5%" }
@-------------------------------------------------------------------------
<#def> shGap			6mm
<#def> shGap			18pt
<#def> shWidth			28mm
<#def> shTextSize		14pt
<#def> shSpaceAbove		10mm
@------------------------------------------------------------------------
<#def> outputScale		50	@- Percent scale value for output display
<#def> outputPageWidth		@eval(<#pageWidth> * <#outputScale> / 100)
<#def> outputFigureWidth	@eval(<#outputPageWidth> + 6)

@------------------------------------------------------------------------
@- All the following are vertical positions of annotation text as a 
@- fraction of page height <#pageHeight> (above).
@------------------------------------------------------------------------
<#def> shDepth			0.82	@- Length of vertical sidehead indicator rules
					@- as fraction of page height
<#def> shWidthDepth		0.30	@- Position of 'sideheadWidth' text.
<#def> shGapDepth		0.52	@- Position of 'sideheadgap' text.
<#def> fgLeftFromTop		0.94	@- 
@------------------------------------------------------------------------
<shHiddenText/>
@------------------------------------------------------------------------

@- (see line <xlinenum
@- <xlineRef>RunningTextDef.ex.location</xlineRef>

@------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="6|12|18"
	>
<exampleCaption
	dest="ex.sideheads.1"
	>Format definitions for sidehead examples
</exampleCaption>
<hc><#sideheadExampleBegin></hc>
@- 
<ParaDef paraDef="Heading1shA" sidehead="Y" sideheadGap="<#shGap>"
	sideheadWidth="<#shWidth>" ... <hc>
spaceAbove="<#shSpaceAbove>"
        fontWeight="Bold"
        textSize="<#shTextSize>"
</hc> />
<ParaDef paraDef="Heading1shB" sidehead="Y" sideheadGap="<#shGap>"
	sideheadWidth="<#shWidth>" ... <hc>
spaceAbove="<#shSpaceAbove>"
        fontWeight="Bold"
	textAlign="end"
	sideheadVerticalAlign="firstBaselines"
        textSize="<#shTextSize>"
</hc> >
	<paraFrame startMargin="3pt" topMargin="5pt" endMargin="3pt" topRule="Thin"
		@- bottomMargin="4pt" bottomRule="Thin" fillColor="Green" fillTint="20%" />
		@- bottomMargin="4pt" bottomRule="Thin" fillColor="Salmon" fillTint="30%" />
		bottomMargin="4pt" bottomRule="Thin" svgDef="grayGradient" />
</ParaDef>
<FontDef fontDef="Author" textSize="-30%" fontWeight="Regular" />
</exampleBlock>

<#def> sideheadFormats		<#example_output>

@------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="6|12|18"
	>
<exampleCaption
	dest="ex.sideheads.2"
	>Sidehead example (1)
</exampleCaption>
<!-- = <xfEPS1><#cn1></xfEPS1> =========================================== -->
<P paraDef="Heading1shA" >
The White Tiger<br/><Font fontDef="Author" >by Aravind Adiga</Font></P>
<P paraDef="Body" <hc>fontFamily="Frutiger LT 45 Light" hyphenate="N" </hc>> Hearing that Chinese premier Wen Jiabao is to visit India, ...<hc>a successful Bangalore entrepreneur
@- decides to write a letter to inform him about the <fI>real</fI> India, as distinct from the India he will see during his
official visit.</hc>
</P>
<!-- = <xfEPS1><#cn2></xfEPS1> =========================================== -->
@- <P paraDef="Heading1shA" >
<P paraDef="Heading1shA" <xfB>textAlign="end" sideheadVerticalAlign="textTops"</xfB> >
The White Tiger<br/><Font fontDef="Author" >by Aravind Adiga</Font></P>
<P paraDef="Body" <hc>fontFamily="Frutiger LT 45 Light" hyphenate="N" </hc>>Balram's letter also explains what it takes to become ...<hc>an entrepreneur</hc>.</P>
<!-- = <xfEPS1><#cn3></xfEPS1> =========================================== -->
<P paraDef="Heading1shB" >
The White Tiger<br/><Font fontDef="Author" >by Aravind Adiga</Font></P>
<P paraDef="Body" >Hello</P>
@--------------------------------------------------------------------------------
<hc><#sideheadExampleEnd></hc>
</exampleBlock>

<#def> example_output		@trim(<#sideheadFormats><#example_output>)
<#def> example_output		@gsub(<#example_output>, { \.\.\.}, { })
<@write> -n 			<#sideheadsHomeDir>/sideheads1.mm {<#example_output>}

@--------------------------------------------------------------------------------
@- 'runJob' macro is in 'runJob.mmp' in ${control}/exampleProcessing folder
@--------------------------------------------------------------------------------
<runJob
	file="sideheadsRun1.ps1"
	folder="<#sideheadsHomeDir>"
	parameter1="sideheadsHomeDir='<#sideheadsHomeDir>'"
	parameter2="sideheadExampleFile='<#sideheadExampleFile1>'"
	parameter2="sideheadExampleFile='sideheadExampleFile1.pdf'"
/>
@--------------------------------------------------------------------------------


@--------------------------------------------------------------------------------

<exp>
Xxx Xxxx <cnum>1</cnum> Xxx Xxxxyz
<exampleOutput
	width="<#outputFigureWidth>"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	@- minimumHeight="44mm"
	leftMargin="3mm"
	topMargin="3mm"
	bottomMargin="2.0mm"
	belowGap="1.5mm"
	>
<outputCaption
	dest="fig.paraFrameHorizontal"
	>
Lateral <#xparaFrame> extents
</outputCaption>
<AFrame wrapContent="Y" align="start" 
	fillColor="Green"
	fillOpacity="100"
	fillTint="7%"
	penWidth="0.3pt"
	penColor="Green"
	penTint="30"
	>
<xFrameShadow
	shadowDistance="2.5pt"
	shadowAngle="122"
	shadowBlend="screen"
	@- shadowColor="rgb(#ccccd9)"
	shadowColor="rgb(#ccd9cc)"
	shadowDeviation="2"
	fillOpacity="100"
	/>
<Image width="<#outputPageWidth>"
	@- file="C:\u\miramo\man\man100\refguide\mfd\include\paradef\sideheads/yy.pdf"
	@- file="<#sideheadsHomeDir>/<#sideheadExampleFile1>"
	file="<#sideheadsHomeDir>/sideheadExampleFile1.pdf"
	/>
	<mmDraw>
<shAnnotations/>
	</mmDraw>
</AFrame>
</exampleOutput>

</exp>
@--------------------------------------------------------------------------------
