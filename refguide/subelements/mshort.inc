@--------------------------------------------------------------------------------
@- mfd.inc
@- include files for Miramo Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> inMFD		1
<#def> elementType	mfd
@- <@include>	${mfd}/introduction
<Section allPages="MP_referenceGuide" />
<#def> elementStart.firstExample	1
@- <@include>	${mfd}/xxparadef5.inc
@- <@include>	${mfd}/svgdef
@- <@include>	${mfd}/shadowdef
@-<@include>	${mfd}/tbldef
@-<@include>	${mfd}/rectangle
@- <@include>	${mfd}/paradeftest
@- <@include>	${mfd}/metadata
@- <@include>	${mfd}/footnotedef
@- <@include>	${mfd}/numbersequencedef
@- <@include>	${mfd}/rectangle
<@include>	${mfd}/paradef
@- <@include>      ${mfd}/textframedef	@- Not a problem
@- <@include>      ${mfd}/vardef		@- Not a problem
@- <@include>      ${mfd}/xrefdef
@- <@include>	${mfd}/textframedef
@- <@include>	${mfd}/smallCaps
@--------------------------------------------------------------------------------
<@skipStart>
@--------------------------------------------------------------------------------
<@include>	${mfd}/changebardef
<@include>	${mfd}/colordef
<@include>	${mfd}/contentsdef
<@include>	${mfd}/datedef
<@include>	${mfd}/docdef
<@include>	${mfd}/fnotedef
<@include>	${mfd}/fontdef
<@include>	${mfd}/indexdef
<@include>	${mfd}/mathmldef
<@include>	${mfd}/pagedef
<@include>	${mfd}/paradef
<@include>	${mfd}/paragroupdef
<@include>	${mfd}/ruledef
<@include>	${mfd}/runningtextdef
<@include>	${mfd}/sectiondef
<@include>	${mfd}/tbldef
<@include>	${mfd}/tblcolumndef
<@include>	${mfd}/tblcontinuationdef
<@include>	${mfd}/textframedef
<@include>	${mfd}/vardef
<@include>	${mfd}/xrefdef
<#def> inMFD		0
@--------------------------------------------------------------------------------
<@skipEnd>
@--------------------------------------------------------------------------------
<chapterEnd/>
@--------------------------------------------------------------------------------
