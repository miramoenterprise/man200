@--------------------------------------------------------------------------------
@- sideheaddef2.inc
@--------------------------------------------------------------------------------
<@include>			{<#sideheadsHomeDir>/shHiddenText.mmp}
<@include>			{<#sideheadsHomeDir>/shAnnotations.mmp}
<@skipStart>
@------------------------------------------------------------------------
@- sidehead example 1 parameters
@------------------------------------------------------------------------
<#def> pageWidth		74mm
<#def> pageHeight		105mm
<#def> pageHeight		95mm
<#def> pageHeight		85mm
@------------------------------------------------------------------------
@- fg properties for foreground/main text flow
@------------------------------------------------------------------------
<#def> fgLeft			44mm
<#def> shGap			18pt
<#def> shWidth			28mm
<#def> fgTop			08mm
<#def> fgWidth			43mm
<#def> fgHeight			107mm
<#def> fgBorder			{penWidth="1.8pt" penColor="Red" penStyle="dotted" }
<#def> fgFillColor		{fillColor="Green" fillOpacity="100" fillTint="5%" }
@-------------------------------------------------------------------------
<#def> bodySpaceAbove		6pt
<#def> bodyTextSize		10pt
<#def> bodyLeading		4pt
@-------------------------------------------------------------------------
<#def> shGap			18pt
<#def> shWidth			28mm
<#def> shTextSize		14pt
<#def> shSpaceAbove		10mm
<#def> shSpaceAbove		29pt
@------------------------------------------------------------------------
@- <@skipEnd>
@------------------------------------------------------------------------

@------------------------------------------------------------------------
@- All the following are vertical positions of annotation text as a 
@- fraction of page height <#pageHeight> (above).
@------------------------------------------------------------------------
<@skipEnd>
@------------------------------------------------------------------------

<#def> outputScale		60	@- Percent scale value for output display
<#def> outputScale		54	@- Percent scale value for output display
<#def> outputScale		50	@- Percent scale value for output display
<#def> outputScale		70	@- Percent scale value for output display
<#def> outputScale		64	@- Percent scale value for output display
<#def> pageWidth		74mm
<#def> pageWidth		82mm
<#def> pageWidth		80mm
<#def> outputPageWidth		@eval(<#pageWidth> * <#outputScale> / 100)
<#def> outputFigureWidth	@eval(<#outputPageWidth> + 6)
@------------------------------------------------------------------------
<#def> fgLeft			41.5mm
<#def> fgLeft			47.5mm
<#def> fgLeft			47.0mm
<#def> fgLeft			46.0mm
<#def> shGap			18pt
@- <#def> shWidth			26mm
<#def> pageHeight		75mm
@------------------------------------------------------------------------
<#def> shDepth			0.90	@- Length of vertical sidehead indicator rules
<#def> shDepth			0.80	@- Length of vertical sidehead indicator rules
					@- as fraction of page height
<#def> shWidthDepth		0.48	@- Position of 'sideheadWidth' text.
<#def> shGapDepth		0.88	@- Position of 'sideheadgap' text.
<#def> fgLeftFromTop		0.94	@- 
@------------------------------------------------------------------------
<shHiddenText/>
@------------------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.sideheads.2" /> shows the input used to produce
the output shown in Figure <xparanumonly
	id="fig.sideheads.2" />.
</exp>
@------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="6|12|18"
	>
<exampleCaption
	dest="ex.sideheads.2"
	>Sidehead example (1)
</exampleCaption>
<!-- = <xfEPS1><#cn4></xfEPS1> =========================================== -->
<P paraDef="Heading1shB"  <xfB> sideheadVerticalAlign="firstBaselines" </xfB> >
The Analects<br/><Font fontDef="Author" >of Confucius</Font></P>
<P paraDef="Body1shpA" >sideheadVerticalAlign<br/>="firstBaselines"</P>
<!-- = <xfEPS1><#cn5></xfEPS1> =========================================== -->
<P paraDef="Heading1shB" <xfB> sideheadVerticalAlign="tops" </xfB> >
The Analects<br/><Font fontDef="Author" >of Confucius</Font></P>
<P paraDef="Body1shpA" >sideheadVerticalAlign<br/>="tops"</P>
@--------------------------------------------------------------------------------
<hc><#sideheadExampleEnd></hc>
</exampleBlock>

@--------------------------------------------------------------------------------
@- <#grayGradientEx> defined in ${control}/svgDefs.mmp
@--------------------------------------------------------------------------------
@- <#def> example_output		@trim(<#sideheadExampleBegin><#sideheadFormats><#example_output>)
<#def> example_output		@trim(<#sideheadExampleBegin><#grayGradientEx><#sideheadFormats><#example_output>)
<#def> example_output		@gsub(<#example_output>, { \.\.\.}, { })
<@write> -n 	<#sideheadsHomeDir>/sideheadExampleFile2.mm {<#example_output>}

@--------------------------------------------------------------------------------
@- 'runJob' macro is in 'runJob.mmp' in ${control}/exampleProcessing folder
@--------------------------------------------------------------------------------
<runJob
	file="sideheadsRun1.ps1"
	folder="<#sideheadsHomeDir>"
	parameter1="sideheadsHomeDir='<#sideheadsHomeDir>'"
	parameter2="sideheadExampleFile='sideheadExampleFile2'"
	@- sideheadExampleFile1.mm
/>
@--------------------------------------------------------------------------------


@--------------------------------------------------------------------------------

<exp>
In the general case there is no difference 
between setting the <pn>sideheadVerticalAlign</pn> to <pv>textTops</pv> or
to <pv>tops</pv>: the vertical position of the sidehead's associated (following)
paragraph is adjusted so that the top of its first line of text aligns with
to top of the first line of sidehead text, as illustrate in
sideheads <cnum>2</cnum> in Figure <xparanumonly
	id="fig.sideheads.1" />.
In the case either the sidehead or it's associated paragraph
has a <#xparaFrame> sub-element with a <pn>topRule</pn> or
a <pn>topMargin</pn>, the shift of the associated paragraph may
be much increased.
This is illustrated by
sideheads <cnum>4</cnum> (<pv>textTops</pv>)
and <cnum>5</cnum> (<pv>tops</pv>)
in Figure <xparanumonly
	id="fig.sideheads.2" />.

<exampleOutput
	width="<#outputFigureWidth>"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	@- minimumHeight="44mm"
	leftMargin="3mm"
	topMargin="3mm"
	bottomMargin="2.0mm"
	belowGap="1.5mm"
	>
<outputCaption
	dest="fig.sideheads.2"
	>
Output from Examples <xparanumonly
	id="ex.sideheadFormats.1" /> and <xparanumonly
	id="ex.sideheads.2" />
</outputCaption>
<AFrame wrapContent="Y" align="start" 
	fillColor="Green"
	fillOpacity="100"
	fillTint="10%"
	penWidth="0.3pt"
	penColor="Green"
	penTint="30"
	shadowDef="defaultShadow"
	>
	<@skipStart>
<xFrameShadow
	shadowDistance="2.5pt"
	shadowAngle="122"
	shadowBlend="screen"
	@- shadowColor="rgb(#ccccd9)"
	shadowColor="rgb(#ccd9cc)"
	shadowDeviation="2"
	fillOpacity="100"
	/>
	<@skipEnd>
<Image width="<#outputPageWidth>"
	@- file="<#sideheadsHomeDir>/sideheadExampleFile2.pdf"
	file="${mfd}/include/paradef/sideheads/sideheadExampleFile2.pdf"
	/>
	<mmDraw>
<shAnnotations/>
<shAnnotationNumber count="1" number="4" showHeight="Y" />
<shAnnotationNumber count="2" number="5" />
	</mmDraw>
</AFrame>
</exampleOutput>
</exp>

<exp>
The height of a sidehead, dimension h<Font
	fontFamily="Frutiger LT 45 Light" verticalShift="48"
	textSize="70%">sh</Font> in
sidehead <cnum>4</cnum> in Figure <xparanumonly
	id="fig.sideheads.2" />, is calculated as shown in Equation <xparanumonly
	id="equation.ParaDef.sideheadHeight" />.
</exp>

<exEquation2 fli="10mm" si="5mm" dest="equation.ParaDef.sideheadHeight"
	equalBefore="26mm"
	equalAfter="5mm"
	spaceBelow="9pt"
        >
h<Font verticalShift="48" textSize="70%">sh</Font>        = (<pn>textSize</pn> + <pn>leading</pn>) x <fI><Font textSize="90%" >line count</Font></fI> - <pn>leading</pn><br/>
                + <fI><Font textSize="90%" >top rule width</Font></fI> + <pn>topMargin</pn><br/>
                + <fI><Font textSize="90%" >bottom rule width</Font></fI> + <pn>bottomMargin</pn>
</exEquation2>


<@skipStart>
<#def> objectHeight             ((shTextSize + leading>) x lineCount)
                                         - leading
                                         + <#shTopRuleWidth> + <#shTopMargin>
                                         + <#shBottomRuleWidth> + <#shBottomMargin>
                                         )
<@skipEnd>


@--------------------------------------------------------------------------------
