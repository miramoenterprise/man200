@-----------------------------------------------------------
@- textframedef.inc
@-----------------------------------------------------------
@- <chapterStart
@- 	dest="inlineMarkupElements"
@- 	pgfSuffix="ExA"
@- 	>Inline markup elements
@- </chapterStart>
<#def> inINLINE		1
<#def> elementType	inline
@-----------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@-----------------------------------------------------------
<@include>	${mfd}/textframedef
@-----------------------------------------------------------
<#def> inINLINE		0
@-----------------------------------------------------------
@- <chapterEnd/>
@-----------------------------------------------------------
