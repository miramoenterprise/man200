<@skipStart>
@--------------------------------------------------------------------------------
@- language.inc
@- include files for languages
@--------------------------------------------------------------------------------
<MasterPageRule
	page="First"
	allPages="Right"
	/>
<@include>      ${refguide}/language/thai/thaiText
<@include>      ${refguide}/language/intro
@--------------------------------------------------------------------------------
<@include>	${refguide}/language/multi
<@skipEnd>

@-----------------------------------------------------------
@- languages.inc
@- include files for Languages
@-----------------------------------------------------------
<chapterStart
	dest="languages"
	>Languages
</chapterStart>
@-----------------------------------------------------------
<@include>	${refguide}/language/intro
<@include>	${refguide}/language/language
<@include>	${refguide}/language/multi
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------

