<P ff="Arial" p="10pt"
        wn="N" wp="N"
        fw="Regular"
        @- align="both"
        justify="LR"
        fi="30mm" li="30mm" ri="37mm"
        lang="de" hyphenate="Y"
        >
Herkunft und Jugend
</P>

<P>
Johann Wolfgang von Goethe wurde am 28. August 1749 im heutigen Goethe-Haus am Frankfurter Großen Hirschgraben geboren. Der Vater Johann Caspar Goethe (1710–1782) war Jurist, übte diesen Beruf jedoch nicht aus, sondern lebte von den Erträgen seines Vermögens, das später auch dem Sohn ein Leben ohne finanzielle Zwänge ermöglichen sollte.[3] Er war vielseitig interessiert und gebildet, jedoch auch streng und pedantisch, was wiederholt zu Konflikten in der Familie führte.
</P>

<P>
Goethes Mutter, Catharina Elisabeth Goethe, geb. Textor (1731–1808), entstammte einer wohlhabenden und angesehenen Frankfurter Familie; ihr Vater war als Stadtschultheiß der ranghöchste Justizbeamte der Stadt. Die lebenslustige und kontaktfreudige Frau hatte mit 17 Jahren den damals 38-jährigen Rat Goethe geheiratet. Nach Johann Wolfgang wurden noch fünf weitere Kinder geboren, von denen jedoch nur die wenig jüngere Schwester Cornelia das Kindesalter überlebte. Mit ihr stand der Bruder in einem engen Vertrauensverhältnis. Ihren Sohn nannte die Mutter ihren „Hätschelhans“.
</P>

<P>
Die Geschwister erhielten eine aufwendige Ausbildung. Von 1756 bis 1758 besuchte Johann Wolfgang eine öffentliche Schule. Danach wurde er gemeinsam mit der Schwester vom Vater sowie durch Hauslehrer unterrichtet. Auf dem Stundenplan standen u. a. Französisch, Englisch, Italienisch, Latein, Griechisch, naturwissenschaftliche Fächer, Religion und Zeichnen. Außerdem lernte er Cellospielen, Reiten, Fechten und Tanzen.
</P>

<P>
Schon früh kam der Junge in Kontakt mit Literatur. Das begann mit den Gutenachtgeschichten der Mutter und mit der Bibellektüre in der frommen, lutherisch-protestantischen Familie. Zu Weihnachten 1753 bekam er von der Großmutter ein Puppentheater geschenkt. Das für diese Bühne vorgesehene Theaterstück lernte er auswendig und führte es immer wieder mit Begeisterung gemeinsam mit Freunden auf.[4] Erste Ansätze seiner literarischen Phantasie bewies der kleine Goethe auch mit seinem (nach eigener Aussage) „anmaßenden“[5] Talent, wunderliche Märchen zu erfinden und seinen staunenden Freunden in der Ich-Form zur spannenden Unterhaltung aufzutischen. Gelesen wurde viel im Hause Goethe; der Vater besaß eine Bibliothek von rund 2000 Bänden. So lernte Goethe schon als Kind unter anderem das Volksbuch vom Dr. Faust kennen. Im Zuge des Siebenjährigen Krieges war von 1759 bis 1761 ein französischer Offizier im Elternhaus einquartiert. Ihm und der mitgereisten Schauspieltruppe verdankte Goethe seine erste Begegnung mit der französischen Dramenliteratur.
</P>
