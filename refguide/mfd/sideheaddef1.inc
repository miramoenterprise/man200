@--------------------------------------------------------------------------------
@- sideheaddef.inc
@--------------------------------------------------------------------------------
<#def> sideheadsHomeDir		{${mfd}/include/paradef/sideheads/}
@--------------------------------------------------------------------------------
<#def> sideheadExampleFile1	{sideheadExampleFile1.pdf}
<@include>			{<#sideheadsHomeDir>/shHiddenText.mmp}
<@include>			{<#sideheadsHomeDir>/shAnnotations.mmp}
<@include>			{<#sideheadsHomeDir>/shAnnotationNumbers.mmp}
@------------------------------------------------------------------------
@- sidehead example 1 parameters
@------------------------------------------------------------------------
<#def> pageWidth		74mm
<#def> pageHeight		105mm
<#def> pageHeight		95mm
<#def> pageHeight		95mm
<#def> pageFillTint		10%	@- Page bg color (in containing AFrame)
@------------------------------------------------------------------------
@- fg properties for foreground/main text flow
@------------------------------------------------------------------------
<#def> fgLeft			44mm
<#def> fgTop			23pt
<#def> fgWidth			43mm
<#def> fgHeight			107mm
<#def> fgBorder			{penWidth="1.6pt" penColor="Red" penStyle="dotted" }
<#def> fgFillColor		{fillColor="Green" fillOpacity="100" fillTint="5%" }
<#def> fgFillColor		{fillColor="Green" fillOpacity="100" fillTint="4%" }
@-------------------------------------------------------------------------
<#def> bodySpaceAbove		6pt
<#def> bodyTextSize		10pt
<#def> bodyLeading		4pt
@-------------------------------------------------------------------------
<#def> shGap			18pt
<#def> shWidth			80pt
@-------------------------------------------------------------------------
<#def> shTextSize		14pt
<#def> shLeading		 4pt
<#def> shTopMargin		 8pt
<#def> shBottomMargin		 4pt
<#def> shSpaceAbove		23pt
@-------------------------------------------------------------------------
<#def> shTopRule		 Medium
<#def> shBottomRule		 Medium
<#def> shTopRuleWidth		 2pt
<#def> shBottomRuleWidth	 2pt
@------------------------------------------------------------------------
<#def> outputScale		50	@- Percent scale value for output display
<#def> outputScale		100	@- Percent scale value for output display
<#def> outputScale		60	@- Percent scale value for output display
<#def> outputScale		48	@- Percent scale value for output display
<#def> outputPageWidth		@eval(<#pageWidth> * <#outputScale> / 100)
<#def> outputFigureWidth	@eval(<#outputPageWidth> + 6)

@------------------------------------------------------------------------
@- All the following are vertical positions of annotation text as a 
@- fraction of page height <#pageHeight> (above).
@------------------------------------------------------------------------
<#def> shDepth			0.86	@- Length of vertical sidehead indicator rules
					@- as fraction of page height
<#def> shWidthDepth		0.33	@- Position of 'sideheadWidth' text.
<#def> shGapDepth		0.61	@- Position of 'sideheadgap' text.
<#def> fgLeftFromTop		0.97	@- 
@------------------------------------------------------------------------
<shHiddenText/>
@------------------------------------------------------------------------
<exp>
</exp>

@--------------------------------------------------------------------------------
<elementExampleTitle
	dest="ParaDef.ex.sideheads"
	>Sideheads
</elementExampleTitle>

@--------------------------------------------------------------------------------

@- (see line <xlinenum
@- <xlineRef>RunningTextDef.ex.location</xlineRef>

<exp>
Example <xparanumonly
	id="ex.sideheadFormats.1" /> defines two sidehead paragraph
formats,
Heading1shA
and
Heading1shB, one non-sidehead paragraph format,
Body1shpA, and one <#xFontDef>, Author, for use in 
Examples <xparanumonly
	id="ex.sideheads.1"
	/> and <xparanumonly
	id="ex.sideheads.2"
	/>.
</exp>

@------------------------------------------------------------------------
@- Format definitions
@------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="6|12|18"
	>
<exampleCaption
	dest="ex.sideheadFormats.1"
	>Format definitions for sidehead examples
</exampleCaption>
@- <hc><#sideheadExampleBegin></hc>
@- <hc><shHiddenText/></hc>
@- 
<!-- ===================================================== -->
<ParaDef paraDef="Heading1shA" sidehead="Y" sideheadGap="<#shGap>" <xlineRef>sidehead.ex.pgfADef.start</xlineRef>
	sideheadWidth="<#shWidth>" ... <hc>
	spaceAbove="<#shSpaceAbove>"
        fontWeight="Bold"
        leading="<#shLeading>"
        hyphenate="N"
        textSize="<#shTextSize>"
		@- >
		@- <paraFrame topRule="Thin"
		@- bottomRule="Thin" />
	</hc> /><xlineRef>sidehead.ex.pgfADef.end</xlineRef>
@- </hc> </ParaDef>
<!-- ===================================================== -->
<ParaDef paraDef="Heading1shB" sidehead="Y" sideheadGap="<#shGap>"<xlineRef>sidehead.ex.pgfBDef.start</xlineRef>
	sideheadWidth="<#shWidth>" ... <hc>
spaceAbove="<#shSpaceAbove>"
        fontWeight="Bold"
	textAlign="end"
        hyphenate="N"
        leading="<#shLeading>"
	sideheadVerticalAlign="firstBaselines"
        textSize="<#shTextSize>"
</hc> >
	<paraFrame startMargin="4pt" topMargin="<#shTopMargin>" endMargin="3pt" topRule="<#shTopRule>"
		@- bottomMargin="<#shBottomMargin>" bottomRule="<#shBottomRule>" fillColor="Green" fillTint="20%" />
		bottomMargin="<#shBottomMargin>" bottomRule="<#shBottomRule>" fillColor="Salmon" fillTint="30%" />
		@- bottomMargin="<#shBottomMargin>" bottomRule="<#shBottomRule>" svgDef="grayGradient" />
	</ParaDef> <xlineRef>sidehead.ex.pgfBDef.end</xlineRef>
<!-- ===================================================== -->
<ParaDef paraDef="Body1shpA" ... <hc>
        fontFamily="Corbel"
	spaceAbove="<#bodySpaceAbove>"
        fontWeight="Regular"
	textAlign="start"
        leading="<#bodyLeading>"
        hyphenate="N"
        textSize="<#bodyTextSize>"
</hc> > <xlineRef>sidehead.ex.pgfCDef.start</xlineRef>
	<paraFrame startMargin="3pt" topMargin="6pt" endMargin="3pt" topRule="Thin"
		bottomMargin="4pt" bottomRule="Thin" fillColor="Blue" fillTint="20%" />
		@- bottomMargin="4pt" bottomRule="Thin" svgDef="grayGradient" />
	</ParaDef> <xlineRef>sidehead.ex.pgfCDef.end</xlineRef>
<FontDef fontDef="Author" textSize="-30%" fontWeight="Regular" />
</exampleBlock>
@------------------------------------------------------------------------
<#def> sideheadFormats		<#example_output>
@------------------------------------------------------------------------


<#def> eacute	@ucs2_to_utf8(00E9)
<#def> eacute	@hex2bin(<#eacute>)


<exp>
Example <xparanumonly
	id="ex.sideheads.1" > shows input that uses the formats defined
in Example <xparanumonly
	id="ex.sideheadFormats.1" /> to produce the output shown in
Figure <xparanumonly
	id="fig.sideheads.1" >.
</exp>
@------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="6|12|18"
	>
<exampleCaption
	dest="ex.sideheads.1"
	>Sidehead example (1)
</exampleCaption>
<!-- = <xfEPS1><#cn1></xfEPS1> =========================================== -->
<P paraDef="Heading1shA" > <xlineRef>sidehead.ex.pgfA.start</xlineRef>
The White Tiger<br/><Font fontDef="Author" >by Aravind Adiga</Font></P><xlineRef>sidehead.ex.pgfA.end</xlineRef>
<P paraDef="Body" <hc>fontFamily="Frutiger LT 45 Light" hyphenate="N" </hc>> Hearing that Chinese premier Wen Jiabao is to visit India, ...<hc>a successful Bangalore entrepreneur
@- decides to write a letter to inform him about the <fI>real</fI> India, as distinct from the India he will see during his
official visit.</hc>
</P>
<!-- = <xfEPS1><#cn2></xfEPS1> =========================================== -->
@- <P paraDef="Heading1shA" >
<P paraDef="Heading1shA" <xfB>textAlign="end" sideheadVerticalAlign="textTops"</xfB> ><xlineRef>sidehead.ex.pgfB.start</xlineRef>
The Tiger's Wife<br/><Font fontDef="Author" >by T<#eacute>a Obreht</Font></P>
<P paraDef="Body" <hc>fontFamily="Frutiger LT 45 Light" hyphenate="N" </hc>>Another novel with tiger ...<hc>in its title</hc>.</P> <xlineRef>sidehead.ex.pgfB.end</xlineRef>
<!-- = <xfEPS1><#cn3></xfEPS1> =========================================== -->
<P paraDef="Heading1shB" textAlign="start" ><paraFrame svgDef="grayGradient" /> <xlineRef>sidehead.ex.pgfC.start</xlineRef>
The Night Tiger<br/><Font fontDef="Author" >by Yangsze Choo</Font></P>
<P paraDef="Body" <hc>fontFamily="Frutiger LT 45 Light" hyphenate="N" </hc>>None of these tiger novels are really about tigers.</P> <xlineRef>sidehead.ex.pgfC.end</xlineRef>
@--------------------------------------------------------------------------------
<hc><#sideheadExampleEnd></hc>
</exampleBlock>


@--------------------------------------------------------------------------------
<#def> example_output		@trim(<#sideheadExampleBegin><#grayGradientEx><#sideheadFormats><#example_output>)
<#def> example_output		@gsub(<#example_output>, { \.\.\.}, { })
<@write> -n <#sideheadsHomeDir>/sideheadExampleFile1.mm {<#example_output>}

@--------------------------------------------------------------------------------
@- 'runJob' macro is in 'runJob.mmp' in ${control}/exampleProcessing folder
@--------------------------------------------------------------------------------
<runJob
	file="sideheadsRun1.ps1"
	folder="<#sideheadsHomeDir>"
	parameter1="sideheadsHomeDir='<#sideheadsHomeDir>'"
	parameter2="sideheadExampleFile='sideheadExampleFile1'"
/>
@--------------------------------------------------------------------------------



@--------------------------------------------------------------------------------
<exp>
Sidehead <cnum>1</cnum> in Figure <xparanumonly
	id="fig.sideheads.1" /> uses the Heading1shA paragraph
format, lines <xlinenum
	id="sidehead.ex.pgfA.start" 
	/>&endash;<xlinenum id="sidehead.ex.pgfA.end"
		/> in Example <xparanumonly 
		id="ex.sideheads.1" /> (defined
		in Example <xparanumonly 
	id="ex.sideheadFormats.1" />, lines <xlinenum id="sidehead.ex.pgfADef.start"
	/>&endash;<xlinenum id="sidehead.ex.pgfADef.end"
		/>), to produce the default sidehead
layout, <fI>viz.</fI> the sidehead text is aligned at the
start of the sidehead area and the first baseline of the sidehead's
associated (following) paragraph is aligned the with the first
baseline of the sidehead, i.e. the default for
the <pn>sideheadVerticalAlign</pn> property is <pv>firstBaselines</pv> (see
page <xnpage
	id="ParaDef.property.sideheadVerticalAlign" />).



<exampleOutput
	width="<#outputFigureWidth>"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	@- minimumHeight="44mm"
	leftMargin="3mm"
	topMargin="3mm"
	bottomMargin="2.0mm"
	belowGap="1.5mm"
	>
<outputCaption
	dest="fig.sideheads.1"
	>
Output from Examples <xparanumonly
	id="ex.sideheadFormats.1" /> and <xparanumonly
	id="ex.sideheads.1" />
</outputCaption>
<AFrame wrapContent="Y" align="start" 
	fillColor="Green"
	fillOpacity="100"
	fillTint="<#pageFillTint>"
	penWidth="0.3pt"
	penColor="Green"
	penTint="30"
	shadowDef="defaultShadow"
	>
	<@skipStart>
<xFrameShadow
	shadowDistance="2.5pt"
	shadowAngle="122"
	shadowBlend="screen"
	@- shadowColor="rgb(#ccccd9)"
	shadowColor="rgb(#ccd9cc)"
	shadowDeviation="2"
	fillOpacity="100"
	/>
	<@skipEnd>
<Image width="<#outputPageWidth>"
	@- file="<#sideheadsHomeDir>/sideheadExampleFile1.pdf"
	file="${mfd}/include/paradef/sideheads/sideheadExampleFile1.pdf"
	/>
	<mmDraw>
<shAnnotations/>
<shAnnotationNumber count="1" number="1" />
<shAnnotationNumber count="2" number="2" numberTop="106pt"/>
<shAnnotationNumber count="2" number="3" numberTop="182pt"/>
	</mmDraw>
</AFrame>
</exampleOutput>
</exp>

<exp>
Sidehead <cnum>2</cnum> in Figure <xparanumonly
	id="fig.sideheads.1" /> also uses the Heading1shA paragraph
format, but with overrides that specify sidehead end text
alignment and the position of the first baseline of the associated (following) 
paragraph vertically adjusted to align with the top of the text in the sidehead
paragraph (<pn>sideheadVerticalAlign</pn> is set to <pv>textTops</pv>, see line <xlinenum
	id="sidehead.ex.pgfB.start" 
		/> in Example <xparanumonly 
		id="ex.sideheads.1" />).
</exp>

<exp>
Sidehead <cnum>3</cnum> in Figure <xparanumonly
	id="fig.sideheads.1" /> uses the Heading1shB paragraph
format, lines <xlinenum
	id="sidehead.ex.pgfC.start" 
	/>&endash;<xlinenum id="sidehead.ex.pgfC.end"
		/> in Example <xparanumonly 
		id="ex.sideheads.1" /> (defined
		in Example <xparanumonly 
	id="ex.sideheadFormats.1" />, lines <xlinenum id="sidehead.ex.pgfCDef.start"
	/>&endash;<xlinenum id="sidehead.ex.pgfCDef.end"
		/>), to produce a sidehead with a background fill, start,
top, end and bottom margins and top and bottom
rulings, all specified by the <#xParaDef> <#xparaFrame> sub-element.
</exp>

@--------------------------------------------------------------------------------
