@------------------------------------------------------------
@- colordef
@------------------------------------------------------------

<elementStart elementType="xml_no_cont" elementName="ColorDef" >

<esp>
Use <#xColorDef> to define a color format.
</esp>

</elementStart>


@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Color format name"
	dest="<#elementName>.propertyGroup.start"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="colorDef"
	value="name"
	>
<pdp>
<#Required>
<br/>
<fI>name</fI> is color format name.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	title="Reference another color format name"
	dest="<#elementName>.propertyGroup.colorRef"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="colorRef"
	value="name"
	>
<pdp>
Use color values defined by another <#xColorDef> as defaults
for the following properties. <fI>name</fI> is the value of
another <#xColorDef> element's <pn>colorDef</pn> property.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="CMYK values"
	longTitle=""
	dest="<#elementName>.propertyGroup.cmyk"
	condition="fmcGuide"
	>If one or more of the following CMYK properties is set,
the color model is assumed to be CMYK.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="CMYK values"
	longTitle=""
	dest="<#elementName>.propertyGroup.cmyk"
	condition="mmcGuide"
	>If one or more of the following CMYK properties is set,
the color specification is assumed to be CMYK and the values
are converted to RGB.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="c" long="cyan"
        value="percent" >
<pdp>
Percentage cyan.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="m" long="magenta"
        value="percent" >
<pdp>
Percentage magenta.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="y" long="yellow"
        value="percent" >
<pdp>
Percentage yellow.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="k" long="black"
        value="percent" >
<pdp>
Percentage black.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="RGB values"
	longTitle=""
	dest="<#elementName>.propertyGroup.rgb"
	>If one or more of the following RGB properties is set,
and no CMYK properties are set, the
color <outputRule
	condition="fmcGuide">model</outputRule><outputRule
	condition="mmcGuide">specification</outputRule> is assumed to be RGB.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="r" long="red"
        value="percent" >
<pdp>
Percentage red.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="g" long="green"
        value="percent" >
<pdp>
Percentage green.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property short="b" long="blue"
        value="percent" >
<pdp>
Percentage blue.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

<@skipStart>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Color tint"
	longTitle=""
	dest="<#elementName>.propertyGroup.tints"
	>If the <on>tintBase</on> and the <on>tintBase</on> properties
all other properties are ignored.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="tintBase" long="tintBase"
        value="name" >
<pdp>
The name of the color from which the tint is derived.
</pdp>

<propertyDefault>Black</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="tint"
	long="tintPercent"
        value="percent" >
<pdp>
100% indicates solid color. Less than 100% indicates a reduced
percentage of the color. Ignored unless <on>tintBase</on> is specified.
</pdp>
</property>
<@skipEnd>


@------------------------------------------------------------
@- Spot colors
@------------------------------------------------------------
@- http://wiki.scribus.net/canvas/How_to_use_spot_colours_with_Scribus
@------------------------------------------------------------
<propertyGroup
	shortTitle="Spot colors"
	longTitle=""
	dest="<#elementName>.propertyGroup.spot_colors"
	condition="fmcGuide"
	>The following properties are for defining a spot color.
If the <on>libraryName</on> and the <on>inkName</on> properties
all other properties are ignored.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="libraryName"
	long="libraryName"
        value="key"
	condition="fmcGuide"
	>
<pdp>
Color library name. The <on>libraryName</on> property is used with
the <on>inkName</on> property (see page <xnpage
id="ColorDef.property.inkName" />) to specify a spot color ink.
</pdp>

<propertyValueKeyList
	second="14.5mm" newRow="1"
        description="Color library name"
	>
<vk key="crayon" >
'Crayon'.<br/>
Key alias: <ov>Crayon</ov>.<br/>
Example ink name: <ov>Apricot</ov>.
</vk>
<vk key="dic" >
'DIC COLOR GUIDE SPOT'.<br/>
Key alias: <ov>DICcolor</ov>.<br/>
Example ink name: <ov>lp</ov>.
</vk>
<vk key="focol" >
'FOCOLTONE'.<br/>
Key alias: <ov>focoltone</ov>.<br/>
Example ink name: <ov>1070</ov>.
</vk>
<vk key="greys" >
'Greys'.<br/>
Key alias: <ov>Greys</ov>.<br/>
Example ink name: <ov>1% Grey.prcs</ov>.
</vk>
<vk key="mun-hi" >
'MUNSELL&reg; High Chroma Colors'.<br/>
Key alias: <ov>munsellHighChroma</ov>.<br/>
Example ink name: <ov>2.5R 7:10</ov>.
</vk>
<vk key="mun-bk" >
'MUNSELL&reg; Book of Color'.<br/>
Key alias: <ov>munsellBook</ov>.<br/>
Example ink name: <ov>2.5R 9:1</ov>.
</vk>
<vk key="online" >
'Online'.<br/>
Key alias: <ov>Online</ov>.<br/>
Example ink name: <ov>001-990033</ov>.
</vk>
<vk key="panC" >
'PANTONE&reg; Coated'<br/>
Key alias: <ov>pantoneCoated</ov>.<br/>
Example ink name: <ov>Yellow</ov>.
</vk>
<vk key="panPS" >
'PANTONE&reg; ProSim'.<br/>
Key alias: <ov>pantoneProSim</ov>.<br/>
Example ink name: <ov>Process Yellow</ov>.
</vk>
<vk key="panU" >
'PANTONE&reg; Uncoated'.<br/>
Key alias: <ov>pantoneUncoated</ov>.<br/>
Example ink name: <ov>Yellow</ov>.
</vk>
<vk key="panPE" >
'PANTONE&reg; Process Euro'.<br/>
Key alias: <ov>pantoneProcessEuro</ov>.<br/>
Example ink name: <ov>E 1-1</ov>.
</vk>
<vk key="panPSE" >
'PANTONE ProSim EURO&reg;'.<br/>
Key alias: <ov>pantoneProSimEuro</ov>.<br/>
Example ink name: <ov>Process Yellow</ov>.
</vk>
<vk key="panPCSG" >
'PANTONE&reg; Process CSG'.<br/>
Key alias: <ov>pantoneProcessCSG</ov>.<br/>
Example ink name: <ov>1-1</ov>.
</vk>
<vk key="toyo" >
'TOYO COLOR FINDER'.<br/>
Key alias: <ov>Toyo</ov>.<br/>
Example ink name: <ov>0001pc*</ov>.
</vk>
<vk key="tru" >
'TRUMATCH 4-Color Selector'.<br/>
Key alias: <ov>Trumatch</ov>.<br/>
Example ink name: <ov>1-a</ov>.
</vk>
<vk key="custom" >
'mmCustom'.<br/>
Key alias: <ov>mmCustom</ov>.<br/>
Example ink name: <ov>myInk</ov>.
</vk>

</propertyValueKeyList>

</property>

@------------------------------------------------------------
<property
	short="inkName"
	long="inkName"
	condition="fmcGuide" 
	value="name"
	>
<pdp>
Use the <on>inkName</on> property to specify the name of
an ink within the color library specified by
the <on>libraryName</on> property (see page <xnpage
id="ColorDef.property.libraryName" />). The <on>inkName</on> property
requires the <on>libraryName</on> property.
</pdp>

<pdp>
Valid ink names for a color library may be found from the FrameMaker
color definitions GUI.
</pdp>

</property>


@------------------------------------------------------------
<propertyGroup
	shortTitle="Ink density print property"
	longTitle=""
	dest="<#elementName>.propertyGroup.printing"
	condition="fmcGuide" 
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="o"
	long="overprint"
        value="N | Y"
	condition="fmcGuide" 
	>
<pdp>
Y =" Overprint." N =" Knockout."
</pdp>

<propertyDefault>N</propertyDefault>
</property>



<|propertiesEnd>
<@skipStart>

@------------------------------------------------------------
<|propertiesEnd> {
STANDARD1
<#Required>
<br/>
Every <#xColorDef> element must be terminated by a <#yColorDef> element
}

<elementNote>
<MkDest fmt="B" id="colordef.reserved_colors_list" />
The following is a list of FrameMaker reserved colors:
Black, White, Red, Green, Blue, Cyan, Magenta, Yellow,
Dark Grey, Pale Green, Forest Green, Royal Blue, Mauve, Light Salmon, Olive and Salmon.

<elementNote>
The following is a default color definition:

<@nChartEx> {10|40} {fi="30" li="30" nolines} {no_endpara } {
<ColorDef
	fmt="default"		<!-- Default color settings --> 
	c =" 0"		<!-- Cyan % --> 
	m =" 0"		<!-- Magenta % --> 
	y =" 0"		<!-- Yellow % --> 
	k =" 0"		<!-- Black % --> 
/> 
}
<elementNote>
Color definitions do not inherit properties from preceding color
definitions.
A short <#xColorDef>, as follows

<@nChartEx> {10|40} {nolines fi="30" li="30" } {P="P_RefList}" {
<ColorDef
	fmt =" vlight.yell"	<!-- Color name -->
	y =" 10"		<!-- Yellow % --> 
/> 
}

is equivalent to:

<@nChartEx> {10|40} {fi="30" li="30" nolines} {P="P_RefList}" {
<ColorDef
	fmt =" vlight.yell"		<!-- Color name --> 
	c =" 0"		<!-- Cyan % --> 
	m =" 0"		<!-- Magenta % --> 
	y =" 10"		<!-- Yellow % --> 
	k =" 0"		<!-- Black % --> 
/> 
}

regardless of preceding color definitions.

<elementNote>
It is possible to reference a <#osq>non-existent<#csq> color. 
For example
a <#xCell> element or a <#xRuleDef> may have an property
color="daydream" without <#osq>daydream<#csq> being a color
defined either in a template file or in a <#xColorDef>
element. In this case no error will be reported by
Miramo, and the FrameMaker document will contain a
color definition equivalent to:


<@nChartEx> {16|41|50|59|68} {fi="30" li="30" nolines} {no_endpara} {
<!-- Equivalent <#osqb>non-existent<#csqb> color def --> 
<ColorDef	fmt =" ""/xfI/colorname/xfnI/"	c =" 0"	m =" 0"	y =" 0"	k =" 100"  /> 
}

<elementNoteP>
i.e. the undefined color is regarded as black. However the undefined
color is defined by default as <#osq>invisible<#csq>, with the
result that neither the intended color nor black is actually printed.
To avoid such problems, ensure that all colors referenced are defined.

<elementExamples>
The following defines a new color called <#osq>p165<#csq>:

<@nChartEx> {10|40} {nolines} {no_endpara} {
<ColorDef
	fmt ="p165"	<!--  Name this definition <#osqb>p165<#csqb> --> 
	c =" 0"	<!--  Cyan % --> 
	m =" 60"	<!--  Magenta % --> 
	y =" 100"	<!--  Yellow % --> 
	k =" 0"	<!--  Black % --> 
/>
}


@- <ColorDef colorDef="myColor1"
@- 	libraryName="panU"
@- 	inkName="Reflex Blue"
@- 	/>

@- <AFrame A="R" W="22mm" H="11mm" fill="0" color="myColor1" />


<Comment>
<@suspend>
<P paraDef="P_RefBody" >
Example
<xparanumonly id="ex.swatch_1
 illustrates extracting a picture object from
a Microsoft Excel structured storage file.



<P paraDef="P_RefBody" >
<#def> swatchWidth	40mm
<#def> swatchHeight	30mm
<#def> swatchWidth	20mm
<#def> swatchHeight	10mm
<#def> margin		1.1mm
<#def> swatchHeight2	@eval(<#swatchHeight> -  <#margin> * 2)
<#def> swatchWidth2	@eval(<#swatchWidth> -  <#margin> * 2)

@------------------------------------------------------------
<pdfExample
	Width="<#swatchWidth>"
	Height="<#swatchHeight>"
        substr="ColorDef"
        file="swatch_1"
        XRef="swatch_1"
        type="AFrame"
        Caption="Using the <#osq>objName<#csq> property"
        inExample="Y"
	specialCmd="1"
        copy="Y"
        >
        <nExample noshow="Y" fi="20mm" li="4mm" >
@------------------------------------------------------------
<DocDef pcolor="myColor1" />
<ColorDef colorDef="myColor1"
	libraryName="panU"
	overprint="Y"
	inkName="Reflex Blue"
	/>
<AFrame W="<#swatchWidth>" H="<#swatchHeight>"  color="myColor1" xxHide P="R"  pen="0" fill="0"  pw="0" >
</AFrame>
</nExample>
@------------------------------------------------------------
</pdfExample>
<@resume>
</Comment>

<@skipEnd>
@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
