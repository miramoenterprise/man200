@------------------------------------------------------------
@-- pagedef
@------------------------------------------------------------

<elementStart elementType="xml_mand_atts" elementName="PageDef" >

<esp>
Use the <#xPageDef> element to define a named master
page layout, specifying text columns and background
text (e.g. running headers and footers) and
graphic objects.
</esp>

<esp>
The special page names <#osq>Left<#csq> and <#osq>Right<#csq> may be
used to define default page layouts used within
a document.
<vidx id="<#osq>Left<#csq> master pages" />
<vidx id="<#osq>Right<#csq> master pages" />
<vidx id="master pages||<#osq>Left<#csq> and <#osq>Right<#csq>" />
</esp>

</elementStart>

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	@- title="Create new master or reference page"
	title="Create new master page"
	dest="<#elementName>.optgroup.start"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="pageDef"
	long="pageDef"
	value="name"
	>
<pdp>
<#Required>
<br/>
<fI>name</fI> is the name of master page.
</pdp>

</property>


@------------------------------------------------------------
<@include>	${CTEXT}/pageProperties.inc
@------------------------------------------------------------
<@include>	${CTEXT}/fillProperties.inc
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="copy"
	long="copyPage"
	value="name"
	condition="fmcGuide"
	>
<pdp>
Name of master page to copy onto this page, optionally connecting
tagged text frames on the named master page with matching 
tagged text frames defined using this <#xPageDef> element. See
the <on>copycon</on> property below for information on controlling
how these connections are made.
</pdp>

<pdp>
Document objects (text frames, graphic objects, etc) on the named 
master page may either be copied on top of
or beneath objects created using this <#xPageDef> element:
this is controlled using the <on>copytop</on> property below. 
</pdp>

<pdp>
The named master page may be taken from a template file 
or may be created using another <#xPageDef> element.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="copytop"
	long="copyTop"
	value="N | Y"
	condition="fmcGuide"
	>
<pdp>
Copy master page named using the <on>copy</on> property
on top of (<ov>Y</ov>) or beneath (<ov>N</ov>) the objects created
by this <#xPageDef> element.
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="copycon"
	long="copyConnect"
	value="B | E | N"
	condition="fmcGuide"
	>
<pdp>
Connect (<ov>B</ov>,<ov>E</ov>) or don't connect (<ov>N</ov>) tagged text frames
on master page given using the <on>copy</on> property to the beginning (<ov>B</ov>)
or end (<ov>E</ov>) of text frames on this page where flow tags
on the target (this) master page match flow tags on the copied
page.
</pdp>

<pdp>
<ov>N</ov>: don't connect text frames with matching tags. In 
this case, where text frames have matching tags
the top-most text frame takes precedence (see
<on>copytop</on> property above).
</pdp>

<propertyDefault>B</propertyDefault>
</property>

<@skipStart>
@------------------------------------------------------------
<property
	short="a"
	long="pageAngle"
	value="0 | 90 | 180 | 270"
	>
<pdp>
Page angle. Specifies rotation angle for page
in degrees anticlockwise from the page orientation
indicated by the <#xDocDef> element.
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="type"
	long="pageType"
	value="M | R"
	>
<pdp>
Page type.<br/>
M = Master page, R = Reference page.
</pdp>

<propertyDefault>M</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short=""
	long=""
	value=""
	>
<pdp>
Zero or more of the following elements may 
be enclosed within the <#xPageDef> element:
<P paraDef="P_Inline_Opt" nl="Y" fi="5mm" li="5mm">
<#xTextFrame>
<#xmmDraw>
<#xFrame>
<#xImage>
</pdp>

</property>
<@skipEnd>

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------



@------------------------------------------------------------
<elementNote>

<enp>
The <#xTextFrame> element is used to define text
column layouts on body pages
which are formatted using this master page.
<@skipStart>
The <#xmmDraw> element enables drawing elements to
be incorporated on the master page
(see the <#MRef>, pages
<xnpage 
	id="mmDraw.element.start" 
	/><xphyphen/><xnpage 
	id="mmDraw.element.end" 
	/>).
<@skipEnd>
</enp>

<enp>
The <#xFrame> element specifies a fixed graphics frame or rectangle.
The <#xImage> element enables external graphics to be imported
onto the master page.
</enp>

<enp>
@- In the case a background image or tint is required to cover
@- all of the master page (e.g. a multi-sided bleed), or intersects
@- with the main text flow, then the <#xFrame> element
@- should not be used. Instead the <#xImage> element or the <#xRectangle> element
If a background image or tint on a master page will be
behind or intersect a text frame containing a foreground
text flow, then the <#xFrame> element<vidx
id="main text flow" /> should not be used as a container for a tint, graphic
or image.
Instead the <#xImage> element may be used
directly within the <#xPageDef> element, as illustrated in Example
<xparanumonly id="ex.background_tint_and_image.2" />.
<vidx id="background tint||on master page" />
<vidx id="background image||on master page" />
<vidx id="image||on master page" />
<vidx id="foreground text flow" />
<vidx id="tint||on master page" />
<vidx id="bleed||on master page" />
</enp>

<exampleBlock
	>
<exampleCaption
	dest="ex.background_tint_and_image.2"
	>Including a background image or tint with bleed</exampleCaption>
	<Image width="8.5in" height="11.5in" top="-.25in" left="-.25in"
		file="myBackgroundImage"/>
<xfI>or</xfI>
	<Frame width="8.5in" height="11.5in" top="-.25in" left="-.25in"
		penOpacity="0" fillColor="myBackgroundTint"/>
</exampleBlock>

<enp>
The <#xFrame> element is described on pages <xnpage
	id="Frame.element.start" /><xphyphen/><xnpage
	id="Frame.element.end"  />.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote>
<enp>
A single document may include pages with different sizes.
</enp>
</elementNote>



<@skipStart>
@------------------------------------------------------------
<elementNote>

<enp>
More than one <#xTextFrame> (tagged <#osq>A<#csq>)
may be included within a page definition, and
all will appear on the master page. 
However, if there is more than one such text column
on the master page, the first <#xTextFrame>
within the <#xPageDef> will be used to reformat text on body
pages when the master page is applied using the <#xSection> element.
</enp>
</elementNote>

@------------------------------------------------------------
<elementNote>
<enp>
If a master page with the same name as the master page defined
using <#xPageDef> is imported from a template file
using the <#xTemplate> element <#osq>l="Y"<#csq> or <#osq>left="Y"<#csq> properties, 
the resulting master page in the Miramo document
contains text columns and background text and
graphics from both the template file and the <#xPageDef>
element: i.e., the effect is additive.
</enp>

<enp>
If this master page is subsequently 
applied to a body page using the <#xSection> element,
the text column layout specified using the <#xPageDef> element
takes precedence over that specified within the template. Thus, if
the <#xPageDef> incorporates one or more <#xTextFrame> elements,
the body page text columns are reformatted according to the text column
or columns defined within the first <#xTextFrame> element.
</enp>

<enp>
However, all background text and graphics 
from both the template and <#xPageDef> master page definition will be
displayed on the resulting body page.
</enp>

</elementNote>
@------------------------------------------------------------
<@skipEnd>

@------------------------------------------------------------

<exp>
Example
<xparanumonly 
	id="ex.pagedef_using_pagedef.1" /> illustrates using the <#xPageDef> element
to set up two-column and three-column master pages.
@-  both
@- using running header and footers copied from
@- a <#osq>Background<#csq> master page:
</exp>


<exampleBlock
	lineNumbers="Y"
	tabs="5.5|11|15|20"
	>
<exampleCaption
	dest="ex.pagedef_using_pagedef.1"
	>Using the <#xPageDef> element (1)</exampleCaption>
<!--	Define document page dimensions        -->
<DocDef pageWidth="8.5in"  pageHeight="11in" />
<!--	Define master page with running H/Fs   -->
<PageDef pageDef="MP_twoColumns" >
	<!-- 	Page header                  --> 
	<TextFrame type="background"<xlineRef>PageDef.exLine.hf.start</xlineRef>
		left="1.4in" width="6in" height="0.5in" top="1in" >
		<Line top="0.4in" /> <!-- <xfI>horizontal ruling within text frame</xfI> -->
		<P paraDef="Header" >Document title</P></TextFrame>
	<!--	Page footer contains page number <xfI>n</xfI> of <xfI>last page number</xfI> -->
	<TextFrame type="background"
		left="1.4in" width="6in" height="0.5in" top="10in" >
		><P><currentPageNumber/> of <lastPageNumber/></P></TextFrame><xlineRef>PageDef.exLine.hf.end</xlineRef>
	<!-- 	Foreground (main) text flow  --> 
	<TextFrame type="foreground"
		left="1.4in" width="6in" height="7.5in" top="2in" columns="2" />
</PageDef>
<!--	Define 'MP_threeColumns' master page -->
<PageDef pageDef="MP_threeColumns" >
	<!-- <xfI>repeat header and footer format definitions here (lines <xParaText>PageDef.exLine.hf.start.lineNum</xParaText> to <xParaText>PageDef.exLine.hf.end.lineNum</xParaText> above)</xfI> -->
	<!-- 	Foreground (main) text flow  --> 
	<TextFrame type="foreground"
		left="1.4in" width="6in" height="7.5in" top="2in" columns="3" />
</PageDef>
@- <!--	Start the document with a MP_twoColumns page layout -->
@- <Section Page="MP_twoColumns" />
 @- ...
</exampleBlock>

@------------------------------------------------------------
@- Example 2a
@------------------------------------------------------------
<exp>
Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2a" /> combined with Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2b" /> illustrates a different way
of using the <#xPageDef> element
to set up two-column and three-column master pages.
</exp>


<exampleBlock
	lineNumbers="Y"
	tabs="5.5|11|15|20"
	>
<exampleCaption
	dest="ex.pagedef_using_pagedef.2a"
	>
Using the <#xPageDef> element (2a)
</exampleCaption>
<!-- 	Page header                  --> 
<TextFrameDef textFrameDef="BT_header" type="background"<xlineRef>PageDef.exLine.BT_header.start</xlineRef>
	left="1.2in" width="6in" height="0.5in" top="1in" penColor="Black" penStyle="dotted" >
	<P paraDef="Header" >Document title</P>
	<Line top="13pt" penWidth="3pt" /> <!-- <xfI>horizontal ruling on page</xfI> -->
</TextFrameDef><xlineRef>PageDef.exLine.BT_header.end</xlineRef>
<!--	Page footer contains page number <xfI>n</xfI> of <xfI>last page number</xfI> -->
<TextFrameDef textFrameDef="BT_footer" type="background" <xlineRef>PageDef.exLine.BT_footer.start</xlineRef>
	left="1.2in" width="6in" height="0.5in" top="10in" penColor="Blue" penStyle="dotted" >
	><P textAlign="end"><currentPageNumber/> of <lastPageNumber/></P><xlineRef>PageDef.exLine.BT_header.end</xlineRef>
</TextFrameDef> <xlineRef>PageDef.exLine.BT_footer.end</xlineRef>
<!-- 	Foreground (main) text flow  --> 
<TextFrameDef textFrameDef="FT_mainflow" type="foreground" penColor="Red" <xlineRef>PageDef.exLine.FT_mainflow.start</xlineRef>
	penStyle="dotted" left="1.2in" width="6in" height="7.5in" top="2in" columns="2" /> <xlineRef>PageDef.exLine.FT_mainflow.end</xlineRef>
</exampleBlock>

<#def> pageDef.text2a	<#example_output>


<exp>
Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2a" /> defines two background
text frames, <sq>BT_header</sq> and <sq>BT_footer</sq> (lines <xlinenum
	id="PageDef.exLine.BT_header.start" />&endash;<xlinenum
	id="PageDef.exLine.BT_header.end" /> and <xlinenum
	id="PageDef.exLine.BT_footer.start" />&endash;<xlinenum
	id="PageDef.exLine.BT_footer.end" />), and a foreground text
frame <sq>FT_mainflow</sq> (lines <xlinenum
	id="PageDef.exLine.FT_mainflow.start" />&endash;<xlinenum
	id="PageDef.exLine.FT_mainflow.end" />).
</exp>

<exp>
Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2b" /> does the same thing
as Example <xparanumonly 
	id="ex.pagedef_using_pagedef.1" /> except the <#xPageDef> elements
use the text frames pre-defined in Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2a" />.
</exp>

@------------------------------------------------------------
@- Example 2b
@------------------------------------------------------------

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20"
	>
<exampleCaption
	dest="ex.pagedef_using_pagedef.2b"
	>
Using the <#xPageDef> element (2b)
</exampleCaption>
<DocDef pageWidth="8.5in"  pageHeight="11in" />
<PageDef pageDef="MP_twoColumns" >
	@- <Line top="1.2in" left="1.2in" length="6in" /> <!-- <xfI>horizontal ruling on page</xfI> -->
	<TextFrame textFrameDef="BT_header" /><TextFrame textFrameDef="BT_footer" />
	<TextFrame textFrameDef="FT_mainflow" />
</PageDef>
<PageDef pageDef="MP_threeColumns" >
	@- <Line top="1.2in" left="1.2in" length="6in" /> <!-- <xfI>horizontal ruling on page</xfI> -->
	<TextFrame textFrameDef="BT_header" /><TextFrame textFrameDef="BT_footer" />
	<TextFrame textFrameDef="FT_mainflow" columns="3"/><xlineRef>PageDef.exLine.textFrameDef.override</xlineRef>
</PageDef>
</exampleBlock>

<#def> pageDef.text2b	{<#example_output>}


<exp>
<#xTextFrame> elements that reference <#xTextFrameDef> elements may
contain overrides, as illustrated by the use of the <pn>columns</pn> property
on line <xlinenum
	id="PageDef.exLine.textFrameDef.override" /> in Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2b" />.
</exp>

<exp>
Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2c" /> illustrates combining
the <#xPageDef> and <#xTextFrameDef> format definitions in Examples <xparanumonly 
	id="ex.pagedef_using_pagedef.2a" /> and <xparanumonly 
	id="ex.pagedef_using_pagedef.2b" /> with input data to produce
an entire document.
</exp>

<#def> xmlDeclaration	{<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>}
<#def> pageDefMiramoXML	{<MiramoXML>}


@------------------------------------------------------------
@- Example 2c
@------------------------------------------------------------

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20"
	>
<exampleCaption
	dest="ex.pagedef_using_pagedef.2c"
	>
Using the <#xPageDef> element (2c)
</exampleCaption>
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<MiramoXML>
<!-- <xfI> include Example <xParaNum>ex.pagedef_using_pagedef.2a</xParaNum> here</xfI> --><hc>pdxv1</hc>

<!-- <xfI> include Example <xParaNum>ex.pagedef_using_pagedef.2b</xParaNum> here</xfI> --><hc>pdxv2</hc>

<!-- ===================================   -->
<!-- Input data start:   -->
<Section Page="MP_twoColumns" />
<P>Page 1 Column 1</P>
<P position="topOfColumn">Page 1 Column 2</P>
<Section Page="MP_threeColumns" />
<P position="topOfPage">Page 2 Column 1</P>
<P position="topOfColumn">Page 2 Column 2</P>
<P position="topOfColumn">Page 2 Column 3</P>
</MiramoXML>
</exampleBlock>

<#def> pageDef.text2c		{<#example_output>}

<#def> pageDef.completeExample	{
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<MiramoXML>
<#pageDef.text2a>
<#pageDef.text2b>
<#pageDef.text2c>
}

<#def> pageDef.completeExample	{<#pageDef.text2c>}

<#def> pageDef.completeExample	@sub({<#pageDef.completeExample>}, {pdxv1}, {<#pageDef.text2a>})
<#def> pageDef.completeExample	@sub({<#pageDef.completeExample>}, {pdxv2}, {<#pageDef.text2b>})



<#def> pageDef.completeExample		@trim(<#pageDef.completeExample>)
<@write> -n ${mmtmp}/pageDefTest.mmxml	{<#pageDef.completeExample>}

@- $env:mmtmp

@- miramo -composer mmc -Opdf yy.pdf pageDef.test

<exp>
Assuming the contents of Example <xparanumonly 
	id="ex.pagedef_using_pagedef.2c" /> is saved to a file
called <sq>pageDefTest.mmxml</sq>, then running the following
command:
</exp>

<exampleBlock>
miramo -composer mmc -Opdf pageDefTest.pdf pageDefTest.mmxml
</exampleBlock>
<exp>
produces a two-page output document. The first page is in two columns.
The second page is in three columns.
</exp>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
