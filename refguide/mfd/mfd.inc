@--------------------------------------------------------------------------------
@- mfd.inc
@- include files for Miramo Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> inMFD		1
<#def> elementType	mfd
<@include>	${mfd}/introduction
<Section allPages="MP_referenceGuide" />
<@skipStart>
<Section
	@- Needs <DocDef> doubleSided="Y" in pagelayouts.mmp
	oddPages="MP_referenceGuide"
	evenPages="MP_referenceGuideLeft"
	/>
<@skipEnd>
@- <@include>	${mfd}/tblcolumn
@- <@include>	${mfd}/mathmldef
@- <@include>	${mfd}/fnotedef
@--------------------------------------------------------------------------------
<@include>	${mfd}/changebardef
<@include>	${mfd}/colordef
<@include>	${mfd}/compositefontdef
<@include>	${mfd}/contentsdef
@----	<@include>	${mfd}/columnsdef
<@include>	${mfd}/datedef
<@include>	${mfd}/docdef
<@include>	${mfd}/equationdef
<@include>	${mfd}/fnotedef
<@include>	${mfd}/fontdef
<@include>	${mfd}/indexdef
<@include>	${mfd}/joboptionsdef
<@include>	${mfd}/mapchar
<@include>	${mfd}/metadata
<@include>	${mfd}/numbersequencedef	@---
<@include>	${mfd}/pagedef
@- <@include>	${mfd}/pagenumber
<@include>	${mfd}/pagenumberdef
<@include>	${mfd}/paradef
<@include>	${mfd}/paragroupdef
<@include>	${mfd}/pdfimportdef
<@include>	${mfd}/ruledef
<@include>	${mfd}/runningtextdef
<@include>	${mfd}/sectiondef
<@include>	${mfd}/tbldef
@-- <@include>	${mfd}/tblcolumndef
<@include>	${mfd}/tblcolumn
<@include>	${mfd}/tblcontinuationdef
@- <@skipStart>
@- <@include>	${mfd}/tblsheetdef
<@include>	${mfd}/textframedef
<@include>	${mfd}/vardef
<@include>	${mfd}/xrefdef
@- <@include>	${mfd}/frameshadowdef
<@include>	${mfd}/shadowdef
<@include>	${mfd}/svgdef
<@skipStart>
<@skipEnd>
<#def> inMFD		0
@--------------------------------------------------------------------------------
<chapterEnd/>
@--------------------------------------------------------------------------------
