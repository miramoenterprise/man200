@------------------------------------------------------------
@-- svgdef
@------------------------------------------------------------

<elementStart
	elementType="xml_mand_atts"
	elementName="xSvgDef"
	changeBar="Y"
	>

<esp>
The <#xxSvgDef> element defines a named SVG code block for use as a background
in 
<#xAFrame>,
<#xATextFrame>,
<#xCell>,
<#xFrame>,
<#xRow>,
<#xTbl>,
<#xPageDef>,
and
<#xTblDef>, using the <pn>svgDef</pn> property of these elements.
See pages <xnpage
	id="AFrame.property.svgDef" />, <xnpage
	id="ATextFrame.property.svgDef" />, <xnpage
	id="Cell.property.svgDef" />, <xnpage
	id="Frame.property.svgDef" />, <xnpage
	id="Row.property.svgDef" />, <xnpage
	id="Tbl.property.svgDef" />, <xnpage
	id="PageDef.property.svgDef" /> and <xnpage
	id="TblDef.property.svgDef" />.
<@skipStart>
<@skipEnd>
</esp>

<esp>
The <#xxSvgDef> element enables users to create custom vector drawing
artifacts, patterns, and color gradients, etc, as background fills
within the elements which support the <pn>svgDef</pn> property.
</esp>

<@skipStart>
<esp>
<#xxFrameShadow> cannot be used in a CMYK workflow.
</esp>
<@skipEnd>

<esp>
The supported SVG sub-elements are as described in
Scalable Vector Graphics (SVG) Version 1.1<FNote>See
<URL url="https://www.w3.org/TR/SVG11/" 
>Scalable Vector Graphics (SVG) 1.1 (Second Edition)</URL>,
W3C Recommendation, 16 August 2011.
</FNote>
</esp>

@--------------------------------------------------------------------------------
@- BG info, references:
@--------------------------------------------------------------------------------
<@skipStart>
SVG Tutorial
https://www.w3.org/2002/Talks/www2002-svgtut-ih/hwtut.pdf
https://riptutorial.com/ebook/svg
https://flaviocopes.com/svg/

SVG parser:
http://svgpp.org/

@- vector-effect="non-scaling-stroke" Not Supported
@- https://issues.apache.org/jira/browse/BATIK-1094
@- http://batik.2283329.n4.nabble.com/Rasterizer-issues-for-complex-SVG-td4656689.html
@- Jan Tosovsky
@- j.tosovsky@tiscali.cz
<@skipEnd>
@--------------------------------------------------------------------------------


@- &lt;svg&gt; 
<esp>
The SVG XML declaration and the SVG &lt;svg&gt; root element must not be
included within the contents of the <#xxSvgDef> element.
No namespace prefixes may be used.
</esp>

</elementStart>


@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="SVG format name"
	dest="<#elementName>.propertyGroup.name"
	/>
@------------------------------------------------------------

<property
	name="svgDef"
	value="name"
	>

<pdp>
<#Required>
<br/>
<fI>name</fI> is name of SVG code definition.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	title="Margins between container and SVG region (viewport)"
	dest="<#elementName>.propertyGroup.margins"
	>If the sum of either pair of opposite margins
is greater than the corresponding container width or height, there
is no SVG content. A warning message is generated in this case.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="allMargins"
	value="dim | percent"
	>
<pdp>
Default for all margins between container and SVG region (viewport).
</pdp>

<pdp>
Percent values relate to the container width
in the case of left and right margins.
Percent values relate to the container height
in the case of top and bottom margins.
</pdp>

<pdp>
The corresponding margin value specified by <pn>allMargins</pn> is overridden
in the case any of the <pn>leftMargin</pn>,
<pn>topMargin</pn>, <pn>rightMargin</pn> or <pn>bottomMargin</pn> is
specified.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="leftMargin"
	value="dim | percent"
	>
<pdp>
Margin between the left edge of the SVG render region (viewport) and the
left edge of the container.
</pdp>

<pdp>
Percent values relate to the container width.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="topMargin"
	value="dim | percent"
	>
<pdp>
Margin between the top edge of the SVG render region (viewport) and the
top edge of the container.
</pdp>

<pdp>
Percent values relate to the container height.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="rightMargin"
	value="dim | percent"
	>
<pdp>
Margin between the right edge of the SVG render region (viewport) and the
right edge of the container.
</pdp>

<pdp>
Percent values relate to the container width.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="bottomMargin"
	value="dim | percent"
	>
<pdp>
Margin between the bottom edge of the SVG render region (viewport) and the
bottom edge of the container.
</pdp>

<pdp>
Percent values relate to the container height.
</pdp>

<propertyDefault>0</propertyDefault>
</property>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="crop"
	value="Y | N"
	>
<pdp>
Specify whether to crop (exclude) the SVG render region
outside the container rectangle if any of the <pn>leftMargin</pn>,
<pn>topMargin</pn>, <pn>rightMargin</pn> or <pn>bottomMargin</pn> properties
has a negative value.
</pdp>

<propertyDefault>N</propertyDefault>
</property>
@------------------------------------------------------------




@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.distortion"
	>
<enp>
When rendered in PDF output the size of pixel units is 1/72 of an inch.
All dimension units are rendered with an accuracy of 1/72000 of an inch.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.distortion"
	>
<enp>
By default the SVG instructions are rendered to fit within the containing
rectangle. See Figure <xparanumonly
	id="fig.greenCrossesOutput1" /> on page <xnpage
	id="fig.greenCrossesOutput1" />.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.distortion"
	>
<enp>
As a minimum the content within the <#xxSvgDef> element
must comprise one or more well-formed XML elements, without a root element
or an XML declaration. If the content contains mal-formed XML,
the entire job will fail.
@- https://en.wikipedia.org/wiki/Well-formed_element
</enp>

<enp>
If the content contains invalid SVG markup, it is discarded and a warning
message is generated.
</enp>

</elementNote>
@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.distortion"
	>
<enp>
No user-specified attributes on the &lt;svg&gt; root element are supported.
The &lt;svg&gt; root element's viewport <pn>width</pn> and <pn>height</pn> values
are automatically derived from the width and height of its container,
unless overridden by values specified by the <#xxSvgDef> 
element's <pn>leftMargin</pn>,
<pn>topMargin</pn>,
<pn>rightMargin</pn> and <pn>bottomMargin</pn>
properties (see page <xnpage
	id="xSvgDef.property.width" />).
For example neither the <pn>preserveAspect</pn> the <pn>viewBox</pn> attributes
nor any style or presentation attributes can be applied to the  &lt;svg&gt; root element.
All such elements may be used on SVG sub-elements.
</enp>
<enp>
If any of the <#xxSvgDef> element's <pn>leftMargin</pn>,
<pn>topMargin</pn>,
<pn>rightMargin</pn> or <pn>bottomMargin</pn>
properties
is set to a negative value the SVG
viewport will extend beyond the container rectangle but will be invisible
unless <pn>crop</pn> is set to <pv>N</pv>.
</enp>

<@skipStart>
preserveAspectRatio
    How the svg fragment must be deformed if it is displayed with a different aspect ratio.
    Value type: (none| xMinYMin| xMidYMin| xMaxYMin| xMinYMid| xMidYMid|
	xMaxYMid| xMinYMax| xMidYMax| xMaxYMax) (meet|slice)? ;
Default value: xMidYMid meet; Animatable: yes
<@skipEnd>


</elementNote>

@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.colorspace"
	>
<enp>
The colors in SVG content specified within the <#xxSvgDef> element
has no access or knowledge of the color formats defined
by the <#xColorDef> element (see page <xnpage
	id="ColorDef.element.start" />).
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="xSvgDef.note.precedence"
	>
<enp>
The <pn>svgDef</pn> property has a higher precedence than 
generic <pn>fillColor</pn>, <pn>fillTint</pn> and <pn>fillOpacity</pn> properties.
</enp>

</elementNote>


@-===========================================================
<elementExampleTitle dest="svgDef.frames.start"
>Applying <#xxSvgDef> format definitions to frames
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.aSimpleGradient1" /> illustrates using simple SVG code
to create a special background fill which may be applied to multiple objects.
</exp>


@------------------------------------------------------------
@- Should be:
@------------------------------------------------------------
<exampleBlock
	show="Y"
	leftIndent="14mm"
	tabs="2|4|58"
	>
<exampleCaption
	dest="ex.aSimpleGradient1"
	><#xxSvgDef> format definition
</exampleCaption>
<#aSimpleGradientEx>
</exampleBlock>

@------------------------------------------------------------

@------------------------------------------------------------
<exp>
Example <xparanumonly
	id="ex.aSimpleGradient1Usage1" /> illustrates applying the <#xxSvgDef> format
defined in Example <xparanumonly
	id="ex.aSimpleGradient1" /> to an <#xATextFrame>.
</exp>
<exampleBlock
	show="Y"
	tabs="2|4|58"
	leftIndent="14mm"
	>
<exampleCaption 
	dest="ex.aSimpleGradient1Usage1"
	>
Including SVG background fill
</exampleCaption>
<ATextFrame width="22mm" svgDef="aSimpleGradient" <hc>position="runin" align="start" allMargins="0.8mm" leftMargin="0mm" rightMargin="0mm" bottomMargin=".1mm" fillOpacity="0" </hc> >
	<P textSize="20pt" textAlign="center" textColor="White" fontFamily="Gabriola" <hc>fontFeaturesList="ss06"</hc> >S P Q R</P>
	<P textSize="7.7pt" textAlign="center" fontFamily="Gabriola" <hc>fontFeaturesList="ss06"</hc> >C A R P E&ensp;D I E M</P>
@- <P textSize="10.8pt" spaceAbove="7pt" fontFeaturesList="ss06" >'seize the day'</P>
</ATextFrame>
</exampleBlock>


<exp>
The output from Examples <xparanumonly
	id="ex.aSimpleGradient1" /> and <xparanumonly
	id="ex.aSimpleGradient1Usage1" /> is shown in Figure <xparanumonly
	id="fig.SvgDef1output" />.

<exampleOutput
	width="28mm"
	fillTint="100%"
	shadow="Y"
	bottomMargin="-1mm"
	topMargin="1mm"
	>
@------------------------------------------------------------
<outputCaption
	dest="fig.SvgDef1output"
	>
Output from Examples <xparanumonly
	id="ex.aSimpleGradient1" /> and <xparanumonly
	id="ex.aSimpleGradient1Usage1" />.
</outputCaption>
<P>
<#example_output>
</P>
</exampleOutput>
</exp>

@-===========================================================
<elementExampleTitle dest="svgDef.tables.start"
>Applying <#xxSvgDef> format definitions in tables
</elementExampleTitle>

<exp>
Example <xparanumonly
	id="ex.greenCrosses1" /> illustrates using simple SVG code
to create a special background fill which may be applied to multiple objects.
</exp>


@------------------------------------------------------------
@- Should be:
@------------------------------------------------------------
<exampleBlock
	show="Y"
	leftIndent="14mm"
	tabs="2|4|58"
	>
<exampleCaption
	dest="ex.greenCrosses1"
	><#xxSvgDef> format definition (2)
</exampleCaption>
<xSvgDef svgDef="greenX" width="container" height="container" >
	&lt;line x1="0" y1="0" x2="100%" y2="100%" stroke-width="3" stroke="green"/>
	&lt;line x1="100%" y1="0" x2="0" y2="100%" stroke-width="3" stroke="green"/>
</xSvgDef>
<xSvgDef svgDef="bluePlus" width="container" height="container" >
	&lt;line x1="50%" y1="0" x2="50%" y2="100%" stroke-width="10" stroke="blue"/>
	&lt;line x1="0" y1="50%" x2="100%" y2="50%" stroke-width="10" stroke="blue"/>
</xSvgDef>
<xSvgDef svgDef="blueO" width="container" height="container" >
	&lt;circle cx="50%" cy="50%" r="5mm" stroke-width="2" stroke="blue" fill="none" />
	@- &lt;circle cx="9" cy="9" r="5mm" stroke-width="2" stroke="blue" fill="none" />
</xSvgDef>
<xSvgDef svgDef="mustardGradient" width="container" height="container" >
	&lt;linearGradient id="lgrag5" x1="0%" y1="0%" x2="100%" y2="100%" >
		&lt;stop offset="0%" stop-color="#ebaa10" />	&lt;stop offset="100%" stop-color="#f8e0ad" />
	&lt;/linearGradient>
	&lt;rect x="0" y="0" width="100%" height="100%" fill="url(#lgrag5)"/>
</xSvgDef>


</exampleBlock>


@------------------------------------------------------------

<#def> example_output	@gsub(<#example_output>, {&lt;}, {<})
<#example_output>

@------------------------------------------------------------
<exp>
Example <xparanumonly
	id="ex.greenCrossesUsage1" /> illustrates applying the <#xxSvgDef> format
defined in Example <xparanumonly
	id="ex.greenCrosses1" /> within table cells 
</exp>
<exampleBlock
	show="Y"
	tabs="2|4|8"
	leftIndent="14mm"
	>
<exampleCaption 
	dest="ex.greenCrossesUsage1"
	>
Including SVG background fill
</exampleCaption>
<Tbl tblDef="thinRulings" svgDef="mustardGradient" ><TblColumnDef width="4mm" />
<TblTitle<hc>textAlign="start" textSize="8.0pt" titleGap="1ot"</hc>>T 49: svgDefs in a table</TblTitle>
	<TblColumnDef width="7mm" /><TblColumnDef width="3mm" />
	<TblColumnDef width="20mm" /><TblColumnDef width="4mm" />
	<Row height="5mm" ><Cell/><Cell svgDef="bluePlus" /><Cell/><Cell svgDef="bluePlus" />
		</Row>
	<Row height="3mm" />
	<Row height="6mm" ><Cell/><Cell svgDef="greenX" /><Cell/><Cell svgDef="greenX" />
		</Row>
	<Row height="6mm" ><Cell/><Cell svgDef="blueO" /><Cell/><Cell svgDef="blueO" />
		</Row>
</Tbl>
</exampleBlock>


<exp>
The output from Examples <xparanumonly
	id="ex.greenCrosses1" /> and <xparanumonly
	id="ex.greenCrossesUsage1" /> is shown in Figure <xparanumonly
	id="fig.greenCrossesOutput1" />.

<exampleOutput
	width="44mm"
	fillTint="100%"
	shadow="Y"
	bottomMargin="3.0mm"
	topMargin="0.1mm"
	>
@------------------------------------------------------------
<outputCaption
	dest="fig.greenCrossesOutput1"
	>
Output from Examples <xparanumonly
	id="ex.greenCrosses1" /> and <xparanumonly
	id="ex.greenCrossesUsage1" />.
</outputCaption>
<P>
<#example_output>
</P>
</exampleOutput>
</exp>


@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
