@------------------------------------------------------------
@- paradef5.inc
@------------------------------------------------------------

<#def> atfIndentMode	{indentMode="relative"}
<#def> tblIndentMode	{indentMode="relative"}

<#def> afAlign		{align="start"}
<#def> afIndentMode	{indentMode="absolute"}
<#def> afIndentMode	{}			@- Default
<#def> atfIndentMode	{<#afIndentMode>}
<#def> tblIndentMode	{<#afIndentMode>}

@------------------------------------------------------------
<elementExampleTitle
	dest="ParaDef.ex.relativeIndent.start"
	@- pgfProps=" position='topOfPage' "
	>Relative indents for objects within paragraphs</elementExampleTitle>
@------------------------------------------------------------


<exp>
When <#xP>, <#xAFrame>, <#xATextFrame> or <#xTbl> elements are
nested within paragraphs (within higher level <#xP> elements),
their horizontal positioning is affected by the value of their
respective <pn>indentMode</pn> properties.
</exp>

<exp>
The <pn>indentMode</pn> setting of the foregoing elements
has no effect unless the elements are nested
within <#xP> elements. In the case of nested <#xP> elements
the indent mode is <pv>relative</pv> by default and
the <pn>labelIndent</pn>, <pn>firstLineIndent</pn> and <pn>startIndent</pn> indents,
as measured from the start edge of the container, are
automatically increased by the effective value of the <pn>startIndent</pn> of
the parent paragraph, as shown in
Equations <xparanum 
	id="equation.ParaDef.indentMode1" /> through <xparanum 
	id="equation.ParaDef.indentMode3" />.
</exp>


<exEquation3 beforeEquals="24mm" dest="equation.ParaDef.indentMode1" startIndent="10.2mm"
        >
eLabelIndent<sub>&thinsp;child</sub>	<=> startIndent<sub>&thinsp;parent</sub> &emsp; +&emsp;labelIndent<sub>&thinsp;child</sub>
</exEquation3>

<exEquation3 beforeEquals="24mm" dest="equation.ParaDef.indentMode2" startIndent="10.2mm"
        >
eFirstLineIndent<sub>&thinsp;child</sub>	<=> startIndent<sub>&thinsp;parent</sub> &emsp; +&emsp;firstLineIndent<sub>&thinsp;child</sub>
</exEquation3>

<exEquation3 beforeEquals="24mm" dest="equation.ParaDef.indentMode3" startIndent="10.2mm"
        >
eStartIndent<sub>&thinsp;child</sub>	<=> startIndent<sub>&thinsp;parent</sub> &emsp; +&emsp;startIndent<sub>&thinsp;child</sub>
</exEquation3>

<exp>
In Equations <xparanum 
	id="equation.ParaDef.indentMode1" /> through <xparanum 
	id="equation.ParaDef.indentMode3" /> eLabelIndent,
eFirstLineIndent and eStartIndent represent actual indents of the
corresponding properties.
This behavior may be overridden by setting the value
of <pn>indentMode</pn> to <pv>absolute</pv>, as illustrated
in Example <xparanumonly
	id="ex.relativeIndent2" /> (line <xlinenum id="iM.3lP2.start" />) and in the
paragraph labeled <Font fontFamily="Arial" fontWeight="Bold"
	textSize="7pt" textColor="Red" >ZZ2</Font> in Figure <xparanumonly
	id="fig.relativeIndent1" /> (page <xnpage
	id="fig.relativeIndent1" />).
</exp>


@------------------------------------------------------------
@- <@include> ${mfd}/include/paradef/indentmode/macros.mmp06apr2020
<@include> ${mfd}/include/paradef/indentmode/macros.mmp
@- vi/npp  vi "$env:mfd\include/paradef/indentmode/macros.mmp"
@------------------------------------------------------------
<#def> ipls1	{1.2pt}
@------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.relativeIndent1" /> contains five paragraph format
definitions referenced in
Example <xparanumonly
	id="ex.relativeIndent2" />.
</exp>


<#def> pdllprops	{ fontFamily="Arial" fontWeight="Bold" textSize="6pt" }

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20|25"
	tabs="3|6|9|12|15"
	>
<exampleCaption
	dest="ex.relativeIndent1"
	>
Format definitions for relative indents
</exampleCaption>
<!-- = Format definitions ================================ -->
@------------------------------------------------------------
<ParaDef paraDef="exP1"
<hc>
	fontFamily="Cambridge Light"
	fontWeight="Light"
	textSize="<#np.textSize>"
	textAlign="start"
	maximumWordSpace="100"
	minimumWordSpace="100"
	optimumWordSpace="100"
</hc>
	firstLineIndent="<#np.fLI1>" startIndent="<#np.sI1>" ><ListLabel textColor="Blue"
<hc>
	<#pdllprops>
</hc>
		labelIndent="<#np.lI1>" labelWidth="<#np.lW1>" ><xfB>P1</xfB></ListLabel></ParaDef>
@------------------------------------------------------------
<ParaDef paraDef="exP2"
<hc>
	fontFamily="Cambridge Light"
	fontWeight="Light"
	textSize="<#np.textSize>"
	textAlign="start"
</hc>
	firstLineIndent="<#np.fLI2>" startIndent="<#np.sI2>" listLabel="Y" ><ListLabel textColor="Red"
<hc>
	<#pdllprops>
</hc>
		labelIndent="<#np.lI2>" labelWidth="<#np.lW2>" ><xfB>P2</xfB></ListLabel></ParaDef>
@------------------------------------------------------------
<ParaDef paraDef="exP2.1"
<hc>
	fontFamily="Cambridge Light"
	fontWeight="Light"
	textSize="<#np.textSize>"
	textAlign="start"
</hc>
	firstLineIndent="<#np.fLI3>" startIndent="<#np.sI3>" listLabel="Y" ><ListLabel textColor="Red"
<hc>
	<#pdllprops>
</hc>
		labelIndent="<#np.lI3>" labelWidth="<#np.lW3>" ><xfB>P2.1</xfB></ListLabel></ParaDef>
@------------------------------------------------------------
<ParaDef paraDef="exP2.2"
<hc>
	fontFamily="Cambridge Light"
	fontWeight="Light"
	textSize="<#np.textSize>"
</hc>
	firstLineIndent="<#np.fLI4>" startIndent="<#np.sI4>" listLabel="Y" ><ListLabel textColor="Red"
<hc>
	<#pdllprops>
</hc>
		labelIndent="<#np.lI4>" labelWidth="<#np.lW4>" ><xfB>P2.2</xfB></ListLabel></ParaDef>
@------------------------------------------------------------
<ParaDef paraDef="exP2.3"
<hc>
	fontFamily="Cambridge Light"
	fontWeight="Light"
	textSize="<#np.textSize>"
</hc>
	firstLineIndent="<#np.fLI5>" startIndent="<#np.sI5>" listLabel="Y" ><ListLabel textColor="Red"
<hc>
	<#pdllprops>
</hc>
		labelIndent="<#np.lI5>" labelWidth="<#np.lW5>" ><xfB>P2.3</xfB></ListLabel></ParaDef>
@------------------------------------------------------------
</exampleBlock>

@- <P/>
@------------------------------------------------------------
<#def> indentMode.fdefs		<#example_output>
@------------------------------------------------------------


<exp>
@- <exp pgfProps='position="topOfPage"' >
Example <xparanumonly
	id="ex.relativeIndent2" /> contains one top level
paragraph instance and five singly or doubly-nested paragraphs.
</exp>

<#def> afw	3cm
<#def> afh	2mm
<#def> afh	1.4mm
<#def> afpw	.3pt
<#def> affc	Green
<#def> affo	45%
<#def> afsa	0mm
<#def> afsb	1mm

<#def> afmu1	{<AFrame width="<#afw>" height="<#afh>" penWidth="<#afpw>" <#afAlign> <#afIndentMode> }
<#def> afmu2	{align="start" spaceBelow="<#afsb>" spaceAbove="<#afsa>" fillColor="<#affc>" fillOpacity="<#affo>"}
<#def> afmuT	{/>}

@------------------------------------------------------------
<@xmacro> afnum {
	@----------------------------------------------------
	@- Open-circled numbers
	@----------------------------------------------------
	<#ifndef> afnum		-1
	<#def> afnum		@eval(<#afnum> + 1)
<eto><xfEPS1>\&ensp;<xfTC textColor="DarkGreen" >\&#x246<#afnum>;</xfTC></xfEPS1></eto>}{}
@------------------------------------------------------------
<@xmacro> prnr {
	@----------------------------------------------------
	@- Filled-circled numbers (in example text)
	@----------------------------------------------------
	<#ifndef> prnr		5
	<#def> prnr		@eval(<#prnr> + 1)
<eto><xfEPS1>\&ensp;<xfBlue><xfTS textSize="9pt" >\&#x277<#prnr>;</xfTS></xfBlue></xfEPS1></eto>}{}
@------------------------------------------------------------
<@xmacro> prnum {
	@----------------------------------------------------
	@- Filled-circled numbers (in output text)
	@----------------------------------------------------
	<#ifndef> prnum		5
	<#def> prnum		@eval(<#prnum> + 1)
<Font fontFamily="European Pi Std 1"
	@- textSize="8.5pt"
	textSize="9.0pt"
	fontWeight="Regular" textColor="Blue" >\&#x277<#prnum>;</Font>
}{}
@------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20|25"
	tabs="3|6|9|12|15"
	@- fontFamily="Source Sans Pro"
	processNumericEntities="Y"
	>
<exampleCaption
	dest="ex.relativeIndent2"
	>
Instances of relatively indented paragraphs
</exampleCaption>
@------------------------------------------------------------
<!-- = Input text ====================================== -->
@------------------------------------------------------------
<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
<P paraDef="exP1" <hc>endIndent="1.2mm"</hc> >firstLineIndent (<#np.fLI1>, <xfB>lI</xfB>=<#np.lI1>)<hc><endTab/><prnum/>\&emsp;\&ensp;</hc><br/>startIndent (<#np.sI1>)<prnr/><xlineRef>iM.tlP.start</xlineRef>
<hc>	<paraFrame fillColor="Blue" fillOpacity="10%" endMargin="2pt"
startMargin="2pt" topMargin="3pt"/></hc>
<@skipStart>
<@skipEnd>
@------------------------------------------------------------
	<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
	<P paraDef="exP2" <hc>endIndent="-1mm" spaceAbove="2mm" spaceBelow="2mm" </hc> >firstLineIndent (<#np.fLI2>, <xfB>lI</xfB>=<#np.lI2>)<hc><endTab/><prnum/>\&ensp;\&ensp;</hc><br/>startIndent (<#np.sI2>)<prnr/><xlineRef>iM.2lP1.start</xlineRef>
<hc>	<paraFrame fillColor="Blue" fillOpacity="10%" startMargin="2pt" topMargin="2pt" endMargin="-2mm" /></hc>
<@skipStart>
<@skipEnd>
@------------------------------------------------------------
		<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
		<P paraDef="exP2.1" <hc>spaceBelow="0" spaceAbove="2mm"</hc> >firstLineIndent (<#np.fLI3>, <xfB>lI</xfB>=<#np.lI3>)<br/>startIndent (<#np.sI3>)<xlineRef>iM.3lP1</xlineRef>
@------------------------------------------------------------
			<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
		</P>
@------------------------------------------------------------
<P paraDef="exP2.2" <hc>endIndent="2.5mm" spaceAbove="2mm"</hc> <xfB>indentMode="absolute"</xfB> >firstLineIndent (<#np.fLI4>, <xfB>lI</xfB>=<#np.lI4>)<hc><endTab/><prnum/>\&emsp;\&emsp;\&emsp;</hc><br/><prnr/><xlineRef>iM.3lP2.start</xlineRef>
		<hc>	<paraFrame fillColor="Blue" fillOpacity="10%" startMargin="2pt" topMargin="2pt" endMargin="-2mm" /></hc>
			startIndent (<#np.sI4>, indentMode="absolute")</P><xlineRef>iM.3lP2.end</xlineRef>
		<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
		<P paraDef="exP2.3" <hc>spaceAbove="0pt"</hc> >firstLineIndent (<#np.fLI5>, <xfB>lI</xfB>=<#np.lI5>)<br/>startIndent (<#np.sI5>)</P><xlineRef>iM.3lP3</xlineRef>
@------------------------------------------------------------
		<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
	</P><xlineRef>iM.2lP1.end</xlineRef>
@------------------------------------------------------------
	<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
	<P paraDef="exP2" <hc>spaceBelow="0" spaceAbove="2mm"</hc> >firstLineIndent (<#np.fLI2>, <xfB>lI</xfB>=<#np.lI2>)<br/>startIndent (<#np.sI2>)</P><xlineRef>iM.2lP2</xlineRef>
</P><xlineRef>iM.tlP.end</xlineRef>
<@skipStart>
<@skipEnd>
@------------------------------------------------------------
<#afmu1><hc><#afmu2></hc> <#afmuT> <afnum/><xlineRef>iM.AF1.start</xlineRef>
@------------------------------------------------------------
</exampleBlock>

<#def> example_output	@gsub(<#example_output>, {lI=}, {<Font
				fontFamily="Cambridge Light"
				fontWeight="Semibold">lI</Font>=})
<#def> example_output	@sub(<#example_output>, {indentMode="absolute"\)}, {<Font
				fontFamily="Cambridge Light"
				fontWeight="Semibold">indentMode="absolute"</Font>)})
@--------------------------------------------------------------------------------
<@macro> af2atf {
	@------------------------------------------------------------------------
	@- Replace AFrame with ATextFrame
	@------------------------------------------------------------------------
@- <#def> tblIndentMode            relative
@- <#def> afIndentMode             relative
@- <#def> atfIndentMode            relative
	@for(af = 1 to 9) {
		<#def> num	$$af
		<#def> num	@eval(<#num> - 1)
		<#def> example_output	@sub(<#example_output>, {[<]AFrame[^/]+[/][>]}, {<ATextFrame
			width="<#afw>"
			bottomMargin="0"
			topMargin="0"
			<#atfIndentMode>
			maximumHeight="<#afh>"
			minimumHeight="<#afh>"
			fillColor="<#affc>"
			fillOpacity="<#affo>" 
			spaceAbove="<#afsa>" 
			spaceBelow="<#afsb>" 
			penWidth="<#afpw>" 
			align="start"
			crop="N"
				><P
				textAlign="end"
				textSize="6pt"
				firstLineIndent="0"
				startIndent="0"
				@- endIndent="0mm"
				endIndent="-3mm"
				listLabel="N"
				fontFamily="European Pi Std 1"
				>\&#x246<#num>;</P></ATextFrame>})
		}
}
@------------------------------------------------------------
<@macro> af2tbl {
	@------------------------------------------------------------------------
	@- Replace AFrame with Tbl
	@------------------------------------------------------------------------
	@for(af = 1 to 9) {
		<#def> num	$$af
		<#def> num	@eval(<#num> - 1)
		<#def> example_output	@sub(<#example_output>, {[<]AFrame[^/]+[/][>]}, {<Tbl
			columns="2"
			tblDef="noRulings"
			spaceAbove="<#afsa>" 
			spaceBelow="<#afsb>" 
			<#tblIndentMode>
			align="start"
			>
			<TblColumn width="<#afw>" />
			<TblColumn width="5mm" />
			<Row height="<#afh>"
				bottomMargin="0"
				topMargin="-1pt"
				>
				<Cell 
					startRule="Very Thin"
					topRule="Very Thin"
					endRule="Very Thin"
					bottomRule="Very Thin"
					fillColor="<#affc>"
					fillOpacity="<#affo>" 
					/>
				<Cell
					@- bottomMargin="-7pt"
					bottomMargin="0"
					@- topMargin="-3pt"
					topMargin="-0.7pt"
					>
					<P
					textAlign="end"
					textSize="7pt"
					textColor="DarkGreen"
					firstLineIndent="0"
					startIndent="0"
					fontFamily="European Pi Std 1"
					>\&#x246<#num>;</P>
				</Cell>
			</Row>
			</Tbl>})
		}
}
@------------------------------------------------------------
@- <|af2atf>	@- Replace AFrame with ATextFrame zzzz
<|af2tbl>	@- Replace AFrame with Tbl zzzz
@------------------------------------------------------------

<@skipStart>
<exp>
Parent paragraph start: line <xlinenum id="iM.tlP.start" /> <br/>
Parent paragraph end: line <xlinenum id="iM.tlP.end" /> <br/>
First level nested paragraph start: line <xlinenum id="iM.2lP1.start" /> <br/>
First level nested paragraph end: line <xlinenum id="iM.2lP1.end" /> <br/>
Second first level nested paragraph end: line <xlinenum id="iM.2lP2" /> <br/>
Second level nested paragraph 1: line <xlinenum id="iM.3lP1" /> <br/>
Second level nested paragraph 2 start: line <xlinenum id="iM.3lP2.start" /> <br/>
Second level nested paragraph 2 end: line <xlinenum id="iM.3lP2.end" /> <br/>
Second level nested paragraph 3: line <xlinenum id="iM.3lP3" /> <br/>
</exp>
<exp>
xxxx
between when indentMode is set to ...
</exp>
<@skipEnd>


<exp>
In Example <xparanumonly
	id="ex.relativeIndent2" /> a top-level
parent paragraph, format exP1, starts at
line <xlinenum id="iM.tlP.start" /> and
is terminated on line <xlinenum id="iM.tlP.end" />.
The extent of paragraph exP1 and its nested content
is the shaded area <Font fontFamily="European Pi Std 1"
	@- textSize="8.5pt"
	@-textSize="9.0pt"
	fontWeight="Regular" textColor="Blue" >&#x2776;</Font> in
the output shown in Figure <xparanumonly
	id="fig.relativeIndent1" />.
The first second-level paragraph, format exP2, starts on
line <xlinenum id="iM.2lP1.start" /> and
is terminated on line <xlinenum id="iM.2lP1.end" />.
The extent of paragraph exP2 and its nested content
is the shaded area <Font fontFamily="European Pi Std 1"
	@- textSize="8.5pt"
	@-textSize="9.0pt"
	fontWeight="Regular" textColor="Blue" >&#x2777;</Font> in
the output shown in Figure <xparanumonly
	id="fig.relativeIndent1" />.

<exampleOutput
	leftMargin="<#np.leftMargin>"
	width="96mm"
	width="80mm"
	width="72mm"
	width="62mm"
	width="61.7mm"
	width="64.5mm"
	width="66.5mm"
	@- penColor="Black"
	@- penWidth="1pt"
	topMargin="8mm"
	bottomMargin="2mm"
	bottomMargin="4mm"
	shadow="Y"
	shadowDistance="5pt"
	shadowAngle="122"
>
<outputCaption
	dest="fig.relativeIndent1"
>
Output from Examples <xparanumonly
	id="ex.relativeIndent1" /> and <xparanumonly
	id="ex.relativeIndent2" />
</outputCaption>
@- <vbarAnnots count="20" lineLength="33.5" />	@- Vert line len in mm
@- <vbarAnnots count="20" lineLength="40" />	@- Vert line len in mm
@- <vbarAnnots count="6" lineLength="48" />	@- Vert line len in mm
<vbarAnnots count="6" lineLength="72.5" />	@- Vert line len in mm
<#indentMode.fdefs>
<#example_output>
</exampleOutput>
</exp>

<@skipStart>
<exp>
The <#xAFrame> elements included on lines <xlinenum
	id="iM.AF1.start" />, <xlinenum
	id="iM.AF2.start" /> and <xlinenum
	id="iM.AF3.start" /> in Example <xparanumonly
	id="ex.relativeIndent2" /> all
have <pn>align</pn> properties set to <pv>start</pv> (not shown).

</exp>
<@skipEnd>

<exp>
A second second-level paragraph, format exP2 again, is shown on
line <xlinenum id="iM.2lP2" />.
</exp>
<exp>
Three third level paragraphs, formats exP2.1, exP2.2 and exP2.3, are included
within the first second-level paragraph on lines <xlinenum
	id="iM.3lP1" /> through <xlinenum id="iM.3lP3" />.
The second of the third-level paragraphs, format exP2.2,
labeled <Font fontFamily="Arial" fontWeight="Bold"
	textSize="7pt" textColor="Red" >P2.2</Font> in Figure <xparanumonly
	id="fig.relativeIndent1" />, has a different output indent
owing to the <pn>indentMode</pn> property being set to <pv>absolute</pv> for
this paragraph instance (see line <xlinenum id="iM.3lP2.start" /> in
Example <xparanumonly
	id="ex.relativeIndent2" />).
</exp>

@- <P/>
@- <#example_output>

@- <P>labelIndent is 0.5cm</P>



