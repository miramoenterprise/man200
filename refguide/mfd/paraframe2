@--------------------------------------------------------------------------------
@- paraFrame
@--------------------------------------------------------------------------------
<elementStart elementType="xml_opt_atts"  elementName="paraFrame"
        subElements="Y"
        >

@- <P paraDef="P_refBody" >
<esp>
XUse one or more <#xTblColumnDef> elements immediately after a <#xTblDef>
@- or a <#xTblFrame>
element to set fixed or proportional table column widths.
<#xTblColumnFormat> sub-elements may be included within
<#xTblColumnDef> elements to specify the default paragraph
formats for the table column header, body and footer rows.
</esp>
@- </P>

</elementStart>



@--------------------------------------------------------------------------------
<@skipStart>
Auriga
Cassiopeia
Cygnus
Eridanus
Hydra
Sagittarius
Scorpius
Ursa Major
Virgo
<@skipEnd>
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<@xmacro> annoNumberMap {
	<#def> num[1]		&#x2460;
	<#def> num[2]		&#x2461;
	<#def> num[3]		&#x2462;
	<#def> num[4]		&#x2463;
	<#def> num[5]		&#x2464;
	<#def> num[6]		&#x2465;
	<#def> num[7]		&#x2466;
	<#def> num[8]		&#x2467;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[9]		&#x2468;
	<#def> num[20]		&#x2473;
}{}
@--------------------------------------------------------------------------------
<annoNumberMap/>
@--------------------------------------------------------------------------------
<@xmacro> gAnnoNumber {
	<#def> gan.number	1
	<#def> gan.textSize	11pt
	<#def> gan.textColor	Black
	<#def> gan.top		@eval(<#g.h1> + 8)
	<#def> gan.left		@eval(<#g.c1> + <#g.c2> + <#g.c3> + <#g.c4> - 16)
	@------------------------------------------------------------------------
	@xgetvals(gan.)
	@------------------------------------------------------------------------
	<#def> g.annoNumber {
		<TextLine
			@- left="<#gan.left>pt"
			@- top="<#gan.top>pt"
			left="<#gan.left>"
			top="<#gan.top>"
			textSize="<#gan.textSize>"
			textColor="<#gan.textColor>"
			fontFamily="European Pi Std 1"
			@- >&#x2463;</TextLine>
			>$num[<#gan.number>]</TextLine>
			}
}{}

@--------------------------------------------------------------------------------
<@xmacro> - getRulingWidths {
	<#def> pgf.startRuleWidth		0
	<#def> pgf.topRuleWidth			0
	<#def> pgf.endRuleWidth			0
	<#def> pgf.bottomRuleWidth		0
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.startRule>X}, {XGoldX})) {<#def> pgf.startRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.startRule>X}, {XDenseBlueX})) {<#def> pgf.startRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.startRule>X}, {XKhakiX})) {<#def> pgf.startRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.startRule>X}, {XRubyX})) {<#def> pgf.startRuleWidth	<#rubyWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.topRule>X}, {XGoldX})) {<#def> pgf.topRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.topRule>X}, {XDenseBlueX})) {<#def> pgf.topRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.topRule>X}, {XKhakiX})) {<#def> pgf.topRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.topRule>X}, {XRubyX})) {<#def> pgf.topRuleWidth	<#rubyWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.endRule>X}, {XGoldX})) {<#def> pgf.endRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.endRule>X}, {XDenseBlueX})) {<#def> pgf.endRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.endRule>X}, {XKhakiX})) {<#def> pgf.endRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.endRule>X}, {XRubyX})) {<#def> pgf.endRuleWidth	<#rubyWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.bottomRule>X}, {XGoldX})) {<#def> pgf.bottomRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XDenseBlueX})) {<#def> pgf.bottomRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XKhakiX})) {<#def> pgf.bottomRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XRubyX})) {<#def> pgf.bottomRuleWidth	<#rubyWidth>}
<@write> error {
-------------------------------------------------
	startRule:		<#pgf.startRuleWidth>
	startMargin:		<#pgf.startMargin>
	endMargin:		<#pgf.endMargin>
	endRule:		<#pgf.endRuleWidth>
	topRule:		<#pgf.topRuleWidth>
	topMargin:		<#pgf.topMargin>
	bottomMargin:		<#pgf.bottomMargin>
	bottomRule:		<#pgf.bottomRuleWidth>
	startIndent:		<#pgf.li>
	firstLineIndent:	<#pgf.fi>
	endIndent:		<#pgf.ri>
}
}{}
@--------------------------------------------------------------------------------


@--------------------------------------------------------------------------------
<#def>	g.dashedBorder		{}
@--------------------------------------------------------------------------------
<#def> g.h1			7	@--- Top row height
@--------------------------------------------------------------------------------
<#def> g.c1			7	@--- Width of column 1
<#def> g.c2			78	@--- Width of column 2
<#def> g.c3			22	@--- Width of column 3
<#def> g.c4			78	@--- Width of column 4
<#def> g.l			4	@--- Leading
@--------------------------------------------------------------------------------
<#def> g.ts			12pt	@--- default text size
@--------------------------------------------------------------------------------
<#def> goldWidth		6pt
<#def> denseBlueWidth		6pt
<#def> khakiWidth		4pt
<#def> rubyWidth		4pt
@--------------------------------------------------------------------------------
<#def> pgf.count		0	@--- Counter for number of pgfs / paraframes
<#def> g.borderLeftBase		@eval(<#g.c1> + <#g.c2> + <#g.c3>)
<#def> g.borderTopBase		@eval(<#g.h1>)
@--------------------------------------------------------------------------------
<#def> g.showTableRules		Y
<#def> g.tblRuling		{Very Thin}
<#def> g.tblRuling		{None}
@--------------------------------------------------------------------------------
<#def> g.leadingColor		{Black}
<#def> g.leadingTint		{35%}
<#def> g.leadingOpacity		{35%}
<#def> g.leadingOpacity		{100%}
@--------------------------------------------------------------------------------
<#def> g.spaceColor		{Black}
<#def> g.spaceTint		{9%}
<#def> g.spaceOpacity		{35%}
<#def> g.spaceOpacity		{100}
@--------------------------------------------------------------------------------
<@xmacro> - pgf1 {
	<#def> pgf.p			<#g.ts>	@--- text size
	<#def> pgf.l			0	@--- text leading
	<#def> pgf.ff			Arial	@--- font family
	<#def> pgf.spw			569	@--- space char width
	<#def> pgf.upm			2048	@--- units per em
	<#def> pgf.fi			0	@--- first line indent
	<#def> pgf.li			0	@--- start indent
	<#def> pgf.ri			0	@--- end indent
	<#def> pgf.sa			10	@--- space above
	<#def> pgf.sb			0	@--- space below
	<#def> pgf.pf			Y	@--- Include paraFrame
	<#def> pgf.sti			N	@--- Show text indicators (mmts4 font)
	@------------------------------------------------------------------------
	<#def> pgf.fillColor		Black	@--- paraFrame fill
	<#def> pgf.fillTint		20	@--- paraFrame tint
	<#def> pgf.fillOpacity		100	@--- paraFrame opacity
	<#def> pgf.startMargin		0pt	@--- paraFrame margin
	<#def> pgf.topMargin		0pt	@--- paraFrame margin
	<#def> pgf.endMargin		0pt	@--- paraFrame margin
	<#def> pgf.bottomMargin		0pt	@--- paraFrame margin
	@------------------------------------------------------------------------
	<#def> pgf.startRule		{}	@--- minimumWordSpace
	<#def> pgf.topRule		{}	@--- minimumWordSpace
	<#def> pgf.endRule		{}	@--- minimumWordSpace
	<#def> pgf.bottomRule		{}	@--- minimumWordSpace
	@------------------------------------------------------------------------
	<#def> pgf.minWSP		{}	@--- minimumWordSpace
	<#def> pgf.optWSP		{}	@--- optimumWordSpace
	<#def> pgf.maxWSP		{}	@--- maximumWordSpace
	<#def> pgf.drawParaFrameBorder	N	@--- dashed border on <paraFrame>
	@------------------------------------------------------------------------
	@xgetvals(pgf.)
	@------------------------------------------------------------------------
}{
	<#def> i			{<Font fontFamily="mmts4" textColor="Red" >#xE300;</Font>}
	<#def> pgf.text			$pgf1
	@if(<#pgf.sti> = Y) {
		<#def> pgf.textA		@gsub({<#pgf.text>}, { }, {<#i> <#i>})
		<#def> pgf.textA		@gsub({<#pgf.textA>}, {#x}, {\&#x})
		}
	@else {
		<#def> pgf.textA		{<#pgf.text>}
		}
	@------------------------------------------------------------------------
<P paraDef="Body" 
	fontFamily="<#pgf.ff>"
	textSize="<#pgf.p>"
	firstLineIndent="<#pgf.fi>"
	startIndent="<#pgf.li>"
	endIndent="<#pgf.ri>"
	spaceAbove="<#pgf.sa>"
	spaceBelow="<#pgf.sb>"
	leading="<#pgf.l>"
	@------------------------------------------------------------------------
	@if(@len(<#pgf.minWSP>)) {
		minimumWordSpace="<#pgf.minWSP>"
		}
	@if(@len(<#pgf.optWSP>)) {
		optimumWordSpace="<#pgf.optWSP>"
		}
	@if(@len(<#pgf.maxWSP>)) {
		maximumWordSpace="<#pgf.maxWSP>"
		}
	@------------------------------------------------------------------------
	paraFrame="<#pgf.pf>"
	><paraFrame
		startMargin="<#pgf.startMargin>"
		topMargin="<#pgf.topMargin>"
		endMargin="<#pgf.endMargin>"
		bottomMargin="<#pgf.bottomMargin>"
		fillColor="<#pgf.fillColor>"
		fillTint="<#pgf.fillTint>"
		fillOpacity="<#pgf.fillOpacity>"
		@if(@len(<#pgf.startRule>)) {
			startRule="<#pgf.startRule>"
			}
		@if(@len(<#pgf.topRule>)) {
			topRule="<#pgf.topRule>"
			}
		@if(@len(<#pgf.endRule>)) {
			endRule="<#pgf.endRule>"
			}
		@if(@len(<#pgf.bottomRule>)) {
			bottomRule="<#pgf.bottomRule>"
			}
		/><#pgf.textA></P>
	@------------------------------------------------------------------------
	@if(<#pgf.drawParaFrameBorder> = Y ) {
		<getRulingWidths/>
		@- <#def> g.borderTopBase		@eval(<#g.h1>)
		@- <#def> g.borderLeftBase		@eval(<#g.c1> + <#g.c2> + <#g.c3>)
		@----------------------------------------------------------------
		<#def> pgf.startAnchor		<#pgf.li>
		@if(<#pgf.fi> < <#pgf.li> ) {
			<#def> pgf.startAnchor		<#pgf.fi>
			}
		@----------------------------------------------------------------
		<#def> g.borderLeft		@eval(<#g.borderLeftBase>
							+ <#pgf.startAnchor>
							- <#pgf.startMargin>
							- <#pgf.startRuleWidth>
							)
		<#def> g.borderLeft2		@eval(<#g.borderLeftBase>
							+ <#g.c4>
							- <#pgf.ri>
							+ <#pgf.endMargin>
							+ <#pgf.endRuleWidth>
							)
		@----------------------------------------------------------------
		@if(<#pgf.count> = 0) {
			<#def> g.borderTop		<#g.h1>pt
		<@write> error	{StartTop:	<#g.borderTop>}

			}
		@else {
			<#def> g.borderTop		@eval(<#g.borderTop2>
								@- + (<#pgf.p> + <#pgf.l> + <#pgf.sa>)
								+ (<#pgf.l> + <#pgf.sa>)
								)
			<@skipStart>
			<#def> g.borderTop		@eval(<#g.h1>
							+ ((<#pgf.p> + <#pgf.l> + <#pgf.sa>
							) * <#pgf.count>)
							@- - <#pgf.topMargin>
							@- + <#pgf.bottomMargin>
							- <#pgf.topRuleWidth>
							@- + <#pgf.bottomRuleWidth>
							)
			<@skipEnd>
			}
		<#def> g.borderTop2		@eval(<#g.borderTop>
							+ <#pgf.p>
							+ <#pgf.topMargin>
							+ <#pgf.bottomMargin>
							+ <#pgf.topRuleWidth>
							+ <#pgf.bottomRuleWidth>
							)
		<#def> g.lineTop		@eval(<#g.h1> + <#g.ts>)
		<#def> pgf.numberCount		@eval(<#pgf.count> + 4)
		<#def> pgf.numberLeft		@eval(<#g.borderLeft2> - 16 - <#pgf.endMargin> - <#pgf.endRuleWidth> )
		<#def> pgf.numberTop		@eval(<#g.borderTop> + 12 + <#pgf.topRuleWidth>)
		<gAnnoNumber left="<#pgf.numberLeft>" top="<#pgf.numberTop>"  number="<#pgf.numberCount>" />
		@- <@write> error {Number:	<#g.annoNumber>}
		@----------------------------------------------------------------
		<#def> g.dashedBorder		{
			<#g.dashedBorder>
			<#g.annoNumber>
			<PolyLine
				penWidth="0.4pt"
				penColor="Black"
				penOpacity="100"
				penTint="100"
				penStyle="dashed"
				arrowHead="none"
				lineCap="butt"
				>
				<Point
					left="<#g.borderLeft>"
					top="<#g.borderTop>"
					/>
				<Point
					left="<#g.borderLeft2>"
					top="<#g.borderTop>"
					/>
				<Point
					left="<#g.borderLeft2>"
					top="<#g.borderTop2>"
					/>
				<Point
					left="<#g.borderLeft>"
					top="<#g.borderTop2>"
					/>
				@----
				<Point
					left="<#g.borderLeft>"
					top="<#g.borderTop>"
					/>
			</PolyLine>
			}
		@----------------------------------------------------------------
		<#def> pgf.count		@eval(<#pgf.count> + 1)
		@----------------------------------------------------------------
		}
}
@--------------------------------------------------------------------------------

<elementExamples>
@--------------------------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.paraFrameRuleDefs" > contains a set of
ruling format definitions used in illustrating the operation
of the <#xparaFrame> sub-element.
</exp>

<exampleBlock
        lineNumbers="Y"
        tabs="5|10|45"
	show="Y"
        >
<exampleCaption
	dest="ex.paraFrameRuleDefs"
	>The effect of <#xTblColumnDef> in a <#xTbl> instance</exampleCaption>
<hc>
<ColorDef colorDef="xGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xKhaki"  r="45" g="42" b="14" />
<ColorDef colorDef="xDenseBlue"  r="0" g="0" b="78" />
<ColorDef colorDef="xRuby" c="0" m="91" y="100" k="51"  />
</hc>
<RuleDef ruleDef="Gold" penWidth="<#goldWidth>" penColor="xGold" />
<RuleDef ruleDef="DenseBlue" penWidth="<#denseBlueWidth>" penColor="xDenseBlue" />
<RuleDef ruleDef="Khaki" penWidth="<#khakiWidth>" penColor="xKhaki" />
<RuleDef ruleDef="Ruby" penWidth="<#rubyWidth>" penColor="xRuby" />
</exampleBlock>

<#example_output>
<exampleBlock
        lineNumbers="Y"
        tabs="5|10|45"
	show="N"
        >
@------------------------------------------------------------
<RuleDef ruleDef="H" penWidth="4mm" penColor="Black" />
<RuleDef ruleDef="H" penWidth="0.2pt" penColor="Black" />
<@skipStart>
<RuleDef ruleDef="2mm" penWidth="2mm" penColor="Black" />
<RuleDef ruleDef="1mm" penWidth="2mm" penColor="Maroon" />
<RuleDef ruleDef="4mm" penWidth="4mm" penColor="Black" />
<RuleDef ruleDef="Gray6pt" penWidth="6pt" penColor="DarkGray2" />
<RuleDef ruleDef="Gray6pt" penWidth="6pt" penColor="DenseBlue" />
<RuleDef ruleDef="DarkBlue" penWidth="4pt" penColor="DarkBlue" />
<RuleDef ruleDef="DarkOlive" penWidth="4pt" penColor="VeryDarkOlive" />
<@skipEnd>
@------------------------------------------------------------
<P leading="0" textSize="0" spaceBelow="0" spaceAbove="0" >
<Tbl tblDef="none" allMargins="0" spaceAbove="0" >
	<TblColumn width="<#g.c1>pt" />
	<TblColumn width="<#g.c2>pt" />
	<TblColumn width="<#g.c3>pt" />
	<TblColumn width="<#g.c4>pt" />
@------------------------------------------------------------
<Row height="<#g.h1>pt"
	startRule="<#g.tblRuling>" endRule="<#g.tblRuling>" bottomRule="<#g.tblRuling>"
	/>
<@skipStart>
@-===========================================================
@- LATERAL <paraFrames> margins and rulings
@-===========================================================
<Row bottomRule="<#g.tblRuling>"
		topRule="<#g.tblRuling>" startRule="<#g.tblRuling>" endRule="<#g.tblRuling>"
	>
<Cell/>
<Cell
		startRule="<#g.tblRuling>"
	>
<pgf1
	@- topMargin="6pt"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Auriga</pgf1>
<pgf1
	sa="<#pgf.sa>"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Cassiopeia</pgf1>
<pgf1
	sa="<#pgf.sa>"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Cygnus</pgf1>
<pgf1
	sa="<#pgf.sa>"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	li="12pt"
	fi="12pt"
	ri="12pt"
	>Hydra</pgf1>
</Cell>
<Cell/>
@------------------------------------------------------------
<Cell
	startRule="<#g.tblRuling>" @- endMargin="1pt" 
	>
<pgf1
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	drawParaFrameBorder="Y"
	pf="Y"
	>Auriga</pgf1>
<pgf1
	startMargin="6pt"
	endMargin="8pt"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	drawParaFrameBorder="Y"
	pf="Y"
	>Cassiopeia</pgf1>
<pgf1
	startMargin="6pt"
	endMargin="3pt"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	drawParaFrameBorder="Y"
	pf="Y"
	startRule="Gold"
	endRule="Khaki"
	>Cygnus</pgf1>
<pgf1
	startMargin="6pt"
	endMargin="8pt"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	drawParaFrameBorder="Y"
	pf="Y"
	startRule="Gold"
	endRule="Khaki"
	@- startRule="H"
	li="12pt"
	fi="12pt"
	ri="12pt"
	>Hydra</pgf1>
</Cell>
</Row>
@------------------------------------------------------------
<Row height="5mm"
	topMargin="5pt"
	startRule="<#g.tblRuling>" endRule="<#g.tblRuling>" bottomRule="<#g.tblRuling>"
	>
<Cell/>
<Cell
	><P fontFamily="Cambridge Light" fontWeight="Light" textSize="8.5pt" textAlign="center"
	>Container A</P>
</Cell>
<Cell/>
<Cell><P fontFamily="Cambridge Light" fontWeight="Light" textSize="8.5pt" textAlign="center"
	>Container B</P>
</Cell>
</Row>
@------------------------------------------------------------
<Row 	@- height="5mm"
	topMargin="5pt"
	startRule="<#g.tblRuling>" endRule="<#g.tblRuling>" bottomRule="<#g.tblRuling>"
	>
<Cell/>
<Cell columnSpan="remainder"
	>
	@----------------------------------------------------
	<P textSize="0" spaceBelow="0" >
	<Tbl tblDef="none" spaceAbove="0"  allMargins="0pt"
		startIndent="50pt"
		>
	<TblColumn width="30pt" />
	<TblColumn width="80pt" />
	<Row>
	<Cell fillColor="<#g.leadingColor>"
		fillTint="<#g.leadingTint>"
		@- fillOpacity="<#g.leadingOpacity>"
		startMargin="5pt"
		/>
	<Cell
		startMargin="5pt"
		>
	<P fontFamily="Cambridge Light" fontWeight="Light" textSize="6.5pt" textAlign="start"
		>Inter-line leading</P>
	</Cell>
	</Row>
	<Row height="1.5mm" />
	<Row>
	<Cell fillColor="<#g.spaceColor>"
		fillTint="<#g.spaceTint>"
		@- fillOpacity="<#g.spaceOpacity>"
		/>
	<Cell
		startMargin="5pt"
		>
	<P fontFamily="Cambridge Light" fontWeight="Light" textSize="6.5pt" textAlign="start"
		@- >max(spaceAbove, spaceBelow)</P>
		>spaceAbove</P>
	</Cell>
	</Row>
	</Tbl>
	</P>
</Cell>
</Row>
<@skipEnd>
@------------------------------------------------------------
@-===========================================================
@- TOP / BOTTOM  <paraFrames> margins and rulings
@-===========================================================
<Row
	bottomRule="<#g.tblRuling>" topRule="<#g.tblRuling>" startRule="<#g.tblRuling>" endRule="<#g.tblRuling>"
	>
<Cell/>
<Cell
	startRule="<#g.tblRuling>"
	>
<pgf1
	@- topMargin="6pt"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Auriga</pgf1>
<pgf1
	sa="<#pgf.sa>"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Cassiopeia</pgf1>
<pgf1
	sa="<#pgf.sa>"
	p="<#g.ts>"
	pf="N"
	l="<#g.l>pt"
	>Cygnus</pgf1>
</Cell>
<Cell/>
@------------------------------------------------------------
<Cell
	startRule="<#g.tblRuling>" @- endMargin="1pt" 
	>
<pgf1
	topMargin="6pt"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	pf="Y"
	drawParaFrameBorder="Y"
	>Auriga</pgf1>
<pgf1
	startMargin="4pt"
	endMargin="8pt"
	topMargin="6pt"
	bottomMargin="8pt"
	p="<#g.ts>"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	pf="Y"
	drawParaFrameBorder="Y"
	>Cassiopeia</pgf1>
<pgf1
	startMargin="6pt"
	endMargin="3pt"
	topMargin="6pt"
	topRule="DenseBlue"
	bottomRule="Ruby"
	bottomMargin="8pt"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	pf="Y"
	startRule="Gold"
	endRule="Khaki"
	drawParaFrameBorder="Y"
	>Cygnus</pgf1>

<pgf1
	fi="12pt"	@--- firstLineIndent (This the effective one!)
	li="12pt"	@--- startIndent
	startMargin="6pt"
	endMargin="8pt"
	topMargin="6pt"
	topRule="DenseBlue"
	bottomRule="Ruby"
	bottomMargin="8pt"
	sa="<#pgf.sa>"
	p="<#g.ts>"
	l="<#g.l>pt"
	fillColor="Blue"
	pf="Y"
	startRule="Gold"
	endRule="Khaki"
	drawParaFrameBorder="Y"
	>Hydra</pgf1>

</Cell>
</Row>

</Tbl>
</P>
</exampleBlock>

<@skipStart>
<TextLine left="1mm" top="17.5mm" textSize="7pt" fontFamily="European Pi Std 1">&#x2460;</TextLine>
<TextLine left="0.8mm" top="05mm" textSize="7pt" fontFamily="European Pi Std 1">&#x2461;</TextLine>
<TextLine left="39mm" top="3.5mm" textSize="7pt" fontFamily="European Pi Std 1">&#x2462;</TextLine>
<TextLine left="39.6mm" top="10.7mm" textSize="7pt" fontFamily="European Pi Std 1">&#x2463;</TextLine>
<@skipEnd>
@--------------------------------------------------------------------------------
<@xmacro> horizontalLine {
	<#def> al.top		4
	<#def> al.width		4
	<#def> al.color		Black
	<#def> al.tint		100
	<#def> al.opacity	100
	<#def> al.left2		@eval(<#g.c1> + <#g.c2> + <#g.c3> + <#g.c4>)
	@------------------------------------------------------------------------
	@xgetvals(al.)
	@------------------------------------------------------------------------
	<#def> al.top		@eval(<#al.top> + <#al.width> / 2)
	<PolyLine
		penWidth="<#al.width>"
		penColor="<#al.color>"
		penOpacity="<#al.opacity>"
		penTint="<#al.tint>"
		penStyle="solid"
		arrowHead="none"
		lineCap="butt"
		>
		<Point
			left="<#g.c1>pt"
			top="<#al.top>"
			/>
		<Point
			@- left="200pt"
			left="<#al.left2>pt"
			top="<#al.top>"
			/>
	</PolyLine>
}{}
@--------------------------------------------------------------------------------
<@xmacro> textContainer {
	<#def> tc.top		<#g.h1>
	<#def> tc.width		<#g.c2>
	<#def> tc.left		<#g.c1>
	<#def> tc.color		Black
	<#def> tc.tint		100
	<#def> tc.opacity	100
	<#def> tc.top2		@eval(<#g.lineTop> + <#pgf.sa> + <#g.ts>)
	@------------------------------------------------------------------------
	@xgetvals(tc.)
	@------------------------------------------------------------------------
	<#def> tc.top		@sub({<#tc.top>}, {pt}, {})
	<mmDraw>
	<PolyLine
		@- penWidth="<#tc.width>"
		penWidth="1pt"
		@- penColor="<#tc.color>"
		penColor="Red"
		@- penOpacity="<#tc.opacity>"
		@- penTint="<#tc.tint>"
		penStyle="dotted"
		arrowHead="none"
		@- lineCap="butt"
		>
		<Point left="<#tc.left>pt"	top="<#tc.top>pt" />
		<#def> tc.l2			@eval(<#tc.left> + <#tc.width>)
		<Point left="<#tc.l2>pt"	top="<#tc.top>pt" />
		@- <#def> tc.t2			@eval(<#g.lineTop> + <#pgf.sa> + <#g.ts>)
		<#def> tc.t2			<#tc.top2>
		<Point left="<#tc.l2>pt"	top="<#tc.t2>" />
		<Point left="<#tc.left>pt"	top="<#tc.t2>" />
		<Point left="<#tc.left>pt"	top="<#tc.top>pt" />
	</PolyLine>
		<#g.dashedBorder>
	</mmDraw>
}{}
@--------------------------------------------------------------------------------
<@xmacro> gHorizontalRule {
	<ALine
		left="<#g.c1>pt"
		top="<#g.lineTop>"
		lineAngle="90"
		lineLength="200pt"
		@- penWidth="1.0pt"
		penWidth="0.1pt"
		penColor="Black"
		penOpacity="100"
		penStyle="solid"
		arrowHead="none"
		/>
}{}
@--------------------------------------------------------------------------------
<@xmacro> gAnnotations {
	<mmDraw>
		<#def> g.lineTop	@eval(<#g.h1> + <#g.ts>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#g.l>pt"
			color="<#g.leadingColor>" tint="<#g.leadingTint>" opacity="<#g.leadingOpacity>" />
		<#def> g.lineTop	@eval(<#g.lineTop> + <#g.l>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#pgf.sa>pt"
			color="<#g.spaceColor>" tint="<#g.spaceTint>" opacity="<#g.spaceOpacity>" />

		<#def> g.lineTop	@eval(<#g.lineTop> + <#pgf.sa> + <#g.ts>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#g.l>pt"
			color="<#g.leadingColor>" tint="<#g.leadingTint>" opacity="<#g.leadingOpacity>" />
		<#def> g.lineTop	@eval(<#g.lineTop> + <#g.l>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#pgf.sa>pt"
			color="<#g.spaceColor>" tint="<#g.spaceTint>" opacity="<#g.spaceOpacity>" />

		<#def> g.lineTop	@eval(<#g.lineTop> + <#pgf.sa> + <#g.ts>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#g.l>pt"
			color="<#g.leadingColor>" tint="<#g.leadingTint>" opacity="<#g.leadingOpacity>" />
		<#def> g.lineTop	@eval(<#g.lineTop> + <#g.l>)
		<@write> error	{lineTop:	<#g.lineTop>}
	<horizontalLine top="<#g.lineTop>" width="<#pgf.sa>pt"
			color="<#g.spaceColor>" tint="<#g.spaceTint>" opacity="<#g.spaceOpacity>" />
		<#g.dashedBorder>
	</mmDraw>
}{}
@--------------------------------------------------------------------------------
@- <gAnnoNumber number="1" />
@--------------------------------------------------------------------------------
<#def> textContainerLeft2	@eval(<#g.c1> + <#g.c2> + <#g.c3>)
@--------------------------------------------------------------------------------

<@write> -n /tmp/vvv {
		<#g.dashedBorder>
}

<#def> g.textContainerBottom	@sub({<#g.borderTop2>}, {pt}, {})
<#def> g.textContainerBottom	<#g.borderTop2>
<exp>
<exampleOutput
	width="71mm"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0.5mm"
	leftMargin="0mm"
	@- leftMargin="4mm"
	bottomMargin="2.0mm"
	topMargin="0mm"
	belowGap="3.5mm"
	>
<outputCaption
	dest="fig.TblColumnDef.1A"
	>
Output from Examples <xparanumonly
	id="ex.TblColumnDef.1A" /> and <xparanumonly
	id="ex.TblColumnDef.2" />
</outputCaption>
<gAnnotations/>
<textContainer left="<#g.c1>" />
@- <textContainer left="@eval(<#g.c1> + <#g.c2> + <#g.c3>)" />
@- <textContainer left="@eval(<#g.c1> + <#g.c2> + <#g.c3>)" />
@- <textContainer left="90" />
<#example_output>
@- <textContainer left="<#textContainerLeft2>" top="<#g.borderTop>" />
<textContainer left="<#textContainerLeft2>" top2="<#g.textContainerBottom>" />
@- <gAnnoNumber number="1" />
</exampleOutput>

</exp>

<exp/>

@- <@include> ${mfd}/paraframe4
</elementExamples>
@------------------------------------------------------------

<@skipStart>
<@skipEnd>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------

