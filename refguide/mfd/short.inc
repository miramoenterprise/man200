@--------------------------------------------------------------------------------
@- short.inc
@- include files for Miramo Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> elementType	mfd
<@include>      ${mfd}/intro
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${mfd}/colordef
<@include>	${mfd}/datedef
<@skipStart>
<@include>	${mfd}/docdef
<@include>	${mfd}/fontdef
<@include>	${mfd}/mapchar
<@include>	${mfd}/pagedef
<@include>	${mfd}/paradef
<@include>	${mfd}/ruledef
<@include>	${mfd}/runningtext
<@include>	${mfd}/tbldef
<@include>	${mfd}/tblcontinuationdef
<@include>	${mfd}/tblsheetdef
<@include>	${mfd}/textframedef
<@include>	${mfd}/xrefdef
<@skipEnd>

