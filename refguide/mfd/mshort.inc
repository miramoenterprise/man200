@--------------------------------------------------------------------------------
@- mshort.inc
@- include files for Miramo Format Definitions
@--------------------------------------------------------------------------------
<chapterStart
	dest="formatDefinitionElements"
	pgfSuffix="ExA"
	>Format definitions
</chapterStart>
<#def> inMFD		1
<#def> elementType	mfd
@- <@include>	${mfd}/introduction
@- <Section allPages="MP_referenceGuide" />
<@skipStart>
<Section
	oddPages="MP_referenceGuide"
	evenPages="MP_referenceGuideLeft"
		/>
<@skipEnd>
<Section allPages="MP_referenceGuide" />
<#def> elementStart.firstExample	1
@---------------------------------------------------------------
<@include>	${mfd}/pdfpermissions
<@skipStart>
<@include>	${mfd}/annonote
<@include>	${mfd}/equationdef
<@include>	${mfd}/mapchar
<@include>	${mfd}/xrefdef
<@include>	${mfd}/numbersequencedef
<@include>	${mfd}/columnsdef
@---------------------------------------------------------------
<@include>	${mfd}/changebardef
<@include>	${mfd}/colordef
<@include>	${mfd}/compositefontdef
<@include>	${mfd}/contentsdef
<@include>	${mfd}/datedef
<@include>	${mfd}/docdef
<@include>	${mfd}/equationdef
<@include>	${mfd}/fnotedef
<@include>	${mfd}/fontdef
<@include>	${mfd}/indexdef
<@include>	${mfd}/joboptionsdef
<@include>	${mfd}/mapchar
<@include>	${mfd}/pagedef
<@include>	${mfd}/paradef
<@include>	${mfd}/paragroupdef
<@include>	${mfd}/ruledef
<@include>	${mfd}/runningtextdef
<@include>	${mfd}/sectiondef
<@include>	${mfd}/tbldef
<@include>	${mfd}/tblcolumndef
<@include>	${mfd}/tblcontinuationdef
<@include>	${mfd}/textframedef
<@include>	${mfd}/vardef
<@include>	${mfd}/xrefdef
<@skipEnd>
@--------------------------------------------------------------------------------
<#def> inMFD		0
@--------------------------------------------------------------------------------
<chapterEnd/>
@--------------------------------------------------------------------------------
