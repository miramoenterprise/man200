@------------------------------------------------------------
@-- indexdef
@------------------------------------------------------------


<elementStart elementType="xml_mand_atts" elementName="IndexDef" 
	subElements="Y"
	>

<esp>
The <#xIndexDef> element specifies the inclusion criteria and the
layout and sorting properties of an index.
</esp>

@- using information collated from preceding <#xIX>
@- <esp>
@- The <#xIX> element is used to specify text for inclusion in the generated index.
@- Different index entry levels are delimited using the one or
@- more <#xIXsub> element within the <#xIX> element content.
@- </esp>

@- <esp>
@- The simplest index is generated using the following markup:
@- </esp>

</elementStart>

@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> - snk {
	@- Script name key value
}{
	<#def> snk.text		$snk
	<#def> snk.text		{<Font fontFamily="Cambridge" 
					fontWeight="Regular" ><#snk.text></Font>}
	@print({<#snk.text>}, n)
}
@------------------------------------------------------------

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Index format name"
	longTitle="Index format name "
	dest="<#elementName>.propertyGroup.formatname"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="indexDef"
	long="indexDef"
	value="string"
	>
<pdp>
<fI>Required</fI>
<br/>
Name of index format definition.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Index contents selection"
	longTitle="Index contents selectionname "
	dest="<#elementName>.propertyGroup.selection"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="names"
	long="names"
	value="namelist"
	>
<pdp>
Bar or comma separated list of index format names to include
in the index.
</pdp>

<pdp>
E.g. the following:
</pdp>

<exampleBlock
	leftIndent="10mm"
	>
names="generalIndex|authors"
</exampleBlock>

<pdp>
includes index entries from <#xIX> elements that have
<on>name</on> values 
<ov>generalIndex</ov> or <ov>authors</ov>.
See the <#xIX> element <pn>indexDef</pn> property on page <xnpage
	id="IX.property.name" />.
</pdp>

<propertyDefault>index</propertyDefault>

</property>

@------------------------------------------------------------
<propertyGroup
	shortTitle="Collation properties"
	longTitle="Collation properties"
	dest="<#elementName>.propertyGroup.collation_properties"
>
Apart from the <pn>language</pn> property, the following collation
properties are rarely needed.
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	short="language"
	long="language"
	value="unicode | key"
	>

<propertyValueKeyList
	>
<vk key="unicode" >
Use <fI>default unicode collation</fI>, without regard to language-specific
collation rules.
</vk>

<vk key="key" >
Use language-specific collation rules.
<br/>
The <fI>key</fI> values for the <on>language</on> property are
listed in <xparalabel
	id="tbl.list_of_language_values.start" /> on
page <xnpage
 	id="tbl.list_of_language_values.start" />.
<vidx id="text||language" />
<vidx id="language||text" />
<vidx id="<#languageContext> language" />
</vk>

</propertyValueKeyList>

@- <pdp>
@- </pdp>

<propertyDefault>unicode</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="casePrecedence"
	long="casePrecedence"
	value="default | lower | upper"
	>

<pdp>
Override the language-specific case precedence rules.
If <on>casePrecedence</on> is set to <ov>upper</ov> two
strings which are identical except in respect of letter-case
are listed in uppercase-first order, and <fI>vice versa</fI> if
<on>casePrecedence</on> is set to <ov>lower</ov>.
</pdp>

<pdp>
If <on>casePrecedence</on> is set to <ov>default</ov> the default
language-specific case precedence rules are used. (In most cases
a lowercase entry is listed before a corresponding uppercase entry.)
</pdp>

<propertyDefault>default</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="ignorePunctuation"
	long="ignorePunctuation"
	value="N | Y"
	>

<pdp>
Override the language-specific punctuation rules to
ignore punctuation characters when sorting.
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="ignoreSpace"
	long="ignoreSpace"
	value="N | Y"
	>

<pdp>
Override the language-specific space rules to
ignore space characters when sorting.
</pdp>

<propertyDefault>N</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="numberOrdering"
	long="numberOrdering"
	value="Y | N"
	>

<pdp>
Sort a series of digits in order of their numerical value,
from lowest to highest. Applies to decimal digits (Nd) only,
not to letter or other types of digits (see <HyperCmd
	fontDef="F_URL"
        openURL="http://www.fileformat.info/info/unicode/category/Nd/list.htm"
	>List of numbers, type Decimal Digit</HyperCmd>).
@- Unicode Characters in the 'Number, Decimal Digit' Category
</pdp>


@- [Nd]    540             Number, Decimal Digit
@- [Nl]    236             Number, Letter
@- [No]    570             Number, Other


<propertyDefault>Y</propertyDefault>
</property>

@------------------------------------------------------------
<property
	short="specialCollation"
	long="specialCollation"
	value="phonebook | big5han | hindiDirect | gb2312han | pinyin | stroke | traditional"
	>

<pdp>
The <on>specialCollation</on> property is used to modify
some language-specific collations.
</pdp>

<propertyValueKeyList
	>
<vk key="phonebook" >
Special phone book collation.
Applicable only when <on>language</on> is set
to <ov>German</ov> or <ov>Finnish</ov>.
</vk>

<vk key="big5han" >
Pinyin ordering for Latin and
Big5 charset ordering for Chinese, Japanese, and Korean characters.
Typically used with Traditional Chinese.
</vk>

<vk key="hindiDirect" >
Hindi variant collation.
</vk>

<vk key="gb2312han" >
Pinyin ordering for Latin and
gb2312han charset ordering for Chinese, Japanese, and Korean characters.
</vk>

<vk key="pinyin" >
Ordering for Chinese, Japanese, and Korean characters
based on character-by-character transliteration into Pinyin.
Typically used with Simplified Chinese.
</vk>

<vk key="stroke" >
Ordering for Chinese, Japanese, and Korean characters
based on character-by-character transliteration into Pinyin.
Typically used with simplified Chinese.
</vk>

<vk key="traditional" >
Spanish variant collation.
</vk>

</propertyValueKeyList>

</property>
@------------------------------------------------------------
<propertyGroup
	shortTitle="Group titles"
	longTitle="Group titles"
	dest="<#elementName>.propertyGroup.group_titles"
/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	name="groupTitles"
	value="Y | N"
	>

<pdp>
Include group title headings.
</pdp>

<propertyDefault>Y</propertyDefault>

</property>

@------------------------------------------------------------
<property
	name="scriptOrder"
	value="namelist"
	>

<pdp>
A space, comma or vertical bar separated list
specifying the order of scripts in the index.
<fI>namelist</fI> may include any or all of
the following script key names (in default order):
<snk>specialCharacters</snk>,
<snk>spaces</snk>,
<snk>punctuation</snk>,
<snk>symbols</snk>,
<snk>currencySigns</snk>,
<snk>numerics</snk>,
@-
<snk>latin</snk>,
<snk>greek</snk>,
<snk>cyrillic</snk>,
<snk>georgian</snk>,
<snk>armenian</snk>,
@------------------------------------------------------------
<snk>hebrew</snk>,
<snk>arabic</snk>,
<snk>ethiopic</snk>,
@------------------------------------------------------------
<snk>hebrew</snk>,
<snk>devanagari</snk>,
<snk>bengali</snk>,
<snk>gurmukhi</snk>,
<snk>gujarati</snk>,
<snk>oriya</snk>,
<snk>tamil</snk>,
<snk>telugu</snk>,
<snk>kannada</snk>,
<snk>malayalam</snk>,
<snk>sinhala</snk>,
@------------------------------------------------------------
<snk>thai</snk>,
<snk>lao</snk>,
<snk>tibetan</snk>,
<snk>myanmar</snk>,
<snk>khmer</snk>,
<snk>katakana</snk>,
<snk>thaana</snk>,	@- Maldives [RTL] probably exclude?
@------------------------------------------------------------
<snk>hangul</snk>,	@- Korean
@------------------------------------------------------------
<snk>bopomofo</snk>,	@- Used for transliteration of Mandarin Chinese
<snk>han</snk> and
<snk>others</snk>.
@------------------------------------------------------------
</pdp>

</property>

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------




@------------------------------------------------------------
@- <|subElementsStart>
@------------------------------------------------------------

@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	fontProperties="Y"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
@- <groupTitles ../>
@------------------------------------------------------------
<subElement
	name="groupTitles"
	content="Y"
	>
<subElementProperties>
@- paraDef="name" <fI>paragraphProperties</fI>
<subProperty name="paraDef" value= "name" genericProperty="paragraphProperties" desc="yyyy" />
</subElementProperties>

<bbp>
Wrapper for <#xgroupTitle> elements.
@- If the <#xgroupTitles> sub-element
@- is absent (default) or empty, no index group titles are included in the index.
</bbp>

<bbp>
The <on>paraDef</on> property specifies the name
of a paragraph format defined using the <#xParaDef> element (see
pages <xnpage
	id="ParaDef.element.start" /><xphyphen/><xnpage
	id="ParaDef.element.end" />).
</bbp>

<bbp>
The default value of <on>paraDef</on> is <ov>groupTitles</ov>.
</bbp>

<bbp>
<fI>paragraphProperties</fI> refers to <#xP> element properties.
See the <#xP> element on pages <xnpage
	id="P.element.start" /><xphyphen/><xnpage
	id="P.element.end" />).
</bbp>

</subElement>

@------------------------------------------------------------
@- <groupTitle ../>
@------------------------------------------------------------
<subElement
	name="groupTitle"
	content="Y"
	>
<subElementProperties>
@- before="string"
@- <subProperty name="before" value= "string" desc="yyyy" />
<subProperty name="before" value= "string | name" desc="yyyy" />
</subElementProperties>

<bbp>
Specify where to place index entry group titles. 
<#xgroupTitle> sub-elements must be contained
within the <#xgroupTitles> element. The <on>before</on> property
indicates the sort position which the group title should precede.
E.g.
</bbp>

<exampleBlock
	leftIndent="10mm"
	>
<@skipStart>
<groupTitles paraDef="myGroupTitlesPgfFormat" >
	<groupTitle before="A" >A - I</groupTitle>
	<groupTitle before="J" >J - Z</groupTitle>
</groupTitles>
@- https://en.wikipedia.org/wiki/Chinese_characters
<@skipEnd>
<groupTitles paraDef="myGroupTitlesPgfFormat" >
	<groupTitle before="specialCaracters" >Special characters</groupTitle>
	<groupTitle before="latin" >Latin text</groupTitle>
	<groupTitle before="cyrillic" >Cyrillic text</groupTitle>
	<groupTitle before="han" >Hanja, Hanzi and Kanji</groupTitle>
	<groupTitle before="others" >Miscellaneous</groupTitle>
	<groupTitle before="A" >A - I</groupTitle>
	<groupTitle before="J" >J - Z</groupTitle>
</groupTitles>
</exampleBlock>

<bbp>
Group title text is included only if the <#xIndexDef> or <#xIndex> element
<on>groupTitles</on> property is not set to <ov>N</ov>.
</bbp>

</subElement>

@------------------------------------------------------------
@- <levelFormat ../>
@------------------------------------------------------------
<subElement
	name="levelFormat"
	>
<subElementProperties>
@- paraDef="name"
<subProperty name="paraDef" value= "name" genericProperty="paragraphProperties" desc="yyyy" />
</subElementProperties>

<bbp>
The <on>paraDef</on> property specifies the name
of a paragraph format defined using the <#xParaDef> element (see
pages <xnpage
	id="ParaDef.element.start" /><xphyphen/><xnpage
	id="ParaDef.element.end" />).
The first levelFormat sub-element
specifies the paragraph format to be used for the top-level
index entry (an index entry with no <#xIXsub> sub-elements).
</bbp>

<bbp>
Successive <#xlevelFormat> sub-elements specify the paragraph
formats to be used at lower levels.
</bbp>

</subElement>

@------------------------------------------------------------
@- <entrySuffix ../>
@------------------------------------------------------------
<subElement
	name="entrySuffix"
	content="Y"
	>
<bbp>
<fI>content</fI> is suffix text to be used immediately
following the index entry text if the index entry contains
page number references.
--indextextsuffix--
</bbp>

<bbp>
Default value for <fI>content</fI>: a single space character " "
</bbp>

</subElement>

@------------------------------------------------------------
@- <pageReferenceSeparator ../>
@------------------------------------------------------------
<subElement
	name="pageReferenceSeparator"
	content="Y"
	fontProperties="Y"
	>
<bbp>
<fI>content</fI> appears between each page or page range reference.
</bbp>

<bbp>
Default value for <fI>content</fI>: ", " (comma, space)
</bbp>

</subElement>

@------------------------------------------------------------
@- <pageRangeSeparator ../>
@------------------------------------------------------------
<subElement
	name="pageRangeSeparator"
	content="Y"
	fontProperties="Y"
	dest="IndexDef.pageRangeSeparator.start"
	>
<subElementProperties>
@- entryType="normal | primary"
<subProperty name="entryType" value= "normal | primary" desc="yyyy" />
</subElementProperties>

<bbp>
<fI>content</fI> appears between the start and end page number in
a page range reference.
</bbp>

<bbp>
The <#xpageRangeSeparator> sub-element
<on>entryType</on> property enables different formatting for normal and
primary entries, specified by the <#xIX> <on>entryType</on> property (see
page <xnpage
	id="IX.property.entryType" />).
The default value for <on>entryType</on> is <ov>normal</ov>.
</bbp>

<bbp>
If there is more than one <#xpageRangeSeparator> sub-element
with the same <on>entryType</on> value only the first is applicable.
</bbp>

<bbp>
Default value for <fI>content</fI>: "&endash;" (En dash)
</bbp>

</subElement>

@------------------------------------------------------------
@- <pageNumber ../>
@------------------------------------------------------------
<subElement
	name="pageNumber"
	content="N"
	fontProperties="Y"
	dest="<#elementName>.pageNumber.start"
	>
<subElementProperties>
@- entryType="normal | primary"
<subProperty name="entryType" value= "normal | primary" desc="yyyy" />
</subElementProperties>

<bbp>
Expands to the referenced page number or to the first and last
referenced page numbers in a page number range.
</bbp>

<bbp>
The <#xpageNumber> sub-element
<on>entryType</on> property enables different formatting for normal and
primary entries, specified by the <#xIX> and <#xIXsub> <on>entryType</on> property (see
page <xnpage
	id="IX.property.entryType" />).
The default value for <on>entryType</on> is <ov>normal</ov>.
</bbp>

<bbp>
If there is more than one <#xpageNumber> sub-element
with the same <on>entryType</on> value, the first is used.
</bbp>

<bbp>
By default the page number references have the same font
properties as the index entry text, as specified by the
paragraph formats referenced by the <#xIndexDef> <#xlevelFormat> sub-elements
see page <xnpage
	id="IndexDef.subelement.levelFormat" />). <fI>fontProperties</fI> override
the may be used with the <#xpageNumber> sub-element to override
the default properties.
</bbp>

<propertyDefault>normal</propertyDefault>

</subElement>


@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------

@------------------------------------------------------------
@- NOTES
@------------------------------------------------------------

@- <elementNote dest="note.ListLabel.label_types" >

@- <enp>
@- The <#xListLabel> element is used for two distinct types
@- of lists:
@- </enp>
@- </elementNote>


@------------------------------------------------------------
@- EXAMPLES
@------------------------------------------------------------


<elementExampleTitle
	dest="ex.IndexDef.simple_index_format_definitionTitle"
	>Simple index format definition</elementExampleTitle>
<exp>
Example <xparanumonly
	id="ex.IndexDef.simple_index_format_definition" /> illustrates
a simple index format definition.
</exp>

<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
        dest="ex.IndexDef.simple_index_format_definition"
        >
Simple <#xIndexDef> format definition
</exampleCaption>
<IndexDef indexDef="simpleIndex"
	names="Index" >
	<groupTitles paraDef="GroupTitlesIX">
		<GroupTitle before="\ ">Symbols</GroupTitle>
		<GroupTitle before="0">Numerics</GroupTitle>
		<GroupTitle before="A">A-M</GroupTitle>
		<GroupTitle before="N">N-Z</GroupTitle>
	</groupTitles>
	<levelFormat paraDef="Level1IX" />
	<entrySuffix>, </entrySuffix>
	<pageReferenceSeparator>, </pageReferenceSeparator>
	<pageRangeSeparator>&endash;</pageRangeSeparator>
	<pageNumber fontDef="_IndexPageNum"/>
	</IndexLevel>
</IndexDef>
</exampleBlock>


@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
