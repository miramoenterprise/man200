@------------------------------------------------------------
@-- ruledef
@------------------------------------------------------------

<elementStart elementType="xml_no_cont" elementName="RuleDef" >

<esp>
Use <#xRuleDef> to define a ruling for use in tables.
A ruling may be referenced <fI>via</fI>:
<#xTblDef>,
<#xTbl>,
<#xRow>,
or
<#xCell>
elements.
</esp>

</elementStart>

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Table ruling names and properties"
	longTitle="Table ruling names and properties"
	dest="<#elementName>.propertyGroup.start"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="ruleDef"
	long="ruleDef"
	@- long="ruleName"
        value="name"
	>

<pdp>
<#Required>
<br/>
<fI>name</fI> is ruling format name.
</pdp>

</property>

@------------------------------------------------------------
<property
	short="pen"
	long="penNum"
        value="0 - 7, 15"
	condition="fmcGuide"
	>
<pdp>
Pen pattern for ruling,
0 (black) through to 15 (none).
</pdp>

<propertyDefault>0</propertyDefault>
</property>

@------------------------------------------------------------
<property
	@- short="pw"
	long="penWidth"
	value="dim"
	>
<pdp>
Pen width.
Default units are points.
</pdp>

<propertyDefault>0.5pt</propertyDefault>
</property>


@------------------------------------------------------------
<property
	short="rl"
	long="numLines"
	value="0 | 1 | 2"
	condition="fmcGuide"
	>
<pdp>
Number of lines in ruling.
</pdp>

<propertyDefault>1</propertyDefault>
</property>


@------------------------------------------------------------
<property
	name="lineGap"
	value="dim"
	condition="fmcGuide"
	>
<pdp>
Distance between ruling line centers.
Applies only if <#osq>rl="2"<#csq> (see above). 
Default units are points.
</pdp>

<propertyDefault>2pt</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="penColor"
	value="name"
	>
<pdp>
Ruling color.
</pdp>

<propertyDefault>Black</propertyDefault>
</property>


@------------------------------------------------------------
<property
	name="penTint"
	value="percent"
	>
<pdp>
Ruling tint.
</pdp>

<propertyDefault>100%</propertyDefault>
</property>

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------


@------------------------------------------------------------
<@skipStart>
@------------------------------------------------------------


<elementNote>
Rulings are centered about table cell borders. See illustration in
Figure <xparanumonly
        id="ex.tcmtcb" file="tables" /> on page <xnpage
        id="ex.tcmtcb" file="tables" /> in the <#MUGuide>.

<elementNote>
Properties not specified within a <#xRuleDef> element 
always default to the default values shown above (i.e. properties
do not inherit values defined by preceding <#xRuleDef> elements).

<elementNote>
If there are
two or more <#xRuleDef> elements with the same tag name, the values of
properties included in later instances will override the values defined
in preceding instances.

<elementNote>
If the value of color is not the name of a color defined
either in a template file (and accessed using the import color property
in <#xTemplate>) or the name of a color defined using a <#xColorDef> element,
the color defaults to <#osq>Black<#csq>, and is <#osq>invisible<#csq>.

<elementNote>
Do not use <ov>None</ov> as the name of a ruling definition. 

<elementExamples>

The default <#xRuleDef> properties are as follows:

<@nChartEx> {10|22|40} {nolines} {P="P_RefBody}" {
<RuleDef
	ruleDef	=" "default""
	pen	=" "1""	<!-- Pen type 1 (solid) --> 
	pw	=" "0.5pt""	<!-- Ruling pen width --> 
	rl	=" "1""	<!-- No of rule lines (0, 1 or 2) --> 
	rg	=" "2pt""	<!-- Ruling gap (used if rl="2)" --> 
	color	=" "Black""	<!-- Color --> 
/>
}

@- <P fmt="P_RefBody" sa="0" sb="0"  >
Further <#xRuleDef> examples are shown below:

<@nChartEx> {10|22|40} {nolines} {P="P_RefBody}" {
<RuleDef ruleDef="test_rule" color="Elephant" pw="10pt" />

<RuleDef 
	ruleDef	="3.0mm(Tint2)"
	pen	="2"	<!-- pen type 2 (gray) --> 
	color	="Blue"
	pw	="3mm"	<!-- pen width --> 
	rl	="1"	<!-- No of rule lines (1 0r 2) --> 
/>

<RuleDef ruleDef="test_rule" pen="0" />

<RuleDef ruleDef=" "3.0mm(Tint3)"" pen=" "3"" rl=" "1"" />
}


@------------------------------------------------------------
<@skipEnd>
@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
