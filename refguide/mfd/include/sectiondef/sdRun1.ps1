#--------------------------------------------------------------------------------
#- sdRun1.ps1
#--------------------------------------------------------------------------------
#
#	called from inline/masterpagerule
#
# <@system> { set mprDoubleSided="Y" && powershell -file %inline%/include/masterpagerule/mpRun1.ps1 }
#
#--------------------------------------------------------------------------------
#
$sdHomeDir	= "$env:mfd/include/sectiondef"

#--------------------------------------------------------------------------------
if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
if (-not (Test-Path env:sdExampleFile)) { $env:sdExampleFile = "masterpagerule1.pdf" }
write-host "-========================== $env:mprDoubleSided "

#--------------------------------------------------------------------------------
$env:mprDoubleSided	= $env:mprDoubleSided.Trim()
$env:mprDoubleSided	= $env:mprDoubleSided.Trim("'",'"')

$env:sdExampleFile	= $env:sdExampleFile.Trim()
$env:sdExampleFile	= $env:sdExampleFile.Trim("'",'"')
#--------------------------------------------------------------------------------
$composerz = "$env:mcomposer"
$abc    = "mfd/include/sectiondef/mpRun1.sh xxyy env composerTag value: $env:composerTag"

$abc | out-file -enc oem "C:\tmp\abc.txt"

mmpp "$sdHomeDir/control.mmp" > "$sdHomeDir/mmpout.mm"

# miramo -composer "$env:composer" -M -Opdf "$sdHomeDir/$env:sdExampleFile"  "$sdHomeDir/control.mmp"
# miramo -composer "$env:composerTag" -M -Opdf "$sdHomeDir/$env:sdExampleFile"  "$sdHomeDir/control.mmp"
#
# Should work but does not, because of -M
# miramoRP -M -Opdf "$sdHomeDir/$env:sdExampleFile"  "$sdHomeDir/control.mmp"
#
mmpp "$sdHomeDir/control.mmp" | out-file -enc oem "$sdHomeDir/sdRun1-mmppout.tmp"
miramoRP -M -Opdf "$sdHomeDir/$env:sdExampleFile"  "$sdHomeDir/sdRun1-mmppout.tmp"
