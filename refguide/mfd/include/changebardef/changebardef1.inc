@------------------------------------------------------------
@- changebardef1.inc
@------------------------------------------------------------
<@xmacro> changeBarParams {
	<#def> cbp.position		start
	<#def> cbp.color		Red
	<#def> cbp.style		groove
	<#def> cbp.style		ridge
	<#def> cbp.width		22pt
	<#def> cbp.offset		26pt
	@----------------------------------------------------
	<#def> cbp.firstLineIndent	0
	<#def> cbp.startIndent		0
	@----------------------------------------------------
	<#def> cbp.outputTotalWidth	45mm
	<#def> cbp.textFrameAnnoTop	2.9mm
	<#def> cbp.topMargin		4.5mm
	<#def> cbp.leftMargin		14mm
	<#def> cbp.rightMargin		2mm
	<#def> cbp.bottomMargin		4.0mm
	@----------------------------------------------------
	@xgetvals(cbp.)
	@----------------------------------------------------
	<#def> cbp.textFrameWidth	@eval(<#cbp.outputTotalWidth>
						- <#cbp.leftMargin>
						- <#cbp.rightMargin> )
	<#def> cbp.rightMarginOffset	@eval(<#cbp.outputTotalWidth>
						- <#cbp.rightMargin> )
	@----------------------------------------------------
	@- Offset annotation
	@----------------------------------------------------
	<#def> cbp.offsetAnnoTop	6.4mm
	<#def> cbp.offsetPoint		@eval(<#cbp.leftMargin>
						- <#cbp.offset> )
	@----------------------------------------------------
	@- Width annotation
	@----------------------------------------------------
	<#def> cbp.widthAnnoTop		10.9mm
	<#def> cbp.widthAnnoTop		12.5mm
	<#def> cbp.widthPoint		@eval(<#cbp.leftMargin>
						- <#cbp.offset>
						- <#cbp.width> / 2)
}{}
@------------------------------------------------------------
<changeBarParams/>
@------------------------------------------------------------
<elementExampleTitle
	dest="<#elementName>.changeBarDef1.start"
	>Defining change bar formats
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.changeBarDef1" /> illustrates how to use
the <#xChangeBarDef> element to define a change bar format.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.changeBarDef1"
	>Change bar definition (1)
</exampleCaption>
<ChangeBarDef
	changeBarDef="myChangeBar"
	offset="<#cbp.offset>"
	width="<#cbp.width>"
	colorDef="<#cbp.color>"
	style="<#cbp.style>"
	position="<#cbp.position>"
	/>
</exampleBlock>

<#example_output>

<exp>
A change bar format, e.g. as defined in
Example <xparanumonly
	id="ex.changeBarDef1" />, may be used as shown in
Example <xparanumonly
	id="ex.changeBarDef2" />.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.changeBarDef2"
	>Change bar usage (1)
</exampleCaption>
<P fontFamily="Arial" textSize="7pt" >The Orangan forest has:<br/>bears<ChangeBarBegin
	changeBarDef="myChangeBar" /> and wolves<ChangeBarEnd/>.</P>
</exampleBlock>

<exp>
The output from Example <xparanumonly
	id="ex.changeBarDef2" /> is shown in Figure <xparanumonly
	id="fig.changeBarDef2" />. The dotted red lines delimit
the container frame border.

<exampleOutput
	width="<#cbp.outputTotalWidth>"
	fillOpacity="100"
	rightMargin="10"
	topMargin="<#cbp.topMargin>"
	leftMargin="<#cbp.leftMargin>"
	rightMargin="<#cbp.rightMargin>"
	bottomMargin="<#cbp.bottomMargin>"
	shadow="Y"
	crop="N"
	>
<outputCaption
	dest="fig.changeBarDef2"
> Output from Example <xparanumonly
	id="ex.changeBarDef2" />
</outputCaption>
<#example_output>
<mmDraw>

	@--- Top margin
	<textFrameBorder top="<#cbp.topMargin>" left="<#cbp.leftMargin>"
		lineLength="<#cbp.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#cbp.topMargin>" left="<#cbp.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#cbp.topMargin>"
		@- left="@eval(<#cbp.leftMargin> + <#cbp.textFrameWidth> - 1)"
		left="<#cbp.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

	@--- Text frame width
	<doubleArrow top="<#cbp.textFrameAnnoTop>" left="<#cbp.leftMargin>" lineLength="<#cbp.textFrameWidth>"
		textLen="14.5mm" textSize="5.5pt"
		>text frame width</doubleArrow>

	@--- Offset dimension
	<doubleArrow top="<#cbp.offsetAnnoTop>" left="<#cbp.offsetPoint>" lineLength="<#cbp.offset>"
		showEndLines="Y"
		textLen="5.3mm" textSize="5.5pt"
		>offset</doubleArrow>

	@--- Width dimension
	<doubleArrow top="<#cbp.widthAnnoTop>" left="<#cbp.widthPoint>" lineLength="<#cbp.width>"
		textLen="5.3mm" textSize="5.5pt"
		>width</doubleArrow>
</mmDraw>
</exampleOutput>
</exp>

