#--------------------------------------------------------------------------------
#- testRun.ps1
#--------------------------------------------------------------------------------
#
#	called from inline/masterpagerule
#
# <@system> { set mprDoubleSided="Y" && powershell -file %inline%/include/masterpagerule/mpRun1.ps1 }
#
#--------------------------------------------------------------------------------
$mprHomeDir	= "$env:inline/include/masterpagerule"
#--------------------------------------------------------------------------------
if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
if (-not (Test-Path env:mprExampleFile)) { $env:mprExampleFile = "masterpagerule2.pdf" }
# write-host "-========================== $env:mprDoubleSided "

#--------------------------------------------------------------------------------
$env:mprDoubleSided	= $env:mprDoubleSided.Trim()
$env:mprDoubleSided	= $env:mprDoubleSided.Trim("'",'"')

$env:mprExampleFile	= $env:mprExampleFile.Trim()
$env:mprExampleFile	= $env:mprExampleFile.Trim("'",'"')
#--------------------------------------------------------------------------------

# mmpp "$mprHomeDir/testControl.mmp" > "$mprHomeDir/mmpout.mm"

# miramo -composer mmc -M -Opdf "$mprHomeDir/$env:mprExampleFile"  "$mprHomeDir/testControl.mmp"

# miramo -composer mmc -M -Opdf yy.pdf sideheads1.mmp

miramoRP -composer mmc -Opdf yy.pdf sideheads1.mmp
