#--------------------------------------------------------------------------------
#- sideheadsRun1.ps1
#--------------------------------------------------------------------------------
#
#	called from mfd/sideheads.inc
#
#--------------------------------------------------------------------------------
# $mprHomeDir	= "$env:inline/include/masterpagerule"
#--------------------------------------------------------------------------------
# if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
# if (-not (Test-Path env:mprExampleFile)) { $env:mprExampleFile = "masterpagerule1.pdf" }

#--------------------------------------------------------------------------------
#
#
# <runJob> macro has:
#	parameter1="sideheadsHomeDir='%mfd%/include/paradef/sideheads'"
#	parameter2="sideheadExampleFile='<#sideheadExampleFile1>'"
#

# $env:sideheadsHomeDir		= "C:\u\miramo\man\man100\refguide\mfd\include\paradef\sideheads"
# $env:sideheadsHomeDir		= $env:sideheadsHomeDir.Trim()
# $env:sideheadsHomeDir		= $env:sideheadsHomeDir.Trim("'",'"')
# $env:sideheadsHomeDir		= "C:\u\miramo\man\man100\refguide\mfd\include\paradef\sideheads"

# $env:sideheadExampleFile	= "sideheadExampleFile1"
# $env:sideheadExampleFile	= $env:sideheadExampleFile.Trim()
# $env:sideheadExampleFile	= $env:sideheadExampleFile.Trim("'",'"')
# $env:sideheadExampleFile	= "sideheadExampleFile1"
#--------------------------------------------------------------------------------


write-host	"$env:sideheadsHomeDir/$env:sideheadExampleFile.pdf"

# mmpp "$env:sideheadsHomeDir/sideheads1.mmp" | out-file -enc oem  "$env:sideheadsHomeDir/tmpOutput.mm"
# $env:composerTag specified in metaDefs.mmp from runx.ps1
# miramo -composer "$env:composerTag"  -showProperties N `
# 	-Opdf "$env:sideheadsHomeDir/$env:sideheadExampleFile.pdf" `
# 	"$env:sideheadsHomeDir/$env:sideheadExampleFile.mm"
miramoRP -showProperties N `
	-Opdf "$env:sideheadsHomeDir/$env:sideheadExampleFile.pdf" `
	"$env:sideheadsHomeDir/$env:sideheadExampleFile.mm"

