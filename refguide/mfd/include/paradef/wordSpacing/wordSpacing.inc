@--------------------------------------------------------------------------------
@- wordSpacing.inc
@--------------------------------------------------------------------------------
<MiramoXML>
<P paraDef="Body" 
	textSize="72pt"
	spaceAbove="0"
	spaceBelow="0"
	>Hello<Font fontFamily="mmts4"
	>&#xE300;</Font> <Font fontFamily="mmts4" >&#xE300;</Font>There </P>
<P paraDef="Body" 
	textSize="72pt"
	spaceAbove="0"
	spaceBelow="0"
	>Hello There</P>
</MiramoXML>
