@--------------------------------------------------------------------------------
@- paraFrame.mmp
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<@xmacro> - getRulingWidths {
	<#def> pgf.startRuleWidth		0
	<#def> pgf.topRuleWidth			0
	<#def> pgf.endRuleWidth			0
	<#def> pgf.bottomRuleWidth		0
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.startRule>X}, {XGoldX})) {<#def> pgf.startRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.startRule>X}, {XDenseBlueX})) {<#def> pgf.startRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.startRule>X}, {XDenseBlue2X})) {<#def> pgf.startRuleWidth	<#denseBlueWidth2>}
	@if(@match({X<#pgf.startRule>X}, {XKhakiX})) {<#def> pgf.startRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.startRule>X}, {XRubyX})) {<#def> pgf.startRuleWidth	<#rubyWidth>}
	@if(@match({X<#pgf.startRule>X}, {XthinGoldX})) {<#def> pgf.startRuleWidth	<#thinGoldWidth>}
	@if(@match({X<#pgf.startRule>X}, {XthinBlueX})) {<#def> pgf.startRuleWidth	<#thinBlueWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.topRule>X}, {XGoldX})) {<#def> pgf.topRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.topRule>X}, {XDenseBlueX})) {<#def> pgf.topRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.topRule>X}, {XDenseBlue2X})) {<#def> pgf.topRuleWidth	<#denseBlueWidth2>}
	@if(@match({X<#pgf.topRule>X}, {XKhakiX})) {<#def> pgf.topRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.topRule>X}, {XRubyX})) {<#def> pgf.topRuleWidth	<#rubyWidth>}
	@if(@match({X<#pgf.topRule>X}, {XthinBlueX})) {<#def> pgf.topRuleWidth	<#thinBlueWidth>}
	@if(@match({X<#pgf.topRule>X}, {XthinGoldX})) {<#def> pgf.topRuleWidth	<#thinGoldWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.endRule>X}, {XGoldX})) {<#def> pgf.endRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.endRule>X}, {XDenseBlueX})) {<#def> pgf.endRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.endRule>X}, {XDenseBlue2X})) {<#def> pgf.endRuleWidth	<#denseBlueWidth2>}
	@if(@match({X<#pgf.endRule>X}, {XKhakiX})) {<#def> pgf.endRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.endRule>X}, {XRubyX})) {<#def> pgf.endRuleWidth	<#rubyWidth>}
	@if(@match({X<#pgf.endRule>X}, {XthinGoldX})) {<#def> pgf.endRuleWidth	<#thinGoldWidth>}
	@if(@match({X<#pgf.endRule>X}, {XthinBlueX})) {<#def> pgf.endRuleWidth	<#thinBlueWidth>}
	@------------------------------------------------------------------------
	@if(@match({X<#pgf.bottomRule>X}, {XGoldX})) {<#def> pgf.bottomRuleWidth	<#goldWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XDenseBlueX})) {<#def> pgf.bottomRuleWidth	<#denseBlueWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XDenseBlue2X})) {<#def> pgf.bottomRuleWidth	<#denseBlueWidth2>}
	@if(@match({X<#pgf.bottomRule>X}, {XKhakiX})) {<#def> pgf.bottomRuleWidth	<#khakiWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XRubyX})) {<#def> pgf.bottomRuleWidth	<#rubyWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XthinGoldX})) {<#def> pgf.bottomRuleWidth	<#thinGoldWidth>}
	@if(@match({X<#pgf.bottomRule>X}, {XthinBlueX})) {<#def> pgf.bottomRuleWidth	<#thinBlueWidth>}
}{}
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<#def>	g.dashedBorder		{}
@--------------------------------------------------------------------------------
<#def> g.h1			7	@--- Top row height
<#def> g.h1			1	@--- Top row height
@--------------------------------------------------------------------------------
<#def> g.c1			7	@--- Width of column 1
<#def> g.c2			59	@--- Width of column 2
<#def> g.c3			18	@--- Width of column 3
<#def> g.c4			58	@--- Width of column 4
<#def> g.l			4	@--- Leading
<#def> g.l			3	@--- Leading
<#def> g.sa			7	@--- space above
<#def> g.sa			10	@--- space above
@--------------------------------------------------------------------------------
<#def> g.ts			12pt	@--- default text size
<#def> g.ts			10pt	@--- default text size
<#def> g.ts			9pt	@--- default text size
<#def> pgf.sa			7	@--- space above
@--------------------------------------------------------------------------------
<@skipStart>
<#def> goldWidth		6pt
<#def> denseBlueWidth		6pt
<#def> khakiWidth		4pt
<#def> rubyWidth		4pt
<@skipEnd>
@--------------------------------------------------------------------------------
<#def> pgf.count		0	@--- Counter for number of pgfs / paraframes
<#def> g.borderLeftBase		@eval(<#g.c1> + <#g.c2> + <#g.c3>)
<#def> g.borderTopBase		@eval(<#g.h1>)
@--------------------------------------------------------------------------------
<#def> g.showTableRules		Y
<#def> g.showMargins		Y
<#def> g.showNumbers		Y
<#def> g.tblRuling		{Very Thin}
<#def> g.tblRuling		{None}
@--------------------------------------------------------------------------------
<#def> g.leadingColor		{Black}
<#def> g.leadingTint		{39%}
<#def> g.leadingOpacity		{35%}
<#def> g.leadingOpacity		{100%}
@--------------------------------------------------------------------------------
<#def> g.spaceColor		{Black}
<#def> g.spaceTint		{19%}
<#def> g.spaceOpacity		{35%}
<#def> g.spaceOpacity		{100}
@--------------------------------------------------------------------------------
<#def> g.annoNumber {}
@--------------------------------------------------------------------------------
