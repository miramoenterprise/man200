#--------------------------------------------------------------------------------
#- runTest.ps1
#--------------------------------------------------------------------------------
#
#	called from mfd/sectiondef
#
# <@system> { set mprDoubleSided="Y" && powershell -file %mfd%/include/sectiondef2/mpRun1.ps1 }
#
#--------------------------------------------------------------------------------
$mprHomeDir	= "$env:mfd/include/sectiondef2"
#--------------------------------------------------------------------------------
if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
if (-not (Test-Path env:mprExampleFile)) { $env:mprExampleFile = "sectiondef2.1.pdf" }
write-host "-========================== $env:mprDoubleSided "

#--------------------------------------------------------------------------------
$env:mprDoubleSided	= "Y"
$env:mprDoubleSided	= $env:mprDoubleSided.Trim()
$env:mprDoubleSided	= $env:mprDoubleSided.Trim("'",'"')

$env:mprExampleFile	= $env:mprExampleFile.Trim()
$env:mprExampleFile	= $env:mprExampleFile.Trim("'",'"')
#--------------------------------------------------------------------------------

mmpp "$mprHomeDir/testControl.mmp" > "$mprHomeDir/mmpout.mm"

miramo -composer mmc -M -Opdf "$mprHomeDir/$env:mprExampleFile"  "$mprHomeDir/testControl.mmp"
