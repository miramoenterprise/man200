#--------------------------------------------------------------------------------
#- mpRun1.ps1
#--------------------------------------------------------------------------------
#
#	called from inline/masterpagerule
#
# <@system> { set mprDoubleSided="Y" && powershell -file %inline%/include/masterpagerule/mpRun1.ps1 }
#
#--------------------------------------------------------------------------------
#
$sdHomeDir	= "$env:mfd/include/sectiondef"

#--------------------------------------------------------------------------------
if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
if (-not (Test-Path env:sdExampleFile)) { $env:sdExampleFile = "sectiondef2.1.pdf" }
write-host "-========================== $env:mprDoubleSided "

#--------------------------------------------------------------------------------
$env:mprDoubleSided	= $env:mprDoubleSided.Trim()
$env:mprDoubleSided	= $env:mprDoubleSided.Trim("'",'"')

$env:sdExampleFile	= $env:sdExampleFile.Trim()
$env:sdExampleFile	= $env:sdExampleFile.Trim("'",'"')
#--------------------------------------------------------------------------------

$abc	= "mfd/include/sectiondef/mpRun1.sh env composer value: $env:composer"

$abc | out-file -enc oem "C:\tmp\abc.txt"

mmpp "$sdHomeDir/control.mmp" > "$sdHomeDir/mmpout.mm"

miramo -composer mmc -M -Opdf "$sdHomeDir/$env:sdExampleFile"  "$sdHomeDir/control.mmp"
