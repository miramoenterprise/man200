@------------------------------------------------------------------------
@- cropMarginsFigure.inc
@------------------------------------------------------------------------
<@xmacro> cropMarginsFigure {
	<#def> baseImageW	28
	<#def> baseImageW	30
	<#def> baseImageW	22
	<#def> baseImageW	16
	<#def> baseImageW	18
	<#def> baseImageW	20
	<#def> imageToAnno	@eval(38 / 20.5)
	<#def> imageToAnno	@eval(52 / 33.5)
	<#def> imageToAnno	@eval(52 / 30.5)
	<#def> imageToAnno	@eval(52 / 31.5)
	<#def> imageToAnno	@eval(52 / 31.3)	@- Smaller denominator makes image bigger
	<#def> imageToAnno	@eval(52 / 31.4)	@- Smaller denominator makes image bigger
	<#def> croppedImageW	@eval(<#baseImageW> / 20.5)
	
	<#def> actPageW		203.2
	<#def> pdfLM		37
	<#def> pdfRM		39
	<#def> img1W		<#baseImageW>
	<#def> img2W		@eval(<#baseImageW> * <#imageToAnno>)
	<#def> img3W		@eval(<#img2W> - (1 / <#imageToAnno>))
	<#def> img4W		@eval((<#actPageW> - (<#pdfLM> + <#pdfLM> )) * <#baseImageW> / <#actPageW>)
	
	<#def> col1W		@eval(<#img1W> + 3)
	<#def> col2W		@eval(<#img2W> + 3)
	<#def> col3W		@eval(<#img3W> + 3)
	<#def> col4W		@eval(<#img4W> + 8)
	
	
	<#def> pdiImageDir	$\{mman\}/refguide/mfd/include/pdfimportdef/images
	@------------------------------------------------------------------------
	@- <Tbl tblDef="thinRulings" allMargins="0" >
<exampleOutput
	width="110"
	position="below"
	shadow="Y"
	leftMargin="1mm"
	bottomMargin="-1mm"
	>
<outputCaption
	dest="fig.PdfImportDef.cropMargins"
	>
Crop margins
</outputCaption>
	<Tbl tblDef="noRulings" allMargins="0" >
	<TblColumnDef width="<#col1W>mm" />
	<TblColumnDef width="<#col2W>mm" />
	<TblColumnDef width="<#col3W>mm" />
	<TblColumnDef width="<#col4W>mm" />
	@- <Row height="40mm" withNext="Y" >
	<Row withNext="Y" >
	<Cell>
	<AFrame wrapContent="Y" align="start" >
	<Image file="<#pdiImageDir>/cropMargins1.pdf" width="<#img1W>mm" />
	</AFrame>
	</Cell>
	<Cell>
	<AFrame wrapContent="Y" align="start" >
	<Image file="<#pdiImageDir>/cropMargins2.pdf" width="<#img2W>mm" />
	</AFrame>
	</Cell>
	<Cell>
	<AFrame wrapContent="Y" align="start" >
	<Image file="<#pdiImageDir>/cropMargins3.pdf" width="<#img3W>mm" />
	</AFrame>
	</Cell>
	<Cell>
	<AFrame wrapContent="Y" align="start" >
	<Image file="<#pdiImageDir>/cropMargins4.pdf" width="<#img4W>mm" />
	</AFrame>
	</Cell>
	</Row>
	@------------------------------------------------------------------------
	<Row withNext="Y" allMargins="2mm" startMargin="0" >
	
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	><sq><Font fontFamily="Frutiger LT 45 Light" fontWeight="Bold" >A</Font></sq>
	&ensp;Uncropped page
	</Cell>
	
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	><sq><Font fontFamily="Frutiger LT 45 Light" fontWeight="Bold" >B</Font></sq>
	&ensp;Small crop margins
	</Cell>
	
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	><sq><Font fontFamily="Frutiger LT 45 Light" fontWeight="Bold" >C</Font></sq>
	&ensp;Large crop margins
	</Cell>
	
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	><sq><Font fontFamily="Frutiger LT 45 Light" fontWeight="Bold" >D</Font></sq>
	&ensp;Result
	<br/>
	of <sq><Font 
	fontFamily="Frutiger LT 45 Light" fontWeight="Bold" >C</Font></sq>
	</Cell>
	@------------------------------------------------------------------------
	</Row>
	</Tbl>
	@------------------------------------------------------------------------
</exampleOutput>
}{}
