@--------------------------------------------------------------------------------
@- leftAndTopNote.inc
@--------------------------------------------------------------------------------
<@xmacro> leftAndTopFigure {
	<exampleOutput
		width="70mm"
		shadow="Y"
		belowGap="-4mm"
		sideGap="2mm"
		leftMargin="2.5mm"
		>
	<outputCaption
	        dest="fig.PdfImportDef.leftAndTop1"
	        >
	Positive <pn>left</pn> and <pn>top</pn> values
	</outputCaption>
	@- <Tbl tblDef="thinRulings"
	<Tbl tblDef="noRulings"
		columns="2"
		allMargins="0"
		spaceBelow="0"
		>
	<TblColumnDef width="38mm" />
	<TblColumnDef width="29mm" />
	<Row>
	@------------------------------------------------------------
	<Cell>
	<AFrame wrapContent="Y" align="start"
		fillColor="Black"
		fillTint="10%"
		>
	<Image file="<#pdiImageDir>/positiveShift2.pdf" height="24mm" />
	</AFrame>
	</Cell>
	@------------------------------------------------------------
	<Cell>
	<AFrame wrapContent="Y" align="start"
		fillColor="Black"
		fillTint="10%"
		>
	<Image file="<#pdiImageDir>/negativeShift.pdf" height="24mm" />
	</AFrame>
	</Cell>
	</Row>
	<Row topMargin="1.5mm" >
	@------------------------------------------------------------
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	>Positive <pn>left</pn> and <pn>top</pn>
	</Cell>
	@------------------------------------------------------------
	<Cell textSize="7pt" fontFamily="Frutiger LT 55 Roman"
	>Negative <pn>left</pn> and <pn>top</pn>
	</Cell>
	@------------------------------------------------------------
	</Row>
	</Tbl>
	</exampleOutput>
	@------------------------------------------------------------
}{}
@--------------------------------------------------------------------------------
