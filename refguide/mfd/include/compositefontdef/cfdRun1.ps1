#--------------------------------------------------------------------------------
#- cfdRun1.ps1
#--------------------------------------------------------------------------------
#
#	called from mfd/compositefontdef
#
#--------------------------------------------------------------------------------
# $mprHomeDir	= "$env:inline/include/masterpagerule"
#--------------------------------------------------------------------------------
# if (-not (Test-Path env:mprDoubleSided)) { $env:mprDoubleSided = "Y " }
# if (-not (Test-Path env:mprExampleFile)) { $env:mprExampleFile = "masterpagerule1.pdf" }

#--------------------------------------------------------------------------------
#
#
# <runJob> macro has:
#	parameter1="sideheadsHomeDir='%mfd%/include/paradef/sideheads'"
#	parameter2="sideheadExampleFile='<#sideheadExampleFile1>'"
#

# $env:sideheadExampleFile	= "sideheadExampleFile1"
#--------------------------------------------------------------------------------


write-host	"$env:compositeFontDefIncludeDir/$env:cfdInput1.pdf"

# miramo -composer "$env:composerTag"  -showProperties N `
# 	-Opdf "$env:compositeFontDefIncludeDir/$env:cfdInput1.pdf" `
# 	"$env:compositeFontDefIncludeDir/$env:cfdInput1.mm"
miramoRP -showProperties N `
	-Opdf "$env:compositeFontDefIncludeDir/$env:cfdInput1.pdf" `
	"$env:compositeFontDefIncludeDir/$env:cfdInput1.mm"

