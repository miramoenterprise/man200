@------------------------------------------------------------
@-- frameAboveBelow.inc
@------------------------------------------------------------

@------------------------------------------------------------
<#def> pfab.totalWidth		66mm
<#def> pfab.leftMargin		7mm
<#def> pfab.topMargin		12pt
<#def> pfab.rightMargin		3mm
<#def> pfab.bottomMargin	0mm
<#def> pfab.bottomMargin	-3mm
<#def> pfab.topBorderLength	70mm
<#def> pfab.rulingLength	70mm
<#def> pfab.sideBorderLength	70mm
<#def> pfab.textSizeLeft	28mm
<#def> pfab.containerWidth	@eval(<#pfab.totalWidth>
					- <#pfab.leftMargin>
					- <#pfab.rightMargin>
					)
@------------------------------------------------------------
<#def> pfab.width		156pt
<#def> pfab.width		99pt
<#def> pfab.width		22mm
@------------------------------------------------------------
<#def> pfab.frameAboveHeight	22pt	
<#def> pfab.frameBelowHeight	12pt	
<#def> pfab.pgfBlineCount	3
<#def> pfab.pointSizeA		9pt	@--- Warning pgf
<#def> pfab.pointSizeB		9pt	@--- Normal pgf
<#def> pfab.leadingA		10pt	@--- Warning pgf
<#def> pfab.leadingB		6pt	@--- Normal pgf
<#def> pfab.spaceAboveA		18pt	@--- Warning pgf
<#def> pfab.spaceBelowA		8pt	@--- Warning pgf
<#def> pfab.spaceAboveB		15pt	@--- Normal pgf
<#def> pfab.spaceBelowB		10pt	@--- Normal pgf
<#def> pfab.lineCount		0

@------------------------------------------------------------
<@include> ${mfd}/include/textframedef/frameAboveBelow.mmp
@------------------------------------------------------------


@------------------------------------------------------------
<elementExampleTitle
	dest="TextframeDef.frameAboveBelow.start"
	>Using <#xTextFrameDef> to define above and below frames
</elementExampleTitle>

<exp>
Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" /> illustrates
defining two <#xTextFrameDef> elements, <pv>tfWarnStart</pv> and <pv>tfWarnEnd</pv> (lines
	<xlinenum id="TextFrameDef.textFrameDefsStart" /> to
	<xlinenum id="TextFrameDef.textFrameDefsEnd" />), for use as values for
the <#xP> element <pn>frameAbove</pn> and <pn>frameBelow</pn> properties.
The formatted output of the <#xTextFrameDef> elements is
indicated by <Font fontFamily="Quivira" >&#x24B6;</Font>
 and <Font fontFamily="Quivira" >&#x24B7;</Font> symbols in Figure <xparanumonly
	id="fig.TextFrameDef.frameAboveBelow1" /> (on page <xnpage
	id="fig.TextFrameDef.frameAboveBelow1" />).
Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" /> also contains the
paragraph format definitions (lines
	<xlinenum id="TextFrameDef.paraDefsStart" /> to
	<xlinenum id="TextFrameDef.paraDefsEnd" />) need to produce the
output shown in Figure <xparanumonly
	id="fig.TextFrameDef.frameAboveBelow1" />.
</exp>


@------------------------------------------------------------
<#def> circledA		@ucs2_to_utf8(24b6)
<#def> circledA		@hex2bin(<#circledA>)
<#def> circledB		@ucs2_to_utf8(24b7)
<#def> circledB		@hex2bin(<#circledB>)
@------------------------------------------------------------
<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	keepSpaces="Y"
	>
<exampleCaption
	dest="ex.TextFrameDef.frameAboveBelow1"
	>Defining a frame above/below
</exampleCaption>
<!-- TextFrameDef for paragraph <xfB>frameAbove</xfB> property    <xfQuivira><#circledA></xfQuivira>    --><xlineRef>TextFrameDef.textFrameDefsStart</xlineRef>
<TextFrameDef textFrameDef="<xfM>tfWarnStart</xfM>"  height="<#pfab.frameAboveHeight>" topMargin="4.5pt"<xlineRef>TextFrameDef.textframeA1</xlineRef>
	fillOpacity="20" fillColor="Red" <hc>penColor="Blue" penWidth="0.3pt" penStyle="dashed" </hc> ><xlineRef>TextFrameDef.textframeA2</xlineRef>
	<Line top="0mm" penColor="Blue" penWidth="1.5pt" /><xlineRef>TextFrameDef.line11</xlineRef>
	<P paraDef="pWarnHead" firstIndent="6mm" >Warning</P>
	<Line top="18pt" penColor="Red" penWidth="1.5pt" /><xlineRef>TextFrameDef.line12</xlineRef>
	@- <Image file="${images}/warningSign1.png" width="11pt" height="11pt" top="2pt" left="0" />
	<Image file="${images}/warningSign1.png" width="11pt" height="11pt" <hc>top="2pt"</hc> clip="N" />
@- 	clip="N"
</TextFrameDef>

<!-- TextFrameDef for paragraph <xfB>frameBelow</xfB> property     <xfQuivira><#circledB></xfQuivira>     -->
<TextFrameDef textFrameDef="<xfM>tfWarnEnd</xfM>" height="<#pfab.frameBelowHeight>" fillOpacity="16" <xlineRef>TextFrameDef.textframeB1</xlineRef>
	topMargin="4pt" fillColor="Green" <hc>penColor="Blue" penWidth="0.3pt" penStyle="dashed"</hc> ><xlineRef>TextFrameDef.textframeB2</xlineRef>
	@- bottomMargin="4pt" fillColor="Green" <hc>penColor="Blue" penWidth="0.3pt" penStyle="dashed"</hc> ><xlineRef>TextFrameDef.textframeB2</xlineRef>
	<Line top="0.7mm" penColor="DarkOlive" penWidth="1pt" /><xlineRef>TextFrameDef.line13</xlineRef>
	<P paraDef="pWarnEnd" firstIndent="6mm" >Warning End</P>
</TextFrameDef><xlineRef>TextFrameDef.textFrameDefsEnd</xlineRef>

<!-- Paragraph format definitions --><xlineRef>TextFrameDef.paraDefsStart</xlineRef>
<ParaDef paraDef="<xfM>pWarning</xfM>" fontFamily="Arial" fontWeight="Regular" textSize="<#pfab.pointSizeA>"
	spaceAbove="<#pfab.spaceAboveA>" spaceBelow="<#pfab.spaceBelowA>" leading="<#pfab.leadingA>"
	frameAbove="<xfM>tfWarnStart</xfM>" frameBelow="<xfM>tfWarnEnd</xfM>" <hc>textAlign="justify"</hc> />
<ParaDef paraDef="pWarnHead" fontFamily="Arial" fontWeight="Bold" textSize="12pt"  />
<ParaDef paraDef="pWarnEnd" fontFamily="Arial" fontWeight="Regular" textSize="6pt"  />
<ParaDef paraDef="normalText" fontFamily="Arial" fontWeight="Regular" textSize="<#pfab.pointSizeB>"
	spaceAbove="<#pfab.spaceAboveB>" spaceBelow="<#pfab.spaceBelowB>" leading="<#pfab.leadingB>" <hc>textAlign="justify"</hc> /><xlineRef>TextFrameDef.paraDefsEnd</xlineRef>
</exampleBlock>
@------------------------------------------------------------
<#def> textframeOutput1	<#example_output>
@------------------------------------------------------------

<exp>
In Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" />
 no <pn>left</pn> or <pn>width</pn> properties are set for
the <#xTextFrameDef> elements (lines
	<xlinenum id="TextFrameDef.textframeA1" />-<xlinenum id="TextFrameDef.textframeA2" /> and
	<xlinenum id="TextFrameDef.textframeB1" />-<xlinenum id="TextFrameDef.textframeB2" />). This is
because the default value for <pn>left</pn> is the value
of the <pn>startIndent</pn> of the associated paragraph when
the <#xTextFrameDef> is used in a paragraph frame above or below context.
The default value of <pn>width</pn> is the width of the container,
the main text flow in this case, <fI>minus</fI> the effective value of <pn>left</pn>.
Similarly
horizontal rulings within the above and below text frames are
specified using the <#xLine> element without <pn>left</pn> or <pn>width</pn> properties
(lines
	<xlinenum id="TextFrameDef.line11" />,
	<xlinenum id="TextFrameDef.line12" /> and
	<xlinenum id="TextFrameDef.line13" />) and so default to starting at
the beginning of the enclosing text frame and extending to its full width.
</exp>

<exp>
The usage of the format definitions contained in Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" /> is illustrated in Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow2" />.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.TextFrameDef.frameAboveBelow2"
	>Using the <#xTextFrameDef> elements defined in Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" />
</exampleCaption>
<P paraDef="<xfM>pWarning</xfM>" <hc>textAlign="start" hyphenate="N" </hc>>To avoid hypothermia,
wear thick clothing before abandoning ship.</P>
<P paraDef="normalText" >Normal text.<br/>More normal text.<br/>Normal text.</P>
<P paraDef="<xfM>pWarning</xfM>" startIndent="5mm" firstLineIndent="5mm" >
@- Close the cargo door and all windows before take-off.
Close the door before take-off.
</P>
</exampleBlock>


@-----------------------------------------------------------
<exp>
The output from Examples <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" /> and <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow2" /> is shown in
Figure <xparanumonly
	id="fig.TextFrameDef.frameAboveBelow1" />.
For illustration purposes the thin blue dashed rectangles in Figure <xparanumonly
	id="fig.TextFrameDef.frameAboveBelow1" /> indicate
the borders of the text frame. These border rectangles would not
be drawn with the markup shown in Example <xparanumonly
	id="ex.TextFrameDef.frameAboveBelow1" />.
</exp>

<exp>
The paragraph frame above and below text frames,
indicated by <Font fontFamily="Quivira" >&#x24B6;</Font>
 and <Font fontFamily="Quivira" >&#x24B7;</Font> symbols,
are located immediately above
and below the associated paragraph text, and their heights form
part of the paragraph's vertical extent, as illustrated
in Figure <xparanumonly
	id="fig.TextFrameDef.frameAboveBelow1" />.

	@--- xxxx
<exampleOutput
        width="<#pfab.totalWidth>"
        shadow="Y"
        @- fillOpacity="0"
        @- shadowDistance="4pt"
        @- shadowAngle="122"
        leftMargin="<#pfab.leftMargin>"
        rightMargin="<#pfab.rightMargin>"
        topMargin="<#pfab.topMargin>"
        crop="Y"
        @- belowGap="-2mm"
        >
<outputCaption
        dest="fig.TextFrameDef.frameAboveBelow1"
        > Annotated output from Examples <xparanumonly
        	id="ex.TextFrameDef.frameAboveBelow1" /> and <xparanumonly
        	id="ex.TextFrameDef.frameAboveBelow2" />
</outputCaption>
<#textframeOutput1>
<#example_output>
<pfab.mmDraw>
</exampleOutput>
</exp>

@- RD Jan 10th 2018
@- <MkDest id="TextframeDef.frameAboveBelow.end" />
<P invisible="Y" withPrevious="Y" ><MkDest id="TextframeDef.frameAboveBelow.end" /></P>
