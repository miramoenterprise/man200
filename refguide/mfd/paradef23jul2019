@------------------------------------------------------------
@-- paradef
@------------------------------------------------------------
<@include> ${CTEXT}/def.portrait.template.inc

<elementStart elementType="xml_opt_cont" elementName="ParaDef" 
	subElements="Y"
	>

<esp>
Use <#xParaDef> to create a new paragraph format,
which may be referenced by the 
<#xP>, <#xTblTitle> and <#xFNote> element
<on>paraDef</on> properties to set the appearance of paragraph
text the document.
</esp>

</elementStart>

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Paragraph format name"
	longTitle="Paragraph format name"
	dest="<#elementName>.optgroup.start"
	/>
@------------------------------------------------------------


@------------------------------------------------------------
<property
	short="paraDef"
	long="paraDef"
	value="name"
	>
<pdp>
<#Required>
<br/>
<fI>name</fI> is paragraph format name.
</pdp>
</property>
@------------------------------------------------------------


@------------------------------------------------------------
<@include> ${CTEXT}/fontPrimaryProperties.inc
@------------------------------------------------------------

<@include> ${CTEXT}/paragraphHorizontalAlignment.inc
<@include> ${CTEXT}/paragraphLineSpacing.inc

@------------------------------------------------------------
@- "Paragraph placement"
@------------------------------------------------------------
<@include> ${CTEXT}/paragraphPlacement.inc
<@include> ${CTEXT}/textColumnSpan.inc

@------------------------------------------------------------
@- "List label and autonumbering"
@------------------------------------------------------------
<@include> ${CTEXT}/p.numbering.inc

@------------------------------------------------------------
@- "Text features"
@------------------------------------------------------------
@- <@include> ${CTEXT}/fontTextFeatures.inc
<@include> ${CTEXT}/fontTextFeaturesNew.inc
<@include> ${CTEXT}/paragraphBackgroundColor.inc

@------------------------------------------------------------
@- "Line breaking"
@------------------------------------------------------------
<@include> ${CTEXT}/fontLineBreaking.inc
@- <@include> ${CTEXT}/allowLineBreaks.inc
@- <@include> ${CTEXT}/allowHyphenation.inc

@------------------------------------------------------------
@- "Horizontal text spacing"
@------------------------------------------------------------
<@include> ${CTEXT}/fontHorizontalTextSpacing.inc

@------------------------------------------------------------
@- "PDF bookmarks"
@------------------------------------------------------------
<@include> ${CTEXT}/paragraphBookmarkProperties.inc


<@include> ${CTEXT}/p.frameAboveBelow.inc
 
@------------------------------------------------------------
 

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	fontProperties="N"
	onlyOneSubElement="1"
	/>
<@include> ${ctext}/paraFrame.inc
@------------------------------------------------------------
<subElementsEnd/>
<@skipStart>
<@skipEnd>

@------------------------------------------------------------
@- Inter-paragraph spacing is normally seen as (is in file:)
@------------------------------------------------------------
<@include> ${inline}/include/p/spaceAboveBelow.inc
@------------------------------------------------------------
@------------------------------------------------------------
<#def> pf.annoFont		{fontFamily="Cambridge" fontWeight="Regular" }
<#def> pf.annoFont		{fontFamily="Cambridge Light" fontWeight="Light" }
@------------------------------------------------------------
<#def> goldWidth		6pt
<#def> denseBlueWidth		6pt
<#def> denseBlueWidth2		8pt
<#def> khakiWidth		4pt
<#def> rubyWidth		4pt
<#def> thinGoldWidth		2pt
<#def> thinBlueWidth		2pt
<#def> thinGoldWidth		0.5pt
<#def> thinBlueWidth		0.5pt
@------------------------------------------------------------
<#def> g.paraFrameFillColor	xPgfBGcolor
<#def> g.paraFrameFillTint	96
<#def> g.paraFrameFillTint	16
<#def> g.paraFrameFillTint	56
<#def> g.paraFrameFillOpacity	100
@------------------------------------------------------------
<#def> g.hydraFirstLineIndent	12pt
<#def> g.hydraStartIndent	12pt
<#def> g.hydraEndIndent		12pt
<#def> g.hydraStartMargin	6pt
<#def> g.hydraEndMargin		8pt
@------------------------------------------------------------
<ColorDef colorDef="<#g.paraFrameFillColor>" red="87%" green="79%" blue="65%" opacity="100" />
<ColorDef colorDef="xGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xKhaki"  r="45" g="42" b="14" />
<ColorDef colorDef="xDenseBlue"  r="0" g="0" b="78" />
<ColorDef colorDef="xRuby" c="0" m="91" y="100" k="51"  />
<ColorDef colorDef="xthinBlue"  r="0" g="0" b="78" />
<ColorDef colorDef="xthinGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xthinGoldDark" red="60%" green="45%" penTint="100%" penOpacity="100" />
<RuleDef ruleDef="thinGold" penWidth="<#thinGoldWidth>" penColor="xthinGold" />
<RuleDef ruleDef="thinBlue" penWidth="<#thinBlueWidth>" penColor="xthinBlue" />
<RuleDef ruleDef="Gold" penWidth="<#goldWidth>" penColor="xGold" />
<RuleDef ruleDef="DenseBlue" penWidth="<#denseBlueWidth>" penColor="xDenseBlue" />
@- <RuleDef ruleDef="DenseBlue2" penWidth="<#denseBlueWidth2>" penColor="xDenseBlue" />
<RuleDef ruleDef="Khaki" penWidth="<#khakiWidth>" penColor="xKhaki" />
<RuleDef ruleDef="Ruby" penWidth="<#rubyWidth>" penColor="xRuby" />
@------------------------------------------------------------
<#def> radiusHorizontal1	12pt
<#def> radiusHorizontal1	5mm
<#def> radiusVertical1		32pt
<#def> radiusHorizontal2	30pt
<#def> radiusVertical2		12pt
@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframe.mmp
<@include> ${mfd}/include/paradef/paraframe/paraframeRadii

<elementNote
	dest="note.TextFrameDef.paraframeRadii"
	>

<enp>
In the case the <#xparaFrame> <pn>roundedCorners</pn> property is set
to <pv>Y</pv>, the curvature of the corners may be globally
or individually
specified using the
<pn>cornerRadius</pn>,
<pn>topStartRadius</pn>,
<pn>topEndRadius</pn>,
<pn>bottomStartRadius</pn> and
<pn>bottomEndRadius</pn> properties.
Each of these properties may have one or two, whitespace
separated values specifying the corner radius or radii.
A single value results in a circular corner. If there are
two values the corner is eliptical: the first value specifies
the horizontal radius, the second value specifies the vertical
radius.

<exampleOutput
	width="88pt"
	shadow="N"
	fillOpacity="0"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0mm"	@-- <-- DO NOT CHANGE !!!
	@- leftMargin="4mm"
	bottomMargin="0.0mm"
	topMargin="0"		
	belowGap="0.0mm"
	align="end"
	>
<outputCaption
	dest="fig.paraFrameRadii"
	>
Corner radii
</outputCaption>
<#example_output>
<textContainerX/>
<textContainerX/>
</exampleOutput>
<@skipStart>
<@skipEnd>

In Figure <xparanumonly
        id="fig.paraFrameRadii" /> the dimensions 
<Font fontFamily="Cambridge Light" fontWeight="Light"
	>R<Font subscript="Y" textSize="-20%" >h</Font></Font> and
<Font fontFamily="Cambridge Light" fontWeight="Light"
	>R<Font subscript="Y" textSize="-20%" >v</Font></Font> are the horizontal and vertical
values respectively, as in:

</enp>

<exampleBlock
	tabs="6"
	>
(A)	<paraFrame ... topStartRadius="<#radiusHorizontal1> <#radiusVertical1>" fillColor="brown" ... />
(B)	<paraFrame ... topStartRadius="<#radiusHorizontal2> <#radiusVertical2>" fillColor="brown" ... />
</exampleBlock>

</elementNote>

@-===========================================================
<elementExampleTitle dest="paraFrame.ex.paraHorizontal.start"><#xparaFrame> 
lateral positioning
</elementExampleTitle>

<#def> g.paraFrameFillTint	96
<#def> g.paraFrameFillTint	86
<#def> paraFrame.fLI		23pt
<#def> paraFrame.sI		36pt
<#def> paraFrame.sM		36pt

@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframeLateralPosition1
@------------------------------------------------------------

<exp>
Figures <xparanumonly
        id="fig.paraFrameHorizontal1" /> through <xparanumonly
        id="fig.paraFrameHorizontal4" /> illustrate the lateral
positioning of the <#xparaFrame> fill area, 
<pn>startRule</pn> and 
<pn>endRule</pn> in relation to the <#xP> or <#xParaDef> <pn>firstLineIndent</pn> and
<pn>startIndent</pn> and, in the case the paragraph has a fixed-width
list label, the <#xListLabel> <pn>labelIndent</pn> (see page <xnpage
	id="ListLabel.property.labelIndent" />) properties.

<exampleOutput
	width="100pt"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0mm"	@-- <-- DO NOT CHANGE !!!
	@- leftMargin="4mm"
	bottomMargin="2.0mm"
	topMargin="2mm"
	belowGap="3.5mm"
	align="end"
	bottomMargin="1.0mm"
	topMargin="1.5mm"
	belowGap="0.5mm"
	>
<outputCaption
	dest="fig.paraFrameHorizontal1"
	>
<#xparaFrame> lateral positioning (1)
</outputCaption>
<#example_output>
<textContainer left="<#textContainerLeft>"
		@- top2="<#g.textContainerBottom>"
		top2="200"
		width="200"
		/>
</exampleOutput>

These settings, unlike the corresponding vertical settings
which affect the vertical positioning of the paragraph text,
have no effect on the lateral positioning of the paragraph text.

</exp>


<#def> paraFrame.fLI	36pt
<#def> paraFrame.sI	23pt
<#def> paraFrame.sM	36pt
@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframeLateralPosition2
@------------------------------------------------------------

<exp>
The dimensions in Figures <xparanumonly
        id="fig.paraFrameHorizontal1" /> through <xparanumonly
        id="fig.paraFrameHorizontal4" /> and Equation <xparanum
	id="equation.paraFrame.startPosition" /> are shown
in Table <xparanumonly
	id="paraFrame.referencedDimensions"/>.


<exampleOutput
	width="100pt"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0mm"	@-- <-- DO NOT CHANGE !!!
	@- leftMargin="4mm"
	bottomMargin="2.0mm"
	topMargin="2mm"
	belowGap="0.5mm"
	align="end"
	bottomMargin="1.0mm"
	topMargin="1.5mm"
	belowGap="0.5mm"
	belowGap="0.0mm"
	>
<outputCaption
	dest="fig.paraFrameHorizontal2"
	>
<#xparaFrame> lateral positioning (2)
</outputCaption>
<#example_output>
<textContainer left="<#textContainerLeft>"
		@- top2="<#g.textContainerBottom>"
		top2="200"
		width="200"
		/>
</exampleOutput>
</exp>

@------------------------------------------------------------
<xList type="text"
	labelIndent="12mm"
	labelWidth="14mm"
	indent="10mm"
	>
@------------------------------------------------------------
<listItem
	spaceBelow="0pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>pgf.fLI</Font>"
	>
<#xP>/<#xParaDef> <pn>firstLineIndent</pn>
</listItem>
@------------------------------------------------------------
<listItem
	spaceAbove="2pt"
	spaceBelow="2pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>pgf.sI</Font>"
	>
<#xP>/<#xParaDef> <pn>startIndent</pn>
</listItem>
@------------------------------------------------------------
<listItem
	spaceAbove="2pt"
	spaceBelow="2pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>pgf.eI</Font>"
	>
<#xP>/<#xParaDef> <pn>endIndent</pn>
</listItem>
@------------------------------------------------------------
<listItem
	spaceAbove="2pt"
	spaceBelow="2pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>pgf.lI</Font>"
	>
<#xP>/<#xParaDef> <#xListLabel> <pn>labelIndent</pn>
</listItem>
@------------------------------------------------------------
<listItem
	spaceAbove="2pt"
	spaceBelow="2pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>sM</Font>"
	>
<#xparaFrame> <pn>startMargin</pn>
</listItem>
@------------------------------------------------------------
<listItem
	spaceAbove="2pt"
	text="<Font fontFamily='Cambridge Light' fontWeight='Light' textSize='80%'>eM</Font>"
	>
<#xparaFrame> <pn>endMargin</pn>
</listItem>
<P paraDef="P_tableTitle"
	spaceAbove="4pt"
	spaceBelow="8pt"
	withPrevious="Y"
	textSize="8pt"
	firstLineIndent="12mm"
	><MkDest id="paraFrame.referencedDimensions"/>
<#xparaFrame> referenced dimensions
</P>

</xList>



<#def> paraFrame.fLI	27pt
<#def> paraFrame.sI	27pt
<#def> paraFrame.sM	18.5pt
<#def> paraFrame.lI	14pt


@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframeLateralPosition3
@------------------------------------------------------------
<exp>
Equation <xparanum
        id="equation.paraFrame.startPosition" /> shows the
starting point for the <#xparaFrame>.
<exampleOutput
	width="113pt"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0mm"	@-- <-- DO NOT CHANGE !!!
	@- leftMargin="4mm"
	bottomMargin="1.0mm"
	topMargin="1.5mm"
	belowGap="0.5mm"
	align="end"
	>
<outputCaption
	dest="fig.paraFrameHorizontal3"
	>
<#xparaFrame> lateral positioning (3)
</outputCaption>
<#example_output>
<textContainer left="<#textContainerLeft>"
		@- top2="<#g.textContainerBottom>"
		top2="200"
		width="200"
		/>
</exampleOutput>
</exp>

@------------------------------------------------------------
<Equation2 fli="15mm" si="25mm" dest="equation.paraFrame.startPosition"
        spaceBelow="9pt"
        >
<fI>margin start position</fI>       = min(<pn>pgf.fLI</pn>, <pn>pgf.sI</pn>, <pn>pgf.lI</pn>)
</Equation>
@------------------------------------------------------------
<exp>
In other words, the starting position of the <#xparaFrame> fill margin
is at the lowest value of <#xParaDef> <pn>firstLineIndent</pn>,
 <pn>startIndent</pn> and, if present,
the <#xParaDef> <#xListLabel> <pn>labelIndent</pn> properties.
In all cases when the <#xparaFrame> specifies a <pn>startRule</pn> or
an <pn>endRule</pn> the ruling is added to the outside
edge of the <pn>startMargin</pn> or <pn>endMargin</pn>, as illustrated
in the case of <pn>startRule</pn> in Examples <xparanumonly
        id="fig.paraFrameHorizontal1" /> (second paragraph) and <xparanumonly
        id="fig.paraFrameHorizontal2" />.
</exp>


<#def> paraFrame.fLI	0pt
<#def> paraFrame.sI	0pt
<#def> paraFrame.sM	0pt
<#def> paraFrame.eI	22pt
<#def> paraFrame.eM	15pt

@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframeLateralPosition4
@------------------------------------------------------------

<exp>
@- More text ...
The three different starting points are illustrated
via the <sq><Font fontFamily='Cambridge Light' fontWeight='Light' textSize='90%'>sM</Font></sq> dimension
in Figures <xparanumonly
        id="fig.paraFrameHorizontal1" /> through <xparanumonly
        id="fig.paraFrameHorizontal3" />.
Figure <xparanumonly
        id="fig.paraFrameHorizontal3" /> illustrates the case when
the paragraph contains a fixed-width <#xListLabel>: the margin dimension
starts from the <#xListLabel> <pn>labelIndent</pn>.
Figure <xparanumonly
        id="fig.paraFrameHorizontal4" /> illustrates the starting
point of the <#xparaFrame> end margin: this is always at the
beginning the paragraph's <pn>endIndent</pn> value (default
is 0).

<exampleOutput
	width="146pt"
	width="136pt"
	width="116pt"
	width="100pt"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0mm"	@-- <-- DO NOT CHANGE !!!
	@- leftMargin="4mm"
	bottomMargin="2.0mm"
	topMargin="2mm"
	belowGap="0.5mm"
	align="end"
	bottomMargin="1.0mm"
	topMargin="1.5mm"
	belowGap="0.5mm"
	>
<outputCaption
	dest="fig.paraFrameHorizontal4"
	>
<#xparaFrame> lateral positioning (4)
</outputCaption>
<#example_output>
<textContainer left="<#textContainerLeft>"
		@- top2="<#g.textContainerBottom>"
		top2="200"
		width="200"
		/>
</exampleOutput>
</exp>

@-===========================================================
<elementExampleTitle dest="paraFrame.ex.paraVertical.start"><#xparaFrame> 
vertical positioning
</elementExampleTitle>


@------------------------------------------------------------
<#def> goldWidth		6pt
<#def> denseBlueWidth		6pt
<#def> denseBlueWidth2		8pt
<#def> khakiWidth		4pt
<#def> rubyWidth		4pt
<#def> thinGoldWidth		2pt
<#def> thinBlueWidth		2pt
<#def> thinGoldWidth		0.5pt
<#def> thinBlueWidth		0.5pt
@------------------------------------------------------------
<#def> g.paraFrameFillColor	xPgfBGcolor
<#def> g.paraFrameFillTint	96
<#def> g.paraFrameFillOpacity	100
@------------------------------------------------------------
<#def> g.hydraFirstLineIndent	12pt
<#def> g.hydraStartIndent	12pt
<#def> g.hydraEndIndent		12pt
<#def> g.hydraStartMargin	6pt
<#def> g.hydraEndMargin		8pt
@------------------------------------------------------------

<@skipStart>
<exp>
The <#xparaFrame> sub-element enables background color fills and border
rulings to be applied to paragraph text.
Paragraphs containing the <#xparaFrame> sub-element cannot be split across
pages or columns, and so are normally heading or other short
paragraphs. Border rulings are specified by reference to
rulings define by the <#xRuleDef> element (see pages <xnpage
	id="RuleDef.element.start" /><xphyphen/><xnpage
	id="RuleDef.element.end" />).

</exp>
<@skipEnd>

<exp>
Example <xparanumonly
	id="ex.paraFrameRuleDefs" /> contains a set of
ruling format definitions used in illustrating the operation
of the <#xparaFrame> sub-element.
</exp>

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|45"
	show="Y"
	>
<exampleCaption
	dest="ex.paraFrameRuleDefs"
	><#xRuleDef>s used in Figures <xparanumonly
        id="fig.paraFrameHorizontal" /> and <xparanumonly
        id="fig.paraFrameVertical" /></exampleCaption>
<hc>
@- <ColorDef colorDef="<#g.paraFrameFillColor>" red="87%" green="79%" penTint="65%" />
<ColorDef colorDef="<#g.paraFrameFillColor>" red="87%" green="79%" blue="65%" opacity="100" />
<ColorDef colorDef="xGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xKhaki"  r="45" g="42" b="14" />
<ColorDef colorDef="xDenseBlue"  r="0" g="0" b="78" />
<ColorDef colorDef="xRuby" c="0" m="91" y="100" k="51"  />
<ColorDef colorDef="xthinBlue"  r="0" g="0" b="78" />
<ColorDef colorDef="xthinGold" red="80%" green="60%" penTint="100%" penOpacity="100" />
<ColorDef colorDef="xthinGoldDark" red="60%" green="45%" penTint="100%" penOpacity="100" />
<RuleDef ruleDef="thinGold" penWidth="<#thinGoldWidth>" penColor="xthinGold" />
<RuleDef ruleDef="thinBlue" penWidth="<#thinBlueWidth>" penColor="xthinBlue" />
<RuleDef ruleDef="DenseBlue2" penWidth="<#denseBlueWidth2>" penColor="xDenseBlue" />
</hc>
<RuleDef ruleDef="Gold" penWidth="<#goldWidth>" penColor="xGold" />
<RuleDef ruleDef="DenseBlue" penWidth="<#denseBlueWidth>" penColor="xDenseBlue" />
<RuleDef ruleDef="Khaki" penWidth="<#khakiWidth>" penColor="xKhaki" />
<RuleDef ruleDef="Ruby" penWidth="<#rubyWidth>" penColor="xRuby" />
</exampleBlock>
<#example_output>
<@skipStart>
<@skipEnd>



@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframe1
@------------------------------------------------------------

<exp>
Figure <xparanumonly
        id="fig.paraFrameHorizontal" > illustrates the operation
of the <#xparaFrame> sub-element when lateral margins and rulings
are specified, i.e. when any or all of the
<pn>startRule</pn>,
<pn>startMargin</pn>,
<pn>endMargin</pn> or
<pn>endRule</pn> properties are specified.
These settings have no effect on the positioning of the
paragraph body text.
<exampleOutput
        width="164pt"
        width="156pt"
        shadow="Y"
        shadowDistance="4pt"
        shadowAngle="122"
        leftMargin="0.5mm"
        leftMargin="0mm"
        @- leftMargin="4mm"
        bottomMargin="2.0mm"
        topMargin="0mm"
        belowGap="3.5mm"
        >
<outputCaption
        dest="fig.paraFrameHorizontal"
        >
Lateral <#xparaFrame> extents</outputCaption>
<gAnnotations/>
<textContainer left="<#g.c1>" />
<#example_output>
<textContainer left="<#textContainerLeft2>" />
</exampleOutput>

This can be seen by comparing the text positioning in <sq>Container A</sq> and
<sq>Container B</sq> in Figure <xparanumonly
        id="fig.paraFrameHorizontal" >: the background fills and
lateral margins and rulings
applied within <sq>Container B</sq> do not
affect the text positioning.
</exp>

<exp>
By default, assuming the value of either <pn>firstLineIndent</pn> or
<pn>startIndent</pn> is <pv>0</pv> and the value of <pn>firstLineIndent</pn> is <pv>0</pv>,
the fill area extends from the start to the end of the paragraph container.
When <pn>fillColor</pn> is the only setting, the paragraph fill is aligned
with the top of the first line of text and the bottom of the last line
of text, as illustrated in case <cnum>1</cnum> in Figure <xparanumonly
        id="fig.paraFrameHorizontal" >.
</exp>

@------------------------------------------------------------
<exp>
Equation <xparanum
	id="equation.paraFrame.startPosition" /> shows the relationship
between the start edge of the text container and the start of the
paragraph fill and ruling area.
</exp>

@------------------------------------------------------------
<Equation2 fli="15mm" si="25mm" dest="equation.paraFrame.startPosition"
	spaceBelow="9pt"
	>
<fI>start</fI>       = min(<pn>firstLineIndent</pn>, <pn>startIndent</pn>, <pn>labelIndent</pn>) - (<pn>startMargin</pn> + <fI>ruling width</fI>)
</Equation>
@------------------------------------------------------------

<exp>
In Equation <xparanum
	id="equation.paraFrame.startPosition" />&#x2005;<sq><fI>start</fI></sq> is
the offset of the outer starting edge of the fill area <fI>plus</fI> the
ruling width from the start side of the paragraph container.
<sq><fI>ruling width</fI></sq> is the width of the ruling specified
by the <pn>startRule</pn> property, if any.
</exp>

@------------------------------------------------------------
<exp>
Equation <xparanum
	id="equation.paraFrame.endPosition" /> shows the relationship
between the start edge of the text container and the start of the
paragraph fill and ruling area.
</exp>

@------------------------------------------------------------
<Equation2 fli="15mm" si="25mm" dest="equation.paraFrame.endPosition"
	spaceBelow="9pt"
	>
<fI>end</fI>       = <pn>endIndent</pn> - (<pn>endMargin</pn> + <fI>ruling width</fI>)
</Equation>
@------------------------------------------------------------

<exp>
In Equation <xparanum
	id="equation.paraFrame.endPosition" />&#x2005;<sq><fI>end</fI>&hairsp;</sq> is
the offset of the outer end edge of the fill area <fI>plus</fI> the
ruling width from the end side of the paragraph container.
<sq><fI>ruling width</fI></sq> is the width of the ruling specified
by the <pn>endRule</pn> property, if any.
</exp>

<exp>
The <#xparaFrame> <pn>fillColor</pn> used in
Figures <xparanumonly
        id="fig.paraFrameHorizontal" /> and <xparanumonly
        id="fig.paraFrameVertical" /> is as shown here:
<AFrame width="5mm" height="3mm"
	fillColor="<#g.paraFrameFillColor>"
	penWidth="0.5pt"
	penStyle="dotted"
	fillTint="100"
	@- align="end"
	position="inline" />.
</exp>


@------------------------------------------------------------
<@include> ${mfd}/include/paradef/paraframe/paraframe2
@------------------------------------------------------------

<exp>
Figure <xparanumonly
        id="fig.paraFrameVertical" > illustrates the operation
of the <#xparaFrame> sub-element in the case any or all of the
<pn>topRule</pn>,
<pn>topMargin</pn>,
<pn>bottomMargin</pn> or
<pn>bottomRule</pn> properties are specified.
These settings, unlike the corresponding lateral settings which
do not affect the lateral positioning of the paragraph
text, increase the vertical spacing between the applicable paragraphs.

<exampleOutput
	width="146pt"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="0.5mm"
	leftMargin="0mm"
	@- leftMargin="4mm"
	bottomMargin="2.0mm"
	topMargin="0mm"
	belowGap="3.5mm"
	align="end"
	>
<outputCaption
	dest="fig.paraFrameVertical"
	>
Additive vertical paragraph adjustment when <#xparaFrame> is used</outputCaption>
<gAnnotations/>
<#example_output>
<textContainer left="<#textContainerLeft>" top2="<#g.textContainerBottom>" />
</exampleOutput>
</exp>

<exp>
@- More text ...
</exp>


@- <MkDest id="paraFrame.ex.paraFrameUsage.end" />
@- RD Jan 10th 2018
<P invisible="Y" withPrevious="Y" ><MkDest id="paraFrame.ex.paraFrameUsage.end" /></P>
</elementExamples>


@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------

<@skipStart>
<P firstLineIndent="30mm" startIndent="34mm" endIndent="40mm" fillColor="Blue" fillTint="20%" textAlign="start" >
Verticallyx shift the location of the book mark target. Negative values move
the target upwards. Positive values move the target downwards (i.e. in the
initial page display the target will be located above the top of the page
display.)
</P>

<P firstLineIndent="30mm" startIndent="34mm" endIndent="40mm" textAlign="start" >
<Font fillColor="Red" fillTint="20%" >Verticallyz shift the location of the book mark target. Negative values move
the target upwards. Positive values move the target downwards (i.e. in the
</Font>
initial page display the target will be located above the top of the page
display.)
</P>

<P firstLineIndent="30mm" startIndent="34mm" endIndent="40mm" optimumWordSpace="600%" >
Vertically shift the location <Font optimumWordSpace="50%" >of the book mark target. Negative values move
the target upwards. Positive values move the target </Font>downwards (i.e. in the
initial page display the target will be located above the top of the page
display.)
</P>

<P firstLineIndent="30mm" startIndent="34mm" endIndent="40mm" fillColor="Blue" fillTint="20%" >
Vertically shift the location of the book mark target. Negative values move
the target upwards. Positive values move the target downwards (i.e. in the
initial page display the target will be located above the top of the page
display.)
</P>
<@skipEnd>
