@------------------------------------------------------------
@-- footnotedef
@------------------------------------------------------------

<elementStart elementType="xml_mand_atts" elementName="FootnoteDef" 
	subElements="Y"
	>

<esp>
The <#xFootnoteDef> element is used to specify the formatting of
document and table footnotes.
</esp>

<esp>
Any number of <#xFootnoteDef> format definition elements may be
included within the input. Only one may be selected as the format
applicable to document (non-table) footnotes.
</esp>

<esp>
The <#xFootnoteDef> element may be
referenced <fI>via</fI> either the <#xDocDef> or <#xTblDef> elements'
 <pn>fNoteDef</pn> properties. See pages <xnpage
	id="DocDef.property.fNoteDef" /> and <xnpage
	id="TblDef.property.fNoteDef" />.
The <#xFootnoteDef> element is <fI>not</fI> applicable to
the <#xFootnote> element.
</esp>

</elementStart>


@------------------------------------------------------------
<#def> textDecorationProperties	{<fI>text decoration properties</fI> (see
Note <xparanumonly
	id="note.FootnoteDef.textDecorationProperties" /> on page <xnpage
	id="note.FootnoteDef.textDecorationProperties" />)<br/>}
@------------------------------------------------------------

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	title="Footnote format name"
	dest="<#elementName>.propertyGroup.formatName"
	>
</propertyGroup>
@------------------------------------------------------------

<property
	name="footnoteDef"
	value="name"
	>
<pdp>
<fI>Required</fI><br/>
<fI>name</fI> is footnote format name.
</pdp>

</property>

@------------------------------------------------------------
<propertyGroup
	title="Footnote anchor properties"
	dest="<#elementName>.propertyGroup.anchorProperties"
	>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="anchorSize"
	value="percent | dim"
	>
<pdp>
Text size of the footnote anchor.
If a <fI>percent</fI> value, then the size is relative to
the size of the associated body text.
</pdp>

<propertyDefault>67%</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="anchorShift"
	value="percent | dim"
	>
<pdp>
Baseline shift of the footnote anchor.
If a <fI>percent</fI> value, then the shift is relative to
the size of the associated body text.
</pdp>

<propertyDefault>67%</propertyDefault>
</property>

@------------------------------------------------------------
<property
	name="anchorPosition"
	value="superscript | subscript | baseline"
	>
<pdp>
Vertical position of the footnote anchor.
</pdp>

<propertyDefault>superscript</propertyDefault>
</property>

@------------------------------------------------------------
<propertyGroup
	title="Footnote gap"
	dest="<#elementName>.propertyGroup.footnoteGap"
	>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property name="minimumGap" value="name" >
<pdp>
In the general case: minimum distance between the bottom of the body text
and the footnote ruling when the <pn>rule</pn> property is set to <pv>Y</pv>;
otherwise the minimum distance between the body text and the first footnote.
</pdp>

<pdp>
In the case of a table footnote, the gap between the preceding row and
the first footnote.
</pdp>

<propertyDefault>8pt</propertyDefault>

</property>


@------------------------------------------------------------
<propertyGroup
	@- shortTitle="Numbering"
	@- longTitle="Numbering"
	title="Footnote numbering"
	dest="<#elementName>.propertyGroup.numbering"
	>
</propertyGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<property short="numberStyle" long="numberStyle" value="custom | key" >

<pdp>
Footnote numbering mode.
</pdp>

<pdp>
If <on>numberStyle</on> is set to <ov>custom</ov>, the numbering
sequence is as defined by the <on>numberSequence</on> property.
Alternatively the <on>numberStyle</on> may be set to one of the
following <fI>key</fI> values.
</pdp>

@------------------------------------------------------------
@- Defined in ${control}/numberStyleValues.mmp
@------------------------------------------------------------
@- <#numberStyleValues>
<numberStyleValuesFootnote startIndent="5mm" />
@------------------------------------------------------------


<propertyDefault>westernArabic</propertyDefault>
</property>

<#def> defaultCustomSequence {
	&#x2A;|			@- asterix
	&#x2020;|		@- dagger
	&#x2021;|		@- double dagger
	&#xA7;|			@- section sign
	&#xB6;|			@- pilcrow sign
	&#x2A;&#x2A;|		@- two asterixs
	&#x2020;&#x2020;|	@- two daggers
	&#x2021;&#x2021;	@- two double daggers
}
<#def> defaultCustomSequence	@trim(<#defaultCustomSequence>)
<#def> defaultCustomSequence	@gsub({<#defaultCustomSequence>}, { |	|<#nl>}, {})
<#def> defaultCustomSequence	@gsub({<#defaultCustomSequence>}, {\|}, {<Font ff="Cambridge Light" fw="Light" > | </Font>})
<#def> defaultCustomSequence	{<Font ff="Times New Roman" ><#defaultCustomSequence></Font>}
@- <@write> error {ddxv-----<#defaultCustomSequence>---}
@------------------------------------------------------------
<property short="numberSequence" long="numberSequence" value="list" >

<pdp>
Ordered, bar or comma separated list of footnote indicators.
</pdp>

<pdp>
The <on>numberSequence</on> property is ignored unless
the <on>numberStyle</on> property is set to <ov>custom</ov>
</pdp>

<pdp>
E.g.
</pdp>

<exampleBlock>
<xfI> (1)</xfI>		numberSequence="*|**|***"

<xfI> (2)</xfI>			numberSequence=" *
			|**
			| ***"
</exampleBlock>
<pdp>
<fI>(1)</fI> and <fI>(2)</fI> above are equivalent since whitespace (binary spaces,
tabs and newlines) are ignored in list values.
</pdp>

@- <propertyDefault><Font ff="Times New Roman" >*| �| �| �| ?| �</Font></propertyDefault>
@- <propertyDefault><Font ff="Times New Roman"
	@- >&#x2A;|&#x2020;|&#x2021;|&#xA7;|&#xB6;|&#x2225;|&#x2222;</Font></propertyDefault>
	@- >&#x2A;|&#x2020;|&#x2021;|&#xA7;|&#xB6;|&#x2A;&#x2A;|&#x2020;&#x2020;|&#x2021;&#x2021;</Font></propertyDefault>
<propertyDefault>"<#defaultCustomSequence>"</propertyDefault>
</property>


@------------------------------------------------------------
<property short="numberRestart" long="numberRestart" value="perDocument | perPage" >

<pdp>
Continue numbering through the document or restart numbering on every page.
</pdp>

</property>

@-===========================================================
<propertyGroup
	title="Footnote ruling"
	dest="<#elementName>.propertyGroup.footnoteRule"
	>
</propertyGroup>

@------------------------------------------------------------
<property name="rule" value="Y | N" >

<pdp>
Include a horizontal rule above the footnotes. The rule graphical and positioing
properties are specified using the <#xFootnoteSeparator> sub-element (see page <xnpage
	id="FootnoteDef.subelement.FootnoteSeparator" />).
</pdp>

<propertyDefault>Y</propertyDefault>
</property>


@------------------------------------------------------------
<propertyGroup
	title="Footnote instance formatting"
	dest="<#elementName>.propertyGroup.instance_formatting"
	>
</propertyGroup>
@------------------------------------------------------------


@------------------------------------------------------------
<@include> ${ctext}/fontPrimaryProperties.inc
<@include> ${CTEXT}/paragraphHorizontalAlignment.inc
<@include> ${CTEXT}/paragraphLineSpacing.inc
@- <@include> ${CTEXT}/fontTextFeaturesNew.inc
@- <@include> ${CTEXT}/paragraphBackgroundColor.inc
<@include> ${CTEXT}/fontLineBreaking.inc
<@include> ${CTEXT}/fontHorizontalTextSpacing.inc
@------------------------------------------------------------


@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

<#def> bbr	{<br/>}
@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	onlyOneSubElement="0"
	fontProperties="N"
	/>
@------------------------------------------------------------

@------------------------------------------------------------
<subElementGroup>
This is some some subElement group text.
</subElementGroup>
@------------------------------------------------------------
<subElement
	name="anchorStyle"
        dest="FootnoteDef.subElement.anchorStyle"
	@- topRuleType="Medium"
        >
<subElementProperties>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>percent</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Specify font properties for footnote anchor.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="anchorPrefix"
        dest="FootnoteDef.subElement.anchorPrefix"
	@- topRule="N"
        >
<subElementProperties>
text="string"<br/>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>percent</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Specify text and font properties for footnote anchor prefix.
The <#xanchorPrefix> <pn>textSize</pn> may only be specified as
a <fI>percent</fI> value relative to the size of
the <#xFootnoteDef> anchor text size property.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="anchorSuffix"
	fontProperties="N"
        dest="FootnoteDef.subElement.anchorSuffix"
	@- topRule="N"
        >
<subElementProperties>
text="string"<br/>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>percent</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Specify text and font properties for footnote anchor suffix.
Ignored if the <pn>text</pn> property is empty.
All values apart from the <pn>text</pn> property are
inherited from the anchor sub-element.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="anchorSeparator"
	fontProperties="N"
        dest="FootnoteDef.subElement.anchorSeparator"
	@- topRule="N"
        >
<subElementProperties>
text="string"<br/>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>dim</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Footnote anchor separator text. Applicable only in the case the input contains
two or more contiguous (separated by whitespace only) <#xFootnote> instances.
</bbp>

<bbp>
By default the footnote separator text uses the style and format specified by the anchor
sub-element.
</bbp>

<bbp>
Default for <pn>text</pn> property is comma, followed by non-breaking space.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="labelStyle"
	fontProperties="N"
        dest="FootnoteDef.subElement.labelStyle"
	@- topRuleType="Medium"
        >
<subElementProperties>
type="runin | fixed"<#bbr>
labelAlign="start | end"<#bbr>
labelWidth="<fI>dim</fI>"<#bbr>
labelIndent="<fI>dim</fI>"<#bbr>
<#bbr>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>dim</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Footnote label format. The label is autonumbered
in accordance with the value specified by
the <#xFootnoteDef> <pn>numberStyle</pn> property.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="labelPrefix"
	fontProperties="N"
        dest="FootnoteDef.subElement.labelPrefix"
	@- topRule="N"
        >
<subElementProperties>
text="<fI>string</fI>"<br/>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>dim</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Footnote label prefix text and style.
</bbp>

</subElement>

@------------------------------------------------------------
<subElement
	name="labelSuffix"
	fontProperties="N"
        dest="FootnoteDef.subElement.labelSuffix"
	@- topRule="N"
        >
<subElementProperties>
text="<fI>string</fI>"<br/>
fontFamily="<fI>name</fI>"<#bbr>
fontWeight="<fI>name</fI>"<#bbr>
fontAngle="<fI>name</fI>"<#bbr>
textSize="<fI>dim</fI>"<#bbr>
textColor="<fI>name</fI>"<#bbr>
<#textDecorationProperties>
</subElementProperties>

<bbp>
Footnote label suffix text and style.
</bbp>

</subElement>

<@skipStart>
@------------------------------------------------------------
<subElement
	name="FootnoteSeparator"
	fontProperties="N"
        dest="FootnoteDef.subElement.FootnoteSeparator"
        >
<subElementProperties>
minimumGap="<fI>dim</fI>"
ruleGap="<fI>dim</fI>"
ruleStart="<fI>dim</fI>"
ruleLength="<fI>dim</fI> | <fI>percent</fI>"
<fI>penProperties</fI>
</subElementProperties>

@- keyHeading
@- descriptionHeading
@- keyWidth
@- descriptionWidth


<bbp>
Specify footnote gap and ruling properties. The ruling properties are
ignored if the <pn>rule</pn> property (see page <xnpage
	id="FootnoteDef.property.rule" />) is set to <pv>N</pv>.
</bbp>


<propertyValueKeyList
	@- startIndent="14mm"
	startIndent="9mm"
        keyWidth="18mm"
        keyHeading="Property"
        descriptionHeading="Description"
        >

<vk key="ruleGap" >
Distance between the footnote separator ruling and the top of the first
footnote.
<br/>
<fI>Default:</fI> <ov>3pt</ov>
</vk>

<vk key="ruleStart" >
Indent of footnote separator ruling from start
side of text frame.
<br/>
<fI>Default:</fI> <ov>0</ov>
</vk>

<vk key="ruleLength" >
Length of footnote separator ruling.
<br/>
<xidxend id="aaafnotedef" />
<fI>Default:</fI> <ov>80%</ov>
</vk>


<vk key="penProperties" italic="Y" >
<pn>penWidth</pn> (<fI>dim</fI>),
<pn>penColor</pn> (<fI>name</fI>),
<pn>penTint</pn> (<fI>percent</fI>) and
<pn>penStyle</pn> (solid | dashed | dotted).
<IX textColor="Red">AAvmAAAAAAAAAA</IX>
</vk>

</propertyValueKeyList>

@- An end pgf property is necessary, else the sub-element does not appear.
@- Weird BUG in macros
<bbp>
Use <pn>penWidth</pn> to specify the width of the footnote separator
ruling.
</bbp>

</subElement>

<@skipEnd>
@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------

@------------------------------------------------------------
@- NOTES
@------------------------------------------------------------

<elementNote
        dest="note.FootnoteDef.textDecorationProperties"
        >
<enp>
@- Figure <xparanumonly
@-         id="fig.FootnoteDef.additionalProperties" /> lists the
The following is the list of
additional properties supported on the anchor and label sub-elements.
</enp>

<exampleBlock
	tabs="5|10|15"
	lineNumbers="N"
	type="figure"
	>
@- <exampleCaption
@- 	dest="fig.FootnoteDef.additionalProperties"
@- 	>Properties common to sub-elements
@- </exampleCaption>
	textUnderline
		underlineOffset
		underlineThickness
		underlineGap
		underlineColor
	textOverline
	strikeThrough
</exampleBlock>

</elementNote>



@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
