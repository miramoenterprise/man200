@------------------------------------------------------------
@-- currentPageNumber
@------------------------------------------------------------
<elementStart elementType="atts_only" elementName="currentPageNumber" >

<esp>
The <#xcurrentPageNumber> element 
may be included within a <#xP> element to display the
current page number. Normally the <#xcurrentPageNumber> element is
used within a background <#xTextFrame> within a <#xPageDef> to
include the current page number in header or footer running text.
</esp>

<esp>
See also: <#xlastPageNumber> on page <xnpage
	id="lastPageNumber.element.start" />.
</esp>

</elementStart>

@------------------------------------------------------------


@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Font properties"
	longTitle="Font properties"
	dest="<#elementName>.propertyGroup.fontProperties"
	>
</propertyGroup>
@------------------------------------------------------------
<fontProperties fontFormat="Y" >
<fI>fontProperties</fI> override the default page number text
properties, as inherited from the enclosing paragraph format.
</fontProperties>
@------------------------------------------------------------



@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.<#elementName>.mainFlowExample" /> illustrates
using the <#xcurrentPageNumber> element.
</exp>

<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.<#elementName>.mainFlowExample"
	>Using <#xcurrentPageNumber> within the document main flow
</exampleCaption>
<P<hc> textSize="8pt"</hc>>The current page number is: <currentPageNumber/></P>
<P>The current page number is: <currentPageNumber fontDef="F_Red" /></P>
<P>This page number is: <currentPageNumber fontDef="F_Red" fontAngle="Italic"  /></P>
</exampleBlock>

<exp>
<exampleOutput
	width="52mm"
	shadow="Y"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="3mm"
	belowGap="-4mm"
	>
<outputCaption
	dest="fig.ListLabel.autonumbered2"
	> Output from <xparalabel
	id="ex.<#elementName>.mainFlowExample"
	/>.</outputCaption>
<#example_output>
</exampleOutput>
</exp>

<exp>
Input lines 1 and 2 in Example <xparanumonly
	id="ex.<#elementName>.mainFlowExample" /> assume that the
input also contains the following <#xFontDef> format definition:
</exp>
<exampleBlock
	leftIndent="6mm"
	>
	<FontDef fontDef="F_Red" textColor="Red" />
</exampleBlock>

<exp>
Example <xparanumonly
	id="ex.<#elementName>.backgroundTextFrame" /> illustrates
using the <#xcurrentPageNumber> and <#xlastPageNumber> elements in
a background text frame on a master page.
</exp>

<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.<#elementName>.backgroundTextFrame"
	>Using <#xcurrentPageNumber> within a background text frame</exampleCaption>
<PageDef pageDef="Right" >
<!-- ============================ -->
<!-- Page footer contains page number <xfI>n</xfI> of <xfI>m</xfI> -->
<!-- ============================ -->
<TextFrame type="background" L="25.0mm" T="219mm" W="125.0mm" H="4.0mm" >
<P fmt="P_pageNum" align="end" endIndent="0"
		>mmComposer Reference Guide <currentPageNumber
			textColor="Blue" /> of <lastPageNumber/></P>
</TextFrame>
<exampleTextOnly> <xfI>main flow and other background text flow formats omitted</xfI></exampleTextOnly>
</PageDef>
</exampleBlock>

<exp>
The markup shown in
Example <xparanumonly
	id="ex.<#elementName>.backgroundTextFrame" /> produces output similar
to that shown in the running footer of this page.
</exp>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
