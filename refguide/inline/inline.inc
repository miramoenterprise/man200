@-----------------------------------------------------------
@- inline.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<chapterStart
	dest="inlineMarkupElements"
	pgfSuffix="ExA"
	>Inline markup elements
</chapterStart>
<#def> inINLINE		1
<#def> elementType	inline
@-----------------------------------------------------------
<@include>	${inline}/introduction
<Section allPages="MP_referenceGuide" />
@- <@include>	${inline}/font
@-- <@include>	${inline}/cell
@-- <@include>	${inline}/row
@-----------------------------------------------------------
<@include>	${inline}/aframe
<@include>	${inline}/atextframe
<@include>	${inline}/cell
<@include>	${inline}/changebarbegin
<@include>	${inline}/changebarend
<@include>	${inline}/chapter
@- ---- <@include>	${inline}/columns
<@include>	${inline}/contents
<@include>	${inline}/contentsinclude
<@include>	${inline}/currentPageNumber
<@include>	${inline}/date
<@include>	${inline}/equation
<@include>	${inline}/fnote
<@include>	${inline}/font
<@include>	${inline}/frame
<@include>	${inline}/hypercmd
<@include>	${inline}/image
<@include>	${inline}/imagedef
<@include>	${inline}/index
<@include>	${inline}/ix
<@include>	${inline}/lastPageNumber
<@include>	${inline}/line
<@include>	${inline}/listlabel
<@include>	${inline}/mkdest
@- <@include>	${inline}/mmdraw	@--- 
<@include>	${inline}/objecttitle
<@include>	${inline}/p
<@include>	${inline}/paragroup
<@include>	${inline}/pdfimport
<@include>	${inline}/row
<@include>	${inline}/runningtext
<@include>	${inline}/runningtextmarker
<@include>	${inline}/section
<@include>	${inline}/tbl
@- <@include>	${inline}/tblcolumn
<@include>	${inline}/tblcontinuation
<@include>	${inline}/tbltitle
<@include>	${inline}/textframe
<@include>	${inline}/var
<@include>	${inline}/xref
@-----------------------------------------------------------
@- <@skipStart>
<@skipStart>
<@skipEnd>
<#def> inINLINE	0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
