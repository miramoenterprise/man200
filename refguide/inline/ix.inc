@-----------------------------------------------------------
@- ix.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
@- <chapterStart
@- 	dest="inlineMarkupElements"
@- 	pgfSuffix="ExA"
@- 	>Inline markup elements
@- </chapterStart>
<#def> inINLINE		1
<#def> elementType	inline
@-----------------------------------------------------------
<Section allPages="MP_referenceGuide" />
@-----------------------------------------------------------
<@include>	${inline}/ix
@-----------------------------------------------------------
<#def> inINLINE		0
@-----------------------------------------------------------
@- <chapterEnd/>
@-----------------------------------------------------------
