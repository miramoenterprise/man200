@------------------------------------------------------------
@-- runningtext
@------------------------------------------------------------

<elementStart elementType="xml_mand_atts" elementName="RunningText" 
	subElements="Y"
	>

<esp>
Use the <#xRunningText> element within a
background <#xTextFrame>, or a <#xTextFrameDef> element
with it's <on>type</on> property set to <ov>background</ov> (see pages <xnpage
	id="TextFrame.element.start" 
	/> and <xnpage
	id="TextFrameDef.element.start" 
	/>), to include running text derived
from paragraph text of the first- or last-encountered paragraph
text within the body (foreground) text flow on the current page.
That is, one or all of: the paragraph
text referenced using <#xparagraphText>, the paragraph
label text referenced using <#xparagraphLabel> or the paragraph autonumber counter
values (and any text between them) referenced using <#xparagraphNumber> sub-elements.
</esp>

<esp>
The <#xRunningText> element may also be used to
include text contained within hidden <#xRunningTextMarker> elements
(see pages <xnpage
	id="RunningTextMarker.element.start" /><xphyphen/><xnpage
	id="RunningTextMarker.element.end" />) within
the body text flow.
</esp>

<esp>
Each of the <#xparagraphText>, <#xparagraphLabel> and <#xparagraphNumber> sub-elements
supports a <on>select</on> property to specify whether the text of the first
or last occurrence paragraph should be included in the running text.
</esp>

</elementStart>

@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------


@------------------------------------------------------------
<propertyGroup
	shortTitle="Define running text for background text frame"
	@- longTitle="Define running text for background text frame"
	dest="<#elementName>.propertyGroup.start"
	/>


@------------------------------------------------------------
<property
	long="runningTextDef"
	value="name"
	>
<pdp>
<fI>name</fI> is the name of a running text format defined either
in a template or by using the <#xRunningTextDef> element (see pages
<xnpage
	id="RunningTextDef.element.start"
	/><xphyphen/><xnpage
	id="RunningTextDef.element.end"
	/>).
</pdp>

<pdp>
The <pn>runningTextDef</pn> property is <fI>optional</fI>. The sub-element
contents of running text may be specified <fI>either</fI> inline within
a <#xRunningText> element <fI>or</fI> by using
the <pn>runningTextDef</pn> property to reference a running text
format definition, <fI>not</fI> both.
</pdp>

<pdp/>

</property>


@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

@------------------------------------------------------------
<@xmacro> runningtextdefSubElementText {
	<#def> rtfbb.text1	{}
	<#def> rtfbb.text2	{}
	<#def> rtfbb.text3	{}
	@----------------------------------------------------
	@xgetvals(rtfbb.)
	@----------------------------------------------------
<bbp>
Expands to the
<#rtfbb.text1>
of the first paragraph
on the current page which has one of the paragraph formats listed
by the <on>names</on> property.
</bbp>

<bbp>
<fI>namelist</fI> is a comma or bar separated list of one or more paragraph 
formats defined using the <#xParaDef> element (see pages <xnpage
	id="ParaDef.element.start" /><xphyphen/><xnpage
	id="ParaDef.element.end" />).
</bbp>

<bbp>
If there is no paragraph on the current page with a format name
listed by the <on>names</on> property, then the
<#rtfbb.text3>
of the nearest <fI>preceding</fI> paragraph that has one of the paragraph formats listed
by the <on>names</on> property is used.
</bbp>

<bbp>
If the <on>select</on> property is set to <ov>last</ov>, then the
&lt;<#rtfbb.text2> ... /&gt; sub-element expands to the
<#rtfbb.text3>
the <fI>last</fI> paragraph on the page 
that has one of the paragraph formats listed by the <on>names</on> property, or, if
there is no such paragraph, to the
<#rtfbb.text3>
of the nearest <fI>following</fI> paragraph that has one of the paragraph formats listed
by the <on>names</on> property.
</bbp>

<bbp>
If the
&lt;<#rtfbb.text2> ... /&gt; <on>names</on> property contains more
than one <fI>name</fI> value, then these values are treated as ORs.
</bbp>

<bbp>
The <on>names</on> property is <fI>required</fI>.
</bbp>
}{}
@------------------------------------------------------------

@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart><br/>(The following sub-elements are ignored if
the <pn textSize="7.5pt" >runningTextDef</pn> property is used.)
</subElementsStart>

@------------------------------------------------------------
<@include> ${CTEXT}/subElement.deftext.inc


@------------------------------------------------------------
<subElement
	name="Font"
	>
<bbp>
The <#xFont> element may be used within <#xRunningText>.
</bbp>
</subElement>

@------------------------------------------------------------
<subElement
	name="paragraphText"
	>
<subElementProperties>
@- names="namelist" [select="first | last"]<br/>
<subProperty name="names" value= "namelist" desc="yyyy" />
[ <subProperty name="select" value= "first | last" desc="yyyy" /> ]
<br/>
@- keepFormatting = "N | Y"
<subProperty name="keepFormatting" value= "N | Y" desc="yyyy" /> ]
</subElementProperties>

<runningtextdefSubElementText
	text1="paragraph text (excluding label text and autonumber text)"
	text2="paragraphText"
	text3="paragraphText"
	/>

<keepFormatting/>	@- Defined in ${control}/boilerPlateText.mmp

</subElement>

@------------------------------------------------------------
@- <paralabel>
@------------------------------------------------------------
<subElement
	name="paragraphLabel"
	>
<subElementProperties>
@- names="namelist" select="first | last"
<subProperty name="names" value= "namelist" desc="yyyy" />
[ <subProperty name="select" value= "first | last" desc="yyyy" /> ]
</subElementProperties>

<runningtextdefSubElementText
	text1='label text (<fI>including</fI> autonumber text, if any)'
	text2="paragraphLabel"
	text3="text"
	text4="paragraphLabel"
	/>
</subElement>


@------------------------------------------------------------
@- <paranumonly>
@------------------------------------------------------------
<subElement
	name="paragraphNumber"
	>
<subElementProperties>
@- names="namelist" select="first | last"
<subProperty name="names" value= "namelist" desc="yyyy" />
[ <subProperty name="select" value= "first | last" desc="yyyy" /> ]
</subElementProperties>

<runningtextdefSubElementText
	text1='autonumber text only (i.e. <fI>excluding</fI> the
paragraph body text and label text that is not between the first and
last &lt;counter/&gt; elements)'
	text2="paragraphNumber"
	text3='<fI>autonumber text</fI> '
	text4="paragraphNumber"
	/>
</subElement>


@------------------------------------------------------------
<subElement
	name="marker"
	>
<subElementProperties>
@- names="namelist" select="first | last"
<subProperty name="names" value= "namelist" desc="yyyy" />
[ <subProperty name="select" value= "first | last" desc="yyyy" /> ]
</subElementProperties>

<bbp>
Expands to the text included within the first instance
of a corresponding (name="<fI>string</fI>" )
&lt;RunningTextMarker&gt; element on the current page.
</bbp>

<bbp>
If there is no instance of a corresponding
<#xRunningTextMarker> element on the current page, then the text from
the nearest preceding or the nearest following, depending on the
setting of the <on>select</on> property, <#xRunningTextMarker> element
is used.
</bbp>

<bbp>
@- The &lt;RunningTextMarker ... &gt; element is described on page 
The <#xRunningTextMarker> element is described on page 
<xnpage id="RunningTextMarker.element.start" />.
</bbp>

</subElement>

<@skipStart>
<@skipEnd>

@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------

@------------------------------------------------------------
@- <|propertiesEnd>
@------------------------------------------------------------

@------------------------------------------------------------
@- EXAMPLES
@------------------------------------------------------------

@------------------------------------------------------------
<elementExampleTitle
	dest="RunningText.paragraph_based_running_text"
	>Pararagraph-based running text</elementExampleTitle>
<exp>
Example <xparanumonly
	id="ex.paragraph_based_running_text" /> shows how
to define a background text frame that picks up paragraph
text, specifically the selected paragraph's label text, from the body text flow.
</exp>

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20|25"
	>
<exampleCaption
	dest="ex.paragraph_based_running_text"
	>
Pararagraph-based running text
</exampleCaption>
<TextFrame type="background"
	left="25mm" top="20mm" width="160mm" height="7mm" ><xlineRef>RunningText.ex.location</xlineRef>
	<Line top="4.5mm" /> <!-- Ruling across frame, 4.5mm down (see page <xPageNum>Line.element.start</xPageNum>) -->
	<P paraDef="P_chapterRunningHead"><RunningText><paragraphLabel textColor="DarkBlue"<xlineRef>RunningText.ex.running_text</xlineRef>
		names="P_chapterTitle|P_chapterTitle.ExA|P_appendixTitle" /><xlineRef>RunningText.ex.names</xlineRef>
		</RunningText>
	</P>
</TextFrame>
</exampleBlock>

<exp>
@- The <#xTextFrameDef> element 
Example <xparanumonly
	id="ex.paragraph_based_running_text" /> shows how
a background text frame may be positioned within
a containing <#xPageDef> element (line <xlinenum id="RunningText.ex.location" />) and
how to include a <#xRunningText> element for picking up a paragraph text component
from the body text flow, in this case the paragraph label
text via the <#xparagraphLabel> element (line <xlinenum id="RunningText.ex.running_text" />).
The <on>names</on> property (line <xlinenum id="RunningText.ex.names" />) contains
the set of paragraphs that may be included. It is always the contents of
the first (or last) paragraph format in the <on>names</on> set that is encountered that is included.
The order of the paragraphs in the <on>names</on> property is insignificant.
</exp>

<exp>
@- Example <xparanumonly
@- 	id="ex.paragraph_based_running_text" /> shows how to the list
@- label component of a paragraph in a background text frame.
To include
the paragraph text itself as running
text in a background text frame add in the <#xparatext> element, as illustrated
in Example <xparanumonly
	id="ex.paratext_running_text" />.
</exp>

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20|25"
	>
<exampleCaption
	dest="ex.paratext_running_text"
	>
Including paragraph text in running header
</exampleCaption>
	<RunningText>
		<paragraphText names="P_chapterTitle|P_chapterTitle.ExA|P_appendixTitle" /><xlineRef>RunningText.ex.paratext_names</xlineRef>
	</RunningText>
</exampleBlock>

<exp>
To include the paragraph label <fI>and</fI> the paragraph text, line <xlinenum
	id="RunningText.ex.paratext_names" /> in Example <xparanumonly
	id="ex.paratext_running_text" /> may be added to Example <xparanumonly
	id="ex.paragraph_based_running_text" /> after line <xlinenum
	id="RunningText.ex.names" />.
</exp>

@------------------------------------------------------------
<elementExampleTitle
	dest="RunningText.marker_based_running_text"
	>Marker-based running text</elementExampleTitle>
<exp>
Example <xparanumonly
	id="ex.marker_based_running_text" /> shows how
to define a background text frame that picks up hidden marker
text, included using the <#xRunningTextMarker> element (see pages <xnpage
	id="RunningTextMarker.element.start" /><xphyphen><xnpage
	id="RunningTextMarker.element.end" />), from the body text flow.
</exp>

<exampleBlock
	lineNumbers="Y"
	tabs="5|10|15|20|25"
	>
<exampleCaption
	dest="ex.marker_based_running_text"
	>
Pararagraph-based running text
</exampleCaption>
<TextFrame type="background"
	left="25mm" top="30mm" width="160mm" height="7mm" ><xlineRef>RunningText.ex.location2</xlineRef>
	<P paraDef="P_elementRunningHead"><RunningText>
			<marker names="elementNameMarker" /><xlineRef>RunningText.ex.marker_running_text</xlineRef>
		</RunningText>
	</P>
</TextFrame>
</exampleBlock>

<exp>
The value of the <#xmarker> sub-element <on>names</on> property,
 <qm>elementNameMarker</qm>, in Example <xparanumonly
	id="ex.marker_based_running_text" /> (line <xlinenum
	id="RunningText.ex.marker_running_text" />) is
arbitrary. It is the means of identifying which <#xRunningTextMarker> element's
contents should be included in the background text frame, i.e. the contents of the
first or preceding (or last or next nearest later) <#xRunningTextMarker> element that
has a <on>name</on> value of <ov>elementNameMarker</ov>.
</exp>



<@skipStart>
@------------------------------------------------------------
<elementNote>

<xlineRef>ListLabel.autonumbered1.pgfStart</xlineRef>
<xlinenum id="xxxx" />

<enp>
The default background variable definitions
@- are shown in the section
@- <|sectionXRef> RunningText.defaults
@-  on page
@- <xnpage id="RunningText.defaults" /> and
correspond to the
FrameMaker system variable names <#osq>Running H/F 1<#csq>,
<#osq>Running H/F 2<#csq>,
<#osq>Running H/F 3<#csq> and <#osq>Running H/F 4<#csq>.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote>
<enp>
If there are two or more <#xRunningText> elements with the
same format number, the values of properties included in later
instances will overwrite the values defined
in preceding instances.
</enp>

</elementNote>


@------------------------------------------------------------
@- EXAMPLES
@------------------------------------------------------------

<exp>
The following illustrates setting up a dictionary
style running header/footer which picks up the
text of the first and last occurrence of a <#osq>EntryName<#csq>
paragraph on each page. 
<vidx id="running headers/footers||dictionary style" />
</exp>

<exp>
Firstly, the 
type 1 and 2 <#xRunningText> background <#osq>running text<#csq> variable
formats are defined, as shown in Example <xparanumonly
	id="ex.running_header_footer_variable_formats" />.
</exp>


@------------------------------------------------------------
<exampleBlock
	>
<exampleCaption
	dest="ex.running_header_footer_variable_formats"
	>
Running header/footer variable formats
</exampleCaption>
<RunningText runningTextDef="1"><paragraphText paraDef="EntryName" select="first"/></RunningText>
<RunningText runningTextDef="2"><paragraphText paraDef="EntryName" select="last"/></RunningText>
</exampleBlock>


@------------------------------------------------------------
<exp>
This is equivalent to using FrameMaker to create a 
template where the <#osq>Running H/F 1<#csq> system variable 
is defined as:
</exp>

<exampleBlock
	>
&lt;$paragraphText[EntryName]&gt;
</exampleBlock>



<exp>
and the <#osq>Running H/F 2<#csq> system variable is defined as:
</exp>

@------------------------------------------------------------
<exampleBlock
	>
&lt;$paragraphText[+,EntryName]&gt;
</exampleBlock>


<exp>
Secondly, a reference to these two background text variables 
is inserted in a background (untagged) text frame on
the default <#osq>Right<#csq> master page using the <#xBGVar> element
within a <#xPageDef> <#xTextFrame> element,
as shown in Example
<xparanumonly id="ex.dictionary_style_running_headers_and_footers_1" />.
</exp>

@------------------------------------------------------------
<exampleBlock
	>
<exampleCaption
	dest="ex.dictionary_style_running_headers_and_footers_1"
	>
Dictionary style running headers and footers (1)
</exampleCaption>
<PageDef pageDef="Right" type="M">
   <!-- Background text frame for running Header -->
   <TextFrame type="B" L="2.5cm" T="1cm" W="16cm" H="1cm" >
     <P paraDef="Header"><BGVar fmt="1"/> - <BGVar fmt="2"/><tab/><PageNum/></P>
   </TextFrameDef>
   <!-- Foreground text frame for body page text flow -->
   <TextFrame type="F" L="2.5cm" T="2.5cm" W="16cm" H="24.7cm" />
</PageDef>
</exampleBlock>


<exp>
This is equivalent to using FrameMaker to insert 
the <#osq>Running H/F 1<#csq> and <#osq>Running H/F 2<#csq> system
variables in the <#osq>Right<#csq> master page header
text frame in a template file.
</exp>

<exp>
An input file which 
includes the format definitions
shown in Example <xparanumonly
	id="ex.dictionary_style_running_headers_and_footers_1" /> and
which contains the following paragraphs:
</exp>


@------------------------------------------------------------
<exampleBlock
	>
<exampleCaption
	dest="ex.dictionary_style_running_headers_and_footers_2"
	>
Dictionary style running headers and footers (2)
</exampleCaption>
<P paraDef="EntryName">
Albatross
 ...
<P paraDef="EntryName">
Cockatoo
 ...
</exampleBlock>


<exp>
produces a running header as shown in Figure
<xparanumonly id="fig.dictionary_style_header" />.
</exp>

@------------------------------------------------------------
<@ExampleOutput> {
<P paraDef="P_Output"><AutoNum P="E">2</AutoNum>
Albatross - Cockatoo
} {
<@FigureCaption> {Dictionary style header
} {fig.dictionary_style_header}  { A="R" ri="0" }
} { EOTFRAME="Y" P="R" li="5mm" A="R" Tgap="1.5mm" TP="B" EOW="57" sb="2pt" sa="3pt" <#fillcolor>}  {<#fillcolor>
}


<exp>
See Example
<xparanumonly id="ex.bgmarker.bgvar" file="inline" />
 on page
<xnpage id="ex.bgmarker.bgvar" file="inline" />
 for another illustration of using <#xBGVar>
in a running header.
</exp>

<refh1
	dest="RunningText.defaults"
	>
Default background variable definitions</refh1>

<exp>
The default <#xRunningText> variable formats are shown in the listing below:
</exp>

<exampleBlock
	>
<exampleCaption
	dest="ex.default_background_variable_definitions"
	>
Default background variable definitions
</exampleCaption>

<!-- 
	Running H/F 1 system variable included 
	using <BGVar fmt="1"/>, picks up paragraph 
	text from Title paragraph on body page:
-->
<RunningText fmt="1"><paratext fmt="Title"/></RunningText>

<!-- 	
	Running H/F 2 system variable included 
	using <BGVar fmt="2"/>, picks up paragraph
	text from Heading paragraph on body page:
 -->
<RunningText fmt="2"><paratext fmt="Heading"/></RunningText>

<!-- 
	Running H/F 3 system variable included 
	in running header/footer using <BGVar fmt="3"/>, 
	picks up text set using <RunningTextMarker/> or 
	<RunningTextMarker fmt="1"/> on body page:
 -->
<RunningText fmt="3"><marker/></RunningText>

<!-- 
	Running H/F 4 system variable included 
	in running header/footer using <BGVar fmt="4"/>, 
	picks up text set using <RunningTextMarker fmt="2"/> on 
	body page:
 -->
<RunningText fmt="4"><marker fmt="2"/></RunningText>
</exampleBlock>

<@skipEnd>
@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
