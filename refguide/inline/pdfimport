@------------------------------------------------------------
@-- PdfImport
@------------------------------------------------------------
<elementStart
	elementType="xml_opt_cont"
	elementName="PdfImport"
	subElements="Y"
	>

<esp>
Use the <#xPdfImport> element at the top level to include
an entire, or a subset of the pages in an, external PDF document.
Each page from the imported PDF document is included in successive
text frames and columns in the main, foreground text flow.
</esp>
<esp>
The <#xPdfImport> element can define either generic or
specific properties for importing PDF files.
If the <pn>file</pn> property is included, then the properties
are specific to the referenced PDF file.
If the <pn>file</pn> property is <fI>not</fI> included, then the
properties relate to any referenced by the <#xPdfImport> element
with a corresponding <pn>pdfImport</pn> value.
</esp>

<@skipStart>
<esp>
The numbering and layout of footnotes is specified using the
<#xPdfImportDef> element (see pages <xnpage
	id="FNoteDef.element.start" /><xphyphen><xnpage
	id="FNoteDef.element.end" />).
</esp>
<@skipEnd>

</elementStart>



@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<property
	name="pdfImportDef"
	value="name"
	>

<pdp>
Format definition.
</pdp>
</property>

@------------------------------------------------------------
<property
	name="pageDef"
	value="name"
	>

<pdp>
Master page name. Default for all imported PDF pages.
</pdp>

</property>

<additionalProperties>
See the <#xPdfImportDef> element on pages <xnpage
	id="PdfImportDef.element.start" /><xphyphen/><xnpage
	id="PdfImportDef.element.end" />.
</additionalProperties>

@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------


@------------------------------------------------------------
@- 'subElementsStart' is defined in: $env:control/subElements.mmp
@------------------------------------------------------------
<subElementsStart
	fontProperties="N"
	@- onlyOneSubElement="1"
	/>

@------------------------------------------------------------
<subElementGroup>
In the following 
<#xtocEntryx>,
<#xixEntry> and
<#xpdfDest> sub-elements, the <pn>page</pn> property
may have any integer value. If the <pn>page</pn> value
refers to any import PDF page number not included by
the <#xPdfImportDef> <pn>pages</pn> property (see page <xnpage
	id="PdfImportDef.property.pages" />) the specified
metadata is inoperative.
</subElementGroup>
@------------------------------------------------------------

@------------------------------------------------------------
<subElementsEnd/>
@------------------------------------------------------------

<@skipStart>


@------------------------------------------------------------
<elementNote
	dest="note.PdfImportDef.single_column_only"
	>
<enp>
Single column main text flow only.
</enp>
</elementNote>
<@skipEnd>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
