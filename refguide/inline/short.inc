@-----------------------------------------------------------
@- short.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<chapterStart
	dest="inlineMarkupElements"
	pgfSuffix="ExA"
	>Inline markup elements
</chapterStart>
<#def> elementType	inline
@-----------------------------------------------------------
<@include>	${inline}/intro
@-----------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@-----------------------------------------------------------
<@include>	${inline}/aframe
<@include>	${inline}/atextframe
<@include>	${inline}/pdffileimport
<@skipStart>
<@include>	${inline}/backgroundmarker
<@include>	${inline}/cell
<@include>	${inline}/chapter
<@include>	${inline}/date
<@include>	${inline}/fnote
<@include>	${inline}/font
<@include>	${inline}/frame
<@include>	${inline}/frametitle
<@skipEnd>
<@include>	${inline}/contents
<@include>	${inline}/contentsinclude
<@skipStart>
<@include>	${inline}/generatedlistinclude
<@include>	${inline}/genchapter
<@include>	${inline}/masterpagerule	@--- causes FAIL
<@include>	${inline}/hypercmd
<@include>	${inline}/image
<@include>	${inline}/ix
<@include>	${inline}/ixsubNew
<@skipEnd>
<@include>	${inline}/line
<@include>	${inline}/listlabel
<@skipStart>
<@include>	${inline}/mkdest
<@include>	${inline}/mmdraw	@--- 
<@include>	${inline}/p
<@include>	${inline}/row
<@include>	${inline}/runningtextmarker
<@include>	${inline}/tblcolumnwidth
<@skipEnd>
<@include>	${inline}/tbl
<@include>	${inline}/tblcontinuation
<@include>	${inline}/tblcolumnformat
<@skipStart>
<@include>	${inline}/var
<@include>	${inline}/vardef
<@include>	${inline}/xref
@-----------------------------------------------------------
<@skipEnd>
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
