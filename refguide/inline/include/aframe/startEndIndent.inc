@------------------------------------------------------------
@- startEndIndent.inc
@------------------------------------------------------------
<#def> sei.outputTotalWidth	44mm
<#def> sei.topMargin		3mm
<#def> sei.leftMargin		5mm
<#def> sei.rightMargin		5mm
<#def> sei.bottomMargin		2.5mm
<#def> sei.textFrameWidth	@eval(<#sei.outputTotalWidth>
					- <#sei.leftMargin>
					- <#sei.rightMargin> )
<#def> sei.rightMarginOffset	@eval(<#sei.outputTotalWidth>
					- <#sei.rightMargin> )

@------------------------------------------------------------
<elementExampleTitle
	dest="AFrame.usingDefaultProperties.start"
	>Using variations on default properties
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> illustrates how to use
the <#xAFrame> and <#xImage> elements to include an image.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	>Including a graphic image (1)
</exampleCaption>
<P>
Mt. Fuji in a blue sky.
<AFrame wrapContent="Y" >
@- <Image file="${images}/windmills.jpg" ppi="300" />
<Image file="${images}/trainAndFuji.jpg" ppi="2700" />
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number
of property settings. The <#xAFrame> <pn>wrapContent</pn> specifies
that the dimensions of the anchored frame are automatically determined
by the size of the image, which is centered within the text frame
by default.

The output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> is shown in Figure <xparanumonly
	id="fig.AFrame.mtFuji1" />. The dotted red lines delimit
the text frame border.

<exampleOutput
        width="<#sei.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#sei.leftMargin>"
	rightMargin="<#sei.rightMargin>"
	topMargin="<#sei.topMargin>"
	bottomMargin="<#sei.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	> Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
<mmDraw>
	@--- Top margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="<#sei.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#sei.topMargin>"
		@- left="@eval(<#sei.leftMargin> + <#sei.textFrameWidth> - 1)"
		left="<#sei.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

</mmDraw>
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>

<exp>
Example <xparanumonly
	id="ex.AFrame.wideSky1" /> illustrates an anchored
frame with an image bleeding outside the page.
</exp>
<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.wideSky1"
	>Including a graphic image (2)
</exampleCaption>
<P>
Cloudy sky
<AFrame wrapContent="Y" <eto>endIndent="-30mm"</eto> <hc>endIndent="-7mm"</hc> align="end" >
@- <Image file="${images}/windmills.jpg" ppi="300" />
<Image file="${images}/wideSky.jpg" ppi="2700" />
</AFrame>
</P>
</exampleBlock>

<exp>
The output from Example <xparanumonly
	id="ex.AFrame.wideSky1" /> is shown in Figure <xparanumonly
	id="fig.AFrame.wideSky1" />.
<exampleOutput
        width="<#sei.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#sei.leftMargin>"
	rightMargin="<#sei.rightMargin>"
	topMargin="<#sei.topMargin>"
	bottomMargin="<#sei.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.wideSky1"
	> Output from Example <xparanumonly
	id="ex.AFrame.wideSky1" />.</outputCaption>
<#example_output>
<mmDraw>
	@--- Top margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="<#sei.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#sei.topMargin>"
		left="<#sei.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

</mmDraw>
</exampleOutput>
</exp>
