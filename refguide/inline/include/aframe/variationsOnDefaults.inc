@------------------------------------------------------------
@- variationsOnDefaults.inc
@------------------------------------------------------------
<#def> sei.outputTotalWidth	44mm
<#def> sei.topMargin		3mm
<#def> sei.leftMargin		5mm
<#def> sei.rightMargin		5mm
<#def> sei.bottomMargin		2.5mm
<#def> sei.textFrameWidth	@eval(<#sei.outputTotalWidth>
					- <#sei.leftMargin>
					- <#sei.rightMargin> )
<#def> sei.rightMarginOffset	@eval(<#sei.outputTotalWidth>
					- <#sei.rightMargin> )

@------------------------------------------------------------
<elementExampleTitle
	dest="AFrame.usingDefaultProperties.start"
	>Using variations on default properties
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> illustrates how to use
the <#xAFrame> and <#xImage> elements to include an image.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	pgfFormat="xAx"	@- This will be used in Contents example
	>Including a graphic image (1)
</exampleCaption>
<P>
Mt. Fuji in a blue sky.
<AFrame wrapContent="Y" >
@- <Image file="${images}/windmills.jpg" ppi="300" />
<Image file="${images}/trainAndFuji.jpg" ppi="2700" />
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number
of property settings. The <#xAFrame> <pn>wrapContent</pn> specifies
that the dimensions of the anchored frame are automatically determined
by the size of the image, which is centered within the text frame
by default.

The output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> is shown in Figure <xparanumonly
	id="fig.AFrame.mtFuji1" />. The dotted red lines delimit
the text frame border.

<exampleOutput
        width="<#sei.outputTotalWidth>"
	shadow="Y"
	leftMargin="<#sei.leftMargin>"
	rightMargin="<#sei.rightMargin>"
	topMargin="<#sei.topMargin>"
	bottomMargin="<#sei.bottomMargin>"
	@- belowGap="-2mm"
	sideGap="6mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	> Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
<mmDraw>
	@--- Top margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="<#sei.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#sei.topMargin>"
		@- left="@eval(<#sei.leftMargin> + <#sei.textFrameWidth> - 1)"
		left="<#sei.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

</mmDraw>
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>

@------------------------------------------------------------
@- Bleeding image
@------------------------------------------------------------

<#def> sei.pageWidth		{210 mm}
<#def> sei.pageLeftMargin	{25 mm}
<#def> sei.pageRightMargin	{25 mm}
<#def> sei.textFrameWidth	@eval(<#sei.pageWidth> - <#sei.pageLeftMargin>
					- <#sei.pageRightMargin>)
<#def> sei.textFrameWidth	@sub({<#sei.textFrameWidth>}, {mm}, { mm})
<#def> sei.bleedExtent		{6 mm}
@------------------------------------------------------------
@- <#def> sei.bleedExtentText	@sub(6mm
@------------------------------------------------------------
<#def> sei.endIndentNominal	@eval(<#sei.pageRightMargin> + <#sei.bleedExtent>)
<#def> sei.ppi			540
<#def> sei.scaleFactor		.20
<#def> sei.scaleFactorPercent	@eval(<#sei.scaleFactor> * 100)

<#def> sei.topMargin		6.5mm
<#def> sei.outputTotalWidth	@eval(<#sei.pageWidth> * <#sei.scaleFactor>)
<#def> sei.leftMargin		@eval(<#sei.pageLeftMargin> * <#sei.scaleFactor>)
<#def> sei.rightMargin		@eval(<#sei.pageRightMargin> * <#sei.scaleFactor>)
<#def> sei.scaledBleedExtent	@eval(<#sei.bleedExtent> * <#sei.scaleFactor>)
<#def> sei.scaledBleedExtent	@eval(<#sei.scaledBleedExtent> + <#sei.rightMargin>)
<#def> sei.scaledPPI		@eval(<#sei.ppi> / <#sei.scaleFactor>)
<#def> sei.bottomMargin		2.5mm
<#def> sei.scaledTextFrameWidth	@eval(<#sei.outputTotalWidth>
					- <#sei.leftMargin>
					- <#sei.rightMargin> )
<#def> sei.rightMarginOffset	@eval(<#sei.outputTotalWidth>
					- <#sei.rightMargin> )
<exp>
Example <xparanumonly
	id="ex.AFrame.wideSky1" /> illustrates an anchored
frame with an image bleeding outside the page.
</exp>
<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.wideSky1"
	pgfFormat="xAx"	@- This will be used in Contents example
	>Including a graphic image (2)
</exampleCaption>
<P>
Cloudy sky
<AFrame wrapContent="Y" <eto>endIndent="-<#sei.endIndentNominal>"</eto> <hc>endIndent="-<#sei.scaledBleedExtent>"</hc> align="end" ><xlineRef>ex.AFrame.bleedSetting</xlineRef>
<Image file="${images}/wideSky.jpg" <eto>ppi="<#sei.ppi>"</eto> <hc>ppi="<#sei.scaledPPI>"</hc> />
</AFrame>
</P>
</exampleBlock>

<exp>
In Example <xparanumonly
	id="ex.AFrame.wideSky1" /> the <#xAFrame> bleeds <#sei.bleedExtent> outside
the right side of the page edge. This is achieved by setting
the <#xAFrame> <pn>align</pn> property to <pv>end</pv> and
the <pn>endIndent</pn> property to a negative value equivalent
to the sum of the size of the right margin, <#sei.pageRightMargin>, <fI>plus</fI> the desired
amount of bleed (line <xlinenum
	id="ex.AFrame.bleedSetting" />).
<exampleOutput
        width="<#sei.outputTotalWidth>"
	shadow="Y"
	leftMargin="<#sei.leftMargin>"
	rightMargin="<#sei.rightMargin>"
	topMargin="<#sei.topMargin>"
	bottomMargin="<#sei.bottomMargin>"
	@- belowGap="-2mm"
	sideGap="6mm"
	>
	<outputCaption
	dest="fig.AFrame.wideSky1"
	> Output from Example <xparanumonly
	id="ex.AFrame.wideSky1" /> (scaled to <#sei.scaleFactorPercent>%).</outputCaption>
<#example_output>
<mmDraw>
	@--- Page width
	<doubleArrow top="2mm" left="0" lineLength="<#sei.outputTotalWidth>"
		textLen="20mm" textSize="5.5pt"
		>page width = <#sei.pageWidth></doubleArrow>
	@--- Text frame width
	<doubleArrow top="5mm" left="<#sei.leftMargin>" lineLength="<#sei.scaledTextFrameWidth>"
		textLen="24mm" textSize="5.5pt"
		>text frame width = <#sei.textFrameWidth></doubleArrow>
	@--- Top margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="<#sei.scaledTextFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#sei.topMargin>" left="<#sei.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#sei.topMargin>"
		left="<#sei.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

</mmDraw>
</exampleOutput>
</exp>

<exp>
The output from Example <xparanumonly
	id="ex.AFrame.wideSky1" /> is shown in Figure <xparanumonly
	id="fig.AFrame.wideSky1" />.
</exp>
<exp>
To make the bleed occur at the opposite page border,
the <#xAFrame> <pn>align</pn> property is set to <pv>start</pv> and
the <pn>startIndent</pn> is set to a
negative value equivalent to the sum of the size of the left
margin <fI>plus</fI> the desired amount of bleed.
</exp>
