@------------------------------------------------------------
@- rdim.inc
@------------------------------------------------------------


@------------------------------------------------------------
<#def> rdm.textSize		3.0mm
<#def> rdm.textSize		2.8mm
<#def> rdm.outputTotalWidth	44mm
<#def> rdm.topMargin		3mm
<#def> rdm.leftMargin		4.5mm
<#def> rdm.rightMargin		4.5mm
<#def> rdm.bottomMargin		2.5mm
<#def> rdm.bottomMargin		5.0mm
<#def> rdm.bottomMargin		6.0mm
<#def> rdm.aframeHeight		20mm
<#def> rdm.aframeHeight		14mm
<#def> rdm.aframePercent	80
<#def> rdm.aframePenWidth	2mm
<#def> rdm.aframeAnnoBorder	{0.8pt}		@- Dashed border arount AFrame
<#def> rdm.textFrameWidth	@eval(<#rdm.outputTotalWidth>
					- <#rdm.leftMargin>
					- <#rdm.rightMargin> )
<#def> rdm.rightMarginOffset	@eval(<#rdm.outputTotalWidth>
					- <#rdm.rightMargin> )
<#def> rdm.aframeWidth		@eval(<#rdm.textFrameWidth>
					* <#rdm.aframePercent> / 100)
<#def> rdm.aframeLeft		@eval((<#rdm.textFrameWidth> - <#rdm.aframeWidth>) / 2)
<#def> rdm.aframeAnnoTop	@eval(<#rdm.aframeHeight> + <#rdm.textSize> + 5)
<#def> rdm.aframeAnnoLeft	@eval(<#rdm.aframeLeft> + <#rdm.leftMargin>)
<#def> rdm.textFrameAnnoTop	@eval(<#rdm.aframeAnnoTop>  + 3.5)
@------------------------------------------------------------
<@xmacro> imageWrap {
	@----------------------------------------------------
	@xgetvals(iw.)
	@----------------------------------------------------
	@- <Image width="<#iw.width>" file="${images}/blackCircle.png" />
	<Image width="<#iw.width>" file="<#iw.file" />
}{}
@------------------------------------------------------------
<@xmacro> rdm.aframeBorder {
	@--- Anchored frame border
	<#def> origHeight	<#rdm.aframeHeight>
	@if(@len(<#rdm.imageWidth>)) {
		<#def> rdm.filename		{<#rdm.file>}
		@if(@match({<#rdm.filename>}, {images})) {
			<#def> rdm.filename		@basename(<#rdm.filename>)
			<#def> rdm.filename		{${images}/<#rdm.filename>}
			}
		<@write> error { -AR =  |<#rdm.file>|}
		@if(@image(@trim(<#rdm.filename>))) {
			<@write> error { -AR =  <#img.ar>  ------------- cvbngf <#rdm.imageWidth>}
			}
		<#def> rdm.imageWidth		@sub(<#rdm.imageWidth>, {%}, {})
		<#def> rdm.imageHeight		@eval(((<#rdm.aframeWidth>
							- (2 * <#rdm.aframePenWidth>))
							* <#rdm.imageWidth> / 100)
							/ <#img.ar>)
		<#def> rdm.aframeHeight		@eval(<#rdm.imageHeight>
							+ (2 * <#rdm.aframePenWidth>))

		}
	<Rectangle penColor="Black" penWidth="<#rdm.aframeAnnoBorder>" penStyle="dashed"
		fillOpacity="0"
		left="<#rdm.aframeLeft>"
		top="<#rdm.textSize>"
		width="<#rdm.aframeWidth>"
		height="<#rdm.aframeHeight>"
	 	/>
	<#def> rdm.aframeAnnoTop	@eval(<#rdm.aframeHeight> + <#rdm.textSize> + 5)
	<#def> rdm.textFrameAnnoTop	@eval(<#rdm.aframeAnnoTop>  + 3.0)
	<#def> rdm.aframeHeight	<#origHeight>
}{}
@------------------------------------------------------------
<@xmacro> - rdm.annotateAFrame {
	<#def> rdm.imageWidth	{}
	<#def> rdm.file		{}
	@----------------------------------------------------
	@xgetvals(rdm.)
	@----------------------------------------------------
}{
	@if(@len(<#rdm.imageWidth>)) {
		<@write> error { RRRRRRRRRRRRR------------------ cvbngf <#rdm.imageWidth>}
		}
<mmDraw>
	@--- Top margin
	<textFrameBorder top="<#rdm.topMargin>" left="<#rdm.leftMargin>"
		lineLength="<#rdm.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#rdm.topMargin>" left="<#rdm.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#rdm.topMargin>"
		@- left="@eval(<#rdm.leftMargin> + <#rdm.textFrameWidth> - 1)"
		left="<#rdm.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />
	@--- Anchored frame border
	<rdm.aframeBorder/>
	@--- Arrow showing width of AFrame
	<doubleArrow top="2mm"
		top="<#rdm.aframeAnnoTop>"
		left="<#rdm.aframeAnnoLeft>"
		lineLength="<#rdm.aframeWidth>"
		textLen="22mm"
		textSize="5.5pt"
		>AFrame width = <#rdm.aframeWidth></doubleArrow>
	@--- Arrow showing width of text frame (container)
	<doubleArrow top="2mm"
		top="<#rdm.textFrameAnnoTop>"
		left="<#rdm.leftMargin>"
		lineLength="<#rdm.textFrameWidth>"
		textLen="25mm"
		textSize="5.5pt"
		>Container width = <#rdm.textFrameWidth></doubleArrow>

</mmDraw>
	$rdm.annotateAFrame
}
@------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> illustrates how to use
the <#xAFrame> and <#xImage> elements to include an image.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	>Using relative width (1)
</exampleCaption>
<P <hcx> textSize="<#rdm.textSize>" leading="0"</hcx>>
Centered anchored frame
<AFrame width="<#rdm.aframeWidth>" height="<#rdm.aframeHeight>" penColor="Red" penWidth="<#rdm.aframePenWidth>"
	fillOpacity="0"
	>
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number

<exampleOutput
        width="<#rdm.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#rdm.leftMargin>"
	rightMargin="<#rdm.rightMargin>"
	topMargin="<#rdm.topMargin>"
	bottomMargin="<#rdm.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	>Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
@------------------------------------------------------------
<rdm.annotateAFrame/>
@------------------------------------------------------------
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>

<elementExampleTitle
	dest="AFrame.usingRdim.start"
	>Using relative dimensions
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> illustrates how to use
the <#xAFrame> and <#xImage> elements to include an image.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	>Using relative width (1)
</exampleCaption>
<P <hcx> textSize="<#rdm.textSize>" leading="0"</hcx>>
Centered anchored frame
<AFrame width="<#rdm.aframePercent>%" height="<#rdm.aframeHeight>" penColor="Red" penWidth="<#rdm.aframePenWidth>"
	fillOpacity="0"
	>
@- <Image file="${images}/trainAndFuji.jpg" ppi="2700" />
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number

<exampleOutput
        width="<#rdm.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#rdm.leftMargin>"
	rightMargin="<#rdm.rightMargin>"
	topMargin="<#rdm.topMargin>"
	bottomMargin="<#rdm.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	> Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
@------------------------------------------------------------
<rdm.annotateAFrame/>
@------------------------------------------------------------
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>




<exp>
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> illustrates how to use
the <#xAFrame> and <#xImage> elements to include an image.
</exp>

@------------------------------------------------------------------------------------------------------------
<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	>Using relative width (1)
</exampleCaption>
<P <hcx> textSize="<#rdm.textSize>" leading="0"</hcx>>
Centered anchored frame
<AFrame width="<#rdm.aframePercent>%"
	@- height="<#rdm.aframeHeight>"
	wrapContent="Y"
	penColor="Red" penWidth="<#rdm.aframePenWidth>"
	fillOpacity="0"
	>
	@- <FrameTitle><P>Hello</P></FrameTitle>
	<Image width="30%" file="${images}/blackCircle.png" />
	@- <Image width="30%" file="${images}/bb0004-02.gif" />
	@- <imageWrap width="30%" file="${images}/blackCircle.png" />
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number

<exampleOutput
        width="<#rdm.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#rdm.leftMargin>"
	rightMargin="<#rdm.rightMargin>"
	topMargin="<#rdm.topMargin>"
	bottomMargin="<#rdm.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	> Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
@------------------------------------------------------------
<rdm.annotateAFrame imageWidth="30%" file="${images}/blackCircle.png" />
@- <rdm.annotateAFrame imageWidth="30%" file="${images}/bb0004-02.gif" />
@- <rdm.annotateAFrame imageWidth="30%" file="blackCircle.png" />
@------------------------------------------------------------
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>


@------------------------------------------------------------------------------------------------------------
<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	show="Y"
	>
<exampleCaption
	dest="ex.AFrame.mtFuji1"
	>Using relative width (1)
</exampleCaption>
<P <hcx> textSize="<#rdm.textSize>" leading="0"</hcx>>
Centered anchored frame
<AFrame width="<#rdm.aframePercent>%"
	@- height="<#rdm.aframeHeight>"
	wrapContent="Y"
	penColor="Red" penWidth="<#rdm.aframePenWidth>"
	fillOpacity="0"
	>
	@- <FrameTitle><P>Hello</P></FrameTitle>
	@- <Image width="30%" file="${images}/blackCircle.png" />
	<Image width="30%" file="${images}/bb0004-02.gif" />
	@- <imageWrap width="30%" file="${images}/blackCircle.png" />
</AFrame>
</P>
</exampleBlock>

<exp>
The three elements, <#xP>, <#xAFrame> and <#xImage>, included in
Example <xparanumonly
	id="ex.AFrame.mtFuji1" /> have the near-minimum number

<exampleOutput
        width="<#rdm.outputTotalWidth>"
	shadow="N"
	shadow="Y"
	fillOpacity="0"
	fillOpacity="100"
	shadowDistance="4pt"
	shadowAngle="122"
	leftMargin="<#rdm.leftMargin>"
	rightMargin="<#rdm.rightMargin>"
	topMargin="<#rdm.topMargin>"
	bottomMargin="<#rdm.bottomMargin>"
	@- belowGap="-2mm"
	>
	<outputCaption
	dest="fig.AFrame.mtFuji1"
	> Output from Example <xparanumonly
	id="ex.AFrame.mtFuji1" />.</outputCaption>
<#example_output>
@------------------------------------------------------------
@- <rdm.annotateAFrame imageWidth="30%" file="${images}/blackCircle.png" />
<rdm.annotateAFrame imageWidth="30%" file="${images}/bb0004-02.gif" />
@- <rdm.annotateAFrame imageWidth="30%" file="blackCircle.png" />
@------------------------------------------------------------
</exampleOutput>
</exp>
<exp>
By default neither borders nor fills are applied to
the <#xAFrame> or <#xImage> elements.
</exp>

