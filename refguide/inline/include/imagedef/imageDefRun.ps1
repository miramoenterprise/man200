#--------------------------------------------------------------------------------
#- imageDefRun.ps1
#--------------------------------------------------------------------------------
#
#	called from inline/imagedef
#
# <@system> { set imageDoubleSided="Y" && powershell -file %inline%/include/section/mpRun1.ps1 }
#
#--------------------------------------------------------------------------------
$imageDefHomeDir	= "$env:inline/include/imagedef"
#--------------------------------------------------------------------------------
if (-not (Test-Path env:imageDefDoubleSided)) { $env:imageDefDoubleSided = "Y " }
if (-not (Test-Path env:imageDefExampleFile)) { $env:imageDefExampleFile = "imageDef1.pdf" }
# write-host "-========================== $env:imageDefDoubleSided "

#--------------------------------------------------------------------------------
$env:imageDefDoubleSided	= $env:imageDefDoubleSided.Trim()
$env:imageDefDoubleSided	= $env:imageDefDoubleSided.Trim("'",'"')

$env:imageDefExampleFile	= $env:imageDefExampleFile.Trim()
$env:imageDefExampleFile	= $env:imageDefExampleFile.Trim("'",'"')
#--------------------------------------------------------------------------------

mmpp "$imageDefHomeDir/control.mmp" | out-file -enc oem "$imageDefHomeDir/$env:imageDefExampleFile.mm"

# miramo -composer "$env:composerTag" -M -warn 0 -sendEnv Y -jobOptions "$env:jo" -Opdf "$imageDefHomeDir/$env:imageDefExampleFile.pdf"  "$imageDefHomeDir/control.mmp"

#--- miramo -composer "$env:composerTag" -warn 0 -sendEnv Y -jobOptions "$env:jo" -Opdf "$imageDefHomeDir/$env:imageDefExampleFile.pdf"  "$imageDefHomeDir/$env:imageDefExampleFile.mm"

# This should work, but does not, owing to -M mmpp preprocessing ???
# miramoRP  -M -warn 0 -sendEnv Y -jobOptions "$env:jo" -Opdf "$imageDefHomeDir/$env:imageDefExampleFile.pdf"  "$imageDefHomeDir/control.mmp"

# mmpp "$imageDefHomeDir/control.mmp" | out-file -enc oem "$imageDefHomeDir/imageDefRun-mmppout"
# $env:mmCommand                  = "C:/ap/miramo\MiramoPDF-3.0.0\miramoRP"
# $env:port                       = "22088"
# $env:host                       = "localhost"
# $env:user                       = "admin"
# $env:password                   = "admin"
# 
#	-user "$env:user" `
#	-port "$env:port" `
#	-host "$env:host" `
#	-password "$env:password" `
#--------------------------------------------------------------------------------
# & "$env:mmCommand"  -warn 0 `
& miramo  -warn 0 `
	-sendEnv Y `
	-fontPath "$env:mman\fonts" `
	-jobOptions "$env:jo" `
	-Opdf "$imageDefHomeDir/$env:imageDefExampleFile.pdf"  "$imageDefHomeDir/$env:imageDefExampleFile.mm"
#--------------------------------------------------------------------------------
	# -Opdf "$imageDefHomeDir/$env:imageDefExampleFile.pdf"  "$imageDefHomeDir/imageDefRun-mmppout"
