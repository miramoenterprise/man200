@------------------------------------------------------------
@-- maximumWidthHeight.inc
@------------------------------------------------------------

@------------------------------------------------------------
<elementExampleTitle
	dest="Image.exampleTitle.maximumWidthHeight"
	>Specifying maximum width and height</elementExampleTitle>
@------------------------------------------------------------

<exp>
Example <xparanumonly
	id="ex.Image.usingMaximumHeight" /> illustrates the
usage of the <pn>maximumHeight</pn> property.
The usage of the <pn>maximumWidth</pn> property is precisely
anlogous with the <pn>maximumHeight</pn> property.
</exp>
<exp>
The image file, bearing.gif, used in this illustration and shown
in Figure <xparanumonly
	id="fig.Image.usingMaximumHeight" /> is a
5 bits per pixel GIF image with a white (non-transparent) background
100 pixels wide and 300 pixels high. The inverse aspect ratio is 3.
</exp>

@--------------------------------------------------------------------------------
<#def> imw.tableItem1	{
	<Tbl tblDef="noRulings" >
	@- <Tbl tblDef="thinRulings" >
	<TblColumnDef width="9mm" />
	<TblColumnDef width="12mm" />
	<TblColumnDef width="12mm" />
	<TblColumnDef width="8mm" />
	<TblColumnDef width="0mm" />
	@------------------------------------------------------------
	@-Row 1
	@------------------------------------------------------------
	<Row>
	<Cell>
}
@--------------------------------------------------------------------------------
<#def> imw.tableItem2	{
	</Cell>
	</Row>
	@------------------------------------------------------------
	@-Row 2
	@------------------------------------------------------------
	<Row>
	<Cell>(A)</Cell>
	</Row>
	@------------------------------------------------------------
	@-Row 3
	@------------------------------------------------------------
	<Row>
	<Cell/>
	<Cell>(C)</Cell>
	<Cell>(D)</Cell>
	</Row>
	@------------------------------------------------------------
	@-Row 4
	@------------------------------------------------------------
	<Row>
	<Cell/>
	<Cell>(B)</Cell>
	</Row>
	@------------------------------------------------------------
	</Tbl>
}
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
<exampleBlock
	lineNumbers="Y"
	lineNumberStyle="N"
	>
<exampleCaption
	dest="ex.Image.usingMaximumHeight"
	>Using <pn>maximumHeight</pn>
</exampleCaption>
<!-- (A) ======================================================== -->
<Image file="bearing.gif" width="5mm" maximumHeight="20mm" /><xlr>line.Image.widthHeightA</xlr>

<!-- (B) ======================================================== -->
<Image file="bearing.gif" width="8mm" /><xlr>line.Image.widthHeightB</xlr>

<!-- (C) ======================================================== -->
<Image file="bearing.gif" width="8mm" maximumHeight="20mm" keepAspectRatio="N" /><xlr>line.Image.widthHeightC</xlr>

<!-- (D) ======================================================== -->
<Image file="bearing.gif" width="8mm" maximumHeight="20mm"  /><xlr>line.Image.widthHeightD</xlr>
@- <Image file="bearing.gif" maximumWidth="30mm" maximumHeight="20mm"  /><xlr>line.Image.widthHeightD</xlr>
@- <Image file="bearing.gif" maximumWidth="10mm" /><xlr>line.Image.widthHeightD</xlr>
</exampleBlock>
@--------------------------------------------------------------------------------

@--------------------------------------------------------------------------------
@- <#def> example_output		@gsub({<#example_output>}, {bearing.gif}, {$\{images\}/studBearing.gif})
<#def> imwhEO	@gsub({<#example_output>}, {bearing.gif}, {$\{images\}/studBearing.gif})
<#def> imwhEO	@sub({<#imwhEO>}, {\<\!-- \(B\)}, {</Cell><Cell rowSpan="3" ><!-- (B)})
<#def> imwhEO	@sub({<#imwhEO>}, {\<\!-- \(C\)}, {</Cell><Cell rowSpan="2" ><!-- (C)})
<#def> imwhEO	@sub({<#imwhEO>}, {\<\!-- \(D\)}, {</Cell><Cell rowSpan="2" ><!-- (D)})
@--------------------------------------------------------------------------------

<exp>
The output from Example <xparanumonly
	id="ex.Image.usingMaximumHeight" /> is
shown in Figure <xparanumonly
	id="fig.Image.usingMaximumHeight" />.
<exampleOutput
	@- width="52mm"
	width="45mm"
	shadow="Y"
	leftMargin="2mm"
	topMargin="1mm"
	bottomMargin="0"
	@- belowGap="-2mm"
	>
<outputCaption
	dest="fig.Image.usingMaximumHeight"
	>Output from Example <xparanumonly
	id="ex.Image.usingMaximumHeight"
	/>.
</outputCaption>
<#imw.tableItem1>
<#imwhEO>
<#imw.tableItem2>
</exampleOutput>

</exp>

<exp>
The rendering of the bearing.gif image (A) in Figure <xparanumonly
	id="fig.Image.usingMaximumHeight" /> is determined
by the <#xImage> properties on
line <xlinenum id="line.Image.widthHeightA" /> in
Example <xparanumonly
	id="ex.Image.usingMaximumHeight" />.
The image width is 5&nbsp;mm, the image
height is 15&nbsp;mm (5&nbsp;mm &times; 3).
In this case
the <pn>maximumHeight</pn> property has no effect
as the image height is less the the value specified (<pv>20mm</pv>).
Image (B) (line <xlinenum id="line.Image.widthHeightB" /> in
Example <xparanumonly
	id="ex.Image.usingMaximumHeight" />) is specified
with a width of 8&nbsp;mm. As there are no additional
size properties, the height is 24&nbsp;mm (8&nbsp;mm &times; 3).
</exp>

<exp>
Images (C) and (D) (lines <xlinenum
	id="line.Image.widthHeightC" /> and <xlinenum
	id="line.Image.widthHeightD" /> in Example <xparanumonly
	id="ex.Image.usingMaximumHeight" />) are specified
with <pn>width</pn> and <pn>maximumHeight</pn> values
of <pv>8mm</pv> and <pv>20mm</pv> respectively and in both
cases the height is constrained to the value of <pn>maximumHeight</pn>.
In the case of image (D) the width is reduced
to 6.67&nbsp;mm (20&nbsp;mm / 3) to maintain the aspect ratio
(default for <pn>keepAspectRatio</pn> is <pv>Y</pv>).
</exp>

<exp>
In the case either <pn>maximumWidth</pn> or <pn>maximumHeight</pn>,
singly or both in combination, are the only properties specified
for the <#xImage> element, they are equivalent to
to the <pn>width</pn> and <pn>height</pn> properties.
</exp>


@--------------------------------------------------------------------------------
