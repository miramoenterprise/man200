@------------------------------------------------------------
@-- imageScaling.inc
@------------------------------------------------------------

@------------------------------------------------------------
<elementExampleTitle
	dest="Image.exampleTitle.imageScaling"
	>Image scaling</elementExampleTitle>
@------------------------------------------------------------

@--------------------------------------------------------------------------------
@- Using $\{inline\} to support Linux
@--------------------------------------------------------------------------------
<#def> iR.imageDir	{$\{inline\}/include/image/imageScaling}
@--------------------------------------------------------------------------------
<#def> iR.file[1]	{8mm.pdf}
<#def> iR.file[2]	{8mm.png}
<#def> iR.file[3]	{8mm.svg}
<#def> iR.file[4]	{px.svg}
@--------------------------------------------------------------------------------
<#def> i.RowGap		0.5mm		@- Gap between Rows
<#def> i.W1		 4		@- Col width in mm
<#def> i.W2		22		@- Col width in mm
<#def> i.W2		24		@- Col width in mm
<#def> i.W2		31		@- Col width in mm	@- No glyph error
<#def> i.W2		20		@- Col width in mm
<#def> i.W3		11.0		@- Col width in mm
<#def> i.W4		11.0		@- Col width in mm
<#def> i.W5		11.0		@- Col width in mm
<#def> i.W6		11.0		@- Col width in mm
<#def> i.W2		19		@- Col width in mm
<#def> i.W3		10.6		@- Col width in mm
<#def> i.W4		10.6		@- Col width in mm
<#def> i.W5		10.6		@- Col width in mm
<#def> i.W6		10.6		@- Col width in mm
@--------------------------------------------------------------------------------
<#def> i.W1		 4		@- Col width in mm
<#def> i.totalWidth	@eval(<#i.W1> + <#i.W2> + <#i.W3> + <#i.W4> + <#i.W5> + <#i.W6>)
<#def> i.outputWidth	@eval(<#i.totalWidth> + 1)	@- For 'exampleOutput'
@--------------------------------------------------------------------------------
<@include>	${inline}/include/image/imageScaling/imageScaling.mmp
@--------------------------------------------------------------------------------
<#def> i.tblType	thinRulings
<#def> i.tblType	noRulings
@--------------------------------------------------------------------------------
<@xmacro> - ifile {
}{
	<#def> ifile.text	$ifile
	@print({<Font fontFamily="Frutiger LT 45 Light"
		textSize="-10%" ><#ifile.text></Font>}, n)
}

<exp>
Figures <xparanumonly
	id="fig.Image.imageScaling1" /> and <xparanumonly
	id="fig.Image.imageScaling2" /> illustrate the operation of image
scaling in relation to <pn>shrinkToFit</pn>, <pn>ppi</pn>, <pn>scale</pn> and, in
the special case a single image only is included at the top level in a table
cell, the <pn>allMargins</pn> property.
</exp>

<xList
	type="text"
	labelIndent="8mm"	@- This val is LHS of <exp/> body text
	labelIndent="12mm"
	labelWidth="19mm"
	labelFontFamily="Frutiger LT 45 Light"
	labelFontWeight="Regular"
	labelTextColor="Black"
	labelTextSize="-10%"
	labelTextSize="-15%"
	labelTextSize="8.5pt"
	labelTextSize="35%"
	labelTextSize="35%"
	labelTextSize="90%"
	labelTextSize="8.4pt"
	>
<listItem text="8mm.pdf" >
A PDF file with Crop Box dimensions 8mm x 8mm.
</listItem>

<listItem text="8mm.png" >
A PNG file with dimensions 8mm x 8mm. (47px x 47px with
internal pixels per inch = 150)
</listItem>

<listItem text="8mm.svg" >
An SVG file with dimensions 8mm x 8mm.
(<Font fontFamily="Courier New" >&lt;svg width="8mm" height="8mm" viewBox="0 0 345 345" &gt;</Font>).
	
</listItem>

<listItem text="px.svg" >
An SVG file with no absolute dimensions.
(<Font fontFamily="Courier New" >&lt;svg width="37.76" height="37.76" viewBox="0 0 345 345" &gt;</Font>).
</listItem>

</xList>
@- fillColor
@- fillOpacity



<exp>
In Figure <xparanumonly
        id="fig.Image.imageScaling1" /> all the images in row 1 are the
same size because the default <#xDocDef> <pn>ppi</pn> value is 120 and
none of the <#xImage> element's 
<pn>ppi</pn>,
<pn>shrinkToFit</pn> or
<pn>scale</pn> properties are specified.
In Figures <xparanumonly
	id="fig.Image.imageScaling1" /> and <xparanumonly
	id="fig.Image.imageScaling2" /> all cell widths are 7mm.
<exampleOutput width="<#i.outputWidth>mm"
	shadow="Y" belowGap="0" leftMargin="0"
	topMargin="0.5mm"
	endMargin="0" >
<outputCaption
        dest="fig.Image.imageScaling1"
        >Image scaling (1)
</outputCaption>
<imageRow rowLabel="1" tblDef="<#i.tblType>" />
<imageRow ppi="200" header="N"  rowLabel="2" tblDef="<#i.tblType>" />
@- <imageRow ppi="200" header="N"  rowLabel="3" shrinkToFit="Y" allMargins="2pt" tblDef="<#i.tblType>" />
</exampleOutput>
</exp>

<exp>
In row 1 in Figure <xparanumonly
        id="fig.Image.imageScaling1" /> the
rendered dimensions of the <ifile>px.svg</ifile> image are derived as
follows: 37.76 / 120 x 25.4 = 8.0mm.
</exp>

<exp>
Row 2 in Figure <xparanumonly
        id="fig.Image.imageScaling1" /> shows the
rendered dimensions when the <#xImage> element's <pn>ppi</pn> value
is set to <pv>200</pv> for all four images.
In this case the size of the <ifile>8mm.pdf</ifile> and <ifile>8mm.svg</ifile> images
is unchanged because the <pn>ppi</pn> property has no effect on PDF images
or on SVG images that have a &lt;svg> root element width and height values specified in absolute
units (pt, pc, cm, mm or in).
</exp>

<#def> i.W1		 1		@- Col width in mm
<#def> i.totalWidth	@eval(<#i.W1> + <#i.W2> + <#i.W3> + <#i.W4> + <#i.W5> + <#i.W6>)
<#def> i.outputWidth	@eval(<#i.totalWidth> + 1)	@- For 'exampleOutput'

<exp>
The rendered dimensions of the <ifile>8mm.png</ifile> image are derived as
follows: 47 / 200 x 25.4 = 6.0mm.
</exp>

<exp>
The rendered dimensions of the <ifile>px.svg</ifile> image are derived as
follows: 37.76 / 200 x 25.4 = 4.8mm.
</exp>

<exp>
The scaling property settings used in Figure <xparanumonly
        id="fig.Image.imageScaling2" /> are the same as for
row 2 in Figure <xparanumonly
        id="fig.Image.imageScaling1" /> except the <pn>shrinkToFit</pn> property
is set to <pv>Y</pv>.
@------------------------------------------------------------
<exampleOutput width="<#i.outputWidth>mm"
	shadow="Y" belowGap="0" leftMargin="0"
	topMargin="0.5mm"
	endMargin="0" >
<outputCaption
        dest="fig.Image.imageScaling2"
        >Image scaling (2)
</outputCaption>
<imageRow ppi="200" header="Y"  rowLabel="" shrinkToFit="Y" allMargins="2pt" tblDef="<#i.tblType>" />
</exampleOutput>
@------------------------------------------------------------
</exp>
<exp>
When the <pn>shrinkToFit</pn> property is set to <pv>Y</pv> shrinkage
occurs only if the image size after allowing for
the <#xImage> <pn>ppi</pn> value, if applicable, and the <pn>scale</pn> value
(always applicable) is larger that the container area. In the case
an image at the top level within a <#xCell> element, as in the 
Figure <xparanumonly
	id="fig.Image.imageScaling2" />, the container area is
the area within the cell margins.
</exp>

@--------------------------------------------------------------------------------
