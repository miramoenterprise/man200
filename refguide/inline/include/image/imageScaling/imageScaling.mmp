@--------------------------------------------------------------------------------
@- imageInCell.mmp
@--------------------------------------------------------------------------------
@-
@-     |                              |                                |                               
@-     |<---------- W1 -------------->|<---------- W2 ---------------->|<---- W3 
@-     |                              |                                |                               
@-     |           |              |   |            |                |  |                               
@-     |           |<--- w1  ---->|   |            |<---   w2  ---->|  |                               
@-     |           |              |   |            |                |  |                               
@-     |<-- l1 --->|              |   |<-- l2 --- >|                |  |                               
@-     |           |              |   |            |                |  |                               
@-     |           |              |   |            |                |  |                               
@-     |           |              |   |                                |                               
@-     |                              |                                |                               
@--------------------------------------------------------------------------------
@- <#def> iR.file[1]               {8mm.pdf}
@- <#def> iR.file[2]               {8mm.svg}
@- <#def> iR.file[3]               {8mm.png}
@- <#def> iR.file[4]               {px.svg}
@- <Tbl>
@- 	<Row>
@- 		<Cell> <Tbl> </Tbl> </Cell>
@- 		<Cell> <Tbl> </Tbl> </Cell>
@- 	</Row>
@- </Tbl>
@--------------------------------------------------------------------------------
<RuleDef ruleDef="0.5pt" penWidth="0.5pt" penColor="Black" />
<RuleDef ruleDef="0.25pt" penWidth="0.25pt" penColor="Black" />
@--------------------------------------------------------------------------------
<@xmacro> - imageInCell {
	<#def> i.tblStartIndent		1.0mm		@- Start indent of tbl in Cell
	<#def> i.tblStartIndent		0.5mm		@- Start indent of tbl in Cell
	<#def> i.columnWidth		12mm		@- Width of column in sub-table
	<#def> i.allRules		Thin		@- Ruling name
	<#def> i.allMargins		{0.25pt} 	@- Margin size
	<#def> i.iFile			{circleAmp3.svg}	@- Image file name
	<#def> i.scale			{}		@- Image scale percent
	<#def> i.ppi			{}		@- Image ppi override
	<#def> i.shrinkToFit		{}		@- shrinkToFit = Y | N
	@------------------------------------------------------------------------
	@xgetvals(i.)
	@------------------------------------------------------------------------
	@if(@len(<#i.shrinkToFit>)) {
		<#def> i.shrinkToFit		{shrinkToFit="<#i.shrinkToFit>" }
		}
	@if(@len(<#i.ppi>)) {
		<#def> i.ppi			{ppi="<#i.ppi>" }
		}
	@if(@len(<#i.scale>)) {
		<#def> i.scale			{scale="<#i.scale>" }
		}
	@------------------------------------------------------------------------
}{
	<Cell topMargin="4pt" >
		<Tbl tblDef="noRulings"
			@- allRules="<#i.allRules>"
			@- allRules="none"
			allMargins="<#i.allMargins>"
			startIndent="<#i.tblStartIndent>"
			columnCount="3"
			spaceAbove="0"
			>
		<TblColumn width="1mm" />
		<TblColumn width="<#i.columnWidth>" />
		<TblColumn width="1mm" />
			<Row height="1.5mm" topRule="none" bottomRule="<#i.allRules>" >
			<Cell/>
			<Cell
				startRule="<#i.allRules>"
				@- endRule="<#i.allRules>"
				/>
			<Cell/>
			</Row>
			<Row bottomRule="<#i.allRules>" >
				<Cell 
					startRule="none" 
					/>
				<Cell
					@- startRule="<#i.allRules>"
					@- endRule="<#i.allRules>" 
					>
					<Image
						file="<#iR.imageDir>/<#i.iFile>"
						<#i.shrinkToFit>
						<#i.ppi>
						<#i.scale>
						/>
				</Cell>
				<Cell/>
			</Row>
			<Row  height="1.5mm" >
			<Cell/>
			<Cell
				startRule="<#i.allRules>"
				@- endRule="<#i.allRules>"
				/>
			<Cell/>
			</Row>
		</Tbl>
	</Cell>
}
@--------------------------------------------------------------------------------
<@xmacro> imageRow {
	<#def> iR.ppi			{}
	<#def> iR.scale			{}
	<#def> iR.shrinkToFit		{}
	<#def> iR.allRules		{Thin}
	<#def> iR.fillColor		{Yellow}
	<#def> iR.fillOpacity		{100}
	<#def> iR.fillColor		{none}
	<#def> iR.fillOpacity		{0}
	<#def> iR.allMargins		{0.25pt}	@- This show glyph error!!!
	<#def> iR.allMargins		{0}		@- For cell containing image
	<#def> iR.header		{Y}
	<#def> iR.tblDef		{noRulings}
	<#def> iR.topMargin		{1pt}		@- For row containing image cells
	<#def> iR.bottomMargin		{0.5pt}		@- For row containing image cells
	<#def> iR.columnWidth		{7mm}		@- For row containing image cells
	<#def> iR.rowLabel		{}		@- For row containing image cells
	@------------------------------------------------------------------------
	@xgetvals(iR.)
	@------------------------------------------------------------------------
	<Tbl
		tblDef="<#iR.tblDef>"
		fillColor="<#iR.fillColor>"
		fillOpacity="<#iR.fillOpacity>"
		allMargins="0"
		@- topMargin="7.0pt"
		topMargin="4.0pt"
		bottomMargin="2.0pt"
		spaceAbove="0pt"
		spaceBelow="0pt"
		>
		<TblColumn width="<#i.W1>mm" />
		<TblColumn width="<#i.W2>mm" />
		<TblColumn width="<#i.W3>mm" />
		<TblColumn width="<#i.W4>mm" />
		<TblColumn width="<#i.W5>mm" />
		<TblColumn width="<#i.W6>mm" />
		@----------------------------------------------------------------
		<#def> iTextVals	{
				shrinkToFit="<#iR.shrinkToFit>"<br/>
				ppi="<#iR.ppi>"<br/>
				scale="<#iR.scale>"<br/>
				allMargins="<#iR.allMargins>"
				}
		<#def> iTextVals	@trim(<#iTextVals>)
		<#def> iTextVals	@gsub(<#iTextVals>, {	}, {})
		@----------------------------------------------------------------
		@- filenames Row
		@----------------------------------------------------------------
		@if(<#iR.header> = Y ) {
			<Row topMargin="0.8pt" bottomMargin="0.5pt" endRule="Very Thin" >
				@- <Cell columnSpan="2" ><P paraDef="P_Output" textSize="6.5pt"
				<Cell columnSpan="2" ><P paraDef="P_Output" textSize="6.0pt"
					textAlign="end"
					endIndent="5pt"
					hyphenate="N"
					>image filename:</P></Cell>
				@------------------------------------------------
				@- Print image filenames
				@------------------------------------------------
				@for(i = 1 to 4 ) {
					<Cell><P paraDef="P_Output" textSize="6.5pt"
						fontWeight="Bold"
						@- firstLineIndent="2pt"
						textAlign="center"
						>$iR.file[$$i]</P></Cell>
					}
			</Row>
			}
		@----------------------------------------------------------------
		@- Images row
		@----------------------------------------------------------------
		<Row endRule="Very Thin" >
			<Cell
				@- endRule="Very Thin"
				endRule="none"
				><P paraDef="P_Output" textSize="10pt" fontWeight="Bold"
				firstLineIndent="1.5pt"
				startIndent="1pt"
				><#iR.rowLabel></P></Cell>
			<Cell
				@- endRule="Very Thin"
				endRule="none"
				><P paraDef="P_Output" textSize="7pt"
				hyphenate="N"
				><#iTextVals></P></Cell>
				@------------------------------------------------
				@- Show images
				@------------------------------------------------
				@for(i = 1 to 4 ) {
					<#def> fnum	$$i
					<imageInCell
						iFile="$iR.file[<#fnum>]"
						shrinkToFit="<#iR.shrinkToFit>"
						columnWidth="<#iR.columnWidth>"
						scale="<#iR.scale>"
						ppi="<#iR.ppi>"
						@- allRules="<#iR.allRules>"
						allMargins="<#iR.allMargins>"
						/>
					}
		</Row>
	</Tbl>
}{}
@--------------------------------------------------------------------------------
@- For testing
@--------------------------------------------------------------------------------
@- <imageRow/>
@--------------------------------------------------------------------------------
