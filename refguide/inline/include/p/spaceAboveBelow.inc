@------------------------------------------------------------
@- spaceAboveBelow.inc
@------------------------------------------------------------

<#def> psab.Width		61mm
<#def> psab.Width		54.5mm
<#def> psab.leftMargin		7mm
<#def> psab.topMargin		6mm
<#def> psab.topMargin		2.25mm
<#def> psab.topMargin		0mm
<#def> psab.topMargin		0.3mm
<#def> psab.topMargin		4mm
<#def> psab.rightMargin		0mm
<#def> psab.bottomMargin	2mm
<#def> psab.bottomMargin	0
<#def> psab.topBorderLength	70mm
<#def> psab.rulingLength	70mm
<#def> psab.sideBorderLength	70mm
<#def> psab.textSizeLeft	30mm
@------------------------------------------------------------
<#def> psab.pointSizeA		32pt
<#def> psab.pointSizeB		18pt
<#def> psab.leadingA		12pt
<#def> psab.leadingB		8pt
<#def> psab.spaceAboveA		18pt
<#def> psab.spaceBelowA		12pt
<#def> psab.spaceAboveB		18pt
<#def> psab.spaceBelowB		10pt
<#def> psab.lineCount		0
@------------------------------------------------------------
<#def> psab.pointSizeA		28pt
<#def> psab.pointSizeB		16pt
<#def> psab.leadingA		10pt
<#def> psab.leadingB		8pt
<#def> psab.spaceAboveA		18pt
<#def> psab.spaceBelowA		12pt
<#def> psab.spaceAboveB		15pt
<#def> psab.spaceBelowB		10pt
<#def> psab.lineCount		0
<#def> psab.rulingLength	@eval(<#psab.Width> - <#psab.leftMargin>)
@------------------------------------------------------------

@------------------------------------------------------------
<@include> ${inline}/include/p/spaceAboveBelow.mmp
@------------------------------------------------------------

@- <P position="topOfPage" />
<elementNote
	dest="P.note.spaceAboveBelow"
	>

<exampleBlock
	show="N"
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.P.spaceAboveBelow"
	>
Paragraph xqk2 input for Figure <xparanumonly
	id="fig.P.spaceAboveBelow" />
</exampleCaption>
<!-- Paragraph A (upper paragraph) -->
<P paraDef="Body" textSize="<#psab.pointSizeA>" leading="<#psab.leadingA>" spaceBelow="<#psab.spaceBelowA>"
	>A. &Eacute;y<br/>taye</P>
<!-- Paragraph B (lower paragraph) -->
<P paraDef="Body" textSize="<#psab.pointSizeB>" leading="<#psab.leadingB>" spaceAbove="<#psab.spaceAboveB>"
	>B. &Aring;plj<br/>bread<br/>Ayl</P>
</exampleBlock>

@- spa<changeBarBegin/>cing is norm<changeBarEnd/>

<enp>
Inter-paragraph spacing is the distance between the
bottom edge of the text descenders of the upper paragraph and
the top edge of the text (e.g. the top of the capital letters) of the
lower paragraph.<FNote>This perception is applicable
primarily in the case of Latin script, which does not have a high
frequency of single or stacking diacritcs.
Even then, this perception would probably not persist if
the Latin text is set is a stylistic font with swashes.
</FNote> 
In the general case this space is determined by the <fI>maximum</fI> of
the upper paragraph's space below, <pn>spaceBelow</pn>, value and the
lower paragraph's space above, <pn>spaceAbove</pn>, value.
In the special case a paragraph is a sidehead the its space below
value is ignored, or if the paragraph immediately follows a sidehead
paragraph, its space above value is ignored.
<@skipStart>
This
distance is approximately determined by the sum of the
leading value, <pn>leading</pn>, used in the upper paragraph,
e.g. paragraph <sq>A</sq>,
plus either the upper paragraph's space below, <pn>spaceBelow</pn>, value or
the lower paragraph's, e.g. the  paragraph <sq>B</sq>, space
above value, <pn>spaceAbove</pn>, whichever is the greater.
Thus the upper paragraph's <#xP> element (or <#xParaDef> element)
property settings for space below (<pn>spaceBelow</pn>) and
leading (<pn>leading</pn>), and the lower paragraph's <#xP> element
setting for space above (<pn>spaceAbove</pn>) may be
used to control the inter-paragraph spacing.
@- If feathering is switched on, then the actual inter-paragraph spacing may increase (up
@- to the maximum value specified by the <#xDocDef> �maxp� option, or specified in a template
@- file), but not decrease.
<@skipEnd>
</enp>

<enp>
Negative values for paragraph space below and space above settings may be used to bring
particular paragraphs closer together than normal, or even to make them overlap.
<exampleOutput
        width="<#psab.Width>"
        shadow="N"
        shadow="Y"
        topMargin="<#psab.topMargin>"
        bottomMargin="<#psab.bottomMargin>"
        rightMargin="<#psab.rightMargin>"
        leftMargin="<#psab.leftMargin>"
        maximumHeight="180mm"
	@--- fillOpacity="100"
	@--- fillOpacity="0"
        @--- shadowDistance="4pt"
        @--- shadowAngle="122"
        @--- crop="N"
        @- belowGap="-2mm"
        >
<outputCaption
        dest="fig.P.spaceAboveBelow"
        > Inter-paragraph spacing
</outputCaption>
<#example_output>
<psab.mmDraw/>
</exampleOutput>
</enp>

<enp>
In mmComposer the only precise metric for inter-paragraph spacing is
the offset between the baselines of the last line of the upper paragraph
and the first line of the next paragraph. This baseline offset is actually
greater than the sum of values mentioned above, by an amount equal to one
third the pointsize of the upper paragraph plus two thirds the pointsize
of the lower paragraph.
</enp>

<enp>
The relationship between the <#xP> element <pn>spaceBelow</pn>, <pn>spaceAbove</pn> and
<pn>leading</pn> properties is illustrated in Figure <xparanumonly
	id="fig.P.spaceAboveBelow" />.
The paragraph settings and input text used in Figure <xparanumonly
	id="fig.P.spaceAboveBelow" /> is shown in Example <xparanumonly
	id="ex.P.spaceAboveBelow" />.
</enp>


<#printOutput>
<enp>
In the case a <#xParaDef> or <#xP> element has either or
both a <pn>frameAbove</pn> and <pn>frameBelow</pn> property set
to the name of a text frame, the paragraph
spacing metrics are as illustrated in Figure <xparanumonly
        id="fig.TextFrameDef.frameAboveBelow1" />, on page <xnpage
        id="fig.TextFrameDef.frameAboveBelow1" />.
</enp>

</elementNote>
