@------------------------------------------------------------
@- changebardef1.inc
@------------------------------------------------------------
<@xmacro> changeBarParams {
	<#def> cbp.position		start
	<#def> cbp.color		Red
	<#def> cbp.style		groove
	<#def> cbp.style		ridge
	<#def> cbp.style		solid
	<#def> cbp.width		22pt
	<#def> cbp.offset		26pt
	@----------------------------------------------------
	<#def> cbp.firstLineIndent	0
	<#def> cbp.startIndent		0
	@----------------------------------------------------
	<#def> cbp.outputTotalWidth	45mm
	<#def> cbp.textFrameAnnoTop	2.9mm
	<#def> cbp.topMargin		4.5mm
	<#def> cbp.leftMargin		14mm
	<#def> cbp.rightMargin		2mm
	<#def> cbp.bottomMargin		4.0mm
	@----------------------------------------------------
	@xgetvals(cbp.)
	@----------------------------------------------------
	<#def> cbp.textFrameWidth	@eval(<#cbp.outputTotalWidth>
						- <#cbp.leftMargin>
						- <#cbp.rightMargin> )
	<#def> cbp.rightMarginOffset	@eval(<#cbp.outputTotalWidth>
						- <#cbp.rightMargin> )
	@----------------------------------------------------
	@- Offset annotation
	@----------------------------------------------------
	<#def> cbp.offsetAnnoTop	6.4mm
	<#def> cbp.offsetPoint		@eval(<#cbp.leftMargin>
						- <#cbp.offset> )
	@----------------------------------------------------
	@- Width annotation
	@----------------------------------------------------
	<#def> cbp.widthAnnoTop		10.9mm
	<#def> cbp.widthAnnoTop		12.9mm
	<#def> cbp.widthAnnoTop		12.4mm
	<#def> cbp.widthPoint		@eval(<#cbp.leftMargin>
						- <#cbp.offset>
						- <#cbp.width> / 2)
}{}
@------------------------------------------------------------
<changeBarParams/>
@------------------------------------------------------------
<elementExampleTitle
	dest="<#elementName>.changeBarBegin1.start"
	>Using change bars
</elementExampleTitle>


<exp>
Example <xparanumonly
	id="ex.changeBarBegin1" /> illustrates using
the <#xChangeBarDef> element.
</exp>

<exampleBlock
	tabs="5|10|15|20|25|30|35"
	lineNumbers="Y"
	>
<exampleCaption
	dest="ex.changeBarBegin1"
	>Change bar definition (1)
</exampleCaption>
<P fontFamily="Arial" textSize="7pt" >The Orangan forest has:<br/>bears<ChangeBarBegin
	offset="<#cbp.offset>" width="<#cbp.width>" colorDef="<#cbp.color>" style="<#cbp.style>" position="<#cbp.position>"
	/> and wolves<ChangeBarEnd/>.</P>
</exampleBlock>

<exp>
The output from Example <xparanumonly
	id="ex.changeBarBegin1" /> is shown in Figure <xparanumonly
	id="fig.changeBarBegin1" />. The dotted red lines delimit
the container frame border.
In Figure <xparanumonly
	id="fig.changeBarBegin1" /> the <sq>offset</sq> and <sq>width</sq> annotations
indicate the values of the <pn>offset</pn> and <pn>width</pn> properties.

<exampleOutput
	width="<#cbp.outputTotalWidth>"
	fillOpacity="100"
	rightMargin="10"
	topMargin="<#cbp.topMargin>"
	leftMargin="<#cbp.leftMargin>"
	rightMargin="<#cbp.rightMargin>"
	bottomMargin="<#cbp.bottomMargin>"
	shadow="Y"
	crop="N"
	>
<outputCaption
	dest="fig.changeBarBegin1"
>
Output from Example <xparanumonly
	id="ex.changeBarBegin1" />
</outputCaption>
<#example_output>
<mmDraw>

	@--- Top margin
	<textFrameBorder top="<#cbp.topMargin>" left="<#cbp.leftMargin>"
		lineLength="<#cbp.textFrameWidth>" lineAngle="90" />
	@--- Left margin
	<textFrameBorder top="<#cbp.topMargin>" left="<#cbp.leftMargin>"
		lineLength="100mm" lineAngle="180" />
	@--- Right margin
	<textFrameBorder top="<#cbp.topMargin>"
		@- left="@eval(<#cbp.leftMargin> + <#cbp.textFrameWidth> - 1)"
		left="<#cbp.rightMarginOffset>"
		lineLength="100mm" lineAngle="180" />

	@--- Text frame width
	<doubleArrow top="<#cbp.textFrameAnnoTop>" left="<#cbp.leftMargin>" lineLength="<#cbp.textFrameWidth>"
		textLen="14.5mm" textSize="5.5pt"
		>text frame width</doubleArrow>

	@--- Offset dimension
	<doubleArrow top="<#cbp.offsetAnnoTop>" left="<#cbp.offsetPoint>" lineLength="<#cbp.offset>"
		showEndLines="Y"
		textLen="5.3mm" textSize="5.5pt"
		>offset</doubleArrow>

	@--- Width dimension
	<doubleArrow top="<#cbp.widthAnnoTop>" left="<#cbp.widthPoint>" lineLength="<#cbp.width>"
		textLen="5.3mm" textSize="5.5pt"
		>width</doubleArrow>
</mmDraw>
</exampleOutput>
</exp>

