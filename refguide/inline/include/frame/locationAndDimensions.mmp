@-===========================================================
@- The AFrame
@-===========================================================
<#def> f.frameTop		1mm
<#def> f.frameLeft		1mm
<#def> f.borderAnnoTop		@eval(<#f.dimWidthTop> + 6)

@------------------------------------------------------------
<AFrame width="<#f.AFw>mm" height="<#f.AFh>mm" position="runin" align="start"
	penColor="none"
	@- penWidth="0.2pt"
	penWidth="0"
	fillOpacity="0"
	crop="Y"
	>

@------------------------------------------------------------
@- 'Masking' frame
@------------------------------------------------------------
<Frame left="<#f.frameLeft>" top="<#f.frameTop>" width="44mm" height="50mm"
	penColor="none"
	penWidth="0"
	penTint="100"
	>
@------------------------------------------------------------
@- 'Actual' frame
@------------------------------------------------------------
<Frame left="<#f.l>mm" top="<#f.t>mm" width="<#f.w>mm" height="<#f.h>mm"
	fillColor="none"
	fillOpacity="100"
	fillTint="100"
	penColor="Black"
	penWidth="<#f.pw>mm"
	penTint="<#f.pt>"
	@- crop="Y"	@- Crops to size of center rectangle below
	>
</Frame>
<@macro> f.showAnnotations {
	@if(<#f.showAnnotations> = Y ) {
	<#def> f.pwXLeft	@eval(<#f.l> - (<#f.pw> / 2) )
	<#def> f.pwXRight	@eval(<#f.l> + (<#f.pw> / 2) )
@- f.dimPenWidthTop
@-===========================================================
@- Annotations
@-===========================================================
<mmDraw>
@------------------------------------------------------------
@- Show center of Frame
@------------------------------------------------------------
	<Rectangle left="<#f.l>mm" top="<#f.t>mm" width="<#f.w>mm" height="<#f.h>mm"
		penWidth="<#f.centerLineWidth>"
		penColor="<#f.centerLineColor>"
		penStyle="<#f.centerLineStyle>"
		/>
@------------------------------------------------------------------------
@- Pen width annotation
@------------------------------------------------------------------------
	<ALine
		left="<#f.pwXLeft>mm"
		top="<#f.dimPenWidthTop>mm"
		lineAngle="270"
		lineLength="2mm"
		textLength="0"
		textAngle="90"
		textColor="Blue"
		@- <#irot.arrowProps>
		textAlign="left"
		horizontalShift="50%"
		verticalShift="-50%"
		arrowHead="start"
		<#f.arrowProperties>
		></ALine>
	<ALine
		left="<#f.pwXRight>mm"
		top="<#f.dimPenWidthTop>mm"
		lineAngle="90"
		lineLength="3mm"
		textLength="0"
		textAngle="90"
		textColor="Blue"
		@- <#irot.arrowProps>
		textAlign="L"
		@-horizontalShift="-100%"
		horizontalShift="-45%"
		verticalShift="-50%"
		arrowHead="start"
		<#f.arrowProperties>
		>penWidth</ALine>
@- f.dimPenWidthTop
@------------------------------------------------------------------------
@- Draw arrow showing top distance between Frame and container
@------------------------------------------------------------------------
<@skipStart>
<@skipEnd>
	<ALine
		left="<#f.dimTopLeft>mm"
		top="0"
		lineAngle="180"
		lineLength="<#f.t>mm"
		textLength="0"
		textAngle="90"
		textColor="Blue"
		@- <#irot.arrowProps>
		textAlign="L"
		horizontalShift="50%"
		verticalShift="-20%"
		arrowHead="end"
		<#f.arrowProperties>
		>top</ALine>
@------------------------------------------------------------------------
@- Draw arrow showing left distance between Frame and container
@------------------------------------------------------------------------
	<ALine
		left="0"
		top="<#f.dimLeftTop>mm"
		lineAngle="90"
		lineLength="<#f.l>mm"
		textAngle="90"
		textColor="Blue"
		textAlign="center"
		arrowHead="end"
		horizontalShift="-110%"
		verticalShift="80%"
		<#f.arrowProperties>
		>left</ALine>
@------------------------------------------------------------------------
@- Draw both-end arrow showing width of container (at bottom)
@------------------------------------------------------------------------
	<ALine
		left="<#f.l>"
		top="<#f.dimWidthTop>mm"
		lineAngle="90"
		lineLength="<#f.w>mm"
		textAngle="90"
		textColor="Blue"
		@- <#irot.arrowProps>
		textAlign="center"
		horizontalShift="50%"
		arrowHead="both"
		verticalShift="80%"
		<#f.arrowProperties>
		>width</ALine>
<@skipStart>
@------------------------------------------------------------------------
@- Container border annotations
@------------------------------------------------------------------------
	<ALine
		left="<#f.l>"
		top="<#f.dimWidthTop>mm"
		lineAngle="90"
		lineLength="<#f.w>mm"
		textAngle="90"
		textColor="Blue"
		@- <#irot.arrowProps>
		textAlign="center"
		horizontalShift="50%"
		arrowHead="both"
		verticalShift="80%"
		<#f.arrowProperties>
		>Container border</ALine>
<@skipEnd>
@------------------------------------------------------------------------
@- Draw both-end arrow showing height of container (at right)
@------------------------------------------------------------------------
	<ALine
		top="<#f.t>mm"
		left="<#f.dimHeightLeft>"
		lineAngle="180"
		lineLength="<#f.h>mm"
		textAngle="90"
		textColor="Blue"
		textAlign="left"
		horizontalShift="60%"
		arrowHead="both"
		<#f.arrowProperties>
		>height</ALine>
</mmDraw>
		}
}
<|f.showAnnotations>
</Frame>
	@---------------------------------------------------------------
	@- 'Container' borders and annotation
	@---------------------------------------------------------------
	<ALine
		left="<#f.frameLeft>"
		top="<#f.frameTop>"
		lineAngle="90"
		lineLength="52mm"
		penColor="Maroon"
		penWidth="1.4pt"
		penStyle="dotted"
		arrowHead="none"
		></ALine>
	<ALine
		left="<#f.frameLeft>"
		top="<#f.frameTop>"
		lineAngle="180"
		lineLength="52mm"
		penColor="Maroon"
		penWidth="1.4pt"
		penStyle="dotted"
		arrowHead="none"
		></ALine>
	<ALine
		left="<#f.frameLeft>"
		@- top="<#f.frameTop>"
		top="<#f.borderAnnoTop>mm"
		@- top="43mm"
		lineAngle="90"
		lineLength="3mm"
		penStyle="solid"
		arrowHead="tail"
		penWidth="0.3pt"
		penOpacity="100" penColor="Maroon" arrowWidthRatio="8"
		fontFamily="Frutiger LT 45 Light" textSize="6.0pt"
		textPosition="head"
		textColor="Maroon"
		verticalShift="70%"
		>&ensp; <Font textColor="Maroon" 
			verticalShift="20%"
>&ensp;container border</Font></ALine>
<@skipStart>
<@skipEnd>
</AFrame>
