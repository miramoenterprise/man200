@--------------------------------------------------------------------------------
@- listLabelAnnoNew.mmp
@--------------------------------------------------------------------------------
<@xmacro> listLabelAnnoNew {
	@- <#def> llan.labelIndent			{}
	<#def> llan.labelTextAlign		{}
	@------------------------------------------------------------------------
	@- paragraph firstLineIndent
	@------------------------------------------------------------------------
	<#def> llan.fLI.show			Y
	<#def> llan.fLI				10mm
	<#def> llan.fLItop			10mm	@- Dimension rule top offset
	<#def> llan.fLIdelimiterTop		{}	@- Vertical delimiter start
	<#def> llan.fLIdelimiterLength		10mm	@- Vertical delimiter length
	@------------------------------------------------------------------------
	@- paragraph startIndent
	@------------------------------------------------------------------------
	<#def> llan.sI.show			Y
	<#def> llan.sI				10mm
	<#def> llan.sItop			10mm	@- Horizontal rule position (from top)
	<#def> llan.sIdelimiterTop		{}	@- Vertical delimiter start
	<#def> llan.sIdelimiterLength		10mm	@- Vertical delimiter length
	@------------------------------------------------------------------------
	@- listLabel area shadow
	@------------------------------------------------------------------------
	<#def> llan.LWshade.show		Y	@- Show label shading rectangle
	<#def> llan.LWshadeHeight		3.0mm	@- Label shading rectangle height
	<#def> llan.LWshadeTop			3.2mm	@- Top of shade area
	<#def> llan.LWshadeLength		10mm	@- Width of shade area
	<#def> llan.LWshadeColor		Black	@- Shading color
	<#def> llan.LWshadeTint			25%	@- Shading tint
	<#def> llan.LWdelimiterLength		5mm	@- Vertical delimiter length
	<#def> llan.LWdelimiterTop		0mm	@- Offset of vertical delimiter from top
	@------------------------------------------------------------------------
	@- listLabel area width dimension
	@------------------------------------------------------------------------
	<#def> llan.LW.show			Y	@- Show label width
	<#def> llan.LW				10mm
	<#def> llan.LWtop			{}	@- Label area dimension indicator
	<#def> llan.LWleft			1.7mm	@- Label area dimension indicator
	@------------------------------------------------------------------------
	@- listLabel area indent dimension
	@------------------------------------------------------------------------
	<#def> llan.LI.show			Y	@- Show label indent
	<#def> llan.LI				{}
	<#def> llan.LItop			{}	@- Label indent dimension indicator
	<#def> llan.LIleft			10mm	@- Label indent dimension indicator
	<#def> llan.LIpenStyle			{2pt 1pt}	@-
	@------------------------------------------------------------------------
	@xgetvals(llan.)
	@------------------------------------------------------------------------
	@if(@len(<#llan.LWtop>) = 0) {
		<#def> llan.LWtop		@eval(<#llan.LWshadeTop> - 1.5 )
		<#def> llan.LItop		@eval(<#llan.LWshadeTop> - 1.5 )
		}
	@if(@len(<#llan.LItop>) = 0) {
		<#def> llan.LWtop		@eval(<#llan.LWshadeTop> - 1.5 )
		<#def> llan.LItop		@eval(<#llan.LWshadeTop> - 1.5 )
		}
	@if(@len(<#llan.LI>) = 0) {
		<#def> llan.labelIndentAnno	@eval(<#llan.sI> - <#llan.LW>)
		}
	@else   {
		<#def> llan.labelIndentAnno	<#llan.LI>
		}
	@if(@len(<#llan.sIdelimiterTop>) = 0) {
		<#def> llan.sIdelimiterTop		@eval(<#llan.sItop> - 5)
		}
	@if(@len(<#llan.fLIdelimiterTop>) = 0) {
		<#def> llan.fLIdelimiterTop		@eval(<#llan.fLItop> - 5)
		}
	@if(@len(<#llan.labelTextAlign>) > 0) {
		<#def> llan.labelTextAlign	{ align="<#llan.labelTextAlign>" }
		}
	@- @if(@len(<#llan.LI>) = 0) {
	@----------------------------------------------------
	<#def> llan.labelEndPos		@eval(<#llan.labelIndentAnno> + <#llan.LW>)
	@----------------------------------------------------
<#def> listLabelAnnoNew {
	@if(@match({<#llan.LW.show>}, {Y})) {
		@------------------------------------------------------------
		@- label 'width' dimension (horizontal)
		@------------------------------------------------------------
		<ALine
			fmt="anno"
			left="<#llan.labelIndentAnno>"
			top="<#llan.LWtop>"
			lineLength="<#llan.LW>"
			textLen="4mm"
			penWidth="0.2pt"
			arrowHead="both"
			arrowWidthRatio="10"
			lineAngle="90"
			textColor="Blue"
			ff="Cambridge" fw="Regular"
			penColor="Blue"
		>lW</ALine>
		@------------------------------------------------------------
		@- 'labelWidth' end position delimiter (vertical)
		@------------------------------------------------------------
		<ALine
			left="<#llan.labelEndPos>"
			top="<#llan.LWdelimiterTop>"
			lineLength="<#llan.LWdelimiterLength>"
			penWidth="0.2pt"
			arrowHead="none"
			lineAngle="180"
			penStyle="2pt 1pt"
			penColor="Blue"
		/>
		@------------------------------------------------------------
		@- label indent delimiter (vertical)
		@------------------------------------------------------------
		<ALine
			left="<#llan.labelIndentAnno>"
			top="<#llan.LWdelimiterTop>"
			lineLength="<#llan.LWdelimiterLength>"
			penWidth="0.2pt"
			arrowHead="none"
			lineAngle="180"
			penStyle="2pt 1pt"
			penColor="Blue"
		/>
		@------------------------------------------------------------
		@if(@match({<#llan.LWshade.show>}, {Y})) {
			@------------------------------------------------------------
			@- label rectangle
			@------------------------------------------------------------
			<Rectangle
				left="<#llan.labelIndentAnno>"
				top="<#llan.LWshadeTop>"
				width="<#llan.LW>"
 				height="<#llan.LWshadeHeight>"
				penWidth="0.2pt"
				penStyle="2pt 1pt"
				penColor="Blue"
				fillColor="Black"
				fillTint="<#llan.LWshadeTint>"
			/>
			@------------------------------------------------------------
			}
		}
	@if(@match({<#llan.LI.show>}, {Y})) {
		@------------------------------------------------------------
		@- label 'indent' dimension (horizontal)
		@------------------------------------------------------------
		<ALine
			fmt="anno"
			left="0"
			@- top="1.7mm"
			top="<#llan.LItop>"
			lineLen="<#llan.labelIndentAnno>"
			textLen="3.2mm"
			penWidth="0.2pt"
			arrowHead="end"
			arrowWidthRatio="10"
			lineAngle="90"
			ff="Cambridge" fw="Regular"
			textColor="Blue"
			penColor="Blue"
			penStyle="<#llan.LIpenStyle>"
		>lI</ALine>
		@------------------------------------------------------------
		}
	@if(@match({<#llan.fLI.show>}, {Y})) {
		@------------------------------------------------------------
		@- 'firstLineIndent' dimension (horizontal)
		@------------------------------------------------------------
		<ALine
			fmt="anno"
			left="0"
			@- top="9.8mm"
			@- top="7.3mm"
			top="<#llan.fLItop>"
			lineLength="<#llan.fLI>"
			textLength="8mm"
			textColor="Black"
			penWidth="0.2pt"
			arrowHead="end"
			arrowWidthRatio="10"
			lineAngle="90"
			ff="Cambridge" fw="Regular"
			penColor="Red"
		>pgf.fLI</ALine>
		@------------------------------------------------------------
		@- 'firstLineIndent' delimiter (vertical)
		@------------------------------------------------------------
		<ALine
			left="<#llan.fLI>"
			@- top="2.7mm"
			top="<#llan.fLIdelimiterTop>"
			lineLength="<#llan.fLIdelimiterLength>"
			penWidth="0.2pt"
			arrowHead="none"
			lineAngle="180"
			penStyle="2pt 1pt"
			penColor="Red"
		/>
		@------------------------------------------------------------
		}
	@if(@match({<#llan.sI.show>}, {Y})) {
		@------------------------------------------------------------
		@- 'startIndent' dimension (horizontal)
		@------------------------------------------------------------
		<ALine
			fmt="anno"
			left="0"
			@- top="7.3mm"
			top="<#llan.sItop>"
			lineLength="<#llan.sI>"
			@- textLen="7mm"
			textLen="6.5mm"
			textColor="Blue"
			penWidth="0.2pt"
			arrowHead="end"
			arrowWidthRatio="10"
			lineAngle="90"
			fontFamily="Cambridge" fontWeight="Regular"
			penColor="Blue"
		>pgf.sI</ALine>
		@------------------------------------------------------------
		@- 'startIndent' delimiter (vertical)
		@------------------------------------------------------------
		<ALine
			left="<#llan.sI>"
			top="<#llan.sIdelimiterTop>"
			lineLength="<#llan.sIdelimiterLength>"
			penWidth="0.2pt"
			arrowHead="none"
			lineAngle="180"
			penStyle="2pt 1pt"
			penColor="Blue"
		/>
		@------------------------------------------------------------
		}
		@------------------------------------------------------------
	}
@- <#listLabelAnnoNew>
}{}
@--------------------------------------------------------------------------------
<@skipStart>
@--------------------------------------------------------------------------------
<listLabelAnnoNew
	show.LI="Y"
	show.LWshade="Y"
	show.LW="Y"
	show.fLI="Y"
	show.sI="Y"
		@------------------------------------------------------------
	show.fLI="Y"
	show.LI="Y"
	show.LWshade="Y"
	show.LW="Y"
	show.sI="Y"
	/>
@--------------------------------------------------------------------------------
<@skipEnd>
@--------------------------------------------------------------------------------
