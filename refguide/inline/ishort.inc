@-----------------------------------------------------------
@- ishort.inc
@- include files for Inline Markup Codes
@-----------------------------------------------------------
<chapterStart
	dest="inlineMarkupElements"
	pgfSuffix="ExA"
	>Inline markup elements
	@- >Inline <Font fontAngle="Italic" >markup</Font> <Font textSize="+30%" textColor="Red" >elements</Font>
</chapterStart>
<#def> inINLINE		1
<#def> elementType	inline
<#def> hiddenCodeCount	0
<#def> hiddenCodeCountx	0
<#def> exampleTextOnlyCount	0
<#def> eb.processNumericEntities	N
@-----------------------------------------------------------
<@include>	${inline}/introduction
<Section allPages="MP_referenceGuide" />
@- <@include>	${inline}/aframe
@- <@include>	${inline}/image
<@include>	${inline}/columns
<@include>	${inline}/objectannotation
<@skipStart>
@-----------------------------------------------------------
<@include>	${inline}/aframe
<@include>	${inline}/atextframe
<@include>	${inline}/cell
<@include>	${inline}/changebarbegin
<@include>	${inline}/changebarend
<@include>	${inline}/chapter
<@include>	${inline}/contents
<@include>	${inline}/contentsinclude
<@include>	${inline}/currentPageNumber
<@include>	${inline}/date
@- <@skipStart>
<@include>	${inline}/fnote
<@include>	${inline}/font
<@include>	${inline}/frame
<@include>	${inline}/hypercmd
@- <@skipStart>
<@include>	${inline}/image
<@include>	${inline}/imagedef
<@include>	${inline}/index
<@skipStart>
<@include>	${inline}/ix
<@include>	${inline}/lastPageNumber
@- <@skipStart>
<@include>	${inline}/line
<@include>	${inline}/listlabel
<@include>	${inline}/mathml
<@include>	${inline}/mkdest
<@include>	${inline}/objecttitle
<@include>	${inline}/p
<@include>	${inline}/paragroup
<@include>	${inline}/row
<@include>	${inline}/runningtext
<@include>	${inline}/runningtextmarker
<@include>	${inline}/section
<@include>	${inline}/tbl
<@include>	${inline}/tblcolumn
<@include>	${inline}/tblcontinuation
<@include>	${inline}/tbltitle
<@include>	${inline}/textframe
<@include>	${inline}/var
<@include>	${inline}/xref
@-----------------------------------------------------------
<@skipEnd>
<#def> inINLINE	0
@-----------------------------------------------------------
<chapterEnd/>
@-----------------------------------------------------------
