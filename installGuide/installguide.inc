@--------------------------------------------------------------------------------
@- linuxinstall.inc
@- 
@--------------------------------------------------------------------------------
<@include>	${installGuide}/cover.inc
<@include>      ${refguide}/toc/toc
<chapterStart
	dest="linuxInstall"
	>Linux installation
</chapterStart>
<@include>	${installGuide}/linuxinstall
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

