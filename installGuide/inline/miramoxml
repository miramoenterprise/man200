@------------------------------------------------------------
@-- miramoxml
@------------------------------------------------------------

<elementStart elementType="xml_mand_atts" elementName="MiramoXML" >

<esp>
The <#xMiramoXML> element has been extended for producing output in
the following formats:
Responsive HTML5, EPUB 3, WebHelp, Microsoft HTML help and Kindle.
</esp>

<esp>
The following <#xMiramoXML> properties are effective only when
running with FrameMaker 12 or later.
See Note <xparanumonly
	id="MiramoXML.note.fmVersion" /> on page <xnpage
	id="MiramoXML.note.fmVersion" />.
</esp>


</elementStart>


@------------------------------------------------------------
<|propertiesStart>
@------------------------------------------------------------

@------------------------------------------------------------
<propertyGroup
	shortTitle="Single-document and book output properties"
	dest="<#elementName>.propertyGroup.fmaddendum"
	/>


@------------------------------------------------------------
<property
	long="Oepub"
	value="filename"
	>
<pdp>
File name for output in EPUB format.
</pdp>


<propertyDefault>fmPublishOutput, as a sub-folder in the current working folder</propertyDefault>

</property>

@------------------------------------------------------------
<property
	long="Omshelp"
	value="filename"
	>
<pdp>
File name for output in Microsoft Help format (.chm).
</pdp>

</property>

@------------------------------------------------------------
<property
	long="Okindle"
	value="filename"
	>
<pdp>
File name for output in Kindle format (.mobi).
See Note <xparanumonly
        id="MiramoXML.note.kindle" /> on page <xnpage
        id="MiramoXML.note.kindle" />.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="Ohtml5"
	value="foldername"
	>
<pdp>
Folder name for output in HTML 5 format.
If <fI>foldername</fI> already exists, it must be an
empty folder. Otherwise the job will fail.
On job completion <fI>foldername</fI> will contain several
files and sub-folders, e.g. HTML, JavaScript and CSS
files.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="OwebHelp"
	value="foldername"
	>
<pdp>
Folder name for output in web help format.
If <fI>foldername</fI> already exists, it must be an
empty folder. Otherwise the job will fail.
On job completion <fI>foldername</fI> will contain several
files and sub-folders, e.g. HTML, JavaScript and CSS
files.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="stsFile"
	value="filename"
	>
<pdp>
File name of .sts configuration file saved from FrameMaker.
</pdp>

<pdp>
The .sts FrameMaker configuration <fI>filename</fI> must be readable
and writable by the Miramo RunAs user. If not, an error will occur.
In this case the <programName>mmaccess</programName> utility may be
used to grant <fI>filename</fI> read/write permissions to the runAs user.
See Note <xparanumonly
        id="MiramoXML.note.mmaccess_file" />.
</pdp>

<propertyDefault>%MM_HOME%/postprocessors/Settings.sts</propertyDefault>

</property>

@------------------------------------------------------------
<property
	long="Bsaveformat"
	value="MIF | binary"
	>
<pdp>
Save a generated book file in MIF or binary format.
Ignored unless the <pn>-Bfile</pn> command line option or the
equivalent <#xMiramoXML> <pn>Bfile</pn> option is used to output
to a FrameMaker book.
If one of the FMpublish options,
<pn>Oepub</pn>,
<pn>Omshelp</pn>,
<pn>Okindle</pn> or
<pn>OwebHelp</pn>, is used,
the default value is <pv>binary</pv>. In all other cases the default is <pv>MIF</pv>.
</pdp>

</property>

@------------------------------------------------------------
<property
	long="roleMap"
	value="filename"
	>
<pdp>
See the description of the <pn>-roleMap</pn> command line option in
Table <xparanumonly id="tbl.fmcaddendum.command_line_options.start" /> on
page <xnpage id="roleMapCommandLineOption" />.
</pdp>

</property>

@------------------------------------------------------------


@------------------------------------------------------------
<|propertiesEnd>
@------------------------------------------------------------

@------------------------------------------------------------
<elementNote
	dest="MiramoXML.note.fmVersion"
	>
<enp>
If any of the <#xMiramoXML> <pn>Oepub</pn>,
<pn>Omshelp</pn>,
<pn>Okindle</pn>,
<pn>Ohtml5</pn> 
 or 
<pn>OwebHelp</pn> properties are used
(or any of the corresponding command line
options shown in Table <xparanumonly
  id="tbl.fmcaddendum.command_line_options" /> on page <xnpage
  id="tbl.fmcaddendum.command_line_options" />) and
the installed version of FrameMaker is earlier than FrameMaker 12, an
error will occur and the job will fail.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="MiramoXML.note.mmaccess_folderError"
	>
<enp>
If the values for <fI>folder</fI> or <fI>foldername</fI> refers
to a file or folder that
is not writable by the Miramo RunAs user
Miramo will fail with the following error:
</enp>

<exampleBlock
	>
MM_ERR_FMPUBLISHER_OUTPUT_DIR (556), Miramo runAs user
does not have write permission to specified output folder.
</exampleBlock>

<enp>
If this error occurs, create the folder and use
the <programName>mmaccess</programName> utility
to grant write permission to the runAs user.
See Note <xparanumonly
	id="MiramoXML.note.mmaccess_folder" />.
</enp>


</elementNote>

@------------------------------------------------------------
<elementNote
	dest="MiramoXML.note.mmaccess_folder"
	>
<enp>
<programName>mmaccess</programName> is a command-line utility that grants the Miramo
RunAs user write access to a user's folder.
Miramo jobs cannot run unless the Miramo RunAs user has
read/write access to the output folder. The following
illustrates possible usage of <programName>mmaccess</programName> to
grant the Miramo RunAs user read/write access to a folder:
</enp>

<exampleBlock
	>
mkdir <xfI>foldername</xfI>
mmaccess -dir <xfI>foldername</xfI>
</exampleBlock>

<enp>
<fI>foldername</fI> may be a relative or absolute path.
</enp>

<enp>
Use <programName>mmaccess -h</programName> to see a full list of options.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="MiramoXML.note.mmaccess_file"
	>
<enp>
The <programName>mmaccess</programName> command-line utility may
also be used to grant read/write access to a specific file, e.g.:
</enp>

<exampleBlock
	>
mmaccess -file <xfI>filename</xfI>.sts
</exampleBlock>

<enp>
<fI>filename</fI>.sts may be the name of a file in the current folder
or a full path name.
</enp>

<enp>
Use <programName>mmaccess -h</programName> to see a full list of options.
</enp>

</elementNote>

@------------------------------------------------------------
<elementNote
	dest="MiramoXML.note.kindle"
	>
<enp>
If the <pv>Okindle</pv> property is used
the <programName>kindlegen.exe</programName> executable
must be present in the <programName>%MM_HOME%/bin</programName> 
 folder, otherwise the job will fail with the following error message:
</enp>

<exampleBlock
	>
MM_ERR_KINDLEGEN_EXE (555)
"Cannot locate kindlegen.exe - download it, copy kindlegen.exe
to %MM_HOME%/bin and try again"
</exampleBlock>

<enp>
If the <programName>kindlegen.exe</programName> executable is already present
on the Miramo host, it may be copied to the folder <programName
	>%MM_HOME%/bin</programName>. If the <programName
	>kindlegen.exe</programName> executable is not already present
on the Miramo host, it may be downloaded from the Amazon website&emdash;from
the KindleGen <URL
	url="http://www.amazon.com/gp/feature.html?docId=1000765211"
	>download page</URL>. Extract
the <programName>kindlegen.exe</programName> file from the downloaded
zip archive and copy it to the folder <programName
	>%MM_HOME%/bin</programName>.
</enp>

</elementNote>


@------------------------------------------------------------
@- EXAMPLES
@------------------------------------------------------------
<exp>
Example <xparanumonly
	id="MiramoXML.example.using1" /> illustrates the
usage of the <#xMiramoXML> element.
</exp>

<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
	dest="MiramoXML.example.using1"
	>
Using the <#xMiramoXML> element (1)
</exampleCaption>
<MiramoXML Oepub="forEPUB.epub" stsFile="myConfig.sts" >
<!-- Output in <xfB>file</xfB> and <xfB>settings</xfB> values are relative to the -->
<!-- current working folder, or can be absolute -->
	<P><xfI>miramo_input: document content</xfI></P>
</MiramoXML>
</exampleBlock>

<@skipStart>
<exp>
Examples <xparanumonly
	id="MiramoXML.example.using2" /> and <xparanumonly
	id="MiramoXML.example.using3" /> produce the
same output as, and are equivalents of,
Example <xparanumonly
	id="MiramoXML.example.using1" />.
</exp>


<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
	dest="MiramoXML.example.using2"
	>
Using the <#xMiramoXML> element (2)
</exampleCaption>
<MiramoXML MiramoXML="Y" >
	<P><xfI>fmpublisher_test.xml: document content</xfI></P>
</MiramoXML>
</exampleBlock>

<exampleBlock
	lineNumbers="Y"
	>
<exampleCaption
	dest="MiramoXML.example.using3"
	>
Using the <#xMiramoXML> element (3)
</exampleCaption>
<MiramoXML>
	<P><xfI>fmpublisher_test.xml: document content</xfI></P>
</MiramoXML>
</exampleBlock>

<exp>
In the case of Example <xparanumonly
	id="MiramoXML.example.using3" /> the <programName>-MiramoXML Y</programName> command
line option must be used.
E.g.
</exp>

<exampleBlock
	lineNumbers="N"
	>
@- <exampleCaption
@- 	dest="MiramoXML.example.using4"
@- 	>
@- Using the <#xMiramoXML> element
@- </exampleCaption>
1iramo -composer mmc -MiramoXML Y <xfI>inputfile.mmxml</xfI>
</exampleBlock>

<exp>
where <fI>inputfile.mmxml</fI> is a Miramo input file.
</exp>

<@skipEnd>

@------------------------------------------------------------
<|elementEnd>
@------------------------------------------------------------
