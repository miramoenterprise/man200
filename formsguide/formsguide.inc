@--------------------------------------------------------------------------------
@- fmcrelnotes.inc
@- include files for drawguide guide
@--------------------------------------------------------------------------------
<MasterPageRule Page="MP_Cover1"  allPages="MP_Cover2" />
@-----------------------------------------------------------
<@include>	${covers}/refguideCover
@-----------------------------------------------------------
<@include>      ${refguide}/toc/toc.inc
<@include>	${formsguide}/introduction
@--------------------------------------------------------------------------------
<MasterPageRule allPages="MP_referenceGuide" />
@--------------------------------------------------------------------------------
<@include>	${formsguide}/acroform
<@include>	${formsguide}/combobox
<@skipStart>
<@skipEnd>
@--------------------------------------------------------------------------------

